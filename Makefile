#!make
MAKE_ENV_FILE  ?= .env
BUILD_ENV_FILE ?= .build.env
NAME            = Geeftlist
VERSION         = 0.1.0
TARGETS        := $(MAKEFILE_LIST)
SHELL          := /bin/bash

WEB_LOCAL_PATH := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
export WEB_LOCAL_PATH

$(shell test -f .env || sed -e "s/^.*WEBAPP_UID=.*/WEBAPP_UID=$$(id -u)/g" -e "s/^.*WEBAPP_GID=.*/WEBAPP_GID=$$(id -g)/g" .env.sample > .env)

include ${BUILD_ENV_FILE}
export $(shell grep -vE '^(#|$$)' ${BUILD_ENV_FILE} | sed 's/=.*//')

include ${MAKE_ENV_FILE}
export $(shell grep -vE '^(#|$$)' ${MAKE_ENV_FILE} | sed 's/=.*//')

RUN_USER        ?= $(WEBAPP_UID):$(WEBAPP_GID)

.PHONY: help
help: ## [Help] This help
	@grep -hE '^[2a-zA-Z_-]+:.*?## .*$$' $(TARGETS) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo "All targets are also usable under the 'developer' environment by using the 'dev-' prefix. E.g. 'make dev-upd'"


#
# DEV TARGETS
#

dev-%: export COMPOSE_FILE = docker-compose.yml:docker-compose.dev-ci.yml:docker-compose.dev.yml
dev-%: export WEBAPP_IMAGE_NAME = geeftlist-dev
dev-%:
	$(MAKE) $*

.PHONY: dev-theme-shell
dev-theme-shell: build-theme-builder ## [Dev] Run the theme-builder container.
	docker compose rm -f theme-builder
	docker compose run \
		--rm \
		-v $$(pwd)/src:/themebuilder \
		--name ${DOCKER_THEMEBUILDER_CONTAINER_NAME} \
		--user ${WEBAPP_UID} \
		$(run_args) \
		theme-builder

.PHONY: dev-theme-build
dev-theme-build: build-theme-builder ## [Dev] Build theme assets.
	docker compose rm -f theme-builder
	docker compose run \
		--rm \
		-v $$(pwd)/src:/themebuilder \
		--name ${DOCKER_THEMEBUILDER_CONTAINER_NAME} \
		--user ${WEBAPP_UID} \
		$(run_args) \
		theme-builder \
		bash -c 'cd app/ui/default/ && ([ -d node_modules ] || yarn install) && npm run build'

.PHONY: dev-theme-watch
dev-theme-watch: build-theme-builder ## [Dev] Watch & live-build theme assets.
	docker compose run \
		--rm \
		-v $$(pwd)/src:/themebuilder \
		--name ${DOCKER_THEMEBUILDER_CONTAINER_NAME}_watch \
		--user ${WEBAPP_UID} \
		theme-builder \
		bash -c 'cd app/ui/default/ && ([ -d node_modules ] || yarn install) && npm start'

.PHONY: dev-xhprofgui
dev-start-xhprofgui: ## [Dev] Start the Xhprof profile viewer in the webapp container.
	docker compose exec \
		webapp \
		php -S 0.0.0.0:8142 -t /usr/local/lib/php/xhprof_html/

.PHONY: dev-fix-perms
dev-fix-perms: ## [Dev] Fix directories & files permissions
	IFS=':'; for d in "${WWW_WRITABLE_DIRS}"; do \
		mkdir -p $$d; \
		sudo chown -R ${WWW_UID}:${WEBAPP_GID} $$d \
			&& sudo chmod -R ug+rwX $$d; \
	done

.PHONY: dev-install
dev-install: ## [Dev] Install dependencies and application
	$(MAKE) dev-composer-install \
		&& $(MAKE) dev-fix-perms \
		&& $(MAKE) dev-install-app

.PHONY: dev-composer-install
dev-composer-install:
	$(MAKE) dev-bash-exec cmd="cd src/ && composer install"

.PHONY: dev-install-app
dev-install-app:
	$(MAKE) app-install && $(MAKE) app-install-sample-data

.PHONY: dev-clear-cache
dev-clear-cache:
	docker compose exec redis-cache redis-cli flushall

#
# CI TARGETS
#

ci-%: export COMPOSE_FILE = docker-compose.yml:docker-compose.dev-ci.yml:docker-compose.ci.yml
ci-%:
	$(MAKE) $*

#
# COMMON TARGETS
#

.PHONY: env
env: ## [Debug] Print current environment variables
	env | sort

.PHONY: config
config: ## [Debug] Print docker compose config
	printf "%80s\n" | tr " " "=" \
		&& docker compose config \
		&& printf "%80s\n" | tr " " "="

.PHONY: up
up: build ## [Docker] Start the containers. Args: up_args=...
	docker compose up webapp $(up_args)

.PHONY: upd
upd: build ## [Docker] Start the containers (detached). Args: up_args=...
	docker compose up \
		--detach \
		$(up_args) \
		webapp

.PHONY: pull
pull: ## [Docker] Pull latest images
	docker compose pull --ignore-pull-failures

.PHONY: prepare-build
prepare-build: ## [Docker] Prepare build
	$(MAKE) pull
	docker compose down --remove-orphans

.PHONY: build
build: build-theme-builder build-base-web ## [Docker] Build the webapp image and dependencies (via docker compose). Args: build_args=...
	docker compose $(build_args) build webapp

.PHONY: build-theme-builder
build-theme-builder: ## [Docker] Build the theme-builder image.
	$(MAKE) config
	test ! -z "$$(docker images -f reference=${DOCKER_THEMEBUILDER_CONTAINER_NAME}:${DOCKER_THEMEBUILDER_IMAGE_TAG} -q)" \
		|| $(MAKE) build-theme-builder-force

.PHONY: build-theme-builder-force
build-theme-builder-force: ## [Docker] Build the theme-builder image.
	$(MAKE) config
	docker compose build theme-builder

.PHONY: build-base-web
build-base-web: ## [Docker] Build the base-web image.
	$(MAKE) config
	test ! -z "$$(docker images -f reference=${DOCKER_BASE_WEB_IMAGE_NAME}:${DOCKER_BASE_WEB_IMAGE_TAG} -q)" \
		|| $(MAKE) build-base-web-force

.PHONY: build-base-web-force
build-base-web-force: ## [Docker] Force build the base-web image.
	$(MAKE) config
	docker compose build base-web

.PHONY: stop
stop: ## [Docker] Stop the containers. Args: service=...
	docker compose stop $(service)

.PHONY: rm
rm: ## [Docker] Remove containers. Args: rm_args=...
	docker compose rm $(rm_args)

.PHONY: down
down: ## [Docker] Stop & remove all the containers and associated networks. Args: down_args=...
	docker compose down $(down_args)

.PHONY: destroy
destroy: ## [Docker] Alias for down
	$(MAKE) down down_args=$(destroy_args)

.PHONY: ps
ps: ## [Docker] Show running containers
	docker compose ps -a

.PHONY: logs
logs: ## [Docker] Show output of containers. Args: logs_args=...
	docker compose logs $(logs_args)

.PHONY: logs-follow
logs-follow: ## [Docker] Show output of containers and follow. Args: logs_args=...
	docker compose logs -f --tail=100 $(logs_args)

.PHONY: bash
bash: ## [Docker] Enter bash shell in webapp container with webapp user. Args: exec_args=...
	docker compose exec \
		--user ${WEBAPP_UID} \
		$(exec_args) \
		webapp \
		/bin/bash -c 'cd ${WEBAPP_PATH}; /bin/bash'

.PHONY: bash-exec
bash-exec: ## [Docker] Run a command in bash shell in webapp container with webapp user. Args: exec_args=... cmd=...
	docker compose exec \
		--user ${WEBAPP_UID} \
		$(exec_args) \
		webapp \
		/bin/bash -c 'cd ${WEBAPP_PATH}; /bin/bash -c "$(cmd)"'

.PHONY: bash-run
bash-run: build-theme-builder build ## [Docker] Run a command in bash shell in a dedicated webapp container with webapp user. Args: run_args=... cmd=...
	docker compose run \
		--rm \
		-e RUN_VERBOSE=${RUN_VERBOSE} \
		$(run_args) \
		webapp \
		$(cmd)

.PHONY: bash-run-test
bash-run-test: build-theme-builder build ## [Docker] Run a command in bash shell in a dedicated webapp container with webapp user using test environment configuration. Args: run_args=... cmd=...
	docker compose run \
		--rm \
		-e RUN_VERBOSE=${RUN_VERBOSE} \
		-e app__RESOURCE_CONFIG__db__hostname=${MARIADB_TEST_HOSTNAME} \
		-e app__RESOURCE_CONFIG__db__database=${MARIADB_TEST_DATABASE} \
		-e app__RESOURCE_CONFIG__db__username=${MARIADB_TEST_USER} \
		-e app__RESOURCE_CONFIG__db__password=${MARIADB_TEST_PASSWORD} \
		-e app__RESOURCE_CONFIG__redis_cache__database=${REDIS_CACHE_TEST_DATABASE} \
		$(run_args) \
		webapp \
		$(cmd)

.PHONY: bash-test
bash-test:
	$(MAKE) bash-run-test run_args="--user ${WEBAPP_UID}" cmd="/bin/bash"

.PHONY: bash-www-data
bash-www-data: ## [Docker] Enter bash shell in webapp container as www-data. Args: exec_args=...
	docker compose exec \
		$(exec_args) \
		webapp \
		/bin/bash -c 'cd ${WEBAPP_PATH}; su www-data -s /bin/bash'

.PHONY: bash-root
bash-root: ## [Docker] Launch bash on webapp container as root
	docker compose exec webapp /bin/bash


#
# APP TARGETS
#

.PHONY: app-install
app-install: cmd = /bin/bash ${WEBAPP_PATH}/resources/shell/install_app.sh $(install_app_args)
app-install: override run_args += -T
app-install: bash-run ## [Install] Run install scripts

.PHONY: app-install-sample-data
app-install-sample-data: cmd = /bin/bash ${WEBAPP_PATH}/resources/shell/install_sampledata.sh
app-install-sample-data: override run_args += -T
app-install-sample-data: bash-run ## [Install] Run install scripts

.PHONY: app-build-run-install
app-build-run-install: build upd app-install ## [Install] Build, start containers and run install scripts


#
# RUN TARGETS
#

.PHONY: run-phploc
run-phploc: override run_args += -T --user $(RUN_USER)
run-phploc: cmd = /bin/bash ${WEBAPP_PATH}/resources/shell/run_phploc.sh $(phploc_opts)
run-phploc: bash-run-test ## [CC] Run PHPLoc analysis. Args: run_args=..., phploc_opts=...

.PHONY: run-phpcs
run-phpcs: override run_args += -T --user $(RUN_USER)
run-phpcs: cmd = /bin/bash ${WEBAPP_PATH}/resources/shell/run_phpcs.sh $(phpcs_opts)
run-phpcs: bash-run-test ## [CC] Run PHPCS. Args: run_args=..., phpcs_opts=...

.PHONY: run-phpstan
run-phpstan: override run_args += -T --user $(RUN_USER)
run-phpstan: cmd = /bin/bash ${WEBAPP_PATH}/resources/shell/run_phpstan.sh $(phpstan_opts)
run-phpstan: bash-run-test ## [CC] Run PHPStan. Args: run_args=..., phpstan_opts=...

.PHONY: run-unit-tests
run-unit-tests: override run_args += -T --user $(RUN_USER)
run-unit-tests: cmd = /bin/bash ${WEBAPP_PATH}/resources/shell/run_unit_tests.sh $(ut_opts)
run-unit-tests: bash-run-test ## [UT] Run Unit Tests. Args: run_args=..., ut_opts=...


#
# MISC
#
.PHONY: loadtest-export-geefter-emails
loadtest-export-geefter-emails:
	cat $(WEB_LOCAL_PATH)/resources/load-test/export_geefters.sql \
		| docker compose exec -T db sh -c ' \
			rm -f /tmp/geefters.csv \
			&& mysql -u root -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} \
			&& cat /tmp/geefters.csv' \
		> $(WEB_LOCAL_PATH)/resources/load-test/geefters.csv
