<?php

return [
    // ########################################################################
    // Values
    // ########################################################################
    // == OliveOil
    \OliveOil\Core\Service\Route::class . '.params' => function(\DI\Container $container) {
        return [
            'route_mapping' => [
                // Left side: what's defined in routing*.ini
                // Right side: what's visible to the client instead
                '/api' => $container->get('api.area_url_path')
            ]
        ];
    },
    \OliveOil\Core\Service\Session\Factory::class => DI\autowire()
        ->constructorParameter('sessionClass', \OliveOil\Core\Model\Session\Api::class),

    // Event Services
    'event_services.all' => [
        // Note: The array key here is NOT the event space, just a unique key to identify each instance
        // and to be able to override each one if necessary. For event space, see 40-virtual.php
        'default' => \DI\get(\OliveOil\Core\Service\Event::class),
        'activity' => \DI\get('Geeftlist\Service\Event\Activity'),
        'activitylog' => \DI\get('Geeftlist\Service\Event\ActivityLog'),
        'adminotification' => \DI\get('Geeftlist\Service\Event\AdminNotification'),
        //'cron' => \DI\get('Geeftlist\Service\Event\Cron'),
        'geefteraccess' => \DI\get('Geeftlist\Service\Event\GeefterAccess'),
        'geefternotification' => \DI\get('Geeftlist\Service\Event\GeefterNotification'),
        'index' => \DI\get('Geeftlist\Service\Event\Index'),
        'cache' => \DI\get('Geeftlist\Service\Event\Cache'),
        'mailnotification' => \DI\get('Geeftlist\Service\Event\MailNotification'),
        //'profiler' => \DI\get('OliveOil\Core\Service\Event\Dev\Profiler'),
    ],

    // == Geeftlist
    'Geeftlist\Service\Authentication\Geefter\Api\AllMethodFactory.codeClassArray' => [
        'password' => \Geeftlist\Service\Authentication\Geefter\PasswordMethod::class,
        'apikey' => \Geeftlist\Service\Authentication\Geefter\ApiKeyMethod::class,
        'bearer' => \Geeftlist\Service\Authentication\Geefter\JwtMethod::class,
    ],
    'Geeftlist\Service\Authentication\Geefter\Api\MethodFactory.codeClassArray' => [
        'bearer' => \Geeftlist\Service\Authentication\Geefter\JwtMethod::class,
    ],
    'Geeftlist\Service\Rest\Factory.codeClassArray' => [
        \Geeftlist\Model\Badge::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Badge::class,
        \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Discussion\Topic::class,
        \Geeftlist\Model\Discussion\Post::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Discussion\Post::class,
        \Geeftlist\Model\Family::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Family::class,
        \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Family\MembershipRequest::class,
        \Geeftlist\Model\Geeftee::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geeftee::class,
        \Geeftlist\Model\Geefter::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geefter::class,
        \Geeftlist\Model\Gift::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Gift::class,
        \Geeftlist\Model\GiftList::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\GiftList::class,
        \Geeftlist\Model\Reservation::ENTITY_TYPE => \Geeftlist\Service\Rest\JsonApi\ModelHandler\Reservation::class,
    ],

    // Third-party
    \Art4\JsonApiClient\Manager::class => \DI\get(\Art4\JsonApiClient\Manager\ErrorAbortManager::class),
    \Neomerx\JsonApi\Encoder\Encoder::class . '.options' => function (\Psr\Container\ContainerInterface $container) {
        if ($container->get(\OliveOil\Core\Model\App\Config::class)->getValue('MODE') == 'developer') {
            return JSON_PRETTY_PRINT;
        }

        return 0;
    },
    \Neomerx\JsonApi\Schema\SchemaContainer::class . '.schemas' => [
        \Geeftlist\Model\Badge::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Badge::class,
        \Geeftlist\Model\Discussion\Post::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Discussion\Post::class,
        \Geeftlist\Model\Discussion\Topic::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Discussion\Topic::class,
        \Geeftlist\Model\Family::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Family::class,
        \Geeftlist\Model\Family\MembershipRequest::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Family\MembershipRequest::class,
        \Geeftlist\Model\Geeftee::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Geeftee::class,
        \Geeftlist\Model\Geefter::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Geefter::class,
        \Geeftlist\Model\Gift::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Gift::class,
        \Geeftlist\Model\GiftList::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\GiftList::class,
        \Geeftlist\Model\Reservation::class => \Geeftlist\Service\Rest\JsonApi\ModelSchema\Reservation::class,
    ],

    // == Misc

    // ########################################################################
    // Interface/class Preferences
    // ########################################################################
    // == OliveOil
    \OliveOil\Core\Model\Rest\RequestInterface::class
        => \DI\autowire(\Geeftlist\Model\Api\Rest\JsonApi\Request::class),
    \OliveOil\Core\Model\Rest\ResponseInterface::class
        => \DI\autowire(\OliveOil\Core\Model\Rest\Response::class),
    \OliveOil\Core\Model\AppInterface::class => DI\autowire(\Geeftlist\Model\App\Http::class),
    \OliveOil\Core\Model\Http\RequestInterface::class => \DI\autowire(\Geeftlist\Model\Api\Rest\JsonApi\Request::class),
    \OliveOil\Core\Model\Http\ResponseInterface::class => \DI\autowire(\OliveOil\Core\Model\Rest\Response::class),
    \OliveOil\Core\Service\App\ErrorHandlerInterface::class
        => DI\autowire(\OliveOil\Core\Service\App\Api\Rest\JsonApi\ErrorHandler::class),
    \OliveOil\Core\Service\Http\ResponseWriterInterface::class
        => \DI\get(\OliveOil\Core\Service\Rest\JsonApi\ResponseWriter::class),
    \OliveOil\Core\Service\RouteInterface::class => \DI\autowire(\OliveOil\Core\Service\Route\Api::class)
        ->constructorParameter('params', \DI\get(\OliveOil\Core\Service\Route::class . '.params')),

    // == Geeftlist
    \Geeftlist\Model\Session\GeefterInterface::class => DI\autowire(\Geeftlist\Model\Session\Api\Geefter::class)
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter('name', 'geefter'),

    // Third-party
    \Art4\JsonApiClient\Factory::class => \DI\autowire(\Art4\JsonApiClient\V1\Factory::class),
    \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface::class => \DI\get(\Neomerx\JsonApi\Encoder\Encoder::class),
    \Neomerx\JsonApi\Contracts\Factories\FactoryInterface::class
        => \DI\get(\OliveOil\Core\Service\Rest\JsonApi\Factories\Factory::class),
    \Neomerx\JsonApi\Contracts\Schema\SchemaContainerInterface::class
        => \DI\get(\OliveOil\Core\Service\Rest\JsonApi\Schema\SchemaContainer::class),
    \Neomerx\JsonApi\Schema\SchemaContainer::class
        => \DI\get(\OliveOil\Core\Service\Rest\JsonApi\Schema\SchemaContainer::class),

    // ########################################################################
    // DI Definitions
    // ########################################################################

    // == OliveOil
    \OliveOil\Core\Service\Rest\JsonApi\ResponseWriter::class => \DI\autowire(),
    \OliveOil\Core\Service\Session\Manager::class => \DI\create()
        ->constructor(
            \DI\get(\OliveOil\Core\Service\Session\Factory::class),
            [
                \DI\get('OliveOil\Core\Model\Session\Core'),
                \DI\get(\Geeftlist\Model\Session\GeefterInterface::class)
            ]
        ),

    // == Geeftlist
    // Controllers
    \Geeftlist\Controller\Api\Context::class => \DI\autowire()
        ->constructorParameter(
            'authenticationMethodFactory',
            \DI\get('Geeftlist\Service\Authentication\Geefter\Api\MethodFactory')
        )
        ->constructorParameter(
            'dataValidationErrorProcessor',
            \DI\autowire(\Geeftlist\Helper\Data\Validation\ErrorProcessor\JsonApi::class)
        ),
    \Geeftlist\Controller\Api\Rest\Context::class => \DI\autowire()
        ->constructorParameter(
            'authenticationMethodFactory',
            \DI\get('Geeftlist\Service\Authentication\Geefter\Api\MethodFactory')
        )
        ->constructorParameter(
            'dataValidationErrorProcessor',
            \DI\get(\Geeftlist\Helper\Data\Validation\ErrorProcessor\JsonApi::class)
        )
        ->constructorParameter(
            'restServiceFactory',
            \DI\get('Geeftlist\Service\Rest\Factory')
        ),
    \Geeftlist\Controller\Api\Rest\Badge::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Badge::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Discussion\Post::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Discussion\Post::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Discussion\Topic::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Family::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Family\MembershipRequest\CustomActions::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Family\MembershipRequest::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Family\MembershipRequest\Relationships::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Family\Relationships::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Geeftee::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Geeftee::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Geeftee\Relationships::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Geeftee::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Geefter::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Geefter::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Geefter\Relationships::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Geefter::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Gift::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Gift::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Gift\Relationships::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Gift::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\GiftList::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\GiftList::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\GiftList\Relationships::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\GiftList::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Reservation::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Reservation::ENTITY_TYPE),
    \Geeftlist\Controller\Api\Rest\Service\Account\Register::class => \DI\autowire()
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository')),
    \Geeftlist\Controller\Api\Rest\Service\Token\Jwt::class => \DI\autowire()
        ->constructorParameter(
            'allAuthenticationMethodFactory',
            \DI\get('Geeftlist\Service\Authentication\Geefter\Api\AllMethodFactory')
        ),

    // Helpers
    \Geeftlist\Helper\Data\Validation\ErrorProcessor\JsonApi::class => \DI\autowire(),

    // Services
    \Geeftlist\Service\Rest\Context::class => \DI\autowire()
        ->constructorParameter(
            'restServiceFactory',
            \DI\get('Geeftlist\Service\Rest\Factory')
        )
        ->constructorParameter(
            'dataValidationErrorProcessor',
            \DI\get(\Geeftlist\Helper\Data\Validation\ErrorProcessor\JsonApi::class)
        ),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Badge::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Badge::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Discussion\Post::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Discussion\Post::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Discussion\Topic::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Family::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Family\MembershipRequest::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geeftee::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Geeftee::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geefter::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Geefter::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Gift::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Gift::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\GiftList::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\GiftList::ENTITY_TYPE),
    \Geeftlist\Service\Rest\JsonApi\ModelHandler\Reservation::class => \DI\autowire()
        ->constructorParameter('entityType', \Geeftlist\Model\Reservation::ENTITY_TYPE),

    // == Third-party
    \Neomerx\JsonApi\Encoder\Encoder::class => \DI\autowire()
        ->constructorParameter('container', \DI\get(\OliveOil\Core\Service\Rest\JsonApi\Schema\SchemaContainer::class))
        ->methodParameter('withEncodeOptions', 'options', \DI\get(\Neomerx\JsonApi\Encoder\Encoder::class . '.options'))
        ->methodParameter('withUrlPrefix', 'prefix', \DI\get('api.base_url')),
    \OliveOil\Core\Service\Rest\JsonApi\Schema\SchemaContainer::class => \DI\autowire()
        ->constructorParameter('schemas', \DI\get(\Neomerx\JsonApi\Schema\SchemaContainer::class . '.schemas')),

    // ########################################################################
    // Virtual Classes
    // ########################################################################
    // == OliveOil
    'OliveOil\Core\Model\Session\Core' => DI\autowire(\OliveOil\Core\Model\Session\Api::class)
        ->constructorParameter('name', 'core'),

    // == Geeftlist
    'Geeftlist\Service\Rest\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('codeClassArray', \DI\get('Geeftlist\Service\Rest\Factory.codeClassArray'))
        ->constructorParameter('useResolveFromCodeFallback', false),

    'Geeftlist\Service\Authentication\Geefter\Api\AllMethodFactory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter(
            'codeClassArray',
            \DI\get('Geeftlist\Service\Authentication\Geefter\Api\AllMethodFactory.codeClassArray')
        )
        ->constructorParameter('useResolveFromCodeFallback', false),
    'Geeftlist\Service\Authentication\Geefter\Api\MethodFactory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter(
            'codeClassArray',
            \DI\get('Geeftlist\Service\Authentication\Geefter\Api\MethodFactory.codeClassArray')
        )
        ->constructorParameter('useResolveFromCodeFallback', false),
];
