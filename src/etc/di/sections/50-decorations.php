<?php

return [
    // ########################################################################
    // \DI Definitions
    // ########################################################################

    // ========================================================================
    // == OliveOil
    // ========================================================================

    \OliveOil\Core\Model\App\Config::class => \DI\decorate(
        function ($appConfig, \Psr\Container\ContainerInterface $container) {
            $fw = $container->get(\Base::class);
            // F3 "DEBUG" to "app.LOG_LEVEL" conversion
            // Due to dependency cycle issues, this is the only location where we can be sure we can define it
            // before its first access (see etc/monolog.yml for example).
            if (!$appConfig->hasValue('LOG_LEVEL') && ($debug = $fw->get('DEBUG'))) {
                $appConfig->setValue('LOG_LEVEL', \OliveOil\Core\Service\Log::debugToLogLevel($debug));
            }
            return $appConfig;
        }),
];
