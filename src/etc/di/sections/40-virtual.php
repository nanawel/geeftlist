<?php

use Geeftlist\Model\Notification;

return [
    // ########################################################################
    // Virtual Classes
    // ########################################################################
    // == OliveOil
    // Models
    'OliveOil\Core\Model\Cookie\DefaultSession' => \DI\autowire(\OliveOil\Core\Model\Cookie::class)
        ->constructorParameter('name', function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('SESSION.default');
        })
        ->methodParameter('setDomain', 'domain', \DI\get('app.cookie.domain'))
        ->methodParameter('setPath', 'path', \DI\get('app.cookie.path'))
        ->methodParameter('setSecure', 'secure', function(\Psr\Container\ContainerInterface $container) {
            return strtolower($container->get('app.base_url_parts.scheme')) === 'https';
        })
        ->methodParameter('setHttponly', 'httponly', true)
        ->methodParameter('setSamesite', 'samesite', \OliveOil\Core\Model\Cookie::SAMESITE_LAX),
    'OliveOil\Core\Model\Session\Http\Core' => \DI\autowire(\OliveOil\Core\Model\Session\Http::class)
        ->constructorParameter('name', 'core'),

    // Indexers
    'Geeftlist\Indexer\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Indexer'),

    // Services
    'OliveOil\Core\Service\Cache\ResourceModel\Connection' => \DI\autowire(\OliveOil\Core\Service\Cache::class)
        ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
        ->constructorParameter('namespace', 'resource_connection')
        ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long'))
        ->constructorParameter('enableLog', \DI\get('cache.enable_log')),
    'OliveOil\Core\Service\Cache\RuntimeConfig\Event' => \DI\autowire(\OliveOil\Core\Service\Cache::class)
        ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
        ->constructorParameter('namespace', 'runtime_config|event|default')
        ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long'))
        ->constructorParameter('enableLog', \DI\get('cache.enable_log')),
    'OliveOil\Core\Service\Cache\RuntimeConfig\Log' => \DI\autowire(\OliveOil\Core\Service\Cache::class)
        ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
        ->constructorParameter('namespace', 'runtime_config|log')
        ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long'))
        ->constructorParameter('enableLog', \DI\get('cache.enable_log')),
    'OliveOil\Core\Service\Event\Dev\Profiler' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('OliveOil\Core\Service\Event\Dev\Profiler\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'oliveoil_config|event|profiler')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long'))
            ->constructorParameter('enableLog', \DI\get('cache.enable_log')))
        ->constructorParameter('eventSpace', 'profiler'),
    'OliveOil\Core\Service\Event\Dev\Profiler\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.profiler'),

    // Template engine factories
    'OliveOil\Core\Service\View\Template\Engine\EmailFactory'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\Engine\Factory::class)
            ->constructorParameter(
                'codeClassArray',
                \DI\get(\OliveOil\Core\Service\View\Template\Engine\Factory::class . '.codeClassArray')
            )
            ->methodParameter(
                'setTemplateResolver',
                'templateResolver',
                \DI\get('OliveOil\Core\Service\View\Template\PathResolver\EmailPathResolver')
            ),
    'OliveOil\Core\Service\View\Template\Engine\FormElementFactory'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\Engine\Factory::class)
            ->constructorParameter(
                'codeClassArray',
                \DI\get(\OliveOil\Core\Service\View\Template\Engine\Factory::class . '.codeClassArray')
            )
            ->methodParameter(
                'setTemplateResolver',
                'templateResolver',
                \DI\get('OliveOil\Core\Service\View\Template\PathResolver\FormElementPathResolver')
            ),
    'OliveOil\Core\Service\View\Template\Engine\LocalizedFactory'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\Engine\Factory::class)
            ->constructorParameter(
                'codeClassArray',
                \DI\get(\OliveOil\Core\Service\View\Template\Engine\Factory::class . '.codeClassArray')
            )
            ->methodParameter(
                'setTemplateResolver',
                'templateResolver',
                \DI\get('OliveOil\Core\Service\View\Template\PathResolver\LocalizedPathResolver')
            ),

    // Template renderers
    'OliveOil\Core\Service\View\Template\Renderer\EmailRenderer'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\Renderer\DesignRenderer::class)
            ->constructorParameter('engineFactory', \DI\get('OliveOil\Core\Service\View\Template\Engine\EmailFactory')),
    'OliveOil\Core\Service\View\Template\Renderer\FormElementRenderer'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\Renderer\DesignRenderer::class)
            ->constructorParameter('engineFactory', \DI\get('OliveOil\Core\Service\View\Template\Engine\FormElementFactory')),
    'OliveOil\Core\Service\View\Template\Renderer\LocalizedRenderer'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\Renderer\DesignRenderer::class)
            ->constructorParameter('engineFactory', \DI\get('OliveOil\Core\Service\View\Template\Engine\LocalizedFactory')),

    // Template path resolvers
    'OliveOil\Core\Service\View\Template\PathResolver\EmailPathResolver'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\PathResolver\DesignPathResolver::class)
            ->constructorParameter('baseDir', 'email'),
    'OliveOil\Core\Service\View\Template\PathResolver\FormElementPathResolver'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\PathResolver\DesignPathResolver::class)
            ->constructorParameter('useLanguage', false)
            ->constructorParameter('baseDir', 'form/element'),
    'OliveOil\Core\Service\View\Template\PathResolver\LocalizedPathResolver'
        => \DI\autowire(\OliveOil\Core\Service\View\Template\PathResolver\DesignPathResolver::class)
            ->constructorParameter('baseDir', 'loc'),

    'OliveOil\Core\Service\View\Context\HelperFactory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('codeClassArray', \DI\get('OliveOil\Core\Service\View\Context\HelperFactory.codeClassArray')),

    // == Geeftlist
    // Observers
    'Geeftlist\Observer\Cron\MailNotification\Cleanup\Admin' => \DI\autowire(\Geeftlist\Observer\Cron\MailNotification\Cleanup::class)
        ->constructorParameter('notificationRecipientType', Notification::RECIPIENT_TYPE_ADMIN),
    'Geeftlist\Observer\Cron\MailNotification\Cleanup\Geefter' => \DI\autowire(\Geeftlist\Observer\Cron\MailNotification\Cleanup::class)
        ->constructorParameter('notificationRecipientType', Notification::RECIPIENT_TYPE_GEEFTER),

    // Repositories
    'Geeftlist\Model\Activity\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Activity::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\ActivityLog\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\ActivityLog::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Badge\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Badge::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Cms\Block\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Cms\Block::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Discussion\Post\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Discussion\Post::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Discussion\Topic\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Family\MembershipRequest\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Family\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Family::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Geeftee\ClaimRequest\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Geeftee\ClaimRequest::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Geeftee\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Geeftee::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Geefter\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Geefter::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Gift\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Gift::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\GiftList\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\GiftList::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Invitation\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Invitation::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Notification\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Notification::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Reservation\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Reservation::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Share\Entity\Rule\Repository' => \DI\autowire(\OliveOil\Core\Model\Repository::class)
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class))
        ->constructorParameter('entityClass', \Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
        ->constructorParameter('entityClassType', \OliveOil\Core\Model\Repository::ENTITY_CLASS_TYPE_RCN),
    'Geeftlist\Model\Share\RuleTypeFactory\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter(
            'codeClassArray',
            \DI\get('Geeftlist\Model\Share\RuleTypeFactory\Factory.codeClassArray')
        ),
    'Geeftlist\Model\Share\Discussion\Post\RuleType\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Model\Share\Discussion\Post\RuleType')
        ->constructorParameter('classSuffix', 'Type'),
    'Geeftlist\Model\Share\Discussion\Topic\RuleType\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Model\Share\Discussion\Topic\RuleType')
        ->constructorParameter('classSuffix', 'Type'),
    'Geeftlist\Model\Share\Gift\RuleType\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Model\Share\Gift\RuleType')
        ->constructorParameter('classSuffix', 'Type'),

    // Services
    'Geeftlist\Service\Authentication\Geefter\MethodFactory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Service\Authentication\Geefter')
        ->constructorParameter('classSuffix', 'Method'),
    'Geeftlist\Service\Avatar\Generator\Geeftee' => \DI\autowire(\Geeftlist\Service\Avatar\Generator::class)
        ->constructorParameter('initialAvatar', function(\Psr\Container\ContainerInterface $container) {
            /* @var $initialAvatar \LasseRafn\InitialAvatarGenerator\InitialAvatar */
            $initialAvatar = $container->make(\LasseRafn\InitialAvatarGenerator\InitialAvatar::class);
            $initialAvatar
                ->size(96)
                ->background('#fe5655')
                ->color('#eeeeee')
                ->font($container->get('Geeftlist\Service\Avatar\Generator\Geeftee.fontFile'));
            return $initialAvatar;
        }),

    'Geeftlist\Service\Event\Activity' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\Activity\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|activity')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'activity'),
    'Geeftlist\Service\Event\Activity\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.activity'),

    'Geeftlist\Service\Event\ActivityLog' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\ActivityLog\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|activitylog')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'activitylog'),
    'Geeftlist\Service\Event\ActivityLog\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.activitylog'),

    'Geeftlist\Service\Event\AdminNotification' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\AdminNotification\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|admin_notification')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'adminnotification'),
    'Geeftlist\Service\Event\AdminNotification\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.adminnotification'),

    'Geeftlist\Service\Event\Cache' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\Cache\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|cache')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'cache'),
    'Geeftlist\Service\Event\Cache\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.cache'),

    'Geeftlist\Service\Event\Cron' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\Cron\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|cron')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'cron'),
    'Geeftlist\Service\Event\Cron\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.cron'),

    'Geeftlist\Service\Event\GeefterAccess' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\GeefterAccess\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|geefter_access')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'geefteraccess'),
    'Geeftlist\Service\Event\GeefterAccess\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.geefteraccess'),

    'Geeftlist\Service\Event\GeefterNotification' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\GeefterNotification\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|geefter_notification')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'geefternotification'),
    'Geeftlist\Service\Event\GeefterNotification\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.geefternotification'),

    'Geeftlist\Service\Event\Index' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\Index\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|index')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'index'),
    'Geeftlist\Service\Event\Index\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.index'),

    'Geeftlist\Service\Event\MailNotification' => \DI\autowire(\OliveOil\Core\Service\Event::class)
        ->constructorParameter('observerProvider', \DI\get('Geeftlist\Service\Event\MailNotification\Provider'))
        ->constructorParameter('cache', \DI\autowire(\OliveOil\Core\Service\Cache::class)
            ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
            ->constructorParameter('namespace', 'runtime_config|event|mail_notification')
            ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')))
        ->constructorParameter('eventSpace', 'mailnotification'),
    'Geeftlist\Service\Event\MailNotification\Provider' => \DI\autowire(\OliveOil\Core\Service\Event\Provider\Hive::class)
        ->constructorParameter('hiveKey', 'events.mailnotification'),

    'Geeftlist\Service\MailNotification\Item\Factory' => \DI\autowire(\OliveOil\Core\Service\GenericFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Service\MailNotification\Item'),
    'Geeftlist\Service\Upload\Image\Geeftee' => \DI\autowire(\OliveOil\Core\Service\Upload\Image::class)
        ->constructorParameter('validator', function (\Psr\Container\ContainerInterface $container) {
            $i18n = $container->get(\OliveOil\Core\Model\I18n::class);
            $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);

            $valueValidator = new \Sirius\Validation\ValueValidator();
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\Image([
                    'allowed' => $appConfig->getValue('AVATAR_FILETYPES')
                ]))->setMessageTemplate($i18n->tr(
                    'Invalid image type. Accepted formats: {0}.',
                    implode(', ', $appConfig->getValue('AVATAR_FILETYPES'))
                ))
            );
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\ImageHeight([
                    'max' => $appConfig->getValue('AVATAR_MAX_HEIGHT')
                ]))->setMessageTemplate($i18n->tr('Invalid image height.'))
            );
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\ImageWidth([
                    'max' => $appConfig->getValue('AVATAR_MAX_WIDTH')
                ]))->setMessageTemplate($i18n->tr('Invalid image width.'))
            );
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\Size([
                    'size' => $appConfig->getValue('AVATAR_MAX_FILESIZE')
                ]))->setMessageTemplate($i18n->tr('Invalid image filesize.'))
            );

            return $valueValidator;
        }),
    'Geeftlist\Service\Upload\Image\Gift' => \DI\autowire(\OliveOil\Core\Service\Upload\Image::class)
        ->constructorParameter('validator', function (\Psr\Container\ContainerInterface $container) {
            $i18n = $container->get(\OliveOil\Core\Model\I18n::class);
            $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);

            $valueValidator = new \Sirius\Validation\ValueValidator();
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\Image([
                    'allowed' => $appConfig->getValue('GIFT_IMAGE_FILETYPES')
                ]))->setMessageTemplate($i18n->tr(
                    'Invalid image type. Accepted formats: {0}.',
                    implode(', ', $appConfig->getValue('GIFT_IMAGE_FILETYPES'))
                ))
            );
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\ImageHeight([
                    'max' => $appConfig->getValue('GIFT_IMAGE_MAX_HEIGHT')
                ]))->setMessageTemplate($i18n->tr('Invalid image height.'))
            );
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\ImageWidth([
                    'max' => $appConfig->getValue('GIFT_IMAGE_MAX_WIDTH')
                ]))->setMessageTemplate($i18n->tr('Invalid image width.'))
            );
            $valueValidator->addRule(
                (new \Sirius\Validation\Rule\Upload\Size([
                    'size' => $appConfig->getValue('GIFT_IMAGE_MAX_FILESIZE')
                ]))->setMessageTemplate($i18n->tr('Invalid image filesize.'))
            );

            return $valueValidator;
        }),

    // View
    'Geeftlist\View\Grid\Family\Geeftee\Members' => \DI\autowire(\Geeftlist\View\Grid\Geeftee::class)
        ->constructorParameter('renderer', \DI\get(\Geeftlist\View\Grid\Renderer\Family\Geeftee\Members::class)),
    'Geeftlist\View\Grid\Family\Geeftee\Transfer' => \DI\autowire(\Geeftlist\View\Grid\Geeftee::class)
        ->constructorParameter('renderer', \DI\get(\Geeftlist\View\Grid\Renderer\Family\Geeftee\Transfer::class)),
    'Geeftlist\View\Grid\Family\Geeftee\Invite' => \DI\autowire(\Geeftlist\View\Grid\Geeftee::class)
        ->constructorParameter('renderer', \DI\get(\Geeftlist\View\Grid\Renderer\Family\Geeftee\Invite::class)),

    // Third-party
    'Prometheus\Collector\InMemory' => function(\Psr\Container\ContainerInterface $container) {
        return new \Prometheus\CollectorRegistry(new \Prometheus\Storage\InMemory());
    },
    'Redis\Cache' => function(\Psr\Container\ContainerInterface $container) {
        $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);
        $redisConfig = $appConfig->getValue('RESOURCE_CONFIG.redis_cache');

        /** @var \Redis $redis */
        $redis = $container->make(\Redis::class);
        $success = $redis->connect(
            $host = ($redisConfig['host'] ?? '127.0.0.1'),
            $port = ($redisConfig['port'] ?? 6379),
            $redisConfig['timeout'] ?? 0.0,
            $redisConfig['reserved'] ?? null,
            $redisConfig['retry_interval'] ?? 0,
            $redisConfig['read_timeout'] ?? 0.0
        );
        if (!$success) {
            throw new \OliveOil\Core\Exception\SystemException("Could not connect to Redis on $host:$port");
        }
        $db = $appConfig->getValue('RESOURCE_CONFIG.redis_cache.database', 0);
        if (!$redis->select($db)) {
            throw new \OliveOil\Core\Exception\SystemException('Could not select Redis database #' . $db);
        }

        $redisOptions = $appConfig->getValue('RESOURCE_CONFIG.redis_cache.options', []);
        foreach ($redisOptions as $opt => $value) {
            $redis->setOption($opt, $value);
        }

        return $redis;
    }
];
