<?php

return [
    // ########################################################################
    // Values
    // ########################################################################
    // == OliveOil
    \OliveOil\Core\Helper\Data\Factory::class . '.codeClassArray' => [
        'application/json'         => \OliveOil\Core\Helper\Data\Json::class,
        'application/vnd.api+json' => \OliveOil\Core\Helper\Data\Json::class,
        'text/json'                => \OliveOil\Core\Helper\Data\Json::class,
        'text/plain'               => \OliveOil\Core\Helper\Data\Text::class,
    ],
    \OliveOil\Core\Model\Event\Manager::class . '.enableTriggerEvents' => function(\Psr\Container\ContainerInterface $container) {
        return !!$container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('DEV.EVENT_ENABLE_TRIGGER_EVENTS');
    },
    \OliveOil\Core\Model\I18n::class . '.prefix' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get(\Base::class)->get('PREFIX');
    },
    \OliveOil\Core\Model\I18n::class . '.defaultCurrencyCode' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('CURRENCY');
    },
    \OliveOil\Core\Model\ResourceModel\Db\Connection::class . '.enableQueryLog'
        => function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\Base::class)->get('DEBUG') > 2;
        },
    \OliveOil\Core\Model\View\Layout::class . '.params' => [],
    \OliveOil\Core\Service\Cache::class . '.pool' => \DI\get('cache.pool_class'),
    \OliveOil\Core\Service\Design::class . '.assetDistBasePath' => \DI\get('app.dirs.assets.dist_path'),
    \OliveOil\Core\Service\Design::class . '.assetSrcBasePath' => \DI\get('app.dirs.assets.src_path'),
    \OliveOil\Core\Service\Design::class . '.assetUrlBasePath' => \DI\get('app.dirs.assets.url_base_path'),
    \OliveOil\Core\Service\Event::class . '.enableEventDebugListener' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get(\Base::class)->get('DEBUG') > 2;
    },
    \OliveOil\Core\Service\Lock::class . '.mutexFabric' => function(\Psr\Container\ContainerInterface $container) {
        $implementor = $container->make(\NinjaMutex\Lock\FlockLock::class, [
            'dirname' => $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('LOCK_DIR')
        ]);

        return $container->make(\NinjaMutex\MutexFabric::class, [
            'lockImplementorName' => 'file',
            'lockImplementor' => $implementor
        ]);
    },
    \OliveOil\Core\Service\Log\Resolver::class . '.configFiles'         => ['etc/loggers.yml'],
    \OliveOil\Core\Service\Log\Resolver::class . '.monologConfigFiles'  => ['etc/monolog.yml'],
    \OliveOil\Core\Service\Route::class . '.params' => [],
    \OliveOil\Core\Service\Security\Captcha::class . '.builderDefaultOptions' => [
        'width' => 150,
        'height' => 40
    ],
    \OliveOil\Core\Service\Security\Captcha::class . '.options' => [
        'use_inline_image' => true
    ],
    \OliveOil\Core\Service\View\Template\Engine\Factory::class . '.codeClassArray' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('VIEW.TEMPLATE.ENGINE');
    },
    \OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider::class . '.tagHandlers' => [
        'ooimg' => \DI\get(\OliveOil\Core\Service\View\Template\Engine\TagHandler\Image::class),
        'ooblock' => \DI\get(\OliveOil\Core\Service\View\Template\Engine\TagHandler\Block::class),
        'oocmsblock' => \DI\get(\OliveOil\Core\Service\View\Template\Engine\TagHandler\CmsBlock::class),
    ],

    // == Geeftlist
    \Geeftlist\Model\Context::class . '.fieldModelFactory' => \DI\get(\Geeftlist\Model\ModelFactory::class),
    \Geeftlist\Form\Element\Geeftee\Avatar::class . '.data' => function(\Psr\Container\ContainerInterface $container) {
        $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);

        return [
            'allowed_extensions' => $appConfig->getValue('AVATAR_FILETYPES'),
            'max_height' => $appConfig->getValue('AVATAR_MAX_HEIGHT'),
            'max_width' => $appConfig->getValue('AVATAR_MAX_WIDTH'),
            'max_filesize' => $appConfig->getValue('AVATAR_MAX_FILESIZE'),
        ];
    },
    \Geeftlist\Form\Element\Gift\Image::class . '.data' => function(\Psr\Container\ContainerInterface $container) {
        $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);

        return [
            'allowed_extensions' => $appConfig->getValue('GIFT_IMAGE_FILETYPES'),
            'max_height' => $appConfig->getValue('GIFT_IMAGE_MAX_HEIGHT'),
            'max_width' => $appConfig->getValue('GIFT_IMAGE_MAX_WIDTH'),
            'max_filesize' => $appConfig->getValue('GIFT_IMAGE_MAX_FILESIZE'),
        ];
    },
    \Geeftlist\Model\Geefter::class . '.tokenLifetime' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('GEEFTER_TOKEN_LIFETIME');
    },
    \Geeftlist\Model\ResourceModel\Db\DefaultConnection::class . '.adapter' => function(\Psr\Container\ContainerInterface $container) {
        return $container->make(
            \Laminas\Db\Adapter\Adapter::class,
            ['driver' => $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('RESOURCE_CONFIG.db')]
        );
    },
    \Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer::class . '.staticEntityShareRules' => function(
        \Psr\Container\ContainerInterface $container
    ) {
        $return = [];
        $factory = $container->get(\Geeftlist\Model\ModelFactory::class);

        // Rule: "Gifts discussion topics permissions are inferred from gift's"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Discussion\Topic\RuleType\System\GiftType::CODE,
                'priority'    => 500
            ]);
        // Rule: "The topic's author has manage permissions on it"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Discussion\Topic\RuleType\System\AuthorType::CODE,
                'priority'    => 1000
            ]);
        // Rule: "Topics for deleted gifts are not visible to anyone"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Discussion\Topic\RuleType\System\DeletedGiftType::CODE,
                'priority'    => 9000
            ]);

        return $return;
    },
    \Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer::class . '.staticEntityShareRules' => function(
        \Psr\Container\ContainerInterface $container
    ) {
        $return = [];
        $factory = $container->get(\Geeftlist\Model\ModelFactory::class);

        // Rule: "A geeftee must no see a gift for him"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Gift\RuleType\System\GiftGeefteesType::CODE,
                'priority'    => 500 // Caution: see also \Geeftlist\Model\Share\Gift\RuleType\OpenGiftType
            ]);
        // Rule: "A reserver can always view its gift"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Gift\RuleType\System\ReserverType::CODE,
                'priority'    => 1000
            ]);
        // Rule: "An archived gift is only visible to its owner"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Gift\RuleType\System\ArchivedGiftsType::CODE,
                'priority'    => 2000
            ]);
        // Rule: "The owner can view and edit its gift if geeftees are still related"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Gift\RuleType\System\CreatorType::CODE,
                'priority'    => 3000
            ]);
        // Rule: "A gift without any geeftee assigned is not reservable by anyone"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Gift\RuleType\System\GeefteelessGiftType::CODE,
                'priority'    => 4000
            ]);
        // Rule: "A deleted gift is not visible to anyone"
        $return[] = $factory->resolveMake(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE)
            ->setData([
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'rule_type'   => \Geeftlist\Model\Share\Gift\RuleType\System\DeletedGiftsType::CODE,
                'priority'    => 9999
            ]);

        return $return;
    },
    'Geeftlist\Model\Share\RuleTypeFactory\Factory.codeClassArray' => [
        \Geeftlist\Model\Gift::ENTITY_TYPE => 'Geeftlist\Model\Share\Gift\RuleType\Factory',
        \Geeftlist\Model\Discussion\Post::ENTITY_TYPE => 'Geeftlist\Model\Share\Discussion\Post\RuleType\Factory',
        \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE => 'Geeftlist\Model\Share\Discussion\Topic\RuleType\Factory'
    ],
    \Geeftlist\Observer\Cron\MailNotification\Geefter::class . '.geefterNotifiableStatus'
        => \DI\create(\OliveOil\Core\Model\Cache\ArrayObject::class),
    \Geeftlist\Observer\Cron\MailNotification\Geefter\Discussion\PostMentionNotification::class . '.enabled'
        => function(\Psr\Container\ContainerInterface $container) {
            return (bool) $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
                ->getValue('DISCUSSION_POST_NOTIFICATION_ENABLED', true);
        },
    \Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.enabled'
        => function(\Psr\Container\ContainerInterface $container) {
            return (bool) $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
                ->getValue('RESERVATION_NOTIFICATION_STALE_ENABLED', true);
        },
    \Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.staleReservationDelay'
        => function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
                ->getValue('RESERVATION_NOTIFICATION_STALE_DELAY', 1080);
        },
    \Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.stalePurchaseDelay'
        => function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
                ->getValue('RESERVATION_NOTIFICATION_STALE_PURCHASE_DELAY', 2160);
        },
    \Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.batchSize'
        => function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
                ->getValue('RESERVATION_NOTIFICATION_BATCH_SIZE', 100);
        },
    \Geeftlist\Observer\Geefter\PasswordBasedSessionValidation::class . '.geefterSessionSecretKey'
        => function(\Psr\Container\ContainerInterface $container) {
        $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);
        $key = (string) $appConfig->getValue('GEEFTER_SESSION_SECRET_KEY');
            if (!$key
                && ($key = (string) $container->get(\OliveOil\Core\Model\App\FlagInterface::class)
                    ->get('compat/session/geefter/secret_key'))
            ) {
                $container->get('app.logger')->warning(
                    'Using compatibility fallback for GEEFTER_SESSION_SECRET_KEY. Consider using app config instead.'
                );
                $appConfig->setValue('GEEFTER_SESSION_SECRET_KEY', $key);
            }

            return $key;
        },
    \Geeftlist\Observer\Indexer::class . '.dependentEntityResolvers' => [
        \DI\get(\Geeftlist\Indexer\Discussion\Topic\GeefterAccess\DependentEntitiesResolver::class)
    ],

    \Geeftlist\Service\App\ErrorHandler::class . '.customErrorHandlers' => [],
    \Geeftlist\Service\Avatar\Geeftee::class . '.avatarBasePath' => \DI\string('{app.dirs.userdata.path}geeftee/avatar'),
    \Geeftlist\Service\Avatar\Geeftee::class . '.avatarUrlBasePath' => \DI\string('{app.dirs.userdata.url_base_path}geeftee/avatar'),
    \Geeftlist\Service\Avatar\Geeftee::class . '.placeholderUrl' => function(\Psr\Container\ContainerInterface $container) {
        $design = $container->get(\OliveOil\Core\Service\DesignInterface::class);

        return $design->getImageUrl('geeftee/avatar-placeholder.png');
    },
    'Geeftlist\Service\Avatar\Generator\Geeftee.fontFile' => \DI\string('{app.dirs.ui.path}font/Amaranth-Regular.ttf'),
    \Geeftlist\Service\Gift\Image::class . '.imageBasePath' => \DI\string('{app.dirs.userdata.path}gift/image'),
    \Geeftlist\Service\Gift\Image::class . '.imageUrlBasePath' => \DI\string('{app.dirs.userdata.url_base_path}gift/image'),
    \Geeftlist\Service\Gift\Image::class . '.placeholderUrl' => null,
    \Geeftlist\Service\Gift\Image::class . '.imageTypesConfig' => \DI\value([
        \Geeftlist\Service\Gift\Image::IMAGE_TYPE_TMP => [
            'width' => 800,
            'height' => 600,
            'format' => 'jpg',
            'quality' => '80',
            'filename_callback' => function($imageId, $type) {
                return "{$imageId}_{$type}.jpg";
            }
        ],
        \Geeftlist\Service\Gift\Image::IMAGE_TYPE_FULL => [
            'width' => 800,
            'height' => 600,
            'format' => 'jpg',
            'quality' => '80',
            'filename_callback' => function($imageId, $type) {
                return "{$imageId}.jpg";
            }
        ],
        \Geeftlist\Service\Gift\Image::IMAGE_TYPE_THUMBNAIL => [
            'width' => 160,
            'height' => 120,
            'format' => 'jpg',
            'quality' => '80',
            'filename_callback' => function($imageId, $type) {
                return "{$imageId}_{$type}.jpg";
            }
        ],
    ]),
    \Geeftlist\Service\Permission\Geefter::class . '.entityDelegators' => function(\Psr\Container\ContainerInterface $container) {
        return [
            \Geeftlist\Model\Discussion\Post::ENTITY_TYPE
                => $container->get(\Geeftlist\Model\Discussion\Post\GeefterAccess::class),
            \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE
                => $container->get(\Geeftlist\Model\Discussion\Topic\GeefterAccess::class),
            \Geeftlist\Model\Family::ENTITY_TYPE
                => $container->get(\Geeftlist\Model\Family\GeefterAccess::class),
            \Geeftlist\Model\Geeftee::ENTITY_TYPE
                => $container->get(\Geeftlist\Model\Geeftee\GeefterAccess::class),
            \Geeftlist\Model\Gift::ENTITY_TYPE
                => $container->get(\Geeftlist\Model\Gift\GeefterAccess::class),
            \Geeftlist\Model\GiftList::ENTITY_TYPE
                => $container->get(\Geeftlist\Model\GiftList\GeefterAccess::class),
        ];
    },

    // View
    \Geeftlist\Block\Widget\RecentActivity\Action\ActionRendererFactory::class . '.codeClassArray' => \DI\value([
        'geefter-create-gift'    => \Geeftlist\Block\Widget\RecentActivity\Action\Gift::class,
        'geefter-update-gift'    => \Geeftlist\Block\Widget\RecentActivity\Action\Gift::class,
        'geefter-delete-gift'    => \Geeftlist\Block\Widget\RecentActivity\Action\Gift::class,
        //'geefter-update-geeftee' => \Geeftlist\Block\Widget\RecentActivity\Action\Geeftee::class, // Does not exist yet
        'geeftee-join-family'    => \Geeftlist\Block\Widget\RecentActivity\Action\Family::class,
        'geeftee-leave-family'   => \Geeftlist\Block\Widget\RecentActivity\Action\Family::class,
    ]),

    // == Third-party
    \Cache\Adapter\Redis\RedisCachePool::class . '.cache' => \DI\get('Redis\Cache'),
    \Symfony\Component\Mailer\Mailer::class . '.transport' => function(\Psr\Container\ContainerInterface $container) {
        return \Symfony\Component\Mailer\Transport::fromDsn($container->get('mailer.dsn'));
    },

    // == Emails
    'email.default_data' => function(\Psr\Container\ContainerInterface $container) {
        return [
            'base_url' => $container->get('app.base_url'),
            'abuse_email' => $container->get(\OliveOil\Core\Model\App\Config::class)->getValue('EMAIL_ABUSE')
        ];
    },

    // == Event Services
    'event_services.all' => [
        // Note: The array key here is NOT the event space, just a unique key to identify each instance
        // and to be able to override each one if necessary. For event space, see 40-virtual.php
        'default' => \DI\get(\OliveOil\Core\Service\Event::class),
        'activity' => \DI\get('Geeftlist\Service\Event\Activity'),
        'activitylog' => \DI\get('Geeftlist\Service\Event\ActivityLog'),
        'adminotification' => \DI\get('Geeftlist\Service\Event\AdminNotification'),
        'cron' => \DI\get('Geeftlist\Service\Event\Cron'),
        'geefteraccess' => \DI\get('Geeftlist\Service\Event\GeefterAccess'),
        'geefternotification' => \DI\get('Geeftlist\Service\Event\GeefterNotification'),
        'index' => \DI\get('Geeftlist\Service\Event\Index'),
        'cache' => \DI\get('Geeftlist\Service\Event\Cache'),
        'mailnotification' => \DI\get('Geeftlist\Service\Event\MailNotification'),
        'profiler' => \DI\get('OliveOil\Core\Service\Event\Dev\Profiler'),
    ],

    // == Indexers
    'indexers.all' => [
        \Geeftlist\Indexer\Gift\GeefterAccess::CODE
            => \DI\get(\Geeftlist\Indexer\Gift\GeefterAccess::class),
        \Geeftlist\Indexer\Discussion\Topic\GeefterAccess::CODE
            => \DI\get(\Geeftlist\Indexer\Discussion\Topic\GeefterAccess::class),
    ],

    // == Models
    'contact.predefined_subjects' => \DI\value([
        'question' => "I have a question",
        'gift_problem' => "I have a problem with a gift",
        'family_problem' => "I have a problem with a family",
        'reservation_problem' => "I have a problem with a reservation",
        'bug_found' => "I have found a bug",
        'suggestion' => "I have a suggestion",
        'other' => 'Other'
    ]),
    'geeftee.name_validation_regex' => '/^[\p{L}][\p{L}\d\-_@+ .]{2,63}$/iu',
    'geefter.username_validation_regex' => \DI\get('geeftee.name_validation_regex'),

    // == Share Rules
    'share_entity_rules.default' => [
        \Geeftlist\Model\Gift::ENTITY_TYPE => [[
            'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\RegularType::CODE,
            'params'    => []
        ]]
    ],

    // == Misc

    // URL: API
    'api.area_url_path' => function(\Psr\Container\ContainerInterface $container) {
        $fw = $container->get(\Base::class);

        return $fw->get('SERVER.GEEFTLIST_API_PREFIX')
            ?: $fw->get('SERVER.REDIRECT_GEEFTLIST_API_PREFIX')
            ?: '/api';
    },
    'api.base_url' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get('app.base_url') . trim($container->get('api.area_url_path'), '/');
    },

    // URL: frontend
    'app.area_url_path' => function(\Psr\Container\ContainerInterface $container) {
        $fw = $container->get(\Base::class);

        return $fw->get('SERVER.URL_PATH_PREFIX')
            ?: $fw->get('SERVER.REDIRECT_URL_PATH_PREFIX')
            ?: '';
    },
    'app.base_url_object' => function(\Psr\Container\ContainerInterface $container) {
        return \League\Uri\Uri::createFromString($container->get('app.base_url'));
    },
    'app.base_url' => function(\Psr\Container\ContainerInterface $container) {
        $baseUrl = $container->get(\OliveOil\Core\Model\App\Config::class)->getValue('BASE_URL');
        if (! $baseUrl) {
            throw new \OliveOil\Core\Exception\MissingConfigurationException('BASE_URL');
        }

        return $baseUrl;
    },
    'app.base_url_parts' => function(\Psr\Container\ContainerInterface $container) {
        // Add "/" as default value for path if none found (https://codeberg.org/nanawel/geeftlist/issues/9)
        return parse_url($container->get('app.base_url')) + ['path' => '/'];
    },
    'app.base_url_parts.scheme' => DI\factory(function ($parts) {
        return $parts['scheme'];
    })->parameter('parts', DI\get('app.base_url_parts')),
    'app.base_url_parts.host' => DI\factory(function ($parts) {
        return $parts['host'];
    })->parameter('parts', DI\get('app.base_url_parts')),
    'app.base_url_parts.path' => DI\factory(function ($parts) {
        return $parts['path'];
    })->parameter('parts', DI\get('app.base_url_parts')),

    'app.cookie.domain' => function(\Psr\Container\ContainerInterface $container) {
        $cookieDomain = trim($container->get(\OliveOil\Core\Model\App\Config::class)->getValue('COOKIE.domain') ?? '');
        switch ($cookieDomain) {
            case '':
                return $container->get('app.base_url_parts.host');

            case '~':
            case '-':
                return '';

            default:
                return $cookieDomain;
        }
    },
    'app.cookie.path' => function(\Psr\Container\ContainerInterface $container) {
        $cookiePath = trim($container->get(\OliveOil\Core\Model\App\Config::class)->getValue('COOKIE.path') ?? '');
        switch ($cookiePath) {
            case '':
                return $container->get('app.base_url_parts.path');

            case '~':
            case '-':
                return '';

            default:
                return $cookiePath;
        }
    },

    // Application directories (source only)
    'app.dirs.app.path'      => 'app/',
    'app.dirs.code.path'     => \DI\string('{app.dirs.app.path}code/'),
    'app.dirs.template.path' => \DI\string('{app.dirs.app.path}template/'),
    'app.dirs.pub.path'      => 'pub/',
    'app.dirs.ui.path'       => \DI\string('{app.dirs.app.path}ui/'),

    // Assets directories (source + dist)
    'app.dirs.assets.url_base_path' => 'asset/',
    'app.dirs.assets.dist_path'     => \DI\string('{app.dirs.pub.path}asset/'),
    'app.dirs.assets.src_path'      => \DI\get('app.dirs.ui.path'),

    // User data directories
    'app.dirs.userdata.url_base_path' => 'userdata/',
    'app.dirs.userdata.path'          => \DI\string('{app.dirs.pub.path}userdata/'),

    'app.config.f3KeyNamespace' => 'app',
    'api.config.f3KeyNamespace' => 'api',

    'app.logger' => function (\Psr\Container\ContainerInterface $container) {
        return $container->get(\OliveOil\Core\Service\Log::class)->getLoggerByName('app');
    },

    // Cache
    'cache.pool_class' => function (\Psr\Container\ContainerInterface $container) {
        $adapterClass = $container->get(\OliveOil\Core\Model\App\Config::class)
            ->getValue('RESOURCE_CACHE.pool', \Cache\Adapter\Void\VoidCachePool::class);

        return $container->get(trim($adapterClass, '\\'));
    },
    'cache.enable_log' => false,    // For debugging purposes only - NOT FOR PRODUCTION
    'cache.ttl.minute' => 60,
    'cache.ttl.hour' => 60*60,
    'cache.ttl.day' => 24*60*60,
    'cache.ttl.week' => 7*24*60*60,

    'cache.ttl.short' => 5*60,
    'cache.ttl.medium' => \DI\get('cache.ttl.hour'),
    'cache.ttl.long' => \DI\get('cache.ttl.day'),
    'cache.ttl.x-long' => \DI\get('cache.ttl.week'),

    // Errors
    'errors.silent_message' => 'Woops! Something wrong happened and we could not do what you asked. You should try again.',
    'errors.silent_detail' => 'Sorry, details are not available.',
    'errors.silent_trace' => '[trace not available]',

    // JWT (API)
    'jwt.geefter.config' => function (\Psr\Container\ContainerInterface $container) {
        $config = \Lcobucci\JWT\Configuration::forSymmetricSigner(
            $container->get('jwt.geefter.signer'),
            $container->get('jwt.geefter.secret_key')
        );
        $config->setValidationConstraints(...$container->get('jwt.geefter.validation_constraints'));

        return $config;
    },
    'jwt.geefter.builder' => function (\Psr\Container\ContainerInterface $container) {
        /* @var $config \Lcobucci\JWT\Configuration */
        $config = $container->get('jwt.geefter.config');
        $builder = $config->builder()
            ->issuedBy($container->get('jwt.geefter.token.issued_by'))
            ->permittedFor($container->get('jwt.geefter.token.permitted_for'))
            ->issuedAt($container->get('jwt.geefter.token.issued_at'))
            ->canOnlyBeUsedAfter($container->get('jwt.geefter.token.not_before'))
            ->expiresAt($container->get('jwt.geefter.token.expires'));

        return $builder;
    },
    'jwt.geefter.validation_constraints' => [
        \DI\create(Lcobucci\JWT\Validation\Constraint\IssuedBy::class)
            ->constructor(\DI\get('jwt.geefter.token.issued_by')),
        \DI\create(Lcobucci\JWT\Validation\Constraint\PermittedFor::class)
            ->constructor(\DI\get('jwt.geefter.token.permitted_for')),
        \DI\create(Lcobucci\JWT\Validation\Constraint\ValidAt::class)
            ->constructor(\DI\get('jwt.geefter.clock'), \DI\get('jwt.geefter.leeway')),
        \DI\get(\Geeftlist\Service\Authentication\Geefter\JwtMethod\Constraint\IdentifiedBy::class)
    ],
    'jwt.geefter.signer' => \DI\autowire(\Lcobucci\JWT\Signer\Hmac\Sha384::class),
    'jwt.geefter.secret_key' => function (\Psr\Container\ContainerInterface $container) {
        $appConfig = $container->get(\OliveOil\Core\Model\App\ConfigInterface::class);
        $key = (string) $appConfig->getValue('AUTHENTICATION_JWT_SECRET_KEY');
        if (!$key
            && ($key = (string) $container->get(\OliveOil\Core\Model\App\FlagInterface::class)
                ->get('compat/api/authentication/jwt/secret_key'))
        ) {
            $container->get('app.logger')->warning(
                'Using compatibility fallback for AUTHENTICATION_JWT_SECRET_KEY. Consider using app config instead.'
            );
            $appConfig->setValue('AUTHENTICATION_JWT_SECRET_KEY', $key);
        }

        return \Lcobucci\JWT\Signer\Key\InMemory::plainText($key);
    },
    'jwt.geefter.expiration_time' => function (\Psr\Container\ContainerInterface $container) {
        return (int) $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
            ->getValue('AUTHENTICATION_JWT_EXPIRATION_TIME', 3600);
    },
    'jwt.geefter.clock' => \DI\factory([\Lcobucci\Clock\FrozenClock::class, 'fromUTC']),
    'jwt.geefter.leeway' => \DI\create(DateInterval::class)
        ->constructor('PT60S'),
    // We use *app* base URL here, not API base URL
    'jwt.geefter.token.issued_by' => \DI\get('app.base_url'),
    'jwt.geefter.token.permitted_for' => \DI\get('app.base_url'),
    'jwt.geefter.token.issued_at' => \DI\factory(['jwt.geefter.clock', 'now']),
    'jwt.geefter.token.not_before' => \DI\factory(['jwt.geefter.clock', 'now']),
    'jwt.geefter.token.expires' => function (\Psr\Container\ContainerInterface $container) {
        /** @var \Lcobucci\Clock\Clock $clock */
        $clock = $container->get('jwt.geefter.clock');

        return \DateTimeImmutable::createFromMutable(
            \DateTime::createFromImmutable($clock->now())
                ->add(new \DateInterval(sprintf('PT%dS', $container->get('jwt.geefter.expiration_time'))))
        );
    },

    // Mail
    'mailer.dsn' => function(\Psr\Container\ContainerInterface $container) {
        return $container->get(\OliveOil\Core\Model\App\Config::class)
            ->getValue('EMAIL_MAILER_DSN', 'smtp://localhost:25');
    },
    'mailcatcher.host' => 'mailcatcher',
    'mailcatcher.smtp_port' => 1025,
    'mailcatcher.webui_port' => 1080,
];
