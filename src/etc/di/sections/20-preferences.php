<?php
return [
    // ########################################################################
    // Interface/class Preferences
    //
    // WARNING! Use get() to force using class as a singleton and autowire() to inject new instances.
    //          (you may use get() if the specified class is individually defined in a later file)
    // ########################################################################
    // == OliveOil
    \OliveOil\Core\BootstrapInterface::class => \DI\get(\OliveOil\Core\Bootstrap::class),

    \OliveOil\Core\Model\Api\ConfigInterface::class => \DI\get(\OliveOil\Core\Model\Api\Config::class),
    \OliveOil\Core\Model\AppInterface::class => \DI\get(\Geeftlist\Model\App\Http::class),
    \OliveOil\Core\Model\App\ConfigInterface::class => \DI\get(\OliveOil\Core\Model\App\Config::class),
    \OliveOil\Core\Model\Cache\ArrayObjectInterface::class => \DI\create(\OliveOil\Core\Model\Cache\ArrayObject::class),
    \OliveOil\Core\Model\Http\RequestInterface::class => \DI\autowire(\OliveOil\Core\Model\Http\Request::class),
    \OliveOil\Core\Model\Http\ResponseInterface::class => \DI\autowire(\OliveOil\Core\Model\Http\Response::class),
    \OliveOil\Core\Model\ResourceModel\Db\Connection::class => \DI\get(\Geeftlist\Model\ResourceModel\Db\DefaultConnection::class),
    \OliveOil\Core\Model\FactoryInterface::class => \DI\get(\Geeftlist\Model\ResourceFactory::class),
    \OliveOil\Core\Model\ResourceModel\Iface\Setup::class => \DI\get(\Geeftlist\Model\ResourceModel\Db\Setup::class),
    \OliveOil\Core\Model\ResourceModel\Iface\Transaction::class => \DI\get(\OliveOil\Core\Model\ResourceModel\Db\Transaction::class),
    \OliveOil\Core\Model\Session\Message\ManagerInterface::class
        => \DI\autowire(\OliveOil\Core\Model\Session\Message\Manager::class),
    \OliveOil\Core\Model\Session\Message\StorageInterface::class
        => \DI\autowire(\OliveOil\Core\Model\Session\Message\Storage\DefaultStorage::class),
    \OliveOil\Core\Model\Session\Message\MessageInterface::class => \DI\autowire(\OliveOil\Core\Model\Session\Message\Message::class),
    \OliveOil\Core\Model\SetupInterface::class => \DI\get(\Geeftlist\Model\Setup::class),
    \OliveOil\Core\Model\Validation\ErrorAggregatorInterface::class
        => \DI\autowire(\OliveOil\Core\Model\Validation\ErrorAggregator::class),
    \OliveOil\Core\Model\Validation\ErrorInterface::class => \DI\autowire(\OliveOil\Core\Model\Validation\Error::class),
    \OliveOil\Core\Model\View\LayoutInterface::class => \DI\autowire(\OliveOil\Core\Model\View\Layout::class),

    \OliveOil\Core\Service\App\ErrorHandlerInterface::class => \DI\autowire(\Geeftlist\Service\App\ErrorHandler::class)
        ->constructorParameter('customErrorHandlers', \DI\get(\Geeftlist\Service\App\ErrorHandler::class . '.customErrorHandlers')),
    \OliveOil\Core\Service\Design\Theme\BootstrapInterface::class
        => \DI\get(\OliveOil\Core\Service\Design\Theme\Bootstrap::class),
    \OliveOil\Core\Service\Email\Sender\SenderInterface::class => \DI\get(\OliveOil\Core\Service\Email\Sender\SymfonyMailerSender::class),
    \OliveOil\Core\Service\DesignInterface::class => \DI\get(\OliveOil\Core\Service\Design::class),
    \OliveOil\Core\Service\EventInterface::class => \DI\get(\OliveOil\Core\Service\Event::class),
    \OliveOil\Core\Service\GenericFactoryInterface::class => \DI\get(\OliveOil\Core\Service\GenericFactory::class),
    \OliveOil\Core\Service\Http\RequestManagerInterface::class => \DI\get(\OliveOil\Core\Service\Http\RequestManager::class),
    \OliveOil\Core\Service\Http\ResponseWriterInterface::class => \DI\get(\OliveOil\Core\Service\Http\ResponseWriter::class),
    \OliveOil\Core\Service\Log\ResolverInterface::class => \DI\get(\OliveOil\Core\Service\Log\Resolver::class),
    \OliveOil\Core\Service\RegistryInterface::class => \DI\get(\OliveOil\Core\Service\Registry::class),
    \OliveOil\Core\Service\Rest\JsonApi\Encoder\EncoderInterface::class
        => \DI\get(\OliveOil\Core\Service\Rest\JsonApi\Encoder\Encoder::class),
    \OliveOil\Core\Service\Security\Captcha::class => \DI\autowire()
        ->constructorParameter('builderDefaultOptions', \DI\get(\OliveOil\Core\Service\Security\Captcha::class . '.builderDefaultOptions'))
        ->constructorParameter('options', \DI\get(\OliveOil\Core\Service\Security\Captcha::class . '.options')),
    \OliveOil\Core\Service\Url\BuilderInterface::class => \DI\get(\OliveOil\Core\Service\Url\Builder::class),
    \OliveOil\Core\Service\Url\RedirectInterface::class => \DI\get(\OliveOil\Core\Service\Url\Redirect::class),
    \OliveOil\Core\Service\Validation\Model\ValidatorInterface::class
        => \DI\get(\OliveOil\Core\Service\Validation\Model\Validator::class),
    \OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider::class
        => \DI\get(\OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider::class),

    // == Geeftlist
    // Models
    \Geeftlist\Model\Discussion\Post\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\Discussion\Post\GeefterAccess::class),
    \Geeftlist\Model\Discussion\Topic\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\Discussion\Topic\GeefterAccess::class),
    \Geeftlist\Model\Family\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\Family\GeefterAccess::class),
    \Geeftlist\Model\Geeftee\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\Geeftee\GeefterAccess::class),
    \Geeftlist\Model\Gift\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\Gift\GeefterAccess::class),
    \Geeftlist\Model\GiftList\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\GiftList\GeefterAccess::class),
    \Geeftlist\Model\Reservation\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\Reservation\GeefterAccess::class),
    \Geeftlist\Model\MailNotification\EventInterface::class
        => \DI\autowire(\Geeftlist\Model\MailNotification\Event::class),
    \Geeftlist\Model\Session\GeefterInterface::class
        => \DI\get(\Geeftlist\Model\Session\Http\GeefterInterface::class),
    \Geeftlist\Model\Session\Http\GeefterInterface::class
        => \DI\get(\Geeftlist\Model\Session\Http\Geefter::class),

    // ResourceModels
    \Geeftlist\Model\ResourceModel\Iface\Family\Geeftee::class
        => \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\Geeftee::class),
    \Geeftlist\Model\ResourceModel\Iface\Family\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\GeefterAccess::class),
    \Geeftlist\Model\ResourceModel\Iface\Geeftee\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\GeefterAccess::class),
    \Geeftlist\Model\ResourceModel\Iface\Gift\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\GeefterAccess::class),
    \Geeftlist\Model\ResourceModel\Iface\GiftList\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\ResourceModel\Db\GiftList\GeefterAccess::class),
    \Geeftlist\Model\ResourceModel\Iface\Reservation\RestrictionsAppenderInterface::class
        => \DI\get(\Geeftlist\Model\ResourceModel\Db\Reservation\GeefterAccess::class),

    // Services
    \Geeftlist\Service\ActivityLogInterface::class => \DI\get(\Geeftlist\Service\ActivityLog::class),
    \Geeftlist\Service\Authentication\AuthenticationInterface::class
        => \DI\get(\Geeftlist\Service\Authentication\Geefter\PasswordMethod::class),
    \Geeftlist\Service\Authentication\Geefter\JwtMethod\Constraint\IdentifiedBy::class => \DI\autowire()
        ->constructorParameter('signingKey', \DI\get('jwt.geefter.secret_key')),
    \Geeftlist\Service\Avatar\GeefteeInterface::class => \DI\get(\Geeftlist\Service\Avatar\Geeftee::class),
    \Geeftlist\Service\BadgeInterface::class => \DI\get(Geeftlist\Service\Badge::class),
    \Geeftlist\Service\MailNotificationInterface::class => \DI\get(\Geeftlist\Service\MailNotification::class),
    \Geeftlist\Service\PermissionInterface::class => \DI\get(\Geeftlist\Service\Permission::class),

    // == Third-party
    \Lcobucci\JWT\Builder::class => \DI\get(\Lcobucci\JWT\Token\Builder::class),
    \Prometheus\RegistryInterface::class => \DI\get('Prometheus\Collector\InMemory'),
    \Psr\Log\LoggerInterface::class => \DI\create(\Psr\Log\NullLogger::class),
    \Symfony\Component\Mailer\MailerInterface::class => \DI\get(\Symfony\Component\Mailer\Mailer::class),
];
