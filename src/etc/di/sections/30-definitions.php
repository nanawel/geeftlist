<?php

return [
    // ########################################################################
    // \DI Definitions
    // ########################################################################

    // ========================================================================
    // == OliveOil
    // ========================================================================

    \Base::class => \DI\factory([\Base::class, 'instance']),
    \Cache::class => \DI\factory([\Cache::class, 'instance']),

    // Controllers
    \OliveOil\Core\Controller\Context::class => \DI\autowire(),
    \OliveOil\Core\Controller\Http\Context::class => \DI\autowire(),

    // Helpers
    \OliveOil\Core\Helper\Data\Factory::class => \DI\autowire()
        ->constructorParameter(
            'codeClassArray',
            \DI\get(\OliveOil\Core\Helper\Data\Factory::class . '.codeClassArray')
        ),

    // Models
    \OliveOil\Core\Model\Api\Config::class => \DI\autowire()
        ->constructorParameter('f3KeyNamespace', \DI\get('api.config.f3KeyNamespace')),
    \OliveOil\Core\Model\App\Config::class => \DI\autowire()
        ->constructorParameter('f3KeyNamespace', \DI\get('app.config.f3KeyNamespace')),
    \OliveOil\Core\Model\App\Context::class => \DI\autowire()
        ->constructorParameter('eventServices', \DI\get('event_services.all')),
    \OliveOil\Core\Model\App\Error\Builder::class => \DI\autowire()
        ->constructorParameter('silentMessage', \DI\get('errors.silent_message'))
        ->constructorParameter('silentDetail', \DI\get('errors.silent_detail'))
        ->constructorParameter('silentTrace', \DI\get('errors.silent_trace')),
    \OliveOil\Core\Model\Context::class => \DI\autowire(),
    \OliveOil\Core\Model\Email\Template::class => \DI\autowire()
        ->constructorParameter(
            'templateRenderer',
            \DI\get('OliveOil\Core\Service\View\Template\Renderer\EmailRenderer')
        )
        ->constructorParameter(
            'data',
            \DI\get('email.default_data')
        ),
    \OliveOil\Core\Model\App\FlagInterface::class => \DI\autowire(\Geeftlist\Model\App\Flag::class),
    \OliveOil\Core\Model\Event\Manager::class => \DI\autowire()
        ->constructorParameter(
            'enableTriggerEvents',
            \DI\get(\OliveOil\Core\Model\Event\Manager::class . '.enableTriggerEvents')
        ),
    \OliveOil\Core\Form\Element\TemplateElement\Context::class => \DI\autowire()
        ->constructorParameter(
            'templateRenderer',
            \DI\get('OliveOil\Core\Service\View\Template\Renderer\FormElementRenderer')
        ),
    \OliveOil\Core\Model\I18n::class => \DI\autowire()
        ->constructorParameter('prefix', \DI\get(\OliveOil\Core\Model\I18n::class . '.prefix'))
        ->method('setCurrencyCode', \DI\get(\OliveOil\Core\Model\I18n::class . '.defaultCurrencyCode')),
    \OliveOil\Core\Model\ResourceModel\Db\Setup\Context::class => \DI\autowire(),
    \OliveOil\Core\Model\ResourceModel\Db\Transaction::class => \DI\autowire(),
    \OliveOil\Core\Model\Session\Context::class => \DI\autowire(),
    \OliveOil\Core\Model\Session\Http\PhpSession::class => \DI\autowire()
        ->constructorParameter('cookie', \DI\get('OliveOil\Core\Model\Cookie\DefaultSession'))
        ->constructorParameter('storage', function(\Psr\Container\ContainerInterface $container) {
            return new \Cache(
                $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)
                    ->getValue(sprintf(
                        'SESSION.%s.dsn',
                        $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('SESSION.default')
                    ))
            );
        })
        ->constructorParameter('name', function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('SESSION.default');
        })
        ->method(
            'attachInstanceListener',
            'start::after_session_start',
            \DI\autowire(\OliveOil\Core\Model\Session\Http\PhpSession\SuspectObserver::class)
        )
        ->method(
            'attachInstanceListener',
            'start::after_session_start',
            \DI\autowire(\OliveOil\Core\Model\Session\Http\PhpSession\TypeObserver::class)
        )
        ->method(
            'attachInstanceListener',
            'start::after_session_start',
            \DI\autowire(\OliveOil\Core\Model\Session\Http\PhpSession\ExpireObserver::class)
        )->method(
            'attachInstanceListener',
            'setData::type',
            \DI\autowire(\OliveOil\Core\Model\Session\Http\PhpSession\TypeObserver::class)
        ),
    \OliveOil\Core\Model\View\Breadcrumbs::class => \DI\autowire(),
    \OliveOil\Core\Model\View\Layout::class => \DI\autowire()
        ->constructorParameter('params', \DI\get(\OliveOil\Core\Model\View\Layout::class . '.params')),

    // View Blocks
    \OliveOil\Core\Block\Context::class => \DI\autowire(),
    \OliveOil\Core\Block\Factory::class => \DI\autowire(),
    \OliveOil\Core\Block\LocalizedTemplate::class => \DI\autowire()
        ->constructorParameter(
            'templateRenderer',
            \DI\get('OliveOil\Core\Service\View\Template\Renderer\LocalizedRenderer')
        ),
    \OliveOil\Core\Block\SpecialInput\CsrfToken::class => \DI\autowire(),
    \OliveOil\Core\Block\SessionMessages::class => \DI\autowire(),

    // Services
    \OliveOil\Core\Service\Cache::class => \DI\autowire()
        ->constructorParameter('pool', \DI\get(\OliveOil\Core\Service\Cache::class . '.pool'))
        ->method('setLogger', function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Service\Log::class)
                ->getLoggerForClass(\OliveOil\Core\Service\Cache::class);
        }),
    \OliveOil\Core\Service\Design::class => \DI\autowire()
        ->constructorParameter('assetDistBasePath', \DI\get(\OliveOil\Core\Service\Design::class . '.assetDistBasePath'))
        ->constructorParameter('assetSrcBasePath', \DI\get(\OliveOil\Core\Service\Design::class . '.assetSrcBasePath'))
        ->constructorParameter('assetUrlBasePath', \DI\get(\OliveOil\Core\Service\Design::class . '.assetUrlBasePath')),
    \OliveOil\Core\Service\Di\Container::class => function(\Psr\Container\ContainerInterface $container) {
        return \OliveOil\Core\Service\Di\Container::instance();
    },
    \OliveOil\Core\Service\Email\Sender\SymfonyMailerSender::class => \DI\autowire()
        ->constructorParameter('mailer', \DI\get(\Symfony\Component\Mailer\Mailer::class)),
    \OliveOil\Core\Service\Event::class => \DI\autowire()
        ->constructorParameter('observerProvider', \DI\get(\OliveOil\Core\Service\Event\Provider\Hive::class))
        ->constructorParameter('cache', \DI\get('OliveOil\Core\Service\Cache\RuntimeConfig\Event'))
        ->constructorParameter(
            'enableEventDebugListener',
            \DI\get(\OliveOil\Core\Service\Event::class . '.enableEventDebugListener')
        ),
    \OliveOil\Core\Service\Event\Provider\Hive::class => \DI\autowire(),
    \OliveOil\Core\Service\GenericFactory::class => \DI\autowire()
        ->constructorParameter('namespaces', 'OliveOil\Core\Model'),
    \OliveOil\Core\Service\Lock::class => \DI\autowire()
        ->constructorParameter('mutexFabric', \DI\get(\OliveOil\Core\Service\Lock::class . '.mutexFabric')),
    \OliveOil\Core\Service\Log\Resolver::class => \DI\autowire()
        ->constructorParameter('cache', \DI\get('OliveOil\Core\Service\Cache\RuntimeConfig\Log'))
        ->constructorParameter('configFiles', \DI\get(\OliveOil\Core\Service\Log\Resolver::class . '.configFiles'))
        ->constructorParameter('monologConfigFiles', \DI\get(\OliveOil\Core\Service\Log\Resolver::class . '.monologConfigFiles')),
    \OliveOil\Core\Service\Response\Redirect::class => \DI\autowire(\OliveOil\Core\Service\Response\Redirect::class),
    \OliveOil\Core\Service\Rest\JsonApi\ParamsBuilder::class => \DI\autowire(),
    \OliveOil\Core\Service\RouteInterface::class => \DI\autowire(\OliveOil\Core\Service\Route::class)
        ->constructorParameter('params', \DI\get(\OliveOil\Core\Service\Route::class . '.params')),
    \OliveOil\Core\Service\Security\Csrf::class => \DI\autowire()
        ->constructorParameter('tokenName', function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\OliveOil\Core\Model\App\ConfigInterface::class)->getValue('CSRF_TOKEN');
        }),
    \OliveOil\Core\Service\Session\Factory::class => \DI\autowire()
        ->constructorParameter('sessionClass', \OliveOil\Core\Model\Session\Http::class),
    \OliveOil\Core\Service\Session\Manager::class => \DI\create()
        ->constructor(
            \DI\get(\OliveOil\Core\Service\Session\Factory::class),
            [
                \DI\get('OliveOil\Core\Model\Session\Http\Core'),
                \DI\get(\Geeftlist\Model\Session\Http\GeefterInterface::class)
            ]
        ),
    \OliveOil\Core\Service\System::class => \DI\autowire(),
    \OliveOil\Core\Service\Upload\Factory::class => \DI\autowire()
        ->constructorParameter('namespaces', 'OliveOil\Core\Service\Upload'),
    \OliveOil\Core\Service\Url\Builder::class => \DI\autowire()
        ->constructorParameter('baseUrl', \DI\get('app.base_url'))
        ->constructorParameter('areaPath', \DI\get('app.area_url_path')),
    \OliveOil\Core\Service\View::class => \DI\autowire(\Geeftlist\Service\View::class),
    \OliveOil\Core\Service\View\Context::class => \DI\autowire(\Geeftlist\Service\View\Context::class),
    \OliveOil\Core\Service\View\Template\Engine\Factory::class => \DI\autowire()
            ->constructorParameter(
                'codeClassArray',
                \DI\get(\OliveOil\Core\Service\View\Template\Engine\Factory::class . '.codeClassArray')
            ),
    \OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider::class => \DI\autowire()
        ->constructorParameter(
            'tagHandlers',
            \DI\get(\OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider::class . '.tagHandlers')
        ),

    // Utils
    \OliveOil\Core\Util\Profiler::class => \DI\autowire()
        ->constructorParameter('outputDir', function(\Psr\Container\ContainerInterface $container) {
            return $container->get(\Base::class)->get('LOGS');
        }),

    // ========================================================================
    // == Geeftlist
    // ========================================================================

    // Blocks
    \Geeftlist\Block\Widget\RecentActivity\Action\ActionRendererFactory::class => \DI\autowire()
        ->constructorParameter('namespaces', 'Geeftlist\Block\Widget\RecentActivity\Action')
        ->constructorParameter(
            'codeClassArray',
            \DI\get(\Geeftlist\Block\Widget\RecentActivity\Action\ActionRendererFactory::class . '.codeClassArray')
        ),
    \Geeftlist\Block\Widget\RecentActivity\Action\Family::class => \DI\autowire()
        ->constructorParameter('config', ['template' => 'common/widgets/recent_activity/action/family.phtml']),
    \Geeftlist\Block\Widget\RecentActivity\Action\Gift::class => \DI\autowire()
        ->constructorParameter('config', ['template' => 'common/widgets/recent_activity/action/gift.phtml']),

    // Controllers
    \Geeftlist\Controller\Account\AvatarUpload::class => \DI\autowire()
        ->constructorParameter('uploadService', \DI\get('Geeftlist\Service\Upload\Image\Geeftee')),
    \Geeftlist\Controller\Cli\Dev::class => \DI\autowire(),
    \Geeftlist\Controller\Cli\Indexer::class => \DI\autowire()
        ->constructorParameter('indexerFactory', \DI\get('Geeftlist\Indexer\Factory'))
        ->constructorParameter('indexers', \DI\get(\Geeftlist\Controller\Cli\Indexer::class . '.indexers')),
    \Geeftlist\Controller\Cli\System::class => \DI\autowire(),
    \Geeftlist\Controller\Cli\Setup::class => \DI\autowire(),
    \Geeftlist\Controller\Cron::class => \DI\autowire()
        ->constructorParameter('cronJobsEventService', \DI\get('Geeftlist\Service\Event\Cron')),
    \Geeftlist\Controller\Family\Manage\Invite::class => \DI\autowire()
        ->constructorParameter('geefteeGrid', \DI\get('Geeftlist\View\Grid\Family\Geeftee\Invite')),
    \Geeftlist\Controller\Family\Manage\Members::class => \DI\autowire()
        ->constructorParameter('geefteeGrid', \DI\get('Geeftlist\View\Grid\Family\Geeftee\Members')),
    \Geeftlist\Controller\Family\Manage\Transfer::class => \DI\autowire()
        ->constructorParameter('geefteeGrid', \DI\get('Geeftlist\View\Grid\Family\Geeftee\Transfer')),
    \Geeftlist\Controller\Gift\Grid::class => \DI\autowire()
        ->constructorParameter('giftGrid', \DI\get(\Geeftlist\View\Grid\Gift::class)),
    \Geeftlist\Controller\Gift\Upload::class => \DI\autowire()
        ->constructorParameter('uploadService', \DI\get('Geeftlist\Service\Upload\Image\Gift')),
    \Geeftlist\Controller\Http\Context::class => \DI\autowire()
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class)),
    \Geeftlist\Controller\Login::class => \DI\autowire()
        ->constructorParameter(
            'authenticationService',
            \DI\get(\Geeftlist\Service\Authentication\Geefter\PasswordMethod::class)
        ),

    // Form Elements
    \Geeftlist\Form\Element\BadgeSelect::class => \DI\autowire(),
    \Geeftlist\Form\Element\ContactSubjectSelect::class => \DI\autowire()
        ->constructorParameter('predefinedSubjects', \DI\get('contact.predefined_subjects')),
    \Geeftlist\Form\Element\Family\VisibilityRadioGroup::class => \DI\autowire()
        ->constructorParameter('fieldModel', \DI\get(\Geeftlist\Model\Family\Field\Visibility::class)),
    \Geeftlist\Form\Element\Geeftee\Avatar::class => \DI\autowire()
        ->method('addData', \DI\get(\Geeftlist\Form\Element\Geeftee\Avatar::class . '.data')),
    \Geeftlist\Form\Element\Discussion\PostRecipientsSelect::class => \DI\autowire()
        ->constructorParameter(
            'topicRepository',
            \DI\get('Geeftlist\Model\Discussion\Topic\Repository')
        )
        ->constructorParameter(
            'geefterRepository',
            \DI\get('Geeftlist\Model\Geefter\Repository')
        ),
    \Geeftlist\Form\Element\GeefterSelect::class => \DI\autowire()
        ->constructorParameter(
            'geefterRepository',
            \DI\get('Geeftlist\Model\Geefter\Repository')
        ),
    \Geeftlist\Form\Element\Gift\Image::class => \DI\autowire()
        ->method('addData', \DI\get(\Geeftlist\Form\Element\Gift\Image::class . '.data')),
    \Geeftlist\Form\Element\Gift\PriorityRadioGroup::class => \DI\autowire()
        ->constructorParameter('fieldModel', \DI\get(\Geeftlist\Model\Gift\Field\Priority::class)),
    \Geeftlist\Form\Element\Gift\ShareRuleRadioGroup::class => \DI\autowire()
        ->constructorParameter(
            'familyRepository',
            \DI\get('Geeftlist\Model\Family\Repository')
        )
        ->constructorParameter(
            'geefterRepository',
            \DI\get('Geeftlist\Model\Geefter\Repository')
        ),
    \Geeftlist\Form\Element\Gift\StatusRadioGroup::class => \DI\autowire()
        ->constructorParameter('fieldModel', \DI\get(\Geeftlist\Model\Gift\Field\Status::class)),
    \Geeftlist\Form\Element\GiftList\StatusRadioGroup::class => \DI\autowire()
        ->constructorParameter('fieldModel', \DI\get(\Geeftlist\Model\GiftList\Field\Status::class)),
    \Geeftlist\Form\Element\GiftList\VisibilityRadioGroup::class => \DI\autowire()
        ->constructorParameter('fieldModel', \DI\get(\Geeftlist\Model\GiftList\Field\Visibility::class)),
    \Geeftlist\Form\Element\TemplateElement\Context::class => \DI\autowire()
        ->constructorParameter(
            'templateRenderer',
            \DI\get('OliveOil\Core\Service\View\Template\Renderer\FormElementRenderer')
        ),


    // Helpers
    \Geeftlist\Helper\Discussion\Post::class => \DI\autowire()
        ->constructorParameter('postRepository', \DI\get('Geeftlist\Model\Discussion\Post\Repository')),
    \Geeftlist\Helper\Family\Membership::class => \DI\autowire(),
    \Geeftlist\Helper\Geefter\EmailUpdate::class => \DI\autowire(\Geeftlist\Helper\Geefter\EmailUpdate::class)
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter('geefteeRepository', \DI\get('Geeftlist\Model\Geeftee\Repository')),
    \Geeftlist\Helper\Gift::class => \DI\autowire()
        ->constructorParameter('giftRepository', \DI\get('Geeftlist\Model\Gift\Repository'))
        ->constructorParameter('shareEntityRuleRepository', \DI\get('Geeftlist\Model\Share\Entity\Rule\Repository')),
    \Geeftlist\Helper\Reservation::class => \DI\autowire()
        ->constructorParameter('reservationRepository', \DI\get('Geeftlist\Model\Reservation\Repository')),
    \Geeftlist\Service\Security::class => \DI\autowire()
        ->constructorParameter('securityEventService', \DI\get('Geeftlist\Service\Event\GeefterAccess'))
        ->constructorParameter('registryFlag', 'geefteraccess_restriction_level'),
    \Geeftlist\Helper\Share\Entity::class => \DI\autowire(\Geeftlist\Helper\Share\Entity::class)
        ->constructorParameter('shareEntityRuleRepository', \DI\get('Geeftlist\Model\Share\Entity\Rule\Repository'))
        ->constructorParameter(
            'shareRuleTypeFactoryFactory',
            \DI\get('Geeftlist\Model\Share\RuleTypeFactory\Factory')
        )
        ->constructorParameter('defaultShareEntityRule', \DI\get('share_entity_rules.default')),
    \Geeftlist\Helper\Url::class => \DI\autowire()
        ->constructorParameter('geefteeRepository', \DI\get('Geeftlist\Model\Geeftee\Repository')),
    \Geeftlist\Helper\View\Bbcode::class => \DI\autowire()
        ->constructorParameter('decoda', \DI\factory(function(\Decoda\Decoda $decoda) {
            $decoda->setStrict(false);

            // See \Decoda\Decoda::defaults
            $decoda->addFilter(new \Decoda\Filter\DefaultFilter());
            $decoda->addFilter(new \Decoda\Filter\EmailFilter());
            $decoda->addFilter(new \Decoda\Filter\ImageFilter());
            $decoda->addFilter(new \Decoda\Filter\UrlFilter());
            $decoda->addFilter(new \Decoda\Filter\TextFilter());
            $decoda->addFilter(new \Decoda\Filter\BlockFilter());
            $decoda->addFilter(new \Decoda\Filter\VideoFilter());
            $decoda->addFilter(new \Decoda\Filter\CodeFilter());
            $decoda->addFilter(new \Decoda\Filter\QuoteFilter());
            $decoda->addFilter(new \Decoda\Filter\ListFilter());
            $decoda->addFilter(new \Decoda\Filter\TableFilter());

            // Not wanted: only works for english words (and expensive to run)
            //$decoda->addHook(new \Decoda\Hook\CensorHook());
            $decoda->addHook(new \Decoda\Hook\ClickableHook());

            return $decoda;
        })->parameter('decoda', \DI\create(\Decoda\Decoda::class))),

    // Indexers
    \Geeftlist\Indexer\Discussion\Topic\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'indexerResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer::class)
        ),
    \Geeftlist\Indexer\Gift\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'indexerResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer::class)
        ),

    // Models
    \Geeftlist\Model\App\Http::class => \DI\autowire(),
    \Geeftlist\Model\App\Cli::class => \DI\autowire(),
    \Geeftlist\Model\App\Context::class => \DI\autowire()
        ->constructorParameter('eventServices', \DI\get('event_services.all')),
    \Geeftlist\Model\Badge\Entity::class => \DI\autowire()
        ->constructorParameter(
            'resourceRelationHelper',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Badge\Entity::class)
        ),
    \Geeftlist\Model\Context::class => \DI\autowire()
        ->constructorParameter('fieldModelFactory', \DI\get(\Geeftlist\Model\Context::class . '.fieldModelFactory')),
    \Geeftlist\Model\Discussion\Post\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'restrictionsAppender',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Post\GeefterAccess::class))
        ->constructorParameter(
            'geefterAccessResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Post\GeefterAccess::class)),
    \Geeftlist\Model\Discussion\Topic\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'restrictionsAppender',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Topic\GeefterAccess::class))
        ->constructorParameter(
            'geefterAccessResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Topic\GeefterAccess::class)
        ),
    \Geeftlist\Model\Family\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'geefterAccessResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\GeefterAccess::class)
        ),
    \Geeftlist\Model\Geeftee\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'geefterAccessResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\GeefterAccess::class)
        ),
    \Geeftlist\Model\Gift\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'geefterAccessResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\GeefterAccess::class)
        ),
    \Geeftlist\Model\GiftList\GeefterAccess::class => \DI\autowire()
        ->constructorParameter(
            'geefterAccessResource',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\GiftList\GeefterAccess::class)
        ),
    \Geeftlist\Model\Geefter::class => \DI\autowire()
        ->constructorParameter('tokenLifetime', \DI\get(\Geeftlist\Model\Geefter::class . '.tokenLifetime')),
    \Geeftlist\Model\ModelFactory::class => \DI\autowire()
        ->constructorParameter('namespaces', [
            '\Geeftlist\Model',
            '\OliveOil\Core\Model'
        ]),
    \Geeftlist\Model\RepositoryFactory::class => \DI\autowire()
        ->constructorParameter('classSuffix', '\\Repository'),
    \Geeftlist\Model\ResourceModel\Db\Badge\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Badge\Relation\EntityRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Badge\Relation\LanguageRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Context::class => \DI\autowire(),
    \Geeftlist\Model\ResourceModel\Db\DefaultConnection::class => \DI\autowire()
        ->constructorParameter('adapter', \DI\get(\Geeftlist\Model\ResourceModel\Db\DefaultConnection::class . '.adapter'))
        ->constructorParameter('cache', \DI\get('OliveOil\Core\Service\Cache\ResourceModel\Connection'))
        ->constructorParameter(
            'enableQueryLog',
            \DI\get(\OliveOil\Core\Model\ResourceModel\Db\Connection::class . '.enableQueryLog')
        ),
    \Geeftlist\Model\ResourceModel\Db\Discussion\Post\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Post\Relation\AuthorRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Post\Relation\TopicRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Relation\EntityRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Relation\MetadataRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Family\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\Relation\GeefteeRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\Relation\OwnerRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation\FamilyOwnerRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation\FamilyRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation\GeefteeRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation\SponsorRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\Relation\FamilyRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\Relation\GiftRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\Relation\ReservationRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Relation\GeefteeRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Relation\GeefterRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Geefter\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geefter\Relation\FamilyRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geefter\Relation\GeefteeRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Geefter\Relation\RelatedGeefterRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Gift\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\Relation\CreatorRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\Relation\GeefteeRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\Relation\GiftListRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\Relation\ReservationStatsRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Gift\Relation\ReserverRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\GiftList\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\GiftList\Relation\GiftRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\GiftList\Relation\OwnerRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Indexer\Context::class => \DI\autowire()
        ->constructorParameter('entityShareRuleRepository', \DI\get('Geeftlist\Model\Share\Entity\Rule\Repository'))
        ->constructorParameter('shareRuleTypeFactoryFactory', \DI\get('Geeftlist\Model\Share\RuleTypeFactory\Factory')),
    \Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer::class => \DI\autowire()
        ->constructorParameter(
            'staticEntityShareRules',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer::class . '.staticEntityShareRules')
        ),
    \Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer::class => \DI\autowire()
        ->constructorParameter(
            'staticEntityShareRules',
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer::class . '.staticEntityShareRules')
        ),
    \Geeftlist\Model\ResourceModel\Db\Reservation\Collection::class => \DI\autowire()
        ->constructorParameter('relationHelpers', [
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Reservation\Relation\GeefteeRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Reservation\Relation\GeefterRelationHelper::class),
            \DI\get(\Geeftlist\Model\ResourceModel\Db\Reservation\Relation\GiftRelationHelper::class),
        ]),
    \Geeftlist\Model\ResourceModel\Db\Setup::class => \DI\autowire()
        ->constructorParameter('context', \DI\get(\Geeftlist\Model\ResourceModel\Db\Setup\Context::class)),
    \Geeftlist\Model\ResourceModel\Db\Setup\Context::class => \DI\autowire()
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class)),
    \Geeftlist\Model\ResourceFactory::class => \DI\autowire(\Geeftlist\Model\ResourceFactory::class)
        ->constructorParameter('namespaces', 'Geeftlist\Model\ResourceModel\Db'),
    \Geeftlist\Model\Session\Http\Geefter::class => \DI\autowire()
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter('phpSession', \DI\get(\OliveOil\Core\Model\Session\Http\PhpSession::class))
        ->constructorParameter('name', 'geefter'),
    \Geeftlist\Model\Setup::class => \DI\autowire()
        ->constructorParameter('modelFactory', \DI\get(\Geeftlist\Model\ModelFactory::class))
        ->constructorParameter('resourceModelFactory', \DI\get(\Geeftlist\Model\ResourceFactory::class)),
    \Geeftlist\Model\Share\Entity\Rule::class => \DI\autowire()
        ->constructorParameter(
            'shareRuleTypeFactoryFactory',
            \DI\get('Geeftlist\Model\Share\RuleTypeFactory\Factory')
        ),
    \Geeftlist\Block\Cms::class => \DI\autowire()
        ->constructorParameter(
            'templateRenderer',
            \DI\get(\OliveOil\Core\Service\View\Template\Renderer\DesignRenderer::class)
        ),
    \Geeftlist\Block\Form\Gift::class => \DI\autowire(),
    \Geeftlist\Block\Geeftee::class => \DI\autowire()
        ->constructorParameter('geefteeRepository', \DI\get('Geeftlist\Model\Geeftee\Repository')),
    \Geeftlist\Block\Gift::class => \DI\autowire()
        ->constructorParameter('geefteeRepository', \DI\get('Geeftlist\Model\Geeftee\Repository'))
        ->constructorParameter('giftRepository', \DI\get('Geeftlist\Model\Gift\Repository'))
        ->constructorParameter('reservationRepository', \DI\get('Geeftlist\Model\Reservation\Repository')),

    // Observers
    \Geeftlist\Observer\Activity::class => \DI\autowire()
        ->constructorParameter('activityRepository', \DI\get('Geeftlist\Model\Activity\Repository'))
        ->constructorParameter('giftRestrictionsAppender', \DI\get(\Geeftlist\Model\Gift\GeefterAccess::class)),
    \Geeftlist\Observer\Context::class => \DI\autowire(),
    \Geeftlist\Observer\Cron\Gift\Image::class => \DI\autowire()
        ->constructorParameter('imageBasePath', \DI\get(\Geeftlist\Service\Gift\Image::class . '.imageBasePath')),
    \Geeftlist\Observer\Cron\Indexer::class => \DI\autowire()
        ->constructorParameter('indexers', \DI\get('indexers.all')),
    \Geeftlist\Observer\Cron\MailNotification\Context::class => \DI\autowire()
        ->constructorParameter('notificationRepository', \DI\get('Geeftlist\Model\Notification\Repository'))
        ->constructorParameter(
            'mailNotificationItemServiceFactory',
            \DI\get('Geeftlist\Service\MailNotification\Item\Factory')
        ),
    \Geeftlist\Observer\Cron\MailNotification\Geefter::class => \DI\autowire()
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository')),
    \Geeftlist\Observer\Cron\MailNotification\Geefter\Discussion\PostMentionNotification::class => \DI\autowire()
        ->constructorParameter('postRepository', \DI\get('Geeftlist\Model\Discussion\Post\Repository'))
        ->constructorParameter(
            'enabled',
            \DI\get(\Geeftlist\Observer\Cron\MailNotification\Geefter\Discussion\PostMentionNotification::class . '.enabled')
        ),
    \Geeftlist\Observer\Cron\MailNotification\Geefter\LatestActivity::class => \DI\autowire()
        ->constructorParameter(
            'geefterNotifiableStatus',
            \DI\get(\Geeftlist\Observer\Cron\MailNotification\Geefter::class . '.geefterNotifiableStatus')
        ),
    \Geeftlist\Observer\Cron\Reservation\StaleNotification::class => \DI\autowire()
        ->constructorParameter('reservationRepository', \DI\get('Geeftlist\Model\Reservation\Repository'))
        ->constructorParameter('giftRepository', \DI\get('Geeftlist\Model\Gift\Repository'))
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter(
            'enabled',
            \DI\get(\Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.enabled')
        )
        ->constructorParameter(
            'staleReservationDelay',
            \DI\get(\Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.staleReservationDelay')
        )
        ->constructorParameter(
            'stalePurchaseDelay',
            \DI\get(\Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.stalePurchaseDelay')
        )
        ->constructorParameter(
            'batchSize',
            \DI\get(\Geeftlist\Observer\Cron\Reservation\StaleNotification::class . '.batchSize')
        ),
    \Geeftlist\Observer\Discussion\Topic::class => \DI\autowire()
        ->constructorParameter('topicRepository', \DI\get('Geeftlist\Model\Discussion\Topic\Repository')),
    \Geeftlist\Observer\Geeftee::class => \DI\autowire()
        ->constructorParameter('nameValidationPattern', \DI\get('geeftee.name_validation_regex')),
    \Geeftlist\Observer\Geefter::class => \DI\autowire()
        ->constructorParameter('usernameValidationPattern', \DI\get('geefter.username_validation_regex')),
    \Geeftlist\Observer\Geefter\PasswordBasedSessionValidation::class => \DI\autowire()
        ->constructorParameter(
            'geefterSessionSecretKey',
            \DI\get(\Geeftlist\Observer\Geefter\PasswordBasedSessionValidation::class . '.geefterSessionSecretKey')
        ),
    \Geeftlist\Observer\Gift::class => \DI\autowire()
        ->constructorParameter('giftRestrictionsAppender', \DI\get(\Geeftlist\Model\Gift\RestrictionsAppenderInterface::class)),
    \Geeftlist\Observer\GiftList::class => \DI\autowire()
        ->constructorParameter('giftListRestrictionsAppender', \DI\get(\Geeftlist\Model\GiftList\RestrictionsAppenderInterface::class)),
    \Geeftlist\Observer\Indexer::class => \DI\autowire()
        ->constructorParameter('dependentEntityResolvers', \DI\get(\Geeftlist\Observer\Indexer::class . '.dependentEntityResolvers'))
        ->constructorParameter('indexers', \DI\get('indexers.all')),
    \Geeftlist\Observer\MailNotification\Discussion\Post::class => \DI\autowire(),
    \Geeftlist\Observer\MailNotification\Reservation::class => \DI\autowire()
        ->constructorParameter('giftRepository', \DI\get('Geeftlist\Model\Gift\Repository'))
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository')),
    \Geeftlist\Observer\Setup::class => \DI\autowire()
        ->constructorParameter('indexers', \DI\get('indexers.all')),
    \Geeftlist\Observer\Share\Entity\Rule::class => \DI\autowire()
        ->constructorParameter(
            'shareRuleTypeFactoryFactory',
            \DI\get('Geeftlist\Model\Share\RuleTypeFactory\Factory')
        )
        ->constructorParameter(
            'shareEntityRuleRepository',
            \DI\get('Geeftlist\Model\Share\Entity\Rule\Repository')
        ),

    // Services
    \Geeftlist\Service\ActivityLog::class => \DI\autowire()
        ->constructorParameter('activityLogRepository', \DI\get('Geeftlist\Model\ActivityLog\Repository')),
    \Geeftlist\Service\Authentication\Geefter\Context::class => \DI\autowire()
        ->constructorParameter(
            'geefterRepository', // This instance must *not* use cache, see \Geeftlist\Service\Authentication\Geefter\Context
            function (\Psr\Container\ContainerInterface $container) {
                return $container->make('Geeftlist\Model\Geefter\Repository', [
                    'useCache' => false
                ]);
            }
        ),
    \Geeftlist\Service\Authentication\Geefter\JwtMethod::class => \DI\autowire()
        ->constructorParameter('configuration', \DI\get('jwt.geefter.config'))
        ->constructorParameter('builderPrototype', \DI\get('jwt.geefter.builder')),
    \Geeftlist\Service\Avatar\Geeftee::class => \DI\autowire()
        ->constructorParameter('generator', \DI\get('Geeftlist\Service\Avatar\Generator\Geeftee'))
        ->constructorParameter('uploadService', \DI\get('Geeftlist\Service\Upload\Image\Geeftee'))
        ->constructorParameter('geefteeRepository', \DI\get('Geeftlist\Model\Geeftee\Repository'))
        ->constructorParameter('avatarBasePath', \DI\get(\Geeftlist\Service\Avatar\Geeftee::class . '.avatarBasePath'))
        ->constructorParameter('avatarUrlBasePath', \DI\get(\Geeftlist\Service\Avatar\Geeftee::class . '.avatarUrlBasePath'))
        ->constructorParameter('placeholderUrl', \DI\get(\Geeftlist\Service\Avatar\Geeftee::class . '.placeholderUrl')),
    \Geeftlist\Service\Badge::class => \DI\autowire()
        ->constructorParameter('badgeRepository', \DI\get('Geeftlist\Model\Badge\Repository')),
    \Geeftlist\Service\Geefter\Account::class => \DI\autowire()
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository')),
    \Geeftlist\Service\Gift\Image::class => \DI\autowire()
        ->constructorParameter('uploadService', \DI\get('Geeftlist\Service\Upload\Image\Gift'))
        ->constructorParameter('imageBasePath', \DI\get(\Geeftlist\Service\Gift\Image::class . '.imageBasePath'))
        ->constructorParameter('imageUrlBasePath', \DI\get(\Geeftlist\Service\Gift\Image::class . '.imageUrlBasePath'))
        ->constructorParameter('placeholderUrl', \DI\get(\Geeftlist\Service\Gift\Image::class . '.placeholderUrl'))
        ->constructorParameter('imageTypesConfig', \DI\get(\Geeftlist\Service\Gift\Image::class . '.imageTypesConfig')),
    \Geeftlist\Service\Invitation::class => \DI\autowire()
        ->constructorParameter('invitationRepository', \DI\get('Geeftlist\Model\Invitation\Repository')),
    \Geeftlist\Service\MailNotification::class => \DI\autowire()
        ->constructorParameter('notificationRepository', \DI\get('Geeftlist\Model\Notification\Repository'))
        ->constructorParameter('eventService', \DI\get('Geeftlist\Service\Event\MailNotification')),
    \Geeftlist\Service\MailNotification\Item\Context::class => \DI\autowire()
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter('giftRepository', \DI\get('Geeftlist\Model\Gift\Repository'))
        ->constructorParameter('geefteeRepository', \DI\get('Geeftlist\Model\Geeftee\Repository'))
        ->constructorParameter('familyRepository', \DI\get('Geeftlist\Model\Family\Repository'))
        ->constructorParameter('reservationRepository', \DI\get('Geeftlist\Model\Reservation\Repository')),
    \Geeftlist\Service\MailNotification\Item\Discussion\Post::class => \DI\autowire()
        ->constructorParameter('discussionTopicRepository', \DI\get('Geeftlist\Model\Discussion\Topic\Repository'))
        ->constructorParameter('discussionPostRepository', \DI\get('Geeftlist\Model\Discussion\Post\Repository')),
    \Geeftlist\Service\Permission::class => \DI\autowire()
        ->constructorParameter('delegators', function (\Psr\Container\ContainerInterface $container) {
            return [
                'geefterSession' => $container->get(\Geeftlist\Service\Permission\GeefterSession::class)
            ];
        }),
    \Geeftlist\Service\Permission\Geefter::class => \DI\autowire()
        ->constructorParameter('entityDelegators', \DI\get(\Geeftlist\Service\Permission\Geefter::class . '.entityDelegators')),
    \Geeftlist\Service\Url\Builder\Api::class => \DI\autowire()
        ->constructorParameter('baseUrl', \DI\get('api.base_url'))
        ->constructorParameter('areaPath', \DI\get('api.area_url_path')),
    \Geeftlist\Service\View\Context::class => \DI\autowire(),

    // == View
    \Geeftlist\View\Grid\Gift::class => \DI\autowire()
        ->constructorParameter('renderer', \DI\get(\Geeftlist\View\Grid\Renderer\Gift::class)),

    // == Third-party
    \Cache\Adapter\Redis\RedisCachePool::class => \DI\autowire()
        ->constructorParameter('cache', \DI\get(\Cache\Adapter\Redis\RedisCachePool::class . '.cache')),
    \Symfony\Component\Mailer\Mailer::class => \DI\autowire()
        ->constructorParameter('transport', \DI\get(\Symfony\Component\Mailer\Mailer::class . '.transport')),
];
