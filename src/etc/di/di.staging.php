<?php
return [
    'mailcatcher.host' => 'mailrelay',
    'mailcatcher.webui_port' => 48281,

    \OliveOil\Core\Model\ResourceModel\Db\Connection::class . '.enableQueryLog' => false,
    \OliveOil\Core\Service\Event::class . '.enableEventDebugListener' => false,
];
