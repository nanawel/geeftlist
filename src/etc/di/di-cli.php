<?php
return [
    // ########################################################################
    // Values
    // ########################################################################
    // OliveOil
    \OliveOil\Core\Service\Route::class . '.params' => function(\DI\Container $container) {
        $fw = $container->get(\Base::class);
        return [
            'route_mapping' => [
                // Fallback to REDIRECT_* needed because when this line is executed the keys have not been translated yet
                '/cli' => $fw->get('SERVER.URL_PATH_PREFIX') ?: $fw->get('SERVER.REDIRECT_URL_PATH_PREFIX')
            ]
       ];
    },

    // Event Services
    'event_services.all' => [
        // Note: The array key here is NOT the event space, just a unique key to identify each instance
        // and to be able to override each one if necessary. For event space, see 40-virtual.php
        'default' => \DI\get(\OliveOil\Core\Service\Event::class),
        //'activity' => \DI\get('Geeftlist\Service\Event\Activity'),
        //'activitylog' => \DI\get('Geeftlist\Service\Event\ActivityLog'),
        //'adminotification' => \DI\get('Geeftlist\Service\Event\AdminNotification'),
        //'cron' => \DI\get('Geeftlist\Service\Event\Cron'),
        //'geefteraccess' => \DI\get('Geeftlist\Service\Event\GeefterAccess'),
        //'geefternotification' => \DI\get('Geeftlist\Service\Event\GeefterNotification'),
        'index' => \DI\get('Geeftlist\Service\Event\Index'),
        'cache' => \DI\get('Geeftlist\Service\Event\Cache'),
        //'mailnotification' => \DI\get('Geeftlist\Service\Event\MailNotification'),
        //'profiler' => \DI\get('OliveOil\Core\Service\Event\Dev\Profiler'),
    ],

    // ########################################################################
    // Interface/class Preferences
    // ########################################################################
    // OliveOil
    \OliveOil\Core\Model\AppInterface::class => DI\autowire(\Geeftlist\Model\App\Cli::class),
    \OliveOil\Core\Service\Session\Manager::class => DI\create()
        ->constructor(
            DI\get(\OliveOil\Core\Service\Session\Factory::class),
            []
        ),

    // ########################################################################
    // DI Definitions
    // ########################################################################

    \Geeftlist\Controller\Cli\Indexer::class . '.indexers' => \DI\get('indexers.all'),
];
