<?php
return [
    'Geeftlist\Service\Authentication\Geefter\Api\MethodFactory.codeClassArray' => [
        'apikey' => \Geeftlist\Service\Authentication\Geefter\ApiKeyMethod::class,
        'bearer' => \Geeftlist\Service\Authentication\Geefter\JwtMethod::class,
    ],
];
