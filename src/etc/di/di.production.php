<?php
return [
    \OliveOil\Core\Model\ResourceModel\Db\Connection::class . '.enableQueryLog' => false,
    \OliveOil\Core\Service\Event::class . '.enableEventDebugListener' => false,
];
