; APP CONFIG (HTTP)
; Do not modify. Use local.ini to override instead.
;

[app]
TITLE=Geeftlist
VERSION=dev
DB_VERSION_SCHEMA=0.48.0
DB_VERSION_DATA=0.36.0
DB_VERSION_SAMPLEDATA=0.39.0
BASE_URL=https://geeftlist.local/
MODE=production

; Setup
SETUP_SCRIPTS_DIR=app/code/Geeftlist/setup/

; Some more system stuffs
URL_DEFAULT_FORCE_SCHEME=0
USE_X_FORWARDED_HOST=1
DB_CONNECT_TIMEOUT=20
MAINTENANCE_FLAG=var/lock/maintenance.flag
ERROR_USE_REDIRECT=1
ERROR_FALLBACK_REDIRECT_ROUTE=error
ERROR_STATIC_500_PAGE=static/500.html
LOCK_DIR=var/lock/
CRON_ALLOWED_IPS=127.0.0.1/8,::0,172.16.0.1/12
HEALTHCHECK_ALLOWED_IPS=127.0.0.1/8,::0,172.16.0.1/12

; View
VIEW.TEMPLATE.ENGINE.text/html=OliveOil\Core\Service\View\Template\Engine\DefaultEngine
VIEW.TEMPLATE.ENGINE.text/markdown=OliveOil\Core\Service\View\Template\Engine\MarkdownEngine
VIEW.TEMPLATE.ENGINE.*=OliveOil\Core\Service\View\Template\Engine\DefaultEngine

; Sessions - Use local.ini to override
SESSION.default=PHPSESSID
; (use the same syntax as for CACHE in config.ini)
;SESSION.admin=admin     ;For future use
SESSION.PHPSESSID.dsn=folder=var/sessions/
; Behavior on session context change ("suspect"): invalidate, invalidate-client, 403 or ignore. Default: invalidate
; See \OliveOil\Core\Model\Session\Http\PhpSession
; Update 2019-11-24: (#456) Ignore suspect sessions since IP may change on mobile devices and F3 does not offer
;                    any good way of correctly handling that (= check only user-agent for example)
SESSION.PHPSESSID.on_suspect=ignore
SESSION_TYPE.session.lifetime=0
SESSION_TYPE.persistent.lifetime=2592000

; Cookies
; ~ means "null" which translates in "only current domain excluding subdomains" (recommended)
COOKIE.domain=~
;COOKIE.path=

; CSRF
CSRF_TOKEN=tok

; CSP
CSP_HEADER=default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; style-src-elem 'self' 'unsafe-inline'; img-src 'self' data:; report-uri /system/cspReport/post
;CSP_REPORT_ONLY_HEADER=default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; style-src-elem 'self' 'unsafe-inline'; img-src 'self' data:; report-uri /system/cspReport/post

; Cache
RESOURCE_CACHE.pool=Cache\Adapter\Redis\RedisCachePool

; Models Resources
RESOURCE_CONFIG.type=db

; NOTICE: In the default context using provided Makefile, those variables will be taken from the environment
; so overridden values here *will be ignored*
; Database configuration (see http://framework.zend.com/manual/current/en/modules/zend.db.adapter.html)
RESOURCE_CONFIG.db.driver=Pdo_Mysql
;RESOURCE_CONFIG.db.database=
;RESOURCE_CONFIG.db.username=
;RESOURCE_CONFIG.db.password=
;RESOURCE_CONFIG.db.hostname=
;RESOURCE_CONFIG.db.port=
;RESOURCE_CONFIG.db.charset=

; Redis cache configuration
;RESOURCE_CONFIG.redis_cache.host=
;RESOURCE_CONFIG.redis_cache.port=6379
;RESOURCE_CONFIG.redis_cache.database=0
;RESOURCE_CONFIG.redis_cache.timeout=5

; Logs
LOG_ENABLED=1
LOG_HTTP_ERRORS=0
;LOG_LEVEL=     # Keep commented, special behavior based on globals.DEBUG
APP_LOG=app.log
CACHE_LOG=cache.log
CACHE_LOG_LEVEL=WARNING
CRON_LOG=cron.log
CRON_LOG_LEVEL=NOTICE
DEBUG_LOG=debug.log
ERROR_LOG=error.log
EVENTS_LOG=events.log
EVENTS_LOG_LEVEL=WARNING
INDEXER_LOG=indexer.log
INDEXER_LOG_LEVEL=WARNING
I18N_LOG=i18n.log
I18N_LOG_LEVEL=WARNING
MAIL_LOG=mail.log
MAIL_LOG_LEVEL=NOTICE
SECURITY_LOG=security.log
SECURITY_LOG_LEVEL=WARNING
SETUP_LOG=setup.log
SETUP_LOG_LEVEL=WARNING

; PHP config
PHP.html_errors=0
PHP.display_errors=0
PHP.memory_limit=256M

; Developper tweaks - Use local.ini to override
; Those settings should be set to 0 in production
DEV.VIEW_BLOCK_THROW_EXCEPTION=0
DEV.PROFILER_ENABLE=0
DEV.EVENT_ENABLE_TRIGGER_EVENT=0
DEV.DUMP_ENTITIES_CONTROLLER_ENABLED=0

; Mail - Use local.ini to override
EMAIL_ENABLED=1
EMAIL_ADMIN=admin@geeftist.local
EMAIL_ABUSE=abuse@geeftist.local
EMAIL_CONTACT=contact@geeftlist.local
EMAIL_DEFAULT_SENDER=Geeftlist <noreply@geeftlist.local>
EMAIL_TEMPLATE_TTL=1800
EMAIL_TEMPLATE_THEME=email-default
EMAIL_MAILER_DSN=smtp://localhost:25
SKIP_EMAIL_CHECK_MX=0
NO_CONFIRMATION_EMAIL=0

; Notifications
; Delay before sending (in minutes)
NOTIFICATION_DEFER_DELAY=20
; Max pending notification before sending
NOTIFICATION_DEFER_MAX_COUNT=12
; Delay before cleaning up success notification (in hours)
NOTIFICATION_SUCCESS_CLEANUP_HISTORY_KEEP=24
; Delay before cleaning up failed notification (in hours)
NOTIFICATION_FAILURE_CLEANUP_HISTORY_KEEP=48

; Geefters
PASSWORD_MIN_LENGTH=8
; Session hardening using password
GEEFTER_VALIDATE_PASSWORD_HASH=1
;GEEFTER_SESSION_SECRET_KEY=changeme
; Token validity lifetime, in hours
GEEFTER_TOKEN_LIFETIME=24
; Display/Hide API token field in the account
GEEFTER_SHOW_API_TOKEN_FORM=0
; Allow new registrations
GEEFTER_REGISTRATION_ENABLE=1

; Geeftees
AVATAR_VERSION=1
AVATAR_FILETYPES=png,jpg,jpeg,gif,webp
AVATAR_MAX_FILESIZE=102400
AVATAR_MAX_WIDTH=100
AVATAR_MAX_HEIGHT=100

; Gifts
GIFT_IMAGE_FILETYPES=png,jpg,jpeg,gif,webp
GIFT_IMAGE_MAX_FILESIZE=5242880
GIFT_IMAGE_MAX_WIDTH=5000
GIFT_IMAGE_MAX_HEIGHT=5000
GIFT_REPORT_MESSAGE_MAXLENGTH=1000
GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION=1

; Reservations
RESERVATION_NOTIFICATION_STALE_ENABLED=1
; Delay before notifying a stale reservation (in hours)
RESERVATION_NOTIFICATION_STALE_DELAY=1080
; Delay before notifying a stale purchase (in hours)
RESERVATION_NOTIFICATION_STALE_PURCHASE_DELAY=2160
RESERVATION_NOTIFICATION_BATCH_SIZE=100

; Discussions
DISCUSSION_POST_NOTIFICATION_ENABLED=1

; Activities
; Delay before cleaning up activities (in days)
ACTIVITY_HISTORY_KEEP=45

; Activity Log
; Delay before cleaning up activity logs (in days)
ACTIVITYLOG_HISTORY_KEEP=90

; Captchas
CAPTCHA_GEEFTER_SIGNUP_ENABLE=1
CAPTCHA_INVITATION_CREATION_ENABLE=1
CAPTCHA_CONTACT_GEEFTER_ENABLE=0
CAPTCHA_CONTACT_VISITOR_ENABLE=1

; Invitations
INVITATION_ENABLED=1
INVITATION_MAX_PER_GEEFTER=0
INVITATION_TOKEN_LENGTH=64
INVITATION_TOKEN_TTL=86400

; Dashboard widgets
DASHBOARD_WIDGET.LATEST_RESERVATIONS.items_count=5
DASHBOARD_WIDGET.LATEST_DISCUSSION_POSTS.items_count=5
DASHBOARD_WIDGET.RECENT_ACTIVITY.items_count=5

; Theme / UI
THEME=default
FAVICON=favicon.png
ASSETS_VERSION_ID=

; Available languages/locales (see app/dict/ for lexicons)
LANGUAGES=en-US,fr-FR
; Skeleton language for translation
LANGUAGE_SKEL=fr

; Locale settings - Use local.ini to override
; See also LANGUAGE and FALLBACK in config.ini
; See https://en.wikipedia.org/wiki/ISO_4217#Active_codes for currency codes
CURRENCY=EUR
CURRENCIES=EUR,USD,GBP,JPY
TIMEZONE=UTC

; Misc - Use local.ini to override
; Display page rendering stats in page footer. Should be disabled in production env.
DISPLAY_STATS=0
SHOW_VERSION=0

; ActivityLog
ACTIVITYLOG_ENABLED=1
ACTIVITYLOG_CRON_ENABLED=0

[api]
ENABLE=1
SENSITIVE_ACTIONS_DELAY=500
;AUTHENTICATION_JWT_SECRET_KEY=changeme
;AUTHENTICATION_JWT_EXPIRATION_TIME=3600
AUTHORIZATION_HEADER=Authorization
