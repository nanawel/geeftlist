<?php
return [
    // ########################################################################
    // Values
    // ########################################################################

    'placeholder_service.host' => 'placeholder-service',
    'placeholder_service.port' => '80',

    // In UT context, geeftee names may start with "@" (which has a special meaning = geefterless)
    'geeftee.name_validation_regex' => '/^[\p{L}@][\p{L}\d\-_@+ .]{2,63}$/iu',

    \OliveOil\Core\Service\Log\Resolver::class . '.configFiles' => ['etc/loggers.yml', 'etc/test/loggers.yml'],
    \OliveOil\Core\Service\Route::class . '.params' => function(\Psr\Container\ContainerInterface $container) {
        return [
            'route_mapping' => [
                '/api' => '/api',
                '/cli' => '/cli'
            ]
        ];
    },

    'Geeftlist\Service\Authentication\Geefter\Api\MethodFactory.codeClassArray' => [
        'basic' => \Geeftlist\Service\Authentication\Geefter\PasswordMethod::class,
        'apikey' => \Geeftlist\Service\Authentication\Geefter\ApiKeyMethod::class,
        'bearer' => \Geeftlist\Service\Authentication\Geefter\JwtMethod::class,
    ],


    // ########################################################################
    // Interface/class Preferences
    // ########################################################################
    // OliveOil
    \OliveOil\Core\Model\Cache\ArrayObjectInterface::class => \DI\create(\OliveOil\Core\Model\Cache\VolatileArrayObject::class),
    \OliveOil\Core\Service\Rest\JsonApi\ResponseWriter::class => \DI\autowire()
        ->constructorParameter('callbacks', \DI\value([
            'afterWriteBody' => [
                // Special callback for controllers UT: Throw an exception to be easily checked during tests
                function(\OliveOil\Core\Model\Rest\ResponseInterface $response) {
                    /** @var \OliveOil\Core\Service\Registry $registry */
                    $registry = \OliveOil\Core\Service\Di\Container::instance()->get(\OliveOil\Core\Service\Registry::class);
                    if (!$registry->get('skip_exception_after_write_response')
                        && !preg_match('/2../', $response->getCode())
                    ) {
                        \Narrowspark\HttpStatus\HttpStatus::getReasonException($response->getCode());
                    }
                }
            ]
        ])),
    \OliveOil\Core\Service\Email\Sender\SenderInterface::class => \DI\get(\OliveOil\Core\Test\Util\MailcatcherEmailSender::class),
    \OliveOil\Core\Service\Http\RequestManagerInterface::class => \DI\get(\OliveOil\Core\Test\Service\Http\RequestManager::class),
    \OliveOil\Core\Test\BootstrapInterface::class => \DI\get(\Geeftlist\Test\Bootstrap::class),

    // Geeftlist
    \Geeftlist\Model\Session\GeefterInterface::class => \DI\get(\Geeftlist\Model\Session\Http\GeefterInterface::class),

    // ########################################################################
    // DI Definitions
    // ########################################################################
    'OliveOil\Core\Model\Session\Http\Core' => \DI\autowire(\OliveOil\Core\Test\Model\FakeHttpSession::class)
        ->constructorParameter('name', 'core'),
    \OliveOil\Core\Model\Repository\ModelCache::class => \DI\autowire()
        ->constructorParameter('isEnabled', false),   // Caution! When enabled, it might hide or generate bugs, when disabled, it will heavily harm performances!
    \OliveOil\Core\Test\Util\MailcatcherConnection::class => function(\Psr\Container\ContainerInterface $container) {
        return new \OliveOil\Core\Test\Util\MailcatcherConnection(
            $container->get('mailcatcher.host'),
            $container->get('mailcatcher.webui_port')
        );
    },
    \OliveOil\Core\Test\Util\MailcatcherEmailSender::class => \DI\autowire()
        ->constructorParameter('mailer', \DI\get(\Symfony\Component\Mailer\MailerInterface::class)),

    \Geeftlist\Model\Session\Http\GeefterInterface::class => \DI\autowire(\Geeftlist\Test\Model\Geefter\FakeHttpSession::class)
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter('name', 'geefter'),
    \Geeftlist\Observer\Cron\MailNotification\Geefter::class . '.geefterNotifiableStatus'
        => \DI\autowire(\OliveOil\Core\Model\Cache\VolatileArrayObject::class),
    \Geeftlist\Test\Bootstrap::class => \DI\autowire()
        ->constructorParameter('setup', \DI\get(\Geeftlist\Model\Setup::class))
        ->constructorParameter('indexerFactory', \DI\get('Geeftlist\Indexer\Factory')),

    // Not used atm
    \Geeftlist\Test\Util\SampleData::class => \DI\autowire()
        ->constructorParameter('sampleDataFolder', 'app/code/Geeftlist/Test/sampledata'),

    // ########################################################################
    // Virtual Classes
    // ########################################################################
    // Special cache service used by some test classes where cache is not wanted
    'OliveOil\Core\Service\Cache\Void' => \DI\autowire(\OliveOil\Core\Service\Cache::class)
        ->constructorParameter('pool', \DI\get(\Cache\Adapter\Void\VoidCachePool::class))
        ->constructorParameter('namespace', 'disabled')
        ->constructorParameter('defaultTtl', \DI\get('cache.ttl.x-long')),

    'OliveOil\Core\Test\Util\ImagePlaceholderUrlGenerator' => function(\Psr\Container\ContainerInterface $container) {
        $serviceHost = $container->get('placeholder_service.host');
        $servicePort = $container->get('placeholder_service.port');
        $serviceScheme = $servicePort == 443 ? 'https' : 'http';

        return function($width, $height, $format) use ($serviceScheme, $serviceHost, $servicePort) {
            return sprintf(
                '%s://%s:%d/img?width=%d&height=%d&format=%s',
                $serviceScheme,
                $serviceHost,
                $servicePort,
                $width,
                $height,
                $format
            );
        };
    },
    'OliveOil\Core\Test\Util\InvalidImagePlaceholderUrlGenerator' => function(\Psr\Container\ContainerInterface $container) {
        $serviceHost = $container->get('placeholder_service.host');
        $servicePort = $container->get('placeholder_service.port');
        $serviceScheme = $servicePort == 443 ? 'https' : 'http';

        return function() use ($serviceScheme, $serviceHost, $servicePort) {
            return sprintf(
                '%s://%s:%d/',
                $serviceScheme,
                $serviceHost,
                $servicePort
            );
        };
    }
];
