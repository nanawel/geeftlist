<?php
return [
    \OliveOil\Core\Model\Http\ResponseInterface::class => \DI\autowire(\OliveOil\Core\Test\Model\Rest\TestResponse::class),
    \OliveOil\Core\Model\Rest\ResponseInterface::class => \DI\autowire(\OliveOil\Core\Test\Model\Rest\TestResponse::class),

    \Geeftlist\Model\Session\GeefterInterface::class => \DI\autowire(\Geeftlist\Test\Model\Geefter\FakeSession::class)
        ->constructorParameter('geefterRepository', \DI\get('Geeftlist\Model\Geefter\Repository'))
        ->constructorParameter('name', 'geefter'),
];