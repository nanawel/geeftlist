<?php
return \OliveOil\Core\Model\I18n::toF3Dict(array(
    'gift.field||estimated_price' => 'estimated price/goal amount',
    'gift.field||image_id' => 'image',
    'gift.field||geeftees' => 'geeftee(s)',
    'geeftee.field||dob' => 'date of birth',
    'giftlist_select_option||none' => '(None)',
    'discussion_post.field||message' => 'message',
    'discussion_post.field||recipient_ids' => 'recipients',
));
