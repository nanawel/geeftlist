<?php
//
/** @var \OliveOil\Core\Service\Design\Theme\Bootstrap $this */

/** @var \OliveOil\Core\Block\HtmlRootContainer $head */
$head = $this->layout->getBlock('head'); // @phpstan-ignore-line
$head->addCss(['app.css']);

/** @var \OliveOil\Core\Block\HtmlRootContainer $footer */
$footer = $this->layout->getBlock('footer'); // @phpstan-ignore-line
$footer->addJs(['app.js'], -1000);
