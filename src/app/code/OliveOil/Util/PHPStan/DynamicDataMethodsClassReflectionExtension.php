<?php

declare(strict_types=1);

namespace OliveOil\Util\PHPStan;


use PHPStan\Reflection\ClassReflection;
use PHPStan\Reflection\Dummy\DummyMethodReflection;
use PHPStan\Reflection\MethodReflection;
use PHPStan\Reflection\MethodsClassReflectionExtension;

/**
 * @see \OliveOil\Core\Model\MagicObject
 */
class DynamicDataMethodsClassReflectionExtension implements MethodsClassReflectionExtension
{
    public function hasMethod(ClassReflection $classReflection, string $methodName): bool {
        return $this->hasDynamicDataMethods($classReflection)
            && in_array(substr($methodName, 0, 3), ['get', 'set', 'uns', 'has']);
    }

    public function getMethod(ClassReflection $classReflection, string $methodName): MethodReflection {
        return new DummyMethodReflection($methodName);
    }

    protected function hasDynamicDataMethods(ClassReflection $classReflection): bool {
        return array_key_exists(
            \OliveOil\Core\Iface\DynamicDataMethodsAware::class,
            $classReflection->getInterfaces()
        );
    }
}
