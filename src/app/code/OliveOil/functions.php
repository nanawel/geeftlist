<?php
declare(strict_types=1);

namespace OliveOil;

use OliveOil\Core\Exception\InvalidTypeException;

/**
 * Get class' namespace
 *
 * @param object|string $class
 */
function get_namespace(mixed $class): string {
    $class = is_string($class) ? trim($class, '\\') : $class::class;

    if (($ns = substr($class, 0, strrpos($class, '\\') ?: 0)) === false) {
        throw new \RuntimeException('Cannot find class namespace for ' . $class);
    }

    return $ns;
}

/**
 * Get class' name (without its namespace if any)
 *
 * @param object|string $class
 */
function get_classname(mixed $class): string {
    $class = is_string($class) ? trim($class, '\\') : $class::class;

    return substr($class, (strrpos($class, '\\') ?: -1) + 1);
}

/**
 * Converts string from CamelCase to snake_case
 */
function camelcase2snakecase(string $string): string {
    static $cache = [];
    if (! isset($cache[$string])) {
        $cache[$string] = strtolower((string) preg_replace('/(.)([A-Z])/', '$1_$2', $string));
    }

    return $cache[$string];
}

/**
 * Converts string from snake_case to CamelCase
 */
function snakecase2camelcase(string $string): string {
    static $cache = [];
    if (! isset($cache[$string])) {
        $cache[$string] = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
    }

    return $cache[$string];
}

/**
 * Filters $array keeping only keys present in $keys
 */
function array_mask(array $array, array $keys): array {
    return array_intersect_key($array, array_flip($keys));
}

/**
 * Filters $array keeping only keys NOT present in $keys
 */
function array_discard(array $array, array $keys): array {
    return array_filter(
        $array,
        /** @param mixed $key */
        static fn(mixed $key): bool => !in_array($key, $keys),
        ARRAY_FILTER_USE_KEY
    );
}

/**
 * Filters $array keeping only keys present in $keys and adding them if missing
 * with default value $defaultValue
 */
function array_union_intersect(array $array, array $keys = [], mixed $defaultValue = null): array {
    $defaultArray = array_map(
        static fn($ignored): mixed => $defaultValue,
        array_flip($keys)
    );
    $array += $defaultArray;

    return array_intersect_key($array, $defaultArray);
}

/**
 * Computes the difference of arrays, without throwing an error if given multidimensional arrays.
 *
 * @see http://php.net/manual/en/function.array-diff.php
 * @see https://bugs.php.net/bug.php?id=60198
 *
 * @param bool $strict
 * @return array
 */
function array_diff(array $array1, array $array2, $strict = false) {
    foreach ($array1 as $k1 => $v1) {
        foreach ($array2 as $v2) {
            if (
                ($strict && $v2 === $v1)
                || (!$strict && $v2 == $v1)
            ) {
                unset($array1[$k1]);
                continue 2;
            }
        }
    }

    return $array1;
}

/**
 * @param bool|callable $condition
 */
function swap_if(mixed &$a, mixed &$b, bool|callable $condition): void {
    if (is_callable($condition)) {
        $condition = $condition();
    }

    if ($condition) {
        $tmp = $a;
        $a = $b;
        $b = $tmp;
    }
}

/**
 * @see https://stackoverflow.com/a/11807179
 */
function convertToBytes(string $from, bool $caseSensitive = true): int {
    $units = ['B', 'kiB', 'MiB', 'GiB', 'TiB', 'PiB'];
    $fallbackUnits = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];

    if (!$caseSensitive) {
        $from = strtolower($from);
        $units = array_map('strtolower', $units);
        $fallbackUnits = array_map('strtolower', $fallbackUnits);
    }

    if (!preg_match('/^(\d+)\s*([a-z]*)$/i', trim($from), $matches)) {
        throw new \InvalidArgumentException('Not a valid size.');
    }

    $number = (float) $matches[1];
    $unit = $matches[2];

    if ($unit === '') {
        $unit = $caseSensitive ? 'B' : 'b';
    }

    if (in_array($unit, $units)) {
        $exponent = array_flip($units)[$unit];
    }
    elseif (in_array($unit, $fallbackUnits)) {
        $exponent = array_flip($fallbackUnits)[$unit];
    }
    else {
        throw new \InvalidArgumentException(sprintf('Invalid size unit "%s".', $unit));
    }

    return (int) ($number * (1024 ** $exponent));
}

/**
 * @see https://subinsb.com/convert-bytes-kb-mb-gb-php/
 */
function humanSize(int $sizeBytes, bool $translatable = false, string $format = '%d %s'): array|string {
    $base = log($sizeBytes) / log(1024);
    $suffix = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
    $baseIdx = (int) floor($base);

    $number = round(1024 ** ($base - floor($base)), 1);
    $unit = $suffix[$baseIdx];
    if ($translatable) {
        return [$number, $unit, 'filesize'];
    }

    return sprintf($format, $number, $unit);
}

/**
 * @param class-string $class
 * @throws InvalidTypeException
 */
function isObjectArrayOfType(array $array, string $class): bool {
    if (!is_string($class)) {
        throw new \InvalidArgumentException(__FUNCTION__ . ' expects parameter 2 to be string, null given');
    }

    foreach ($array as $item) {
        if (!is_a($item, $class)) {
            return false;
        }
    }

    return true;
}

/**
 * @param class-string $class
 * @throws InvalidTypeException
 */
function assertObjectArrayOfType(array $array, string $class): void {
    if (!is_string($class)) {
        throw new \InvalidArgumentException(__FUNCTION__ . ' expects parameter 2 to be string, null given');
    }

    foreach ($array as $item) {
        if (!is_a($item, $class)) {
            throw new InvalidTypeException(sprintf(
                'Must be an array of %s (found %s).',
                $class,
                $item::class
            ));
        }
    }
}

/**
 * Encode $string into a URL-safe base64 encoded string
 *
 * @see https://en.wikipedia.org/wiki/Base64#RFC_4648
 */
function base64_url_encode(string $input): string {
    return strtr(base64_encode($input), '+/=', '-_.');
}

/**
 * Decode URL-safe base64 encoded string
 *
 * @see https://en.wikipedia.org/wiki/Base64#RFC_4648
 */
function base64_url_decode(string $input): string {
    if (false === ($string = base64_decode(strtr($input, '-_.', '+/=')))) {
        throw new \InvalidArgumentException('Invalid base64url string.');
    }

    return $string;
}

/**
 * Get the full trace of an exception, Java-style.
 */
function exception_full_trace(\Throwable $e): string {
    $trace = [];
    do {
        if ($trace !== []) {
            $trace[] = "\nCaused by:";
        }

        $trace[] = sprintf(
            "%s: %s in %s:%d\n%s",
            $e::class,
            $e->getMessage(),
            $e->getFile(),
            $e->getLine(),
            $e->getTraceAsString()
        );
    } while ($e = $e->getPrevious());

    return implode("\n", $trace);
}

/**
 * @param array|null $env
 */
function injectEnvVarsToHive(\Base $fw, ?array $env = null): void {
    $env ??= getenv();
    foreach ($env as $key => $value) {
        if (preg_match('/F3__(?<key>[\w\.]+)/', $key, $m)) {
            $fw->set(strtr($m['key'], ['__' => '.']), $value);
        }
    }
}

function idfy(mixed $input): string {
    return strtolower(trim((string) preg_replace('/[^a-z0-9-]+/i', '-', (string) $input), '-'));
}
