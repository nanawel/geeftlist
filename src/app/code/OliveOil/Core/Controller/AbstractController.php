<?php

namespace OliveOil\Core\Controller;


use Narrowspark\HttpStatus\Exception\NotFoundException;
use OliveOil\Core\Model\I18n;
use OliveOil\Core\Model\MagicObject;
use OliveOil\Core\Model\Traits\I18nTrait;

abstract class AbstractController implements ControllerInterface
{
    use I18nTrait;

    protected \Base $fw;

    protected \OliveOil\Core\Service\RegistryInterface $registry;

    protected \OliveOil\Core\Model\AppInterface $app;

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \OliveOil\Core\Service\RouteInterface $routeService;

    protected \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \OliveOil\Core\Service\Log $logService;

    protected \Psr\Log\LoggerInterface $logger;

    public function __construct(Context $context) {
        $this->fw = $context->getFw();
        $this->app = $context->getApp();
        $this->registry = $context->getRegistry();
        $this->appConfig = $context->getAppConfig();
        $this->routeService = $context->getRouteService();
        $this->requestManager = $context->getRequestManager();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->i18n = $context->getI18n();
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->logService = $context->getLogService();
        $this->logger = $context->getLogService()->getLoggerForClass($this);

        $this->app->setup();
        $this->init();
    }

    protected function init() {
        // to be overridden
    }

    /**
     * @param string[] $args
     */
    public function beforeRoute(\Base $fw, array $args = []): bool {
        $this->requestManager->reset();
        $this->initRequestData($args); // TODO: Handle with Request instead

        $this->registry->set('current_controller', $this);

        $transportObject = new MagicObject([
            'args'    => $args,
            'proceed' => true
        ]);
        $this->eventManager->trigger(
            [
                $this->getFullActionName() . '::beforeRoute',
                'action::beforeRoute'
            ],
            $this,
            [
                'controller' => $this,
                'transport_object' => $transportObject,
            ]
        );
        return (bool) $transportObject->getProceed();
    }

    /**
     * @param string[] $args
     */
    public function afterRoute(\Base $fw, array $args = []): bool {
        $transportObject = new MagicObject([
            'args'    => $args,
            'proceed' => true
        ]);
        $this->eventManager->trigger(
            [
                $this->getFullActionName() . '::afterRoute',
                'action::afterRoute'
            ],
            $this,
            [
                'controller' => $this,
                'transport_object' => $transportObject
            ]
        );
        if (! $transportObject->getProceed()) {
            return false;
        }

        if ($this->registry->get('current_controller') === $this) {
            $this->registry->clear('current_controller');
        }

        if ($responseCode = $this->getResponse()->getCode()) {
            http_response_code($responseCode);
        }

        return true;
    }

    /**
     * @param string $route
     * @param array|null $args
     * @param array|null $headers
     */
    public function forward($route, array $args = null, array $headers = null, mixed $body = null): void {
        $this->routeService->forward($route, $args, $headers, $body);
    }

    protected function initRequestData(array $args): static {
        $routeParts = $this->routeService->parseRouteArgs($args);
        $this->getRequest()->initRoute(
            $routeParts['controller'],
            $routeParts['action'],
            $routeParts['route_params'],
            $args
        );

        return $this;
    }

    protected function info($message, array $vars = null, $options = []): static {
        return $this;
    }

    protected function success($message, array $vars = null, $options = []): static {
        return $this;
    }

    protected function warn($message, array $vars = null, $options = []): static {
        return $this;
    }

    protected function error($message, array $vars = null, $options = []): static {
        return $this;
    }

    /**
     * @return string
     */
    public function getControllerName() {
        return $this->getRequest()->getControllerName();
    }

    /**
     * @return string
     */
    public function getActionName() {
        return $this->getRequest()->getActionName();
    }

    public function getFullActionName(): string {
        return $this->getControllerName() . '/' . $this->getActionName();
    }

    /**
     * @param string $param
     * @return $this
     */
    protected function setRequestParam($param, mixed $value): static {
        $this->getRequest()->setRequestParam($param, $value);

        return $this;
    }

    protected function getRequestParam($param) {
        return $this->getRequest()->getRequestParam($param);
    }

    /**
     * @param string|null $param
     * @return array|mixed|null
     */
    public function getRequestQuery($param = null) {
        return $this->getRequest()->getQueryParam($param);
    }

    /**
     * @return \OliveOil\Core\Model\Http\RequestInterface
     */
    public function getRequest() {
        return $this->requestManager->getRequest();
    }

    /**
     * @return \OliveOil\Core\Model\Http\ResponseInterface
     */
    public function getResponse() {
        return $this->requestManager->getResponse();
    }

    /**
     * Notice: Needed for \OliveOil\Core\Model\Traits\I18nTrait
     *
     * @return I18n
     */
    public function getI18n() {
        return $this->i18n;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger($name = null) {
        if ($name === null) {
            return $this->logger;
        }

        return $this->logService->getLoggerByName($name);
    }

    public function __call(string $name, array $arguments) {
        if (strcasecmp(substr($name, -6), 'action') === 0) {
            // Tried to call an undefined action, return 404 (instead of 405 from standard)
            $this->getLogger()->notice(
                'Invalid action requested "' . $name . '" for controller "' . static::class . '".'
            );

            throw new NotFoundException();
        }
    }
}
