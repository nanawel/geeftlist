<?php

namespace OliveOil\Core\Controller;

interface ControllerInterface
{
    /**
     * @param string[] $args
     * @return bool|null
     */
    public function beforeRoute(\Base $fw, array $args = []);

    /**
     * @param string[] $args
     * @return bool|null
     */
    public function afterRoute(\Base $fw, array $args = []);
}
