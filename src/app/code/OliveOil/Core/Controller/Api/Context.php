<?php

namespace OliveOil\Core\Controller\Api;


class Context extends \OliveOil\Core\Controller\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\Api\ConfigInterface $apiConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        protected \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Helper\Data\Factory $helperDataFactory
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $logService
        );
    }

    public function getApiConfig(): \OliveOil\Core\Model\Api\ConfigInterface {
        return $this->apiConfig;
    }

    public function getSessionManager(): \OliveOil\Core\Service\Session\Manager {
        return $this->sessionManager;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getResponseRedirect(): \OliveOil\Core\Service\Response\Redirect {
        return $this->responseRedirect;
    }

    public function getHelperDataFactory(): \OliveOil\Core\Helper\Data\Factory {
        return $this->helperDataFactory;
    }
}
