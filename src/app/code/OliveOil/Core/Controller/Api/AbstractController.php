<?php

namespace OliveOil\Core\Controller\Api;


abstract class AbstractController extends \OliveOil\Core\Controller\AbstractController
{
    public const CACHE_REQUEST_OUTPUT_PREFIX     = 'API_REQUEST_';

    public const CACHE_REQUEST_OUTPUT_TTL        = 120;

    public const CACHE_DEFAULT_TTL               = 600;

    protected \OliveOil\Core\Model\Api\ConfigInterface $apiConfig;

    protected \OliveOil\Core\Service\Session\Manager $sessionManager;

    protected \OliveOil\Core\Service\Response\Redirect $responseRedirect;

    protected \OliveOil\Core\Helper\Data\Factory $helperDataFactory;

    protected $post = [];

    public function __construct(Context $context) {
        $this->apiConfig = $context->getApiConfig();
        $this->sessionManager = $context->getSessionManager();
        $this->responseRedirect = $context->getResponseRedirect();
        $this->helperDataFactory = $context->getHelperDataFactory();
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    final public function beforeRoute(\Base $fw, array $args = []): bool {
        if (parent::beforeRoute($fw, $args) === false) {
            return false;
        }

        return $this->onBeforeRoute($fw, $args) !== false;
    }

    /**
     * @inheritDoc
     */
    final public function afterRoute(\Base $fw, array $args = []): bool {
        if (parent::afterRoute($fw, $args) === false) {
            return false;
        }

        return $this->onAfterRoute($fw, $args) !== false;
    }

    /**
     * @param string[] $args
     * @return bool|null False to stop request processing.
     */
    protected function onBeforeRoute(\Base $fw, array $args = []) {
        // to be overridden

        return null;
    }

    /**
     * @param string[] $args
     * @return bool|null False to stop request processing.
     */
    protected function onAfterRoute(\Base $fw, array $args = []) {
        // to be overridden

        return null;
    }

    /**
     * @param string|null $name
     * @return \OliveOil\Core\Model\SessionInterface
     */
    public function getSession($name = null) {
        return $this->sessionManager->getSession($name);
    }

    protected function getRequestCacheKey() {
        return $this->fw->hash(implode('|', $this->getRequestCacheKeyInfo()));
    }

    protected function getRequestCacheKeyInfo(): array {
        return [
            $this->fw->get('REALM'),
            $this->getSession()->getLocale()
        ];
    }

    protected function saveCache($key, $data, $ttl = self::CACHE_DEFAULT_TTL, $strictFullActionName = true): static {
        // Not supported

        return $this;
    }

    protected function loadCache($key, $strictFullActionName = true) {
        // Not supported

        return null;
    }

    public function getUrl($path, ?array $params = null) {
        return $this->urlBuilder->getUrl($path, $params);
    }

    /**
     * @param string $path
     */
    public function autocompleteUrlPath($path): string {
        $parts = explode('/', trim($path, '/'));
        if (!isset($parts[0])) {
            $parts[0] = 'index';
        }
        elseif ($parts[0] === '*') {
            $parts[0] = $this->getControllerName();
        }

        if (!isset($parts[1])) {
            $parts[1] = 'index';
        }
        elseif (isset($parts[1]) && $parts[1] === '*') {
            $parts[1] = $this->getActionName();
        }

        if (count($parts) == 3 && $parts[2] === '*') {
            $parts[2] = $this->rawRequestParams['*'] ?? '';
        }

        return implode('/', $parts);
    }

    protected function info($message, array $vars = null, $options = []): static {
        $this->getSession()->addInfoMessage($message, $vars, $options);
        return $this;
    }

    protected function success($message, array $vars = null, $options = []): static {
        $this->getSession()->addSuccessMessage($message, $vars, $options);
        return $this;
    }

    protected function warn($message, array $vars = null, $options = []): static {
        $this->getSession()->addWarningMessage($message, $vars, $options);
        return $this;
    }

    protected function error($message, array $vars = null, $options = []): static {
        $this->getSession()->addErrorMessage($message, $vars, $options);
        return $this;
    }

    protected function getReferer() {
        return $this->urlBuilder->getReferer();
    }

    public function redirectReferer(?string $fragment = null): static {
        $this->responseRedirect->redirectReferer($fragment);

        return $this;
    }

    /**
     * @param string|null $param
     * @return array|mixed|null
     */
    public function getRequestQuery($param = null) {
        return $this->urlBuilder->getRequestQuery($param);
    }

    public function reroute($path, $params = [], $permanent = false): static {
        $this->responseRedirect->reroute($path, $params, $permanent);

        return $this;
    }

    public function rerouteUrl($url, $permanent = false): static {
        $this->responseRedirect->rerouteUrl($url, $permanent);

        return $this;
    }
}
