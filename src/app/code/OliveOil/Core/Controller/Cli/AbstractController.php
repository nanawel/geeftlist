<?php
namespace OliveOil\Core\Controller\Cli;


abstract class AbstractController extends \OliveOil\Core\Controller\AbstractController
{
    public const RETURN_SUCCESS = 0;

    public const RETURN_YES = 0;

    public const RETURN_NO = 200;

    public const RETURN_MISSING_ARGUMENT = 250;

    public const RETURN_UNDEFINED_FAILURE = 254;

    /**
     * @param string[] $args
     */
    public function beforeRoute(\Base $fw, array $args = []): bool {
        // Turn off output buffering in CLI
        ob_end_flush();

        if (parent::beforeRoute($fw, $args) === false) {
            return false;
        }

        // Override DEBUG if present in environment
        if (getenv('DEBUG') && is_numeric(getenv('DEBUG'))) {
            $fw->set('DEBUG', getenv('DEBUG'));
        }

        return true;
    }

    public function print($string): void {
        fwrite(STDOUT, (string) $string);
    }

    public function printnl($string): void {
        fwrite(STDOUT, print_r($string, true) . "\n");
    }

    public function errnl($string): void {
        fwrite(STDERR, print_r($string, true) . "\n");
    }

    public function read($prompt = null): string|false {
        return readline($prompt);
    }

    protected function info($message, array $vars = null, $options = []): static {
        if (is_string($message)) {
            $this->printnl($this->i18n->tr($message, $vars));
        }
        else {
            $this->printnl($message);
        }

        return $this;
    }

    protected function success($message, array $vars = null, $options = []): static {
        return $this->info($message, $vars, $options);
    }

    protected function warn($message, array $vars = null, $options = []): static {
        return $this->info($message, $vars, $options);
    }

    protected function error($message, array $vars = null, $options = []): static {
        if (is_string($message)) {
            $this->errnl($this->i18n->tr($message, $vars));
        }
        else {
            $this->errnl($message);
        }

        return $this;
    }
}
