<?php
namespace OliveOil\Core\Controller\Cli;

class DefaultController extends AbstractController
{
    public function indexAction(\Base $fw, $args): void {
        $this->printnl('Available actions:');

        $this->printnl('NOT IMPLEMENTED...');
    }
}
