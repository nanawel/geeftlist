<?php

namespace OliveOil\Core\Controller\Http;


class Context extends \OliveOil\Core\Controller\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        protected \OliveOil\Core\Service\View $view,
        protected \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        protected \OliveOil\Core\Model\View\Breadcrumbs $breadcrumbs,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\Security\Csrf $csrfService
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $logService
        );
    }

    public function getSessionManager(): \OliveOil\Core\Service\Session\Manager {
        return $this->sessionManager;
    }

    public function getView(): \OliveOil\Core\Service\View {
        return $this->view;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getResponseRedirect(): \OliveOil\Core\Service\Response\Redirect {
        return $this->responseRedirect;
    }

    public function getBreadcrumbs(): \OliveOil\Core\Model\View\Breadcrumbs {
        return $this->breadcrumbs;
    }

    public function getCsrfService(): \OliveOil\Core\Service\Security\Csrf {
        return $this->csrfService;
    }
}
