<?php

namespace OliveOil\Core\Controller\Http;


use OliveOil\Core\Exception\Security\CsrfException;

/**
 * Class AbstractController
 */
abstract class AbstractController extends \OliveOil\Core\Controller\AbstractController
{
    public const CACHE_DEFAULT_TTL = 600;

    protected \OliveOil\Core\Service\Session\Manager $sessionManager;

    protected \OliveOil\Core\Service\View $view;

    protected \OliveOil\Core\Service\Response\Redirect $responseRedirect;

    protected \OliveOil\Core\Model\View\Breadcrumbs $breadcrumbs;

    protected \OliveOil\Core\Service\Security\Csrf $csrfService;

    /** @var array */
    protected $post = [];

    public function __construct(Context $context) {
        $this->sessionManager = $context->getSessionManager();
        $this->view = $context->getView();
        $this->responseRedirect = $context->getResponseRedirect();
        $this->breadcrumbs = $context->getBreadcrumbs();
        $this->csrfService = $context->getCsrfService();
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    final public function beforeRoute(\Base $fw, array $args = []): bool {
        $this->breadcrumbs->resetCrumbs();

        if (parent::beforeRoute($fw, $args) === false) {
            return false;
        }

        $this->initLayout();
        $this->eventManager->trigger(
            [
                'action::initLayout::after',
                $this->getFullActionName() . '::initLayout::after'
            ],
            $this,
            ['layout' => $this->view->getLayout()]
        );

        $this->setPost($this->fw->get('POST'));

        if ($this->onBeforeRoute($fw, $args) === false) {
            return false;
        }

        $beforeMethod = $this->getActionName() . 'ActionBefore';
        return !(method_exists($this, $beforeMethod) && $this->$beforeMethod($fw, $args) === false);
    }

    protected function initLayout(): static {
        // By default, reset entirely the layout
        $this->view->getLayout()
            ->clear()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\HtmlRootContainer\Page::class,
                'template' => 'layout.phtml'
            ])
            ->setContentWrapperConfig([
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => 'page/1column.phtml'
            ])
            ->setBlockConfig('content', [
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => str_replace(['_', '/'], DIRECTORY_SEPARATOR, $this->getFullActionName()) . '.phtml'
            ])
            ->setBlockConfig('loading_overlay', [
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => 'page/loading_overlay.phtml'
            ])
            ->setBlockConfig('messages', [
                'type'     => \OliveOil\Core\Block\SessionMessages::class,
                'template' => 'messages.phtml'
            ]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    final public function afterRoute(\Base $fw, array $args = []): bool {
        if (parent::afterRoute($fw, $args) === false) {
            return false;
        }

        if ($this->onAfterRoute($fw, $args) === false) {
            return false;
        }

        $this->eventManager->trigger(
            [
                'action::beforeRender',
                $this->getFullActionName() . '::beforeRender'
            ],
            $this,
            $args
        );
        if ($this->beforeRender($fw, $args) === false) {
            return false;
        }

        if ($this->render() === false) {
            return false;
        }

        if ($this->afterRender($fw, $args) === false) {
            return false;
        }

        $this->eventManager->trigger(
            [
                $this->getFullActionName() . '::afterRender',
                'action::afterRender'
            ],
            $this,
            $args
        );

        return true;
    }

    /**
     * @param string[] $args
     * @return bool|null False to stop request processing.
     * @throws \Throwable
     */
    protected function onBeforeRoute(\Base $fw, array $args = []) {
        // to be overridden

        return null;
    }

    /**
     * @param string[] $args
     * @return bool|null False to stop request processing.
     * @throws \Throwable
     */
    protected function onAfterRoute(\Base $fw, array $args = []) {
        // to be overridden

        return null;
    }

    /**
     * @param null|string $name
     * @return \OliveOil\Core\Model\Session\HttpInterface
     */
    public function getSession(?string $name = null) {
        /** @var \OliveOil\Core\Model\Session\HttpInterface $httpSession */
        $httpSession = $this->sessionManager->getSession($name);

        return $httpSession;
    }

    /**
     * @param string|array $key
     * @param mixed|null $value
     * @return $this
     */
    protected function setPost($key, $value = null): static {
        if (is_array($key)) {
            $this->post = $key;
        } else {
            $this->post[$key] = $value;
        }

        return $this;
    }

    /**
     * @param string|null $key
     * @return array|mixed|null
     */
    protected function getPost($key = null) {
        if ($key === null) {
            return $this->post;
        }

        return $this->post[$key] ?? null;
    }

    /**
     * @return string
     */
    public function getPageTitle() {
        return $this->fw->get('PAGE_TITLE');
    }

    /**
     * @param string $title
     */
    public function setPageTitle($title): static {
        $this->fw->set('PAGE_TITLE', $title);

        return $this;
    }

    /**
     *
     * @deprecated
     * @param string $template
     */
    public function setPageTemplate($template): static {
        $this->view->getLayout()
            ->setPageTemplate($template);

        return $this;
    }

    /**
     * @deprecated
     * @param string $config
     */
    public function setPageConfig($config): static {
        $this->view->getLayout()
            ->setPageConfig($config);

        return $this;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getPageTemplate() {
        return $this->view->getLayout()
            ->getPageTemplate();
    }

    /**
     * @param string[] $args
     * @return $this
     */
    protected function beforeRender(\Base $fw, array $args = []): static {
        if ($cspHeader = trim($this->appConfig->getValue('CSP_HEADER', ''))) {
            header("Content-Security-Policy: $cspHeader");
            $this->getLogger('security')->debug("Set response header Content-Security-Policy: $cspHeader");
        }
        if ($cspHeader = trim($this->appConfig->getValue('CSP_REPORT_ONLY_HEADER', ''))) {
            header("Content-Security-Policy-Report-Only: $cspHeader");
            $this->getLogger('security')->debug("Set response header Content-Security-Policy-Report-Only: $cspHeader");
        }

        return $this;
    }

    /**
     * @param string[] $args
     * @return $this
     */
    protected function afterRender(\Base $fw, array $args = []): static {
        $this->getSession()->setData('last_url', $fw->get('REALM'));

        return $this;
    }

    protected function render(): bool {
        if ($this->fw->get('QUIET')) {
            $this->getLogger()->warning('QUIET is on! Output may not be generated.');
        }

        ob_start();
        echo $this->view->renderPage();
        echo ob_get_clean();

        return true;
    }

    public function getUrl($path, array $params = []) {
        return $this->urlBuilder->getUrl($path, $params);
    }

    protected function info($message, array $vars = null, $options = []): static {
        $this->getSession()->addInfoMessage($message, $vars, $options);

        return $this;
    }

    protected function success($message, array $vars = null, $options = []): static {
        $this->getSession()->addSuccessMessage($message, $vars, $options);

        return $this;
    }

    protected function warn($message, array $vars = null, $options = []): static {
        $this->getSession()->addWarningMessage($message, $vars, $options);

        return $this;
    }

    protected function error($message, array $vars = null, $options = []): static {
        $this->getSession()->addErrorMessage($message, $vars, $options);

        return $this;
    }

    protected function getReferer() {
        return $this->urlBuilder->getReferer();
    }

    public function redirectReferer(?string $fragment = null): void {
        $this->responseRedirect->redirectReferer($fragment);
    }

    /**
     * @param string|null $param
     * @return array|mixed|null
     */
    public function getRequestQuery($param = null) {
        return $this->urlBuilder->getRequestQuery($param);
    }

    public function reroute($path, $params = [], $permanent = false): void {
        $this->responseRedirect->reroute($path, $params, $permanent);
    }

    public function rerouteUrl($url, $permanent = false): void {
        $this->responseRedirect->rerouteUrl($url, $permanent);
    }

    public function escapeHtml($string) {
        return $this->view->esc($string);
    }

    protected function checkFormPost(): static {
        $this->checkCsrf(['POST']);

        return $this;
    }

    protected function checkCsrf($hives = ['POST', 'GET']): static {
        try {
            $this->csrfService->validate($hives);
        }
        catch (CsrfException $csrfException) {
            $this->getSession()->addErrorMessage($csrfException->getMessage());
            $this->redirectReferer();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getMimetype() {
        return $this->getRequest()->getMimetype();
    }

    /**
     * @return \OliveOil\Core\Model\Http\RequestInterface
     */
    public function getRequest() {
        return $this->requestManager->getRequest();
    }

    /**
     * @return \OliveOil\Core\Model\Http\ResponseInterface
     */
    public function getResponse() {
        return $this->requestManager->getResponse();
    }
}
