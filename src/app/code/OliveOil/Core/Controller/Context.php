<?php

namespace OliveOil\Core\Controller;


class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\RegistryInterface $registry,
        protected \OliveOil\Core\Model\AppInterface $app,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\RouteInterface $routeService,
        protected \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Log $logService
    ) {
    }

    public function getApp(): \OliveOil\Core\Model\AppInterface {
        return $this->app;
    }

    public function getRegistry(): \OliveOil\Core\Service\RegistryInterface {
        return $this->registry;
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getRouteService(): \OliveOil\Core\Service\RouteInterface {
        return $this->routeService;
    }

    public function getRequestManager(): \OliveOil\Core\Service\Http\RequestManagerInterface {
        return $this->requestManager;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }
}
