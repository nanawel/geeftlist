<?php

namespace OliveOil\Core\Controller;


interface AutocompleteAwareController
{
    /**
     * @param string $path
     * @return string
     */
    public function autocompleteUrlPath($path);
}
