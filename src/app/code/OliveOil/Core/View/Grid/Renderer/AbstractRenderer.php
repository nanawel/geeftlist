<?php

namespace OliveOil\Core\View\Grid\Renderer;


use OliveOil\Core\Model\Traits\ConfigurableTrait;
use OliveOil\Core\View\Grid\RendererInterface;

abstract class AbstractRenderer implements RendererInterface
{
    use ConfigurableTrait;

    /** @var string[] */
    protected array $renderableColumns = [];

    /** @var string[]|null */
    protected ?array $visibleColumns;

    public function __construct(
        protected \OliveOil\Core\Helper\Data\Factory $dataHelperFactory,
        protected \OliveOil\Core\Service\View $viewService,
        protected \OliveOil\Core\Model\I18n $i18n
    ) {
    }

    /**
     * @return string
     */
    public function getDataTableConfigJson() {
        return $this->getDataHelperFactory()->resolveGet('json')
            ->encode($this->getDataTableConfig());
    }

    /**
     * @param string $column
     */
    public function isColumnVisible($column): bool {
        $visibleColumns = $this->getVisibleColumns();

        return empty($visibleColumns) || in_array($column, $visibleColumns);
    }

    /**
     * @return string[]
     */
    public function getVisibleColumns() {
        if ($this->visibleColumns === null) {
            $visibleColumns = $this->getConfiguration('visible_columns', $this->getRenderableColumns());
            $hiddenColumns = $this->getConfiguration('hidden_columns', []);

            $this->visibleColumns = array_diff($visibleColumns, $hiddenColumns);
        }

        return $this->visibleColumns;
    }

    protected function onConfigurationChange(array $config) {
        $this->visibleColumns = null;
    }

    public function getDataTableConfig(): array {
        return [
            'responsive' => true
        ];
    }

    public function renderEntityCollection(\OliveOil\Core\Model\ResourceModel\Iface\Collection $collection): array {
        $rows = [];
        foreach ($collection as $item) {
            $rows[] = $this->filterData($this->renderEntityRow($item), $item);
        }

        return $rows;
    }

    protected function filterData(array $entityData, \OliveOil\Core\Model\AbstractModel $entity) {
        if (! empty($renderableColumns = $this->getRenderableColumns())) {
            return \OliveOil\array_mask(
                $entityData,
                $renderableColumns
            );
        }

        return $entityData;
    }

    public function getDataHelperFactory(): \OliveOil\Core\Helper\Data\Factory {
        return $this->dataHelperFactory;
    }

    public function getViewService(): \OliveOil\Core\Service\View {
        return $this->viewService;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    /**
     * @return string[]
     */
    public function getRenderableColumns() {
        return $this->renderableColumns;
    }
}
