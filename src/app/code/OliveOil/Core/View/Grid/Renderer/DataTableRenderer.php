<?php

namespace OliveOil\Core\View\Grid\Renderer;


abstract class DataTableRenderer extends AbstractRenderer
{
    public const SELECTED_GRID_ROWS_NAME = 'selected_grid_rows';

    public function renderEntityCollection(\OliveOil\Core\Model\ResourceModel\Iface\Collection $collection): array {
        return [
            'items' => parent::renderEntityCollection($collection)
        ];
    }

    public function getDataTableConfig(): array {
        $config = parent::getDataTableConfig();
        if ($this->getConfiguration('has_selectable_rows')) {
            $config['columns'][0] = $this->getDataTableSelectionColumn();
        }

        // Convert "order" map to DataTable's
        // https://datatables.net/examples/basic_init/table_sorting.html
        if ($sortOrders = $this->getConfiguration('order')) {
            $visibleColumns = $this->getVisibleColumns();
            foreach ($sortOrders as $field => $dir) {
                $config['order'][] = [array_search($field, $visibleColumns, true), $dir];
            }
        }

        return $config;
    }

    public function getDataTableSelectionColumn(): array {
        return [
            'data' => static::SELECTED_GRID_ROWS_NAME,
            'visible' => $this->isColumnVisible(static::SELECTED_GRID_ROWS_NAME),
            'orderable' => false,
            'className' => 'col-selection',
            'responsivePriority' => 10
        ];
    }

    public function getVisibleColumns() {
        $visibleColumns = parent::getVisibleColumns();
        if (
            $this->getConfiguration('has_selectable_rows')
            && !in_array(static::SELECTED_GRID_ROWS_NAME, $visibleColumns)
        ) {
            $visibleColumns[] = static::SELECTED_GRID_ROWS_NAME;
        }

        return $visibleColumns;
    }

    public function getRenderableColumns() {
        $renderableColumns = parent::getRenderableColumns();
        if (
            $this->getConfiguration('has_selectable_rows')
            && !in_array(static::SELECTED_GRID_ROWS_NAME, $renderableColumns)
        ) {
            $renderableColumns[] = static::SELECTED_GRID_ROWS_NAME;
        }

        return $renderableColumns;
    }
}
