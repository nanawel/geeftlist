<?php

namespace OliveOil\Core\View\Grid;


use OliveOil\Core\Iface\Configurable;

interface RendererInterface extends Configurable
{
    /**
     * @return array
     */
    public function getTableBlockConfig();

    /**
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity);

    /**
     * @return array
     */
    public function renderEntityCollection(\OliveOil\Core\Model\ResourceModel\Iface\Collection $collection);
}
