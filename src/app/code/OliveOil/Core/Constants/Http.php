<?php


namespace OliveOil\Core\Constants;


interface Http
{
    public const VERB_OPTIONS = 'OPTIONS';

    public const VERB_GET     = 'GET';

    public const VERB_HEAD    = 'HEAD';

    public const VERB_PATCH   = 'PATCH';

    public const VERB_POST    = 'POST';

    public const VERB_PUT     = 'PUT';

    public const VERB_DELETE  = 'DELETE';

    public const VERBS = [
        self::VERB_OPTIONS,
        self::VERB_GET,
        self::VERB_HEAD,
        self::VERB_PATCH,
        self::VERB_POST,
        self::VERB_PUT,
        self::VERB_DELETE,
    ];
}
