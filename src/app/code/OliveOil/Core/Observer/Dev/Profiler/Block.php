<?php

namespace OliveOil\Core\Observer\Dev\Profiler;

use Laminas\EventManager\Event;

class Block
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Util\Profiler $profiler)
    {
    }

    public function profileRenderingStart(Event $ev): void {
        $blockName = $ev->getParam('block_name');
        $blockId = $ev->getParam('uniqid');

        $this->profiler->start(
            $blockId,
            Constants::TYPE_VIEW_BLOCK,
            [
            'name' => $blockName
            ]
        );
    }

    public function profileRenderingEnd(Event $ev): void {
        $blockId = $ev->getParam('uniqid');

        $this->profiler->end($blockId);
    }
}
