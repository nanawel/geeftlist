<?php

namespace OliveOil\Core\Observer\Dev\Profiler;

class Event
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Util\Profiler $profiler)
    {
    }

    public function profileEventStart(\Laminas\EventManager\Event $ev): void {
        $event = $ev->getParam('event');

        $this->profiler->start(
            $ev->getParam('id'),
            Constants::TYPE_EVENT,
            [
            'event' => $event,
            'target_class' => $ev->getParam('target_class')
            ]
        );
    }

    public function profileEventEnd(\Laminas\EventManager\Event $ev): void {
        $this->profiler->end($ev->getParam('id'));
    }
}
