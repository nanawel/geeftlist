<?php

namespace OliveOil\Core\Observer\Dev\Profiler;

use Laminas\EventManager\Event;
use OliveOil\Core\Controller\AbstractController;

class HttpController
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Util\Profiler $profiler)
    {
    }

    public function profileActionStart(Event $ev): void {
        /** @var AbstractController $controller */
        $controller = $ev->getTarget();

        $this->profiler->start(
            $this->getControllerUniqId($controller),
            Constants::TYPE_CONTROLLER_ACTION,
            [
            'class' => $controller::class,
            'action' => $controller->getFullActionName(),
            'query' => $controller->getRequestQuery()
            ]
        );
    }

    public function profileActionEnd(Event $ev): void {
        /** @var AbstractController $controller */
        $controller = $ev->getTarget();

        $this->profiler->end($this->getControllerUniqId($controller));
    }

    protected function getControllerUniqId(AbstractController $controller): string {
        return sprintf('controller_%s', spl_object_hash($controller));
    }
}
