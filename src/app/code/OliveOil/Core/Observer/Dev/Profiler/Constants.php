<?php


namespace OliveOil\Core\Observer\Dev\Profiler;


interface Constants
{
    public const TYPE_DB                = 'db';

    public const TYPE_VIEW_BLOCK        = 'view_block';

    public const TYPE_EVENT             = 'event';

    public const TYPE_CONTROLLER_ACTION = 'controller_action';

    public const TYPE_UNKNOWN           = 'unknown';
}
