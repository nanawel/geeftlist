<?php

namespace OliveOil\Core\Observer\Dev\Profiler;

use Laminas\Db\Adapter\Profiler\Profiler;
use Laminas\EventManager\Event;

class Db
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Util\Profiler $profiler, protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection)
    {
    }

    public function saveProfilerResults(Event $ev): void {
        /** @var Profiler $profiler */
        $profiler = $this->connection->getAdapter()->getProfiler();
        if ($profiler) {
            $entries = [];
            foreach ($profiler->getProfiles() as $profile) {
                $entries[] = [
                    'time'      => $profile['elapse'],
                    'startTime' => $profile['start'],
                    'endTime'   => $profile['end'],
                    'userData'  => $profile,
                ];
            }

            $this->profiler->saveProfilingEntries($entries, Constants::TYPE_DB);
        }
    }
}
