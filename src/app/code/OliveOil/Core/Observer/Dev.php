<?php

namespace OliveOil\Core\Observer;

use Laminas\EventManager\Event;
use OliveOil\Core\Exception\DebugException;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;

/**
 * Class Dev
 */
class Dev
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        protected \Base $fw,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function dbCollectionCheckItemsCardinality(Event $ev): void {
        /** @var AbstractCollection $collection */
        $collection = $ev->getTarget();
        if (is_string($idField = $collection->getIdField())) {
            $items = $ev->getParam('items');
            if (($expected = count(array_unique(array_column($items, $idField)))) !== ($actual = count($items))) {
                $message = 'Item IDs are not unique in collection of type ' . $collection::class
                    . sprintf(' (expected: %d / actual: %d). Could this be a bug?', $expected, $actual);
                $this->getLogger()->warning($message);
                $this->getLogger()->warning('SQL: ' . $ev->getParam('sql'));
                $this->getLogger()->warning(new DebugException($message));
            }
        }
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }
}
