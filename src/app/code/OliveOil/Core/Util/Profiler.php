<?php

namespace OliveOil\Core\Util;


use OliveOil\Core\Observer\Dev\Profiler\Constants;

class Profiler
{
    /** @var array */
    protected $profilingData = [];

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected string $outputDir,
        protected bool $clearAfterSave = true
    ) {
    }

    /**
     * @param string $id
     * @return $this
     */
    public function start($id, $type = Constants::TYPE_UNKNOWN, array $data = []): static {
        if ($this->isEnabled($type)) {
            $this->profilingData[$id] = [
                'type'        => $type,
                'startTime'   => microtime(true),
                'userData'    => $data
            ];
        }

        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function end($id, array $data = []): static {
        if (isset($this->profilingData[$id]) && $this->isEnabled($this->profilingData[$id]['type'])) {
            $this->profilingData[$id]['time'] = ($this->profilingData[$id]['endTime'] = microtime(true))
                - $this->profilingData[$id]['startTime'];
            $this->profilingData[$id]['userData'] = array_merge($this->profilingData[$id]['userData'], $data);
            $this->saveProfilingEntry($this->profilingData[$id]);
            if ($this->clearAfterSave) {
                // Clear item
                unset($this->profilingData[$id]);
            }
        }

        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function saveProfilingEntry(array $entry, $type = Constants::TYPE_UNKNOWN): static {
        return $this->saveProfilingEntries([$entry], $type);
    }

    /**
     * @param array[] $entries
     * @return $this
     */
    public function saveProfilingEntries(array $entries, $type = Constants::TYPE_UNKNOWN): static {
        try {
            $fp = fopen($this->outputDir . '/profiler.log', 'wb+');
            foreach ($entries as &$entry) {
                if (!isset($entry['time'])) {
                    continue;
                }

                $entry += [
                    'type'       => $type,
                    'requestURI' => $this->fw->get('SERVER.REQUEST_URI'),
                ];
                $entry['userData'] = json_encode($entry['userData']);

                fputcsv($fp, $entry);
            }
        }
        catch (\Throwable) {
        }
        finally {
            if (isset($fp)) {
                fclose($fp);
            }
        }

        return $this;
    }

    /**
     * @param string|callable|null $typeOrFilterCallback
     */
    public function getProfilingData($typeOrFilterCallback = null) {
        if (is_string($typeOrFilterCallback)) {
            $typeOrFilterCallback = static fn($item) => $item['type'] ?? null === $typeOrFilterCallback;
        }

        if (is_callable($typeOrFilterCallback)) {
            return array_filter($this->profilingData, $typeOrFilterCallback);
        }

        return $this->profilingData;
    }

    /**
     * @param string|null $type
     */
    protected function isEnabled($type = null): bool {
        if ($this->appConfig->getValue('DEV.PROFILER_ENABLE')) {
            if ($type) {
                return (bool) $this->appConfig->getValue(sprintf('DEV.PROFILER_%s_ENABLE', $type));
            }

            return true;
        }

        return false;
    }

    public function setClearAfterSave(bool $clearAfterSave): static {
        $this->clearAfterSave = $clearAfterSave;

        return $this;
    }
}
