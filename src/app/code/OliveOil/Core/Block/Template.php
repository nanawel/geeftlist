<?php

namespace OliveOil\Core\Block;

class Template extends AbstractBlock
{
    public function setTemplate(string $template): static {
        return $this->setConfig('template', $template);
    }

    public function getTemplate(): ?string {
        return $this->getConfig('template');
    }

    final public function render(array $hive = null, ?int $ttl = 0): string {
        return $this->doRender($this->getFullRenderingHive($hive), $ttl);
    }

    protected function doRender(array $hive = null, ?int $ttl = 0): string {
        if (! $template = $this->getTemplate()) {
            //throw new InvalidBlockConfigurationException("Missing template for block '{$this->getName()}'.");
            $this->logger->warning(sprintf("Missing template for block '%s'", $this->getName()));
            return '';
        }

        return $this->view->render(
            $template,
            $this->getMimetype(),
            $hive,
            $ttl
        );
    }
}
