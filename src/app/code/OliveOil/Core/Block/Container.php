<?php

namespace OliveOil\Core\Block;


class Container extends AbstractBlock
{
    public function render(array $hive = null, $ttl = 0): string {
        $output = '';
        foreach ($this->getChildren() as $child) {
            $output .= $child->render($hive, $ttl);
        }

        return $output;
    }
}
