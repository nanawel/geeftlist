<?php

namespace OliveOil\Core\Block;

class Dialog extends Template
{
    public const TYPE_SUCCESS  = 'success';

    public const TYPE_QUESTION = 'question';

    public const TYPE_ERROR    = 'error';

    /** @var \DOMDocument */
    protected $domDocument;

    public function getTemplate(): ?string {
        if ($template = parent::getTemplate()) {
            return $template;
        }

        return 'common/dialog.phtml';
    }

    public function getCssClass(): string {
        return 'with-icon ' . $this->getConfig('dialog_type');
    }

    public function getTitle(): ?string {
        $title = $this->getConfig('title');
        if (empty($title)) {
            return null;
        }

        return $title;
    }

    public function getMessage(): ?array {
        $message = $this->getConfig('message');
        if (empty($message)) {
            return null;
        }

        return is_array($message) ? $message : [$message];
    }

    public function getContentBlocks(): ?array {
        $blocks = $this->getConfig('content_blocks');
        if (empty($blocks)) {
            return null;
        }

        return is_array($blocks) ? $blocks : [$blocks];
    }

    public function getButtons(): array {
        // TODO Handle sort order
        return $this->getConfig('buttons', []);
    }

    public function getButtonsHtml(): string {
        $html = [];
        foreach ($this->getButtons() as $buttonConfig) {
            if (!array_key_exists('rel', $buttonConfig)) {
                // Force noreferrer on dialog buttons
                $buttonConfig['rel'] = 'noreferrer';
            }

            $html[] = $this->getDomDocument()->saveHTML($this->getButtonElement($buttonConfig));
        }

        return implode("\n", $html);
    }

    protected function getButtonElement(array $buttonConfig): \DOMElement {
        $domElement = $this->getDomDocument()->createElement($buttonConfig['type'] ?? 'button');
        foreach ($buttonConfig as $attribute => $value) {
            if ($attribute !== 'type') {
                $domElement->setAttribute($attribute, $value);
            }
        }

        if (! empty($buttonConfig['text'])) {
            $domElement->textContent = $this->view->escapeHtml($buttonConfig['text']);
        }

        return $domElement;
    }

    protected function getDomDocument(): \DOMDocument {
        if (! $this->domDocument) {
            $this->domDocument = new \DOMDocument();
        }

        return $this->domDocument;
    }
}
