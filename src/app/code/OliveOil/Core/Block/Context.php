<?php

namespace OliveOil\Core\Block;


class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\View $view,
        protected \OliveOil\Core\Service\Design $design,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \OliveOil\Core\Helper\Output $outputHelper
    ) {
    }

    public function getFw(): \Base
    {
        return $this->fw;
    }

    public function getView(): \OliveOil\Core\Service\View
    {
        return $this->view;
    }

    public function getDesign(): \OliveOil\Core\Service\Design
    {
        return $this->design;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n
    {
        return $this->i18n;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface
    {
        return $this->urlBuilder;
    }

    public function getLogService(): \OliveOil\Core\Service\Log
    {
        return $this->logService;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface
    {
        return $this->coreFactory;
    }

    public function getOutputHelper(): \OliveOil\Core\Helper\Output
    {
        return $this->outputHelper;
    }
}
