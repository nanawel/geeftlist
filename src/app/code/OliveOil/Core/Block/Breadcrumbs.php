<?php

namespace OliveOil\Core\Block;

class Breadcrumbs extends Template
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Model\View\Breadcrumbs $breadcrumbs
    ) {
        parent::__construct($context);
    }

    public function getCrumbs() {
        return $this->breadcrumbs->getCrumbs();
    }
}
