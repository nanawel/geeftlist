<?php

namespace OliveOil\Core\Block;


/**
 * Needs to be improved or replaced with a more complete profiling/debugging system
 * => http://phpdebugbar.com/
 * => https://github.com/jimrubenstein/php-profiler
 */
class Profiler extends Template
{
    public function renderTab(string $tabCode): string {
        return print_r($this->getTabData($tabCode), true);
    }

    public function getTabData(string $tabCode) {
        return $this->fw->get('profiler_data.' . $tabCode);
    }
}
