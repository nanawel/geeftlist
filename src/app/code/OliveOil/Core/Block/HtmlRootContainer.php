<?php

namespace OliveOil\Core\Block;


class HtmlRootContainer extends Template
{
    /**
     * @param array|string $filepath
     * @param int $order
     * @return $this
     */
    public function addCss($filepath, $order = 0): static {
        $this->addItems($filepath, 'css', $order);

        return $this;
    }

    /**
     * @param array|string $content
     * @param int $order
     * @return $this
     */
    public function addInlineCss($content, $order = 0): static {
        $this->addItems($content, 'css_inline', $order);

        return $this;
    }

    /**
     * @param array|string $filepath
     * @param int $order
     * @return $this
     */
    public function addJs($filepath, $order = 0): static {
        $this->addItems($filepath, 'js', $order);

        return $this;
    }

    /**
     * @param array|string $content
     * @param int $order
     * @return $this
     */
    public function addInlineJs($content, $order = 0): static {
        $this->addItems($content, 'js_inline', $order);

        return $this;
    }

    /**
     * @param string|array $items
     * @param string $type
     * @param int $order
     * @return $this
     */
    public function addItems($items, $type, $order = 0): static {
        if (! is_array($items)) {
            $items = [$items];
        }

        if (! isset($this->config['externals'])) {
            $this->config['externals'] = [];
        }

        if (! isset($this->config['externals'][$type])) {
            $this->config['externals'][$type] = [];
        }

        foreach ($items as $item) {
            while (isset($this->config['externals'][$type][$order])) {
                ++$order;
            }

            $this->config['externals'][$type][$order] = $item;
        }

        return $this;
    }

    public function renderItems(): string {
        $html = '';
        if (isset($this->config['externals'])) {
            foreach ($this->config['externals'] as $type => $items) {
                ksort($items, SORT_NUMERIC);
                foreach ($items as $item) {
                    $html .= $this->renderItem($item, $type) . "\n";
                }
            }
        }

        return $html;
    }

    /**
     * @param string $item
     * @param string $type
     * @return string
     */
    protected function renderItem($item, $type) {
        $output = null;
        switch ($type) {
            case 'css':
                $output = '<link rel="stylesheet" href="' . $this->design->getCssUrl($item) . '" type="text/css" />';
                break;

            case 'css_inline':
                $output = <<<"EOCSS"
<style>
{$item}
</style>
EOCSS;
                break;

            case 'js':
                $output = '<script src="' . $this->design->getJsUrl($item) . '" type="application/javascript"></script>';
                break;

            case 'js_inline':
                $output = <<<"EOJS"
<script type="application/javascript">
//<![CDATA[
{$item}
//]]>
</script>
EOJS;
                break;

            case 'block':
                $output = $item instanceof BlockInterface ? $item->render() : '';
                break;

            default:
                $this->logger->warning(sprintf("Unknown item type '%s'", $type));
                break;
        }

        return $output;
    }

    /**
     * @return string
     */
    public function getArea() {
        return $this->getConfig('area');
    }
}
