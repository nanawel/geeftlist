<?php

namespace OliveOil\Core\Block\SpecialInput;


use OliveOil\Core\Block\AbstractBlock;

class CsrfToken extends AbstractBlock
{
    protected \OliveOil\Core\Service\Security\Csrf $csrfService;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        \OliveOil\Core\Service\Security\Csrf $csrfService
    ) {
        parent::__construct($context);
        $this->csrfService = $csrfService;
    }

    /**
     * @param array|null $hive
     * @param int $ttl
     */
    public function render(array $hive = null, $ttl = 0): string {
        return sprintf(
            '<input type="hidden" name="%s" value="%s">',
            $this->csrfService->getTokenName(),
            $this->csrfService->getTokenValue()
        );
    }
}
