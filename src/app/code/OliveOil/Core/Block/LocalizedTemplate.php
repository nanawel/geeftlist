<?php

namespace OliveOil\Core\Block;


class LocalizedTemplate extends Template
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Service\View\Template\Renderer\RendererInterface $templateRenderer
    ) {
        parent::__construct($context);
    }

    protected function doRender(?array $hive = null, ?int $ttl = 0): string {
        $output = $this->templateRenderer->render(
            $this->getTemplate(),
            ['_BLOCK' => $this] + $hive,
            null,
            [
                'i18n' => $this->view->getI18n(),
                'ttl' => $ttl
            ]
        );
        if (! $this->getConfig('skip_wrapper_div')) {
            $classes = [
                'loc-block',
                'loc-block-' . $this->getName()
            ];
            if ($cssClasses = $this->getConfig('css_classes')) {
                $classes = array_merge($classes, $cssClasses);
            }

            $output = sprintf(
                '<div class="%s">%s</div>',
                implode(' ', array_unique($classes)),
                $output
            );
        }

        return $output;
    }
}
