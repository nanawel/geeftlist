<?php

namespace OliveOil\Core\Block;


interface BlockInterface
{
    public const ANONYMOUS_BLOCK_ID_PREFIX = 'block_';

    public function init(string $name, ?array &$config = []): void;

    public function getId(): string;

    public function setConfig(string|array $key, mixed $value = null): static;

    public function extendConfig(array $config): static;

    public function getConfig(?string $key = null, mixed $default = null): mixed;

    public function getName(): string;

    public function render(?array $hive = null, int $ttl = 0): string;
}
