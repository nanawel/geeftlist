<?php

namespace OliveOil\Core\Block;


use OliveOil\Core\Model\Traits\I18nTrait;

abstract class AbstractBlock implements BlockInterface
{
    use I18nTrait;

    protected \Base $fw;

    protected \OliveOil\Core\Service\View $view;

    protected \OliveOil\Core\Service\Design $design;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    protected \OliveOil\Core\Helper\Output $outputHelper;

    protected \Psr\Log\LoggerInterface $logger;

    protected string $id;

    protected string $name;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected array $config = []
    ) {
        $this->fw = $context->getFw();
        $this->view = $context->getView();
        $this->design = $context->getDesign();
        $this->i18n = $context->getI18n();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->coreFactory = $context->getCoreFactory();
        $this->outputHelper = $context->getOutputHelper();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    /**
     * @inheritDoc
     */
    public function init(string $name, ?array &$config = []): void {
        $this->name = $name;

        // Little trick to preserve existing config that might have been injected through DI
        $existingConfig = $this->config;
        $this->config =& $config;
        $this->extendConfig($existingConfig, false);
    }

    public function setConfig(string|array $key, mixed $value = null): static {
        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->config[$k] = $v;
                $this->onConfigChange($k, $v);
            }
        }
        else {
            $this->config[$key] = $value;
            $this->onConfigChange($key, $value);
        }

        return $this;
    }

    public function extendConfig(array $config, bool $override = true): static {
        if ($override) {
            $this->config = array_merge_recursive($this->config, $config);
        }
        else {
            $this->config = array_merge_recursive($config, $this->config);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getConfig($key = null, $default = null): mixed {
        if ($key === null) {
            return $this->config;
        }

        return (isset($this->config[$key]) && $this->config[$key] !== null)
            ? $this->config[$key]
            : $default;
    }

    /**
     * Build a "full" hive for rendering, based on existing data and global View data
     *
     * @param array|null $hive
     */
    public function getFullRenderingHive($hive = []): array {
        $config = $this->getConfig();
        $hive = (is_array($hive) ? $hive : []) + (is_array($config) ? $config : []);
        $hive += $this->view->getData();
        $hive['_VIEW'] = $this->view;
        $hive['_BLOCK'] = $this;

        return $hive;
    }

    /**
     * @param string|array $key
     * @param mixed|null $value
     * @return $this
     */
    protected function onConfigChange(string|array $key, mixed $value): static {
        // to be overridden

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string {
        if ((string) $this->getConfig('id', '') === '') {
            $this->setConfig('id', $this->generateId());
        }

        return $this->getConfig('id');
    }

    protected function generateId(?string $prefix = self::ANONYMOUS_BLOCK_ID_PREFIX): string {
        return uniqid((string) $prefix);
    }

    public function getName(): string {
        return $this->name;
    }

    public function getMimetype(): string {
        return $this->getConfig('mimetype', 'text/html');
    }

    /**
     *
     * @return BlockInterface[]
     */
    public function getChildren(): array {
        return $this->view->getLayout()
            ->getBlockChildren($this->getName());
    }

    public function getView(): \OliveOil\Core\Service\View {
        return $this->view;
    }

    /**
     * Notice: Needed for \OliveOil\Core\Model\Traits\I18nTrait
     */
    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getDesign(): \OliveOil\Core\Service\Design {
        return $this->design;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }
}
