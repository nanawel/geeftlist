<?php

namespace OliveOil\Core\Block\HtmlRootContainer;


use OliveOil\Core\Block\HtmlRootContainer;

class Page extends HtmlRootContainer
{
    /**
     * @param string $class
     * @return $this
     */
    public function addBodyClass($class): static {
        $classes = explode(' ', (string) $this->getConfig('body_class'));
        $classes[] = $class;
        $this->setConfig('body_class', implode(' ', $classes));

        return $this;
    }

    /**
     * @param string $newClass
     */
    public function setBodyClass($newClass): static {
        $this->setConfig('body_class', $newClass);

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyClass() {
        return $this->getConfig('body_class');
    }
}
