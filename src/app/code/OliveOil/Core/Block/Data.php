<?php

namespace OliveOil\Core\Block;


class Data extends AbstractBlock
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Helper\Data\Factory $helperDataFactory
    ) {
        parent::__construct($context);
    }

    public function render(?array $hive = null, ?int $ttl = 0): string {
        if (! $mimetype = $this->getMimetype()) {
            $this->logger->warning(sprintf("Missing mimetype for block '%s'", $this->getName()));
            return '';
        }

        if (! preg_match('#^(.*?)(; .*)?$#', (string) $this->getMimetype(), $m)) {
            $this->logger->warning(sprintf("Invalid mimetype for block '%s'", $this->getName()));
            return '';
        }

        $data = $this->getConfig('data') ?? $hive['data'] ?? null;
        $params  = $this->getConfig('params') ?? $hive['params'] ?? [];

        /** @var \OliveOil\Core\Helper\Data\EncoderInterface $encoder */
        $encoder = $this->helperDataFactory->getFromCode($m[1]);

        $escape = $this->fw->get('ESCAPE');
        try {
            $this->fw->set('ESCAPE', false);

            return $this->view->render(
                'data/raw.phtml',
                $mimetype,
                ['DATA' => $encoder->encode($data, $params)],
                $ttl
            );
        }
        finally {
            // Revert ESCAPE configuration
            $this->fw->set('ESCAPE', $escape);
        }
    }
}
