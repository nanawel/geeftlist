<?php

namespace OliveOil\Core\Block;


class SessionMessages extends Template
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager
    ) {
        parent::__construct($context);
    }

    /**
     * @return array<mixed, array<'content'|'escape'|'type', mixed>>
     */
    public function getMessages(): array {
        $sessions = $this->getConfig('sessions');
        $flush = $this->getConfig('flush_after_render', true);

        if ($sessions !== null) {
            $sessions = is_array($sessions) ? $sessions : [$sessions];
        }
        else {
            $sessions = $this->sessionManager->getSessions();
        }

        $preparedMessages = [];
        foreach ($sessions as $session) {
            $messagesByType = $session->getMessages();
            foreach ($messagesByType as $type => $messages) {
                /** @var \OliveOil\Core\Model\Session\Message\MessageInterface $m */
                foreach ($messages as $m) {
                    $message = $m->getMessage();
                    if (!$m->hasOption('no_escape')) {
                        $message = nl2br((string) $this->view->escapeHtml($message));
                        $escape = true;
                    } else {
                        $escape = false;
                    }

                    $preparedMessages[] = [
                        'type' => $type,
                        'content' => $message,
                        'escape' => $escape
                    ];
                }
            }

            if ($flush) {
                $session->clearMessages();
            }
        }

        return $preparedMessages;
    }
}
