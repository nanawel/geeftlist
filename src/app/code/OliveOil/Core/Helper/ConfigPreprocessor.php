<?php

namespace OliveOil\Core\Helper;


use OliveOil\Core\Exception\InvalidConfigurationException;
use OliveOil\Core\Exception\InvalidPatternException;

class ConfigPreprocessor
{
    public const DEFAULT_PATTERN = '/(\${(?P<varName>\w[\w\.]*)})/';

    public function __construct(
        protected \Base $fw,
        protected string $pattern = self::DEFAULT_PATTERN
    ) {
    }

    /**
     * Replace occurences of ${...} vars in $configString with the given $vars values
     * or \Base hive values.
     *
     * @param array|null $vars NULL to use \Base hive, or an array to use custom values
     */
    public function run(string $configString, ?array $vars = null): ?string {
        return preg_replace_callback($this->pattern, function (array $matches) use ($vars) {
            if (is_array($vars)) {
                if (isset($vars[$matches['varName']])) {
                    return $vars[$matches['varName']];
                }
            }
            else {
                try {
                    if ($this->fw->exists($matches['varName'])) {
                        return $this->fw->get($matches['varName']);
                    }
                } catch (\ErrorException) {
                }

                throw new InvalidConfigurationException(
                    "Cannot resolve variable '{$matches['varName']}'. Is it defined?"
                );
            }

            return $matches[0];
        }, $configString);
    }

    /**
     * @throws InvalidPatternException
     */
    public function setPattern(string $pattern): static {
        if (!str_contains($pattern, '?P<varName>')) {
            throw new InvalidPatternException('Cannot find "varName" submask.');
        }

        $this->pattern = $pattern;

        return $this;
    }
}
