<?php

namespace OliveOil\Core\Helper;


class Output
{
    public function __construct(protected \Base $fw)
    {
    }

    /**
     * @param string $string
     * @param int $flags Flags for htmlspecialchars (default if null: ENT_COMPAT | ENT_HTML401)
     * @deprecated
     */
    public function htmlspecialchars($string, $flags = null): string {
        if ($flags === null) {
            $flags = ENT_COMPAT | ENT_HTML401;
        }

        return htmlspecialchars($string, $flags, $this->fw->get('ENCODING'));
    }

    /**
     * @param bool $hex
     * @param bool $alpha
     */
    public function rgbToCss(array $color, $hex = false, $alpha = null): string {
        if ($hex) {
            if ($alpha) {
                return 'rgba(' . implode(', ', $color) . ', ' . $alpha . ')';
            }

            return 'rgb(' . implode(', ', $color) . ')';
        }

        return '#' . dechex($color[0]) . dechex($color[1]) . dechex($color[2]);
    }

    public function jsQuoteEscape($string, $quote = ["'"]) {
        if (!is_array($quote)) {
            $quote = [$quote];
        }

        foreach ($quote as $q) {
            $string = str_replace($q, '\\' . $q, (string) $string);
        }

        return $string;
    }

    public function truncateText($text, $maxLength, string $ellipsis = '...', $keepFullWords = true, &$truncated = null): string {
        $text = (string) $text;
        if (mb_strlen($text) <= $maxLength) {
            $truncated = false;

            return $text;
        }

        $truncated = true;
        $truncatedText = mb_substr($text, 0, $maxLength - mb_strlen($ellipsis));
        if ($keepFullWords) {
            $i = mb_strlen($truncatedText) - 1;
            while ($i > 0 && preg_match('/\w/', $truncatedText[$i])) {
                --$i;
            }

            $truncatedText = mb_substr($truncatedText, 0, $i);
        }

        return $truncatedText . $ellipsis;
    }
}
