<?php

namespace OliveOil\Core\Helper;


class Transaction
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory)
    {
    }

    /**
     * @return mixed
     */
    public function execute(callable $callable) {
        return $this->newTransaction()->execute($callable);
    }

    /**
     * @return \OliveOil\Core\Model\ResourceModel\Iface\Transaction
     */
    public function newTransaction() {
        return $this->getCoreFactory()->make(\OliveOil\Core\Model\ResourceModel\Iface\Transaction::class);
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }
}
