<?php

namespace OliveOil\Core\Helper;


class Image
{
    /**
     * @param string $defaultOutputFormat
     * @param int $defaultOutputQuality
     */
    public function __construct(protected \Intervention\Image\ImageManager $imageManager, protected $defaultOutputFormat = 'jpg', protected $defaultOutputQuality = 80)
    {
    }

    /**
     * @param string $imagePath
     * @param string $outputPath
     * @param int $width
     * @param int $height
     * @param bool $keepRatio
     */
    public function resize(
        $imagePath,
        $outputPath,
        $width,
        $height,
        $outputFormat = null,
        $outputQuality = null,
        $keepRatio = true,
        $upsize = false
    ): void {
        $image = $this->getImageManager()->make($imagePath);

        $callback = static function ($constraint) use ($keepRatio, $upsize): void {
            if ($keepRatio) {
                $constraint->aspectRatio();
            }

            if ($upsize) {
                $constraint->upsize();
            }
        };
        $image->resize($width, $height, $callback)
            ->encode(
                $outputFormat ?: $this->getDefaultOutputFormat(),
                $outputQuality ?: $this->getDefaultOutputQuality()
            )
            ->save($outputPath);
    }

    public function getImageManager(): \Intervention\Image\ImageManager {
        return $this->imageManager;
    }

    /**
     * @return string
     */
    public function getDefaultOutputFormat() {
        return $this->defaultOutputFormat;
    }

    /**
     * @return int
     */
    public function getDefaultOutputQuality() {
        return $this->defaultOutputQuality;
    }
}
