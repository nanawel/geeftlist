<?php

namespace OliveOil\Core\Helper;


class DateTime
{
    private static ?\DateTimeImmutable $dateZero = null;

    /** @var \DateTimeZone[] */
    private static array $tz = [];

    private static function getDateZero(): \DateTimeImmutable {
        if (!self::$dateZero) {
            self::$dateZero = \DateTimeImmutable::createFromFormat(
                'Y-m-d H:i:s',
                '1-1-0 00:00:00',
                self::getTimeZone()
            );
        }

        return clone self::$dateZero;
    }

    public static function getTimeZone($tz = 'UTC'): \DateTimeZone {
        if (! isset(self::$tz[$tz])) {
            self::$tz[$tz] = new \DateTimeZone($tz);
        }

        return self::$tz[$tz];
    }

    /**
     * @throws \Exception
     */
    public static function getDate(string|int|null $time = 'now', $tz = 'UTC'): \DateTime {
        return new \DateTime(
            is_int($time) ? '@' . $time : ($time ?? 'now'),
            self::getTimeZone($tz)
        );
    }

    /**
     * @throws \Exception
     */
    public static function getDateIso(string|int|\DateTime|null $time = 'now'): string {
        if (!$time instanceof \DateTime) {
            $time = self::getDate($time);
        }

        return $time->format(DATE_ATOM);
    }

    /**
     * @throws \Exception
     */
    public static function getDateSql(string|int|\DateTime|null $time = 'now', ?string $tz = 'UTC'): string {
        if (!$time instanceof \DateTime) {
            $time = self::getDate($time, $tz);
        }

        return $time->format('Y-m-d H:i:s');
    }

    /**
     * @see https://stackoverflow.com/a/3534705
     */
    public static function secondsToTime(int|float $seconds): string {
        $t = round($seconds);

        return sprintf(
            '%02d:%02d:%02d',
            (int) round($t / 3600),
            (int) round($t / 60) % 60,
            $t % 60
        );
    }
}
