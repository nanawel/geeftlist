<?php

namespace OliveOil\Core\Helper\Data;


interface EncoderInterface
{
    /**
     * @param array $params
     * @return string
     */
    public function encode(mixed $data, $params = []);
}
