<?php

namespace OliveOil\Core\Helper\Data;


class Text implements EncoderInterface, DecoderInterface
{
    /**
     * @param mixed $string
     * @param array $params
     * @return mixed
     */
    public function decode($string, $params = []) {
        return $string;
    }

    /**
     * @param mixed $data
     * @param array $params
     * @return string
     */
    public function encode($data, $params = []) {
        return $data;
    }
}
