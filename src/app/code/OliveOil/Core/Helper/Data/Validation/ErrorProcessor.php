<?php

namespace OliveOil\Core\Helper\Data\Validation;


use OliveOil\Core\Model\Validation\ErrorInterface;

class ErrorProcessor
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        protected \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @param array|null $fieldLabelMapping
     * @return mixed[]
     */
    public function process(
        \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator,
        array &$data,
        $fieldLabelMapping = []
    ): array {
        $errors = [];
        if ($allFieldErrors = $errorAggregator->getFieldErrors()) {
            foreach ($allFieldErrors as $fieldErrors) {
                $hasError = false;

                /** @var ErrorInterface $fieldError */
                foreach ($fieldErrors as $fieldError) {
                    $errors[] = $this->processError($fieldError, $fieldLabelMapping, $data, $hasError);
                    if ($hasError) {
                        // skip to next field at first error
                        break;
                    }
                }
            }
        }

        return $errors;
    }

    protected function processError(ErrorInterface $fieldError, array $fieldLabelMapping, array &$data, &$isError) {
        $method = $this->getProcessMethod($fieldError->getType());

        return $this->{$method}(
            $fieldError,
            $fieldLabelMapping,
            $data,
            $isError
        );
    }

    protected function getProcessMethod(string $errorType): string {
        $method = sprintf('process%sError', ucfirst(\OliveOil\snakecase2camelcase($errorType)));
        if (!is_callable([$this, $method])) {
            $method = 'processUnknownError';
        }

        return $method;
    }

    /**
     * @param bool $isError
     * @return string
     */
    protected function processFieldEmptyError(ErrorInterface $fieldError, array $fieldLabelMapping, array &$data, &$isError) {
        $field = $fieldError->getField();

        unset($data[$field]);
        $isError = true;

        return $this->getMessage(
            $fieldError->getLocalizableMessage(),
            'Missing field: {0}.',
            $fieldLabelMapping[$field] ?? $field
        );
    }

    /**
     * @param bool $isError
     * @return string
     */
    protected function processFieldInvalidError(ErrorInterface $fieldError, array $fieldLabelMapping, array &$data, &$isError) {
        $field = $fieldError->getField();

        unset($data[$field]);
        $isError = true;

        return $this->getMessage(
            $fieldError->getLocalizableMessage(),
            'Invalid field: {0}.',
            $fieldLabelMapping[$field] ?? $field
        );
    }

    /**
     * @param bool $isError
     * @return string
     */
    protected function processFieldNotOverwritableError(ErrorInterface $fieldError, array $fieldLabelMapping, array &$data, &$isError) {
        $field = $fieldError->getField();

        unset($data[$field]);
        $isError = true;

        return $this->getMessage(
            $fieldError->getLocalizableMessage(),
            'Cannot overwrite field: {0}.',
            $fieldLabelMapping[$field] ?? $field
        );
    }

    /**
     * @param bool $isError
     * @return string
     */
    protected function processUnknownError(ErrorInterface $fieldError, array $fieldLabelMapping, array &$data, &$isError) {
        $field = $fieldError->getField();

        $this->logger->warning(sprintf("Unknown field error type for field '%s': %s", $field, $fieldError->getType()));

        unset($data[$field]);
        $isError = false;       // Let's assume this is not a serious error

        return $this->getMessage(
            $fieldError->getLocalizableMessage(),
            'Unknown error on field: {0}.',
            $fieldLabelMapping[$field] ?? $field
        );
    }

    protected function getMessage($errorLocalizableMessage, $defaultLocalizableMessage, $field) {
        return $this->i18n->tr(
            $errorLocalizableMessage ?: $defaultLocalizableMessage,
            $field
        );
    }
}
