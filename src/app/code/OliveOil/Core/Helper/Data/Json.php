<?php

namespace OliveOil\Core\Helper\Data;


class Json implements EncoderInterface, DecoderInterface
{
    public function __construct(protected \Laminas\Json\Json $encoder)
    {
    }

    /**
     * @param mixed $string
     * @param array $params
     * @return mixed
     */
    public function decode($string, $params = []) {
        return $this->encoder->decode(
            $string,
            empty($params['typeObject']) ? \Laminas\Json\Json::TYPE_ARRAY : \Laminas\Json\Json::TYPE_OBJECT
        );
    }

    /**
     * @param mixed $data
     * @param array $params
     * @return string
     */
    public function encode($data, $params = []) {
        $json = $this->encoder->encode(
            $data,
            $params['cycleCheck'] ?? false,
            $params['options'] ?? []
        );
        if ($params['prettyPrint'] ?? false) {
            return $this->prettyPrint($json, $params);
        }

        return $json;
    }

    /**
     * @param string $json
     * @param array $params
     * @return string
     */
    public function prettyPrint($json, $params = []) {
        return $this->encoder->prettyPrint(
            $json,
            $params['options'] ?? []
        );
    }
}
