<?php

namespace OliveOil\Core\Helper\Data;


interface DecoderInterface
{
    /**
     * @param array $params
     * @return mixed
     */
    public function decode(mixed $string, $params = []);
}
