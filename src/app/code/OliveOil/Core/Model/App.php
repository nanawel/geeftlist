<?php
namespace OliveOil\Core\Model;

use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Exception\SystemException;
use OliveOil\Core\Model\App\Context;

abstract class App implements AppInterface
{
    protected \Base $fw;

    protected \OliveOil\Core\Model\App\ConfigInterface $config;

    protected \OliveOil\Core\Service\Session\Manager $sessionManager;

    protected \OliveOil\Core\Service\App\ErrorHandlerInterface $errorHandler;

    protected \OliveOil\Core\Model\I18n $i18n;

    /** @var \OliveOil\Core\Service\EventInterface[] */
    protected array $eventServices;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    public function __construct(Context $context) {
        $this->fw = $context->getFw();
        $this->config = $context->getConfig();
        $this->sessionManager = $context->getSessionManager();
        $this->errorHandler = $context->getErrorHandler();
        $this->i18n = $context->getI18n();
        $this->eventServices = $context->getEventServices();
        $this->eventManager = $this->getEventService()->getEventManager($this);

        $this->fw->set('OLIVEOIL_APP', $this);
    }

    final public function bootstrap(): void {
        $this->checkRequirements()
            ->applyPhpConfig()
//            ->migrateAppConfig()
            ->initFilesystem()
            ->setupPhpI18n();
    }

    final public function setup(): void {
        $this->checkMaintenance()
            ->setupListeners()
            ->setupResources()
            ->initSessions()
            ->onSetup();

        $this->eventManager->trigger('setup::after', $this);
    }

    public function onShutdown(): void {
        $this->eventManager->trigger('shutdown', $this);
    }

    public function checkRequirements(): static {
        if (version_compare(phpversion(), '7.4.0', '<')) {
            throw new SystemException('You must run PHP 7.4.0 or above (current version: ' . phpversion() . ').');
        }

        return $this;
    }

//    public function migrateAppConfig(): static {
//        // F3 "DEBUG" to "app.LOG_LEVEL" conversion
//        if ($this->config->hasValue('LOG_LEVEL') && ($debug = $this->fw->get('DEBUG'))) {
//            $this->config->setValue('LOG_LEVEL', Log::debugToLogLevel($debug));
//        }
//
//        return $this;
//    }

    /**
     * TODO Move to a DirectoryManager or whatever, that could be injected into any class
     *
     * @return $this
     * @throws FilesystemException
     */
    protected function initFilesystem(): static {
        $tmpDir = $this->fw->get('TEMP');
        $this->fw->set('TEMP', $this->initDir($tmpDir));
        $lockDir = $this->config->getValue('LOCK_DIR');
        $this->config->setValue('LOCK_DIR', $this->initDir($lockDir));

        return $this;
    }

    /**
     * @return $this
     */
    protected function applyPhpConfig(): static {
        $phpConfig = $this->config->getValue('PHP');
        if (is_array($phpConfig)) {
            foreach ($phpConfig as $config => $value) {
                ini_set($config, $value);
            }
        }

        // Session & cookie config
        if (!headers_sent() && session_status() != PHP_SESSION_ACTIVE) {
            ini_set('session.use_trans_sid', '0');
            ini_set('session.use_cookies', '0');
            ini_set('session.use_only_cookies', '1');
        }

        return $this;
    }

    protected function checkMaintenance(): static {
        if (! $this->fw->get('CLI')) {
            $maintenanceFlag = $this->config->getValue('MAINTENANCE_FLAG');
            if (is_file($maintenanceFlag)) {
                throw new ServiceUnavailableException(
                    sprintf("Temporarily down for Maintenance (%s)", file_get_contents($maintenanceFlag))
                );
            }
        }

        return $this;
    }

    protected function setupListeners(): static {
        foreach ($this->eventServices as $eventService) {
            $eventService->setupListeners();
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function onSetup(): static {
        $this->fw->set('UNLOAD', fn($fw) => $this->onUnload($fw));

        return $this;
    }

    public function initSessions(): static {
        if ($this->fw->get('DISABLE_SESSIONS')) {
            return $this;
        }

        $this->sessionManager->initSession();

        return $this;
    }

    /**
     * @return $this
     */
    protected function setupResources(): static {
        // to be overridden

        return $this;
    }

    /**
     * Called on shutdown
     * Notice: session is NOT available anymore here (already written)
     *
     * @param \Base $fw
     * @return mixed|bool
     */
    protected function onUnload($fw) {
        $transportObject = new MagicObject([
            'fw'      => $fw,
            'proceed' => true
        ]);
        $this->eventManager->trigger('unload', $this, [
            'transport_object' => $transportObject,
            '_skip_log'        => true
        ]);
        return $transportObject->getProceed();
    }

    protected function setupPhpI18n(): static {
        // Avoid decimal separator issues when casting double and float values to strings
        setlocale(LC_NUMERIC, 'C');

        // Internal dates are UTC, formatting is done at rendering
        date_default_timezone_set('UTC');

        return $this;
    }

    /******* MISC ************************************************************/
    /**
     *
     * @return string Normalized path
     * @throws FilesystemException
     */
    protected function initDir(string $path, int $perms = 0770): string {
        if ($path[strlen($path) - 1] !== DIRECTORY_SEPARATOR) {
            $path .= DIRECTORY_SEPARATOR;
        }

        if (!is_dir($path) && !mkdir($path, $perms, true)) {
            throw new FilesystemException("Cannot create directory at $path, please adjust permissions first.");
        }
        if ($perms & 0220 && ! is_writable($path)) {
            throw new FilesystemException("$path must be writable");
        }

        return $path;
    }

    /**
     * @return \OliveOil\Core\Service\EventInterface
     */
    public function getEventService($eventSpace = null) {
        $eventSpace ??= 'default';

        return $this->eventServices[$eventSpace] ?? null;
    }
}
