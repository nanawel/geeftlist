<?php

namespace OliveOil\Core\Model\Event;


use Laminas\EventManager\ResponseCollection;

/**
 * Class Manager
 *
 * Wrapper for \Laminas\EventManager\EventManager that adds support for event spaces
 * and a convenient way to trigger multiple events in one call.
 */
class Manager extends \Laminas\EventManager\EventManager
{
    /**
     * @param bool $enableTriggerEvents
     */
    public function __construct(
        protected \OliveOil\Core\Service\EventInterface $eventService,
        \Laminas\EventManager\SharedEventManager $sharedEventManager,
        array $identifiers = [],
        protected $enableTriggerEvents = false
    ) {
        parent::__construct($sharedEventManager, $identifiers);
    }

    /**
     * Convenient method to trigger multiple events with the same target/arguments
     *
     * @param string|array $eventName
     * @param object|null $target
     * @param array $argv
     * @return \Laminas\EventManager\ResponseCollection
     */
    public function trigger($eventName, $target = null, $argv = []) {
        if (! is_array($eventName)) {
            $eventName = [$eventName];
        }

        $responseCollection = new ResponseCollection();
        foreach ($eventName as $ev) {
            $eventId = uniqid($ev . '_');
            if ($this->enableTriggerEvents && !$target instanceof Manager) {
                $this->getEventManager()->trigger('trigger::before', $this, [
                    'id' => $eventId,
                    'event' => $ev,
                    'target_class' => $target !== null ? $target::class : self::class
                ]);
            }

            foreach (parent::trigger($ev, $target, $argv) as $response) {
                $responseCollection->push($response);
            }

            if ($this->enableTriggerEvents && !$target instanceof Manager) {
                $this->getEventManager()->trigger('trigger::after', $this, ['id' => $eventId]);
            }
        }

        return $responseCollection;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    /**
     * @return Manager
     */
    public function getEventManager() {
        return $this->eventService->getEventManager($this);
    }
}
