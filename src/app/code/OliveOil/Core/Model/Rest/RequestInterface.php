<?php

namespace OliveOil\Core\Model\Rest;

interface RequestInterface extends \OliveOil\Core\Model\Http\RequestInterface
{
    /**
     * @param RequestInterface|null $related
     * @return $this
     */
    public function setRelatedRequest(\OliveOil\Core\Model\Http\RequestInterface $related = null);

    /**
     * @return RequestInterface|null $related
     */
    public function getRelatedRequest();

    /**
     * @param string $verb
     * @return $this
     */
    public function setVerb($verb);

    /**
     * @param string[] $headers
     * @return $this
     */
    public function setHeaders(array $headers);

    /**
     * @return $this
     */
    public function addHeaders(array $headers);

    /**
     * @return $this
     */
    public function setQueryParams(array $params);

    /**
     * @param string $param
     * @return $this
     */
    public function setQueryParam($param, mixed $value);

    /**
     * @return $this
     */
    public function addQueryParams(array $params);

    /**
     * @param string[]|array $params
     * @return $this
     */
    public function setRouteParams(array $params);

    /**
     * @param string $param
     * @return $this
     */
    public function setRouteParam($param, mixed $value);

    /**
     * @return $this
     */
    public function addRouteParams(array $params);

    /**
     * @return mixed
     */
    public function getData();

    /**
     * @return $this
     */
    public function setData(mixed $data);

    /**
     * @return array
     */
    public function getMeta();

    /**
     * @return $this
     */
    public function setMeta(array $meta);

    /**
     * @return $this
     */
    public function addMeta(mixed $meta);
}
