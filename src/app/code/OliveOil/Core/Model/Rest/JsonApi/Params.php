<?php


namespace OliveOil\Core\Model\Rest\JsonApi;


class Params extends \OliveOil\Core\Model\Rest\Params
{
    /** @var string */
    protected $relationshipsParamsClass;

    /** @var array */
    protected $primaryCollectionConstraints = [];

    /** @var array */
    protected $relationshipsCollectionConstraints = [];

    /**
     * Params constructor.
     *
     * @param int $objectType
     * @param string $primaryEntityType
     * @param int $primaryEntityId
     * @param string[] $includedRelationships
     */
    public function __construct(
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected $objectType = 0,
        protected $primaryEntityType = null,
        protected $primaryEntityId = null,
        protected $includedRelationships = []
    ) {
        parent::__construct($coreFactory);
    }


    /**
     * @param bool $include
     */
    public function includeId($include = null): bool {
        if ($include === true) {
            $this->objectType |= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_ID;
        }
        elseif ($include === false) {
            $this->objectType ^= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_ID;
        }

        return (bool) ($this->objectType & Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_ID);
    }

    public function includeAttributes($include = null): bool {
        if ($include === true) {
            $this->objectType |= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_ATTRIBUTES;
        }
        elseif ($include === false) {
            $this->objectType ^= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_ATTRIBUTES;
        }

        return (bool) ($this->objectType & Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_ATTRIBUTES);
    }

    public function includeLinks($include = null): bool {
        if ($include === true) {
            $this->objectType |= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_LINKS;
        }
        elseif ($include === false) {
            $this->objectType ^= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_LINKS;
        }

        return (bool) ($this->objectType & Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_LINKS);
    }

    /**
     * @param bool|null $include
     */
    public function includeLiteRelationships($include = null): bool {
        if ($include === true) {
            $this->objectType |= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_LITE_RELATIONSHIPS;
        }
        elseif ($include === false) {
            $this->objectType ^= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_LITE_RELATIONSHIPS;
        }

        return (bool) ($this->objectType & Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_LITE_RELATIONSHIPS);
    }

    /**
     * @param bool|null $include
     */
    public function includeFullRelationships($include = null): bool {
        if ($include === true) {
            $this->objectType |= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_FULL_RELATIONSHIPS;
        }
        elseif ($include === false) {
            $this->objectType ^= Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_FULL_RELATIONSHIPS;
        }

        return ($this->objectType & Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_FULL_RELATIONSHIPS)
            === Constants::PARAM_OBJECT_TYPE_RESOURCE_WITH_FULL_RELATIONSHIPS;
    }

    /**
     * @param string $relationship
     * @param bool|null $include
     */
    protected function includeRelationship($relationship, $include = null): bool {
        if ($include === true) {
            $this->includedRelationships = array_unique(array_merge($this->includedRelationships, [$relationship]));
            $this->includeLiteRelationships(true);
        }
        elseif ($include === false) {
            $this->includedRelationships = array_filter(
                $this->includedRelationships,
                static fn($item): bool => $item != $relationship
                    && !str_starts_with((string) $item, $relationship . '.')
            );
        }

        return (bool) array_filter($this->includedRelationships, static fn($item): bool => $item === $relationship
            || str_starts_with((string) $item, $relationship . '.'));
    }

    /**
     * @return string
     */
    public function getPrimaryEntityType() {
        return $this->primaryEntityType;
    }

    /**
     * @param string $primaryEntityType
     * @return $this
     */
    public function setPrimaryEntityType($primaryEntityType): static {
        $this->primaryEntityType = $primaryEntityType;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrimaryEntityId() {
        return $this->primaryEntityId;
    }

    /**
     * @param int $primaryEntityId
     * @return $this
     */
    public function setPrimaryEntityId($primaryEntityId): static {
        $this->primaryEntityId = $primaryEntityId;

        return $this;
    }

    /**
     * @return int
     */
    public function getObjectType() {
        return $this->objectType;
    }

    /**
     * @param int $objectType
     * @return $this
     */
    public function setObjectType($objectType): static {
        $this->objectType = $objectType;
        if ($this->includedRelationships !== []) {
            $this->includeLiteRelationships(true);
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function getIncludedRelationships() {
        return $this->includedRelationships;
    }

    /**
     * @param string[] $includedRelationships
     * @return $this
     */
    public function setIncludedRelationships(array $includedRelationships): static {
        $this->includedRelationships = $includedRelationships;
        if ($includedRelationships !== []) {
            $this->includeLiteRelationships(true);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getPrimaryCollectionConstraints() {
        return $this->primaryCollectionConstraints;
    }

    /**
     * @return $this
     */
    public function setPrimaryCollectionConstraints(array $primaryCollectionConstraints): static {
        $this->primaryCollectionConstraints = $primaryCollectionConstraints;

        return $this;
    }

    /**
     * @return $this
     */
    public function addCollectionConstraints(array $collectionConstraints): static {
        $this->primaryCollectionConstraints = array_merge_recursive(
            $this->primaryCollectionConstraints,
            $collectionConstraints
        );

        return $this;
    }

    /**
     * @return array
     */
    public function getRelationshipsCollectionConstraints() {
        return $this->relationshipsCollectionConstraints;
    }

    public function setRelationshipsCollectionConstraints(array $relationshipsCollectionConstraints): static {
        $this->relationshipsCollectionConstraints = $relationshipsCollectionConstraints;

        return $this;
    }

    /**
     * @return $this
     */
    public function addRelationshipsCollectionConstraints(array $relationshipsCollectionConstraints): static {
        $this->relationshipsCollectionConstraints = array_merge_recursive(
            $this->relationshipsCollectionConstraints,
            $relationshipsCollectionConstraints
        );

        return $this;
    }
}
