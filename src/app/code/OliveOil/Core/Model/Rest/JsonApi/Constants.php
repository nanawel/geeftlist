<?php

namespace OliveOil\Core\Model\Rest\JsonApi;


interface Constants
{
    public const WILDCARD = '*';

    /* @see https://jsonapi.org/format/#document-resource-objects */
    public const PARAM_OBJECT_TYPE_RESOURCE =
        self::PARAM_OBJECT_TYPE_RESOURCE_WITH_ID
        | self::PARAM_OBJECT_TYPE_RESOURCE_WITH_ATTRIBUTES
        | self::PARAM_OBJECT_TYPE_RESOURCE_WITH_LINKS
        | self::PARAM_OBJECT_TYPE_RESOURCE_WITH_FULL_RELATIONSHIPS;

    /* @see https://jsonapi.org/format/#document-resource-identifier-objects */
    public const PARAM_OBJECT_TYPE_RESOURCE_ID =
        self::PARAM_OBJECT_TYPE_RESOURCE_WITH_ID;

    /* Custom object type with data but no relationships (default for collections) */
    public const PARAM_OBJECT_TYPE_RESOURCE_LITE =
        self::PARAM_OBJECT_TYPE_RESOURCE_WITH_ID
        | self::PARAM_OBJECT_TYPE_RESOURCE_WITH_ATTRIBUTES
        | self::PARAM_OBJECT_TYPE_RESOURCE_WITH_LINKS
        | self::PARAM_OBJECT_TYPE_RESOURCE_WITH_LITE_RELATIONSHIPS;

    public const PARAM_OBJECT_TYPE_RESOURCE_WITH_ID                 = 0b00000001;

    public const PARAM_OBJECT_TYPE_RESOURCE_WITH_ATTRIBUTES         = 0b00000010;

    public const PARAM_OBJECT_TYPE_RESOURCE_WITH_LINKS              = 0b00000100;

    public const PARAM_OBJECT_TYPE_RESOURCE_WITH_LITE_RELATIONSHIPS = 0b00001000;

    public const PARAM_OBJECT_TYPE_RESOURCE_WITH_FULL_RELATIONSHIPS = 0b00011000;  // Includes WITH_LITE_RELATIONSHIPS
}
