<?php

namespace OliveOil\Core\Model\Rest;


class Response extends \OliveOil\Core\Model\Http\Response implements ResponseInterface
{
    /**
     * @inheritDoc
     */
    public function getData() {
        return $this->body;
    }

    /**
     * @inheritDoc
     */
    public function setData($data): static {
        $this->body = $data;

        return $this;
    }
}
