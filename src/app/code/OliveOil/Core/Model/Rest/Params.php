<?php

namespace OliveOil\Core\Model\Rest;


class Params
{
    /** @var bool */
    protected $useEntityAttributeFrontendValues = false;

    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory)
    {
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    /**
     * @return bool
     */
    public function isUseEntityAttributeFrontendValues() {
        return $this->useEntityAttributeFrontendValues;
    }

    /**
     * @param bool $useEntityAttributeFrontendValues
     */
    public function setUseEntityAttributeFrontendValues($useEntityAttributeFrontendValues): static {
        $this->useEntityAttributeFrontendValues = $useEntityAttributeFrontendValues;

        return $this;
    }
}
