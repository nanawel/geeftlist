<?php

namespace OliveOil\Core\Model\Rest;


interface ResponseInterface extends \OliveOil\Core\Model\Http\ResponseInterface
{
    /**
     * @return mixed
     */
    public function getData();

    /**
     * @return $this
     */
    public function setData(mixed $data);
}
