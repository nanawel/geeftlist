<?php

namespace OliveOil\Core\Model\Rest;


class Request extends \OliveOil\Core\Model\Http\Request implements RequestInterface
{
    /** @var array */
    protected $meta = [];

    /**
     * @inheritDoc
     */
    public function setVerb($verb): static {
        if ($verb === null) {
            throw new \InvalidArgumentException('Verb cannot be null.');
        }

        $this->verb = self::normalizeVerb($verb);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setHeaders(array $headers): static {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addHeaders(array $headers): static {
        $this->headers = array_merge($this->headers, $headers);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setQueryParams(array $params): static {
        $this->queryParams = $params;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setQueryParam($param, $value): static {
        if (str_contains($param, '[')) {
            throw new \InvalidArgumentException('Cannot use brackets in param names. Pass array as value instead.');
        }

        $this->queryParams[$param] = $value;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addQueryParams(array $params): static {
        $this->queryParams = array_merge($this->queryParams, $params);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setRouteParams(array $params): static {
        $this->routeParams = $params;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setRouteParam($param, $value): static {
        $this->routeParams[$param] = $value;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addRouteParams(array $params): static {
        $this->routeParams = array_merge($this->routeParams, $params);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData() {
        return $this->getBody();
    }

    /**
     * @inheritDoc
     */
    public function setData($data): static {
        $this->body = $data;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getMeta() {
        return $this->meta;
    }

    /**
     * @inheritDoc
     */
    public function setMeta(array $meta): static {
        $this->meta = $meta;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addMeta($meta): static {
        if (is_array($meta)) {
            $this->meta = array_merge_recursive($this->meta, $meta);
        }
        else {
            $this->meta[] = $meta;
        }

        return $this;
    }
}
