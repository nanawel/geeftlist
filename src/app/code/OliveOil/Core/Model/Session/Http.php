<?php

namespace OliveOil\Core\Model\Session;

use OliveOil\Core\Exception\Session\ExpiredException;
use OliveOil\Core\Model\Session;

/**
 * HTTP session segment using regular cookie
 */
class Http extends Session implements HttpInterface
{
    /** @var string */
    protected $name;

    /**
     * Session constructor.
     *
     * @param string $name
     */
    public function __construct(
        \OliveOil\Core\Model\Session\Context $context,
        protected \OliveOil\Core\Model\Session\Http\PhpSession $phpSession,
        $name
    ) {
        parent::__construct($context, $name);

        // Make sure all sessions (segments) are initialized properly if reset during request (expired session)
        $this->phpSession->attachInstanceListener('start::after_session_start', function (): void {
            $this->init();
        });
    }

    /**
     * @inheritDoc
     */
    public function init(): static {
        try {
            parent::init();
            $this->start()
                ->syncData();

            $this->initAfter();
        }
        catch (ExpiredException $expiredException) {
            $this->invalidate()
                ->init();

            throw $expiredException;
        }

        return $this;
    }

    protected function initAfter() {
        $this->getEventManager()->trigger('init::after::' . $this->getName(), $this);
    }

    /**
     * @return $this
     * @throws ExpiredException
     */
    protected function start(): static {
        $this->getPhpSession()->start();

        return $this;
    }

    /**
     * @return $this
     */
    protected function syncData(): static {
        $this->_data =& $this->getPhpSession()->getSegment($this->getName());

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function invalidate(): static {
        $this->getPhpSession()->invalidate();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function regenerate(): static {
        $this->getPhpSession()->regenerate();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function abortClose($clear = false): static {
        $this->getPhpSession()->abortClose($clear);

        return $this;
    }

    /**
     * @return string
     */
    public function getCsrfToken() {
        // Token is shared between all segments
        return $this->getPhpSession()->getCsrfToken();
    }

    /**
     * @inheritDoc
     */
    public function setType($type): static {
        $this->getPhpSession()->setData(self::KEY_TYPE, $type);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getType() {
        return $this->getPhpSession()->getData(self::KEY_TYPE);
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    public function getPhpSession(): \OliveOil\Core\Model\Session\Http\PhpSession {
        return $this->phpSession;
    }
}
