<?php


namespace OliveOil\Core\Model\Session;


interface Constants
{
    /**
     * "On suspect" behavior codes
     */
    public const ON_SUSPECT_403               = '403';

                    // HTTP 403
    public const ON_SUSPECT_INVALIDATE        = 'invalidate';

             // Invalidate all and continue
    public const ON_SUSPECT_INVALIDATE_CLIENT = 'invalidate-client';

      // Invalidate cookie and continue
    public const ON_SUSPECT_IGNORE            = 'ignore';             // Do nothing (dev only)

    /**
     * Session types
     */
    public const TYPE_DEFAULT         = self::TYPE_SESSION;

    public const TYPE_SESSION         = 'session';

    public const TYPE_PERSISTENT      = 'persistent';

    /**
     * Session message types
     */
    public const MESSAGE_INFO         = 'info';

    public const MESSAGE_SUCCESS      = 'success';

    public const MESSAGE_WARN         = 'warn';

    public const MESSAGE_ERROR        = 'error';

    /**
     * Key that holds the CSRF token in session AND in F3 hive
     * @see \Session::__construct()
     */
    public const KEY_CSRF_TOKEN       = 'csrf_token';

    /**
     * @var string Session key for "expires" value
     */
    public const KEY_EXPIRES          = 'expires';

    /**
     * @var string Session key for "type" value
     */
    public const KEY_TYPE             = 'type';
}
