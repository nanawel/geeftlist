<?php


namespace OliveOil\Core\Model\Session;


use OliveOil\Core\Exception\SessionException;
use OliveOil\Core\Model\SessionInterface;

interface HttpInterface extends SessionInterface
{
    /**
     * @return $this
     * @throws SessionException
     */
    public function regenerate();

    /**
     * @param bool $clear Also clear session storage
     * @return $this
     */
    public function abortClose($clear = false);

    /**
     * @return string
     */
    public function getTheme();

    /**
     * @return string
     */
    public function getCsrfToken();

    /**
     * @see \OliveOil\Core\Model\Session\Constants
     *
     * @param string $type
     * @return $this
     */
    public function setType($type);

    /**
     * @see \OliveOil\Core\Model\Session\Constants
     *
     * @return string
     */
    public function getType();
}
