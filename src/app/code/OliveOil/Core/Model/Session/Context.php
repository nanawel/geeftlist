<?php


namespace OliveOil\Core\Model\Session;


class Context
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Model\App\ConfigInterface $appConfig, protected \OliveOil\Core\Model\I18n $i18n, protected \OliveOil\Core\Model\Session\Message\ManagerInterface $messageManager, protected \OliveOil\Core\Service\EventInterface $eventService, protected \OliveOil\Core\Service\Crypt $cryptService)
    {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getMessageManager(): \OliveOil\Core\Model\Session\Message\ManagerInterface {
        return $this->messageManager;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }
}
