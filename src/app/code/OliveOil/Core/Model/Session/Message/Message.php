<?php

namespace OliveOil\Core\Model\Session\Message;


class Message implements MessageInterface
{
    /**
     * @param string $message
     * @param string $type
     * @param mixed[] $options
     */
    public function __construct(protected $message = '', protected $type = null, protected $options = [])
    {
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message): static {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type): static {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options): static {
        $this->options = $options;

        return $this;
    }

    public function hasOption($option): bool {
        return isset($this->options[$option]) && ((bool) $this->options[$option]);
    }

    public function __toString(): string {
        return sprintf(
            '(%s) %s',
            strtoupper($this->type),
            $this->message
        );
    }
}
