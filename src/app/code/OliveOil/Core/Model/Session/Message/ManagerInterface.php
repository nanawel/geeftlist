<?php

namespace OliveOil\Core\Model\Session\Message;


interface ManagerInterface
{
    /**
     * @param string $message
     * @param string $type
     * @param array $options
     * @return $this
     */
    public function addMessage($message, $type, $options = []);

    /**
     * @param string $message
     * @param array $options
     * @return $this
     */
    public function addInfoMessage($message, $options = []);

    /**
     * @param string $message
     * @param array $options
     * @return $this
     */
    public function addSuccessMessage($message, $options = []);

    /**
     * @param string $message
     * @param array $options
     * @return $this
     */
    public function addWarningMessage($message, $options = []);

    /**
     * @param string $message
     * @param array $options
     * @return $this
     */
    public function addErrorMessage($message, $options = []);

    /**
     *
     * @return \OliveOil\Core\Model\Session\Message\MessageInterface[][]
     */
    public function getMessages();

    /**
     *
     * @return \OliveOil\Core\Model\Session\Message\MessageInterface[]
     */
    public function getMessagesByType($type);

    /**
     * @return $this
     */
    public function clearMessages();

    /**
     * @return $this
     */
    public function setSession(\OliveOil\Core\Model\SessionInterface $session);
}
