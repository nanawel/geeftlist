<?php

namespace OliveOil\Core\Model\Session\Message;


interface StorageInterface
{
    /**
     * @param string|null $uniqId
     * @return $this
     */
    public function add(MessageInterface $message, $uniqId = null);

    /**
     * @param string $type
     * @return Message[]
     */
    public function getAllByType($type);

    /**
     * @return Message[][]
     */
    public function getAll();

    /**
     * @return $this
     */
    public function clear();

    /**
     * @return $this
     */
    public function setSession(\OliveOil\Core\Model\SessionInterface $session);
}
