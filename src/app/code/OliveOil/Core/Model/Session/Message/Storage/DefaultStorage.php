<?php

namespace OliveOil\Core\Model\Session\Message\Storage;


use OliveOil\Core\Model\Session\Message\MessageInterface;
use OliveOil\Core\Model\Session\Message\StorageInterface;

class DefaultStorage implements StorageInterface
{
    /** @var string */
    public const KEY_MESSAGES = '__messages';

    /** @var \OliveOil\Core\Model\SessionInterface */
    protected $session;

    /**
     * @inheritDoc
     */
    public function add(MessageInterface $message, $uniqId = null): static {
        $internalStorage =& $this->getDataRoot();

        if (!array_key_exists($message->getType(), $internalStorage)) {
            $internalStorage[$message->getType()] = [];
        }

        if ($uniqId !== null) {
            if (!array_key_exists($uniqId, $internalStorage[$message->getType()])) {
                $internalStorage[$message->getType()][$uniqId] = $message;
            }
        }
        else {
            $internalStorage[$message->getType()][] = $message;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAllByType($type) {
        $internalStorage =& $this->getDataRoot();

        return $internalStorage[$type] ?? [];
    }

    /**
     * @inheritDoc
     */
    public function getAll() {
        return $this->getDataRoot();
    }

    /**
     * @inheritDoc
     */
    public function clear(): static {
        $internalStorage =& $this->getDataRoot();
        $internalStorage = [];

        return $this;
    }

    /**
     * @return array
     */
    protected function &getDataRoot() {
        $messages =& $this->getSession()->refData(self::KEY_MESSAGES);
        if (!is_array($messages)) {
            $messages = [];
        }

        return $messages;
    }

    /**
     * @inheritDoc
     */
    public function setSession(\OliveOil\Core\Model\SessionInterface $session): static {
        $this->session = $session;

        return $this;
    }

    /**
     * @return \OliveOil\Core\Model\SessionInterface
     */
    public function getSession() {
        if (!$this->session) {
            throw new \RuntimeException('Session has not been set.');
        }

        return $this->session;
    }
}
