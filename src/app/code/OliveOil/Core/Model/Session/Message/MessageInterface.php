<?php

namespace OliveOil\Core\Model\Session\Message;


interface MessageInterface
{
    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $message
     * @return Message
     */
    public function setMessage($message);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $type
     * @return Message
     */
    public function setType($type);

    /**
     * @return array
     */
    public function getOptions();

    /**
     * @param array $options
     * @return Message
     */
    public function setOptions($options);

    /**
     * @param string $option
     * @return bool
     */
    public function hasOption($option);

    /**
     * @return string
     */
    public function __toString();
}
