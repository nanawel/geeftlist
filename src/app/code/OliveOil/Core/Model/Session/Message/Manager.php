<?php

namespace OliveOil\Core\Model\Session\Message;


class Manager implements ManagerInterface
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Model\Session\Message\MessageInterface $messagePrototype, protected \OliveOil\Core\Model\Session\Message\StorageInterface $storage)
    {
    }

    /**
     * @inheritDoc
     */
    public function addMessage($message, $type, $options = []): static {
        $uniqId = md5($message);

        $message = $this->newMessage()
            ->setMessage($message)
            ->setType($type)
            ->setOptions($options);
        $this->getStorage()->add($message, $uniqId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addInfoMessage($message, $options = []): static {
        return $this->addMessage($message, Constants::MESSAGE_INFO, $options);
    }

    /**
     * @inheritDoc
     */
    public function addSuccessMessage($message, $options = []): static {
        return $this->addMessage($message, Constants::MESSAGE_SUCCESS, $options);
    }

    /**
     * @inheritDoc
     */
    public function addWarningMessage($message, $options = []): static {
        return $this->addMessage($message, Constants::MESSAGE_WARN, $options);
    }

    /**
     * @inheritDoc
     */
    public function addErrorMessage($message, $options = []): static {
        if ($message instanceof \Exception) {
            $message = $message->getMessage();
        }

        return $this->addMessage($message, Constants::MESSAGE_ERROR, $options);
    }

    /**
     * @inheritDoc
     */
    public function getMessages() {
        return $this->getStorage()->getAll();
    }

    /**
     * @inheritDoc
     */
    public function getMessagesByType($type) {
        return $this->getStorage()->getAllByType($type);
    }

    /**
     * @inheritDoc
     */
    public function clearMessages(): static {
        $this->getStorage()->clear();

        return $this;
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    /**
     * @return MessageInterface
     */
    protected function newMessage() {
        return clone $this->messagePrototype;
    }

    public function getStorage(): \OliveOil\Core\Model\Session\Message\StorageInterface {
        return $this->storage;
    }

    /**
     * @inheritDoc
     */
    public function setSession(\OliveOil\Core\Model\SessionInterface $session): static {
        $this->getStorage()->setSession($session);

        return $this;
    }
}
