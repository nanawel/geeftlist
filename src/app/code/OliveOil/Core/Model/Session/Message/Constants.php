<?php


namespace OliveOil\Core\Model\Session\Message;


interface Constants
{
    public const MESSAGE_ERROR   = 'error';

    public const MESSAGE_INFO    = 'info';

    public const MESSAGE_WARN    = 'warn';

    public const MESSAGE_SUCCESS = 'success';
}
