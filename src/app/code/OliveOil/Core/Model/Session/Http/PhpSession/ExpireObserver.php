<?php

namespace OliveOil\Core\Model\Session\Http\PhpSession;


use OliveOil\Core\Exception\Session\ExpiredException;
use OliveOil\Core\Model\Session\Http\PhpSession;

class ExpireObserver
{
    public function __construct(protected \OliveOil\Core\Model\App\ConfigInterface $appConfig)
    {
    }

    /**
     * @param array $args
     * @throws ExpiredException
     */
    public function __invoke(PhpSession $session, $args): void {
        if (($expires = $session->getData('expires')) && time() > $expires) {
            throw new ExpiredException('The session has expired.');
        }
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }
}
