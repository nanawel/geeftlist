<?php

namespace OliveOil\Core\Model\Session\Http\PhpSession;


use Narrowspark\HttpStatus\Exception\ForbiddenException;
use OliveOil\Core\Exception\Session\SuspectException;
use OliveOil\Core\Model\Session\Constants;
use OliveOil\Core\Model\Session\Http\PhpSession;

class SuspectObserver
{
    public function __construct(protected \OliveOil\Core\Model\App\ConfigInterface $appConfig)
    {
    }

    /**
     * @param array $args
     * @throws SuspectException
     */
    public function __invoke(PhpSession $session, $args): void {
        if ($session->isSuspect()) {
            foreach ($this->getOnSuspectBehaviorActionCodes($session->getName()) as $actionCode) {
                switch ($actionCode) {
                    case Constants::ON_SUSPECT_IGNORE:
                        // Do nothing, suspect session will be used without error (dev only)
                        break;

                    case Constants::ON_SUSPECT_INVALIDATE:
                        // Invalidate session (warning: also ends session of the legitimate client)
                        $session->invalidate();
                        break;

                    case Constants::ON_SUSPECT_INVALIDATE_CLIENT:
                        // Invalidate session cookie only
                        $session->invalidateClient()
                            ->abortClose();
                        break;

                    case Constants::ON_SUSPECT_403:
                    default:
                        // Abort session (only prevents suspect client from using it)
                        $session->abortClose();
                        throw new ForbiddenException('Invalid session.');
                }
            }
        }
    }

    protected function getOnSuspectBehaviorActionCodes($sessionName) {
        $codes = $this->getAppConfig()->getValue(sprintf('SESSION.%s.on_suspect', $sessionName));
        if (!is_array($codes)) {
            $codes = [$codes];
        }

        return $codes;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }
}
