<?php

namespace OliveOil\Core\Model\Session\Http;


use OliveOil\Core\Exception\SessionException;
use OliveOil\Core\Iface\ListenableInstance;
use OliveOil\Core\Model\Session\Constants;
use OliveOil\Core\Model\Traits\ListenableInstanceTrait;

class PhpSession implements ListenableInstance, Constants
{
    use ListenableInstanceTrait;

    public const SEGMENT_PREFIX = 'SEGMENT::';

    protected ?\Session $f3Session = null;

    protected int $expires = 0;

    protected bool $isSuspect = false;

    public function __construct(
        protected \Base $fw,
        protected \Cache $storage,
        protected \OliveOil\Core\Model\Cookie $cookie,
        protected \OliveOil\Core\Service\Crypt $cryptService,
        protected string $name
    ) {
    }

    public function start(): static {
        if (!$this->hasStarted()) {
            $this->doStart();
        }

        return $this;
    }

    /**
     * @throws SessionException
     */
    protected function doStart() {
        $this->triggerInstanceEvent('start::before', $this);

        session_name($this->getName());
        session_id($this->getCookie()->getValue() ?: session_create_id());

        $self = $this;
        $onSuspectCallback = static function (\Session $session, $id) use ($self): void {
            // If this code is executed it means F3 has detected that this session is suspect,
            // but it might be for a wrong reason (= changed IP on mobile device). So ATM
            // we actually consider it really suspect if the user-agent has changed.
            // Just set a flag, actual processing might occur later
            $self->isSuspect = $session->agent() != $self->getData('agent');
        };
        $this->f3Session = new \Session($onSuspectCallback, self::KEY_CSRF_TOKEN, $this->storage);

        if (!session_start()) {    // Might call $onSuspectCallback, see \Session::read()
            throw new SessionException('Could not start session.');
        }

        $this->triggerInstanceEvent('start::after_session_start', $this);

        if ($token = $this->getCsrfToken()) {
            $this->setCsrfToken($token);
        }
        else {
            $this->regenerateCsrfToken();
        }

        $this->syncCookie()
            ->writeCookie();

        $this->triggerInstanceEvent('start::after', $this);
    }

    public function hasStarted(): bool {
        return session_status() == PHP_SESSION_ACTIVE;
    }

    protected function syncCookie(): static {
        $this->getCookie()
            ->setName($this->getName())
            ->setValue($this->getId())
            ->setExpires($this->getExpires());

        return $this;
    }

    protected function writeCookie(): static {
        $this->getCookie()->write();

        return $this;
    }

    /**
     * Invalidate session on both sides: server AND client
     * (delete data + send a "delete cookie")
     */
    public function invalidate(): static {
        // Delete in-memory data (similar to session_unset())
        $this->clearData();

        // Delete stored data and destroy session (see \Session::destroy())
        $this->destroy();

        // Send a *delete* cookie to client with invalidated session ID
        $this->invalidateClient();

        return $this;
    }

    /**
     * Invalidate session client side only (send a "delete cookie")
     * The session data on the server is left untouched.
     */
    public function invalidateClient(): static {
        $this->getCookie()->delete();

        return $this;
    }

    /**
     * Generate a new ID for the current PHP session and send a cookie update.
     */
    public function regenerate(bool $regenerateCsrfToken = true): static {
        // Generate new session ID (and destroy old session)
        if (! session_regenerate_id(true)) {
            throw new SessionException('Could not regenerate session');
        }

        // Send it to client via cookie
        $this->syncCookie()
            ->writeCookie();

        if ($regenerateCsrfToken) {
            $this->regenerateCsrfToken();
        }

        return $this;
    }

    /**
     * Accessor wrapper for $_SESSION
     */
    public function getData(string $key): mixed {
        return $this->fw->get('SESSION.' . $key);
    }

    /**
     * Mutator wrapper for $_SESSION
     */
    public function setData(string $key, mixed $value): static {
        $this->fw->set('SESSION.' . $key, $value);
        $this->triggerInstanceEvent('setData::' . $key, $this, [$value]);

        return $this;
    }

    /**
     * Clear $_SESSION content but keep segments because they can be referenced elsewhere
     */
    protected function clearData(): static {
        $sessionData =& $this->fw->ref('SESSION');
        foreach ($sessionData as $k => &$v) {
            if (is_array($v) && str_starts_with((string) $k, self::SEGMENT_PREFIX)) {
                $v = [];
            }
            else {
                unset($sessionData[$k]);
            }
        }

        return $this;
    }

    public function &getSegment(string $segment): array {
        $hiveKey = sprintf('SESSION.%s%s', self::SEGMENT_PREFIX, $segment);
        if (!$this->fw->exists($hiveKey)) {
            $this->fw->set($hiveKey, []);
        }

        return $this->fw->ref($hiveKey);
    }

    public function getCsrfToken(): ?string {
        return $this->getData(self::KEY_CSRF_TOKEN);
    }

    public function setCsrfToken(string $token): static {
        return $this->setData(self::KEY_CSRF_TOKEN, $token);
    }

    public function regenerateCsrfToken(): static {
        return $this->setCsrfToken($this->cryptService->generateToken(16));
    }

    /**
     * End current session without altering already stored data.
     *
     * @param bool $clear Also clear session storage
     * @return $this
     */
    public function abortClose(bool $clear = false): static {
        session_abort();
        if ($this->f3Session) {
            $this->f3Session->close();
        }
        if ($clear) {
            $this->clearData();
        }

        return $this;
    }

    /**
     * End session and write its content.
     */
    protected function writeClose(): static {
        session_write_close();

        return $this;
    }

    public function commit(): static {
        return $this->writeClose();
    }

    protected function destroy(): static {
        session_destroy();

        return $this;
    }

    public function getId(): ?string {
        return $this->f3Session ? $this->f3Session->sid() : null;
    }

    public function getStorage(): \Cache {
        return $this->storage;
    }

    public function getCookie(): \OliveOil\Core\Model\Cookie {
        return $this->cookie;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): static {
        $this->name = $name;

        return $this;
    }

    public function getExpires(): int {
        return (int) ($this->hasStarted() ? $this->getData(self::KEY_EXPIRES) : $this->expires);
    }

    public function setExpires(int $expires): static {
        $this->expires = $expires;

        // Also set value to PHP session data
        if ($this->hasStarted()) {
            $this->setData(self::KEY_EXPIRES, $expires);
        }

        return $this;
    }

    public function isSuspect(): bool {
        return $this->isSuspect;
    }
}
