<?php

namespace OliveOil\Core\Model\Session\Http\PhpSession;


use OliveOil\Core\Exception\SessionException;
use OliveOil\Core\Model\Session\Constants;
use OliveOil\Core\Model\Session\Http\PhpSession;

/**
 * Warning: must be triggered *after* ExpireObserver or it may renew an already expired session!
 */
class TypeObserver implements Constants
{
    public function __construct(
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
    }

    /**
     * @throws SessionException
     */
    public function __invoke(PhpSession $session, array $args): void {
        if ($type = (string) $session->getData(self::KEY_TYPE)) {
            // Update session "expires" timestamp according to $type
            $session->setExpires($this->getNewExpirationTime($type));
        }
    }

    protected function getNewExpirationTime(string $type): int {
        return ($lifetime = (int) $this->getTypeAppConfigValue($type, 'lifetime')) !== 0 ? time() + $lifetime : 0;
    }

    public function getTypeAppConfigValue(string $type, string $key, mixed $default = null): mixed {
        return $this->appConfig->getValue(
            sprintf('SESSION_TYPE.%s.%s', $type, $key),
            $default
        );
    }
}
