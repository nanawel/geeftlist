<?php
namespace OliveOil\Core\Model;

class TransportObject extends MagicObject
{
    /**
     * @return mixed[]
     */
    public function forExtract(): array {
        $data = [];
        foreach ($this->getData() as $key => $value) {
            $data[lcfirst(self::camelize($key))] = $value;
        }

        return $data;
    }
}
