<?php
namespace OliveOil\Core\Model;

use OliveOil\Core\Iface\DynamicDataMethodsAware;
use OliveOil\Core\Traits\DynamicDataMethodsTrait;

class MagicObject implements DynamicDataMethodsAware
{
    use DynamicDataMethodsTrait;

    public function __construct(
        array $data = []
    ) {
        $this->_data = $data;
    }

    /**
     * Extract all values of $key from given array of MagicObject
     *
     * @param MagicObject[] $array
     * @param string $key
     */
    public static function getAllDataValues(array $array, $key): array {
        return array_reduce(
            $array,
            static fn(array $carry, MagicObject $item): array
                => array_merge($carry, [$item->getDataUsingMethod($key)]),
            []
        );
    }
}
