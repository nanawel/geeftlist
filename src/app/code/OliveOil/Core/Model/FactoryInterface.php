<?php

namespace OliveOil\Core\Model;

use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Service\GenericFactoryInterface;

interface FactoryInterface extends GenericFactoryInterface
{
    /**
     * @param string $code Entity code
     */
    public function makeCollectionFromCode(string $code, array $parameters = []): Collection;

    /**
     * @param string $rcn Relative Class Name
     */
    public function resolveMakeCollection(string $rcn, array $parameters = []): Collection;

    /**
     * @param string $rcn Relative Class Name
     */
    public function resolveCollection(string $rcn, array $parameters = []): string;
}
