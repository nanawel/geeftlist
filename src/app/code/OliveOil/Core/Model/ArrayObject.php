<?php

namespace OliveOil\Core\Model;


/**
 * Class ArrayObject
 *
 * This class is only here to be able to use \ArrayObject in DI definitions
 * (PHP internal classes are not supported)
 */
class ArrayObject extends \ArrayObject
{
    public function __construct($input = [], $flags = 0, $iterator_class = 'ArrayIterator') {
        parent::__construct($input, $flags, $iterator_class);
    }

    public function clear(): void {
        $this->exchangeArray([]);
    }
}
