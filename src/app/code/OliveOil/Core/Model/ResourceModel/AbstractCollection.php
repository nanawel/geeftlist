<?php
declare(strict_types=1);

namespace OliveOil\Core\Model\ResourceModel;

use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\MagicObject;
use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Model\Traits\FlagTrait;
use OliveOil\Core\Model\Traits\ListenableInstanceTrait;

/**
 * @template T of \OliveOil\Core\Model\AbstractModel
 * @implements \OliveOil\Core\Model\ResourceModel\Iface\Collection<T>
 */
abstract class AbstractCollection extends MagicObject implements Collection
{
    use FlagTrait;
    use ListenableInstanceTrait;

    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    /** @var T|null */
    protected ?\OliveOil\Core\Model\AbstractModel $itemSampleInstance;

    /** @var array<T>|null */
    protected ?array $items = [];

    protected bool $isLoaded = false;

    protected array $columns = [Select::SQL_STAR];

    protected array $filters = [];

    protected array $orders = [];

    protected bool|int $offset = false;

    protected bool|int $limit = false;

    /**
     * @param class-string $itemClass
     */
    public function __construct(
        protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory,
        protected \OliveOil\Core\Service\GenericFactoryInterface $resourceFactory,
        protected string $itemClass,
        protected mixed $idField,
        \OliveOil\Core\Service\EventInterface $eventService,
        array $data = []
    ) {
        $this->eventManager = $eventService->getEventManager($this);
        parent::__construct($data);
        $this->itemSampleInstance = $this->modelFactory->resolveMake($itemClass);
    }

    public function setItems(array $items): static {
        foreach ($items as $it) {
            $this->addItem($it);
        }

        return $this;
    }

    public function load(bool $force = false): static {
        if ($force || ! $this->isLoaded()) {
            $this->triggerEvent('load::before');
            $this->beforeLoad()
                ->applyConstraints()
                ->doLoad()
                ->afterLoad();

            $this->isLoaded = true;
            $this->triggerEvent('load::after');
        }

        return $this;
    }

    protected function applyConstraints(): static {
        $this->applyColumns()
            ->applyFilters()
            ->applyOrder()
            ->applyOffset()
            ->applyLimit();

        return $this;
    }

    public function getItems(): array {
        $this->load();

        return $this->items;
    }

    protected function beforeLoad(): static {
        return $this;
    }

    protected function afterLoad(): static {
        return $this;
    }

    protected function doLoad(): static {
        //to be overridden
        return $this;
    }

    public function isLoaded(): bool {
        return $this->isLoaded;
    }

    /**
     * @param T $item
     * @return $this
     */
    public function addItem(\OliveOil\Core\Model\AbstractModel $item): static {
        if ($this->idField !== null && ($key = $item->getDataUsingMethod($this->idField)) !== null) {
            $this->items[$key] = $item;
        }
        else {
            $this->items[] = $item;
        }

        $this->isLoaded = false;

        return $this;
    }

    /**
     * @return T
     */
    protected function newItem(array $data = []) {
        /** @var T $item */
        $item = $this->itemSampleInstance
            ? clone $this->itemSampleInstance
            : $this->modelFactory->make($this->itemClass);

        // Process model complex fields
        $fieldModels = $item->getFieldModels();
        foreach ($data as $field => $value) {
            if (array_key_exists($field, $fieldModels)) {
                $data[$field] = $item->getFieldModelInstance($field)->unserialize($item, $value);
            }
        }

        return $item->setData($data)
                ->setOrigData($data);
    }

    /**
     * @return T|null
     */
    public function getItem($value, $key = null, $strict = false): ?\OliveOil\Core\Model\AbstractModel {
        $this->load();
        if ($key === null) {
            $key = $this->idField;
        }

        if ($key === $this->idField) {
            return $this->items[$value] ?? null;
        }

        foreach ($this->items as $item) {
            if ($strict) {
                if ($item->getDataUsingMethod($key) === $value) {
                    return $item;
                }
            } elseif ($item->getDataUsingMethod($key) == $value) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @return T|null
     */
    public function getFirstItem(): ?\OliveOil\Core\Model\AbstractModel {
        $this->load();
        reset($this->items);

        return current($this->items) ?: null;
    }

    /**
     * @return T|null
     */
    public function getLastItem(): ?\OliveOil\Core\Model\AbstractModel {
        $this->load();
        end($this->items);

        return current($this->items) ?: null;
    }

    /**
     * @return int[]
     */
    public function getAllIds(): array {
        $this->load();

        return array_keys($this->items);
    }

    public function addFieldToSelect(array|string $field): static {
        // Nothing here for now
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addFieldToFilter(string $field, mixed $cond): static {
        $this->filters[] = [$field => $cond];
        $this->isLoaded = false;

        return $this;
    }

    public function getFilters(): array {
        return $this->filters;
    }

    public function clearFilters(): static {
        $this->filters = [];
        $this->isLoaded = false;

        return $this;
    }

    public function addJoinRelation(array|string $relation, mixed $joinConditions = null): static {
        // Nothing here for now
        return $this;
    }

    public function orderBy(string $field, int $dir = self::SORT_DIR_DEFAULT): static {
        if (!$dir) {
            $dir = self::SORT_DIR_DEFAULT;
        }

        $this->orders[$field] = $dir;
        $this->isLoaded = false;

        return $this;
    }

    public function clearOrders(): static {
        $this->orders = [];
        $this->isLoaded = false;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setOffset(int|bool $offset): static {
        $this->offset = $offset === false ? false : (int) $offset;

        return $this;
    }

    public function getOffset(): int|bool {
        return $this->offset;
    }

    /**
     * @inheritDoc
     */
    public function setLimit($limit): static {
        $this->limit = $limit === false ? false : (int) $limit;

        return $this;
    }

    public function getLimit(): int|bool {
        return $this->limit;
    }

    public function clear(): static {
        $this->items = [];
        $this->isLoaded = false;
        $this->flags = [];
        $this->columns = [Select::SQL_STAR];
        $this->clearFilters();
        $this->clearOrders();
        $this->offset = false;
        $this->limit = false;

        return $this;
    }

    public function getIdField(): ?string {
        return $this->idField;
    }

    public function delete(): static {
        return $this->walk('delete');
    }

    public function walk(string $itemMethod, array $args = []): static {
        $this->load();
        foreach ($this->items as $item) {
            call_user_func_array([$item, $itemMethod], $args);
        }

        return $this;
    }

    abstract protected function applyColumns(): static;

    abstract protected function applyFilters(): static;

    abstract protected function applyOrder(): static;

    abstract protected function applyOffset(): static;

    abstract protected function applyLimit(): static;

    /*
     * ArrayAccess Methods Implementation
     */

    public function offsetExists($offset): bool {
        $this->load();

        return isset($this->items[$offset]);
    }

    public function offsetGet($offset): mixed {
        $this->load();

        return $this->items[$offset];
    }

    public function offsetSet($offset, $value): void {
        throw new \BadMethodCallException('Setter is not supported.');
    }

    public function offsetUnset($offset): void {
        throw new \BadMethodCallException('Unsetter is not supported.');
    }

    /*
     * IterratorAggregate Methods Implementation
     */

    public function getIterator(): \Traversable {
        $this->load();

        return new \ArrayIterator($this->items);
    }

    /*
     * Countable Methods Implementation
     */

    public function count(): int {
        $this->load();

        return count($this->items);
    }

    /**
     * Triggers:
     *  - an instance event
     *  - an event via the currently attached EventManager
     * using the same params.
     *
     * @param string $eventName
     * @return $this
     */
    public function triggerEvent($eventName, array $params = []): static {
        $params += [
                'collection' => $this,
            ];

        $this->triggerInstanceEvent($eventName, $this, $params);
        $this->eventManager->trigger($eventName, $this, $params);

        return $this;
    }

    /**
     * @param \OliveOil\Core\Model\AbstractModel|int $id
     * @return int
     */
    public static function toId($id): int {
        if (is_array($id)) {
            throw new \InvalidArgumentException('ID cannot be an array.');
        }

        if (is_object($id)) {
            return $id->getId();
        }

        return (int) $id;
    }

    /**
     * @param \OliveOil\Core\Model\AbstractModel|\OliveOil\Core\Model\AbstractModel[]|int|int[] $id
     * @return int[]
     */
    public static function toIdArray($id): array {
        if (!is_array($id)) {
            $id = [$id];
        }

        $return = [];
        foreach ($id as $item) {
            $return[] = self::toId($item);
        }

        return $return;
    }
}
