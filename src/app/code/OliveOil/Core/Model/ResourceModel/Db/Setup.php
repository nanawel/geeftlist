<?php

namespace OliveOil\Core\Model\ResourceModel\Db;


use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Sql\Ddl;
use OliveOil\Core\Exception\Setup\InvalidSetupScriptException;
use OliveOil\Core\Model\SetupInterface;

class Setup extends AbstractModel implements \OliveOil\Core\Model\ResourceModel\Iface\Setup
{
    public const ACTION_INSTALL   = 'install';

    public const ACTION_UPGRADE   = 'upgrade';

    public const ACTION_UNINSTALL = 'uninstall';

    public const DIRECTION_UPGRADE   = 'upgrade';

    public const DIRECTION_DOWNGRADE = 'downgrade';

    public const VERSION_ZERO = '0.0.0';

    protected \OliveOil\Core\Service\Di\Container $container;

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \OliveOil\Core\Model\App\FlagInterface $appFlag;

    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Setup\Context $context,
        array $data = []
    ) {
        $this->container = $context->getContainer();
        $this->appConfig = $context->getAppConfig();
        $this->appFlag = $context->getAppFlag();
        parent::__construct($context, null, null, $data);
    }

    /**
     * @inheritDoc
     */
    public function isInstalled(\OliveOil\Core\Model\SetupInterface $model): bool {
        try {
            return $this->appFlag->has('app/version/' . SetupInterface::TYPE_SCHEMA);
        }
        catch (\PDOException $e) {
            if ($e->getCode() === '42S02') {
                return false;
            }

            throw $e;
        }
    }

    public function isDbUp(\OliveOil\Core\Model\SetupInterface $model): bool {
        try {
            $this->connection->getAdapter()
                ->query('SELECT 1', Adapter::QUERY_MODE_EXECUTE);

            return true;
        }
        catch (\Throwable $throwable) {
            if (
                !str_contains($throwable->getMessage(), 'Connection refused')
                && !str_contains($throwable->getMessage(), 'Connect Error')
            ) {
                throw $throwable;
            }
        }

        return false;
    }

    public function install(
        \OliveOil\Core\Model\SetupInterface $model,
        bool $dropFirst = false,
        ?string $targetVersion = null
    ): bool {
        if ($dropFirst) {
            $this->uninstall($model);
        }

        $upgradeAuto = $dropFirst || ! $this->isInstalled($model);

        $this->logger->info("Installing application...");
        $scriptsExecuted = 0;
        foreach ($this->getInstallScriptTypes() as $type) {
            $actualTargetVersion = $targetVersion ?? $this->getCodeVersion($model, $type);
            $scriptsExecuted += $this->applyScripts($model, $type, self::ACTION_INSTALL, $actualTargetVersion);
        }

        if ($upgradeAuto) {
            $this->upgrade($model, $targetVersion);
        }

        $this->logger->info("Installation finished successfully");

        return (bool) $scriptsExecuted;
    }

    public function upgrade(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null,
        bool $force = false
    ): bool {
        $this->logger->info("Starting upgrade...");

        $scriptsExecuted = 0;
        foreach ($this->getUpgradeScriptTypes() as $type) {
            $actualTargetVersion = $targetVersion ?? $this->getCodeVersion($model, $type);
            $scriptsExecuted += $this->applyScripts(
                $model,
                $type,
                self::ACTION_UPGRADE,
                $actualTargetVersion,
                self::DIRECTION_UPGRADE,
                $force
            );
        }

        $this->logger->info("Upgrade finished successfully");

        return (bool) $scriptsExecuted;
    }

    public function uninstall(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null,
        bool $force = false
    ): bool {
        $this->logger->info("Uninstalling application...");
        $scriptsExecuted = 0;
        if (! $this->isInstalled($model)) {
            $this->logger->notice("Application does not seem to be installed. Skipping.");
        }
        else {
            if ($targetVersion === null) {
                $targetVersion = static::VERSION_ZERO;
            }

            foreach ($this->getUninstallScriptTypes() as $type) {
                $scriptsExecuted += $this->applyScripts(
                    $model,
                    $type,
                    self::ACTION_UNINSTALL,
                    $targetVersion,
                    self::DIRECTION_DOWNGRADE,
                    $force
                );
            }

            $this->logger->info("Uninstallation finished successfully");
        }

        return (bool) $scriptsExecuted;
    }

    public function installSampleData(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null
    ): bool {
        $this->logger->info("Installing sample data...");
        $scriptsExecuted = 0;
        foreach ($this->getSampleDataInstallScriptTypes() as $type) {
            if ($targetVersion === null) {
                $targetVersion = $this->getCodeVersion($model, $type);
            }

            $scriptsExecuted += $this->applyScripts($model, $type, self::ACTION_INSTALL, $targetVersion);
        }

        $this->upgradeSampleData($model, $targetVersion);
        $this->logger->info("Sample data installation finished successfully");

        return (bool) $scriptsExecuted;
    }

    public function upgradeSampleData(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null
    ): bool {
        $this->logger->info("Upgrading sample data...");

        $scriptsExecuted = 0;
        foreach ($this->getSampleDataUpgradeScriptTypes() as $type) {
            if ($targetVersion === null) {
                $targetVersion = $this->getCodeVersion($model, $type);
            }

            $scriptsExecuted += $this->applyScripts($model, $type, self::ACTION_UPGRADE, $targetVersion);
        }

        $this->logger->info("Sample data upgrade finished successfully");

        return (bool) $scriptsExecuted;
    }

    public function uninstallSampleData(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null
    ): bool {
        $this->logger->info("Uninstalling sample data...");
        $scriptsExecuted = 0;
        if (! $this->isInstalled($model)) {
            $this->logger->notice("Application does not seem to be installed. Skipping.");
        }
        else {
            if ($targetVersion === null) {
                $targetVersion = static::VERSION_ZERO;
            }

            foreach ($this->getSampleDataUninstallScriptTypes() as $type) {
                $scriptsExecuted += $this->applyScripts(
                    $model,
                    $type,
                    self::ACTION_UNINSTALL,
                    $targetVersion,
                    self::DIRECTION_DOWNGRADE
                );
            }

            $this->logger->info("Sample data uninstallation finished successfully");
        }

        return (bool) $scriptsExecuted;
    }

    /**
     * @param string $targetVersion Target version to reach (included)
     * @return int The number of scripts executed
     * @throws InvalidSetupScriptException
     * @throws \OliveOil\Core\Exception\DatabaseException
     * @throws \Throwable
     */
    public function applyScripts(
        \OliveOil\Core\Model\SetupInterface $model,
        string $type,
        string $action,
        string $targetVersion,
        string $direction = self::DIRECTION_UPGRADE,
        bool $ignoreErrors = false
    ): int {
        $executedScripts = 0;
        if (! $scripts = $this->getScriptsByType($type, $action, $direction == self::DIRECTION_UPGRADE)) {
            $this->logger->notice(sprintf("No script found for type/action '%s/%s'.", $type, $action));
        }
        else {
            $currentVersion = $this->getDbVersion($model, $type);
            $this->logger->info(sprintf('* %s (Current version: %s. Target: %s)...', $type, $currentVersion, $targetVersion));
            if (version_compare($targetVersion, $currentVersion, '==')) {
                $this->logger->notice('Nothing to do!');
            }
            else {
                foreach ($scripts as $script => $scriptVersion) {
                    if (
                        ! $this->isScriptApplicable(
                            $scriptVersion['from'],
                            $scriptVersion['to'],
                            $currentVersion,
                            $targetVersion,
                            $direction,
                            true
                        )
                    ) {
                        continue;
                    }

                    $this->logger->info(sprintf('  - %s => %s', $scriptVersion['from'], $scriptVersion['to']));

                    if ($this->scriptTypeUsesTransaction($type)) {
                        $this->connection->beginTransaction();
                    }

                    try {
                        $this->runScript($model, $script);
                    }
                    catch (\Throwable $e) {
                        $this->logger->critical(
                            'An error occured during the execution of the setup script ' . $script
                        );
                        $this->logger->critical($e);

                        if ($this->scriptTypeUsesTransaction($type)) {
                            $this->connection->rollbackTransaction();
                        }

                        if (!$ignoreErrors) {
                            throw $e;
                        }
                    }

                    ++$executedScripts;

                    try {
                        $this->setDbVersion($model, $type, $scriptVersion['to']);
                        $currentVersion = $scriptVersion['to'];
                    }
                    catch (\PDOException $e) {
                        // Only raise exception if not downgrading (table might not be available anymore)
                        if ($direction != self::DIRECTION_DOWNGRADE) {
                            throw $e;
                        }

                        $this->logger->warning(
                            sprintf("Could not update current application's version in database. (%s)", $e->getMessage())
                        );
                    }

                    if ($this->scriptTypeUsesTransaction($type)) {
                        $this->connection->commitTransaction();
                    }
                }
            }
        }

        return $executedScripts;
    }

    public function isScriptApplicable(
        string $scriptSourceVersion,
        string $scriptTargetVersion,
        string $currentVersion,
        string $targetVersion,
        string $direction,
        bool $logResult = false
    ): bool {
        $applicable = true;
        if (
            version_compare(
                $scriptSourceVersion,
                $scriptTargetVersion,
                $direction == self::DIRECTION_UPGRADE ? '>=' : '<='
            )
        ) {
            if ($logResult) {
                $dir = $direction == self::DIRECTION_UPGRADE ? 'lower' : 'higher';
                $this->logger->error(
                    sprintf('Invalid script versions: %s should be %s than ', $scriptSourceVersion, $dir) .
                    sprintf('%s (%s specified).', $scriptTargetVersion, $direction)
                );
            }

            $applicable = false;
        }
        elseif (
            version_compare(
                $scriptTargetVersion,
                $targetVersion,
                $direction == self::DIRECTION_UPGRADE ? '>' : '<'
            )
        ) {
            if ($logResult) {
                $this->logger->notice(sprintf(
                    'Cannot apply script for version %s, target is %s. Ignoring.',
                    $scriptTargetVersion,
                    $targetVersion
                ));
            }

            $applicable = false;
        }
        elseif ($currentVersion != $scriptSourceVersion) {
            if ($logResult) {
                $this->logger->notice(sprintf(
                    'Cannot apply script for version %s to %s, currently at %s. Ignoring.',
                    $scriptSourceVersion,
                    $scriptTargetVersion,
                    $currentVersion
                ));
            }

            $applicable = false;
        }

        return $applicable;
    }

    /**
     * @param string $type schema|data|sampledata
     * @param string $action install|upgrade|uninstall
     * @param bool|null $orderAsc Sort result files by ascending or descending version. Use NULL to disable sorting.
     * @return array [scriptFilename => [from => (version), to => (version)], ...]
     */
    protected function getScriptsByType(string $type, string $action, ?bool $orderAsc = true): array {
        $scripts = [];
        foreach (glob(sprintf('%s/%s/%s-*.php', $this->getScriptsDir(), $type, $action)) as $scriptPath) {
            if (false === (preg_match('/.*?(\d+\.\d+\.\d+)(?:-(\d+\.\d+\.\d+))?\.php$/', $scriptPath, $m))) {
                throw new InvalidSetupScriptException(basename($scriptPath) . ' is not a valid setup script filename.');
            }

            $scripts[$scriptPath] = [
                'from' => isset($m[2]) ? $m[1] : static::VERSION_ZERO,
                'to'   => $m[2] ?? $m[1]
            ];
        }

        if ($orderAsc !== null) {
            uasort($scripts, static fn(array $a, array $b): int => version_compare($a['from'], $b['from']));
            if (! $orderAsc) {
                $scripts = array_reverse($scripts, true);
            }
        }

        return $scripts;
    }

    protected function getInstallScriptTypes(): array {
        return [SetupInterface::TYPE_SCHEMA, SetupInterface::TYPE_DATA];
    }

    protected function getUpgradeScriptTypes(): array {
        return $this->getInstallScriptTypes();
    }

    protected function getUninstallScriptTypes(): array {
        return [SetupInterface::TYPE_DATA, SetupInterface::TYPE_SCHEMA];
    }

    protected function getSampleDataInstallScriptTypes(): array {
        return [SetupInterface::TYPE_SAMPLEDATA];
    }

    protected function getSampleDataUpgradeScriptTypes(): array {
        return [SetupInterface::TYPE_SAMPLEDATA];
    }

    protected function getSampleDataUninstallScriptTypes(): array {
        return [SetupInterface::TYPE_SAMPLEDATA];
    }

    protected function runScript(\OliveOil\Core\Model\SetupInterface $model, $script) {
        $this->logger->debug(sprintf('Running setup script %s...', $script));
        $setup = $model;
        include $script;
        $this->logger->debug(sprintf('Setup script %s finished', $script));
    }

    public function getCodeVersion(\OliveOil\Core\Model\SetupInterface $model, string $type): string {
        return $this->appConfig->getValue(sprintf(
            'DB_VERSION_%s',
            strtoupper($type)
        ), static::VERSION_ZERO);
    }

    public function getDbVersion(\OliveOil\Core\Model\SetupInterface $model, string $type): string {
        if (! $this->isInstalled($model)) {
            return static::VERSION_ZERO;
        }

        return $this->appFlag->get('app/version/' . $type, static::VERSION_ZERO);
    }

    /**
     * @inheritDoc
     */
    public function setDbVersion(\OliveOil\Core\Model\SetupInterface $model, string $type, string $version): static {
        $this->appFlag->set('app/version/' . $type, $version);

        return $this;
    }

    protected function getScriptsDir(): string {
        return rtrim((string) $this->appConfig->getValue('SETUP_SCRIPTS_DIR'), '/');
    }

    /**
     * @param array $errorsToIgnore
     * @return $this
     */
    public function runAndIgnoreErrors(callable $callback, $errorsToIgnore = [42000]): static {
        try {
            $callback();
        }
        catch (\PDOException $pdoException) {
            if (! in_array($pdoException->getCode(), $errorsToIgnore)) {
                throw $pdoException;
            }
        }

        return $this;
    }

    /**
     * Little helper to easily drop foreign keys.
     * On MySQL (at least) you need to drop both the constraint and the index.
     *
     * @param string $table
     * @param string $fkName
     * @return $this
     */
    public function dropForeignKey($table, $fkName, $ignoreNotExisting = true): static {
        $alterStmt = new Ddl\AlterTable($table);
        $alterStmt->dropConstraint(new Ddl\Constraint\ForeignKey($fkName, null, '', null));
        if ($ignoreNotExisting) {
            $this->runAndIgnoreErrors(function () use ($alterStmt): void {
                $this->connection->query($alterStmt);
            });
        }
        else {
            $this->connection->query($alterStmt);
        }

        $alterStmt = new Ddl\AlterTable($table);
        $alterStmt->dropConstraint(new Ddl\Index\Index(null, $fkName));
        if ($ignoreNotExisting) {
            $this->runAndIgnoreErrors(function () use ($alterStmt): void {
                $this->connection->query($alterStmt);
            });
        }
        else {
            $this->connection->query($alterStmt);
        }

        return $this;
    }

    protected function scriptTypeUsesTransaction(string $scriptType): bool {
        return $scriptType !== SetupInterface::TYPE_SCHEMA;
    }

    public function getContainer(): \OliveOil\Core\Service\Di\Container {
        return $this->container;
    }

    public function getConnection(): \OliveOil\Core\Model\ResourceModel\Db\Connection {
        return $this->connection;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    /**
     * Only for backward compatibility. Must not be used in new setup scripts.
     *
     * @deprecated
     */
    public function getEnvConfig(): \OliveOil\Core\Model\App\FlagInterface {
        return $this->appFlag;
    }
}
