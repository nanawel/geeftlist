<?php

namespace OliveOil\Core\Model\ResourceModel\Db;


class Context
{
    public function __construct(protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection, protected \OliveOil\Core\Model\ResourceModel\Db\Transaction $transaction, protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory, protected \OliveOil\Core\Model\FactoryInterface $resourceModelFactory, protected \OliveOil\Core\Service\EventInterface $eventService, protected \OliveOil\Core\Service\Log $logService)
    {
    }

    public function getConnection(): \OliveOil\Core\Model\ResourceModel\Db\Connection {
        return $this->connection;
    }

    public function getTransaction(): \OliveOil\Core\Model\ResourceModel\Db\Transaction {
        return $this->transaction;
    }

    public function getModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->modelFactory;
    }

    public function getResourceModelFactory(): \OliveOil\Core\Model\FactoryInterface {
        return $this->resourceModelFactory;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }
}
