<?php

namespace OliveOil\Core\Model\ResourceModel\Db;


class Transaction implements \OliveOil\Core\Model\ResourceModel\Iface\Transaction
{
    public function __construct(protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection)
    {
    }

    /**
     * Execute $callable in a transaction
     *
     * @return mixed
     */
    public function execute(callable $callable) {
        try {
            $this->begin();
            $return = call_user_func($callable);
            $this->commit();
        }
        catch (\Throwable $throwable) {
            $this->rollback();

            throw $throwable;
        }

        return $return;
    }

    /**
     * @inheritDoc
     */
    public function begin(): static {
        $this->getConnection()->beginTransaction();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function commit(): static {
        $this->getConnection()->commitTransaction();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function rollback(): static {
        $this->getConnection()->rollbackTransaction();

        return $this;
    }

    public function getConnection(): \OliveOil\Core\Model\ResourceModel\Db\Connection {
        return $this->connection;
    }
}
