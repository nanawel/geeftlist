<?php

namespace OliveOil\Core\Model\ResourceModel\Db;

use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\MagicObject;
use OliveOil\Core\Model\ResourceModel\Iface\Model;

/**
 * Available hooks on save/load/delete and call order:
 *  - Model::beforeX (should trigger "before::X" event)
 *  - Resource::_beforeX (should trigger "before::X" event)
 *  - Resource::_afterX (should trigger "after::X" event)
 *  - Model::afterX (should trigger "after::X" event)
 *
 * @template T of  \OliveOil\Core\Model\AbstractModel
 * @implements \OliveOil\Core\Model\ResourceModel\Iface\Model<T>
 */
abstract class AbstractModel extends MagicObject implements Model
{
    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory;

    protected \OliveOil\Core\Model\FactoryInterface $resourceModelFactory;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \Psr\Log\LoggerInterface $logger;

    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Context $context,
        protected mixed $idField,
        protected ?string $mainTable,
        array $data = []
    ) {
        $this->connection = $context->getConnection();
        $this->modelFactory = $context->getModelFactory();
        $this->resourceModelFactory = $context->getResourceModelFactory();
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->logger = $context->getLogService()->getLoggerForClass($this);
        parent::__construct($data);
    }

    public function getMainTable(): ?string {
        return $this->mainTable;
    }

    public function getIdField(): ?string {
        return $this->idField;
    }

    protected function beforeLoad(\OliveOil\Core\Model\AbstractModel $object, $id, $field): static {
        $this->eventManager->trigger('load::before', $this, ['object' => $object, 'id' => $id, 'field' => $field]);

        return $this;
    }

    protected function afterLoad(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('load::after', $this, ['object' => $object]);

        return $this;
    }

    public function load(\OliveOil\Core\Model\AbstractModel $object, $id, $field = null): static {
        if ($field === null) {
            $field = $this->getIdField();
        }

        // "Before" hooks
        $object->beforeLoad($id, $field);
        $this->beforeLoad($object, $id, $field);

        $select = $this->connection->getSql($this->getMainTable())->select();
        $select->columns([Select::SQL_STAR])
            ->where([$field => $id])
            ->limit(1);

        $result = $this->connection->query($select);
        if ($row = (array) $result->current()) {
            $fieldModels = $object->getFieldModels();
            foreach ($row as $column => $value) {
                if (array_key_exists($column, $fieldModels)) {
                    $row[$column] = $object->getFieldModelInstance($column)->unserialize($object, $value);
                }
            }

            $object->setData($row)
                ->setOrigData($row)
                ->setDataChanges(false);

            // "After" hooks
            $this->afterLoad($object);
            $object->afterLoad();
        }

        return $this;
    }

    protected function beforeSave(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('save::before', $this, ['object' => $object]);

        return $this;
    }

    protected function afterSave(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('save::after', $this, ['object' => $object]);

        return $this;
    }

    protected function afterSaveCommit(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('save::after_commit', $this, ['object' => $object]);

        return $this;
    }

    public function save(\OliveOil\Core\Model\AbstractModel $object): static {
        // "Before" hooks
        $object->beforeSave();
        $this->beforeSave($object);

        $data = $this->prepareDataForSave($object);

        try {
            // Update
            $this->connection->beginTransaction();
            if (
                ! $object->getFlag('force_create')
                && ! $object->isObjectNew()
                && ($id = $object->getId())
            ) {
                if ($data && $object->hasDataChanges()) {
                    $query = $this->connection->update($this->getMainTable())
                        ->set($data)
                        ->where([$this->getIdField() => $id]);
                    $this->connection->query($query);
                }
            }
            // Insert
            else {
                if ($data === []) {
                    throw new \RuntimeException('Object has no data to save.');
                }

                $query = $this->connection->insert($this->getMainTable())
                    ->columns(array_keys($data))
                    ->values($data);
                $this->connection->query($query);

                $id = $this->connection->getAdapter()->getDriver()->getLastGeneratedValue();
                $object->setId($id);
            }

            // "After" hooks
            $this->afterSave($object);
            $object->afterSave();

            $this->connection->commitTransaction();
        }
        catch (\Throwable $throwable) {
            $this->connection->rollbackTransaction();

            throw $throwable;
        }

        $this->afterSaveCommit($object);
        $object->afterSaveCommit();

        return $this;
    }

    /**
     * @param T $object
     */
    protected function prepareDataForSave(\OliveOil\Core\Model\AbstractModel $object): array {
        $columns = $this->connection->getTableColumns($this->getMainTable());
        $fieldModels = $object->getFieldModels();
        $data = [];
        $idField = $this->getIdField();
        foreach ($columns as $c) {
            if ($c == $idField) {
                continue;
            }

            if ($object->getOrigData($c) !== $object->getData($c)) {
                if (array_key_exists($c, $fieldModels)) {
                    $data[$c] = $object->getFieldModelInstance($c)->serialize($object, $object->getData($c));
                }
                else {
                    $data[$c] = $object->getData($c);
                }
            }
        }

        return $data;
    }

    protected function beforeDelete(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('delete::before', $this, ['object' => $object]);

        return $this;
    }

    protected function afterDelete(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('delete::after', $this, ['object' => $object]);

        return $this;
    }

    protected function afterDeleteCommit(\OliveOil\Core\Model\AbstractModel $object): static {
        $this->eventManager->trigger('delete::after_commit', $this, ['object' => $object]);

        return $this;
    }

    public function delete(\OliveOil\Core\Model\AbstractModel $object): static {
        // "Before" hooks
        $object->beforeDelete();
        $this->beforeDelete($object);

        if (! $object->getId()) {
            throw new \Exception('Cannot delete not loaded object');
        }

        try {
            $this->connection->beginTransaction();
            $delete = $this->connection->delete($this->getMainTable())
                ->where([$this->getIdField() => $object->getId()]);
            $this->connection->query($delete);

            // "After" hooks
            $this->afterDelete($object);
            $object->afterDelete();

            $this->connection->commitTransaction();
        }
        catch (\Throwable $throwable) {
            $this->connection->rollbackTransaction();

            throw $throwable;
        }

        $this->afterDeleteCommit($object);

        return $this;
    }

    public function getModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->modelFactory;
    }

    public function getResourceModelFactory(): \OliveOil\Core\Model\FactoryInterface {
        return $this->resourceModelFactory;
    }

    public function getLogger(): \Psr\Log\LoggerInterface {
        return $this->logger;
    }
}
