<?php

namespace OliveOil\Core\Model\ResourceModel\Db;


use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\In;
use Laminas\Db\Sql\Predicate\IsNotNull;
use Laminas\Db\Sql\Predicate\IsNull;
use Laminas\Db\Sql\Predicate\Like;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\NotIn;
use Laminas\Db\Sql\Predicate\NotLike;
use Laminas\Db\Sql\Predicate\Operator;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Exception\RuntimeException;
use OliveOil\Core\Model\ResourceModel\Db\Collection\RelationHelperInterface;

/**
 * @template T of \OliveOil\Core\Model\AbstractModel
 * @extends \OliveOil\Core\Model\ResourceModel\AbstractCollection<T>
 */
abstract class AbstractCollection extends \OliveOil\Core\Model\ResourceModel\AbstractCollection
{
    public const NORMALIZED_OPERATOR_MAPPING = [
        'eq'    => 'eq',
        '='     => 'eq',
        '=='    => 'eq',
        'ne'    => 'ne',
        'neq'   => 'ne',
        '!='    => 'ne',
        'gt'    => 'gt',
        '>'     => 'gt',
        'ge'    => 'ge',
        '>='    => 'ge',
        'lt'    => 'lt',
        '<'     => 'lt',
        'le'    => 'le',
        '<='    => 'le',
        'null'  => 'null',
        'in'    => 'in',
        'nin'   => 'nin',
        'like'  => 'like',
        'nlike' => 'nlike',
    ];

    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    protected ?\Laminas\Db\Sql\Select $select;

    protected array $joinRelations = [];

    /** @var array[] */
    protected array $joinedTables = [];

    protected bool $isApplyingConstraints = false;

    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\Collection\RelationHelperInterface[] $relationHelpers
     */
    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Context $context,
        string $itemClass,
        string|array|null $idField,
        protected string $mainTable,
        protected array $relationHelpers = [],
        array $data = []
    ) {
        $this->connection = $context->getConnection();
        $this->select = $this->newSelect();
        parent::__construct(
            $context->getModelFactory(),
            $context->getResourceModelFactory(),
            $itemClass,
            $idField,
            $context->getEventService(),
            $data
        );
    }

    protected function newSelect(): Select {
        return $this->connection->getSql()->select(['main_table' => $this->mainTable]);
    }

    public function getMainTable(): string {
        return $this->mainTable;
    }

    public function __clone() {
        if ($this->select) {
            $this->select = clone $this->select;
        }
    }

    public function getSelect(): \Laminas\Db\Sql\Select {
        return $this->select;
    }

    public function clear(): static {
        $this->select = $this->newSelect();

        return parent::clear();
    }

    /**
     * For debug purposes only
     */
    public function dumpSelect(): string {
        return $this->connection->getSql($this->getMainTable())
            ->buildSqlString($this->getFullSelect());
    }

    protected function getFullSelect(): \Laminas\Db\Sql\Select {
        $clone = clone $this;
        if (! $clone->isLoaded()) {
            $clone->applyConstraints();
        }

        return $clone->getSelect();
    }

    protected function applyConstraints(): static {
        try {
            $this->isApplyingConstraints = true;
            $this->applyJoinRelations();
            $this->applyJoinTables();

            return parent::applyConstraints();
        } finally {
            $this->isApplyingConstraints = false;
        }
    }

    protected function doLoad(): static {
        $items = $this->execLoadQuery();
        foreach ($items as $it) {
            $this->addItem($this->newItem($it));
        }

        return $this;
    }

    protected function execLoadQuery(): array {
        $this->triggerEvent('execLoadQuery::before');
        $items = $this->connection->query($select = $this->getSelect());
        $itemsAsArray = $items->toArray();
        $this->triggerEvent('execLoadQuery::after', ['select' => $select, 'items' => $itemsAsArray]);

        return $itemsAsArray;
    }

    /**
     * @param array|string $relation
     * @param array|PredicateInterface|null $joinConditions Ignored if $relation is an array
     * @return $this
     */
    public function addJoinRelation($relation, $joinConditions = null): static {
        if (!is_array($relation)) {
            $relation = [$relation => $joinConditions];
        }

        foreach ($relation as $tableOrRelationName => $params) {
            $this->joinRelations[$tableOrRelationName] = $params;
        }

        return $this;
    }

    protected function applyJoinTables(): static {
        foreach ($this->joinedTables as $joinArgs) {
            $this->applyJoinTable(...$joinArgs);
        }

        return $this;
    }

    protected function applyJoinTable(...$joinArgs): static {
        $this->getSelect()
            ->join(...$joinArgs);

        return $this;
    }

    protected function applyJoinRelations(): static {
        foreach ($this->joinRelations as $relationName => $joinConditions) {
            $handlerFound = false;
            foreach ($this->relationHelpers as $relationHelper) {
                if ($relationHelper->canHandleJoinRelation($relationName)) {
                    $handlerFound = true;
                    $relationHelper->addJoinRelation($this, $relationName, $joinConditions);
                }
            }

            if (!$handlerFound) {
                throw new RuntimeException(sprintf(
                    'Cannot find a proper handler for relation %s on class %s.',
                    $relationName,
                    static::class
                ));
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addFieldToSelect($field): static {
        if (!is_array($field)) {
            $field = [$field => $field];
        }

        foreach ($field as $correlationName => $fieldName) {
            $this->columns[$correlationName] = $fieldName;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function applyColumns(): static {
        if ($this->columns === []) {
            $this->columns[] = Select::SQL_STAR;
        }

        $this->getSelect()->columns($this->columns);

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function applyFilters(): static {
        foreach ($this->filters as $filter) {
            $predicate = null;
            if ($filter instanceof Operator) {
                $predicate = $filter;
            }
            else {
                $field = key($filter);
                $cond = $this->normalizeCondition(current($filter));
                $operator = key($cond);

                foreach ($this->relationHelpers as $relationHelper) {
                    if ($relationHelper->canHandleFilterPredicate($field, $operator)) {
                        $predicate = $relationHelper->getFilterPredicate($field, $operator, current($cond));
                        break;
                    }
                }

                // Fallback to default handling
                if (!$predicate) {
                    $predicate = $this->filterToPredicate($field, $cond);
                }
            }

            // Make sure each predicate is nested in its own pair or parenthesis
            $this->getSelect()->where
                ->nest()->addPredicates($predicate)->unnest();
        }

        return $this;
    }

    /**
     * @param string|array $condition
     */
    protected function normalizeCondition($condition): array {
        if (is_array($condition) && count($condition) === 1) {
            $operator = key($condition);
            $value = current($condition);
        } else {
            $operator = 'eq';
            $value = $condition;
        }

        if (!array_key_exists($operator, static::NORMALIZED_OPERATOR_MAPPING)) {
            throw new \InvalidArgumentException('"' . $operator . '" is not a valid operator.');
        }

        return [
            static::NORMALIZED_OPERATOR_MAPPING[$operator] => $value
        ];
    }

    protected function filterToPredicate(string $field, array $condition): PredicateInterface {
        $operator = key($condition);
        $value = current($condition);
        if (is_null($value)) {
            throw new \InvalidArgumentException('NULL is not a valid value for a condition.');
        }

        switch ($operator) {
            case 'eq':
                $predicate = new Operator($field, Operator::OP_EQ, $value);
                break;

            case 'ne':
                $predicate = new Operator($field, Operator::OP_NE, $value);
                break;

            case 'gt':
                $predicate = new Operator($field, Operator::OP_GT, $value);
                break;


            case 'ge':
                $predicate = new Operator($field, Operator::OP_GTE, $value);
                break;

            case 'lt':
                $predicate = new Operator($field, Operator::OP_LT, $value);
                break;

            case 'le':
                $predicate = new Operator($field, Operator::OP_LTE, $value);
                break;

            case 'null':
                $predicate = $value ? new IsNull($field) : new IsNotNull($field);
                break;

            case 'in':
                // Force array if scalar provided
                if (is_scalar($value)) {
                    $value = [$value];
                }

                // MySQL does not support empty set in IN clause
                if ($value === []) {
                    $predicate = new Literal('FALSE');
                }
                else {
                    $predicate = new In($field, $value);
                }

                break;

            case 'nin':
                // Force array if scalar provided
                if (is_scalar($value)) {
                    $value = [$value];
                }

                // MySQL does not support empty set in NOT IN clause
                if ($value === []) {
                    $predicate = new Literal('TRUE');
                }
                else {
                    $predicate = new NotIn($field, $value);
                }

                break;

            case 'like':
                $predicate = new Like($field, $value);
                break;

            case 'nlike':
                $predicate = new NotLike($field, $value);
                break;

            default:
                throw new \InvalidArgumentException('"' . $operator . '" is not a valid operator');
        }

        return $predicate;
    }

    /**
     * @inheritDoc
     */
    protected function applyOrder(): static {
        if ($this->orders !== []) {
            $orderBy = [];
            foreach ($this->orders as $field => $dir) {
                if ($field === static::ORDER_BY_RANDOM) {
                    $orderBy[] = new Expression('RAND()');
                }
                else {
                    $orderBy[] = $field . ' ' . ($dir == SORT_DESC ? 'DESC' : 'ASC');
                }
            }

            $this->getSelect()->order($orderBy);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function applyLimit(): static {
        if ($this->limit !== false) {
            $this->getSelect()->limit($this->limit);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function applyOffset(): static {
        if ($this->offset !== false) {
            $this->getSelect()->offset($this->offset);
        }

        return $this;
    }

    /**
     * @param string|array $table
     * @param string|mixed $cond
     * @param string|array $columns
     * @param bool $prefixColumnsWithTable
     * @return $this
     */
    public function joinTable(
        $table,
        $cond,
        $columns = Select::SQL_STAR,
        mixed $type = Select::JOIN_INNER,
        $prefixColumnsWithTable = false
    ): static {
        $tableAlias = $this->connection->getTableAlias($table);
        if (! is_array($columns)) {
            $columns = [$columns];
        }

        if ($prefixColumnsWithTable) {
            $tableCols = $this->connection->getTableColumns($table);
            if (count($columns) === 1 && $columns[0] === Select::SQL_STAR) {
                $columns = [];
                foreach ($tableCols as $tc) {
                    $columns[$tableAlias . '_' . $tc] = $tc;
                }
            }
            else {
                foreach ($columns as $c) {
                    // Only alter non-aliased columns
                    if (! is_array($c)) {
                        $columns[$tableAlias . '_' . $c] = $c;
                        unset($columns[$c]);
                    }
                }
            }
        }

        $joinData = [$table, $cond, $columns, $type];
        if (isset($this->joinedTables[$tableAlias])) {
            // Same join already applied? => Just ignore, otherwise throw exception
            if ($this->joinedTables[$tableAlias] != $joinData) {
                throw new RuntimeException(
                    sprintf("Cannot join table: an alias with the same name '%s' already exists.", $tableAlias)
                );
            }
        } elseif ($this->isApplyingConstraints) {
            // Do not defer join application if currently applying constraints
            $this->applyJoinTable(...$joinData);
        } else {
            $this->joinedTables[$tableAlias] = $joinData;
        }

        return $this;
    }

    /**
     * Lightweight deletion using a DELETE with the IDs retrieved from this collection.
     *
     * {@inheritDoc}
     */
    public function delete(): static {
        if (!is_string($this->idField)) {
            throw new NotImplementedException(
                'Mass deletion is only implemented for collections with string-based IDs.'
            );
        }

        $this->triggerEvent('delete::before');
        $delete  = $this->connection->delete($this->getMainTable());
        $clone = clone $this;
        if ($ids = $clone->getAllIds()) {
            $delete->where->in($this->getIdField(), $ids);
            $this->connection->query($delete);
        }

        $this->triggerEvent('delete::after', ['deleted_ids' => $ids]);

        return $this;
    }

    /*
     * \Countable Methods Implementation
     */

    public function count(): int {
        $this->load();

        return count($this->items);
    }

    public function getConnection(): \OliveOil\Core\Model\ResourceModel\Db\Connection {
        return $this->connection;
    }

    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\Collection\RelationHelperInterface[] $relationHelpers
     */
    public function setRelationHelpers(array $relationHelpers): static {
        $this->relationHelpers = $relationHelpers;

        return $this;
    }

    public function addRelationHelpers(RelationHelperInterface $relationHelper): static {
        $this->relationHelpers[] = $relationHelper;

        return $this;
    }
}
