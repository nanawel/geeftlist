<?php

namespace OliveOil\Core\Model\ResourceModel\Db\Collection;

use Laminas\Db\Sql\Predicate\PredicateInterface;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;

interface RelationHelperInterface
{
    /**
     * @param string $relationName
     * @param string $operator
     * @return bool
     */
    public function canHandleFilterPredicate($relationName, $operator);

    /**
     * @param string $relationName
     * @param string $operator
     * @return PredicateInterface|array
     */
    public function getFilterPredicate($relationName, $operator, mixed $value);

    /**
     * @param string $relationName
     * @return bool
     */
    public function canHandleJoinRelation($relationName);

    /**
     * @param string $relationName
     * @param array|null $joinConditions
     * @return void
     */
    public function addJoinRelation(AbstractCollection $collection, $relationName, $joinConditions);
}
