<?php

namespace OliveOil\Core\Model\ResourceModel\Db;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\AbstractResultSet;
use Laminas\Db\Sql\AbstractSql;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Insert;
use Laminas\Db\Sql\InsertMultiple;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\SqlInterface;
use Laminas\Db\Sql\TableIdentifier;
use Laminas\Db\Sql\Update;
use OliveOil\Core\Exception\DatabaseException;

class Connection
{
    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \Psr\Log\LoggerInterface $logger;

    protected ?\Laminas\Db\Metadata\MetadataInterface $dbMetadata = null;

    protected bool $adapterInititialized = false;

    protected int $transactionLevel = 0;

    protected bool $isRolledback = false;

    /**
     * @param bool $enableQueryLog
     */
    public function __construct(
        protected \Base $fw,
        protected \Laminas\Db\Adapter\Adapter $adapter,
        \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Cache $cache,
        \OliveOil\Core\Service\Log $logService,
        protected $enableQueryLog = true
    ) {
        $this->eventManager = $eventService->getEventManager($this);
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function __destruct() {
        if ($this->transactionLevel > 0) {
            $this->logger->warning(sprintf(
                'Transaction level should be 0 but found: %d',
                $this->transactionLevel
            ));
        }
    }

    public function getAdapter(): \Laminas\Db\Adapter\Adapter {
        $adapter = $this->adapter;
        if (! $this->adapterInititialized) {
            $this->onAdapterInit();
            $this->adapterInititialized = true;
        }

        return $adapter;
    }

    protected function onAdapterInit(): static {
        // Force internal date/time to be stored as UTC
        $this->adapter->query(
            "SET time_zone = '+00:00'; SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci;",
            Adapter::QUERY_MODE_EXECUTE
        );

        return $this;
    }

    public function getSql(?string $mainTable = null): \Laminas\Db\Sql\Sql {
        if ($mainTable) {
            return new \Laminas\Db\Sql\Sql($this->getAdapter(), ['main_table' => $mainTable]);
        }

        return new \Laminas\Db\Sql\Sql($this->getAdapter());
    }

    public function getMetadata(): \Laminas\Db\Metadata\MetadataInterface {
        if (!$this->dbMetadata) {
            $this->dbMetadata = \Laminas\Db\Metadata\Source\Factory::createSourceFromAdapter($this->getAdapter());
        }

        return $this->dbMetadata;
    }

    /**
     * @return string[]
     */
    public function getTableColumns(string|array $table): array {
        $table = $this->getRawTableName($table);
        $cacheKey = $this->getCacheKey('columns.' . $table);
        if ($this->cache->has($cacheKey)) {
            $columns = $this->cache->get($cacheKey);
        }
        else {
            $columnsInfo = $this->getMetadata()
                ->getColumns($table);
            $columns = [];
            foreach ($columnsInfo as $c) {
                $columns[] = $c->getName();
            }

            $this->cache->set($cacheKey, $columns);
        }

        return $columns;
    }

    public function getRawTableName(string|array $table): string {
        if (is_array($table)) {
            $table = (string) current($table);
        }

        return $table;
    }

    public function getTableAlias(string|array $table): string {
        if (is_array($table)) {
            $table = (string) key($table);
        }

        return $table;
    }

    /**
     * @param null|string|array|TableIdentifier $table
     */
    public function select($table = null): \Laminas\Db\Sql\Select {
        return new Select($table);
    }

    /**
     * @param null|string|array|TableIdentifier $table
     */
    public function insert($table): \Laminas\Db\Sql\Insert {
        return new Insert($table);   //Cannot use $this->getSql()->insert() because alias makes Zend quoting crash later
    }

    /**
     * @param null|string|array|TableIdentifier $table
     */
    public function insertMultiple($table): \Laminas\Db\Sql\InsertMultiple {
        return new InsertMultiple($table);   //Cannot use $this->getSql()->insert() because alias makes Zend quoting crash later
    }

    /**
     * @param null|string|array|TableIdentifier $table
     */
    public function update($table): \Laminas\Db\Sql\Update {
        return new Update($table);   //Cannot use $this->getSql()->update() because alias makes Zend quoting crash later
    }

    public function delete($table): \Laminas\Db\Sql\Delete {
        return new Delete($table);   //Cannot use $this->getSql()->delete() because alias makes Zend quoting crash later
    }

    /**
     * @param AbstractSql $sql
     * @param string|null $mode
     * @param bool $logException
     * @return \Laminas\Db\Adapter\Driver\StatementInterface|\Laminas\Db\ResultSet\AbstractResultSet
     * @throws \Throwable
     */
    public function query(SqlInterface $sql, ?string $mode = Adapter::QUERY_MODE_EXECUTE, bool $logException = true) {
        if ($mode === null) {
            $mode = Adapter::QUERY_MODE_EXECUTE;
        }

        try {
            $sqlString = $this->getSql(null)->buildSqlString($sql);
            if ($this->enableQueryLog) {
                $this->logger->debug($sqlString);
            }

            return $this->getAdapter()->query($sqlString, $mode);
        }
        catch (\Throwable $throwable) {
            if ($logException) {
                if (isset($sqlString)) {
                    $this->logger->error(sprintf(
                        'An error occured on query execution: %s. Query was: %s',
                        $throwable->getMessage(),
                        $sqlString
                    ));
                } else {
                    $this->logger->error(sprintf(
                        'An error occured on query build: %s.',
                        $throwable->getMessage()
                    ));
                }
            }

            throw $throwable;
        }
    }

    /**
     * @param string $column
     * @return mixed|null
     */
    public function fetchFirstValue(AbstractResultSet $resultSet, $column = null) {
        if ($resultSet->count()) {
            $row = (array)$resultSet->current();    // ResultSet returns an ArrayObject by default
            if ($column === null) {
                $column = key($row);
            }

            return $row[$column] ?? null;
        }

        return null;
    }

    public function fetchOne(AbstractResultSet $resultSet): ?array {
        if ($resultSet->count()) {
            return (array)$resultSet->current();    // ResultSet returns an ArrayObject by default
        }

        return null;
    }

    /**
     * @param string $column
     */
    public function fetchCol(AbstractResultSet $resultSet, $column, $indexColumn = null): array {
        if ($resultSet->count()) {
            $rows = $resultSet->toArray();
            return array_column($rows, $column, $indexColumn);
        }

        return [];
    }

    /**
     * @return $this
     * @throws DatabaseException
     */
    public function beginTransaction(): static {
        if ($this->isRolledback) {
            throw new DatabaseException('Cannot begin transaction: previous transaction has been rolled back');
        }

        if ($this->transactionLevel === 0) {
            $this->getAdapter()->getDriver()->getConnection()->beginTransaction();
        }

        ++$this->transactionLevel;

        return $this;
    }

    /**
     * @return $this
     * @throws DatabaseException
     */
    public function commitTransaction(): static {
        if ($this->isRolledback) {
            throw new DatabaseException('Cannot commit: previous transaction has been rolled back');
        }

        if ($this->transactionLevel === 1) {
            $this->getAdapter()->getDriver()->getConnection()->commit();
            $this->isRolledback = false;
        }
        elseif ($this->transactionLevel === 0) {
            throw new DatabaseException('Cannot commit: no pending transaction');
        }

        --$this->transactionLevel;

        return $this;
    }

    /**
     * @return $this
     * @throws DatabaseException
     */
    public function rollbackTransaction(): static {
        if ($this->transactionLevel === 1) {
            $this->getAdapter()->getDriver()->getConnection()->rollback();
            $this->isRolledback = false;
        }
        elseif ($this->transactionLevel === 0) {
            throw new DatabaseException('Cannot rollback: no pending transaction');
        }
        else {
            $this->isRolledback = true;
        }

        --$this->transactionLevel;

        return $this;
    }

    /**
     * @param string $table
     * @param array|string $columns
     * @param string $referenceColumn
     */
    public function getForeignKeyName($table, $columns, string $referenceTable, $referenceColumn): string {
        if (! is_array($columns)) {
            $columns = [$columns];
        }

        $parts = [
            'FK',
            $table,
            implode('_', $columns),
            $referenceTable,
            $referenceColumn
        ];
        $fkName = implode('_', $parts);

        // https://dev.mysql.com/doc/refman/5.7/en/identifiers.html
        if (strlen($fkName) > 64) {
            $parts = [
                'FK',
                $table,
                crc32(implode('_', $columns) . '_' . $referenceTable),
                $referenceColumn
            ];
            $fkName = implode('_', $parts);
        }

        return strtoupper($fkName);
    }

    /**
     * @param string $table
     * @param array|string $columns
     * @param string $type
     */
    public function getIndexName($table, $columns, $type = ''): string {
        if (! is_array($columns)) {
            $columns = [$columns];
        }

        $parts = ['IDX'];
        if ($type) {
            $parts[] = strtoupper($type);
        }

        $parts[] = $table;
        $parts = array_merge($parts, $columns);
        $idxName = implode('_', $parts);

        // https://dev.mysql.com/doc/refman/5.7/en/identifiers.html
        if (strlen($idxName) > 64) {
            $parts = ['IDX'];
            if ($type) {
                $parts[] = strtoupper($type);
            }

            $parts[] = $table;
            $parts[] = crc32(implode('_', $columns));
            $idxName = implode('_', $parts);
        }

        return strtoupper($idxName);
    }

    /**
     * @param string $key
     */
    protected function getCacheKey($key): string {
        return sprintf(
            '%s.%s',
            $this->getAdapter()->getCurrentSchema(),
            $key
        );
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }
}
