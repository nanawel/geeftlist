<?php

namespace OliveOil\Core\Model\ResourceModel\Db;

use Laminas\Db\Sql\Expression;
use OliveOil\Core\Model\ResourceModel\Iface\RelationHelperInterface;

/**
 * @template T1 of \OliveOil\Core\Model\AbstractModel
 * @template T2 of \OliveOil\Core\Model\AbstractModel
 * @template T1Collection of \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection<T1>
 * @template T2Collection of \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection<T2>
 */
abstract class AbstractRelationHelper implements RelationHelperInterface
{
    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    protected \OliveOil\Core\Model\ResourceModel\Db\Transaction $transaction;

    protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory;

    protected \OliveOil\Core\Model\FactoryInterface $resourceModelFactory;

    /** @var class-string */
    protected $aRelationClass;

    /** @var AbstractModel */
    protected $aResourceModel;

    /** @var class-string */
    protected $bRelationClass;

    /** @var AbstractModel */
    protected $bResourceModel;

    /**
     * @param string $mainTable
     * @param string $aEntityCode
     * @param string $bEntityCode
     * @param string $aRelationIdColumn
     * @param string $bRelationIdColumn
     */
    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Context $context,
        protected string $mainTable,
        protected string $aEntityCode,
        protected string $bEntityCode,
        protected ?string $aRelationIdColumn = null,
        protected ?string $bRelationIdColumn = null
    ) {
        $this->connection = $context->getConnection();
        $this->transaction = $context->getTransaction();
        $this->modelFactory = $context->getModelFactory();
        $this->resourceModelFactory = $context->getResourceModelFactory();
        $this->aRelationClass = $this->modelFactory->resolve($this->aEntityCode);
        $this->aResourceModel = $this->resourceModelFactory->resolveGet($this->aEntityCode);
        $this->aRelationIdColumn = $aRelationIdColumn ?: $this->aResourceModel->getIdField();
        $this->bRelationClass = $this->modelFactory->resolve($this->bEntityCode);
        $this->bResourceModel = $this->resourceModelFactory->resolveGet($this->bEntityCode);
        $this->bRelationIdColumn = $bRelationIdColumn ?: $this->bResourceModel->getIdField();
    }

    /**
     * @inheritDoc
     */
    public function addLinks(array $as, array $bs): int {
        \OliveOil\assertObjectArrayOfType($as, $this->aRelationClass);
        \OliveOil\assertObjectArrayOfType($bs, $this->bRelationClass);

        $aIds = array_map(static fn(\OliveOil\Core\Model\AbstractModel $a) => $a->getId(), $as);
        $bIds = array_map(static fn(\OliveOil\Core\Model\AbstractModel $b) => $b->getId(), $bs);

        if ($aIds === [] || $bIds === []) {
            return 0;
        }

        // Prepare new rows
        $newAssociations = [];
        foreach ($aIds as $aId) {
            foreach ($bIds as $bId) {
                $newAssociations[] = [
                    $this->aRelationIdColumn => $aId,
                    $this->bRelationIdColumn => $bId
                ];
            }
        }

        // Collect existing association
        $select = $this->connection
            ->select($this->mainTable)
            ->columns([$this->aRelationIdColumn, $this->bRelationIdColumn]);
        $select->where->in($this->aRelationIdColumn, $aIds ?: new Expression('FALSE'))
            ->and->in($this->bRelationIdColumn, $bIds ?: new Expression('FALSE'));
        $result = $this->connection->query($select);

        // Only insert non-existing associations
        $existingAssociations = $result->toArray();
        $newAssociations = \OliveOil\array_diff($newAssociations, $existingAssociations);

        if ($newAssociations) {
            $insert = $this->connection->insertMultiple($this->mainTable)
                ->columns([$this->aRelationIdColumn, $this->bRelationIdColumn])
                ->values($newAssociations);

            return $this->connection->query($insert)->count();
        }

        return 0;
    }

    /**
     * @inheritDoc
     */
    public function removeLinks(array $as, array $bs, bool $negate = false): int {
        \OliveOil\assertObjectArrayOfType($as, $this->aRelationClass);
        \OliveOil\assertObjectArrayOfType($bs, $this->bRelationClass);

        $aIds = array_map(static fn(\OliveOil\Core\Model\AbstractModel $a) => $a->getId(), $as);
        $bIds = array_map(static fn(\OliveOil\Core\Model\AbstractModel $b) => $b->getId(), $bs);

        if ($aIds === [] && $bIds === []) {
            throw new \InvalidArgumentException("\$as and \$bs cannot be both empty.");
        }

        if (!$negate && ($aIds === [] || $bIds === [])) {
            return 0;
        }

        $delete = $this->connection->delete($this->mainTable)
            ->from($this->mainTable);

        if ($aIds !== []) {
            $delete->where->in($this->aRelationIdColumn, $aIds);

            if ($negate) {
                $delete->where->notIn($this->bRelationIdColumn, $bIds ?: new Expression('TRUE'));
            }
            else {
                // If $bIds is empty the query won't do anything but it will be valid anyway
                $delete->where->in($this->bRelationIdColumn, $bIds ?: new Expression('FALSE'));
            }
        } else {
            $delete->where->in($this->bRelationIdColumn, $bIds);

            if ($negate) {
                $delete->where->notIn($this->aRelationIdColumn, $aIds ?: new Expression('TRUE'));
            }
            else {
                // If $aIds is empty the query won't do anything but it will be valid anyway
                $delete->where->in($this->aRelationIdColumn, $aIds ?: new Expression('FALSE'));
            }
        }

        return $this->connection->query($delete)->count();
    }

    /**
     * @inheritDoc
     */
    public function isLinkedTo($a, $b): bool {
        if (!$a->getId() || !$a->getId()) {
            return false;
        }

        if (is_a($a, $this->bRelationClass)) {
            $tmp = $b;
            $b = $a;
            $a = $tmp;
        }

        if (!is_a($b, $this->bRelationClass)) {
            throw new \InvalidArgumentException(sprintf(
                'Must provide instances of %s and %s.',
                $this->aRelationClass,
                $this->bRelationClass
            ));
        }

        $select = $this->connection->select($this->mainTable)
            ->columns(['e' => new Expression('1')]);
        $select->where->equalTo($this->aRelationIdColumn, $a->getId())
            ->and->equalTo($this->bRelationIdColumn, $b->getId());
        $select->limit(1);
        $result = $this->connection->query($select);

        return (bool) $result->current();
    }

    /**
     * @inheritDoc
     */
    public function getLinkedCollection($object): \OliveOil\Core\Model\ResourceModel\Iface\Collection {
        if (is_a($object, $this->aRelationClass)) {
            return $this->getLinkedBCollection($object);
        }

        if (is_a($object, $this->bRelationClass)) {
            return $this->getLinkedACollection($object);
        }

        throw new \InvalidArgumentException(sprintf(
            'Must provide instances of %s or %s.',
            $this->aRelationClass,
            $this->bRelationClass
        ));
    }

    /**
     * @inheritDoc
     */
    public function getLinkedIds($object): array {
        if (is_a($object, $this->aRelationClass)) {
            return $this->getLinkedBCollection($object)
                ->getAllIds();
        }

        if (is_a($object, $this->bRelationClass)) {
            return $this->getLinkedACollection($object)
                ->getAllIds();
        }

        throw new \InvalidArgumentException(sprintf(
            'Must provide instances of %s or %s.',
            $this->aRelationClass,
            $this->bRelationClass
        ));
    }

    /**
     * @param T2 $b
     * @return T1Collection
     */
    protected function getLinkedACollection($b): \OliveOil\Core\Model\ResourceModel\Iface\Collection {
        /** @var T1Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection($this->aEntityCode);
        $collection->joinTable(
            [$this->getMainTableJoinAlias() => $this->mainTable],
            sprintf(
                '%s.%s = %s.%s',
                $this->getMainTableJoinAlias(),
                $this->aRelationIdColumn,
                'main_table',
                $this->getAResourceModel()->getIdField()
            )
        )
            ->addFieldToFilter(
                sprintf('%s.%s', $this->getMainTableJoinAlias(), $this->bRelationIdColumn),
                $b->getId() ?: 0
            )
        ;

        return $collection;
    }

    /**
     * @param T1 $a
     * @return T2Collection
     */
    protected function getLinkedBCollection($a): \OliveOil\Core\Model\ResourceModel\Iface\Collection {
        /** @var T2Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection($this->bEntityCode);
        $collection->joinTable(
            [$this->getMainTableJoinAlias() => $this->mainTable],
            sprintf(
                '%s.%s = %s.%s',
                $this->getMainTableJoinAlias(),
                $this->bRelationIdColumn,
                'main_table',
                $this->getBResourceModel()->getIdField()
            )
        )
            ->addFieldToFilter(
                sprintf('%s.%s', $this->getMainTableJoinAlias(), $this->aRelationIdColumn),
                $a->getId() ?: 0
            )
        ;

        return $collection;
    }

    /**
     * @return AbstractModel
     */
    protected function getAResourceModel(): AbstractModel {
        return $this->aResourceModel;
    }

    /**
     * @return AbstractModel
     */
    protected function getBResourceModel(): AbstractModel {
        return $this->bResourceModel;
    }

    /**
     * @return string
     */
    protected function getMainTableJoinAlias(): string {
        return $this->mainTable;
    }
}
