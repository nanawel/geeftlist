<?php

namespace OliveOil\Core\Model\ResourceModel\Db\Collection;

use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;

/**
 * @template T1 of \OliveOil\Core\Model\AbstractModel
 * @template T2 of \OliveOil\Core\Model\AbstractModel
 */
abstract class AbstractRelationHelper implements RelationHelperInterface
{
    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Context $context
    ) {
        $this->connection = $context->getConnection();
    }

    /**
     * Default implementation based on self methods.
     *
     * @inheritDoc
     */
    public function canHandleFilterPredicate($relationName, $operator): bool {
        $handlingMethod = $this->getFilterPredicateHandlingMethod($relationName, $operator);

        return method_exists($this, $handlingMethod);
    }

    public function getFilterPredicate($relationName, $operator, $value) {
        $handlingMethod = $this->getFilterPredicateHandlingMethod($relationName, $operator);

        // We also pass $relationName and $operator as additional arguments for whatever purpose they may serve
        return $this->$handlingMethod($value, $relationName, $operator);
    }

    /**
     * @param string $relationName
     * @param string $operator
     */
    protected function getFilterPredicateHandlingMethod($relationName, $operator): string {
        return sprintf('filterConditionToPredicate_%s_%s', $relationName, $operator);
    }

    /**
     * Default implementation based on self methods.
     *
     * @inheritDoc
     */
    public function canHandleJoinRelation($relationName): bool {
        $handlingMethod = $this->getJoinRelationHandlingMethod($relationName);

        return method_exists($this, $handlingMethod);
    }

    public function addJoinRelation(AbstractCollection $collection, $relationName, $joinConditions): void {
        $handlingMethod = $this->getJoinRelationHandlingMethod($relationName);

        // We also pass $relationName as additional arguments for whatever purpose they may serve
        $this->$handlingMethod($collection, $joinConditions, $relationName);
    }

    protected function getJoinRelationHandlingMethod(string $relationName): string {
        return 'join_' . $relationName;
    }
}
