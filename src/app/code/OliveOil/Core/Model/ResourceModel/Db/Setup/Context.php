<?php

namespace OliveOil\Core\Model\ResourceModel\Db\Setup;


class Context extends \OliveOil\Core\Model\ResourceModel\Db\Context
{
    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Connection $connection,
        \OliveOil\Core\Model\ResourceModel\Db\Transaction $transaction,
        \OliveOil\Core\Service\GenericFactoryInterface $modelFactory,
        \OliveOil\Core\Model\FactoryInterface $resourceModelFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\Di\Container $container,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\App\FlagInterface $appFlag
    ) {
        parent::__construct(
            $connection,
            $transaction,
            $modelFactory,
            $resourceModelFactory,
            $eventService,
            $logService
        );
    }

    public function getContainer(): \OliveOil\Core\Service\Di\Container {
        return $this->container;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getAppFlag(): \OliveOil\Core\Model\App\FlagInterface {
        return $this->appFlag;
    }
}
