<?php

namespace OliveOil\Core\Model\ResourceModel\Iface;


interface Transaction
{
    /**
     * Execute $callable in a transaction
     *
     * @return mixed
     */
    public function execute(callable $callable);

    /**
     * @return $this
     */
    public function begin();

    /**
     * @return $this
     */
    public function commit();

    /**
     * @return $this
     */
    public function rollback();
}
