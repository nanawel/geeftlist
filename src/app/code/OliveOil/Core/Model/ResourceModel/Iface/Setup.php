<?php

namespace OliveOil\Core\Model\ResourceModel\Iface;

interface Setup
{
    /**
     * @throws \Throwable
     */
    public function isInstalled(\OliveOil\Core\Model\SetupInterface $model): bool;

    public function isDbUp(\OliveOil\Core\Model\SetupInterface $model): bool;

    public function install(
        \OliveOil\Core\Model\SetupInterface $model,
        bool $dropFirst = false,
        ?string $targetVersion = null
    ): bool;

    public function upgrade(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null,
        bool $force = false
    ): bool;

    public function uninstall(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null,
        bool $force = true
    ): bool;

    public function installSampleData(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null
    ): bool;

    public function upgradeSampleData(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null
    ): bool;

    public function uninstallSampleData(
        \OliveOil\Core\Model\SetupInterface $model,
        ?string $targetVersion = null
    ): bool;

    public function getDbVersion(\OliveOil\Core\Model\SetupInterface $model, string $type): string;

    public function setDbVersion(\OliveOil\Core\Model\SetupInterface $model, string $type, string $version): static;

    public function getCodeVersion(\OliveOil\Core\Model\SetupInterface $model, string $type): string;
}
