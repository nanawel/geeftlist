<?php

namespace OliveOil\Core\Model\ResourceModel\Iface;

/**
 * @template T of \OliveOil\Core\Model\AbstractModel
 */
interface Model
{
    /**
     * @param T $object
     * @param int|string $id
     * @param string|null $field
     * @return $this
     */
    public function load(\OliveOil\Core\Model\AbstractModel $object, $id, $field = null);

    /**
     * @param T $object
     * @return $this
     */
    public function save(\OliveOil\Core\Model\AbstractModel $object);

    /**
     * @param T $object
     * @return $this
     */
    public function delete(\OliveOil\Core\Model\AbstractModel $object);

    /**
     * @return string
     */
    public function getIdField();
}
