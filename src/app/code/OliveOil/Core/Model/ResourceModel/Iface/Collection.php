<?php

namespace OliveOil\Core\Model\ResourceModel\Iface;

use OliveOil\Core\Iface\FlagAware;
use OliveOil\Core\Iface\ListenableInstance;


/**
 * @template T of \OliveOil\Core\Model\AbstractModel
 */
interface Collection extends \Countable, \IteratorAggregate, \ArrayAccess, ListenableInstance, FlagAware
{
    public const SORT_DIR_DEFAULT = SORT_ASC;

    public const ORDER_BY_RANDOM = '__RANDOM__';

    /**
     * @param array<T> $items
     */
    public function setItems(array $items): static;

    public function load(bool $force = false): static;

    /**
     * @return array<T>
     */
    public function getItems(): array;

    /**
     * @return bool
     */
    public function isLoaded(): bool;

    /**
     * @param T $item
     */
    public function addItem(\OliveOil\Core\Model\AbstractModel $item): static;

    /**
     * @return T|null
     */
    public function getItem(
        mixed $value,
        ?string $key = null,
        bool $strict = false
    ): ?\OliveOil\Core\Model\AbstractModel;

    /**
     * @return T|null
     */
    public function getFirstItem(): ?\OliveOil\Core\Model\AbstractModel;

    /**
     * @return T|null
     */
    public function getLastItem(): ?\OliveOil\Core\Model\AbstractModel;

    /**
     * @return int[]
     */
    public function getAllIds(): array;

    public function addFieldToSelect(array|string $field): static;

    /**
     * @param array|mixed $cond
     */
    public function addFieldToFilter(string $field, mixed $cond): static;

    public function getFilters(): array;

    public function clearFilters(): static;

    /**
     * @param array|string $relation
     * @param array|mixed|null $joinConditions Ignored if $relation is an array
     * @return $this
     */
    public function addJoinRelation(array|string $relation, mixed $joinConditions = null);

    public function orderBy(string $field, int $dir = self::SORT_DIR_DEFAULT): static;

    public function clearOrders(): static;

    /**
     * @param int|bool $offset FALSE to clear
     */
    public function setOffset(int|bool $offset): static;

    public function getOffset(): int|bool;

    /**
     * @param int|bool $limit FALSE to clear
     */
    public function setLimit(int|bool $limit): static;

    public function getLimit(): int|bool;

    public function clear(): static;

    public function delete(): static;
}
