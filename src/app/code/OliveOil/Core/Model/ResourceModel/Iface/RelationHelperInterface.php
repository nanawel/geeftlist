<?php

namespace OliveOil\Core\Model\ResourceModel\Iface;

/**
 * @template T1 of \OliveOil\Core\Model\AbstractModel
 * @template T2 of \OliveOil\Core\Model\AbstractModel
 * @template T1Collection of \OliveOil\Core\Model\ResourceModel\Iface\Collection<T1>
 * @template T2Collection of \OliveOil\Core\Model\ResourceModel\Iface\Collection<T2>
 */
interface RelationHelperInterface
{
    /**
     * @param array<T1> $as
     * @param array<T2> $bs
     * @return int
     */
    public function addLinks(array $as, array $bs): int;

    /**
     * @param array<T1> $as
     * @param array<T2> $bs
     * @param bool $negate
     * @return int
     */
    public function removeLinks(array $as, array $bs, bool $negate = false): int;

    /**
     * @param T1 $a
     * @param T2 $b
     * @return bool
     */
    public function isLinkedTo($a, $b): bool;

    /**
     * @param T1|T2 $object
     * @return T2Collection|T1Collection
     */
    public function getLinkedCollection($object): \OliveOil\Core\Model\ResourceModel\Iface\Collection;

    /**
     * @param T1|T2 $object
     * @return int[]
     */
    public function getLinkedIds($object): array;
}
