<?php

namespace OliveOil\Core\Model;

class Session extends MagicObject implements SessionInterface
{
    protected \Base $fw;

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \OliveOil\Core\Model\I18n $i18n;

    /** @var \OliveOil\Core\Model\Session\Message\ManagerInterface */
    protected $messageManager;

    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    protected \OliveOil\Core\Service\Crypt $cryptService;

    /**
     * Session constructor.
     * @param string $name
     */
    public function __construct(
        \OliveOil\Core\Model\Session\Context $context,
        protected $name
    ) {
        $this->fw = $context->getFw();
        $this->appConfig = $context->getAppConfig();
        $this->i18n = $context->getI18n();
        $this->messageManager = $context->getMessageManager()
            ->setSession($this);
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->cryptService = $context->getCryptService();
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init(): static {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function invalidate(): static {
        $this->resetData()
            ->resetOrigData()
            ->init();

        return $this;
    }

    public function getId() {
        if (! $id = parent::getId()) {
            $this->setId($id = $this->getCryptService()->generateToken());
        }

        return $id;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * @param string|null $key
     * @return mixed
     */
    public function &refData($key = null) {
        if ($key === null) {
            return $this->_data;
        }

        if (!array_key_exists($key, $this->_data)) {
            $this->_data[$key] = null;
        }

        return $this->_data[$key];
    }

    /*
     * Messages management
     */

    /**
     * @inheritDoc
     */
    public function addMessage($message, $type, $options = []): static {
        $this->getMessageManager()->addMessage($message, $type, $options);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addInfoMessage($messsage, array $vars = null, $options = []): static {
        $this->getMessageManager()->addInfoMessage($this->getI18n()->tr($messsage, $vars), $options);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addSuccessMessage($messsage, array $vars = null, $options = []): static {
        $this->getMessageManager()->addSuccessMessage($this->getI18n()->tr($messsage, $vars), $options);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addWarningMessage($messsage, array $vars = null, $options = []): static {
        $this->getMessageManager()->addWarningMessage($this->getI18n()->tr($messsage, $vars), $options);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addErrorMessage($messsage, array $vars = null, $options = []): static {
        if ($messsage instanceof \Throwable) {
            $messsage = $messsage->getMessage();
        }

        $this->getMessageManager()->addErrorMessage($this->getI18n()->tr($messsage, $vars), $options);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getMessages() {
        return $this->getMessageManager()->getMessages();
    }

    /**
     * @inheritDoc
     */
    public function getMessagesByType($type) {
        return $this->getMessageManager()->getMessagesByType($type);
    }

    /**
     * @inheritDoc
     */
    public function clearMessages(): static {
        $this->getMessageManager()->clearMessages();

        return $this;
    }


    /*
     * Locale & Theme management
     */

    public function getLocale() {
        if (!$locale = parent::getLocale()) {
            $locale = $this->getI18n()->getLanguage();
            if (!str_contains($locale, '.')) {
                $locale .= '.' . $this->getFw()->get('ENCODING');
            }

            $this->setLocale($locale);
        }

        return $locale;
    }

    public function getTheme() {
        if (! $theme = parent::getTheme()) {
            $theme = $this->getAppConfig()->getValue('THEME');
        }

        return $theme;
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    /**
     * @return \OliveOil\Core\Model\Session\Message\ManagerInterface
     */
    public function getMessageManager() {
        return $this->messageManager;
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }
}
