<?php

namespace OliveOil\Core\Model\Email;



use OliveOil\Core\Model\MagicObject;

/**
 * Class Simple
 *
 * @method $this setFrom($from)
 * @method string getTo()
 * @method $this setTo($to)
 * @method string getCc()
 * @method $this setCc($cc)
 * @method string getBcc()
 * @method $this setBcc($bcc)
 * @method string getReplyTo()
 * @method $this setReplyTo($replyTo)
 * @method string getSubject()
 * @method $this setSubject($subject)
 * @method string getBody()
 * @method $this setBody($body)
 * @method $this setAdditionalHeaders(array $additionalHeaders)
 *
 * @method string getEncoding()
 * @method $this setEncoding($encoding)
 *
 * @method string getError()
 * @method $this setError($error)
 * @method $this unsError()
 * @method string getException()
 * @method $this setException(\Throwable $exception)
 */
class Simple extends MagicObject implements EmailInterface
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var string[] */
    protected $additionalHeaders = [];

    public function __construct(
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\Log $logService,
        array $data = []
    ) {
        $this->logger = $logService->getLoggerForClass($this);
        parent::__construct($data);

        // Default
        $this->setEncoding('utf-8');
        $this->setAdditionalHeader('MIME-Version', '1.0');
    }

    public function getFrom() {
        if (!$this->hasData('from')) {
            $this->setData('from', $this->appConfig->getValue('EMAIL_DEFAULT_SENDER'));
        }

        return $this->getData('from');
    }

    public function setMimetype($mime): static {
        parent::setMimetype($mime);

        return $this->setAdditionalHeader('Content-Type', $mime);
    }

    public function getAdditionalHeader($header) {
        return $this->additionalHeaders[$header] ?? null;
    }

    public function setAdditionalHeader($header, $value): static {
        $this->additionalHeaders[(string) $header] = (string) $value;

        return $this;
    }

    /**
     *
     * @return array
     */
    public function getAdditionalHeaders() {
        $headers = $this->additionalHeaders;
        if ($from = $this->getFrom()) {
            $headers['From'] = $from;
        }

        if ($cc = $this->getCc()) {
            $headers['Copy-To'] = $cc;
        }

        //TODO The rest + check syntax

        return $headers;
    }

    /**
     * @inheritDoc
     */
    public function prepareForSending(): static {
        return $this;
    }

    /**
     * @return string
     */
    public function getMimetype() {
        return parent::getMimetype() ?: 'text/html';
    }
}
