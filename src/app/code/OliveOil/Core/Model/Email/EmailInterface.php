<?php
namespace OliveOil\Core\Model\Email;


use OliveOil\Core\Iface\DynamicDataMethodsAware;

/**
 * Interface EmailInterface
 *
 * @method string getFrom()
 * @method $this setFrom($from)
 * @method string getTo()
 * @method $this setTo($to)
 * @method string getCc()
 * @method $this setCc($cc)
 * @method string getBcc()
 * @method $this setBcc($bcc)
 * @method string getReplyTo()
 * @method $this setReplyTo($replyTo)
 * @method string getSubject()
 * @method $this setSubject($subject)
 * @method string getBody()
 * @method $this setBody($body)
 * @method string getMimetype()
 * @method $this setMimetype($mimetype)
 * @method array getAdditionalHeaders()
 * @method $this setAdditionalHeaders(array $additionalHeaders)
 *
 * @method string getEncoding()
 * @method $this setEncoding($encoding)
 *
 * @method string getError()
 * @method $this setError($error)
 * @method $this unsError()
 * @method string getException()
 * @method $this setException(\Throwable $exception)
 */
interface EmailInterface extends DynamicDataMethodsAware
{
    /**
     * @return $this
     */
    public function prepareForSending();
}
