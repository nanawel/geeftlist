<?php

namespace OliveOil\Core\Model\Email;

use OliveOil\Core\Exception\MailException;
use OliveOil\Core\Exception\TemplateNotFoundException;
use OliveOil\Core\Model\I18n;
use Pelago\Emogrifier\CssInliner;

/**
 * @method string|null getTemplate()
 * @method $this setTemplate(string $template)
 * @method bool hasTemplateTtl()
 * @method int|null getTemplateTtl()
 * @method $this setTemplateTtl(int $templateTtl)
 * @method string|null getTheme()
 * @method $this setTheme(string $theme)
 */
class Template extends \OliveOil\Core\Model\Email\Simple
{
    public const RESERVED_DATA_KEYS = [
        'template',
        'from',
        'to',
        'cc',
        'bcc',
        'reply_to',
        'subject',
        'body',
        'mime',
        'encoding'
    ];

    public function __construct(
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\DesignInterface $design,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\View\Template\Renderer\RendererInterface $templateRenderer,
        array $data = []
    ) {
        parent::__construct($appConfig, $logService, $data);
        $this->setMimetype('text/html');
        if (! $this->hasTemplateTtl()) {
            $this->setTemplateTtl($appConfig->getValue('EMAIL_TEMPLATE_TTL', 0));
        }
    }

    /**
     * @inheritDoc
     */
    public function prepareForSending(): static {
        $this->processTemplate();

        return parent::prepareForSending();
    }

    protected function processTemplate(): static {
        if (! $template = $this->getTemplate()) {
            throw new MailException('Missing email template');
        }

        // Body
        $hive = $this->getData();
        $processedTemplate = $this->render($template, $this->getMimetype(), $hive, $this->getTemplateTtl());
        $this->setBody($this->inline($this->compile($processedTemplate)));

        // Subject
        if (! $this->getSubject()) {
            if (! $subject = $this->extractSubject($processedTemplate)) {
                throw new MailException(sprintf("Cannot find subject in template '%s'", $template));
            }

            $this->setSubject($subject);
        }

        return $this;
    }

    /**
     * @param string $inkyContent
     * @return string
     */
    protected function compile($inkyContent): string|false {
        $startTime = microtime(true);
        $transpiled = \Pinky\transformString($inkyContent);
        $this->logger->debug(sprintf('Email content transpiled in %1.2f sec.', microtime(true) - $startTime));

        return $transpiled->saveHTML();
    }

    /**
     * @return string
     */
    protected function inline(string $html) {
        $theme = $this->getTheme() ?: $this->appConfig->getValue('EMAIL_TEMPLATE_THEME', 'email-default');

        // TODO Use cache
        $css = file_get_contents($this->design->getAssetSrcPath('css/base.css', $theme))
            . file_get_contents($this->design->getAssetSrcPath('css/theme.css', $theme));

        $startTime = microtime(true);
        $return = CssInliner::fromHtml($html)
            ->inlineCss($css)
            ->render();
        $this->logger->debug(sprintf('Email content inlined in %1.2f sec.', microtime(true) - $startTime));

        return $return;
    }

    /**
     * Find and return the @subject tag into the processed template
     *
     * @param string $body
     * @return string|false
     */
    protected function extractSubject($body): string|false {
        if (preg_match('/<!--\s+@subject\s+(.+?)\s+-->/i', $body, $matches)) {
            return html_entity_decode($matches[1]);
        }

        return false;
    }

    /**
     * @param string $file
     * @param string|null $mime
     * @param array|null $hive
     * @param int $ttl
     * @return string
     * @throws TemplateNotFoundException
     */
    public function render($file, $mime = null, array $hive = null, $ttl = 0) {
        if ($hive === null) {
            $hive = [];
        }

        return $this->templateRenderer->render(
            $file,
            $hive,
            $mime,
            ['i18n' => $this->i18n, 'ttl' => $ttl]
        );
    }

    /**
     * @return $this
     */
    public function setI18n(I18n $i18n) {
        $this->i18n = $i18n;

        // Set also _data to use it as hive when rendering template
        // see self::_processTemplate()
        return parent::setI18n($i18n);
    }
}
