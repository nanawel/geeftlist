<?php

namespace OliveOil\Core\Model;


use OliveOil\Core\Exception\CookieException;

class Cookie
{
    public const SAMESITE_LAX    = 'Lax';
    public const SAMESITE_STRICT = 'Strict';

    protected int $expires = 0;

    protected string $path = '';

    protected string $domain = '';

    protected bool $secure = false;

    protected bool $httponly = false;

    protected string $samesite = self::SAMESITE_LAX;

    protected bool $hasChanged = true;

    public function __construct(
        protected \Base $fw,
        protected string $name
    ) {
    }

    protected function set(
        $name,
        $value,
        $expires,
        $path,
        $domain,
        $secure,
        $httponly,
        $samesite
    ): static {
        $success = setcookie(
            $name,
            (string) $value,
            [
                'expires'  => $expires,
                'path'     => $path,
                'domain'   => $domain,
                'secure'   => $secure,
                'httponly' => $httponly,
                'samesite' => $samesite,
            ]
        );
        if (!$success) {
            throw new CookieException('Could not write cookie.');
        }

        return $this;
    }

    public function write(bool $force = false): static {
        if ($force || $this->hasChanged) {
            // Workaround for #652/#661: Force clear subdomains cookies
            if ($this->domain === '') {
                $this->set(
                    $this->getName(),
                    null,
                    null,
                    $this->getPath(),
                    $this->fw->get('HOST'),
                    $this->isSecure(),
                    $this->isHttponly(),
                    $this->getSamesite()
                );
            }

            $this->set(
                $this->getName(),
                $this->getValue(),
                $this->getExpires(),
                $this->getPath(),
                $this->getDomain(),
                $this->isSecure(),
                $this->isHttponly(),
                $this->getSamesite()
            );
            $this->hasChanged = false;
        }

        return $this;
    }

    public function delete(): static {
        $this->set(
            $this->getName(),
            null,
            null,
            $this->getPath(),
            $this->getDomain(),
            $this->isSecure(),
            $this->isHttponly(),
            $this->getSamesite()
        );

        // Cannot use F3 here as it writes the cookie unconditionally
        unset($_COOKIE[$this->getName()]);
        $this->hasChanged = false;

        return $this;
    }

    public function reset(): static {
        $this->setValue(null)
            ->setExpires(0);
        $this->hasChanged = true;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        if ($this->name !== $name) {
            $this->name = $name;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function getValue(): mixed
    {
        // Cannot use F3 here as it writes the cookie unconditionally
        return $_COOKIE[$this->getName()] ?? null;
    }

    public function setValue(mixed $value): static
    {
        if ($this->getValue() !== $value) {
            // Cannot use F3 here as it writes the cookie unconditionally
            $_COOKIE[$this->getName()] = $value;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function getExpires(): int
    {
        return $this->expires;
    }

    public function setExpires(int $expires): static
    {
        if ($this->expires !== $expires) {
            $this->expires = $expires;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        if ($this->getPath() !== $path) {
            $this->path = $path;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): static
    {
        if ($this->getDomain() !== $domain) {
            $this->domain = $domain;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function isSecure(): bool
    {
        return $this->secure;
    }

    public function setSecure(bool $secure): static
    {
        if ($this->isSecure() !== $secure) {
            $this->secure = $secure;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function getSamesite(): string
    {
        return $this->samesite;
    }

    public function setSamesite(string $samesite): static
    {
        if ($this->getSamesite() !== $samesite) {
            $this->samesite = $samesite;
            $this->hasChanged = true;
        }

        return $this;
    }

    public function isHttponly(): bool
    {
        return $this->httponly;
    }

    public function setHttponly(bool $httponly): static
    {
        if ($this->isHttponly() !== $httponly) {
            $this->httponly = $httponly;
            $this->hasChanged = true;
        }

        return $this;
    }
}
