<?php


namespace OliveOil\Core\Model\Security;


class Captcha
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $imageSrc;

    /** @var int|null */
    protected $imageWidth;

    /** @var int|null */
    protected $imageHeight;

    /** @var array */
    protected $imageParams = [];

    /** @var string */
    protected $phrase;

    public function __serialize(): array {
        return get_object_vars($this);
    }

    public function __unserialize(array $data): void {
        foreach ($data as $property => $value) {
            $this->$property = $value;
        }
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id): static {
        if (!is_string($id) || ($id === '' || $id === '0')) {
            throw new \InvalidArgumentException('Captcha ID must be a non-empty string.');
        }

        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageSrc() {
        return $this->imageSrc;
    }

    /**
     * @param string $imageSrc
     * @return $this
     */
    public function setImageSrc($imageSrc): static {
        if (!is_string($imageSrc) || ($imageSrc === '' || $imageSrc === '0')) {
            throw new \InvalidArgumentException('Image source must be a non-empty string.');
        }

        $this->imageSrc = $imageSrc;

        return $this;
    }

    /**
     * @return int
     */
    public function getImageWidth() {
        return $this->imageWidth;
    }

    /**
     * @param int $imageWidth
     * @return $this
     */
    public function setImageWidth($imageWidth): static {
        $this->imageWidth = $imageWidth;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getImageHeight() {
        return $this->imageHeight;
    }

    /**
     * @param int $imageHeight
     * @return $this
     */
    public function setImageHeight($imageHeight): static {
        $this->imageHeight = $imageHeight;

        return $this;
    }

    /**
     * @return array
     */
    public function getImageParams() {
        return $this->imageParams;
    }

    /**
     * @return $this
     */
    public function setImageParams(array $imageParams): static {
        $this->imageParams = $imageParams;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhrase() {
        return $this->phrase;
    }

    /**
     * @param string $phrase
     * @return $this
     */
    public function setPhrase($phrase): static {
        if (!is_string($phrase) || ($phrase === '' || $phrase === '0')) {
            throw new \InvalidArgumentException('Phrase must be a non-empty string.');
        }

        $this->phrase = $phrase;

        return $this;
    }
}
