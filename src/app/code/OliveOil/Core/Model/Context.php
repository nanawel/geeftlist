<?php

namespace OliveOil\Core\Model;


class Context
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory, protected \OliveOil\Core\Service\GenericFactoryInterface $resourceModelFactory, protected \OliveOil\Core\Service\GenericFactoryInterface $fieldModelFactory, protected \OliveOil\Core\Service\EventInterface $eventService, protected \OliveOil\Core\Service\Log $logService, protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory, protected \OliveOil\Core\Helper\Transaction $transactionHelper)
    {
    }

    public function getModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->modelFactory;
    }

    public function getResourceModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->resourceModelFactory;
    }

    public function getFieldModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->fieldModelFactory;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getTransactionHelper(): \OliveOil\Core\Helper\Transaction {
        return $this->transactionHelper;
    }
}
