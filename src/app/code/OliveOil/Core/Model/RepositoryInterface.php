<?php

namespace OliveOil\Core\Model;

/**
 * @template T of AbstractModel
 */
interface RepositoryInterface
{
    public const WILDCARD_ID_FIELD            = '_id';

    public const COLLECTION_DEFAULT_PAGE_SIZE = 10;

    /**
     * @param int|string $id
     * @param string|null $field
     * @param array $options [use_cache => bool, flags => array]
     * @return T|null
     */
    public function get($id, $field = null, $options = []);

    /**
     * @param array|object $criteria
     * @return array<T>
     */
    public function find($criteria = null);

    /**
     * @param array|object $criteria
     * @return T|null
     */
    public function findOne($criteria = null);

    /**
     * @param array|object $criteria
     * @return int[]
     */
    public function findAllIds($criteria = null);

    /**
     * @param T $model
     * @return $this
     */
    public function save(\OliveOil\Core\Model\AbstractModel $model);

    /**
     * @param T $model
     * @return $this
     */
    public function delete(\OliveOil\Core\Model\AbstractModel $model);

    /**
     * @param array|object $criteria
     * @return $this
     */
    public function deleteAll($criteria = null);

    /**
     * @param int $id
     * @return $this
     */
    public function deleteById($id);

    /**
     * @param T $model
     * @return \OliveOil\Core\Model\Validation\ErrorAggregatorInterface
     */
    public function validate(\OliveOil\Core\Model\AbstractModel $model);

    /**
     * @param mixed $args,...
     * @return T
     */
    public function newModel(mixed ...$args);
}
