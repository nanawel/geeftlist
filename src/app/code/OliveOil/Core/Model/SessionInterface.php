<?php

namespace OliveOil\Core\Model;

use OliveOil\Core\Exception\Session\ExpiredException;
use OliveOil\Core\Iface\DynamicDataMethodsAware;
use OliveOil\Core\Model\Session\Constants;


interface SessionInterface extends DynamicDataMethodsAware, Constants
{
    /**
     * Start a new session.
     * Typically in HTTP context, restore a session if it exists, or create a new one and send a cookie to client.
     *
     * @return $this
     * @throws ExpiredException
     * @throws \Exception
     */
    public function init();

    /**
     * Invalidate current session and destroy any data associated.
     * Typically in HTTP context, destroy session storage and send a "delete cookie" header to client.
     *
     * @return $this
     */
    public function invalidate();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string|null $key
     * @return mixed
     */
    public function &refData($key = null);

    /*
     * Messages management
     */

    /**
     * @param string $message
     * @param string $type Type constant
     *               - self::MESSAGE_INFO
     *               - self::MESSAGE_SUCCESS
     *               - self::MESSAGE_WARN
     *               - self::MESSAGE_ERROR
     * @param array $options
     *              "no_escape" => Do not HTML-escape message content on rendering
     * @return $this
     */
    public function addMessage($message, $type, $options = []);

    /**
     * @param string $messsage
     * @param array|null $vars
     * @param array $options
     * @return $this
     */
    public function addInfoMessage($messsage, array $vars = null, $options = []);

    /**
     * @param string $messsage
     * @param array|null $vars
     * @param array $options
     * @return $this
     */
    public function addSuccessMessage($messsage, array $vars = null, $options = []);

    /**
     * @param string $messsage
     * @param array|null $vars
     * @param array $options
     * @return $this
     */
    public function addWarningMessage($messsage, array $vars = null, $options = []);

    /**
     * @param string $messsage
     * @param array|null $vars
     * @param array $options
     * @return $this
     */
    public function addErrorMessage($messsage, array $vars = null, $options = []);

    /**
     * @return \OliveOil\Core\Model\Session\Message\MessageInterface[][]
     */
    public function getMessages();

    /**
     * @return \OliveOil\Core\Model\Session\Message\MessageInterface[]
     */
    public function getMessagesByType($type);

    /**
     * @return $this
     */
    public function clearMessages();

    /**
     * @return string
     */
    public function getLocale();
}
