<?php

namespace OliveOil\Core\Model\Entity;

use OliveOil\Core\Model\Validation\ErrorInterface;

interface FieldInterface
{
    /**
     * @param array|null $params
     * @return ErrorInterface|null
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, mixed $value, $params = null);

    /**
     * @param array|null $params
     * @return string|null
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, mixed $value, $params = null);

    /**
     * @param array|null $params
     * @return string
     */
    public function serialize(\OliveOil\Core\Model\AbstractModel $object, mixed $value, $params = []);

    /**
     * @param string $value
     * @param array|null $params
     * @return mixed
     */
    public function unserialize(\OliveOil\Core\Model\AbstractModel $object, $value, $params = []);

    /**
     * @param string $value
     * @param array|null $params
     */
    public function afterModelLoad(\OliveOil\Core\Model\AbstractModel $object, $value, $params = []);
}
