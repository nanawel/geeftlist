<?php

namespace OliveOil\Core\Model\Entity;

use OliveOil\Core\Model\Validation\ErrorInterface;

interface OptionsFieldInterface extends FieldInterface
{
    /**
     * @return array
     */
    public function getAllOptions();

    /**
     * @param mixed $value
     * @param array|null $params
     * @return ErrorInterface|null
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null);

    /**
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null);
}
