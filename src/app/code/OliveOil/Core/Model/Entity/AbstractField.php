<?php

namespace OliveOil\Core\Model\Entity;


use OliveOil\Core\Model\Validation\ErrorInterface;

abstract class AbstractField implements FieldInterface
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory)
    {
    }

    /**
     * @return \OliveOil\Core\Model\Validation\ErrorInterface
     */
    protected function getNewErrorInstance($type, $message, $level = ErrorInterface::LEVEL_ERROR) {
        return $this->getCoreFactory()->make(ErrorInterface::class)
            ->setType($type)
            ->setMessage($message)
            ->setLevel($level);
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    /**
     * @inheritDoc
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null)
    {
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function serialize(\OliveOil\Core\Model\AbstractModel $object, $value, $params = [])
    {
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function unserialize(\OliveOil\Core\Model\AbstractModel $object, $value, $params = [])
    {
        return $value;
    }

    /**
     * @param string $value
     * @param array|null $params
     */
    public function afterModelLoad(\OliveOil\Core\Model\AbstractModel $object, $value, $params = []) {
    }
}
