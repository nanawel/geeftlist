<?php

namespace OliveOil\Core\Model\Entity;


use OliveOil\Core\Exception\InvalidFieldValueException;
use OliveOil\Core\Model\Validation\ErrorInterface;

abstract class AbstractOptionsField extends AbstractField implements OptionsFieldInterface
{
    /** @var mixed[] */
    public const VALUES = [];

    /** @var array */
    public const OPTIONS = [];

    /** @var bool */
    protected $valueIsMandatory = false;

    /**
     * @inheritDoc
     */
    public function getAllOptions(): array {
        return static::OPTIONS;
    }

    /**
     * @inheritDoc
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null) {
        if (trim((string) $value) === '') {
            if ($this->valueIsMandatory) {
                return $this->getNewErrorInstance(
                    ErrorInterface::TYPE_FIELD_EMPTY,
                    'Missing value'
                );
            }

            // else: OK empty allowed
        }
        elseif (! in_array($value, static::VALUES)) {
            return $this->getNewErrorInstance(
                ErrorInterface::TYPE_FIELD_INVALID,
                'Invalid value'
            );
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null)
    {
        if ($value === null) {
            return null;
        }

        if (array_key_exists($value, static::OPTIONS)) {
            return static::OPTIONS[$value]['label'];
        }

        throw new InvalidFieldValueException($value);
    }
}
