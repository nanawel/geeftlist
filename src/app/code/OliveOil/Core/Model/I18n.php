<?php

namespace OliveOil\Core\Model;

use OliveOil\Core\Exception\NotImplementedException;

/**
 * Class I18n
 */
class I18n
{
    public const DATETIME_FORMATTER_MAPPING = [
        'none'    => \IntlDateFormatter::NONE,
        'short'   => \IntlDateFormatter::SHORT,
        'medium'  => \IntlDateFormatter::MEDIUM,
        'long'    => \IntlDateFormatter::LONG,
        'full'    => \IntlDateFormatter::FULL
    ];

    public const STRING_CONTEXT_SPECIFICATION_SEPARATOR = '||';

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var string|null */
    protected $language;

    /** @var string|null */
    protected $languageFallback;

    /** @var string|null */
    protected $globalLanguage;

    /** @var string|null */
    protected $globalLanguageFallback;

    protected $languages                = [];

    protected $sortedLanguages          = [];

    protected $currencyCode;

    protected $timezone;

    protected $numberFormatter          = [];

    protected $currencyFormatter        = [];

    protected $currencySymbolFormatters = [];

    protected $dateTimeFormatter        = [];

    protected $missingTranslations       = [];

    /**
     * @param string $prefix
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\Log $logService,
        protected $prefix = 'LANG.'
    ) {
        if (! extension_loaded('intl')) {
            throw new \Exception('Missing "intl" extension.');
        }
        $this->logger = $logService->getLoggerForClass($this);
        $this->globalLanguage =& $this->fw->ref('LANGUAGE');
        $this->globalLanguageFallback =& $this->fw->ref('FALLBACK');

        $this->language = $this->globalLanguage;
        $this->languageFallback = $this->globalLanguageFallback;

        $this->init();
    }

    protected function init() {
        // not used yet
    }

    /**
     * Set language for $this instance of I18n.
     *
     * @param string $lang
     * @param string|null $fallback
     * @return $this
     */
    public function setLanguage($lang, $fallback = null): static {
        $newLanguage = $this->deriveLanguage($lang, $fallback);

        if ($this->language !== $newLanguage) {
            $this->language = $newLanguage;
            $this->languageFallback = $fallback;
            $this->sortedLanguages = [];
        }

        return $this;
    }

    /**
     * Get language for $this instance of I18n.
     *
     * @param bool $withTerritory
     */
    public function getLanguage($withTerritory = true): string {
        return $this->getPrimaryLanguage($this->language, $withTerritory);
    }

    /**
     * Get default language for all instances of I18n.
     *
     * @param bool $withTerritory
     */
    public function getGlobalLanguage($withTerritory = true): string {
        return $this->getPrimaryLanguage($this->globalLanguage, $withTerritory);
    }

    /**
     * Set default language for all instances of I18n.
     *
     * @param string $lang
     * @param string|null $fallback
     * @return $this
     */
    public function setGlobalLanguage($lang, $fallback = null): static {
        $newLanguage = $this->deriveLanguage($lang, $fallback);
        if ($this->globalLanguage !== $newLanguage) {
            // Need to use \Base::set() to update current dict
            $this->fw->set('LANGUAGE', $newLanguage);
        }

        if ($fallback && $this->globalLanguageFallback !== $fallback) {
            $this->fw->set('FALLBACK', $fallback);
        }

        $this->afterGlobalLanguageChange($lang, $fallback);

        return $this;
    }

    protected function afterGlobalLanguageChange($lang, $fallback = null) {
        setlocale(LC_NUMERIC, 'C');   // Force keeping C format for numeric values
    }

    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getLanguageWithFallback($asArray = false) {
        if ($asArray) {
            return explode(',', (string) $this->language);
        }

        return $this->language;
    }

    /**
     * @param string $language
     * @param string|null $fallback
     * @param bool $asArray
     */
    public function deriveLanguage($language, $fallback = null, $asArray = false): string|array {
        if ($fallback === null) {
            $fallback = (string)$this->languageFallback;
        }

        $languages = [$language];
        if (str_contains($language, '-')) {
            $languages[] = explode('-', $language)[0];
        }

        $languages[] = $fallback;
        if (str_contains($fallback, '-')) {
            $languages[] = explode('-', $fallback)[0];
        }

        $return = array_filter(array_unique($languages));

        return $asArray
            ? $return
            : implode(',', $return);
    }

    /**
     * @param string $language
     * @param bool $withTerritory
     */
    protected function getPrimaryLanguage($language, $withTerritory = true): string {
        [$lang] = explode(',', $language);
        if (! $withTerritory) {
            [$lang] = explode('-', $lang);
        }

        return $lang;
    }

    public function getLocale(): string {
        return $this->getLanguage() . '.' . $this->fw->get('ENCODING');
    }

    /**
     * @param string $currencyCode
     * @return $this
     */
    public function setCurrencyCode($currencyCode): static {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode() {
        return $this->currencyCode;
    }

    /**
     * @param string $tz
     * @return $this
     */
    public function setTimezone($tz): static {
        $this->timezone = $tz;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone() {
        return $this->timezone;
    }

    /**
     * @param bool $withTerritory
     * @return string[]
     */
    public function getAvailableLanguages($withTerritory = true) {
        $key = $withTerritory ? 'withTerritory' : 'withoutTerritory';
        if (! isset($this->languages[$key])) {
            $this->languages[$key] = [];
            $lang = $this->appConfig->getValue('LANGUAGES');
            foreach ($lang as $l) {
                if ($withTerritory) {
                    $this->languages[$key] = array_merge($this->languages[$key], [$l, explode('-', (string) $l)[0]]);
                }
                else {
                    $this->languages[$key][] = explode('-', (string) $l)[0];
                }
            }

            $this->languages[$key] = array_unique($this->languages[$key]);
        }

        return $this->languages[$key];
    }

    /**
     * @return string[]
     */
    public function getSortedAvailableLanguages() {
        if (! $this->sortedLanguages) {
            $currentLanguage = $this->getLanguage();
            $this->sortedLanguages = array_unique(array_merge(
                [$currentLanguage, explode('-', $currentLanguage)[0]],
                $this->getAvailableLanguages()
            ));
        }

        return $this->sortedLanguages;
    }

    /**
     * @param string $lang
     */
    public function isValidLanguage($lang): bool {
        return in_array($lang, $this->getAvailableLanguages());
    }

    /**
     * @param string $lang
     */
    public function getLanguageDisplayName($lang): string {
        return \Locale::getDisplayName($lang, $this->getLanguage());
    }

    /**
     * @return string[]
     */
    public function getAvailableCurrencies() {
        return $this->appConfig->getValue('CURRENCIES');
    }

    public function getAvailableTimezones(): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * @param callable $callable
     * @param array $args
     * @return mixed
     */
    public function callWithLocaleContext($callable, $args = []) {
        // TODO Might also be used later for encoding context (= full locale)
        if ($this->language != $this->globalLanguage) {
            $globalLanguage = $this->globalLanguage;
            $globalLanguageFallback = $this->globalLanguageFallback;
            try {
                $this->fw->mset([
                    'LANGUAGE' => $this->language,
                    'FALLBACK' => $this->languageFallback
                ]);

                return call_user_func_array($callable, $args);
            }
            finally {
                $this->fw->mset([
                    'LANGUAGE' => $globalLanguage,
                    'FALLBACK' => $globalLanguageFallback
                ]);
            }
        }
        else {
            return call_user_func_array($callable, $args);
        }
    }

    /**
     * @param string $string
     * @param array|string|null $vars
     * @return mixed
     */
    public function tr($string, $vars = null) {
        if ($string === '' || $string === null) {
            return '';
        }

        if (! is_array($vars)) {
            $vars = array_slice(func_get_args(), 1);
        }

        // Try direct translation
        $translation = $this->get($string, $vars);

        if (! $translation) {
            $origString = $string;

            // String uses the context specification separator, try without as fallback
            if (($pos = strpos($string, self::STRING_CONTEXT_SPECIFICATION_SEPARATOR)) !== false) {
                $string = substr($string, $pos + 2);
                $translation = $this->get($string, $vars);
            }

            // Still no match found with fallback
            if (! $translation) {
                $this->logger->notice('Missing translation for string: ' . $origString);
                // Last fallback: add the given string (without context separator if any) as translation itself
                $this->set($origString, $string);
                $translation = $this->get($string, $vars);
                $this->missingTranslations[$string] = $string;
            }
        }

        $this->logger->debug(sprintf('[%s] %s  =>  %s', $this->getLocale(), $string, $translation));

        return $translation;
    }

    /**
     * Formats a currency value (price, etc.)
     *
     * @param float $value
     * @param bool $withContainer
     * @param string|null $currencyCode
     * @return string
     */
    public function currency($value, $withContainer = false, $currencyCode = null) {
        if ($currencyCode === null) {
            $currencyCode = $this->currencyCode;
        }

        if ($formatter = $this->getCurrencyFormatterInstance()) {
            $return = $formatter->formatCurrency($value, $currencyCode);
        }
        else {
            $return = $this->fw->format(sprintf('{0,number,currency,%s}', $currencyCode), $value);
        }

        if ($withContainer) {
            $return = $this->wrapHtmlContainer($value, $return, 'currency');
        }

        return $return;
    }

    /**
     * @param float $value
     * @param bool $withContainer
     * @return string
     * @throws \Exception
     */
    public function number($value, $withContainer = false) {
        if ($formatter = $this->getNumberFormatterInstance()) {
            $return = $formatter->format($value);
        }
        else {
            $return = $this->fw->format('{0,number}', $value);
        }

        if ($withContainer) {
            $return = $this->wrapHtmlContainer($value, $return);
        }

        return $return;
    }

    /**
     * @param int|array $value
     * @param string|null $unit
     * @param string|null $unitType
     * @param bool $withContainer
     * @return string
     * @throws \Exception
     */
    public function numberUnit($value, $unit = null, $unitType = null, $withContainer = false) {
        if (is_array($value)) {
            [$value, $unit, $unitType, $withContainer] = $value + [null, null, null, $withContainer];
        }

        $return = $this->tr(
            sprintf(
                'unit_%s%s{0} {1}',
                $unitType,
                self::STRING_CONTEXT_SPECIFICATION_SEPARATOR
            ),
            $this->number($value),
            $this->tr(sprintf('unit_%s%s%s', $unitType, self::STRING_CONTEXT_SPECIFICATION_SEPARATOR, $unit))
        );
        if ($withContainer) {
            $return = $this->wrapHtmlContainer($value, $return);
        }

        return $return;
    }

    protected function wrapHtmlContainer($rawValue, $displayValue, $additionalClasses = ''): string {
        $class = explode(' ', (string) $additionalClasses);
        if (is_numeric($rawValue)) {
            $class[] = 'number';
            $class[] = $rawValue < 0 ? 'number-neg' : 'number-pos';
        }

        $class = implode(' ', $class);

        return sprintf('<span class="%s">%s</span>', $class, $displayValue);
    }

    /**
     * @param string $dateStyle
     * @param string|null $tz
     * @return string
     */
    public function date(mixed $value, $dateStyle = 'short', $tz = null) {
        if (is_string($value) && !is_numeric($value)) {
            $value = strtotime($value);
        }

        if ($formatter = $this->getDateFormatterInstance($dateStyle, $tz)) {
            return $formatter->format($value);
        }

        //TODO Map date/time styles to F3 constants (short, long, custom)
        return $this->fw->format('{0,date}', $value);
    }

    public function datetime($value, $dateStyle = 'short', $timeStyle = 'short') {
        if (is_string($value) && !is_numeric($value)) {
            $value = strtotime($value);
        }

        if ($formatter = $this->getDateTimeFormatterInstance($dateStyle, $timeStyle)) {
            return $formatter->format($value);
        }

        //TODO Map date/time styles to F3 constants (short, long, custom)
        return $this->fw->format('{0,date} {0,time}', $value);
    }

    /**
     * @param string $dateStyle
     * @param string|null $tz
     */
    public function parseDate(mixed $value, $dateStyle = 'short', $tz = null): int|false {
        if (! is_string($value)) {
            $value = (string) $value;
        }

        return $this->getDateFormatterInstance($dateStyle, $tz)->parse($value);
    }

    public function getDatePattern($dateStyle = 'short') {
        return $this->tr($this->getDateFormatterInstance($dateStyle)->getPattern());
    }

    public function getCurrencySymbol($currencyCode = null): string {
        return $this->getCurrencySymbolFormatterInstance($currencyCode)
            ->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
    }

    /**
     * @return \NumberFormatter
     */
    public function getNumberFormatterInstance() {
        $cacheKey = implode('|', [
            $this->getLanguage()
        ]);
        if (! isset($this->numberFormatter[$cacheKey])) {
            if (! class_exists('NumberFormatter')) {
                throw new \RuntimeException('Class not found: NumberFormatter');
            }

            $this->numberFormatter[$cacheKey] = new \NumberFormatter(
                $this->getLanguage(),
                \NumberFormatter::DECIMAL
            );
        }

        return $this->numberFormatter[$cacheKey];
    }

    /**
     * @return \NumberFormatter
     */
    public function getCurrencyFormatterInstance() {
        $cacheKey = implode('|', [
            $this->getLanguage()
        ]);
        if (! isset($this->currencyFormatter[$cacheKey])) {
            if (! class_exists('NumberFormatter')) {
                throw new \RuntimeException('Class not found: NumberFormatter');
            }

            $this->currencyFormatter[$cacheKey] = new \NumberFormatter(
                $this->getLanguage(),
                \NumberFormatter::CURRENCY
            );
        }

        return $this->currencyFormatter[$cacheKey];
    }

    /**
     * @param string|null $currencyCode
     * @return \NumberFormatter
     */
    public function getCurrencySymbolFormatterInstance($currencyCode = null) {
        if ($currencyCode === null) {
            $currencyCode = $this->getCurrencyCode();
        }

        $cacheKey = implode('|', [
            $this->getLanguage(),
            $currencyCode
        ]);
        if (! isset($this->currencySymbolFormatters[$cacheKey])) {
            if (! class_exists('NumberFormatter')) {
                throw new \RuntimeException('Class not found: NumberFormatter');
            }

            $this->currencySymbolFormatters[$cacheKey] = new \NumberFormatter(
                $this->getLanguage() . '@currency=' . $currencyCode,
                \NumberFormatter::CURRENCY
            );
        }

        return $this->currencySymbolFormatters[$cacheKey];
    }

    /**
     * @param string $dateStyle
     * @param string|null $tz
     * @return \IntlDateFormatter
     */
    public function getDateFormatterInstance($dateStyle = 'short', $tz = null) {
        return $this->getDateTimeFormatterInstance($dateStyle, 'none', $tz);
    }

    /**
     * @param string $dateStyle
     * @param string|null $tz
     * @return \IntlDateFormatter
     */
    public function getMonthDayFormatterInstance($dateStyle = 'medium', $tz = null) {
        return $this->getDateTimeFormatterInstance(
            $dateStyle,
            'none',
            $tz,
            $this->getLanguage(),
            'month-day',
            static function ($formatter): void {
                /** @var \IntlDateFormatter $formatter */
                $formatter->setPattern(preg_replace('/([^Md]*)y([^Md]*)/', '', $formatter->getPattern()));
            }
        );
    }

    /**
     * @param string $dateStyle
     * @param string $timeStyle
     * @param string $timezone
     * @param string $language
     * @param string|null $uniqId
     * @param callable|null $onCreateCallback
     * @return \IntlDateFormatter
     * @throws \Exception
     */
    protected function getDateTimeFormatterInstance(
        $dateStyle,
        $timeStyle,
        $timezone = null,
        $language = null,
        $uniqId = null,
        $onCreateCallback = null
    ) {
        $dateStyle = $dateStyle ?: 'short';
        $timeStyle = $timeStyle ?: 'short';
        $timezone = $timezone ?: $this->getTimezone();
        $language = $language ?: $this->getLanguage();
        $cacheKey = implode('|', [
            $dateStyle,
            $timeStyle,
            $timezone,
            $language,
            $uniqId
        ]);
        if (! isset($this->dateTimeFormatter[$cacheKey])) {
            if (! class_exists('IntlDateFormatter')) {
                throw new \Exception('Class not found: IntlDateFormatter');
            }

            $this->dateTimeFormatter[$cacheKey] = new \IntlDateFormatter(
                $language,
                self::DATETIME_FORMATTER_MAPPING[$dateStyle],
                self::DATETIME_FORMATTER_MAPPING[$timeStyle],
                $timezone
            );
            if (is_callable($onCreateCallback)) {
                $onCreateCallback(
                    $this->dateTimeFormatter[$cacheKey],
                    $language,
                    $dateStyle,
                    $timeStyle,
                    $timezone,
                    $uniqId
                );
            }
        }

        return $this->dateTimeFormatter[$cacheKey];
    }

    public function getLocaleCountryCodeISO2(): string {
        return substr($this->getLanguage(), 0, 2);
    }

    public function set(string $key, mixed $value): mixed {
        return $this->callWithLocaleContext(fn($key, $val, $ttl = 0) => $this->fw->set($key, $val, $ttl), [$this->getHiveKey($key), $value]);
    }

    public function get(string $key, ?array $vars = null): mixed {
        $return = $this->callWithLocaleContext(fn($key, $args = \NULL) => $this->fw->get($key, $args), [$this->getHiveKey($key), $vars]);
        if (is_array($return)) {
            return self::fromF3Dict($return);
        }

        return $return;
    }

    public static function toF3Dict(array $dict): array {
        $return = [];
        foreach ($dict as $key => $value) {
            $return[self::toF3DictKey($key)] = $value;
        }

        return $return;
    }

    public static function fromF3Dict(array $dict): array {
        $return = [];
        foreach ($dict as $key => $value) {
            $return[self::fromF3DictKey($key)] = $value;
        }

        return $return;
    }

    /**
     * This conversion is necessary to avoid strings containing periods "." from being split in a tree-like way
     * by F3. So the result string is almost identical to the input one, but with no periods in it. Plus, it's
     * an XML-valid string too.
     */
    protected static function toF3DictKey(string $key): string {
        return str_replace('.', '&period;', $key);
    }

    /**
     * Reverse-conversion function to self::toF3DictKey()
     */
    protected static function fromF3DictKey(string $key): string {
        return str_replace('&period;', '.', $key);
    }

    public function clearCache(): static {
        $this->languages                = [];
        $this->sortedLanguages          = [];
        $this->numberFormatter          = [];
        $this->currencyFormatter        = [];
        $this->currencySymbolFormatters = [];
        $this->dateTimeFormatter        = [];
        $this->missingTranslations      = [];

        return $this;
    }

    public function getHiveKey(string $key): string {
        return $this->prefix . self::toF3DictKey($key);
    }
}
