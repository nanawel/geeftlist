<?php

namespace OliveOil\Core\Model\Validation;


class ErrorAggregator implements ErrorAggregatorInterface
{
    /** @var ErrorInterface[][] */
    protected $errorsByField = [];

    /** @var ErrorInterface[][] */
    protected $errorsByLevel = [];

    /**
     * @inheritDoc
     */
    public function addFieldError(ErrorInterface $error): static {
        if (! array_key_exists($error->getField(), $this->errorsByField)) {
            $this->errorsByField[$error->getField()] = [];
        }

        $this->errorsByField[$error->getField()][] = $error;

        if (! array_key_exists($error->getLevel(), $this->errorsByLevel)) {
            $this->errorsByLevel[$error->getLevel()] = [];
        }

        $this->errorsByLevel[$error->getLevel()][] = $error;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addAllFieldError($errors): static {
        foreach ($errors as $error) {
            $this->addFieldError($error);
        }

        return $this;
    }

    public function getFieldErrors($field = null, $level = null) {
        if ($field !== null && $level !== null) {
            return array_intersect(
                $this->errorsByField[$field] ?? [],
                $this->errorsByLevel[$level] ?? []
            );
        }

        if ($field !== null) {
            return $this->errorsByField[$field] ?? [];
        }

        if ($level !== null) {
            return $this->errorsByLevel[$level] ?? [];
        }

        return $this->errorsByField;
    }

    /**
     * @inheritDoc
     */
    public function merge(ErrorAggregatorInterface $aggregator): static {
        foreach ($aggregator->getFieldErrors() as $fieldErrors) {
            $this->addAllFieldError($fieldErrors);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function clear($field = null): static {
        if ($field === null) {
            $this->errorsByField = [];
            $this->errorsByLevel = [];
        }
        else {
            unset($this->errorsByField[$field]);
            foreach ($this->errorsByLevel as $level => $errors) {
                foreach ($errors as $i => $error) {
                    if ($error->getField() === $field) {
                        unset($this->errorsByLevel[$level][$i]);
                    }
                }
            }
        }

        return $this;
    }

    public function isEmpty(): bool {
        return empty($this->errorsByField);
    }
}
