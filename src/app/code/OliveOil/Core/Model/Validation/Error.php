<?php

namespace OliveOil\Core\Model\Validation;


class Error implements ErrorInterface
{
    /** @var string */
    protected $field;

    /** @var string */
    protected $type;

    /** @var string */
    protected $message;

    /** @var string */
    protected $localizableMessage;

    /** @var int */
    protected $level = self::LEVEL_ERROR;

    /**
     * @param string $field
     */
    public function setField($field): static {
        $this->field = $field;

        return $this;
    }

    /**
     * @param string $type
     */
    public function setType($type): static {
        $this->type = $type;

        return $this;
    }

    /**
     * @param string $message
     */
    public function setMessage($message): static {
        $this->message = $message;

        return $this;
    }

    /**
     * @param string $localizableMessage
     */
    public function setLocalizableMessage($localizableMessage): static {
        $this->localizableMessage = $localizableMessage;

        return $this;
    }

    /**
     * @param int $level
     */
    public function setLevel($level): static {
        $this->level = $level;

        return $this;
    }

    /**
     * @return string
     */
    public function getField() {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getLocalizableMessage() {
        return $this->localizableMessage;
    }

    /**
     * @return int
     */
    public function getLevel() {
        return $this->level;
    }
}
