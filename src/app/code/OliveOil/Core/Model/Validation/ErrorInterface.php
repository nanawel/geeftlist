<?php

namespace OliveOil\Core\Model\Validation;


interface ErrorInterface
{
    /** Missing or NULL value */
    public const TYPE_FIELD_EMPTY = 'field_empty';

    /** Invalid value */
    public const TYPE_FIELD_INVALID = 'field_invalid';

    /** Value is not a scalar */
    public const TYPE_FIELD_NOT_SCALAR = 'field_not_scalar';

    /** Attempted to overwrite a readonly value */
    public const TYPE_FIELD_NOT_OVERWRITABLE = 'field_not_overwritable';

    /** Unknown reason (should *not* happen) */
    public const TYPE_UNKNOWN = 'unknown';

    /** Value has been rejected */
    public const LEVEL_ERROR  = 900;

    /** Value has been altered to comply with requirements */
    public const LEVEL_WARN   = 500;

    /** Value is accepted but might be invalid soon (or other informational message) */
    public const LEVEL_NOTICE = 200;

    /**
     * @param string $field
     * @return $this
     */
    public function setField($field);

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type);

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * @param string $localizableMessage
     * @return $this
     */
    public function setLocalizableMessage($localizableMessage);

    /**
     * @param int $level
     * @return $this
     */
    public function setLevel($level);

    /**
     * @return string
     */
    public function getField();

    /**
     * @return string
     */
    public function getType();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getLocalizableMessage();

    /**
     * @return int
     */
    public function getLevel();
}
