<?php

namespace OliveOil\Core\Model\Validation;


interface ErrorAggregatorInterface
{
    /**
     * @return $this
     */
    public function addFieldError(ErrorInterface $error);

    /**
     * @param \OliveOil\Core\Model\Validation\ErrorInterface[] $errors
     * @return $this
     */
    public function addAllFieldError($errors);

    /**
     * @param string|null $field
     * @param int|null $level
     * @return ErrorInterface[][]|ErrorInterface[]
     */
    public function getFieldErrors($field = null, $level = null);

    /**
     * @return $this
     */
    public function merge(ErrorAggregatorInterface $aggregator);

    /**
     * @param string $field
     * @return $this
     */
    public function clear($field = null);

    /**
     * @return bool
     */
    public function isEmpty();
}
