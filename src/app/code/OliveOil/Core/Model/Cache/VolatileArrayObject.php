<?php

namespace OliveOil\Core\Model\Cache;

/**
 * Class VolatileArrayObject
 *
 * Special ArrayObject subclass that allows to clear all instances content at once
 * between test cases.
 * Used by properties using arrays as internal caches that can be invalidated as context changes.
 */
class VolatileArrayObject extends \OliveOil\Core\Model\Cache\ArrayObject
{
    /** @var self[] */
    protected static $__instances = [];

    public function __construct($input = [], $flags = 0, $iterator_class = "ArrayIterator") {
        parent::__construct($input, $flags, $iterator_class);
        self::$__instances[spl_object_hash($this)] = $this;
    }

    public function __destruct() {
        unset(self::$__instances[spl_object_hash($this)]);
    }

    public static function clearAllObjects(): void {
        foreach (self::$__instances as $instance) {
            $instance->clear();
        }
    }
}
