<?php

namespace OliveOil\Core\Model\Cache;

use ArrayAccess;
use Countable;
use IteratorAggregate;
use OliveOil\Core\Iface\FlagAware;
use Serializable;

interface ArrayObjectInterface extends IteratorAggregate, ArrayAccess, Serializable, Countable, FlagAware
{
    /**
     * @return void
     */
    public function clear();
}
