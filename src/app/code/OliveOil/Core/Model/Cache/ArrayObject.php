<?php

namespace OliveOil\Core\Model\Cache;


use OliveOil\Core\Model\Traits\FlagTrait;

class ArrayObject extends \OliveOil\Core\Model\ArrayObject implements ArrayObjectInterface
{
    use FlagTrait;
}
