<?php

namespace OliveOil\Core\Model;

/**
 * @template T1 of \OliveOil\Core\Model\AbstractModel
 * @template T2 of \OliveOil\Core\Model\AbstractModel
 * @template T1Collection of ResourceModel\Iface\Collection<T1>
 * @template T2Collection of ResourceModel\Iface\Collection<T2>
 */
abstract class AbstractModelManyToManyHelper
{
    protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory;

    protected \OliveOil\Core\Helper\Transaction $transactionHelper;

    /** @var class-string|null */
    protected ?string $aRelationClass;

    /** @var class-string|null */
    protected ?string $bRelationClass;

    /**
     * @param ResourceModel\Iface\RelationHelperInterface<T1, T2, T1Collection, T2Collection> $resourceRelationHelper
     * @param string|null $aEntityCode
     * @param string|null $bEntityCode
     * @param string $aRelationName
     * @param string $bRelationName
     * @param string|null $aRelationEventSuffix
     * @param string|null $bRelationEventSuffix
     */
    public function __construct(
        \OliveOil\Core\Model\Context $context,
        protected \OliveOil\Core\Model\ResourceModel\Iface\RelationHelperInterface $resourceRelationHelper,
        protected $aEntityCode,
        protected $bEntityCode,
        protected $aRelationName,
        protected $bRelationName,
        protected $aRelationEventSuffix = null,
        protected $bRelationEventSuffix = null
    ) {
        $this->modelFactory = $context->getModelFactory();
        $this->transactionHelper = $context->getTransactionHelper();
        $this->aRelationClass = $this->aEntityCode ? $this->modelFactory->resolve($this->aEntityCode) : null;
        $this->bRelationClass = $this->bEntityCode ? $this->modelFactory->resolve($this->bEntityCode) : null;
    }

    /**
     * @param array<T1> $as
     * @param array<T2> $bs
     */
    public function addLinks(array $as, array $bs): int {
        // Nothing to change, exit early
        if (!$as || !$bs) {
            // Should we throw an exception in this case instead?
            return 0;
        }

        // Swap $as and $b if needed
        \OliveOil\swap_if($as, $bs, \OliveOil\isObjectArrayOfType($as, $this->bRelationClass));

        if ($this->aRelationClass) {
            \OliveOil\assertObjectArrayOfType($as, $this->aRelationClass);
        }

        if ($this->bRelationClass) {
            \OliveOil\assertObjectArrayOfType($bs, $this->bRelationClass);
        }

        $eventParams = [
            $this->aRelationName => $as,
            $this->bRelationName => $bs
        ];

        $addAEventGroup = sprintf('add%s', $this->getARelationEventSuffix());
        $addBEventGroup = sprintf('add%s', $this->getBRelationEventSuffix());

        foreach ($as as $a) {
            $a->triggerEvent($addBEventGroup . '::before', $eventParams);
        }

        foreach ($bs as $b) {
            $b->triggerEvent($addAEventGroup . '::before', $eventParams);
        }

        $changesCount = $this->resourceRelationHelper->addLinks($as, $bs);
        $eventParams += ['changes_count' => $changesCount];

        foreach ($as as $a) {
            $a->triggerEvent($addBEventGroup . '::after', $eventParams);
        }

        foreach ($bs as $b) {
            $b->triggerEvent($addAEventGroup . '::after', $eventParams);
        }

        return $changesCount;
    }

    /**
     * @param array<T1> $as
     * @param array<T2> $bs
     * @param boolean $negate TRUE to remove T2 from $as NOT present in $bs, FALSE otherwise
     */
    public function removeLinks(array $as, array $bs, bool $negate = false): int {
        // Nothing to change, exit early
        if (!$negate && (!$as || !$bs)) {
            // Should we throw an exception in this case instead?
            return 0;
        }

        // Swap $as and $b if needed
        \OliveOil\swap_if($as, $bs, \OliveOil\isObjectArrayOfType($as, $this->bRelationClass));

        if ($this->aRelationClass) {
            \OliveOil\assertObjectArrayOfType($as, $this->aRelationClass);
        }

        if ($this->bRelationClass) {
            \OliveOil\assertObjectArrayOfType($bs, $this->bRelationClass);
        }

        $eventParams = [
            $this->aRelationName => $as,
            $this->bRelationName => $bs,
            'negate'             => $negate
        ];

        $addAEventGroup = sprintf('remove%s', $this->getARelationEventSuffix());
        $addBEventGroup = sprintf('remove%s', $this->getBRelationEventSuffix());

        foreach ($as as $a) {
            $a->triggerEvent($addBEventGroup . '::before', $eventParams);
        }

        foreach ($bs as $b) {
            $b->triggerEvent($addAEventGroup . '::before', $eventParams);
        }

        $changesCount = $this->resourceRelationHelper->removeLinks($as, $bs, $negate);
        $eventParams += ['changes_count' => $changesCount];

        foreach ($as as $a) {
            $a->triggerEvent($addBEventGroup . '::after', $eventParams);
        }

        foreach ($bs as $b) {
            $b->triggerEvent($addAEventGroup . '::after', $eventParams);
        }

        return $changesCount;
    }

    /**
     * Assign the specified $objects2 to all $objects1, replacing any previous existing links.
     *
     * @param array<T1|T2> $objects1
     * @param array<T2|T1> $objects2
     */
    public function replaceLinks(array $objects1, array $objects2): int {
        if ($objects1 === []) {
            // Here we throw an exception because this call is highly invalid and may reveal a logic issue.
            throw new \InvalidArgumentException('$objects1 cannot be empty.');
        }

        $changesCount = 0;
        $this->transactionHelper->execute(function () use ($objects1, $objects2, &$changesCount): void {
            foreach ($objects1 as $object1) {
                $currentLinkedObjects = $this->getLinkedObjectsForReplace($object1);

                $changesCount += $this->removeLinks(
                    [$object1],
                    array_udiff($currentLinkedObjects, $objects2, static fn($o1, $o2): int => $o1->getId() <=> $o2->getId())
                );

                $changesCount += $this->addLinks(
                    [$object1],
                    array_udiff($objects2, $currentLinkedObjects, static fn($o1, $o2): int => $o1->getId() <=> $o2->getId())
                );
            }
        });

        return $changesCount;
    }

    /**
     * @param T1|T2 $object
     * @return int[]
     */
    public function getLinkedObjectIds($object): array {
        return $this->resourceRelationHelper->getLinkedIds($object);
    }

    /**
     * @param T1|T2 $object
     * @return array<T2|T1>
     */
    public function getLinkedObjects($object): array {
        return $this->resourceRelationHelper->getLinkedCollection($object)->getItems();
    }

    /**
     * @param T1|T2 $object
     * @return array<T2|T1>
     */
    public function getLinkedObjectsForReplace($object): array {
        return $this->getLinkedObjects($object);
    }

    protected function getARelationEventSuffix(): string {
        return ucfirst($this->aRelationEventSuffix ?? $this->aRelationName);
    }

    protected function getBRelationEventSuffix(): string {
        return ucfirst($this->bRelationEventSuffix ?? $this->bRelationName);
    }

    /**
     * @param bool $asTarget TRUE to return the relation event param considering $object as the event's target,
     *                       FALSE otherwise
     * @return string
     */
    protected function getRelationEventParam(object $object, $asTarget): string {
        return match (true) {
            is_a($object, $this->aRelationClass)
                || ($this->aRelationClass === null && !is_a($object, $this->bRelationClass)) => $asTarget
                    ? $this->bRelationName
                    : $this->aRelationName,
            is_a($object, $this->bRelationClass)
                || ($this->bRelationClass === null && !is_a($object, $this->aRelationClass)) => $asTarget
                    ? $this->aRelationName
                    : $this->bRelationName,
            default => throw new \InvalidArgumentException('Invalid object class: ' . $object::class),
        };
    }

    /**
     * @param bool $asTarget TRUE to return the relation event suffix considering $object as the event's target,
     *                       FALSE otherwise
     */
    protected function getRelationEventSuffix(object $object, $asTarget): string {
        return match (true) {
            is_a($object, $this->aRelationClass)
                || ($this->aRelationClass === null && !is_a($object, $this->bRelationClass)) => $asTarget
                    ? $this->getBRelationEventSuffix()
                    : $this->getARelationEventSuffix(),
            is_a($object, $this->bRelationClass)
                || ($this->bRelationClass === null && !is_a($object, $this->aRelationClass)) => $asTarget
                    ? $this->getARelationEventSuffix()
                    : $this->getBRelationEventSuffix(),
            default => throw new \InvalidArgumentException('Invalid object class: ' . $object::class),
        };
    }
}
