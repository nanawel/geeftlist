<?php

namespace OliveOil\Core\Model;

interface AppInterface
{
    /**
     * @return void
     */
    public function bootstrap();

    /**
     * @return void
     */
    public function setup();

    /**
     * @return void
     */
    public function onShutdown();

    public function checkRequirements();
}
