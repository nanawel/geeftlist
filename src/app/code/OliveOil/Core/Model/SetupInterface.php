<?php

namespace OliveOil\Core\Model;


use OliveOil\Core\Service\Di\Container;

interface SetupInterface
{
    public const TYPE_SCHEMA     = 'schema';

    public const TYPE_DATA       = 'data';

    public const TYPE_SAMPLEDATA = 'sampledata';

    public function isInstalled(): bool;

    public function isDbUp(): bool;

    /**
     * @return string[]
     */
    public function getDbInfo(bool $hidePassword = true): array;

    public function install(bool $dropFirst = false, ?string $targetVersion = null): static;

    public function upgrade(?string $targetVersion = null, bool $force = false): static;

    public function uninstall(?string $targetVersion = null, bool $force = true): static;

    public function installSampleData(?string $targetVersion = null): static;

    public function upgradeSampleData(?string $targetVersion = null): static;

    public function uninstallSampleData(?string $targetVersion = null): static;

    public function getCodeVersion(string $type): string;

    public function getDbVersion(string $type): string;

    public function addError(string $msg, ?\Throwable $e = null): static;

    public function clearErrors(): static;

    public function getErrors(bool $clear = true): array;

    public function getCryptService(): \OliveOil\Core\Service\Crypt;

    public function getContainer(): Container;
}
