<?php


namespace OliveOil\Core\Model\Http;


class Response implements ResponseInterface
{
    /**
     * Response constructor.
     *
     * @param int $code
     * @param string[] $headers
     */
    public function __construct(protected $code = null, protected array $headers = [], protected mixed $body = null)
    {
    }

    /**
     * @inheritDoc
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    public function setCode($code): static {
        $this->code = $code;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHeaders(): array {
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    public function setHeaders(array $headers): static {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addHeaders(array $headers): static {
        $this->headers = array_merge($this->headers, $headers);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBody(): string {
        return (string) $this->body;
    }

    /**
     * @inheritDoc
     */
    public function setBody($body): static {
        $this->body = $body;

        return $this;
    }
}
