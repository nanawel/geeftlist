<?php


namespace OliveOil\Core\Model\Http;


interface RequestInterface
{
    /**
     * @return $this
     */
    public function fromGlobals();

    /**
     * @param RequestInterface|null $related
     * @return $this
     */
    public function setRelatedRequest(RequestInterface $related = null);

    /**
     * @param string[] $routeParams
     * @param string[] $rawRequestParams
     * @return $this
     */
    public function initRoute(string $controllerName, string $actionName, array $routeParams, array $rawRequestParams);

    /**
     * @see \OliveOil\Core\Constants\Http::VERBS
     * @return string
     */
    public function getVerb();


    /**
     * @return string[]
     */
    public function getHeaders();

    /**
     * @param string $header
     * @return string
     */
    public function getHeader($header);

    /**
     * @return string
     */
    public function getControllerName();

    /**
     * @return string
     */
    public function getActionName();

    /**
     * @return string[]
     */
    public function getQueryParams();

    /**
     * @param string $param
     * @return string|array|null
     */
    public function getQueryParam($param);

    /**
     * @inheritDoc
     */
    public function getPostParam($param);

    /**
     * @return string[]
     */
    public function getRawRequestParams();

    /**
     * @return string
     */
    public function getRawRequestParam($param);

    /**
     * @return string[]
     */
    public function getRequestParams();

    /**
     * @param string $param
     * @return string|null
     */
    public function getRequestParam($param);

    /**
     * @param string $param
     * @return $this
     */
    public function setRequestParam($param, mixed $value);

    /**
     * @return string[]
     */
    public function getRouteParams();

    /**
     * @return string
     */
    public function getRoutePath();

    /**
     * @param string $param
     * @return string|null
     */
    public function getRouteParam($param);

    /**
     * @return string
     */
    public function getBody();

    /**
     * @return string
     */
    public function getMimetype();
}
