<?php


namespace OliveOil\Core\Model\Http;


use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\Di\Exception;

class Request implements RequestInterface
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    protected ?string $verb;

    /** @var string */
    protected $controllerName;

    /** @var string */
    protected $actionName;

    /** @var string[] */
    protected $routeParams = [];

    /** @var string[] */
    protected $queryParams = [];

    /** @var string[] */
    protected $overridenParams = [];

    /** @var string[] */
    protected $rawRequestParams;

    /**
     * @param string $verb
     * @param string[] $headers
     * @param string[] $query
     * @param string[] $routeParams
     * @param string $body
     * @param RequestInterface|null $related
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Helper\Data\Factory $helperDataFactory,
        \OliveOil\Core\Service\Log $logService,
        $verb = null,
        protected array $headers = [],
        array $query = [],
        array $routeParams = [],
        protected $body = null,
        protected ?\OliveOil\Core\Model\Http\RequestInterface $related = null
    ) {
        $this->logger = $logService->getLoggerForClass($this);
        $this->verb = self::normalizeVerb($verb);
        $this->queryParams = $query;
        $this->routeParams = $routeParams;
    }

    /**
     * @inheritDoc
     */
    public function fromGlobals(): static {
        $this->verb        = self::normalizeVerb($this->fw->get('VERB'));
        $this->headers     = getallheaders();
        $this->queryParams = $this->fw->get('GET') ?? [];
        $this->routeParams = $this->fw->get('PARAMS') ?? [];

        // Force ignore body if not relevant with the current verb
        if (in_array($this->getVerb(), [Http::VERB_POST, Http::VERB_PUT, Http::VERB_PATCH, Http::VERB_DELETE])) {
            $this->body = $this->getPost() ?? null;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setRelatedRequest(RequestInterface $related = null): static {
        $this->related = $related;

        return $this;
    }

    public function initRoute(
        string $controllerName,
        string $actionName,
        array $routeParams,
        array $rawRequestParams
    ): static {
        $this->controllerName = $controllerName;
        $this->actionName = $actionName;
        $this->routeParams = $routeParams;
        $this->rawRequestParams = $rawRequestParams;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRelatedRequest(): ?\OliveOil\Core\Model\Http\RequestInterface {
        return $this->related;
    }

    /**
     * @inheritDoc
     */
    public function getVerb(): ?string {
        return $this->verb;
    }

    /**
     * @inheritDoc
     */
    public function getHeaders(): array {
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    public function getHeader($header) {
        return $this->headers[$header] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getControllerName() {
        return $this->controllerName;
    }

    /**
     * @inheritDoc
     */
    public function getActionName() {
        return $this->actionName;
    }

    /**
     * @inheritDoc
     */
    public function getQueryParams() {
        return $this->queryParams;
    }

    /**
     * @inheritDoc
     */
    public function getQueryParam($param) {
        return $this->queryParams[$param] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getPostParam($param) {
        return $this->verb === Http::VERB_POST && isset($this->body[$param])
            ? $this->body[$param]
            : null;
    }

    /**
     * @inheritDoc
     */
    public function getRequestParams(): array {
        return array_merge(
            $this->getQueryParams(),    // Lowest priority
            $this->getPost(),
            $this->getRouteParams(),
            $this->overridenParams      // Highest priority
        );
    }

    /**
     * @inheritDoc
     */
    public function setRequestParam($param, $value): static {
        $this->overridenParams[$param] = $value;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRequestParam($param) {
        return $this->overridenParams[$param]   // Highest priority
            ?? $this->getRouteParam($param)
            ?? $this->getPostParam($param)
            ?? $this->getQueryParam($param)     // Lowest priority
            ?? null;
    }

    /**
     * @return string[]
     */
    public function getRawRequestParams() {
        return $this->rawRequestParams;
    }

    /**
     * @return string
     */
    public function getRawRequestParam($param) {
        return $this->rawRequestParams[$param] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getRouteParams() {
        return $this->routeParams;
    }

    /**
     * @inheritDoc
     */
    public function getRouteParam($param) {
        return $this->routeParams[$param] ?? null;
    }

    /**
     * @return string
     */
    public function getRoutePath() {
        // This is a F3 standard behavior: key "0" holds the full request path
        return $this->rawRequestParams[0];
    }

    /**
     * @inheritDoc
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * @inheritDoc
     */
    public function getMimetype(): string {
        $contentType = $this->fw->get('SERVER.CONTENT_TYPE');
        [$mime] = explode('; ', (string) $contentType) ?? [null];

        return $mime;
    }

    /**
     * @return array|mixed|null
     */
    protected function getPost() {
        $contentType = $this->getMimetype();

        $post = null;
        if (!$contentType || in_array($contentType, ['application/x-www-form-urlencoded', 'multipart/form-data'])) {
            $post = $this->fw->get('POST');
        }
        else {
            try {
                $helper = $this->helperDataFactory->getFromCode($contentType);
            }
            catch (Exception $e) {
                $this->logger->warning('Unsupported Content-Type provided: ' . $contentType, ['exception' => $e]);

                return null;
            }

            try {
                $post = $helper->decode($this->fw->get('BODY'));
            }
            catch (\Throwable $e) {
                $this->logger->warning(
                    sprintf('Invalid request body for the provided Content-Type "%s".', $contentType),
                    ['exception' => $e]
                );

                return null;
            }
        }

        return $post;
    }

    /**
     * @param string|null $verb
     */
    public static function normalizeVerb($verb): ?string {
        return $verb !== null ? strtoupper($verb) : null;
    }
}
