<?php


namespace OliveOil\Core\Model\Http;


interface ResponseInterface
{
    /**
     * @return int
     */
    public function getCode();

    /**
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * @return string[]
     */
    public function getHeaders();

    /**
     * @param string[] $headers
     * @return $this
     */
    public function setHeaders(array $headers);

    /**
     * @return $this
     */
    public function addHeaders(array $headers);

    /**
     * @return string
     */
    public function getBody();

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body);
}
