<?php

namespace OliveOil\Core\Model;


abstract class Setup implements SetupInterface
{
    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected array $errors = [];

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Crypt $cryptService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory,
        protected \OliveOil\Core\Service\GenericFactoryInterface $resourceModelFactory,
        protected \OliveOil\Core\Service\Di\Container $container
    ) {
        $this->eventManager = $this->eventService->getEventManager($this);
    }

    abstract public function getResource(): \OliveOil\Core\Model\ResourceModel\Iface\Setup;

    public function isInstalled(): bool {
        return $this->getResource()->isInstalled($this);
    }

    public function isDbUp(): bool {
        return $this->getResource()->isDbUp($this);
    }

    public function getDbInfo(bool $hidePassword = true): array {
        if ($dbInfo = $this->appConfig->getValue('RESOURCE_CONFIG.db')) {
            if (isset($dbInfo['password']) && $hidePassword) {
                $dbInfo['password'] = preg_replace('/./', '*', (string) $dbInfo['password']);
            }

            return $dbInfo;
        }

        return [];
    }

    public function install(bool $dropFirst = false, ?string $targetVersion = null): static {
        $executed = $this->doInstall($dropFirst, $targetVersion);

        $this->eventManager->trigger('install::after', $this, [
            'setup' => $this,
            'targetVersion' => $targetVersion,
            'executed' => $executed
        ]);

        return $this;
    }

    protected function doInstall(bool $dropFirst = false, ?string $targetVersion = null): bool {
        return $this->getResource()->install($this, $dropFirst, $targetVersion);
    }

    public function upgrade(?string $targetVersion = null, bool $force = false): static {
        $executed = $this->doUpgrade($targetVersion, $force);

        $this->eventManager->trigger('upgrade::after', $this, [
            'setup' => $this,
            'targetVersion' => $targetVersion,
            'executed' => $executed
        ]);

        return $this;
    }

    protected function doUpgrade(?string $targetVersion = null, bool $force = false) {
        return $this->getResource()->upgrade($this, $targetVersion, $force);
    }

    public function uninstall(?string $targetVersion = null, bool $force = false): static {
        $executed = $this->doUninstall($targetVersion, $force);

        $this->eventManager->trigger('uninstall::after', $this, [
            'setup' => $this,
            'targetVersion' => $targetVersion,
            'executed' => $executed
        ]);

        return $this;
    }

    protected function doUninstall(?string $targetVersion = null, bool $force = true) {
        return $this->getResource()->uninstall($this, $targetVersion, $force);
    }

    public function installSampleData(?string $targetVersion = null): static {
        $executed = $this->doInstallSampleData($targetVersion);

        $this->eventManager->trigger('installSampleData::after', $this, [
            'setup' => $this,
            'targetVersion' => $targetVersion,
            'executed' => $executed
        ]);

        return $this;
    }

    protected function doInstallSampleData(?string $targetVersion = null) {
        return $this->getResource()->installSampleData($this, $targetVersion);
    }

    public function upgradeSampleData(?string $targetVersion = null): static {
        $executed = $this->doUpgradeSampleData($targetVersion);

        $this->eventManager->trigger('upgradeSampleData::after', $this, [
            'setup' => $this,
            'targetVersion' => $targetVersion,
            'executed' => $executed
        ]);

        return $this;
    }

    protected function doUpgradeSampleData(?string $targetVersion = null) {
        return $this->getResource()->upgradeSampleData($this, $targetVersion);
    }

    public function uninstallSampleData(?string $targetVersion = null): static {
        $executed = $this->doUninstallSampleData($targetVersion);

        $this->eventManager->trigger('uninstallSampleData::after', $this, [
            'setup' => $this,
            'targetVersion' => $targetVersion,
            'executed' => $executed
        ]);

        return $this;
    }

    protected function doUninstallSampleData(?string $targetVersion = null) {
        return $this->getResource()->uninstallSampleData($this, $targetVersion);
    }

    public function getCodeVersion(string $type): string {
        return $this->getResource()->getCodeVersion($this, $type);
    }

    /**
     * @inheritDoc
     */
    public function getDbVersion(string $type): string {
        return $this->getResource()->getDbVersion($this, $type);
    }

    public function addError(string $msg, ?\Throwable $e = null): static {
        $this->errors[] = [
            'message' => $msg,
            'exception' => $e
        ];

        return $this;
    }

    public function clearErrors(): static {
        $this->errors = [];

        return $this;
    }

    public function getErrors(bool $clear = true): array {
        $errors = $this->errors;
        if ($clear) {
            $this->clearErrors();
        }

        return $errors;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }

    public function getContainer(): \OliveOil\Core\Service\Di\Container {
        return $this->container;
    }
}
