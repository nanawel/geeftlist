<?php

namespace OliveOil\Core\Model\Repository;


/**
 * @template T of \OliveOil\Core\Model\AbstractModel
 */
class ModelCache
{
    protected \OliveOil\Core\Model\Cache\ArrayObjectInterface $modelsById;

    protected \OliveOil\Core\Model\Cache\ArrayObjectInterface $modelsByField;

    protected \OliveOil\Core\Model\Cache\ArrayObjectInterface $modelFields;

    /** @var static[] */
    protected static array $selfInstances = [];

    /**
     * @param class-string $cacheClass
     */
    public function __construct(
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected string $cacheClass = \OliveOil\Core\Model\Cache\ArrayObjectInterface::class,
        protected bool $isEnabled = true
    ) {
        self::$selfInstances[] = $this;

        // Init cache cells
        $this->clearCache();
    }

    /**
     * @return T|null
     */
    public function get(int|string $id, ?string $field = null): ?\OliveOil\Core\Model\AbstractModel {
        if ($this->isEnabled) {
            if ($field === null && isset($this->modelsById[$id])) {
                return $this->modelsById[$id];
            }

            if ($field !== null && isset($this->modelsByField[$field][$id])) {
                return $this->modelsByField[$field][$id];
            }
        }

        return null;
    }

    public function set(\OliveOil\Core\Model\AbstractModel $model, int|string $id, ?string $field = null): static {
        if ($this->isEnabled) {
            if ($field === null) {
                $this->modelsById[$id] = $model;

                // Ensure no remaining - and potentially different - instance is left in $this->modelsByField
                $this->clearModelsByFieldCache($id);
            }
            else {
                $this->modelsByField[$field][$id] = $model;
                $this->modelsById[$model->getId()] = $model;
                $this->modelFields[$model->getId()][$field] = $id;
            }
        }

        return $this;
    }

    public function delete(int|string $id): static {
        if (isset($this->modelsById[$id])) {
            unset($this->modelsById[$id]);
        }

        $this->clearModelsByFieldCache($id);

        return $this;
    }

    protected function clearModelsByFieldCache(int|string $id): static {
        if (!empty($this->modelFields[$id])) {
            foreach ($this->modelFields[$id] as $field => $value) {
                unset($this->modelsByField[$field][$value]);
            }

            unset($this->modelFields[$id]);
        }

        return $this;
    }

    public function clearCache(): static {
        $this->modelsById    = $this->coreFactory->make($this->cacheClass);
        $this->modelsByField = $this->coreFactory->make($this->cacheClass);
        $this->modelFields   = $this->coreFactory->make($this->cacheClass);

        return $this;
    }

    public static function clearAllModelCache(): void {
        foreach (self::$selfInstances as $modelCache) {
            $modelCache->clearCache();
        }
    }
}
