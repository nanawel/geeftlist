<?php
namespace OliveOil\Core\Model;

use OliveOil\Core\Exception\InvalidFieldValueException;
use OliveOil\Core\Exception\InvalidTypeException;
use OliveOil\Core\Iface\FlagAware;
use OliveOil\Core\Iface\ListenableInstance;
use OliveOil\Core\Model\Entity\FieldInterface;
use OliveOil\Core\Model\Traits\FlagTrait;
use OliveOil\Core\Model\Traits\ListenableInstanceTrait;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

abstract class AbstractModel extends MagicObject implements ListenableInstance, FlagAware, \Stringable
{
    use FlagTrait;
    use ListenableInstanceTrait;

    public const DEFAULT_ID_FIELD = 'id';

    protected \OliveOil\Core\Service\GenericFactoryInterface $fieldModelFactory;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \Psr\Log\LoggerInterface $logger;

    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    protected string $eventObjectParam = '';

    protected ?string $idField = null;

    protected bool $isDeleted = false;

    protected array $fieldModels = [];

    public function __construct(
        \OliveOil\Core\Model\Context $context,
        array $data = []
    ) {
        $this->fieldModelFactory = $context->getFieldModelFactory();
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->logger = $context->getLogService()->getLoggerForClass($this);
        $this->coreFactory = $context->getCoreFactory();
        parent::__construct($data);
    }

    /**
     * @deprecated
     * @return \OliveOil\Core\Model\ResourceModel\Iface\Model
     */
    abstract public function getResource();

    public function __toString(): string {
        return sprintf('%s(%d)', static::class, $this->getId() ?: '<no-id>');
    }

    /**
     * @return static
     */
    public function cloneAsNew() {
        $clone = clone $this;

        return $clone->setId(null)
            ->resetOrigData();
    }

    public function getId() {
        return $this->getData($this->getIdField());
    }

    public function setId($id) {
        return $this->setData($this->getIdField(), $id);
    }

    public function getIdField() {
        if (!$this->idField) {
            $this->idField = $this->getResource()->getIdField() ?: self::DEFAULT_ID_FIELD;
        }

        return $this->idField;
    }

    public function isObjectNew(): bool {
        return $this->getOrigData($this->getIdField()) === null;
    }

    public function clearInstance(): static {
        $this->resetData();
        $this->resetOrigData();
        $this->triggerEvent('clearInstance::after');
        return $this;
    }

    /**
     * @deprecated Use Repository::get() instead.
     *
     * @param int|string $id
     * @param null|string $field
     * @return $this
     */
    public function load($id = null, $field = null): static {
        $this->getResource()->load($this, $id, $field);

        return $this;
    }

    public function beforeLoad($id = null, $field = null): static {
        $this->triggerEvent('load::before', ['id' => $id, 'field' => $field]);

        return $this;
    }

    public function afterLoad(): static {
        $this->isDeleted = false;
        foreach (array_keys($this->fieldModels) as $field) {
            $this->getFieldModelInstance($field)->afterModelLoad($this, $this->getData($field));
        }

        $this->triggerEvent('load::after');

        return $this;
    }

    /**
     * @deprecated Use Repository::save() instead.
     * @return $this
     */
    public function save(): static {
        $this->getResource()->save($this);

        return $this;
    }

    public function beforeSave(): static {
        $this->triggerEvent('save::before');

        return $this;
    }

    public function afterSave(): static {
        $this->triggerEvent('save::after');

        return $this;
    }

    public function afterSaveCommit(): static {
        $this->isDeleted = false;
        $this->triggerEvent('save::after_commit');
        $this->setOrigData($this->getData());

        return $this;
    }

    /**
     * @deprecated Use Repository::delete() instead.
     *
     * @return $this
     */
    public function delete(): static {
        $this->getResource()->delete($this);

        return $this;
    }

    public function beforeDelete(): static {
        $this->triggerEvent('delete::before');

        return $this;
    }

    public function afterDelete(): static {
        $this->isDeleted = true;
        $this->triggerEvent('delete::after');

        return $this;
    }

    /**
     * @return FieldInterface
     * @throws InvalidTypeException
     */
    public function getFieldModelInstance(string $field) {
        if (isset($this->fieldModels[$field])) {
            /** @var FieldInterface $model */
            $model = $this->fieldModelFactory->getFromCode($this->fieldModels[$field]);

            return $model;
        }

        throw new InvalidTypeException($field . ' is not a supported complex field.');
    }

    /**
     * @return \OliveOil\Core\Model\Validation\ErrorAggregatorInterface
     * @throws InvalidFieldValueException
     */
    public function validateComplexFieldsValues() {
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $aggregator */
        $aggregator = $this->coreFactory->make(ErrorAggregatorInterface::class);
        foreach (array_keys($this->fieldModels) as $field) {
            if ($error = $this->getFieldModelInstance($field)->validateValue($this, $this->getData($field))) {
                $error->setField($field);
                $aggregator->addFieldError($error);
            }
        }

        return $aggregator;
    }

    /**
     * @return \OliveOil\Core\Model\Validation\ErrorAggregatorInterface
     * @throws InvalidFieldValueException
     */
    public function validate() {
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $aggregator */
        $aggregator = $this->coreFactory->make(ErrorAggregatorInterface::class);

        // Validation is delegated via an event because it may depend on current context
        $this->triggerEvent('validate', ['error_aggregator' => $aggregator]);
        $aggregator->merge($this->validateComplexFieldsValues());

        return $aggregator;
    }

    /**
     * @param string $field
     * @return mixed|string
     * @throws InvalidTypeException
     */
    public function getFrontendValue($field) {
        $value = $this->getData($field);
        if ($fieldModelInstance = $this->getFieldModelInstance($field)) {
            return $fieldModelInstance->getFrontendValue($this, $value);
        }

        return $value;
    }

    /**
     * Triggers:
     *  - an instance event
     *  - an event via the currently attached EventManager
     * using the same params.
     *
     * @param string $eventName
     * @return $this
     */
    public function triggerEvent($eventName, array $params = []): static {
        $params += [
            'model' => $this,
        ];
        if ($this->eventObjectParam !== null) {
            $params[$this->eventObjectParam] = $this;
        }

        $this->triggerInstanceEvent($eventName, $this, $params);
        $this->eventManager->trigger($eventName, $this, $params);

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted() {
        return $this->isDeleted;
    }

    /**
     * @return \OliveOil\Core\Model\Entity\FieldInterface[]
     */
    public function getFieldModels() {
        return $this->fieldModels;
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }
}
