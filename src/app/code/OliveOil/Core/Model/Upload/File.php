<?php

namespace OliveOil\Core\Model\Upload;


/**
 * Class File
 */
class File extends \Sirius\Upload\Result\File
{
    public static function fromSiriusFile(\Sirius\Upload\Result\File $file): static {
        /** @phpstan-ignore-next-line */
        $instance = new static($file->file, $file->container);

        return $instance;
    }

    /**
     * @param string $name
     * @return array|mixed
     */
    public function getData($name = null) {
        if ($name === null) {
            return $this->file;
        }

        return $this->file[$name] ?? null;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setData($name, mixed $value): static {
        $this->file[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function unsetData($name): static {
        unset($this->file[$name]);

        return $this;
    }
}
