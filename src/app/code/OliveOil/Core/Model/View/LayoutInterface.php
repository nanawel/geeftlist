<?php

namespace OliveOil\Core\Model\View;


use OliveOil\Core\Block\BlockInterface;

interface LayoutInterface
{
    /**
     * @return array[]
     */
    public function getAllBlockConfig();

    /**
     * @param string $blockName
     * @param string|null $configKey
     * @return array|string|mixed
     */
    public function getBlockConfig($blockName, $configKey = null);

    /**
     * @param string $blockName
     * @param string|array $configKey
     * @param mixed|null $value
     * @return $this
     */
    public function setBlockConfig($blockName, $configKey, $value = null);

    /**
     * @return $this
     */
    public function clear();

    /**
     * @param string $parentName
     * @param string|BlockInterface $child
     * @return $this
     */
    public function addChild($parentName, $child);

    /**
     * @param string $blockName
     * @param string $parentName
     * @return $this
     */
    public function removeBlock($blockName, $parentName = null);

    /**
     * @param string $type
     * @return BlockInterface
     */
    public function newBlock($type);

    /**
     * @param string $blockName
     * @return BlockInterface|null
     */
    public function getBlock($blockName);

    /**
     * @param string $blockName
     * @return string
     */
    public function getBlockTemplate($blockName);

    /**
     * @param string $blockName
     * @param string $template
     * @return $this
     */
    public function setBlockTemplate($blockName, $template);

    /**
     * @param string $blockName
     * @return BlockInterface[]
     */
    public function getBlockChildren($blockName);

    /**
     * @return BlockInterface
     */
    public function getPageBlock();

    /**
     * @return string
     */
    public function getPageTemplate();

    /**
     * @param string $template
     * @return $this
     */
    public function setPageTemplate($template);

    /**
     * @param string|array $configKey
     * @param mixed|null $value
     * @return $this
     */
    public function setPageConfig($configKey, $value = null);

    /**
     * @return BlockInterface
     */
    public function getContentWrapperBlock();

    /**
     * @return string $template
     */
    public function getContentWrapperTemplate();

    /**
     * @param string $template
     * @return $this
     */
    public function setContentWrapperTemplate($template);

    /**
     * @param string|array $configKey
     * @param mixed|null $value
     * @return $this
     */
    public function setContentWrapperConfig($configKey, $value = null);

    /**
     * @param string $blockName
     * @param callable $callable
     * @param array $params
     * @return $this
     */
    public function setBlockCachePlaceholder($blockName, $callable = null, $params = []);

    /**
     * @param string $blockName
     * @return mixed
     */
    public function getBlockCachePlaceholder($blockName);
}
