<?php

namespace OliveOil\Core\Model\View;


class Breadcrumbs
{
    /** @var array */
    protected $crumbs = [];

    public function __construct(protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder)
    {
    }

    public function setCrumbs(array $crumbs): static {
        $this->crumbs = $crumbs;

        return $this;
    }

    public function addCrumb($label, $path = null): static {
        $this->crumbs[] = [
            'label' => $label,
            'url'   => $path !== null ? $this->getUrlBuilder()->getUrl($path) : null
        ];

        return $this;
    }

    public function addCrumbs(array $crumbs): static {
        foreach ($crumbs as $c) {
            $this->crumbs[] = $c;
        }

        return $this;
    }

    public function getCrumbs() {
        return $this->crumbs;
    }

    public function resetCrumbs(): static {
        return $this->setCrumbs([]);
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }
}
