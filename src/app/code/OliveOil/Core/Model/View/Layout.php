<?php

namespace OliveOil\Core\Model\View;


use OliveOil\Core\Block\BlockInterface;

class Layout implements LayoutInterface
{
    public const BLOCK_HIVE_KEY                  = 'BLOCK';
    public const BLOCK_CONFIG_HIVE_KEY           = 'BLOCK_CONFIG';
    public const BLOCK_HIERARCHY_HIVE_KEY        = 'BLOCK_HIERARCHY';
    public const BLOCK_PLACEHOLDER_KEY_PREFIX    = 'BLOCK_PLACEHOLDER.';
    public const PAGE_BLOCK_NAME                 = 'page';
    public const CONTENT_WRAPPER_BLOCK_NAME      = 'content_wrapper';

    /** @var BlockInterface[]|null[] */
    protected ?array $blocks;

    protected ?array $blockConfig;

    protected ?array $blockHierarchy;

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Block\Factory $blockFactory,
        protected array $params = []
    ) {
        $this->blocks =& $this->fw->ref(self::BLOCK_HIVE_KEY, true);
        if (! is_array($this->blocks)) {
            $this->blocks = [];
        }

        $this->blockConfig =& $this->fw->ref(self::BLOCK_CONFIG_HIVE_KEY, true);
        if (! is_array($this->blockConfig)) {
            $this->blockConfig = [];
        }

        $this->blockHierarchy =& $this->fw->ref(self::BLOCK_HIERARCHY_HIVE_KEY, true);
        if (! is_array($this->blockHierarchy)) {
            $this->blockHierarchy = [];
        }
    }

    /**
     * @inheritDoc
     */
    public function getAllBlockConfig() {
        return $this->blockConfig;
    }

    /**
     * @inheritDoc
     */
    public function getBlockConfig($blockName, $configKey = null) {
        if ($configKey === null) {
            return $this->blockConfig[$blockName] ?? [];
        }

        return $this->blockConfig[$blockName][$configKey] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function setBlockConfig($blockName, $configKey, $value = null): static {
        if ($value === null) {
            if (! is_array($configKey)) {
                throw new \InvalidArgumentException('Array expected for $configKey.');
            }

            foreach ($configKey as $k => $v) {
                $this->blockConfig[$blockName][$k] = $v;
            }
        }
        else {
            $this->blockConfig[$blockName][$configKey] = $value;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function clear(): static {
        $this->blocks = [];
        $this->blockConfig = [];
        $this->blockHierarchy = [];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addChild($parentName, $child): static {
        if ($child instanceof BlockInterface) {
            $child = $child->getName();
        }

        if (! isset($this->blockHierarchy[$parentName])) {
            $this->blockHierarchy[$parentName] = [];
        }

        if (! in_array($child, $this->blockHierarchy[$parentName])) {
            // TODO Handle children order
            $this->blockHierarchy[$parentName][] = $child;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function removeBlock($blockName, $parentName = null): static {
        if ($parentName) {
            $this->blockHierarchy[$parentName][$blockName] = null;
        }
        else {
            $this->blockHierarchy[$parentName][$blockName] = null;
            $this->blocks[$blockName] = null;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function newBlock($type) {
        return $this->getBlockFactory()->make($type);
    }

    /**
     * @inheritDoc
     */
    public function getBlock($blockName): ?\OliveOil\Core\Block\BlockInterface {
        if (array_key_exists($blockName, $this->blocks) && !$this->blocks[$blockName] instanceof \OliveOil\Core\Block\BlockInterface) {
            // Removed block
            return null;
        }

        $blockType = $this->getBlockConfig($blockName, 'type');
        if (! $blockType) {
            $blockType = \OliveOil\Core\Block\Template::class;
        }

        $newBlock = $this->newBlock($blockType);
        if (isset($this->blocks[$blockName])) {
            $block = $this->blocks[$blockName];
            if ($block::class === $newBlock::class) {
                return $block;
            }
        }

        if (!isset($this->blockConfig[$blockName])) {
            $this->blockConfig[$blockName] = [];
        }

        $newBlock->init($blockName, $this->blockConfig[$blockName]);
        $this->blocks[$blockName] = $newBlock;

        return $this->blocks[$blockName];
    }

    /**
     * @inheritDoc
     * @return mixed[]
     */
    public function getBlockChildren($blockName): array {
        $children = [];
        foreach ($this->blockHierarchy[$blockName] ?? [] as $childName) {
            if (($child = $this->getBlock($childName)) instanceof \OliveOil\Core\Block\BlockInterface) {
                $children[] = $child;
            }
        }

        return $children;
    }

    /**
     * @inheritDoc
     */
    public function getBlockTemplate($blockName) {
        return $this->getBlockConfig($blockName, 'template');
    }

    /**
     * @inheritDoc
     */
    public function setBlockTemplate($blockName, $template): static {
        $this->setBlockConfig($blockName, 'template', $template);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPageBlock(): ?\OliveOil\Core\Block\BlockInterface {
        return $this->getBlock(self::PAGE_BLOCK_NAME);
    }

    /**
     * @inheritDoc
     */
    public function getPageTemplate() {
        return $this->getBlockConfig(self::PAGE_BLOCK_NAME, 'template');
    }

    /**
     * @inheritDoc
     */
    public function setPageTemplate($template): static {
        return $this->setBlockConfig(self::PAGE_BLOCK_NAME, 'template', $template);
    }

    /**
     * @inheritDoc
     */
    public function setPageConfig($configKey, $value = null): static {
        return $this->setBlockConfig(self::PAGE_BLOCK_NAME, $configKey, $value);
    }

    /**
     * @inheritDoc
     */
    public function getContentWrapperBlock(): ?\OliveOil\Core\Block\BlockInterface {
        return $this->getBlock(self::CONTENT_WRAPPER_BLOCK_NAME);
    }

    /**
     * @inheritDoc
     */
    public function getContentWrapperTemplate() {
        return $this->getBlockConfig(self::CONTENT_WRAPPER_BLOCK_NAME, 'template');
    }

    /**
     * @inheritDoc
     */
    public function setContentWrapperTemplate($template): static {
        return $this->setBlockConfig(self::CONTENT_WRAPPER_BLOCK_NAME, 'template', $template);
    }

    /**
     * @inheritDoc
     */
    public function setContentWrapperConfig($configKey, $value = null): static {
        return $this->setBlockConfig(self::CONTENT_WRAPPER_BLOCK_NAME, $configKey, $value);
    }

    /**
     * @inheritDoc
     */
    public function setBlockCachePlaceholder($blockName, $callable = null, $params = []): static {
        $this->getFw()->set(self::BLOCK_PLACEHOLDER_KEY_PREFIX . $blockName, [$callable, $params]);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBlockCachePlaceholder($blockName) {
        return $this->getFw()->get(self::BLOCK_PLACEHOLDER_KEY_PREFIX . $blockName);
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getBlockFactory(): \OliveOil\Core\Block\Factory {
        return $this->blockFactory;
    }
}
