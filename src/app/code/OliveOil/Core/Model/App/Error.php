<?php


namespace OliveOil\Core\Model\App;


use Narrowspark\HttpStatus\HttpStatus;
use function OliveOil\array_mask;

/**
 * Class Error
 */
class Error
{
    public const USER_DATA = [
        'id',
        'code',
        'httpCode',
        'level',
        'userMessage',
        'userDetail',
        'userTrace',
    ];

    protected string $id;

    /** @var string */
    protected $requestId;

    /** @var int|string|null */
    protected $code;

    /** @var int */
    protected $httpCode = HttpStatus::STATUS_INTERNAL_SERVER_ERROR;

    /** @var string|null */
    protected $type;

    /** @var string */
    protected $message;

    /** @var int */
    protected $level;

    /** @var string */
    protected $detail;

    /** @var string|null */
    protected $trace;

    /** @var string */
    protected $userMessage;

    /** @var string */
    protected $userDetail;

    /** @var string */
    protected $userTrace;

    /** @var bool */
    protected $isHttp;

    /** @var \Throwable|array|null */
    protected $sourceError;

    public function __construct() {
        $this->id = uniqid('ERR#');
    }

    public function __sleep() {
        // Exceptions can cause serialization failures (if they contains Closure for example)
        return array_keys(\OliveOil\array_discard(
            get_object_vars($this),
            ['sourceError']
        ));
    }

    public function getId(): string {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId(string $id): static {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestId() {
        return $this->requestId;
    }

    /**
     * @return $this
     */
    public function setRequestId(string $requestId): static {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * @return int|string|null
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @param int|string|null $code
     * @return $this
     */
    public function setCode($code): static {
        $this->code = $code;

        return $this;
    }

    /**
     * @return int
     */
    public function getHttpCode() {
        return $this->httpCode;
    }

    /**
     * @return $this
     */
    public function setHttpCode(int $httpCode): static {
        $this->httpCode = $httpCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return $this
     */
    public function setType($type): static {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message): static {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel() {
        return $this->level;
    }

    /**
     * @param int $level
     * @return $this
     */
    public function setLevel($level): static {
        $this->level = $level;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetail() {
        return $this->detail;
    }

    /**
     * @param string $detail
     * @return $this
     */
    public function setDetail($detail): static {
        $this->detail = $detail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrace() {
        return $this->trace;
    }

    /**
     * @param string|null $trace
     * @return $this
     */
    public function setTrace($trace): static {
        $this->trace = $trace;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserMessage() {
        return $this->userMessage;
    }

    /**
     * @param string $userMessage
     * @return $this
     */
    public function setUserMessage($userMessage): static {
        $this->userMessage = $userMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserDetail() {
        return $this->userDetail;
    }

    /**
     * @param string $userDetail
     * @return $this
     */
    public function setUserDetail($userDetail): static {
        $this->userDetail = $userDetail;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserTrace() {
        return $this->userTrace;
    }

    /**
     * @param string $userTrace
     * @return $this
     */
    public function setUserTrace($userTrace): static {
        $this->userTrace = $userTrace;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHttp() {
        return $this->isHttp;
    }

    /**
     * @param bool $isHttp
     * @return $this
     */
    public function setIsHttp($isHttp): static {
        $this->isHttp = $isHttp;

        return $this;
    }

    /**
     * @return array|\Throwable|null
     */
    public function getSourceError() {
        return $this->sourceError;
    }

    /**
     * @param array|\Throwable $sourceError
     * @return $this
     */
    public function setSourceError($sourceError): static {
        $this->sourceError = $sourceError;

        return $this;
    }

    /**
     * @param bool $userDataOnly TRUE to only return user-visible data
     */
    public function toArray($userDataOnly = true): array {
        $properties = get_object_vars($this);
        if ($userDataOnly) {
            $properties = array_mask($properties, static::USER_DATA);
        }

        return $properties;
    }
}
