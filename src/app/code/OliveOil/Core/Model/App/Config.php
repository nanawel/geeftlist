<?php

namespace OliveOil\Core\Model\App;


class Config implements ConfigInterface
{
    /** @var string */
    public const CONFIG_VALUE_SEPARATOR = ';';

    /** @var string */
    public const CONFIG_KEY_VALUE_SEPARATOR = ':';

    public function __construct(
        protected \Base $fw,
        protected string $f3KeyNamespace = 'app',
        protected string $valueSeparator = self::CONFIG_VALUE_SEPARATOR,
        protected string $keyValueSeparator = self::CONFIG_KEY_VALUE_SEPARATOR,
        bool $useEnv = true
    ) {
        if ($useEnv) {
            $this->importValuesFromEnv();
        }
    }

    public function setValue(string $key, $value): static {
        if (!is_scalar($value) && !is_array($value)) {
            throw new \InvalidArgumentException('Only scalar and array values are supported.');
        }

        $this->fw->set($this->getAbsoluteKey($key), $value);

        return $this;
    }

    public function getValue(string $key, $default = null): mixed {
        $key = $this->getAbsoluteKey($key);

        if ($this->fw->exists($key)) {
            $value = $this->fw->get($key);
        }

        return $value ?? $default;
    }

    public function getAllValues(): array {
        return $this->fw->get($this->f3KeyNamespace);
    }

    public function hasValue(string $key): bool {
        return $this->fw->exists($this->getAbsoluteKey($key));
    }

    public function deleteValue(string $key): static {
        $this->fw->clear($this->getAbsoluteKey($key));

        return $this;
    }

    protected function getAbsoluteKey(string $key): string {
        return sprintf('%s.%s', $this->f3KeyNamespace, $key);
    }

    protected function importValuesFromEnv(): static {
        $keyPrefix = $this->getEnvKeyPrefix();
        $keyPrefixLength = strlen($keyPrefix);

        foreach (getenv() as $k => $v) {
            if (substr($k, 0, $keyPrefixLength) === $keyPrefix) {
                $this->setValue($this->envKeyToConfigKey($k), $v);
            }
        }

        return $this;
    }

    protected function getEnvKeyPrefix(): string {
        return $this->f3KeyNamespace . '__';
    }

    protected function envKeyToConfigKey(string $key): string {
        return str_replace('__', '.', substr($key, strlen($this->getEnvKeyPrefix())));
    }
}
