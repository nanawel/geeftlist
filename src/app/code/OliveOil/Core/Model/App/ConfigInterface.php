<?php

namespace OliveOil\Core\Model\App;


interface ConfigInterface
{
    public function setValue(string $key, mixed $value): static;

    public function getValue(string $key, mixed $default = null): mixed;

    public function hasValue(string $key): bool;

    public function deleteValue(string $key): static;

    public function getAllValues(): array;
}
