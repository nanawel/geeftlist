<?php

namespace OliveOil\Core\Model\App;


class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $config,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        protected \OliveOil\Core\Service\App\ErrorHandlerInterface $errorHandler,
        protected \OliveOil\Core\Model\I18n $i18n,
        /** @var \OliveOil\Core\Service\EventInterface[] */
        protected array $eventServices
    ) {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->config;
    }

    public function getSessionManager(): \OliveOil\Core\Service\Session\Manager {
        return $this->sessionManager;
    }

    public function getErrorHandler(): \OliveOil\Core\Service\App\ErrorHandlerInterface {
        return $this->errorHandler;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    /**
     * @return \OliveOil\Core\Service\EventInterface[]
     */
    public function getEventServices(): array {
        return $this->eventServices;
    }
}
