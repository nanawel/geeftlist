<?php

namespace OliveOil\Core\Model\App;


interface FlagInterface
{
    public function get(string $path, mixed $default = null): mixed;

    public function set(string $path, mixed $value = null): static;

    public function del(string $path): static;

    public function has(string $path): bool;

    public function getAll(?string $prefix = null): array;
}
