<?php

namespace OliveOil\Core\Model;


use OliveOil\Core\Exception\DebugException;
use OliveOil\Core\Exception\InvalidTypeException;
use OliveOil\Core\Model\Repository\ModelCache;
use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Model\ResourceModel\Iface\Model;
use function OliveOil\snakecase2camelcase;

/**
 * @template T of \OliveOil\Core\Model\AbstractModel
 * @implements \OliveOil\Core\Model\RepositoryInterface<T>
 */
class Repository implements RepositoryInterface
{
    /** Full Qualified Class Name */
    public const ENTITY_CLASS_TYPE_FQCN = 'fqcn';

    /** Relative Class Name */
    public const ENTITY_CLASS_TYPE_RCN  = 'rcn';

    /** Class Name Code */
    public const ENTITY_CLASS_TYPE_CODE = 'code';

    public const ENTITY_CLASS_TYPES = [
        self::ENTITY_CLASS_TYPE_FQCN,
        self::ENTITY_CLASS_TYPE_RCN,
        self::ENTITY_CLASS_TYPE_CODE
    ];

    /** @var \OliveOil\Core\Model\Repository\ModelCache<T> */
    protected $modelCache;

    /** @var string */
    protected $entityCollectionClass;

    /** @var \OliveOil\Core\Model\ResourceModel\Iface\Model<T> */
    protected $resourceModel;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var class-string */
    protected $entityFqcn;

    /**
     * @param class-string|string $entityClass
     * @param bool $useModelCache
     */
    public function __construct(
        protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory,
        protected \OliveOil\Core\Model\FactoryInterface $resourceModelFactory,
        \OliveOil\Core\Service\Log $logService,
        /** @var class-string|string */
        protected $entityClass,
        protected string $entityClassType = self::ENTITY_CLASS_TYPE_FQCN,
        $entityCollectionClass = null,
        $modelCacheClass = \OliveOil\Core\Model\Repository\ModelCache::class,
        protected $useModelCache = true
    ) {
        $this->logger = $logService->getLoggerForClass($this);
        $this->modelCache = $this->modelFactory->make($modelCacheClass);
        $this->entityCollectionClass = $entityCollectionClass ?? $this->entityClass;

        if (! in_array($this->entityClassType, static::ENTITY_CLASS_TYPES)) {
            throw new \InvalidArgumentException('Invalid entity class type: ' . $this->entityClassType);
        }

        $this->entityFqcn = $this->modelFactory->resolve($this->entityClass);
    }

    /**
     * @inheritDoc
     */
    public function get($id, $field = null, $options = []) {
        if (!$id) {
            throw new \InvalidArgumentException('ID cannot be NULL.');
        }

        if (
            $this->useModelCache
            && (!isset($options['use_cache']) || $options['use_cache'] === true)
            && ($model = $this->modelCache->get($id, $field))
        ) {
            return $model;
        }

        /** @var T $model */
        $model = $this->newModel();

        // NOTICE: Flags are ignored if instance is returned from cache
        if ($options['flags'] ?? false) {
            foreach ($options['flags'] as $flag => $value) {
                $model->setFlag($flag, $value);
            }
        }

        $loadMethod = $this->getLoadMethod($model, $id, $field);
        $loadMethod($model, $id, $field);

        if ($model->getId()) {
            if ($this->useModelCache) {
                $this->modelCache->set($model, $id, $field);
            }

            return $model;
        }

        return null;
    }

    /**
     * @param T $model
     * @param int $id
     * @param string|null $field
     * @return callable
     */
    protected function getLoadMethod(AbstractModel $model, $id, $field = null) {
        // Use dedicated loadBy*() method if available
        if ($field !== null && strlen($field)) {
            $method = sprintf('loadBy%s', snakecase2camelcase($field));

            if (method_exists($this->getResourceModel(), $method)) {
                return fn() => $this->getResourceModel()->$method($model, $id);
            }
        }

        // Fallback: use default load() method
        return fn(\OliveOil\Core\Model\AbstractModel $model, $id, $field) => $this->loadDefault($model, $id, $field);
    }

    protected function loadDefault(AbstractModel $model, $id, $field) {
        return $this->getResourceModel()->load($model, $id, $field);
    }

    /**
     * @inheritDoc
     */
    public function find($criteria = null) {
        $collection = $this->newCollection();

        if ($criteria !== null) {
            if (is_array($criteria)) {
                $this->applyCriteriaArray($collection, $criteria);
            }
            else {
                // TODO: Implement find() method with complex criteria.
                throw new DebugException('Not implemented');
            }
        }

        try {
            return $collection->getItems();
        } catch (\Throwable $throwable) {
            $this->logger->error(
                sprintf(
                    "Could not load collection's items using these criteria: %s",
                    json_encode($criteria)
                ),
                ['exception' => $throwable]
            );

            throw $throwable;
        }
    }

    /**
     * @inheritDoc
     */
    public function findOne($criteria = null) {
        $collection = $this->newCollection();

        if ($criteria !== null) {
            if (is_array($criteria)) {
                $this->applyCriteriaArray($collection, $criteria);
            }
            else {
                // TODO: Implement find() method with complex criteria.
                throw new DebugException('Not implemented');
            }
        }

        $collection->setLimit(1);

        return $collection->getFirstItem();
    }

    /**
     * @inheritDoc
     */
    public function findAllIds($criteria = null): array {
        return array_keys($this->find($criteria));
    }

    protected function applyCriteriaArray(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        array $criteria
    ) {
        foreach ($criteria as $section => $sectionData) {
            switch ($section) {
                case 'filter':
                    if (!is_array($sectionData)) {
                        throw new \InvalidArgumentException('Filters must be an array.');
                    }

                    foreach ($sectionData as $field => $constraints) {
                        $field = $this->resolveField($field);
                        /*if (!str_contains($field, '.') && !empty($criteria['join'])) {
                            $this->logger->warning(sprintf(
                                "Field '%s' for collection of type %s is not fully qualified and can be ambiguous."
                                . " Criteria = %s",
                                $field,
                                get_class($collection),
                                json_encode($criteria)
                            ));
                        }*/
                        foreach ($constraints as $constraint) {
                            foreach ($constraint as $operator => $value) {
                                $collection->addFieldToFilter($field, [$operator => $value]);
                            }
                        }
                    }

                    break;

                case 'join':
                    if (!is_array($sectionData)) {
                        throw new \InvalidArgumentException('Join must be an array.');
                    }

                    foreach ($sectionData as $relationName => $joinParams) {
                        if (is_numeric($relationName)) {
                            throw new \InvalidArgumentException('Join name must be non-numerical.');
                        }

                        $collection->addJoinRelation($relationName, $joinParams ?? []);
                    }

                    break;

                case 'sort_order':
                    if (!is_array($sectionData)) {
                        throw new \InvalidArgumentException('Sort orders must be an array.');
                    }

                    foreach ($sectionData as $field => $dir) {
                        $field = $this->resolveField($field);
                        $collection->orderBy($field, $dir);
                    }

                    break;

                case 'limit':
                    if (!is_numeric($sectionData) && !is_array($sectionData)) {
                        throw new \InvalidArgumentException('Limit must be an integer or an array.');
                    }

                    [$limit, $offset] = is_array($sectionData) ? $sectionData : [$sectionData, 0];

                    $collection->setLimit((int) $limit)
                        ->setOffset((int) $offset);
                    break;

                case 'page':
                    if (!is_numeric($sectionData) && !is_array($sectionData)) {
                        throw new \InvalidArgumentException('Page must be an integer or an array.');
                    }

                    if (is_numeric($sectionData)) {
                        $sectionData = ['number' => $sectionData];
                    }

                    $number = (int) ($sectionData['number'] ?? 1);
                    $size = (int) ($sectionData['size'] ?? self::COLLECTION_DEFAULT_PAGE_SIZE);
                    $collection->setLimit($size)
                        ->setOffset(($number - 1) * $size);
                    break;

                case 'flags':
                    if (!is_array($sectionData)) {
                        throw new \InvalidArgumentException('Flags must be an array.');
                    }

                    foreach ($sectionData as $flag => $value) {
                        $collection->setFlag($flag, $value);
                    }

                    break;

                default:
                    throw new \InvalidArgumentException('Invalid criteria type: ' . $section);
            }
        }
    }

    protected function resolveField($field) {
        if ($field == static::WILDCARD_ID_FIELD) {
            return sprintf('main_table.%s', $this->getResourceModel()->getIdField());
        }

        return $field;
    }

    /**
     * @inheritDoc
     */
    public function save(\OliveOil\Core\Model\AbstractModel $model): static {
        $this->assertIsValidModel($model);
        // Make sure repository cache item is updated first thing after save to make it available to other observers
        // using the same repository
        if (!$model->hasInstanceListener('update_repository_cache')) {
            $model->attachInstanceListener('save::after_commit', function () use ($model): void {
                $this->modelCache->set($model, $model->getId());
            }, 999999, 'update_repository_cache');
        }

        $this->getResourceModel()->save($model);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function delete(\OliveOil\Core\Model\AbstractModel $model): static {
        $this->assertIsValidModel($model);
        if ($id = $model->getId()) {
            $this->getResourceModel()->delete($model);
            $this->modelCache->delete($id);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function deleteAll($criteria = null): static {
        $collection = $this->newCollection();

        if ($criteria !== null) {
            if (is_array($criteria)) {
                $this->applyCriteriaArray($collection, $criteria);
            }
            else {
                // TODO: Implement find() method with complex criteria.
                throw new DebugException('Not implemented');
            }
        }

        $collection->delete();
        $this->clearCache();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id): never {
        // TODO: Implement deleteById() method.
        throw new DebugException('Not implemented');
    }

    /**
     * @inheritDoc
     */
    public function validate(\OliveOil\Core\Model\AbstractModel $model) {
        $this->assertIsValidModel($model);

        return $model->validate();
    }

    /**
     * @inheritDoc
     */
    public function newModel(...$args) {
        $model = match ($this->entityClassType) {
            self::ENTITY_CLASS_TYPE_CODE => $this->modelFactory->makeFromCode($this->entityClass, ...$args),
            self::ENTITY_CLASS_TYPE_RCN => $this->modelFactory->resolveMake($this->entityClass, ...$args),
            default => $this->modelFactory->make($this->entityClass, ...$args),
        };

        if ($this->useModelCache) {
            // Fallback behavior to clear cache if model is saved or deleted outside of this class
            $model->attachInstanceListener('save::after', fn(\OliveOil\Core\Model\AbstractModel $model) => $this->onModelSave($model))
                ->attachInstanceListener('delete::after', fn(\OliveOil\Core\Model\AbstractModel $model) => $this->onModelDelete($model));
        }

        return $model;
    }

    /**
     * @return Collection<T>
     */
    public function newCollection() {
        return match ($this->entityClassType) {
            self::ENTITY_CLASS_TYPE_CODE => $this->resourceModelFactory
                ->makeCollectionFromCode($this->entityCollectionClass),
            self::ENTITY_CLASS_TYPE_RCN => $this->resourceModelFactory
                ->resolveMakeCollection($this->entityCollectionClass),
            default => $this->resourceModelFactory
                ->make($this->entityCollectionClass),
        };
    }

    /**
     * @return Model<T>
     */
    public function getResourceModel() {
        if (! $this->resourceModel) {
            $resourceModel = match ($this->entityClassType) {
                self::ENTITY_CLASS_TYPE_CODE => $this->resourceModelFactory->getFromCode($this->entityClass),
                self::ENTITY_CLASS_TYPE_RCN => $this->resourceModelFactory->resolveGet($this->entityClass),
                default => $this->resourceModelFactory->get($this->entityClass),
            };
            $this->resourceModel = $resourceModel;
        }

        return $this->resourceModel;
    }

    /**
     * @param T $model
     */
    public function onModelSave($model): void {
        $this->modelCache->delete($model->getId());
    }

    /**
     * @param T $model
     */
    public function onModelDelete($model): void {
        $this->modelCache->delete($model->getId());
    }

    /**
     * @return $this
     */
    public function clearCache(): static {
        $this->modelCache->clearCache();

        return $this;
    }

    /**
     * @param object $model
     * @throws InvalidTypeException
     */
    public function assertIsValidModel($model): void {
        if (!is_a($model, $this->entityFqcn)) {
            throw new InvalidTypeException(sprintf(
                'Must be an instance of %s (found %s).',
                $this->entityFqcn,
                $model::class
            ));
        }
    }

    public static function clearAllModelCache(): void {
        ModelCache::clearAllModelCache();
    }
}
