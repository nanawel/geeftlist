<?php

namespace OliveOil\Core\Model\Traits;


/**
 * Trait ConfigurableTrait
 *
 * @see \OliveOil\Core\Iface\Configurable
 */
trait ConfigurableTrait
{
    /** @var array */
    protected $configuration = [];

    /**
     * @param array|string $config
     * @param mixed|null $value
     * @param bool $merge Merge values if $config is an array (first-level array values only), replace otherwise
     * @return $this
     */
    public function configure($config, $value = null, $merge = true) {
        if (is_array($config)) {
            if ($value !== null) {
                throw new \InvalidArgumentException('$config cannot be an array if $value is not null.');
            }

            $this->configuration = $merge
                ? array_merge($this->configuration, $config)
                : $config;
        }
        else {
            $this->configuration[$config] = $value;
        }

        if (is_callable([$this, 'onConfigurationChange'])) {
            $this->onConfigurationChange($this->configuration);
        }

        return $this;
    }

    /**
     * @param null|string $key
     * @param null|mixed $default
     * @return mixed|array
     */
    public function getConfiguration($key = null, $default = null) {
        if ($key === null) {
            return $this->configuration ?? [];
        }

        return $this->configuration[$key] ?? $default;
    }

    /**
     * @param null|string $key
     * @return $this
     */
    public function unsetConfiguration($key = null) {
        if ($key === null) {
            $this->configuration = [];
        }
        else {
            unset($this->configuration[$key]);
        }

        if (is_callable([$this, 'onConfigurationChange'])) {
            $this->onConfigurationChange($this->configuration);
        }

        return $this;
    }
}
