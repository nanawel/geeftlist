<?php

namespace OliveOil\Core\Model\Traits;


trait FlagTrait
{
    /** @var string[] */
    protected array $flags = [];

    public function setFlag(array|string $flag, mixed $flagValue = true): static {
        if (! is_array($flag)) {
            $flag = [$flag => $flagValue];
        }

        foreach ($flag as $f => $fv) {
            $this->flags[$f] = $fv;
        }

        return $this;
    }

    public function getFlag(string $flag): mixed {
        return $this->flags[$flag] ?? null;
    }

    /**
     * @param string $flag
     * @return $this
     */
    public function unsetFlag(string $flag): static {
        unset($this->flags[$flag]);

        return $this;
    }

    /**
     * @return $this
     */
    public function resetFlags(): static {
        $this->flags = [];

        return $this;
    }
}
