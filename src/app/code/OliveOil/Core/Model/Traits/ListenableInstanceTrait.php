<?php

namespace OliveOil\Core\Model\Traits;

/**
 * Class ListenableTrait
 */
trait ListenableInstanceTrait
{
    /** @var bool */
    protected $instanceListenersEnabled = true;

    /**
     * [
     *     eventName => [
     *         priority => [
     *             listener1,
     *             listener2,
     *             ...
     *         ]
     *     ]
     * ]
     *
     * @var array
     */
    protected $instanceListeners = [];

    /**
     * @var array
     */
    protected $instanceListenersByName = [];

    /**
     * Trigger an instance event.
     *
     * @param string $eventName
     * @param object $target
     * @param array $argv
     * @return $this
     */
    public function triggerInstanceEvent($eventName, $target = null, $argv = []) {
        if ($this->instanceListenersEnabled) {
            $listenersByPriority = $this->instanceListeners[$eventName] ?? [];

            ksort($listenersByPriority);
            foreach ($listenersByPriority as $registeredListeners) {
                foreach ($registeredListeners as $registeredListener) {
                    call_user_func($registeredListener, $target, $argv);
                }
            }
        }

        return $this;
    }

    /**
     * Attach an instance listener to this instance.
     *
     * @param string $eventName
     * @param int|null $priority
     * @param string $name
     * @return $this
     */
    public function attachInstanceListener($eventName, callable $listener, $priority = null, $name = null) {
        if ($priority === null) {
            // Find next available priority
            $priority = call_user_func(
                'max',
                array_keys($this->instanceListeners[$eventName] ?? [0 => null])
            ) + 1;
        }
        else {
            $priority = (int) $priority;
        }

        if ($name === null) {
            $name = uniqid('ANONYMOUS_');
        }

        if (array_key_exists($name, $this->instanceListenersByName)) {
            throw new \InvalidArgumentException('A listener already exists with this name.');
        }

        $this->instanceListenersByName[$name] = [
            'eventName' => $eventName,
            'callable'  => $listener,
            'priority'  => $priority
        ];
        if (!array_key_exists($eventName, $this->instanceListeners)) {
            $this->instanceListeners[$eventName] = [];
        }

        if (!array_key_exists($priority, $this->instanceListeners[$eventName])) {
            $this->instanceListeners[$eventName][$priority] = [];
        }

        $this->instanceListeners[$eventName][$priority][] = $listener;

        return $this;
    }

    /**
     * Detach an instance listener from this instance.
     *
     * @param string $eventName
     * @return $this
     */
    public function detachInstanceListener(callable $listener, $eventName = null) {
        foreach ($this->instanceListeners as $evName => $listenersByPriority) {
            if ($eventName !== null && $evName != $eventName) {
                continue;
            }

            foreach ($listenersByPriority as $priority => $registeredListeners) {
                foreach ($registeredListeners as $i => $registeredListener) {
                    if ($registeredListener == $listener) {
                        unset($this->instanceListeners[$eventName][$priority][$i]);
                    }
                }
            }
        }

        foreach ($this->instanceListenersByName as $listenerName => $listenerInfo) {
            if ($listenerInfo['callable'] == $listener) {
                unset($this->instanceListenersByName[$listenerName]);
                break;
            }
        }

        return $this;
    }

    public function detachInstanceListenerByName($listenerName) {
        if (!isset($this->instanceListenersByName[$listenerName])) {
            throw new \InvalidArgumentException('Unknown listener.');
        }

        return $this->detachInstanceListener(
            $this->instanceListenersByName[$listenerName]['callable'],
            $this->instanceListenersByName[$listenerName]['eventName']
        );
    }

    /**
     * Clear all instance listeners attached to this instance.
     *
     * @param string $eventName
     * @return $this
     */
    public function clearInstanceListeners($eventName) {
        $eventNames = $eventName == '*' ? array_keys($this->instanceListeners) : [$eventName];
        foreach ($eventNames as $en) {
            unset($this->instanceListeners[$en]);
        }

        return $this;
    }

    /**
     * @param string|null $eventName
     * @return array
     */
    public function getInstanceListeners($eventName = null) {
        if ($eventName === null) {
            return $this->instanceListeners;
        }

        return $this->instanceListeners[$eventName] ?? [];
    }

    /**
     * @param string|null $listenerName
     * @return callable|null
     */
    public function getInstanceListener($listenerName) {
        return $this->instanceListenersByName[$listenerName] ?? null;
    }

    /**
     * @param string|null $listenerName
     */
    public function hasInstanceListener($listenerName): bool {
        return isset($this->instanceListenersByName[$listenerName]);
    }

    public function enableInstanceListeners($enable = true) {
        $this->instanceListenersEnabled = (bool) $enable;

        return $this;
    }
}
