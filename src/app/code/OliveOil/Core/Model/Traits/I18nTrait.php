<?php

namespace OliveOil\Core\Model\Traits;


trait I18nTrait
{
    public function __($string, $vars = null) {
        return call_user_func_array(
            [$this->getI18n(), 'tr'],
            is_array($string) ? $string : func_get_args()
        );
    }
}
