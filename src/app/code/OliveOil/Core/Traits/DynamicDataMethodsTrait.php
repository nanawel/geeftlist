<?php

namespace OliveOil\Core\Traits;

trait DynamicDataMethodsTrait
{
    /** @var array */
    protected $_data = [];

    /** @var array */
    protected $_origData = [];

    /** @var bool */
    protected $_hasDataChanges = false;

    /**
     * Add data to the object.
     *
     * Retains previous data in the object.
     *
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function addData(array $arr) {
        foreach ($arr as $index => $value) {
            $this->setData($index, $value);
        }

        return $this;
    }

    /**
     * Overwrite data in the object.
     *
     * $key can be string or array.
     * If $key is a string, the attribute value will be overwritten by $value
     * If $key is an array, it will overwrite all the data in the object.
     *
     * @param string|array $key
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function setData($key, mixed $value = null) {
        $this->_hasDataChanges = true;
        if (is_array($key)) {
            $this->_data = $key;
        }
        else {
            $this->_data[$key] = $value;
        }

        return $this;
    }

    /**
     * @param string|array $key
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function setOrigData($key, mixed $value = null) {
        if (is_array($key)) {
            $this->_origData = $key;
        }
        else {
            $this->_origData[$key] = $value;
        }

        return $this;
    }

    /**
     * Unset data from the object.
     *
     * $key can be a string only. Array will be ignored.
     *
     * @param string $key
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function unsetData($key = null) {
        $this->_hasDataChanges = true;
        if (is_null($key)) {
            $this->_data = [];
        }
        else {
            unset($this->_data[$key]);
        }

        return $this;
    }

    /**
     * If $key is empty, checks whether there's any data in the object
     * Otherwise checks if the specified attribute is set.
     *
     * @param string $key
     */
    public function hasData($key = ''): bool {
        if (empty($key) || ! is_string($key)) {
            return ! empty($this->_data);
        }

        return array_key_exists($key, $this->_data);
    }

    /**
     * If $key is empty, checks whether there's any origData in the object
     * Otherwise checks if the specified attribute is set.
     *
     * @param string $key
     */
    public function hasOrigData($key = ''): bool {
        if (empty($key) || ! is_string($key)) {
            return ! empty($this->_origData);
        }

        return array_key_exists($key, $this->_origData);
    }

    /**
     * Retrieves data from the object
     *
     * If $key is empty will return all the data as an array
     * Otherwise it will return value of the attribute specified by $key
     *
     * If $index is specified it will assume that attribute data is an array
     * and retrieve corresponding member.
     *
     * @param string $key
     * @return mixed
     */
    public function getData($key = '') {
        if ($key === '') {
            return $this->_data;
        }

        return $this->_data[$key] ?? null;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getOrigData($key = '') {
        if ($key === '') {
            return $this->_origData;
        }

        return $this->_origData[$key] ?? null;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getDataUsingMethod($key) {
        if (method_exists($this, $method = 'get' . self::camelize($key))) {
            return call_user_func([$this, $method]);
        }

        return $this->getData($key);
    }

    /**
     * @return boolean
     */
    public function hasDataChanges() {
        return $this->_hasDataChanges;
    }

    /**
     * @param bool $hasDataChanges
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function setDataChanges($hasDataChanges) {
        $this->_hasDataChanges = (bool)$hasDataChanges;
        return $this;
    }

    /**
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function resetData() {
        return $this->setData([]);
    }

    /**
     * @return-type-ommitted Conflicts with declaration in \OliveOil\Core\Iface\DynamicDataMethodsAware
     */
    public function resetOrigData() {
        return $this->setOrigData([]);
    }

    /**
     * Set/Get attribute wrapper
     *
     * @param string $method
     * @return mixed
     */
    public function __call($method, array $args) {
        switch (substr($method, 0, 3)) {
            case 'get':
                $key = self::underscore(substr($method, 3));

                return $this->getData($key);

            case 'set':
                $key = self::underscore(substr($method, 3));

                return $this->setData($key, $args[0] ?? null);

            case 'uns':
                $key = self::underscore(substr($method, 3));

                return $this->unsetData($key);

            case 'has':
                $key = self::underscore(substr($method, 3));

                return isset($this->_data[$key]);
        }

        throw new \Exception(sprintf("Invalid method %s::%s()", $this::class, $method));
    }

    /**
     * Converts field names for setters and getters
     *
     * $this->setMyField($value) === $this->setData('my_field', $value)
     * Uses cache to eliminate unneccessary preg_replace
     *
     * @param string $name
     */
    protected static function underscore($name): string {
        return \OliveOil\camelcase2snakecase($name);
    }

    /**
     * Converts method names for setters and getters
     *
     * $this->getDataUsingMethod('my_field', $value) === $this->getMyField($value)
     *
     * @param string $name
     */
    protected static function camelize($name): string {
        return \OliveOil\snakecase2camelcase($name);
    }
}
