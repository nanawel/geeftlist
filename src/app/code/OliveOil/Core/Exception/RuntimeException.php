<?php

namespace OliveOil\Core\Exception;


class RuntimeException extends \RuntimeException
{
    use ExceptionTrait;
}
