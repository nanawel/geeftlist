<?php

namespace OliveOil\Core\Exception;


abstract class AbstractException extends \Exception
{
    use ExceptionTrait;
}
