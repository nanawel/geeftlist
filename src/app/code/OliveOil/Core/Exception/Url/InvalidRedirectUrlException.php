<?php

namespace OliveOil\Core\Exception\Url;


use OliveOil\Core\Exception\AppException;

class InvalidRedirectUrlException extends AppException
{
}
