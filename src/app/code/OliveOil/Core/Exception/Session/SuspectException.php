<?php

namespace OliveOil\Core\Exception\Session;


use OliveOil\Core\Exception\SessionException;

class SuspectException extends SessionException
{
}
