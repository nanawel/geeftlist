<?php

namespace OliveOil\Core\Exception\Session;


use OliveOil\Core\Exception\SessionException;

class ExpiredException extends SessionException
{
    public const IS_DEFAULT_VISIBLE = true;
}
