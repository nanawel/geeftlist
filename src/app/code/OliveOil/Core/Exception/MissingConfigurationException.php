<?php

namespace OliveOil\Core\Exception;


class MissingConfigurationException extends SystemException
{
}
