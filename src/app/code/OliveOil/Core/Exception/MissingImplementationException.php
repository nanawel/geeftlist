<?php

namespace OliveOil\Core\Exception;


class MissingImplementationException extends \LogicException
{
}
