<?php

namespace OliveOil\Core\Exception\Filesystem;


use OliveOil\Core\Exception\FilesystemException;

class FileNotFoundException extends FilesystemException
{
}
