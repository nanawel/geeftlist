<?php

namespace OliveOil\Core\Exception\Security;


use OliveOil\Core\Exception\SecurityException;

class CsrfException extends SecurityException
{
}
