<?php

namespace OliveOil\Core\Exception;


class AppException extends AbstractException
{
    /**
     * @override
     */
    protected static function isDefaultVisible(): bool {
        return true;
    }
}
