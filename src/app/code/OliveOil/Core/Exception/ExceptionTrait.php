<?php

namespace OliveOil\Core\Exception;


trait ExceptionTrait
{
    /** @var bool */
    protected $isUserVisible;

    public function __construct($message, $code = 0, \Throwable $previous = null, $isUserVisible = null) {
        parent::__construct($message, $code, $previous);
        $this->isUserVisible($isUserVisible ?? static::isDefaultVisible());
    }

    /**
     * @return $this|bool
     */
    public function isUserVisible($isUserVisible = null) {
        if ($isUserVisible === null) {
            return $this->isUserVisible;
        }

        $this->isUserVisible = $isUserVisible;

        return $this;
    }

    protected static function isDefaultVisible(): bool {
        return false;
    }
}
