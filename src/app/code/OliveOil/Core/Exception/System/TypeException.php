<?php

namespace OliveOil\Core\Exception\System;


use OliveOil\Core\Exception\SystemException;

class TypeException extends SystemException
{
}
