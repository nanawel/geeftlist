<?php

namespace OliveOil\Core\Exception\Setup;


use OliveOil\Core\Exception\SetupException;

class InvalidSetupScriptException extends SetupException
{
}
