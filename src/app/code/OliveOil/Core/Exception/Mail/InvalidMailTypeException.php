<?php

namespace OliveOil\Core\Exception\Mail;


use OliveOil\Core\Exception\MailException;

class InvalidMailTypeException extends MailException
{
}
