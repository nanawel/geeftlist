<?php

namespace OliveOil\Core\Exception\Di;


use OliveOil\Core\Exception\RuntimeException;

class Exception extends RuntimeException
{
}
