<?php

namespace OliveOil\Core\Exception\Upload;


use OliveOil\Core\Exception\UploadException;

class MissingFileException extends UploadException
{
}
