<?php

namespace OliveOil\Core\Exception\Service;


use OliveOil\Core\Exception\SystemException;

class RegistrationException extends SystemException
{
}
