<?php

namespace OliveOil\Core\Exception;


class TemplateNotFoundException extends SystemException
{
}
