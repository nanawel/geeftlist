<?php

namespace OliveOil\Core;


use OliveOil\Core\Model\AppInterface;

class Bootstrap implements BootstrapInterface
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\AppInterface $app,
        protected \OliveOil\Core\Service\RouteInterface $routeService,
        protected \OliveOil\Core\Service\App\ErrorHandlerInterface $errorHandler
    ) {
    }

    protected function beforeRun(): bool {
        $this->app->bootstrap();
        $this->errorHandler->register();
        $this->routeService
            ->migrateRoutes()
            ->registerRestMapsToRoutes();
        $this->prepareServerEnvVariables();

        //echo '<pre>';var_dump($this->getFw()->hive());exit;

        return true;
    }

    /**
     * @return void
     */
    protected function afterRun(mixed &$return) {
        if ($app = $this->fw->get('OLIVEOIL_APP')) {
            /** @var AppInterface $app */
            $app->onShutdown();
        }

        $this->errorHandler->unregister();
    }

    protected function prepareServerEnvVariables(): static {
        foreach ($this->fw->get('SERVER') as $key => $value) {
            if (str_starts_with((string) $key, 'REDIRECT_')) {
                $this->fw->set('SERVER.' . substr((string) $key, 9), $value);
                $this->fw->clear('SERVER.' . $key);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        $return = null;
        if ($this->beforeRun()) {
            $return = $this->fw->run();
        }

        $this->afterRun($return);

        return $return;
    }
}
