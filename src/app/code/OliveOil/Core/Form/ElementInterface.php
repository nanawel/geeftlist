<?php

namespace OliveOil\Core\Form;

use OliveOil\Core\Iface\DynamicDataMethodsAware;

interface ElementInterface extends DynamicDataMethodsAware
{
    /**
     * @param string $class
     * @return $this
     */
    public function addClass($class);

    /**
     * @param string $class
     * @return $this
     */
    public function removeClass($class);

    /**
     * @return string
     */
    public function toHtml();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * Filter form data
     */
    public function filter(array &$formData);
}
