<?php

namespace OliveOil\Core\Form\Element;

/**
 * @method string|null getPlaceholder()
 */
class Textfield extends AbstractDomElement
{
    protected function init(): void {
        parent::init();
        $this->tagName = 'input';
    }

    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        parent::prepareDOMElement();
        $el = $this->getDOMElement();
        $el->setAttribute('type', 'text');
        $el->setAttribute('value', $this->escapeHtml($this->getValue()));
        $el->setAttribute('placeholder', $this->escapeHtml($this->getPlaceholder()));

        return $this;
    }
}
