<?php

namespace OliveOil\Core\Form\Element;


/**
 * @method string getTemplate()
 * @method $this setTemplate(string $template)
 * @method string getName()
 * @method $this setName(string $name)
 */
class TemplateElement extends AbstractElement
{
    protected \OliveOil\Core\Service\View\Template\Renderer\RendererInterface $templateRenderer;

    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        array $data = []
    ) {
        $this->templateRenderer = $context->getTemplateRenderer();
        parent::__construct($context, $data);
        $this->addClass('input');
    }

    public function render() {
        $hive = $this->getData();
        $hive['_VIEW'] = $this->view;
        $hive['_BLOCK'] = $this;

        if (! $template = $this->getTemplate()) {
            $this->logger->warning(sprintf("Missing template for form element '%s'", $this->getName()));

            return '';
        }

        return $this->templateRenderer->render($template, $hive);
    }

    protected function renderHtml() {
        return $this->render();
    }
}
