<?php

namespace OliveOil\Core\Form\Element;

/**
 * @method bool getChecked()
 */
class Radio extends AbstractDomElement
{
    protected function init(): void {
        parent::init();
        $this->tagName = 'input';
    }

    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        parent::prepareDOMElement();
        $el = $this->getDOMElement();
        $el->setAttribute('type', 'radio');
        $el->setAttribute('value', $this->escapeHtml($this->getValue()));
        if ($this->getChecked()) {
            $el->setAttribute('checked', 'checked');
        }

        return $this;
    }
}
