<?php

namespace OliveOil\Core\Form\Element;

use OliveOil\Core\Exception\InvalidValueException;

class Referer extends AbstractDomElement
{
    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->tagName = 'input';
        $this->setName('referer');
        $this->setParams([
            '_current' => true,
            '_force_scheme' => true
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        parent::prepareDOMElement();
        $el = $this->getDOMElement();
        $el->setAttribute('type', 'hidden');
        $el->setAttribute('value', $this->escapeHtml($this->getValue()));

        return $this;
    }

    public function getValue() {
        if (!$value = parent::getValue()) {
            $value = $this->urlBuilder->getUrl(null, $this->getParams() ?: []);
        }

        return $value;
    }

    /**
     * @param mixed|null $value
     * @return $this
     */
    public function setParam(mixed $key, $value): static {
        if (is_array($key)) {
            throw new InvalidValueException('$key must be a scalar.');
        }

        $this->_data['params'][$key] = $value;

        return $this;
    }
}
