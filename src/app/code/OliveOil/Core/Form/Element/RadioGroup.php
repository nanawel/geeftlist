<?php

namespace OliveOil\Core\Form\Element;

class RadioGroup extends AbstractDomElement
{
    protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory;

    public function __construct(
        \OliveOil\Core\Form\Element\Context $context,
        array $data = []
    ) {
        $this->modelFactory = $context->getModelFactory();
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->tagName = 'div';
        $this->addClass('radiogroup');
    }

    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        $this->addClass('input-' . $this->getName());
        parent::prepareDOMElement();
        $el = $this->getDOMElement();

        foreach ($this->getOptions() as $option) {
            $el->appendChild($this->prepareOption($option));

            if (! empty($option['description'])) {
                $descriptionEl = self::getDocument()->createElement('div');
                $descriptionEl->textContent = $this->escapeHtml($option['description']);
                $descriptionEl->setAttribute('class', 'description');
                $el->appendChild($descriptionEl);
            }
        }

        return $this;
    }

    /**
     * @return \DOMElement
     */
    protected function prepareOption(array $option) {
        $option['name'] = $this->getName();
        if (! isset($option['class'])) {
            $option['class'] = '';
        }

        if ($this->getValue() == $option['value']) {
            $option['checked'] = true;
        }

        $radioEl = $this->modelFactory->make(Radio::class, ['data' => $option]);

        $labelEl = self::getDocument()->createElement('label');
        $labelEl->setAttribute('class', 'option-' . $this->cssIdentifier($option['value']));
        $labelEl->appendChild($radioEl->getPreparedDOMElement());

        $labelWrapper = self::getDocument()->createElement('span');
        $labelText = self::getDocument()->createElement('span');
        $labelText->textContent = $this->escapeHtml($option['label']);
        $labelWrapper->appendChild($labelText);
        $labelEl->appendChild($labelWrapper);

        return $labelEl;
    }

    /**
     * @param string $string
     * @return string
     */
    protected function cssIdentifier($string): ?string {
        return preg_replace('/[^\w]/', '-', strtolower((string) $string));
    }
}
