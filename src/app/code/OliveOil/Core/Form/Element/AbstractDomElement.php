<?php

namespace OliveOil\Core\Form\Element;


/**
 * @method $this setName(string $name)
 * @method string getName()
 * @method $this setOptions(array $options)
 * @method array getOptions()
 */
abstract class AbstractDomElement extends AbstractElement
{
    /** @var \DOMDocument */
    protected static $document;

    /** @var string */
    protected $tagName;

    /** @var \DOMElement */
    protected $domElement;

    /**
     * @return \DOMElement
     */
    public function getDOMElement($reset = false) {
        if ($reset || ! $this->domElement) {
            $this->domElement = self::getDocument()->createElement($this->tagName);
            $this->isPrepared = false;
        }

        return $this->domElement;
    }

    protected function setCommonAttributes(): static {
        $attributes = ['id', 'name', 'class', 'disabled', 'required'];
        foreach ($attributes as $a) {
            if ($v = $this->getDataUsingMethod($a)) {
                $this->getDOMElement()->setAttribute($a, $v);
            }
        }

        return $this;
    }

    protected function addAttributesToDOMElement(\DOMElement $el, array $attributes): static {
        foreach ($attributes as $k => $v) {
            if ($k && !str_starts_with($k, '_')) {
                $el->setAttribute($k, $v);
            }
        }

        return $this;
    }

    protected function renderHtml() {
        return self::getDocument()
            ->saveXML($this->getPreparedDOMElement());
    }

    /**
     * @return \DOMElement
     */
    public function getPreparedDOMElement() {
        if (! $this->isPrepared) {
            $this->prepareDOMElement();
            $this->isPrepared = true;
        }

        return $this->getDOMElement();
    }

    /**
     * @return $this
     */
    protected function prepareDOMElement(): static {
        $this->setCommonAttributes();
        return $this;
    }

    protected static function getDocument() {
        if (! self::$document) {
            self::$document = new \DOMDocument();
        }

        return self::$document;
    }
}
