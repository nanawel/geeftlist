<?php

namespace OliveOil\Core\Form\Element;

/**
 * Class Select
 *
 * @method array|null getOptions()
 * @method $this setOptions(array $options)
 * @method $this setValue(mixed $value)
 * @method string getLabel()
 * @method $this setLabel(string $label)
 */
class Select extends AbstractDomElement
{
    public const SEPARATOR_LABEL = '-';

    public const SEPARATOR_LABEL_RENDERER = '──────────';

    protected function init(): void {
        parent::init();
        $this->tagName = 'select';
    }

    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        parent::prepareDOMElement();
        foreach ($this->getOptions() as $idx => $option) {
            $optEl = self::getDocument()->createElement('option');
            foreach ($option as $n => $a) {
                if ($n === 'label') {
                    if ($a == self::SEPARATOR_LABEL) {
                        $optEl->nodeValue = self::SEPARATOR_LABEL_RENDERER;
                        $optEl->setAttribute('disabled', 'disabled');
                    }
                    else {
                        $optEl->nodeValue = $this->escapeHtml($a);
                    }
                }
                else {
                    $optEl->setAttribute($n, $this->escapeHtml($a));
                }
            }

            $optEl->setAttribute(
                'value',
                $option['value'] ?? $this->escapeHtml($idx)
            );
            if ($idx == $this->getValue()) {
                $optEl->setAttribute('selected', 'selected');
            }

            $this->getDOMElement()->appendChild($optEl);
        }

        return $this;
    }

    public function setSelection($key): void {
        $this->setValue($key);
    }
}
