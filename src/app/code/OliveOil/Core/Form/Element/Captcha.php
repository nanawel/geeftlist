<?php

namespace OliveOil\Core\Form\Element;

/**
 * @method \OliveOil\Core\Model\Security\Captcha getCaptchaModel()
 */
class Captcha extends TemplateElement
{
    protected function init(): void {
        parent::init();
        $this->setTemplate('captcha.phtml');
    }

    protected function renderHtml() {
        if ($this->getCaptchaModel()) {
            return parent::renderHtml();
        }

        $this->logger->warning('Invalid captcha configuration: missing captcha model!');

        return '<p style="color: red">ERROR: Invalid captcha configuration!</p>';
    }

    /**
     * @return string
     */
    public function getImageSrc() {
        return $this->getCaptchaModel()->getImageSrc();
    }

    /**
     * @return int
     */
    public function getImageWidth() {
        return $this->getCaptchaModel()->getImageWidth();
    }

    /**
     * @return int
     */
    public function getImageHeight() {
        return $this->getCaptchaModel()->getImageHeight();
    }

    /**
     * @return string
     */
    public function getCaptchaId() {
        return $this->getCaptchaModel()->getId();
    }

    /**
     * @return string|null
     */
    public function getAnswer(array $post) {
        return $post[$this->getId()]['answer'] ?? null;
    }
}
