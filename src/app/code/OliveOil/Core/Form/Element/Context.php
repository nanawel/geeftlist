<?php

namespace OliveOil\Core\Form\Element;


class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\View $view,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\DesignInterface $design,
        protected \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $modelFactory
    ) {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getView(): \OliveOil\Core\Service\View {
        return $this->view;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getDesign(): \OliveOil\Core\Service\DesignInterface {
        return $this->design;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }

    public function getModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->modelFactory;
    }
}
