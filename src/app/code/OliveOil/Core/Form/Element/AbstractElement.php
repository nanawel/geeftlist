<?php

namespace OliveOil\Core\Form\Element;

use OliveOil\Core\Form\ElementInterface;
use OliveOil\Core\Model\MagicObject;

abstract class AbstractElement extends MagicObject implements ElementInterface
{
    protected \Base $fw;

    protected \OliveOil\Core\Service\View $view;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \OliveOil\Core\Service\DesignInterface $design;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var \DOMDocument */
    protected static $document;

    /** @var string */
    protected $tagName;

    /** @var \DOMElement */
    protected $domElement;

    /** @var bool */
    protected $isPrepared = false;

    /** @var mixed */
    protected $value;

    protected array $additionalAttributes = [];

    public function __construct(
        \OliveOil\Core\Form\Element\Context $context,
        array $data = []
    ) {
        parent::__construct($data);
        $this->fw = $context->getFw();
        $this->view = $context->getView();
        $this->i18n = $context->getI18n();
        $this->design = $context->getDesign();
        $this->logger = $context->getLogService()->getLoggerForClass($this);

        $this->init();
    }

    protected function init(): void {
        // to be overridden
    }

    public function addClass($class): static {
        $classes = explode(' ', (string) $this->getClass());
        $classes[] = $class;
        $this->setClass(implode(' ', $classes));

        return $this;
    }

    public function removeClass($class): static {
        $classes = explode(' ', (string) $this->getClass());
        if (($key = array_search($class, $classes, true)) !== null) {
            unset($classes[$key]);
        }

        $this->setClass(implode(' ', $classes));

        return $this;
    }

    final public function toHtml() {
        return $this->renderHtml();
    }

    abstract protected function renderHtml();

    public function getValue() {
        $useRequest = true; // TODO Use config
        $useDefault = true; // TODO Use config
        if (
            ($v = parent::getValue()) === null && (! $useRequest
            || ! ($v = $this->fw->get('REQUEST.' . $this->getName()))) && (! $useDefault
            || ! ($v = parent::getDefaultValue()))
        ) {
            $v = null;
        }

        return $v;
    }

    /**
     * @inheritDoc
     */
    public function setData($key, $value = null)
    {
        $this->isPrepared = false;

        return parent::setData($key, $value);
    }

    /**
     * @param string $attr
     * @return $this
     */
    public function setAdditionalAttribute($attr, mixed $value): static {
        if ($value === null) {
            unset($this->additionalAttributes[$attr]);
        } else {
            $this->additionalAttributes[$attr] = $value;
        }

        return $this;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setAdditionalAttributes($attributes): static {
        foreach ($attributes as $attr => $value) {
            $this->setAdditionalAttribute($attr, $value);
        }

        return $this;
    }

    public function getAdditionalAttributes(): array {
        return $this->additionalAttributes;
    }

    public function getAdditionalAttributesHtml(): string {
        $html = '';
        foreach ($this->additionalAttributes as $attr => $value) {
            $html .= sprintf('%s="%s" ', $attr, $this->view->inlineJsQuoteEscape($value, ['"']));
        }

        return $html;
    }

    public function escapeHtml($string) {
        return $this->view->esc($string);
    }

    /**
     * Trim values by default if possible (scalars only)
     */
    public function filter(array &$formData): void {
        if (($formDatakey = $this->getFormDataKey()) && array_key_exists($formDatakey, $formData)) {
            $formData[$formDatakey] = is_scalar($formData[$formDatakey])
                ? trim($formData[$formDatakey])
                : $formData[$formDatakey];
        }
    }

    protected function getFormDataKey() {
        return ($name = trim((string) $this->getData('name'))) !== '' ? $name : null;
    }
}
