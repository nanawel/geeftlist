<?php

namespace OliveOil\Core\Form\Element;

class YesNoSelect extends Select
{
    public const OPTIONS = [
        1 => 'Yes',
        0 => 'No'
    ];

    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        if (! $this->getOptions()) {
            $options = [];
            foreach (self::OPTIONS as $value => $label) {
                $options[$value] = [
                    'label' => $this->i18n->tr($label)
                ];
            }

            $this->setOptions($options);
        }

        return parent::prepareDOMElement();
    }
}
