<?php

namespace OliveOil\Core\Form\Element\File;

use OliveOil\Core\Form\Element\TemplateElement;

class Image extends TemplateElement
{
    protected function init(): void {
        parent::init();
        $this->setTemplate('file/image.phtml');
    }
}
