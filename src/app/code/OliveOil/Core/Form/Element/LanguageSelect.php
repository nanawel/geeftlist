<?php

namespace OliveOil\Core\Form\Element;


class LanguageSelect extends Select
{
    /**
     * @inheritDoc
     */
    protected function prepareDOMElement(): static {
        if (! $this->getOptions()) {
            $options = [];
            foreach ($this->i18n->getAvailableLanguages(false) as $lang) {
                $options[$lang] = [
                    'label' => ucfirst($this->i18n->getLanguageDisplayName($lang))
                ];
            }

            $this->setOptions($options);
        }

        return parent::prepareDOMElement();
    }
}
