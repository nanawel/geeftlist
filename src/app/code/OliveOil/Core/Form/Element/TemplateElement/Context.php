<?php

namespace OliveOil\Core\Form\Element\TemplateElement;


class Context extends \OliveOil\Core\Form\Element\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\View $view,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\DesignInterface $design,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\GenericFactory $modelFactory,
        protected \OliveOil\Core\Service\View\Template\Renderer\RendererInterface $templateRenderer
    ) {
        parent::__construct($fw, $view, $i18n, $design, $logService, $modelFactory);
    }

    public function getTemplateRenderer(): \OliveOil\Core\Service\View\Template\Renderer\RendererInterface {
        return $this->templateRenderer;
    }
}
