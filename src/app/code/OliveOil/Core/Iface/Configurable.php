<?php

namespace OliveOil\Core\Iface;


interface Configurable
{
    /**
     * @param array|string $config
     * @param mixed|null $value
     * @return $this
     */
    public function configure($config, $value = null);

    /**
     * @param null|string $key
     * @param null|mixed $default
     * @return mixed|array
     */
    public function getConfiguration($key = null, $default = null);
}
