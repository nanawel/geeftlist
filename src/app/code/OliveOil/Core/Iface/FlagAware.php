<?php


namespace OliveOil\Core\Iface;


interface FlagAware
{
    public function setFlag(array|string $flag, mixed $flagValue = true): static;

    public function getFlag(string $flag): mixed;

    public function unsetFlag(string $flag): static;
}
