<?php

namespace OliveOil\Core\Iface;


interface IdentityInterface
{
    /**
     * @return string[]
     */
    public function getIdentities();
}
