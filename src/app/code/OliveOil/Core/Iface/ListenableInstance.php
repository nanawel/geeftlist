<?php

namespace OliveOil\Core\Iface;


interface ListenableInstance
{
    /**
     * @param string $eventName
     * @param object $target
     * @param array $argv
     * @return $this
     */
    public function triggerInstanceEvent($eventName, $target = null, $argv = []);

    /**
     * @param string $eventName
     * @param int $priority
     * @return $this
     */
    public function attachInstanceListener($eventName, callable $listener, $priority = 1);

    /**
     * @param string $eventName
     * @return $this
     */
    public function clearInstanceListeners($eventName);

    /**
     * @param string $eventName
     * @return callable[]
     */
    public function getInstanceListeners($eventName = null);
}
