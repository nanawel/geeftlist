<?php

namespace OliveOil\Core\Iface;


interface DynamicDataMethodsAware
{
    /**
     * Add data to the object.
     *
     * Retains previous data in the object.
     *
     * @return $this
     */
    public function addData(array $arr);

    /**
     * Overwrite data in the object.
     *
     * $key can be string or array.
     * If $key is string, the attribute value will be overwritten by $value
     *
     * If $key is an array, it will overwrite all the data in the object.
     *
     * @param string|array $key
     * @return $this
     */
    public function setData($key, mixed $value = null);

    /**
     * @param string|array $key
     * @return $this
     */
    public function setOrigData($key, mixed $value = null);

    /**
     * Unset data from the object.
     *
     * $key can be a string only. Array will be ignored.
     *
     * @param string $key
     * @return $this
     */
    public function unsetData($key = null);

    /**
     * If $key is empty, checks whether there's any data in the object
     * Otherwise checks if the specified attribute is set.
     *
     * @param string $key
     * @return boolean
     */
    public function hasData($key = '');

    public function hasOrigData($key = '');

    /**
     * Retrieves data from the object
     *
     * If $key is empty will return all the data as an array
     * Otherwise it will return value of the attribute specified by $key
     *
     * If $index is specified it will assume that attribute data is an array
     * and retrieve corresponding member.
     *
     * @param string $key
     * @return mixed
     */
    public function getData($key = '');

    /**
     * @param string $key
     * @return mixed
     */
    public function getOrigData($key = '');

    /**
     * @param string $key
     * @return mixed
     */
    public function getDataUsingMethod($key);

    /**
     * @return boolean
     */
    public function hasDataChanges();

    /**
     * @param bool $hasDataChanges
     * @return $this
     */
    public function setDataChanges($hasDataChanges);

    public function resetData();

    public function resetOrigData();

    /**
     * Set/Get attribute wrapper
     *
     * @param   string $method
     * @return  mixed
     */
    public function __call($method, array $args);
}
