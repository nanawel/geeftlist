<?php

namespace OliveOil\Core\Service\View\Template\Engine\TagHandler;

use OliveOil\Core\Service\View\Template\Engine\DefaultEngine;

interface TagHandlerInterface
{
    public function __invoke(DefaultEngine $engine, array $args);
}
