<?php

namespace OliveOil\Core\Service\View\Template\Engine\TagHandler;

use OliveOil\Core\Exception\TemplateException;
use OliveOil\Core\Service\View\Template\Engine\DefaultEngine;

class Image implements TagHandlerInterface
{
    public function __invoke(DefaultEngine $engine, array $args): string {
        $attributes = $args['@attrib'];
        if (! isset($attributes['href'])) {
            throw new TemplateException('Missing image path');
        }

        $imageUrl = $engine->getDesign()->getImageUrl($attributes['href']);
        unset($attributes['href']);

        $attrString = [];
        foreach ($attributes as $name => $value) {
            $attrString[] = sprintf('%s="%s"', $name, $engine->esc($value));
        }

        return sprintf('<img src="%s" %s/>', $imageUrl, implode(' ', $attrString));
    }
}
