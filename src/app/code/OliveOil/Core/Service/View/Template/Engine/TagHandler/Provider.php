<?php

namespace OliveOil\Core\Service\View\Template\Engine\TagHandler;

class Provider
{
    /**
     * @param TagHandlerInterface[]|callable[] $tagHandlers
     */
    public function __construct(protected array $tagHandlers)
    {
    }

    /**
     * @return TagHandlerInterface[]|callable[]
     */
    public function getTagHandlers(): array {
        return $this->tagHandlers;
    }
}
