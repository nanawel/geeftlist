<?php

namespace OliveOil\Core\Service\View\Template\Engine;

interface EngineInterface
{
    /**
     * Arguments here are in this precise order to respect \Preview::resolve() signature
     *
     * @param string $file
     * @param string $mime
     * @param array|null $hive
     * @param int $ttl
     * @return string
     */
    public function render($file, $mime = null, array $hive = null, $ttl = 0);

    /**
     * Arguments here are in this precise order to respect \Preview::render() signature
     *
     * @param string|array $node
     * @param array|null $hive
     * @param int $ttl
     * @param bool $persist
     * @param bool|null $escape
     * @param string|null $mime
     * @return string
     */
    public function resolve($node, array $hive = null, $ttl = 0, $persist = false, $escape = null, $mime = null);

    /**
     * @return $this
     */
    public function setEngineFactory(\OliveOil\Core\Service\View\Template\Engine\Factory $engineFactory);

    public function setTemplateResolver(
        \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface $templateResolver
    );

    /**
     * @return $this
     */
    public function setI18n(\OliveOil\Core\Model\I18n $i18n);
}
