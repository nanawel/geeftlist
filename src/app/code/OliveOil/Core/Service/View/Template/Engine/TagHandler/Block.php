<?php

namespace OliveOil\Core\Service\View\Template\Engine\TagHandler;

use OliveOil\Core\Exception\TemplateException;
use OliveOil\Core\Service\View\Template\Engine\DefaultEngine;

class Block implements TagHandlerInterface
{
    public function __invoke(DefaultEngine $engine, array $args): string {
        $attr = $args['@attrib'];
        if (! isset($attr['name'])) {
            throw new TemplateException('Missing block name');
        }

        // TODO Handle all arguments (mime, hive, ttl)
        return sprintf(
            '<?php if (isset($_VIEW)) echo $_VIEW->renderBlock("%s") ?>',
            $engine->token($attr['name'])
        );
    }
}
