<?php

namespace OliveOil\Core\Service\View\Template\Engine;

class MarkdownEngine extends DefaultEngine
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\Design $design,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Service\Url\RedirectInterface $urlRedirect,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\View\Template\Engine\Factory $engineFactory,
        \OliveOil\Core\Helper\Output $outputHelper,
        \OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider $tagHandlerProvider,
        protected \Parsedown $parser,
        \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface $templateResolver = null
    ) {
        parent::__construct(
            $fw,
            $design,
            $urlBuilder,
            $urlRedirect,
            $i18n,
            $engineFactory,
            $outputHelper,
            $tagHandlerProvider,
            $templateResolver
        );

        $this->init();
    }

    protected function postProcessOutput($output, array $hive = null) {
        $output = parent::postProcessOutput($output, $hive);

        return $this->getParser()->parse($output);
    }

    public function getParser(): \Parsedown {
        return $this->parser;
    }
}
