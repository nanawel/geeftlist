<?php

namespace OliveOil\Core\Service\View\Template\PathResolver;

interface PathResolverInterface
{
    /**
     * @return string
     */
    public function getBaseDir();

    /**
     * @param string $baseDir
     * @return $this
     */
    public function setBaseDir($baseDir);

    /**
     * Resolve file path
     *
     * @param string $file
     * @return string|bool
     */
    public function resolvePath($file, array $params = []);
}
