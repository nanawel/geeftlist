<?php

namespace OliveOil\Core\Service\View\Template\PathResolver;


use OliveOil\Core\Exception\TemplateNotFoundException;

class DesignPathResolver implements PathResolverInterface
{
    /** @var string */
    protected $baseDir;

    protected bool $useLanguage;

    public function __construct(
        protected \OliveOil\Core\Service\Design $design,
        protected \OliveOil\Core\Model\I18n $i18n,
        $baseDir = null,
        $useLanguage = true
    ) {
        if (is_string($baseDir)) {
            $this->setBaseDir($baseDir);
        }

        $this->useLanguage = (bool) $useLanguage;
    }

    /**
     * @param string $baseDir
     * @return $this
     */
    public function setBaseDir($baseDir): static {
        $this->baseDir = trim($baseDir, '/\\');

        return $this;
    }

    /**
     * Find file according to i18n theme and locale
     *
     * @param string $file
     * @return string|bool
     * @throws TemplateNotFoundException
     */
    public function resolvePath($file, array $params = []) {
        $i18n = $params['i18n'] ?? $this->getI18n();
        if ($this->useLanguage()) {
            $localeDirs = array_map(fn(string $locale): string => $this->getBaseDir() . DIRECTORY_SEPARATOR . $locale, $i18n->getSortedAvailableLanguages());
        }
        else {
            $localeDirs = [$this->getBaseDir()];
        }

        if ($path = $this->getDesign()->resolveTemplateFile($file, $localeDirs)) {
            return $path;
        }

        throw new TemplateNotFoundException(sprintf("File '%s' cannot be found", $file));
    }

    public function getDesign(): \OliveOil\Core\Service\Design {
        return $this->design;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    /**
     * @return string
     */
    public function getBaseDir() {
        return $this->baseDir;
    }

    public function useLanguage(): bool {
        return $this->useLanguage;
    }
}
