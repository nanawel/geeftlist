<?php

namespace OliveOil\Core\Service\View\Template\Engine;


use OliveOil\Core\Model\I18n;

class DefaultEngine extends \Template implements EngineInterface
{
    /** @var bool */
    protected $isRendering = false;

    /** @var array */
    protected $currentContext = [];

    public function __construct(
        \Base $fw,
        protected \OliveOil\Core\Service\Design $design,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\Url\RedirectInterface $urlRedirect,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\View\Template\Engine\Factory $engineFactory,
        protected \OliveOil\Core\Helper\Output $outputHelper,
        protected \OliveOil\Core\Service\View\Template\Engine\TagHandler\Provider $tagHandlerProvider,
        protected ?\OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface $templateResolver = null
    ) {
        $this->fw = $fw;
        parent::__construct();

        $this->init();
    }

    protected function init() {
        $this->addCustomFilters()
            ->addCustomTags();
    }

    public function resolve($node, array $hive = null, $ttl = 0, $persist = false, $escape = null, $mime = null) {
        $this->prepareHive($hive);
        try {
            if ($this->isRendering) {
                return $this->getEngineFactory()->makeFromCode($mime)
                    ->setI18n($this->getI18n())
                    ->resolve($node, $hive, $ttl, $persist, $escape, $mime);
            }

            $this->isRendering = true;
            $this->saveContext($hive, $ttl);

            return $this->postProcessOutput(
                parent::resolve($this->parse($node), $hive, $ttl, false),
                $hive
            );
        }
        finally {
            $this->isRendering = false;
            $this->clearContext();
        }
    }

    public function render($file, $mime = null, array $hive = null, $ttl = 0) {
        $this->prepareHive($hive);
        try {
            if ($this->isRendering) {
                return $this->getEngineFactory()->makeForFile($file)
                    ->setI18n($this->getI18n())
                    ->render($file, $mime, $hive, $ttl);
            }

            $this->isRendering = true;
            $this->saveContext($hive, $ttl);
            $resolvedFile = $this->getTemplateResolver()->resolvePath($file, ['i18n' => $this->getI18n()]);
            $obLevelBeforeRender = ob_get_level();

            return $this->postProcessOutput(parent::render($resolvedFile, $mime, $hive, $ttl), $hive);
        }
        catch (\Throwable $throwable) {
            if (isset($obLevelBeforeRender)) {
                // #516 Discard incomplete buffers before returning
                while (ob_get_level() > $obLevelBeforeRender) {
                    /** See \View::sandbox() */
                    --$this->level;
                    ob_end_clean();
                }
            }

            throw $throwable;
        }
        finally {
            $this->isRendering = false;
            $this->clearContext();
        }
    }

    protected function postProcessOutput($output, array $hive = null) {
        return $output;
    }

    protected function addCustomFilters(): static {
        $this->filter('url', fn($str, $params = []) => $this->filterUrl($str, $params));
        $this->filter('url_redirect', fn($url) => $this->filterUrlRedirect($url));
        $this->filter('price', fn(float $str, ?\OliveOil\Core\Model\I18n $i18n = null): string => $this->filterPrice($str, $i18n));
        $this->filter('tr', fn(string $str, array $vars = [], ?\OliveOil\Core\Model\I18n $i18n = null): string => $this->filterTr($str, $vars, $i18n));
        $this->filter('tr_raw', fn(string $str, array $vars = [], ?\OliveOil\Core\Model\I18n $i18n = null): string => $this->filterTrRaw($str, $vars, $i18n));
        $this->filter('nl2br', fn(string $str): string => $this->filterNl2br($str));
        $this->filter('date', fn($date, ?\OliveOil\Core\Model\I18n $i18n = null): string => $this->filterDate($date, $i18n));
        $this->filter('datetime', fn($date, ?\OliveOil\Core\Model\I18n $i18n = null): string => $this->filterDateTime($date, $i18n));
        $this->filter('hlist', fn(array $arr, ?\OliveOil\Core\Model\I18n $i18n = null): string => $this->filterHumanList($arr, $i18n));
        $this->filter('esc_js', fn(string $string, ?array $quote = ["'"]): string => $this->filterEscapeJs($string, $quote));

        return $this;
    }

    protected function addCustomTags(): static {
        foreach ($this->tagHandlerProvider->getTagHandlers() as $tagName => $tagHandler) {
            $this->extend($tagName, fn($args) => $tagHandler($this, $args));
        }

        return $this;
    }

    public function filterUrl($str, $params = []) {
        return $this->getUrlBuilder()->getUrl($str, $params);
    }

    public function filterUrlRedirect($url) {
        return $this->getUrlRedirect()->toRedirectUrl($url);
    }

    /**
     * @param float $str
     * @param I18n|null $i18n
     * @return string
     */
    public function filterPrice($str, I18n $i18n = null) {
        if (!$i18n instanceof \OliveOil\Core\Model\I18n) {
            $i18n = $this->getI18n();
        }

        return $str !== null ? $i18n->currency($str) : '';
    }

    /**
     *
     * @param string $str
     * @param I18n|null $i18n
     * @return string
     */
    public function filterTr($str, array $vars = [], I18n $i18n = null) {
        // Escape all $vars
        array_walk($vars, function (&$var): void {
            $var = $this->esc($var);
        });

        return $this->filterTrRaw($str, $vars, $i18n);
    }

    /**
     * @param string $str
     * @param I18n|null $i18n
     * @return string
     */
    public function filterTrRaw($str, array $vars = [], I18n $i18n = null) {
        if (!$i18n instanceof \OliveOil\Core\Model\I18n) {
            $i18n = $this->getI18n();
        }

        return $i18n->tr($str, $vars);
    }

    /**
     * @param string $str
     */
    public function filterNl2br($str): string {
        return nl2br($str);
    }

    /**
     * @param I18n|null $i18n
     * @return string
     */
    public function filterDate(mixed $date, I18n $i18n = null) {
        if (!$i18n instanceof \OliveOil\Core\Model\I18n) {
            $i18n = $this->getI18n();
        }

        return $i18n->date($date);
    }

    /**
     * @param I18n|null $i18n
     * @return string
     */
    public function filterDateTime(mixed $date, I18n $i18n = null) {
        if (!$i18n instanceof \OliveOil\Core\Model\I18n) {
            $i18n = $this->getI18n();
        }

        return $i18n->datetime($date);
    }

    /**
     * @param array $arr
     * @param I18n|null $i18n
     * @return string
     */
    public function filterHumanList($arr, I18n $i18n = null) {
        if (!$i18n instanceof \OliveOil\Core\Model\I18n) {
            $i18n = $this->getI18n();
        }

        $string = '';
        switch (count($arr)) {
            case 0:
                // Nothing
                break;

            case 1:
                $string = (string) current($arr);
                break;

            default:
                $string = $i18n->tr(
                    '{0} and {1}',
                    implode($i18n->tr(', '), array_slice($arr, 0, -1)),
                    current(array_slice($arr, -1))
                );
                break;
        }

        return $string;
    }

    /**
     * @param string $string
     * @param string[]|null $quote
     * @return string
     */
    public function filterEscapeJs($string, $quote = ["'"]) {
        return $this->getOutputHelper()->jsQuoteEscape($string, $quote);
    }

    protected function prepareHive(mixed &$hive) {
        if (! $hive) {
            $hive = ['__disableF3HiveFallback' => true]; // Arbitrary value just to avoid having an empty $hive
                                                         // that would cause F3 to fallback to the global HIVE
        }
    }

    protected function saveContext(array $hive = null, $ttl = null): static {
        $this->currentContext = [
            'hive' => $hive,
            'ttl'  => $ttl
        ];

        return $this;
    }

    protected function getContextElement($key) {
        return $this->currentContext[$key] ?? null;
    }

    protected function clearContext(): static {
        $this->currentContext = [];

        return $this;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getUrlRedirect(): \OliveOil\Core\Service\Url\RedirectInterface {
        return $this->urlRedirect;
    }

    public function getDesign(): \OliveOil\Core\Service\Design {
        return $this->design;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    /**
     * @return $this
     */
    public function setI18n(I18n $i18n): static {
        $this->i18n = $i18n;

        return $this;
    }

    /**
     * @return \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface
     */
    public function getTemplateResolver(): ?\OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface {
        return $this->templateResolver;
    }

    public function setTemplateResolver(
        \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface $templateResolver
    ): static {
        $this->templateResolver = $templateResolver;

        return $this;
    }

    public function getEngineFactory(): \OliveOil\Core\Service\View\Template\Engine\Factory {
        return $this->engineFactory;
    }

    public function getOutputHelper(): \OliveOil\Core\Helper\Output {
        return $this->outputHelper;
    }

    public function setEngineFactory(\OliveOil\Core\Service\View\Template\Engine\Factory $engineFactory): static {
        $this->engineFactory = $engineFactory;

        return $this;
    }
}
