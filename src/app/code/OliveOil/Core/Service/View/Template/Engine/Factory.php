<?php

namespace OliveOil\Core\Service\View\Template\Engine;


use OliveOil\Core\Service\GenericFactory;

class Factory extends GenericFactory
{
    /** @var \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface */
    protected $templateResolver;

    /**
     * @param string $file
     * @return \OliveOil\Core\Service\View\Template\Engine\EngineInterface
     */
    public function makeForFile($file, array $parameters = []) {
        return $this->makeFromCode($this->guessFormatFromTemplate($file), $parameters);
    }

    /**
     * @param string $fqcn
     * @return \OliveOil\Core\Service\View\Template\Engine\EngineInterface
     */
    public function make($fqcn, array $parameters = []) {
        /** @var \OliveOil\Core\Service\View\Template\Engine\EngineInterface $instance */
        $instance = parent::make($fqcn, $parameters);
        if ($instance instanceof \OliveOil\Core\Service\View\Template\Engine\EngineInterface) {
            $instance->setEngineFactory($this);
            if ($templateResolver = $this->getTemplateResolver()) {
                $instance->setTemplateResolver($templateResolver);
            }
        }

        return $instance;
    }

    /**
     * TODO Move to a dedicated class or at least to DI definition
     *
     * @param string $file
     */
    protected function guessFormatFromTemplate($file): string {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        return match (strtolower($extension)) {
            'md', 'pmd' => 'text/markdown',
            'xml' => 'text/xml',
            default => 'text/html',
        };
    }

    /**
     * @return \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface
     */
    public function getTemplateResolver() {
        return $this->templateResolver;
    }

    /**
     * @param \OliveOil\Core\Service\View\Template\PathResolver\PathResolverInterface $templateResolver
     */
    public function setTemplateResolver($templateResolver): static {
        $this->templateResolver = $templateResolver;

        return $this;
    }
}
