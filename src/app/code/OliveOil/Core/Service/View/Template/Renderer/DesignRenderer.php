<?php

namespace OliveOil\Core\Service\View\Template\Renderer;

/**
 * Class DesignRenderer
 */
class DesignRenderer implements RendererInterface
{
    public function __construct(protected \OliveOil\Core\Service\Design $design, protected \OliveOil\Core\Model\I18n $i18n, protected \OliveOil\Core\Service\View\Template\Engine\Factory $engineFactory)
    {
    }

    public function resolve($node, $hive = [], $mime = null, array $params = []) {
        return $this->getEngineFactory()->makeFromCode($mime)
            ->setI18n($params['i18n'] ?? $this->getI18n())
            ->resolve(
                $node,
                $hive,
                $params['ttl'] ?? 0,
                false,
                null,
                $mime
            );
    }

    public function render($file, $hive = [], $mime = null, array $params = []) {
        return $this->getEngineFactory()->makeForFile($file)
            ->setI18n($params['i18n'] ?? $this->getI18n())
            ->render(
                $file,
                $mime,
                $hive,
                $params['ttl'] ?? 0
            );
    }

    public function getDesign(): \OliveOil\Core\Service\Design {
        return $this->design;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getEngineFactory(): \OliveOil\Core\Service\View\Template\Engine\Factory {
        return $this->engineFactory;
    }
}
