<?php

namespace OliveOil\Core\Service\View\Template\Engine\TagHandler;

use OliveOil\Core\Exception\TemplateException;
use OliveOil\Core\Service\View\Template\Engine\DefaultEngine;

class CmsBlock implements TagHandlerInterface
{
    /** @var \OliveOil\Core\Model\RepositoryInterface* */
    protected $cmsBlockRepository;

    public function __construct(
        \Geeftlist\Model\RepositoryFactory $repositoryFactory
    ) {
        $this->cmsBlockRepository = $repositoryFactory->resolveGet(\Geeftlist\Model\Cms\Block::ENTITY_TYPE);
    }

    public function __invoke(DefaultEngine $engine, array $args) {
        $attr = $args['@attrib'];
        if (! isset($attr['code'])) {
            throw new TemplateException('Missing CMS block code');
        }

        /** @var \Geeftlist\Model\Cms\Block|null $cmsBlock */
        $cmsBlock = $this->cmsBlockRepository->get($attr['code'], 'code');
        if ($cmsBlock) {
            return sprintf(
                '<?php if (isset($_VIEW)) echo $_VIEW->getLayout()->newBlock("%s")->setConfig("code", "%s")->render() ?>',
                \Geeftlist\Block\Cms::class,
                $engine->token($cmsBlock->getCode())
            );
        }
    }
}
