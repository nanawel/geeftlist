<?php

namespace OliveOil\Core\Service\View\Template\Renderer;


interface RendererInterface
{
    /**
     * @param array $hive
     * @param string|null $mime
     * @return string
     */
    public function resolve(mixed $node, $hive = [], $mime = null, array $params = []);

    /**
     * @param string $file
     * @param array $hive
     * @param string|null $mime
     * @return string
     */
    public function render($file, $hive = [], $mime = null, array $params = []);
}
