<?php

namespace OliveOil\Core\Service\View;


interface ProcessorInterface
{
    public function resolve($content, array $hive = null, array $config = []);
}
