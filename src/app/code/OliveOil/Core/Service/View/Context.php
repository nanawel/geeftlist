<?php

namespace OliveOil\Core\Service\View;

class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Model\View\LayoutInterface $layout,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory
    ) {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getLayout(): \OliveOil\Core\Model\View\LayoutInterface {
        return $this->layout;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }
}
