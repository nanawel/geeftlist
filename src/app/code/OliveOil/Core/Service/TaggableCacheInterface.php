<?php

namespace OliveOil\Core\Service;


interface TaggableCacheInterface
{
    /**
     * @param string $key
     * @param int|null $ttl
     * @param string[] $tags
     * @return bool
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function set($key, mixed $value, $ttl = null, array $tags = []);

    /**
     * @param array $values
     * @param int|null $ttl
     * @param string[] $tags
     * @return bool
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function setMultiple($values, $ttl = null, array $tags = []);

    /**
     * @param string $tag
     * @return bool
     * @throws \InvalidArgumentException When $tag is not valid
     */
    public function invalidateTag($tag);

    /**
     * @param string[] $tags
     * @return bool
     * @throws \InvalidArgumentException When $tags is not valid
     */
    public function invalidateTags(array $tags);
}
