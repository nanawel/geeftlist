<?php

namespace OliveOil\Core\Service;


class Crypt
{
    public const TOKEN_FORMAT_REGEXP = '/[a-f0-9]{%d}/i';

    public const TOKEN_DEFAULT_LENGTH = 32;

    /**
     * @param string $pw
     * @param string $algo
     */
    public function hash(string $pw, string $algo = PASSWORD_BCRYPT): string {
        return password_hash($pw, $algo);
    }

    /**
     * @param string $pw
     * @param string $hash
     */
    public function verify(string $pw, string $hash): bool {
        return password_verify($pw, $hash);
    }

    /**
     * Generate secure token
     *
     * @param int $length
     */
    public function generateToken(int $length = self::TOKEN_DEFAULT_LENGTH): string {
        if ($length % 2 !== 0) {
            throw new \InvalidArgumentException('$length must be a multiple of 2');
        }

        return bin2hex(random_bytes($length / 2));
    }

    /**
     * Check token format
     *
     * @param string $token
     * @param int $length
     */
    public function isTokenFormatValid(string $token, int $length = self::TOKEN_DEFAULT_LENGTH): bool {
        return (bool) preg_match(sprintf(self::TOKEN_FORMAT_REGEXP, $length), $token);
    }
}
