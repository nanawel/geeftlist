<?php

namespace OliveOil\Core\Service\Log;


use Cascade\Cascade;
use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Exception\InvalidConfigurationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

class Resolver implements ResolverInterface
{
    public const DEFAULT_LOGGER = 'app';
    public const VOID_LOGGER    = 'void';

    protected ?array $definitions = null;

    /** @var string[]|null */
    protected ?array $loggerByIdentifierCache = null;

    protected bool $initialized = false;

    protected string $cacheKey = '';

    /**
     * @param string[] $configFiles
     * @param string[] $monologConfigFiles
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Helper\ConfigPreprocessor $configPreprocessor,
        protected \OliveOil\Core\Service\Cache $cache,
        protected array $configFiles,
        protected array $monologConfigFiles,
        protected bool $enabled = true
    ) {
    }

    /**
     * @throws FilesystemException
     */
    public function getLoggerByIdentifier(string $identifier, array $params = []): ?LoggerInterface {
        $loggerName = $this->getLoggerNameFromIdentifier($identifier);

        return $this->getLoggerByName($loggerName, $params);
    }

    /**
     * @return LoggerInterface|null
     */
    public function getLoggerByName(string $name, array $params = []): ?LoggerInterface {
        if (!$this->initialized) {
            Cascade::fileConfig($this->loadMonologConfigFiles());
            $this->initialized = true;
        }

        if (!$this->enabled) {
            $name = static::VOID_LOGGER;
        }

        return Cascade::getLogger($name);
    }

    /**
     * @throws FilesystemException
     */
    protected function getLoggerNameFromIdentifier(string $identifier): string {
        $cacheKey = implode('|', ['loggerByIdentifier', $this->getCacheKey()]);
        if ($this->loggerByIdentifierCache === null) {
            $this->loggerByIdentifierCache = $this->cache->get($cacheKey, []);
        }

        if (! isset($this->loggerByIdentifierCache[$identifier])) {
            $definitions = $this->getDefinitions();
            foreach ($definitions as $config) {
                if ($this->identifierMatchesDefinition($identifier, $config)) {
                    $this->loggerByIdentifierCache[$identifier] = $config['name'];
                    break;
                }
            }

            // Unknown identifier, use default
            if (! isset($this->loggerByIdentifierCache[$identifier])) {
                $this->loggerByIdentifierCache[$identifier] = self::DEFAULT_LOGGER;
            }

            $this->cache->set($cacheKey, $this->loggerByIdentifierCache);
        }

        return $this->loggerByIdentifierCache[$identifier];
    }

    public function identifierMatchesDefinition(string $identifier, array $definitionConfig): bool {
        if (! isset($definitionConfig['pattern'])) {
            return true;
        }

        return fnmatch($definitionConfig['pattern'], $identifier);
    }

    /**
     * @throws FilesystemException
     */
    protected function getDefinitions(): array {
        if ($this->definitions === null) {
            $this->definitions = $this->loadDefinitions();
            if (!isset($this->definitions['loggers'])) {
                $this->definitions['loggers'] = [];
            }

            // Sort from most specific to less specific for priority
            uksort(
                $this->definitions['loggers'],
                static fn($a, $b): int
                    => substr_count((string) $b, '.') - substr_count((string) $a, '.')
            );

            // Add default pattern based on ID if missing
            foreach ($this->definitions['loggers'] as $id => &$config) {
                if (!array_key_exists('pattern', $config)) {
                    $config['pattern'] = $id;
                }
            }
        }

        return $this->definitions['loggers'];
    }

    /**
     * @throws FilesystemException
     * @throws \Exception
     */
    protected function loadDefinitions(): array {
        $cacheKey = implode('|', ['definitions', $this->getCacheKey()]);
        if ($this->cache->has($cacheKey)) {
            $definitions = $this->cache->get($cacheKey);
        }
        else {
            $definitions = [];
            foreach ($this->configFiles as $configFile) {
                if (! is_file($configFile) || ! is_readable($configFile)) {
                    throw new FilesystemException('File not found or not readable: ' . $configFile);
                }

                /** @noinspection SlowArrayOperationsInLoopInspection */
                $definitions = array_replace_recursive(
                    $definitions,
                    Yaml::parse(file_get_contents($configFile))
                );
            }

            $this->cache->set($cacheKey, $definitions);
        }

        return $definitions;
    }

    /**
     * Load Monolog config file and replace variables inside if any.
     *
     * @return string[]
     * @throws FilesystemException
     */
    protected function loadMonologConfigFiles(): array {
        $cacheKey = sha1(implode('|', $this->monologConfigFiles));
        if ($this->cache->has($cacheKey)) {
            $config = $this->cache->get($cacheKey);
        }
        else {
            $config = [];
            foreach ($this->monologConfigFiles as $configFile) {
                if (! is_file($configFile) || ! is_readable($configFile)) {
                    throw new FilesystemException('File not found or not readable');
                }

                // Temporary error handler just for the next step
                set_error_handler(
                    static function ($errno, $errstr, $errfile = null, $errline = null, array $errcontext = []): void {
                        throw new \ErrorException($errstr, $errno, 1, $errfile, $errline);
                    }
                );
                try {
                    $content = $this->configPreprocessor->run(file_get_contents($configFile));
                }
                catch (\Throwable $e) {
                    throw new InvalidConfigurationException("Could not parse file: $configFile", previous: $e);
                }
                restore_error_handler();

                /** @noinspection SlowArrayOperationsInLoopInspection */
                $config = array_replace_recursive(
                    $config,
                    Yaml::parse($content)
                );
            }

            $this->cache->set($cacheKey, $config);
        }

        return $config;
    }

    protected function getCacheKey() {
        if (!$this->cacheKey) {
            $this->cacheKey = sha1(implode('|', array_merge($this->configFiles, $this->monologConfigFiles)));
        }

        return $this->cacheKey;
    }

    public function reset(): void
    {
        $this->definitions = null;
        $this->loggerByIdentifierCache = null;
        $this->initialized = false;
    }
}
