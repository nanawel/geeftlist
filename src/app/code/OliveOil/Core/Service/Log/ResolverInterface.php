<?php

namespace OliveOil\Core\Service\Log;

use Psr\Log\LoggerInterface;

interface ResolverInterface
{
    /**
     * @param string $identifier
     * @param array $params
     * @return LoggerInterface|null
     */
    public function getLoggerByIdentifier(string $identifier, array $params = []);

    /**
     * @param string $name
     * @param array $params
     * @return LoggerInterface|null
     */
    public function getLoggerByName(string $name, array $params = []);

    /**
     * Reset internal configuration in order to force loading it again.
     */
    public function reset(): void;
}
