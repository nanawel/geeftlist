<?php

namespace OliveOil\Core\Service\Log\Monolog\Processor;


class IntrospectionProcessor extends \Monolog\Processor\IntrospectionProcessor
{
    public function __invoke(array $record): array
    {
        $record = parent::__invoke($record);

        // Keep full path as backup
        $record['extra']['file_full'] = $record['extra']['file'];
        // Add short path
        $record['extra']['file'] = substr(
            (string) $record['extra']['file'],
            strlen(rtrim(getcwd(), DIRECTORY_SEPARATOR))
        );

        return $record;
    }
}
