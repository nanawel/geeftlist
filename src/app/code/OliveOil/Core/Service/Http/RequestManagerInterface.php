<?php


namespace OliveOil\Core\Service\Http;


use OliveOil\Core\Model\Http\RequestInterface;
use OliveOil\Core\Model\Http\ResponseInterface;

interface RequestManagerInterface
{
    /**
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * @return ResponseInterface
     */
    public function getResponse();

    /**
     * @return $this
     */
    public function reset();

    /**
     * @param bool $initFromGlobals
     * @return RequestInterface
     */
    public function newRequest($initFromGlobals = true);

    /**
     * @return ResponseInterface
     */
    public function newResponse();
}
