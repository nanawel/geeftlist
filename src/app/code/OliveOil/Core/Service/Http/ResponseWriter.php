<?php


namespace OliveOil\Core\Service\Http;


use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Model\Http\ResponseInterface;

class ResponseWriter implements ResponseWriterInterface
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Helper\Data\Factory $helperDataFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        /** @var callable[] */
        protected array $callbacks = []
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    public function write(ResponseInterface $response): void {
        $this->beforeWrite($response);

        // Response code
        if (!$responseCode = $response->getCode()) {
            $response->setCode(HttpStatus::STATUS_OK);
        }

        http_response_code($response->getCode());

        // Headers
        if (!isset($response->getHeaders()['Content-Type'])) {
            // Default: JSON
            $response->addHeaders(['Content-Type' => $this->getDefaultContentType()]);
        }

        $this->beforeWriteHeaders($response);
        $this->writeHeaders($response->getHeaders());

        // Body
        $body = $this->prepareBody($response);

        $this->beforeWriteBody($response);
        $this->writeBody($body, $response->getHeaders()['Content-Type']);
        $this->afterWriteBody($response);

        $this->afterWrite($response);
    }

    protected function beforeWrite(ResponseInterface $response) {
        $this->getEventManager()->trigger(
            'write::before',
            $this,
            ['response' => $response]
        );
    }

    protected function beforeWriteHeaders(ResponseInterface $response) {
        $this->getEventManager()->trigger(
            'write::beforeWriteHeaders',
            $this,
            ['response' => $response]
        );
    }

    /**
     * @param array $headers
     */
    protected function writeHeaders($headers) {
        //if (! headers_sent()) {
        foreach ($headers as $headerName => $headerValue) {
            header(sprintf('%s: %s', $headerName, $headerValue));
        }

        //}
    }

    /**
     * @return mixed
     */
    protected function prepareBody(ResponseInterface $response) {
        return $response->getBody();
    }

    protected function beforeWriteBody(ResponseInterface $response) {
        $this->getEventManager()->trigger(
            'write::beforeWriteBody',
            $this,
            ['response' => $response]
        );
    }

    /**
     * @param string $body
     * @param string $contentType
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function writeBody($body, $contentType) {
        echo $body;
    }

    protected function afterWriteBody(ResponseInterface $response) {
        foreach ($this->callbacks['afterWriteBody'] ?? [] as $callback) {
            call_user_func($callback, $response);
        }
    }

    protected function afterWrite(ResponseInterface $response) {
        $this->getEventManager()->trigger(
            'write::after',
            $this,
            ['response' => $response]
        );
    }

    public function getDefaultContentType(): string {
        // FIXME Should be split in encoding/charset
        return 'text/html; charset=utf-8';
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getHelperDataFactory(): \OliveOil\Core\Helper\Data\Factory {
        return $this->helperDataFactory;
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }
}
