<?php


namespace OliveOil\Core\Service\Http;


use OliveOil\Core\Model\Http\RequestInterface;
use OliveOil\Core\Model\Http\ResponseInterface;

class RequestManager implements RequestManagerInterface
{
    /** @var RequestInterface|null */
    protected $request;

    /** @var ResponseInterface|null */
    protected $response;

    public function __construct(protected \Base $fw, protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory)
    {
    }

    /**
     * @return RequestInterface
     */
    public function getRequest() {
        if (!$this->request) {
            $this->request = $this->newRequest(true);
        }

        return $this->request;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse() {
        if (!$this->response) {
            $this->response = $this->newResponse();
        }

        return $this->response;
    }

    /**
     * @return $this
     */
    public function reset(): static {
        $this->request = null;
        $this->response = null;

        return $this;
    }

    /**
     * @param bool $initFromGlobals
     * @return RequestInterface
     */
    public function newRequest($initFromGlobals = true) {
        /** @var RequestInterface $request */
        $request = $this->coreFactory->make(RequestInterface::class);
        if ($initFromGlobals) {
            $request->fromGlobals();
        }

        return $request;
    }

    /**
     * @return ResponseInterface
     */
    public function newResponse() {
        return $this->coreFactory->make(ResponseInterface::class);
    }
}
