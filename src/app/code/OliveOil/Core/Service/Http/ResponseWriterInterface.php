<?php


namespace OliveOil\Core\Service\Http;


use OliveOil\Core\Model\Http\ResponseInterface;

interface ResponseWriterInterface
{
    public function write(ResponseInterface $response);
}
