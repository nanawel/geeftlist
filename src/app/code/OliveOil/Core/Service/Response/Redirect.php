<?php

namespace OliveOil\Core\Service\Response;


class Redirect
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder)
    {
    }

    /**
     * @param string $fragment
     * @param string $fallback
     * @return $this
     */
    public function redirectReferer(?string $fragment = '', $fallback = '/'): static {
        // 1st priority: query referer if present and valid
        if (($referer = $this->getUrlBuilder()->getQueryReferer()) && str_starts_with((string) $referer, $this->getUrlBuilder()->getBaseUrl())) {
            $this->rerouteUrl($referer . ($fragment ? '#' . $fragment : ''));
        }

        // 2nd priority: referer header if present and valid
        if (($referer = $this->getUrlBuilder()->getReferer()) && str_starts_with($referer, $this->getUrlBuilder()->getBaseUrl())) {
            $this->rerouteUrl($referer . ($fragment ? '#' . $fragment : ''));
        }

        // fallback
        $this->reroute($fallback);

        return $this;
    }

    public function reroute($path, $params = [], $permanent = false): void {
        $url = $this->getUrlBuilder()->getUrl($path, array_merge(['_force_scheme' => true], $params));
        $this->rerouteUrl($url, $permanent);
    }

    /**
     * @param string $url
     * @param bool $permanent
     * @return never
     * @return never-return
     * @return never-returns
     * @return no-return
     */
    public function rerouteUrl($url, $permanent = false): void {
        $this->getFw()->reroute($url, $permanent);
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }
}
