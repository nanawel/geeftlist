<?php

namespace OliveOil\Core\Service;

use OliveOil\Core\Exception\RegistryException;

/**
 * Class Registry
 *
 * Wrapper for F3 \Registry that can be extended to use a non-static storage.
 */
class Registry implements RegistryInterface
{
    /**
     * Return TRUE if object exists in registry
     *
     * @param string $key
     * @return bool
     */
    public function exists($key) {
        return \Registry::exists($key);
    }

    /**
     * Add object to registry
     *
     * @param string $key
     * @param mixed $value
     * @param bool $overwrite
     * @return $this
     * @throws RegistryException
     */
    public function set($key, $value, $overwrite = true): static {
        if ($this->exists($key) && !$overwrite) {
            throw new RegistryException(sprintf('Key "%s" already exists.', $key));
        }

        \Registry::set($key, $value);

        return $this;
    }

    /**
     * Retrieve object from registry
     *
     * @param string $key
     * @return mixed|null
     */
    public function get($key) {
        if (!$this->exists($key)) {
            return null;
        }

        return \Registry::get($key);
    }

    /**
     * Delete object from registry
     *
     * @param string $key
     * @return $this
     */
    public function clear($key): static {
        \Registry::clear($key);

        return $this;
    }
}
