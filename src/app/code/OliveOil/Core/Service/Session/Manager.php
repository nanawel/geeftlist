<?php

namespace OliveOil\Core\Service\Session;


class Manager
{
    public const SESSION_DEFAULT = 'core';

    /** @var \OliveOil\Core\Model\SessionInterface[] */
    protected $sessions = [];

    /**
     * Manager constructor.
     *
     * @param \OliveOil\Core\Model\SessionInterface[] $customSessions
     */
    public function __construct(
        protected \OliveOil\Core\Service\Session\Factory $factory,
        array $customSessions = []
    ) {
        foreach ($customSessions as $customSession) {
            $this->sessions[$customSession->getName()] = $customSession;
        }
    }

    /**
     * @param string $name
     * @return \OliveOil\Core\Model\SessionInterface
     */
    public function getSession($name = null) {
        if ($name === null) {
            $name = self::SESSION_DEFAULT;
        }

        if (!isset($this->sessions[$name])) {
            $this->sessions[$name] = $this->factory->make($name);
        }

        return $this->sessions[$name];
    }

    /**
     * @return \OliveOil\Core\Model\SessionInterface[]
     */
    public function getSessions() {
        return $this->sessions;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function initSession($name = null): static {
        if ($name !== null) {
            $this->getSession($name)->init();
        }
        else {
            foreach ($this->sessions as $session) {
                $session->init();
            }
        }

        return $this;
    }
}
