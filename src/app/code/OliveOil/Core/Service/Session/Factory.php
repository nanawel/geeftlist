<?php

namespace OliveOil\Core\Service\Session;


class Factory
{
    /**
     * @param string $sessionClass
     */
    public function __construct(protected \DI\Container $container, protected $sessionClass)
    {
    }

    /**
     * @param string $name
     * @return \OliveOil\Core\Model\Session
     */
    public function make($name) {
        return $this->container->make($this->sessionClass, ['name' => $name]);
    }
}
