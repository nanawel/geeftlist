<?php

namespace OliveOil\Core\Service\Email\Sender;


use OliveOil\Core\Model\Email\EmailInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

/**
 * Email sender implementation using symfony/mailer
 */
class SymfonyMailerSender implements SenderInterface
{
    public const DEFAULT_MIMETYPE = 'text/html';

    public const DEFAULT_CHARSET  = 'utf-8';

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\Log $logService,
        protected \Symfony\Component\Mailer\MailerInterface $mailer
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @inheritDoc
     */
    public function send(EmailInterface $email, $throwException = true): static {
        if (!$this->appConfig->getValue('EMAIL_ENABLED')) {
            $this->logger->debug('Email sending is disabled, ignoring request.');

            return $this;
        }

        try {
            $startTime = microtime(true);
            $email->unsError()
                ->prepareForSending();

            $this->sendViaMailer($email);

            $time = microtime(true) - $startTime;
            $this->logger->debug(sprintf(
                'Sent "%s" to %s in %1.2f sec.',
                $email->getSubject(),
                $email->getTo(),
                $time
            ));
        }
        catch (\Throwable $throwable) {
            $email->setError($throwable->getMessage())
                ->setException($throwable);

            if ($throwException) {
                throw $throwable;
            }

            $this->logger->critical('Silently caught exception: ' . $throwable->getMessage(), ['exception' => $throwable]);
        }

        return $this;
    }

    /**
     * @return void
     */
    protected function sendViaMailer(EmailInterface $email) {
        $this->mailer->send($this->toSymfonyEmail($email));
    }

    /**
     * TODO Complete it (a lot of fields are still missing)
     */
    protected function toSymfonyEmail(EmailInterface $email): \Symfony\Component\Mime\Email {
        /** @var \Symfony\Component\Mime\Email $symfonyEmail */
        $symfonyEmail = new Email();
        $symfonyEmail->to($email->getTo())
            ->subject($email->getSubject());

        $mimetype = $email->getMimetype() ?: self::DEFAULT_MIMETYPE;
        $encoding = $email->getEncoding() ?: self::DEFAULT_CHARSET;

        if ($mimetype === 'text/html') {
            $symfonyEmail->html($email->getBody(), $encoding);
        } else {
            $symfonyEmail->text($email->getBody(), $encoding);
        }

        if ($from = $email->getFrom()) {
            $symfonyEmail->from($this->toSymfonyAddress($from));
        }

        if ($replyTo = $email->getReplyTo()) {
            $symfonyEmail->replyTo($this->toSymfonyAddress($replyTo));
        }

        /*if ($cc = $email->getCc()) {
            if (is_array($cc)) {
                //TODO
            }
            $symfonyEmail->setCc($cc);
        }*/

        //TODO Map the rest when needed

        return $symfonyEmail;
    }

    /**
     * @param string $address
     */
    protected function toSymfonyAddress($address): \Symfony\Component\Mime\Address {
        if (!is_string($address)) {
            throw new \InvalidArgumentException('$address must be a string.');
        }

        return Address::create($address);
    }
}
