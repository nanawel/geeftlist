<?php

namespace OliveOil\Core\Service\Email\Sender;


use OliveOil\Core\Exception\MailException;
use OliveOil\Core\Model\Email\EmailInterface;

interface SenderInterface
{
    /**
     * @param bool $throwException
     * @return $this
     * @throws MailException
     * @throws \Throwable
     */
    public function send(EmailInterface $email, $throwException = true);
}
