<?php

namespace OliveOil\Core\Service\Security;


use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\Security\CsrfException;

class Csrf
{
    protected \Psr\Log\LoggerInterface $logger;

    /**
     * @param string $tokenName
     * @param string $sessionName
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\Log $logService,
        protected $tokenName,
        protected $sessionName = null
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function getTokenName(): string {
        return $this->tokenName;
    }

    public function getTokenValue(): string {
        return $this->getSession()->getCsrfToken();
    }

    /**
     * @throws CsrfException
     */
    public function validate(array $hives = [Http::VERB_POST, Http::VERB_GET]): void {
        if (empty($hives)) {
            throw new \InvalidArgumentException('$hives cannot be empty.');
        }

        $sessionToken = (string) $this->getTokenValue();
        $tokenName = $this->getTokenName();
        $this->logger->debug('CSRF token: ' . implode(' | ', [
            $sessionToken,
            $tokenName,
            $this->fw->get('POST.' . $tokenName)
        ]));

        if ($tokenName && $sessionToken) {
            foreach ($hives as $hive) {
                if ($csrfTokenValue = trim((string) $this->fw->get($hive . '.' . $tokenName))) {
                    if (hash_equals($sessionToken, $csrfTokenValue)) {
                        return;
                    }
                }
            }

            $this->logger->notice('Missing or invalid CSRF token.');

            throw new CsrfException('Missing or invalid CSRF token. Please retry.');
        }
    }

    protected function getSession(): \OliveOil\Core\Model\Session\HttpInterface {
        /** @var \OliveOil\Core\Model\Session\HttpInterface $httpSession */
        $httpSession = $this->sessionManager->getSession($this->sessionName);

        return $httpSession;
    }
}
