<?php

namespace OliveOil\Core\Service\Security;


class Captcha
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory, protected \Gregwar\Captcha\CaptchaBuilder $captchaBuilder, protected array $builderDefaultOptions = [], protected array $options = [])
    {
    }

    /**
     * @return \OliveOil\Core\Model\Security\Captcha
     */
    public function generate(array $builderOptions = []) {
        $builder = $this->getCaptchaBuilder();

        $builderOptions += $this->getBuilderDefaultOptions();
        $this->setBuilderOptions($builderOptions);

        $builder->build(
            $builderOptions['width'],
            $builderOptions['height'],
            $builderOptions['font'] ?? null,
            $builderOptions['fingerprint'] ?? null,
        );

        $captchaId = $this->getCaptchaId();

        /** @var \OliveOil\Core\Model\Security\Captcha $captcha */
        $captcha = $this->getCoreFactory()->make(\OliveOil\Core\Model\Security\Captcha::class);
        $captcha->setId($captchaId)
            ->setImageSrc($this->getImageSrc($captchaId, $builderOptions))
            ->setPhrase($builder->getPhrase())
            ->setImageWidth($builderOptions['width'])
            ->setImageHeight($builderOptions['height']);

        return $captcha;
    }

    /**
     * @param string $answer
     */
    public function validate(\OliveOil\Core\Model\Security\Captcha $captchaModel, $answer): bool {
        if (!is_string($answer)) {
            return false;
        }

        return strcasecmp($captchaModel->getPhrase(), trim($answer)) === 0;
    }

    /**
     * @return $this
     */
    protected function setBuilderOptions(array $options): static {
        foreach ($options as $option => $value) {
            $setterMethod = 'set' . ucfirst(\OliveOil\snakecase2camelcase($option));
            if (method_exists($this->captchaBuilder, $setterMethod)) {
                call_user_func_array(
                    [$this->captchaBuilder, $setterMethod],
                    is_array($value) ? $value : [$value]
                );
            }
        }

        return $this;
    }

    protected function getCaptchaId(): string {
        return sha1((string) $this->getCaptchaBuilder()->inline());
    }

    /**
     * @param string $captchaId
     * @return string
     */
    protected function getImageSrc($captchaId, array $builderOptions = []) {
        if ($this->options['use_inline_image'] ?? false) {
            return $this->getCaptchaBuilder()->inline($builderOptions['image_quality'] ?? 90);
        }
        else {
            $imagePath = $this->options['image_path'];
            $imageUrl = $this->options['image_url'];
            $filename = sprintf('%s.jpg', $captchaId);
            $filepath = sprintf('%s/%s', $imagePath, $filename);
            $this->getCaptchaBuilder()->save($filepath, $builderOptions['image_quality'] ?? 90);

            return sprintf('%s/%s', $imageUrl, $filename);
        }
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getCaptchaBuilder(): \Gregwar\Captcha\CaptchaBuilder {
        return $this->captchaBuilder;
    }

    public function getBuilderDefaultOptions(): array {
        return $this->builderDefaultOptions;
    }
}
