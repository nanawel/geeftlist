<?php

namespace OliveOil\Core\Service\Di;


use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Exception\RuntimeException;
use OliveOil\Core\Exception\SystemException;
use function OliveOil\get_classname;
use function OliveOil\get_namespace;

class Container extends \Prefab
{
    protected ?\DI\Container $container = null;

    protected ?\DI\ContainerBuilder $containerBuilder = null;

    protected \Base $fw;

    protected array $definitions = [];

    public function __construct() {
        $this->fw = \Base::instance();
    }

    /**
     * @template T
     * @param class-string<T> $name
     * @return T
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function get(string $name): mixed {
        return $this->getContainer()->get($name);
    }

    /**
     * @template T
     * @param class-string<T> $name
     * @param array $parameters
     * @return T
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function make(string $name, array $parameters = []): mixed {
        return $this->getContainer()->make($name, $parameters);
    }

    /**
     * @return \DI\Container
     */
    public function getContainer(): \DI\Container {
        if (! $this->container) {
            $this->container = $this->getContainerBuilder()->build();
        }

        return $this->container;
    }

    /**
     * @return \DI\ContainerBuilder
     */
    public function getContainerBuilder(): \DI\ContainerBuilder {
        if (! $this->containerBuilder) {
            $this->containerBuilder = new \DI\ContainerBuilder();
            $this->containerBuilder->useAnnotations(false);

            $useCompilation = (bool) $this->fw->get('CONTAINER_USE_COMPILED_FILE');
            $compiledDirPath = $this->fw->get('CONTAINER_COMPILED_DIR_PATH');
            $compiledDirPerms = $this->fw->get('CONTAINER_COMPILED_DIR_PERMS');
            $compiledClassName = $this->fw->get('CONTAINER_COMPILED_CLASS') ?: 'CompiledContainer';
            $clearCompiledFile = (bool) $this->fw->get('CONTAINER_CLEAR_COMPILED_FILE');

            $compiledFile = $compiledDirPath . DIRECTORY_SEPARATOR . $compiledClassName . '.php';

            if ($useCompilation && $compiledDirPath) {
                if (!is_dir($compiledDirPath)) {
                    if (!mkdir($compiledDirPath)) {
                        throw new FilesystemException('Could not create directory at ' . $compiledDirPath);
                        // Force permissive rights (needed for dev environment)
                    }

                    if (!chmod($compiledDirPath, $compiledDirPerms)) {
                        throw new FilesystemException(
                            'Could not change permissions on directory at ' . $compiledDirPath
                        );
                    }
                }

                // Clear compiled file (for development environment)
                if ($clearCompiledFile && file_exists($compiledFile)) {
                    unlink($compiledFile);
                }

                $this->containerBuilder->enableCompilation($compiledDirPath, $compiledClassName);
            }
        }

        return $this->containerBuilder;
    }

    public function reset(): static {
        unset($this->container, $this->containerBuilder);

        return $this;
    }

    /**
     * @param string|string[] $definitions
     * @throws FilesystemException
     */
    public function addDefinitionsAsFiles(string|array $definitions): static {
        if (is_array($definitions)) {
            foreach ($definitions as $definition) {
                $this->addDefinitionsAsFiles($definition);
            }
            return $this;
        }

        $files = glob($definitions);
        foreach ($files as $definition) {
            $this->getContainerBuilder()->addDefinitions($definition);
        }

        return $this;
    }

    /**
     * @throws SystemException
     */
    public function init(): void {
        $this->registerVirtualClassesAutoloader();
    }

    /**
     * @throws SystemException
     */
    protected function registerVirtualClassesAutoloader(): void {
        $result = spl_autoload_register(function (string $class): bool {
            if (!$this->container) {
                throw new RuntimeException(
                    "Cannot register virtual classes autoloader: DI container has not been initialized!\n"
                    . ('Provided class was: ' . $class)
                );
            }

            $ns = get_namespace($class);
            $cn = get_classname($class);
            if ($this->container->has($class)) {
                eval(<<<EOPHP
                    namespace {$ns};
                    class {$cn} {}
                EOPHP
                );
                return true;
            }

            return false;
        });

        if (!$result) {
            throw new SystemException('Could not initialize DI container autoloader.');
        }
    }
}
