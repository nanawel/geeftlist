<?php

namespace OliveOil\Core\Service;

use OliveOil\Core\Exception\RegistryException;


/**
 * Class Registry
 *
 * Wrapper for F3 \Registry that can be extended to use a non-static storage.
 */
interface RegistryInterface
{
    /**
     * Return TRUE if object exists in registry
     *
     * @param string $key
     * @return bool
     */
    public function exists($key);

    /**
     * Add object to registry
     *
     * @param string $key
     * @param bool $overwrite
     * @return $this
     * @throws RegistryException
     */
    public function set($key, mixed $value, $overwrite = true);

    /**
     * Retrieve object from registry
     *
     * @param string $key
     * @return mixed|null
     */
    public function get($key);

    /**
     * Delete object from registry
     *
     * @param string $key
     * @return $this
     */
    public function clear($key);
}
