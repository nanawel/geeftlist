<?php

namespace OliveOil\Core\Service\Url;

interface BuilderInterface
{
    public const QUERY_PARAM_REFERER = 'referer';

    public const SYS_PARAM_REFERER   = '_referer';

    /**
     * Return the base URL
     *
     * @return string
     */
    public function getBaseUrl();

    /**
     * Return the full base URL (with area prefix like /api , etc.)
     *
     * @return string
     */
    public function getFullBaseUrl();

    /**
     * @return array [host, port]
     */
    public function getHostPort();

    /**
     * System params:
     *   _csrf          : true to add CSRF token to query
     *   _current       : true to return current URL
     *   _force_scheme  : deprecated
     *   _fragment      : fragment to add to  URL
     *   _referer       : add referer as query param
     *                    true: generate referer based on current URL
     *                    "_keep": keep current referer param if any
     *                    <url>: use this URL as referer
     *  _path           : path overriding $path if present
     *  _query          : raw query, "*" to keep existing
     *
     * @param string $path
     * @return string
     */
    public function getPath($path, array $params = []);

    /**
     * @return string|null URL-encoded referer if any
     */
    public function buildEncodedRefererFromParams(array $sysParams = []);

    /**
     * System params:
     *   _csrf          : true to add CSRF token to query
     *   _current       : true to return current URL
     *   _force_scheme  : deprecated
     *   _fragment      : fragment to add to  URL
     *   _referer       : add referer as query param
     *                    true: generate referer based on current URL
     *                    "_keep": keep current referer param if any
     *                    <url>: use this URL as referer
     *  _path           : path overriding $path if present
     *  _query          : raw query, "*" to keep existing
     *
     * @param string $path
     * @return string
     */
    public function getUrl($path, array $params = []);

    /**
     * @return string|null
     */
    public function getReferer();

    public function getQueryReferer();

    /**
     * @param string|null $param
     * @return array|mixed|null
     */
    public function getRequestQuery($param = null);
}
