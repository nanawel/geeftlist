<?php

namespace OliveOil\Core\Service\Url;

use OliveOil\Core\Exception\Url\InvalidRedirectUrlException;

interface RedirectInterface
{
    /**
     * Convert parameters $path and $params to a full redirect-URL
     *
     * @param string $path
     * @param array $params
     * @return string
     */
    public function getUrlForRedirect($path, $params = []);

    /**
     * Convert an incoming redirect target $to to a full URL
     *
     * @param string $to
     * @return string
     */
    public function getTargetRedirectUrl($to);

    /**
     * Convert a full URL to a redirect-URL
     *
     * @param string $url
     * @throws InvalidRedirectUrlException
     */
    public function toRedirectUrl($url);
}
