<?php

namespace OliveOil\Core\Service\Url;

class Builder implements BuilderInterface
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    protected string $baseUrl;

    protected string $areaPath;

    /** @var array [host, port] */
    protected $hostPort;

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\RegistryInterface $registry,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\RouteInterface $routeService,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\Log $logService,
        string $baseUrl,
        string $areaPath = '/'
    ) {
        $this->logger = $logService->getLoggerForClass($this);
        $this->baseUrl = static::cleanupUrlPart($baseUrl);
        $this->areaPath = static::cleanupUrlPart($areaPath) . '/';
    }

    /**
     * Return the full base URL (with area prefix like /api , etc.)
     */
    public function getFullBaseUrl(): string {
        return $this->getBaseUrl() . $this->getAreaPath();
    }

    /**
     * @return array [host, port]
     */
    public function getHostPort() {
        if (! $this->hostPort) {
            $this->hostPort = [];
            $urlParts = parse_url($this->getBaseUrl());
            $this->hostPort[] = $urlParts['host'];
            if (isset($urlParts['port']) && $urlParts['port'] !== 0) {
                $this->hostPort[] = $urlParts['port'];
            }
            else {
                $this->hostPort[] = ($urlParts['scheme'] === 'https' ? 443 : 80);
            }
        }

        return $this->hostPort;
    }

    /**
     * System params:
     *   _csrf          : true to add CSRF token to query
     *   _current       : true to return current URL
     *   _force_scheme  : deprecated
     *   _fragment      : fragment to add to  URL
     *   _referer       : add referer as query param
     *                    true: generate referer based on current URL
     *                    "_keep": keep current referer param if any
     *                    <url>: use this URL as referer
     *  _path           : path overriding $path if present
     *  _query          : raw query, "*" to keep existing
     *
     * @param string $path
     */
    public function getPath($path, array $params = []): string {
        $path = trim((string) $path, '/');
        if (isset($params['_path'])) {
            $path = trim((string) $params['_path'], '/');
        }

        if ($params === null) {
            $params = [];
        }

        if (isset($params['_current']) && $params['_current'] === true) {
            $path = '*/*/*';
            $params['_query'] = '*';
        }

        $path = $this->autocompletePath($path);

        $sysParams = $this->extractSysParams($params);

        $query = isset($sysParams['_query']) ? $this->parseQuery($sysParams['_query']) : [];

        if ($encodedReferer = $this->buildEncodedRefererFromParams($sysParams)) {
            $query[self::QUERY_PARAM_REFERER] = $encodedReferer;
        }

        if (isset($sysParams['_csrf']) && ($csrfTokenName = $this->appConfig->getValue('CSRF_TOKEN'))) {
            $query[$csrfTokenName] = $this->sessionManager->getSession()->getCsrfToken();
        }

        $fragment = $sysParams['_fragment'] ?? '';

        $query = $this->filterQuery($query);
        $queryString = $this->unparseQuery($query, $sysParams);

        return static::cleanupUrlPart($path . '/' . $this->buildParamString($params))
            . ($queryString !== '' && $queryString !== '0' ? '?' . $queryString : '')
            . ($fragment ? '#' . $fragment : '');
    }

    /**
     * @return string|null URL-encoded referer if any
     */
    public function buildEncodedRefererFromParams(array $sysParams = []) {
        $referer = null;
        if (isset($sysParams[static::SYS_PARAM_REFERER])) {
            if (
                $sysParams[static::SYS_PARAM_REFERER] === '_keep'
                && $encodedReferer = $this->fw->get('GET.referer')
            ) {
                $referer = $encodedReferer;
            }
            else {
                $currentUrlParams = [
                    '_current' => true,
                    '_force_scheme' => true
                ];
                if (isset($sysParams['_refererFragment']) && $sysParams['_refererFragment'] !== '') {
                    $currentUrlParams['_fragment'] = $sysParams['_refererFragment'];
                }

                $referer = \OliveOil\base64_url_encode(
                    $sysParams[static::SYS_PARAM_REFERER] === true
                        ? $this->getUrl('', $currentUrlParams)
                        : $sysParams[static::SYS_PARAM_REFERER]
                );
            }
        }

        return $referer;
    }

    /**
     * @param string $path
     * @return string
     */
    public function autocompletePath(string $path): string {
        // Delegate path autocomplete to current controller if any
        $relativeController = $this->registry->get('current_controller');
        if ($relativeController && $relativeController instanceof \OliveOil\Core\Controller\AutocompleteAwareController) {
            $path = $relativeController->autocompleteUrlPath($path);
        }
        elseif ($routeArgs = $this->fw->get('PARAMS')) {
            $routeParts = $this->routeService->parseRouteArgs($routeArgs);

            $matches = null;
            $parts = explode('/', trim((string) $path, '/'));
            if (empty($parts[0])) {
                $parts[0] = 'index';
            }
            elseif ($parts[0] === '*') {
                $parts[0] = $routeParts['controller'];
            }

            if (!isset($parts[1])) {
                $parts[1] = 'index';
            }
            elseif (isset($parts[1]) && $parts[1] == '*') {
                $parts[1] = $routeParts['action'];
            }

            if (count($parts) == 3 && $parts[2] == '*') {
                $parts[2] = $routeArgs['*'] ?? '';
            }

            $path = implode('/', $parts);
        }

        return $path;
    }

    /**
     * System params:
     *   _csrf          : true to add CSRF token to query
     *   _current       : true to return current URL
     *   _force_scheme  : deprecated
     *   _fragment      : fragment to add to  URL
     *   _referer       : add referer as query param
     *                    true: generate referer based on current URL
     *                    "_keep": keep current referer param if any
     *                    <url>: use this URL as referer
     *  _path           : path overriding $path if present
     *  _query          : raw query, "*" to keep existing
     *
     * @param string $path
     */
    public function getUrl($path, array $params = []): string {
        return $this->getFullBaseUrl()
            . $this->getPath($path, $params);
    }

    /**
     * @return mixed[]
     */
    protected function extractSysParams(array &$params = []): array {
        $sysParams = array_filter(
            $params,
            static fn($k): bool => str_starts_with((string) $k, '_'),
            ARRAY_FILTER_USE_KEY
        );
        $params = array_diff_key($params, $sysParams);

        return $sysParams;
    }

    protected function buildParamString(array $params = []): string {
        $paramString = [];
        foreach ($params as $k => $v) {
            $paramString[] = urlencode((string) $k) . '/' . urlencode((string) $v);
        }

        return implode('/', $paramString);
    }

    /**
     * @param string|array $query
     * @return array
     */
    protected function parseQuery($query): ?array {
        if ($query === '*') {
            $query = $this->fw->get('GET') ?: [];
        }
        elseif (! is_array($query)) {
            $query = [];
        }

        return array_filter(
            $query,
            static fn($v, $k): bool => (! is_string($v))
                || (is_string($v) && trim($v) !== '' && is_string($k) && trim($k) !== ''),
            ARRAY_FILTER_USE_BOTH
        );
    }

    protected function unparseQuery(array $query, array $sysParams): string {
        $separator = $sysParams['_query_arg_separator'] ?? ini_get('arg_separator.output');
        $encType = $sysParams['_query_enc_type'] ?? PHP_QUERY_RFC1738;

        return http_build_query($query, '', $separator, $encType);
    }

    /**
     * @return string|null
     */
    public function getReferer() {
        $referer = $this->fw->get('SERVER.HTTP_REFERER');
        if ($referer && $referer != $this->baseUrl) {
            return $referer;
        }

        return null;
    }

    /**
     * @param array|null $query
     */
    public function getQueryReferer(array $query = null): ?string {
        if ($query === null) {
            $query = $this->getRequestQuery();
        }

        if ($referer = ($query[static::QUERY_PARAM_REFERER] ?? false)) {
            return \OliveOil\base64_url_decode($referer);
        }

        return null;
    }

    /**
     * @param string $referer
     */
    public function addQueryReferer(array $query, $referer): array {
        if ($referer) {
            $query[static::QUERY_PARAM_REFERER] = \OliveOil\base64_url_encode($referer);
        }

        return $query;
    }

    protected function filterQuery(array $query): array {
        // Only allow referer to own URL
        if (($referer = $this->getQueryReferer($query)) && !str_starts_with($referer, $this->getBaseUrl())) {
            unset($query[static::QUERY_PARAM_REFERER]);
        }

        return $query;
    }

    /**
     * @param string|null $param
     * @return array|mixed|null
     */
    public function getRequestQuery($param = null) {
        $query = $this->fw->get('GET');
        if ($param === null) {
            return $query;
        }

        return $query[$param] ?? null;
    }

    public static function cleanupUrlPart($part): string {
        return rtrim((string) $part, '/ ');
    }

    public function getBaseUrl(): string {
        return $this->baseUrl;
    }

    public function getAreaPath(): string {
        return $this->areaPath;
    }
}
