<?php

namespace OliveOil\Core\Service\Url;


use OliveOil\Core\Exception\Url\InvalidRedirectUrlException;

class Redirect implements RedirectInterface
{
    public const REDIRECT_CONTROLLER = 'redirect/index';

    public function __construct(protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder)
    {
    }

    /**
     * "my/path" + [key => value, _fragment => fragment]
     *    => http://.../redirect/index/to/{BASE64:my/path?key=value#fragment}
     *
     * @param string $path
     * @param array $params
     * @throws InvalidRedirectUrlException
     */
    public function getUrlForRedirect($path, $params = []): string {
        return $this->getUrlBuilder()->getFullBaseUrl() . $this->getPathForRedirect($path, $params);
    }

    /**
     * http://.../my/path?key=value#fragment
     *    => http://.../redirect/index/to/{BASE64:my/path?key=value#fragment}
     *
     * @param string $url
     * @throws InvalidRedirectUrlException
     */
    public function toRedirectUrl($url): string {
        $baseUrl = $this->getUrlBuilder()->getFullBaseUrl();
        if (!str_starts_with($url, $baseUrl)) {
            throw new InvalidRedirectUrlException('Invalid URL for redirect: ' . $url);
        }

        $parameterizedPath = substr($url, strlen($this->getUrlBuilder()->getFullBaseUrl()));

        return $baseUrl . $this->parameterizedPathToRedirectPath($parameterizedPath);
    }

    /**
     * {BASE64:my/path?key=value#fragment}
     *    => http://.../my/path?key=value#fragment
     *
     * @param string $to
     */
    public function getTargetRedirectUrl($to): string {
        return $this->getUrlBuilder()->getFullBaseUrl() . \OliveOil\base64_url_decode($to);
    }

    /**
     * "my/path" + [key => value, _fragment => fragment]
     *     => redirect/index/to/{BASE64:my/path?key=value#fragment}
     *
     * @protected Only public for UT
     *
     * @param string $path
     * @return string
     */
    public function getPathForRedirect($path, $params = []) {
        return $this->parameterizedPathToRedirectPath($this->getUrlBuilder()->getPath($path, $params));
    }

    /**
     * my/path?key=value#fragment
     *    => http://.../redirect/index/to/{BASE64:my/path?key=value#fragment}
     *
     * @protected Only public for UT
     *
     * @return string
     */
    public function parameterizedPathToRedirectPath(string $parameterizedPath) {
        return $this->getUrlBuilder()->getPath(
            static::REDIRECT_CONTROLLER,
            ['to' => \OliveOil\base64_url_encode($parameterizedPath)]
        );
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }
}
