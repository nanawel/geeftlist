<?php

namespace OliveOil\Core\Service;

interface EventInterface
{
    public const EVENT_SPACE_DEFAULT = 'default';

    public const EVENT_SPACE_WILDCARD = '*';

    /**
     * @param object $target
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager($target);

    /**
     * @return \Laminas\EventManager\SharedEventManager
     */
    public function getSharedEventManager();

    /**
     * @return $this
     */
    public function setupListeners();

    /**
     * @param string|null $eventSpace
     * @return $this
     */
    public function enableListeners($eventSpace = null);

    /**
     * @param string|null $eventSpace
     * @return $this
     */
    public function disableListeners($eventSpace = null);

    /**
     * @param string|null $eventSpace
     * @return bool
     */
    public function areListenersDisabled($eventSpace = null);

    /**
     * @return string
     */
    public function getEventSpace();

    /**
     * @param string|null $eventSpace
     * @return bool
     */
    public function getListenersStatus($eventSpace = null);

    /**
     * @return bool[]
     */
    public function getAllListenersStatus();

    /**
     * @param string|array|null $eventSpace
     * @param bool|null $enabled
     * @return $this
     */
    public function setListenersStatus($eventSpace, $enabled = null);
}
