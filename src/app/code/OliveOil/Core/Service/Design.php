<?php

namespace OliveOil\Core\Service;

use OliveOil\Core\Exception\SystemException;

class Design implements DesignInterface
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var array */
    protected $themes;

    /**
     * @param string $assetDistBasePath
     * @param string $assetSrcBasePath
     * @param string $assetUrlBasePath
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\Design\Theme\BootstrapInterface $themeBootstrap,
        \OliveOil\Core\Service\Log $logService,
        protected $assetDistBasePath,
        protected $assetSrcBasePath,
        protected $assetUrlBasePath
    ) {
        $this->logger = $logService->getLoggerForClass($this);

        $this->init();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function init(): static {
        if (!$this->assetUrlBasePath) {
            throw new SystemException('Empty or unspecified themes directory.');
        }

        $this->appendThemesToUi();

        return $this;
    }

    protected function appendThemesToUi(): static {
        if (!$this->getFw()->get('UI_ORIG')) {
            $ui = $this->getFw()->get('UI');
            $this->getFw()->set('UI_ORIG', $ui);
        }
        else {
            $ui = $this->getFw()->get('UI_ORIG');
        }

        $uiPaths = explode(';', (string) $ui);
        $newUiPaths = [];
        foreach ($uiPaths as $uiPath) {
            $newUiPaths[] = $uiPath . $this->getTheme() . DIRECTORY_SEPARATOR;
            $newUiPaths[] = $uiPath . self::DEFAULT_THEME . DIRECTORY_SEPARATOR;
        }

        $this->getFw()->set('UI', implode(';', $newUiPaths));

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bootstrapTheme(): static {
        $this->getThemeBootstrap()->bootstrap($this);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function resolveTemplateFile($file, array $subdirs = [], $returnFullPath = false): ?string {
        if ($subdirs === []) {
            $subdirs[] = '';
        }

        foreach ($subdirs as &$sd) {
            $sd = trim((string) $sd, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        }

        foreach ($this->getFw()->split($this->getFw()->get('UI') . ';./') as $dir) {
            foreach ($subdirs as $subdir) {
                $relativePath = $subdir . $file;
                if (is_file($this->getFw()->fixslashes($dir . $relativePath))) {
                    if ($returnFullPath) {
                        return $dir . $relativePath;
                    }

                    return $relativePath;
                }
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getFaviconUrl() {
        return $this->getImageUrl($this->getAppConfig()->getValue('FAVICON'));
    }

    /**
     * @inheritDoc
     */
    public function setTheme($theme): static {
        $this->getAppConfig()->setValue('THEME', $theme);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTheme() {
        return $this->getAppConfig()->getValue('THEME');
    }

    /**
     * @inheritDoc
     */
    public function getAvailableThemes() {
        if (!$this->themes) {
            $this->themes = $this->collectAvailableThemes();
        }

        return $this->themes;
    }

    /**
     * @return string[]
     */
    protected function collectAvailableThemes(): array {
        $uiDirs = $this->getFw()->get('UI_ORIG') ?: $this->getFw()->get('UI');
        $themes = [];
        foreach (explode(';', (string) $uiDirs) as $uiDir) {
            $directoryIterator = new \DirectoryIterator($uiDir);
            foreach ($directoryIterator as $file) {
                if ($file->isDot() || str_ends_with($file, '-src')) {
                    continue;
                }

                if ($file->isDir()) {
                    $themes[] = $file->getFilename();
                }
            }
        }

        return array_unique($themes);
    }

    /**
     * @inheritDoc
     */
    public function getAssetDistPath($path, $theme = null): string {
        return $this->assetUrlBasePath
            . ($theme ?? $this->getTheme()) . DIRECTORY_SEPARATOR
            . trim($path, DIRECTORY_SEPARATOR);
    }

    /**
     * @inheritDoc
     */
    public function getAssetSrcPath($path, $theme = null): string {
        return $this->assetSrcBasePath
            . ($theme ?? $this->getTheme()) . DIRECTORY_SEPARATOR
            . trim($path, DIRECTORY_SEPARATOR);
    }

    /**
     * @inheritDoc
     */
    public function getAssetUrl($path, $params = [], $fallbackDefault = true) {
        if (str_contains('//', $path)) {    // Full URL (for externals; handles "same protocol as current page" syntax)
            return $path;
        }

        $theme = $this->getTheme();
        if (!file_exists($this->getAssetDistPath($path, $theme)) && $fallbackDefault) {
            $theme = self::DEFAULT_THEME;
        }

        $themedPath = $this->getAssetDistPath($path, $theme);

        if (! isset($params['_skip_scheme'])) {
            $params['_skip_scheme'] = true;
        }

        if (
            (! isset($params['_skip_avid']) || ! $params['_skip_avid'])
            && ($assetsVersionId = $this->getAppConfig()->getValue('ASSETS_VERSION_ID'))
        ) {
            $params['_query'] = ['avid' => $assetsVersionId];
        }

        return $this->getUrlBuilder()->getUrl($themedPath, $params);
    }

    /**
     * @inheritDoc
     */
    public function getCssUrl($path, $params = []) {
        return $this->getAssetUrl('css/' . $path, $params);
    }

    /**
     * @inheritDoc
     */
    public function getJsUrl($path, $params = []) {
        return $this->getAssetUrl('js/' . $path, $params);
    }

    /**
     * @inheritDoc
     */
    public function getImageUrl($path, $params = []) {
        return $this->getAssetUrl('img/' . $path, $params);
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getThemeBootstrap(): \OliveOil\Core\Service\Design\Theme\BootstrapInterface {
        return $this->themeBootstrap;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }
}
