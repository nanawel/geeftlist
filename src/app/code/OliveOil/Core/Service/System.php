<?php

namespace OliveOil\Core\Service;


/**
 * Class System
 */
class System
{
    public function __construct(protected \OliveOil\Core\Model\App\ConfigInterface $appConfig)
    {
    }

    public function startMaintenance(): bool
    {
        return (bool) file_put_contents($this->appConfig->getValue('MAINTENANCE_FLAG'), date('c'));
    }

    public function endMaintenance(): bool
    {
        return unlink($this->appConfig->getValue('MAINTENANCE_FLAG'));
    }
}
