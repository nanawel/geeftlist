<?php

namespace OliveOil\Core\Service;

use OliveOil\Core\Model\MagicObject;
use OliveOil\Core\Model\Traits\I18nTrait;

/**
 * @property \Base $fw
 */
class View extends \View
{
    use I18nTrait;

    public const RENDERING_ERROR_MARKER = '[[oo-block-rendering-error]]';

    public const RENDERING_ERROR_BLOCK_CLASS = 'oo-block-rendering-error';

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \OliveOil\Core\Model\View\LayoutInterface $layout;

    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \Psr\Log\LoggerInterface $logger;

    protected array $data = [];

    public function __construct(
        \OliveOil\Core\Service\View\Context $context
    ) {
        $this->fw = $context->getFw();
        $this->appConfig = $context->getAppConfig();
        $this->i18n = $context->getI18n();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->layout = $context->getLayout();
        $this->coreFactory = $context->getCoreFactory();
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->logger = $context->getLogService()->getLoggerForClass($this);
        parent::__construct();
    }

    public function getUrl(string $path, array $params = []): string {
        $url = $this->getUrlBuilder()->getUrl($path, $params);
        if (isset($params['_no_escape']) && $params['_no_escape']) {
            return $url;
        }

        return $this->escapeHtml($url);
    }


    public function getLayout(): \OliveOil\Core\Model\View\LayoutInterface {
        return $this->layout;
    }

    public function renderPage(array $hive = null, ?int $ttl = 0): string {
        return $this->getLayout()
            ->getPageBlock()->render($hive, $ttl);
    }

    /**
     * @param string $blockName
     * @param string $mime
     * @param int $ttl
     * @return string
     */
    public function renderBlock(
        string $blockName,
        ?string $mime = 'text/html',
        ?array $hive = null,
        ?int $ttl = 0
    ): string {
        $uniqid = uniqid('block_', false);

        $this->getEventManager()->trigger('renderBlock::before', $this, [
            'block_name' => $blockName,
            'uniqid' => $uniqid
        ]);

        $output = $this->doRenderBlock($blockName, $mime, $hive, $ttl);

        $this->getEventManager()->trigger('renderBlock::after', $this, [
            'block_name' => $blockName,
            'uniqid' => $uniqid,
            'output' => $output
        ]);

        return $output;
    }

    public function doRenderBlock(
        string $blockName,
        ?string $mime = 'text/html',
        ?array $hive = null,
        ?int $ttl = 0
    ): string {
        $block = $this->getLayout()->getBlock($blockName);

        $output = '';
        if (empty($block)) {
            return $output;
        }

        $obLevelBeforeRender = ob_get_level();
        try {
            $this->getEventManager()->trigger(
                ['doRenderBlock::before', sprintf('doRenderBlock::%s::before', $blockName)],
                $this,
                ['block' => $block]
            );

            if ($block->getConfig('enabled', true)) {
                $output = $block->render($hive, $ttl);
            }
        }
        catch (\Throwable $throwable) {
            $this->getLogger()->critical(
                sprintf("Error rendering block '%s': %s", $blockName, $throwable->getMessage()),
                ['exception' => $throwable]
            );
            if ($this->getAppConfig()->getValue('DEV.VIEW_BLOCK_THROW_EXCEPTION')) {
                throw $throwable;
            }

            // #563 Discard incomplete buffers before returning (see also #516)
            while (ob_get_level() > $obLevelBeforeRender) {
                /** See \View::sandbox() */
                ob_end_clean();
            }

            if ($mime === null || $mime === 'text/html') {
                $output = sprintf(
                    '<!-- %s --><div class="%s">%s</div>',
                    self::RENDERING_ERROR_MARKER,
                    self::RENDERING_ERROR_BLOCK_CLASS,
                    $this->__("Sorry, an unexpected error happened and this block could not be displayed :(")
                );
            }
        }

        $transportObject = new MagicObject([
            'block' => $block,
            'output' => $output
        ]);
        $this->getEventManager()->trigger(
            ['doRenderBlock::after', sprintf('doRenderBlock::%s::after', $blockName)],
            $this,
            ['transport_object' => $transportObject]
        );

        return $transportObject->getOutput();
    }

    public function getData(?string $key = null) {
        if ($key === null) {
            return $this->data;
        }

        return $this->data[$key] ?? null;
    }

    public function setData(mixed $key, mixed $value): static {
        if ($key === null) {
            if (!is_array($value)) {
                throw new \InvalidArgumentException('$value must be a valid array if $key is null.');
            }
            $this->data = $value;
        }
        else {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function hasData($key): bool {
        return isset($this->data[$key]);
    }

    public function clearData(): static {
        $this->data = [];

        return $this;
    }

    public function escapeHtml($string) {
        return $this->esc($string);
    }

    public function jsQuoteEscape($string, $quote = ["'"]) {
        return $this->getOutputHelper()->jsQuoteEscape($string, $quote);
    }

    public function inlineJsQuoteEscape($string, $quote = ["'", '"']) {
        return $this->getOutputHelper()->jsQuoteEscape($string, $quote);
    }

    public function truncateText($text, $maxLength, string $ellipsis = '...', $keepFullWords = true): string {
        return $this->getOutputHelper()->truncateText($text, $maxLength, $ellipsis, $keepFullWords);
    }

    /**
     * @return \OliveOil\Core\Helper\Output
     */
    public function getOutputHelper() {
        return $this->getCoreFactory()->get(\OliveOil\Core\Helper\Output::class);
    }

    /**
     * @return \Base
     */
    public function getFw() {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getEventManager(): \OliveOil\Core\Model\Event\Manager {
        return $this->eventManager;
    }

    public function getLogger(): \Psr\Log\LoggerInterface {
        return $this->logger;
    }
}
