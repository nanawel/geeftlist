<?php

namespace OliveOil\Core\Service;


use OliveOil\Core\Exception\Di\Exception;

/**
 * @template T of object
 * @implements \OliveOil\Core\Service\GenericFactoryInterface<T>
 */
class GenericFactory implements GenericFactoryInterface
{
    /**
     * @param \DI\Container $container
     * @param string|string[] $namespaces Namespace(s) to resolve relative classnames against
     * @param string[] $codeClassArray
     * @param bool $useResolveFromCodeFallback
     * @param string $classSuffix
     */
    public function __construct(
        protected \DI\Container $container,
        protected string|array $namespaces = [],
        protected array $codeClassArray = [],
        protected bool $useResolveFromCodeFallback = true,
        protected string $classSuffix = ''
    ) {
        if (is_string($this->namespaces)) {
            $this->namespaces = [$this->namespaces];
        }
        if (empty($this->namespaces)) {
            $this->namespaces = [\OliveOil\get_namespace($this)];
        }
        $this->namespaces = $this->cleanupName($this->namespaces);
        $this->addCodeClassMapping($codeClassArray);
    }

    /**
     * @inheritDoc
     */
    public function get($fqcn, array $parameters = []) {
        if (!is_string($fqcn)) {
            throw new \InvalidArgumentException('$fqcn must be a string.');
        }

        try {
            return $this->container->get($fqcn . $this->classSuffix);
        }
        catch (\Throwable $throwable) {
            throw new Exception(sprintf(
                'Could not get instance of %s: %s',
                $fqcn,
                $throwable->getMessage()
            ), previous: $throwable);
        }
    }

    /**
     * @inheritDoc
     */
    public function make($fqcn, array $parameters = []) {
        if (!is_string($fqcn)) {
            throw new \InvalidArgumentException('$fqcn must be a string.');
        }

        try {
            return $this->container->make($fqcn . $this->classSuffix, $parameters);
        }
        catch (\Throwable $throwable) {
            throw new Exception(sprintf('Could not create instance of %s.', $fqcn), previous: $throwable);
        }
    }

    /**
     * @inheritDoc
     */
    public function resolveGet($rcn) {
        return $this->resolveCall($rcn, 'get');
    }

    /**
     * @inheritDoc
     */
    public function resolveMake($rcn, array $parameters = []) {
        return $this->resolveCall($rcn, 'make', $parameters);
    }

    /**
     * @inheritDoc
     */
    public function getFromCode($code) {
        if (!is_string($code)) {
            throw new \InvalidArgumentException('$code must be a string.');
        }

        try {
            if ($class = $this->getClassFromCode($code)) {
                return $this->get($class);
            }

            if ($this->useResolveFromCodeFallback) {
                return $this->resolveGet($code);
            }
        }
        catch (\Throwable $throwable) {
            throw new Exception(sprintf('Could not get instance with code %s.', $code), previous: $throwable);
        }

        throw new Exception('Invalid code: ' . $code);
    }

    /**
     * @inheritDoc
     */
    public function makeFromCode($code, array $parameters = []) {
        if (!is_string($code)) {
            throw new \InvalidArgumentException('$code must be a string.');
        }

        try {
            if ($class = $this->getClassFromCode($code)) {
                return $this->make($class, $parameters);
            }

            if ($this->useResolveFromCodeFallback) {
                return $this->resolveMake($code, $parameters);
            }
        }
        catch (\Throwable $throwable) {
            throw new Exception(sprintf('Could not create instance with code %s.', $code), previous: $throwable);
        }

        throw new Exception('Invalid code: ' . $code);
    }

    /**
     * @inheritDoc
     */
    public function resolve($rcn, $namespace = null) {
        if (!is_string($rcn)) {
            throw new \InvalidArgumentException('$rcn must be a string.');
        }

        foreach (($namespace ? [$namespace] : $this->namespaces) as $ns) {
            $parts = array_map('ucfirst', explode('\\', str_replace('/', '\\', $rcn)));

            $className = $ns . '\\' . implode('\\', $parts);
            if (
                class_exists($className . $this->classSuffix)
                || $this->isDiDefinedEntry($className . $this->classSuffix)
            ) {
                return $className;
            }
        }

        throw new Exception(sprintf(
            "Could not resolve %s in namespace(s) {%s}%s",
            $rcn,
            implode(', ', $this->namespaces),
            $this->classSuffix !== '' && $this->classSuffix !== '0'
                ? sprintf(" using suffix '%s'", $this->classSuffix)
                : ''
        ));
    }

    /**
     * @inheritDoc
     */
    public function addCodeClassMapping($code, $class = null): static {
        if (! is_array($code)) {
            $code = [$code => $class];
        }

        $this->codeClassArray = array_merge(
            $this->codeClassArray,
            $this->cleanupName($code)
        );

        return $this;
    }

    /**
     * @param string $rcn
     * @param string $method
     * @return object
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function resolveCall($rcn, $method, array $parameters = []) {
        if ($this->namespaces === []) {
            throw new Exception(sprintf('Cannot resolve %s: no namespace has been set.', $rcn));
        }

        return call_user_func_array([$this, $method], [
            'fqcn' => $this->resolve($rcn),
            'parameters' => $parameters
        ]);
    }

    /**
     * @param string $code
     * @return false|string
     */
    protected function getClassFromCode($code) {
        if (isset($this->codeClassArray[$code])) {
            return $this->codeClassArray[$code];
        }

        return $this->codeClassArray['*'] ?? false;
    }

    /**
     * @param string $className
     */
    protected function isDiDefinedEntry($className): bool {
        static $entries;
        if (!$entries) {
            $entries = $this->container->getKnownEntryNames();
        }

        return in_array($className, $entries);
    }

    /**
     * @param array|string $name
     */
    protected function cleanupName($name): array {
        if (! is_array($name)) {
            $name = [$name];
        }

        return array_map(static fn($class): string => trim((string) $class, '\\'), $name);
    }
}
