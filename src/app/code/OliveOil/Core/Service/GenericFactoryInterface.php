<?php

namespace OliveOil\Core\Service;

/**
 * @template T of object
 */
interface GenericFactoryInterface
{
    /**
     * @param class-string<T> $fqcn Fully Qualified Class Name
     * @return T
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function get($fqcn);

    /**
     * @param class-string<T> $fqcn Fully Qualified Class Name
     * @return T
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function make($fqcn, array $parameters = []);

    /**
     * @param string $rcn Relative Class Name
     * @return T
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function resolveGet($rcn);

    /**
     * @param string $rcn Relative Class Name
     * @return T
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function resolveMake($rcn, array $parameters = []);

    /**
     * @param string $code Context-dependent class code
     * @return T
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function getFromCode($code);

    /**
     * @param string $code Context-dependent class code
     * @return T
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function makeFromCode($code, array $parameters = []);

    /**
     * @param string $rcn Relative Class Name
     * @param string $namespace
     * @return class-string
     */
    public function resolve($rcn, $namespace = null);

    /**
     * @param string|array $code
     * @param string|null $class
     */
    public function addCodeClassMapping($code, $class = null);
}
