<?php

namespace OliveOil\Core\Service;


use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class Log
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\Log\ResolverInterface $resolver
    ) {
    }

    public function getLogger(string $identifier, array $params = []): LoggerInterface {
        return $this->resolver->getLoggerByIdentifier($identifier, $params);
    }

    public function getLoggerByName(string $name, array $params = []): LoggerInterface {
        return $this->resolver->getLoggerByName($name, $params);
    }

    public function getLoggerForClass(string|object $class): LoggerInterface {
        if (is_object($class)) {
            $class = $class::class;
        }

        return $this->getLogger($this->getLoggerIdentifierForClass($class));
    }

    protected function getLoggerIdentifierForClass(string $class): string {
        return trim(str_replace('\\', '.', $class), '\\');
    }

    public function reset(): void {
        $this->resolver->reset();
    }

    /**
     * DEBUG (F3) => LOG_LEVEL (Monolog) values conversion method
     */
    public static function debugToLogLevel(int $debugValue): string {
        return match ($debugValue) {
            0 => LogLevel::ERROR,
            1 => LogLevel::NOTICE,
            2 => LogLevel::INFO,
            default => LogLevel::DEBUG,
        };
    }
}
