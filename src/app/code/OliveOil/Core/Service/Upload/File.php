<?php

namespace OliveOil\Core\Service\Upload;


use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Exception\UploadException;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;
use OliveOil\Core\Service\UploadInterface;
use Sirius\Upload\Handler;

class File implements UploadInterface
{
    public const SUPPORTED_URL_SCHEMES = [
        'http',
        'https',
    ];

    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var bool */
    protected $acceptIncomingExtension;

    protected array $defaultOptions = [
        self::OPTION_ACCEPT_INCOMING_EXT => true
    ];

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreModelFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Log $logService,
        protected \Sirius\Validation\ValueValidator $validator,
        $defaultOptions = null
    ) {
        $this->eventManager = $eventService->getEventManager($this);
        $this->logger = $logService->getLoggerForClass($this);

        if (is_array($defaultOptions)) {
            $this->defaultOptions = array_merge($this->defaultOptions, $defaultOptions);
        }
    }

    /**
     * @param string $inputName
     * @param string $saveDir
     * @param array $options
     * @return \OliveOil\Core\Model\Upload\File|null
     * @throws UploadException
     */
    public function upload($inputName, $saveDir, ErrorAggregatorInterface $errorAggregator, $options = []) {
        try {
            $uploadHandler = $this->newUploadHandler($saveDir);
        }
        catch (\Throwable $throwable) {
            $this->getLogger()->error('Could not initialize upload handler: ' . $throwable->getMessage(), ['exception' => $throwable]);
            $errorAggregator->addFieldError(
                $this->getCoreModelFactory()->make(ErrorInterface::class)
                    ->setField($inputName)
                    ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                    ->setLocalizableMessage('Unknown error on upload. Please try again later.')
                    ->setMessage('Unknown error on upload. Please try again later.')
            );

            throw new UploadException('Could not initialize upload handler.', 0, $throwable);
        }

        try {
            // HTML input or URL?
            $urlParts = parse_url($url = (string) $this->getFw()->get('POST.' . $inputName));
            if (
                is_array($urlParts)
                && !empty($urlParts['scheme'])
                && in_array($urlParts['scheme'], static::SUPPORTED_URL_SCHEMES)
            ) {
                $file = $this->buildFileFromUrl($inputName, $url, $errorAggregator, $options);
            }
            else {
                $file = $this->buildFileFromPost($inputName, $errorAggregator, $options);
            }
        }
        catch (\Throwable $throwable) {
            throw new UploadException('Could not upload specified file.', 0, $throwable);
        }

        try {
            $this->prepareFile($file, $options);
            $this->beforeProcessFile($file, $options);

            $fileObject = \OliveOil\Core\Model\Upload\File::fromSiriusFile($uploadHandler->process($file));
            if (!$fileObject->getData('name') || !$fileObject->getData('size')) {
                $message = 'No file specified.';
                $errorAggregator->addFieldError(
                    $this->getCoreModelFactory()->make(ErrorInterface::class)
                        ->setField($inputName)
                        ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                        ->setLocalizableMessage($message)
                        ->setMessage($message)
                );

                // Don't throw exception, input file might just have been left blank
                return null;
            }

            $fileObject->setData(
                'final_path',
                sprintf('%s/%s', $saveDir, basename((string) $fileObject->getData('name')))
            );

            $this->afterProcessFile($fileObject, $options);

            if (! $fileObject->isValid()) {
                foreach ($fileObject->getMessages() as $message) {
                    $errorAggregator->addFieldError(
                        $this->getCoreModelFactory()->make(ErrorInterface::class)
                            ->setField($inputName)
                            ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage((string) $message)
                            ->setMessage((string) $message)
                    );
                }

                throw new UploadException('File validation failed.');
            }

            return $fileObject;
        }
        catch (\Throwable $throwable) {
            if ($throwable instanceof UploadException) {
                throw $throwable;
            }

            $this->getLogger()->error('Unknown error on upload: ' . $throwable->getMessage(), ['exception' => $throwable]);
            $errorAggregator->addFieldError(
                $this->getCoreModelFactory()->make(ErrorInterface::class)
                    ->setField($inputName)
                    ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                    ->setLocalizableMessage('Unknown error on upload. Please try again later.')
                    ->setMessage('Unknown error on upload. Please try again later.')
            );

            throw new UploadException('An unknown error occured on upload.', 0, $throwable);
        }
    }

    /**
     * @throws FilesystemException
     * @throws UploadException
     */
    protected function buildFileFromUrl($inputName, string $url, ErrorAggregatorInterface $errorAggregator, $options = []): array {
        $urlParts = parse_url($url);

        $file = [
            'name' => pathinfo($urlParts['path'] ?? 'empty-path', PATHINFO_BASENAME),
            'tmp_name' => tempnam(sys_get_temp_dir(), 'upload-tmp-')
        ];

        if (!$file['tmp_name']) {
            throw new FilesystemException('Cannot get temporary file.');
        }

        if (!$remoteFileHandle = fopen($url, 'r')) {
            $errorAggregator->addFieldError(
                $this->getCoreModelFactory()->make(ErrorInterface::class)
                    ->setField($inputName)
                    ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                    ->setLocalizableMessage('Cannot retrieve file from specified remote URL.')
                    ->setMessage('Cannot retrieve file from specified remote URL.')
            );

            throw new UploadException('Cannot open remote file handler on URL: ' . $url);
        }

        if (!$tmpFileHandle = fopen($file['tmp_name'], 'w')) {
            throw new FilesystemException('Cannot open temporary file for write.');
        }

        if (($file['size'] = fwrite($tmpFileHandle, stream_get_contents($remoteFileHandle))) === false) {
            throw new FilesystemException("Cannot write remote file's content to destination.");
        }

        fclose($remoteFileHandle);
        fclose($tmpFileHandle);

        return $file;
    }

    /**
     * @return array
     * @throws FilesystemException
     * @throws UploadException
     */
    protected function buildFileFromPost(string $inputName, ErrorAggregatorInterface $errorAggregator, $options = []) {
        if (! $this->getFw()->exists('FILES.' . $inputName)) {
            $errorAggregator->addFieldError(
                $this->getCoreModelFactory()->make(ErrorInterface::class)
                    ->setField($inputName)
                    ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                    ->setLocalizableMessage('No files specified.')
                    ->setMessage('No files specified.')
            );

            throw new UploadException('No such file input: ' . $inputName);
        }

        return $this->getFw()->get('FILES.' . $inputName);
    }

    protected function prepareFile(array &$file, array $options) {
        // Override target filename
        if ($saveFilename = $this->getOption(self::OPTION_FILENAME, $options)) {
            if ($this->getOption(self::OPTION_ACCEPT_INCOMING_EXT, $options)) {
                $file['name'] = sprintf(
                    '%s.%3s',
                    pathinfo((string) $saveFilename, PATHINFO_FILENAME),
                    strtolower(pathinfo((string) $file['name'], PATHINFO_EXTENSION)) ?: 'jpg'
                );
            }
            else {
                $file['name'] = $saveFilename;
            }
        }
    }

    protected function beforeProcessFile(&$file, array $options): static {
        return $this;
    }

    protected function afterProcessFile(\OliveOil\Core\Model\Upload\File $file, array $options): static {
        return $this;
    }

    /**
     * @param string $saveDir
     */
    protected function newUploadHandler($saveDir): \Sirius\Upload\Handler {
        return new Handler($saveDir, [], $this->getValidator());
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getCoreModelFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreModelFactory;
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }

    public function getValidator(): \Sirius\Validation\ValueValidator {
        return $this->validator;
    }

    /**
     * @param string $optionName
     * @return mixed
     */
    public function getOption($optionName, array $options = []) {
        return $options[$optionName] ?? $this->defaultOptions[$optionName] ?? null;
    }
}
