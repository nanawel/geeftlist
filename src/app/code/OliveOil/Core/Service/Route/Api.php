<?php

namespace OliveOil\Core\Service\Route;


use OliveOil\Core\Service\Route;

class Api extends Route
{
    /**
     * @param string[] $namedArgs
     * @param string[] $routeParts
     */
    protected function extractAction(array &$namedArgs, array &$routeParts): ?string {
        if ($this->fw->exists('rest_maps.' . $this->fw->get('PATTERN'))) {
            return strtolower((string) $this->fw->get('VERB'));
        }

        return parent::extractAction($namedArgs, $routeParts);
    }

    /**
     * @param string[] $namedArgs
     * @param string[] $routeParts
     * @return string[]
     */
    protected function extractRouteParams(array &$namedArgs, array &$routeParts): array {
        // $routeParts is ignored here (see routing-api.ini)
        return $namedArgs;
    }
}
