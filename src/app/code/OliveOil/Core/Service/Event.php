<?php

namespace OliveOil\Core\Service;


use Laminas\EventManager\EventInterface;
use OliveOil\Core\Exception\Service\RegistrationException;
use OliveOil\Core\Service\Event\ObserverDecorator;

class Event implements \OliveOil\Core\Service\EventInterface
{
    public const LISTENER_DEFINITIONS_CACHE_KEY = 'listenerDefinitions';

    /** @var \OliveOil\Core\Service\EventInterface[][] */
    protected static $eventServices = [];

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var \OliveOil\Core\Model\Event\Manager[] */
    protected $eventManagers = [];

    /** @var bool */
    protected $initialized = false;

    /** @var bool */
    protected $enabled = true;

    /** @var ObserverDecorator|null */
    protected $observerDecoratorPrototype;

    /**
     * @param string $eventManagerClass
     * @param string $eventSpace
     * @param bool $enableEventDebugListener
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \Laminas\EventManager\SharedEventManager $sharedEventManager,
        protected \OliveOil\Core\Service\Event\ProviderInterface $observerProvider,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\Cache $cache,
        protected $eventManagerClass = \OliveOil\Core\Model\Event\Manager::class,
        protected $eventSpace = self::EVENT_SPACE_DEFAULT,
        protected $enableEventDebugListener = false
    ) {
        $this->logger = $logService->getLoggerForClass($this);

        self::register($this);
    }

    protected static function register(\OliveOil\Core\Service\EventInterface $eventService) {
        $shareEventManagerId = spl_object_id($eventService->getSharedEventManager());
        if (isset(self::$eventServices[$shareEventManagerId][$eventService->getEventSpace()])) {
            throw new RegistrationException(sprintf(
                'Event space "%s" is already registered.',
                $eventService->getEventSpace()
            ));
        }

        self::$eventServices[$shareEventManagerId][$eventService->getEventSpace()] = $eventService;
    }

    protected static function unregister(\OliveOil\Core\Service\EventInterface $eventService) {
        $shareEventManagerId = spl_object_id($eventService->getSharedEventManager());
        unset(self::$eventServices[$shareEventManagerId][$eventService->getEventSpace()]);
    }

    /**
     * @inheritDoc
     */
    public function getEventManager($target) {
        if ($target === null) {
            throw new \InvalidArgumentException('$target cannot be null.');
        }

        $targetId = $target::class;
        if (! isset($this->eventManagers[$targetId])) {
            $identifiers = $this->getIdentifiersFromTarget($target);
            $this->eventManagers[$targetId] = $this->getCoreFactory()->make(
                $this->getEventManagerClass(),
                [
                    'eventService'       => $this,
                    'sharedEventManager' => $this->getSharedEventManager(),
                    'identifiers'        => $identifiers
                ]
            );
        }

        return $this->eventManagers[$targetId];
    }

    /**
     * @inheritDoc
     */
    public function setupListeners(): static {
        if ($this->isInitialized()) {
            // Prevent registering listeners multiple times
            return $this;
        }

        if ($this->getCache()->has(self::LISTENER_DEFINITIONS_CACHE_KEY)) {
            $listenerDefinitions = $this->getCache()->get(self::LISTENER_DEFINITIONS_CACHE_KEY);
        }
        else {
            $listenerDefinitions = $this->getObserverProvider()->getDefinitions();
            $this->getCache()->set(self::LISTENER_DEFINITIONS_CACHE_KEY, $listenerDefinitions);
        }

        foreach ($listenerDefinitions as $uniqueListenerId => $parsedDefinition) {
            /* @var string $identifier
             * @var string $class
             * @var string $method
             * @var string $operator
             * @var string $priority
             * @var string $eventName
             * @var string $callback
             */
            extract($parsedDefinition);
            /** @phpstan-ignore-next-line */
            $callable = $this->getDecoratedObserver($callback, $uniqueListenerId);
            /** @phpstan-ignore-next-line */
            $this->getSharedEventManager()->attach($identifier, $eventName, $callable, $priority);
        }

        if ($this->enableEventDebugListener) {
            $this->attachEventDebugListener();
        }

        $this->initialized = true;

        return $this;
    }

    /**
     * @param callable $callback
     * @param string|null $listenerId
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function getDecoratedObserver($callback, $listenerId = null): \OliveOil\Core\Service\Event\ObserverDecorator {
        if (!$this->observerDecoratorPrototype) {
            $this->observerDecoratorPrototype = $this->getCoreFactory()->make(
                ObserverDecorator::class,
                ['eventService' => $this]
            );
        }

        // Using clone here is much faster for a method that is heavily used in the framework
        return (clone $this->observerDecoratorPrototype)->init($callback, $listenerId);
    }

    protected function attachEventDebugListener(): static {
        $this->getSharedEventManager()->attach('*', '*', function (EventInterface $event): void {
            if (! $event->getParam('_skip_log')) {
                $args = [];
                foreach ($event->getParams() as $p) {
                    $args[] = gettype($p) === 'object' ? $p::class : gettype($p);
                }

                $target = gettype($event->getTarget()) === 'object' ? $event->getTarget()::class : gettype($event->getTarget());
                $this->getLogger()->debug(
                    'EVENT: ' . $event->getName() . ' | TARGET: ' . $target . ' | ARGS: ' . implode(',', $args)
                );
            }
        });
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function enableListeners($eventSpace = null): static {
        return $this->setListenersStatus($eventSpace, true);
    }

    /**
     * @inheritDoc
     */
    public function disableListeners($eventSpace = null): static {
        return $this->setListenersStatus($eventSpace, false);
    }

    /**
     * @inheritDoc
     */
    public function setListenersStatus($eventSpace, $enabled = null): static {
        if (is_array($eventSpace)) {
            foreach ($eventSpace as $eventSpaceName => $status) {
                $this->setListenersStatus($eventSpaceName, $status);
            }

            return $this;
        }

        if ($eventSpace === null) {
            $eventSpace = $this->eventSpace;
        }

        if ($eventSpace == self::EVENT_SPACE_WILDCARD) {
            foreach ($this->getEventServices() as $eventService) {
                $eventService->setListenersStatus(null, $enabled);
            }
        }
        elseif ($eventSpace == $this->eventSpace) {
            $this->enabled($enabled);
        }
        else {
            $this->getEventService($eventSpace)->setListenersStatus(null, $enabled);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function areListenersDisabled($eventSpace = null): bool {
        if ($eventSpace === null) {
            $eventSpace = $this->eventSpace;
        }

        return ! $this->getListenersStatus($eventSpace);
    }

    /**
     * @inheritDoc
     */
    public function getListenersStatus($eventSpace = null) {
        if ($eventSpace === null) {
            $eventSpace = $this->eventSpace;
        }

        if ($eventSpace == $this->eventSpace) {
            return $this->enabled();
        }

        return $this->getEventService($eventSpace)->getListenersStatus($eventSpace);
    }

    /**
     * @inheritDoc
     * @return mixed[]
     */
    public function getAllListenersStatus(): array {
        $status = [];
        foreach ($this->getEventServices() as $eventService) {
            $status[$eventService->getEventSpace()] = $eventService->getListenersStatus();
        }

        return $status;
    }

    /**
     * @param object $target
     * @return string[]
     */
    protected function getIdentifiersFromTarget($target): array {
        return array_merge(
            [$target::class],
            array_values(class_parents($target, false)),
            array_values(class_implements($target, false))
        );
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getSharedEventManager(): \Laminas\EventManager\SharedEventManager {
        return $this->sharedEventManager;
    }

    public function getObserverProvider(): \OliveOil\Core\Service\Event\ProviderInterface {
        return $this->observerProvider;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }

    public function getCache(): \OliveOil\Core\Service\Cache {
        return $this->cache;
    }

    /**
     * @return string
     */
    public function getEventSpace() {
        return $this->eventSpace;
    }

    /**
     * @return string
     */
    public function getEventManagerClass() {
        return $this->eventManagerClass;
    }

    /**
     * @return boolean
     */
    public function isInitialized() {
        return $this->initialized;
    }

    /**
     * @return boolean
     */
    protected function enabled($enable = null) {
        if ($enable !== null) {
            $this->enabled = (bool) $enable;
        }

        return $this->enabled;
    }

    /**
     * @return \OliveOil\Core\Service\EventInterface[]
     */
    protected function getEventServices() {
        $shareEventManagerId = spl_object_id($this->getSharedEventManager());

        return self::$eventServices[$shareEventManagerId];
    }

    /**
     * @param string $eventSpace
     * @return \OliveOil\Core\Service\EventInterface
     */
    protected function getEventService($eventSpace) {
        $shareEventManagerId = spl_object_id($this->getSharedEventManager());

        if (! isset(self::$eventServices[$shareEventManagerId][$eventSpace])) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a registered event space.', $eventSpace));
        }

        return self::$eventServices[$shareEventManagerId][$eventSpace];
    }
}
