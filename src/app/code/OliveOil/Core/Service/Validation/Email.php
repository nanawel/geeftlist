<?php

namespace OliveOil\Core\Service\Validation;


class Email
{
    /**
     * @param bool|null $checkMX
     */
    public function __construct(protected \OliveOil\Core\Model\App\ConfigInterface $appConfig, protected $checkMX = null)
    {
    }

    public function validate($email): bool {
        return $email === null || \Audit::instance()->email(
            $email,
            $this->checkMX ?? !$this->getAppConfig()->getValue('SKIP_EMAIL_CHECK_MX')
        );
    }

    public function __invoke($email): bool {
        return $this->validate($email);
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }
}
