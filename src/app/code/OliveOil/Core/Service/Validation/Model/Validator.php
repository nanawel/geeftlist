<?php

namespace OliveOil\Core\Service\Validation\Model;


use OliveOil\Core\Model\AbstractModel;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Validator implements ValidatorInterface
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory)
    {
    }

    /**
     * @param ErrorAggregatorInterface|null $errorAggregator
     * @return ErrorAggregatorInterface
     */
    public function validate(AbstractModel $model, array $validationRules, $errorAggregator = null) {
        if (! $errorAggregator) {
            $errorAggregator = $this->getCoreFactory()->make(ErrorAggregatorInterface::class);
        }

        foreach ($validationRules as $field => $constraintsAsErrors) {
            if ($field == '*') {
                // Will be processed later
                continue;
            }

            if (! is_array($constraintsAsErrors)) {
                $constraintsAsErrors = [$constraintsAsErrors];
            }

            foreach ($constraintsAsErrors as $constraintAsError) {
                if (is_string($constraintAsError)) {
                    if ($this->handleStringConstraintDefinition($constraintAsError, $model, $field, $errorAggregator) === false) {
                        break;
                    }
                }
                elseif (is_callable($constraintAsError)) {
                    if ($this->handleCallableConstraintDefinition($constraintAsError, $model, $field, $errorAggregator) === false) {
                        break;
                    }
                }
            }
        }

        // Process wildcard field validator "*"
        if (isset($validationRules['*'])) {
            $constraintAsError = $validationRules['*'];
            foreach (array_keys(array_diff_key($model->getData(), $validationRules)) as $field) {
                if (is_callable($constraintAsError)) {
                    $this->handleCallableConstraintDefinition($constraintAsError, $model, $field, $errorAggregator);
                }
            }
        }

        return $errorAggregator;
    }

    /**
     * @param string $constraintAsError
     * @param AbstractModel $model
     * @param string $field
     * @param ErrorAggregatorInterface $errorAggregator
     * @return ErrorInterface|bool|null
     */
    protected function handleStringConstraintDefinition($constraintAsError, $model, $field, $errorAggregator) {
        switch ($constraintAsError) {
            case ErrorInterface::TYPE_FIELD_NOT_SCALAR:
                if (
                    $model->getData($field) !== null
                    && ! is_scalar($model->getData($field))
                ) {
                    $error = $this->getCoreFactory()->make(ErrorInterface::class)
                        ->setField($field)
                        ->setType(ErrorInterface::TYPE_FIELD_NOT_SCALAR)
                        ->setLevel(ErrorInterface::LEVEL_ERROR)
                        ->setLocalizableMessage('Invalid field value type: {0}.')
                        ->setMessage(sprintf('Invalid field value type: %s.', $field));
                    $errorAggregator->addFieldError($error);

                    return false;
                }

                break;

            case ErrorInterface::TYPE_FIELD_EMPTY:
                if (
                    ! $model->hasData($field)
                    || $model->getData($field) === null
                    || ($model->getData($field) === [])
                    || (is_scalar($model->getData($field)) && !trim((string) $model->getData($field)))
                ) {
                    $error = $this->getCoreFactory()->make(ErrorInterface::class)
                        ->setField($field)
                        ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                        ->setLevel(ErrorInterface::LEVEL_ERROR)
                        ->setLocalizableMessage('Missing field: {0}.')
                        ->setMessage(sprintf('Missing field: %s.', $field));
                    $errorAggregator->addFieldError($error);
                }

                break;

            case ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE:
                if (
                    $model->getOrigData($field) !== null
                    && $model->getData($field) != $model->getOrigData($field)
                ) {
                    $error = $this->getCoreFactory()->make(ErrorInterface::class)
                        ->setField($field)
                        ->setType(ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE)
                        ->setLevel(ErrorInterface::LEVEL_ERROR)
                        ->setLocalizableMessage('Field "{0}" cannot be overwritten.')
                        ->setMessage(sprintf('Field "%s" cannot be overwritten.', $field));
                    $errorAggregator->addFieldError($error);
                    $model->setData($field, $model->getOrigData($field));
                }

                break;
        }

        return $error ?? null;
    }

    /**
     * @param callable $constraintAsError
     * @param AbstractModel $model
     * @param string $field
     * @param ErrorAggregatorInterface $errorAggregator
     * @return ErrorInterface|bool|null
     */
    protected function handleCallableConstraintDefinition($constraintAsError, $model, $field, $errorAggregator) {
        $return = call_user_func($constraintAsError, $model->getData($field), $field);
        if ($return instanceof ErrorInterface) {
            $error = $return->setField($field);
        }
        elseif ($return === false) {
            $error = $this->getCoreFactory()->make(ErrorInterface::class)
                ->setField($field)
                ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                ->setLevel(ErrorInterface::LEVEL_ERROR)
                ->setLocalizableMessage('Invalid field: {0}.')
                ->setMessage(sprintf('Invalid field: %s.', $field));
        }
        elseif (is_string($return) && strlen($return)) {
            $error = $this->getCoreFactory()->make(ErrorInterface::class)
                ->setField($field)
                ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                ->setLevel(ErrorInterface::LEVEL_ERROR)
                ->setLocalizableMessage($return)
                ->setMessage($return);
        }

        if (isset($error)) {
            $errorAggregator->addFieldError($error);
            $model->unsData($field);
        }

        return $error ?? null;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }
}
