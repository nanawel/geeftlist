<?php

namespace OliveOil\Core\Service\Validation\Model;


use OliveOil\Core\Model\AbstractModel;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

interface ValidatorInterface
{
    /**
     * @param ErrorAggregatorInterface|null $errorAggregator
     * @return ErrorAggregatorInterface
     */
    public function validate(AbstractModel $model, array $validationRules, $errorAggregator = null);
}
