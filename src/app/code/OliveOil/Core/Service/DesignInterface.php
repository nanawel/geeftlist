<?php

namespace OliveOil\Core\Service;

interface DesignInterface
{
    public const DEFAULT_THEME = 'default';

    /**
     * @return $this
     */
    public function bootstrapTheme();

    /**
     * @param string $file
     * @param array $subdirs Optional subdirs to append to theme base path
     * @param bool $returnFullPath TRUE to return a path relative to app root
     *                             (ex: app/template/default/mydir/myfile.phtml)
     *                             FALSE relative to current theme's directory
     *                             (ex: dir1/myfile.phtml)
     * @return null|string
     */
    public function resolveTemplateFile($file, array $subdirs = [], $returnFullPath = false);

    /**
     * @return string
     */
    public function getFaviconUrl();

    /**
     * @param string $theme
     * @return $this
     */
    public function setTheme($theme);

    /**
     * @return string
     */
    public function getTheme();

    /**
     * @return string[]
     */
    public function getAvailableThemes();

    /**
     * @param string $path
     * @param string|null $theme
     * @return string
     */
    public function getAssetDistPath($path, $theme = null);

    /**
     * @param string $path
     * @param string|null $theme
     * @return string
     */
    public function getAssetSrcPath($path, $theme = null);

    /**
     * @param string $path
     * @param array $params
     * @param bool $fallbackDefault
     * @return string
     */
    public function getAssetUrl($path, $params = [], $fallbackDefault = true);

    /**
     * @param string $path
     * @param array $params
     * @return string
     */
    public function getCssUrl($path, $params = []);

    /**
     * @param string $path
     * @param array $params
     * @return string
     */
    public function getJsUrl($path, $params = []);

    /**
     * @param string $path
     * @param array $params
     * @return string
     */
    public function getImageUrl($path, $params = []);
}
