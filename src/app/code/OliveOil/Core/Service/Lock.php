<?php

namespace OliveOil\Core\Service;


class Lock
{
    public function __construct(protected \OliveOil\Core\Model\App\ConfigInterface $appConfig, protected \NinjaMutex\MutexFabric $mutexFabric)
    {
    }

    /**
     * @param string $name
     * @param int|null $timeout
     * @return bool
     */
    public function acquire($name, $timeout = null) {
        return $this->getMutex($name)->acquireLock($timeout);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function isLocked($name) {
        return $this->getMutex($name)->isLocked();
    }

    /**
     * @param string $name
     * @return bool
     */
    public function release($name) {
        return $this->getMutex($name)->releaseLock();
    }

    /**
     * @param callable $callable
     * @param int|null $timeout
     * @return mixed
     */
    public function synchronized($name, $callable, $timeout = null) {
        try {
            if ($this->acquire($name, $timeout)) {
                return call_user_func($callable);
            }
        }
        finally {
            $this->release($name);
        }
    }

    /**
     * @param string $name
     * @return \NinjaMutex\Mutex
     */
    protected function getMutex($name) {
        return $this->getMutexFabric()->get($name);
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getMutexFabric(): \NinjaMutex\MutexFabric {
        return $this->mutexFabric;
    }
}
