<?php


namespace OliveOil\Core\Service\Rest\JsonApi\Schema;


use Neomerx\JsonApi\Contracts\Schema\IdentifierInterface;

class ResourceIdentifier implements IdentifierInterface
{
    /**
     * ResourceIdentifier constructor.
     *
     * @param mixed|null $identifierMeta
     */
    public function __construct(protected string $type, protected string $id, protected $identifierMeta = null)
    {
    }

    public function getType(): string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getId(): string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifierMeta() {
        return $this->identifierMeta;
    }

    public function setIdentifierMeta(mixed $identifierMeta): self {
        $this->identifierMeta = $identifierMeta;

        return $this;
    }

    /**
     * If identifier has meta.
     */
    public function hasIdentifierMeta(): bool {
        return $this->identifierMeta !== null;
    }
}
