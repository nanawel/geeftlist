<?php


namespace OliveOil\Core\Service\Rest\JsonApi;


interface EditableContextInterface extends \Neomerx\JsonApi\Contracts\Parser\EditableContextInterface
{
    /**
     * @param string|null $relationshipName
     */
    public function getRelationshipsCollectionConstraints($relationshipName = null): array;

    /**
     * @param string $relationshipName
     */
    public function getRelationshipFullPath($relationshipName): string;

    /**
     * @param string $relationshipName
     */
    public function isIncludedRelationship($relationshipName): bool;
}
