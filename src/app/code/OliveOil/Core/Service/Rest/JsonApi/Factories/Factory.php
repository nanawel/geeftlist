<?php

namespace OliveOil\Core\Service\Rest\JsonApi\Factories;


use Neomerx\JsonApi\Contracts\Encoder\EncoderInterface;
use Neomerx\JsonApi\Contracts\Http\Headers\AcceptMediaTypeInterface;
use Neomerx\JsonApi\Contracts\Http\Headers\MediaTypeInterface;
use Neomerx\JsonApi\Contracts\Parser\EditableContextInterface;
use Neomerx\JsonApi\Contracts\Parser\ParserInterface;
use Neomerx\JsonApi\Contracts\Parser\RelationshipDataInterface;
use Neomerx\JsonApi\Contracts\Parser\ResourceInterface;
use Neomerx\JsonApi\Contracts\Representation\DocumentWriterInterface;
use Neomerx\JsonApi\Contracts\Representation\ErrorWriterInterface;
use Neomerx\JsonApi\Contracts\Representation\FieldSetFilterInterface;
use Neomerx\JsonApi\Contracts\Schema\IdentifierInterface;
use Neomerx\JsonApi\Contracts\Schema\IdentifierInterface as SchemaIdentifierInterface;
use Neomerx\JsonApi\Contracts\Schema\LinkInterface;
use Neomerx\JsonApi\Contracts\Schema\PositionInterface;
use Neomerx\JsonApi\Contracts\Schema\SchemaContainerInterface;
use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Http\Headers\AcceptMediaType;
use Neomerx\JsonApi\Http\Headers\MediaType;
use Neomerx\JsonApi\Parser\IdentifierAndResource;
use Neomerx\JsonApi\Parser\Parser;
use Neomerx\JsonApi\Parser\RelationshipData\RelationshipDataIsCollection;
use Neomerx\JsonApi\Parser\RelationshipData\RelationshipDataIsIdentifier;
use Neomerx\JsonApi\Parser\RelationshipData\RelationshipDataIsNull;
use Neomerx\JsonApi\Parser\RelationshipData\RelationshipDataIsResource;
use Neomerx\JsonApi\Representation\DocumentWriter;
use Neomerx\JsonApi\Representation\ErrorWriter;
use Neomerx\JsonApi\Representation\FieldSetFilter;
use Neomerx\JsonApi\Schema\Link;
use Neomerx\JsonApi\Schema\SchemaContainer;
use OliveOil\Core\Service\Rest\JsonApi\EditableContext;
use OliveOil\Core\Service\Rest\JsonApi\Schema\ResourceIdentifier;

/**
 * Class Factory
 *
 * Custom factory class for neomerx/json-api using DI instead of hardcoded class names.
 */
class Factory extends \Neomerx\JsonApi\Factories\Factory
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory)
    {
    }

    /**
     * @inheritdoc
     */
    public function createEncoder(SchemaContainerInterface $container): EncoderInterface {
        return $this->getCoreFactory()->make(
            Encoder::class,
            [
                'factory' => $this,
                'container' => $container
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createSchemaContainer(iterable $schemas): SchemaContainerInterface {
        return $this->getCoreFactory()->make(
            SchemaContainer::class,
            [
                'factory' => $this,
                'schemas' => $schemas
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createParser(
        SchemaContainerInterface $container,
        EditableContextInterface $context
    ): ParserInterface {
        return $this->getCoreFactory()->make(
            Parser::class,
            [
                'context' => $context,
                'factory' => $this,
                'container' => $container
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createDocumentWriter(): DocumentWriterInterface {
        return $this->getCoreFactory()->make(DocumentWriter::class);
    }

    /**
     * @inheritdoc
     */
    public function createErrorWriter(): ErrorWriterInterface {
        return $this->getCoreFactory()->make(ErrorWriter::class);
    }

    /**
     * @inheritdoc
     */
    public function createFieldSetFilter(array $fieldSets): FieldSetFilterInterface {
        return $this->getCoreFactory()->make(
            FieldSetFilter::class,
            [
                'fieldSets' => $fieldSets
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createParsedResource(
        EditableContextInterface $context,
        PositionInterface $position,
        SchemaContainerInterface $container,
        $data
    ): ResourceInterface {
        return $this->getCoreFactory()->make(
            IdentifierAndResource::class,
            [
                'context' => $context,
                'position' => $position,
                'factory' => $this,
                'container' => $container,
                'data' => $data
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createLink(bool $isSubUrl, string $value, bool $hasMeta, $meta = null): LinkInterface {
        return $this->getCoreFactory()->make(
            Link::class,
            [
                'isSubUrl' => $isSubUrl,
                'value' => $value,
                'hasMeta' => $hasMeta,
                'meta' => $meta
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createRelationshipDataIsResource(
        SchemaContainerInterface $schemaContainer,
        EditableContextInterface $context,
        PositionInterface $position,
        $resource
    ): RelationshipDataInterface {
        return $this->getCoreFactory()->make(
            RelationshipDataIsResource::class,
            [
                'context' => $context,
                'factory' => $this,
                'schemaContainer' => $schemaContainer,
                'position' => $position,
                'resource' => $resource
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createRelationshipDataIsIdentifier(
        SchemaContainerInterface $schemaContainer,
        EditableContextInterface $context,
        PositionInterface $position,
        SchemaIdentifierInterface $identifier
    ): RelationshipDataInterface {
        return $this->getCoreFactory()->make(
            RelationshipDataIsIdentifier::class,
            [
                'context' => $context,
                'factory' => $this,
                'schemaContainer' => $schemaContainer,
                'position' => $position,
                'identifier' => $identifier
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createRelationshipDataIsCollection(
        SchemaContainerInterface $schemaContainer,
        EditableContextInterface $context,
        PositionInterface $position,
        iterable $resources
    ): RelationshipDataInterface {
        return $this->getCoreFactory()->make(
            RelationshipDataIsCollection::class,
            [
                'context' => $context,
                'factory' => $this,
                'schemaContainer' => $schemaContainer,
                'position' => $position,
                'resources' => $resources
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createRelationshipDataIsNull(): RelationshipDataInterface {
        return $this->getCoreFactory()->make(RelationshipDataIsNull::class);
    }

    /**
     * @inheritdoc
     */
    public function createMediaType(string $type, string $subType, array $parameters = null): MediaTypeInterface {
        return $this->getCoreFactory()->make(
            MediaType::class,
            [
                'type' => $type,
                'subType' => $subType,
                'parameters' => $parameters,
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createAcceptMediaType(
        int $position,
        string $type,
        string $subType,
        array $parameters = null,
        float $quality = 1.0
    ): AcceptMediaTypeInterface {
        return $this->getCoreFactory()->make(
            AcceptMediaType::class,
            [
                'position' => $position,
                'type' => $type,
                'subType' => $subType,
                'parameters' => $parameters,
                'quality' => $quality,
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function createParserContext(
        array $fieldSets,
        array $includePaths
    ): EditableContextInterface {
        return $this->getCoreFactory()->make(
            EditableContext::class,
            [
                'fieldSets' => $fieldSets,
                'includePaths' => $includePaths,
            ]
        );
    }

    /**
     * @param string $type
     * @param string $id
     * @param array|null $meta
     * @return IdentifierInterface
     */
    public function createResourceIdentifier(
        $type,
        $id,
        array $meta = null
    ) {
        return $this->getCoreFactory()->make(
            ResourceIdentifier::class,
            [
                'type'           => (string) $type,
                'id'             => (string) $id,
                'identifierMeta' => $meta,
            ]
        );
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }
}
