<?php


namespace OliveOil\Core\Service\Rest\JsonApi;

use Narrowspark\HttpStatus\Exception\InternalServerErrorException;
use OliveOil\Core\Model\Http\ResponseInterface;

/**
 * Class ResponseWriter
 *
 * This class was originally used to format JSON-API compliant response but is not needed
 * anymore since the switch to neomerx/json-api library.
 */
class ResponseWriter extends \OliveOil\Core\Service\Http\ResponseWriter
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Helper\Data\Factory $helperDataFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Model\Api\ConfigInterface $apiConfig,
        array $callbacks = []
    ) {
        parent::__construct($fw, $helperDataFactory, $eventService, $callbacks);
    }

    protected function beforeWrite(ResponseInterface $response) {
        // An empty response is *not* JSON-API compliant, so turn it into an error
        if (
            (!$response->getCode() || preg_match('/2../', (string) $response->getCode()))
            && (!($response->getHeaders()['Content-Type'] ?? null) || $response->getHeaders()['Content-Type'])
            && !($response->getHeaders()['Allow'] ?? null)
            && trim($response->getBody()) === ''
        ) {
            throw new InternalServerErrorException('No content in response.');
        }

        parent::beforeWrite($response);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultContentType(): string {
        return 'application/vnd.api+json';
    }

    public function getApiConfig(): \OliveOil\Core\Model\Api\ConfigInterface {
        return $this->apiConfig;
    }
}
