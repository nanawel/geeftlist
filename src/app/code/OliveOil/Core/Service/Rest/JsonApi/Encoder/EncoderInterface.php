<?php


namespace OliveOil\Core\Service\Rest\JsonApi\Encoder;


use OliveOil\Core\Model\Rest\JsonApi\Params;

interface EncoderInterface extends \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface
{
    /**
     * @params Params $params
     * @return $this
     */
    public function initFromParams(Params $params);
}
