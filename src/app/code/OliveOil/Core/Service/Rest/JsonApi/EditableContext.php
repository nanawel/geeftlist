<?php

namespace OliveOil\Core\Service\Rest\JsonApi;


use Neomerx\JsonApi\Contracts\Schema\PositionInterface;

/**
 * @see \Neomerx\JsonApi\Factories\Factory::createParserContext()
 */
class EditableContext implements EditableContextInterface
{
    /**
     * @var PositionInterface|null
     */
    protected $position;

    /**
     * @var array
     */
    protected $relationshipsCollectionConstraints = [];

    public function __construct(protected array $fieldSets, protected array $includePaths)
    {
    }

    /**
     * @inheritdoc
     */
    public function getFieldSets(): array {
        return $this->fieldSets;
    }

    /**
     * @inheritdoc
     */
    public function getIncludePaths(): array {
        return $this->includePaths;
    }

    /**
     * @inheritdoc
     */
    public function getPosition(): PositionInterface {
        // parser's implementation should guarantee that position will always be initialized
        // before use in a schema.
        \assert($this->position !== null);

        return $this->position;
    }

    /**
     * @inheritdoc
     */
    public function setPosition(PositionInterface $position): void {
        $this->position = $position;
    }

    /**
     * @inheritDoc
     */
    public function getRelationshipsCollectionConstraints($relationshipName = null): array {
        if ($relationshipName === null) {
            return $this->relationshipsCollectionConstraints;
        }

        return $this->relationshipsCollectionConstraints[$this->getRelationshipFullPath($relationshipName)] ?? [];
    }

    public function setRelationshipsCollectionConstraints(array $relationshipsCollectionConstraints): EditableContext {
        $this->relationshipsCollectionConstraints = $relationshipsCollectionConstraints;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isIncludedRelationship($relationshipName): bool {
        return in_array($this->getRelationshipFullPath($relationshipName), $this->getIncludePaths());
    }

    /**
     * @inheritDoc
     */
    public function getRelationshipFullPath($relationshipName): string {
        return $this->getPosition()->getPath() !== '' && $this->getPosition()->getPath() !== '0'
            ? sprintf('%s.%s', $this->getPosition()->getPath(), $relationshipName)
            : $relationshipName;
    }
}
