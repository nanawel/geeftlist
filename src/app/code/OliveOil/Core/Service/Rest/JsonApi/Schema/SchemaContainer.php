<?php

namespace OliveOil\Core\Service\Rest\JsonApi\Schema;


use Neomerx\JsonApi\Contracts\Schema\SchemaInterface;

/**
 * Class SchemaContainer
 */
class SchemaContainer extends \Neomerx\JsonApi\Schema\SchemaContainer
{
    /** @var \Neomerx\JsonApi\Contracts\Factories\FactoryInterface */
    protected $factory;

    public function __construct(
        \Neomerx\JsonApi\Contracts\Factories\FactoryInterface $factory,
        iterable $schemas,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory
    ) {
        parent::__construct($factory, $schemas);
        $this->factory = $factory;
    }

    /**
     * @override
     * @inheritDoc
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function createSchemaFromClassName(string $className): SchemaInterface {
        return $this->getCoreFactory()->make($className, [
            'factory' => $this->factory
        ]);
    }

    protected function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }
}
