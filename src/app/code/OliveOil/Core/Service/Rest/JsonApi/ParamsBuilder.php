<?php


namespace OliveOil\Core\Service\Rest\JsonApi;


use OliveOil\Core\Model\Rest\JsonApi\Params;
use OliveOil\Core\Model\Rest\RequestInterface;

class ParamsBuilder
{
    public const COLLECTION_CONSTRAINTS = [
        'filter',
        'sort',
        'page',
    ];

    public const RELATIONSHIPS_PATH_SEPARATOR = '.';

    public const COLLECTION_DEFAULT_PAGE_SIZE = 10;

    public const COLLECTION_MAX_PAGE_SIZE     = 100;

    public const RELATIONSHIPS_MAX_DEPTH      = 3;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var string|null */
    protected $primaryEntityType;

    /** @var int|null */
    protected $primaryEntityId;

    /** @var array */
    protected $primaryCollectionConstraints = [];

    /** @var array */
    protected $relationshipsCollectionConstraints = [];

    /** @var array */
    protected $includedRelationships = [];

    /**
     * @param string $paramsClass
     * @param int $collectionMaxPageSize
     * @param int $relationshipsMaxDepth
     */
    public function __construct(
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \OliveOil\Core\Service\Log $logService,
        protected $paramsClass = Params::class,
        protected $collectionMaxPageSize = self::COLLECTION_MAX_PAGE_SIZE,
        protected $relationshipsMaxDepth = self::RELATIONSHIPS_MAX_DEPTH
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function fromRequest(RequestInterface $request): static {
        if (strlen($id = (string) $request->getRouteParam('id')) !== 0) {
            $this->setPrimaryEntityId($id);
        }

        $this->setCollectionConstraints($this->toCollectionConstraints($request));
        $this->setIncludedRelationships($this->toIncludedRelationships($request));

        return $this;
    }

    /**
     * @return Params
     */
    public function build() {
        /** @var Params $params */
        $params = $this->getCoreFactory()->make($this->getParamsClass());
        $params->setPrimaryEntityType($this->getPrimaryEntityType())
            ->setPrimaryEntityId($this->getPrimaryEntityId())
            ->setPrimaryCollectionConstraints($this->getPrimaryCollectionConstraints())
            ->setRelationshipsCollectionConstraints($this->getRelationshipsCollectionConstraints())
            ->setIncludedRelationships($this->getIncludedRelationships());

        $this->reset();

        return $params;
    }

    protected function toCollectionConstraints(RequestInterface $request): array {
        $collectionConstraints = [];
        $queryParams = $request->getQueryParams();
        foreach (self::COLLECTION_CONSTRAINTS as $constraintType) {
            $method = sprintf('get%sCriteria', ucfirst(\OliveOil\snakecase2camelcase($constraintType)));
            if (is_callable([$this, $method])) {
                $collectionConstraints = array_merge(
                    $collectionConstraints,
                    $this->$method($queryParams[$constraintType] ?? null)
                );
            }
        }

        // Return only non-empty constraints
        return array_filter($collectionConstraints);
    }

    protected function toIncludedRelationships(RequestInterface $request): array {
        $include = array_filter(array_map('trim', explode(',', $request->getQueryParam('include') ?? '')));
        foreach ($include as $includedRelationship) {
            if (
                ($depth = count(explode(static::RELATIONSHIPS_PATH_SEPARATOR, $includedRelationship)))
                > $this->getRelationshipsMaxDepth()
            ) {
                throw new \InvalidArgumentException(sprintf(
                    'Invalid included relationship "%s": depth "%d" greater than allowed limit "%d"',
                    $includedRelationship,
                    $depth,
                    $this->getRelationshipsMaxDepth()
                ));
            }
        }

        return $include;
    }

    /**
     * @param array|null $definition
     */
    protected function getFilterCriteria($definition): array {
        if ($definition === null) {
            return [];
        }

        if (! is_array($definition)) {
            throw new \InvalidArgumentException('Invalid definition for the "filter" constraint.');
        }

        $criteria = [];
        foreach ($definition as $filterField => $filterConstraints) {
            if (
                is_numeric($filterField)
                || ! is_array($filterConstraints)
            ) {
                throw new \InvalidArgumentException('Invalid field in the "filter" constraint.');
            }

            foreach ($filterConstraints as $operator => $value) {
                if (!preg_match('/[[:alpha:]]+/', $operator)) {
                    throw new \InvalidArgumentException('Invalid operator in the "filter" constraint.');
                }

                $criteria['filter'][$filterField][] = [$operator => $value];
            }
        }

        return $criteria;
    }

    /**
     * @see https://jsonapi.org/format/#fetching-sorting
     *
     * @param string|null $definition
     */
    protected function getSortCriteria($definition): array {
        if ($definition === null) {
            return [];
        }

        $definition = array_filter(array_map('trim', explode(',', $definition)));
        if ($definition === []) {
            return [];
        }

        $criteria = [];
        foreach ($definition as $fieldDefinition) {
            if (!preg_match('/^(?P<direction>\-)?(?P<field>\w+)$/', $fieldDefinition, $matches)) {
                throw new \InvalidArgumentException('Invalid value for the "sort" constraint.');
            }

            $criteria['sort_order'][$matches['field']] = $matches['direction'] === '-' ? SORT_DESC : SORT_ASC;
        }

        return $criteria;
    }

    /**
     * @param array|string|null $definition
     */
    protected function getPageCriteria($definition): array {
        if ($definition === null) {
            return ['page' => [
                'number' => 1,
                'size'   => self::COLLECTION_DEFAULT_PAGE_SIZE,
            ]];
        }

        if (!is_numeric($definition) && !is_array($definition)) {
            throw new \InvalidArgumentException('Invalid value for the "page" constraint.');
        }

        if (is_numeric($definition)) {
            $definition = ['number' => $definition];
        }

        $number = (int) ($definition['number'] ?? 1);
        $size = (int) ($definition['size'] ?? self::COLLECTION_DEFAULT_PAGE_SIZE);

        if ($size > $this->getCollectionMaxPageSize()) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid page size "%d". Must be <= %d.',
                $size,
                $this->getCollectionMaxPageSize()
            ));
        }

        return ['page' => [
            'number' => $number,
            'size'   => $size,
        ]];
    }

    /**
     * @return $this
     */
    public function reset(): static {
        $this->primaryEntityType = null;
        $this->primaryEntityId = null;
        $this->collectionMaxPageSize = self::COLLECTION_MAX_PAGE_SIZE;  // FIXME Not exactly right, see constructor
        $this->relationshipsMaxDepth = self::RELATIONSHIPS_MAX_DEPTH;   // FIXME Same here
        $this->primaryCollectionConstraints = [];
        $this->relationshipsCollectionConstraints = [];
        $this->includedRelationships = [];

        return $this;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * @return string
     */
    public function getParamsClass() {
        return $this->paramsClass;
    }

    /**
     * @return string
     */
    public function getPrimaryEntityType() {
        return $this->primaryEntityType;
    }

    /**
     * @param string $primaryEntityType
     * @return $this
     */
    public function setPrimaryEntityType($primaryEntityType): static {
        $this->primaryEntityType = $primaryEntityType;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrimaryEntityId() {
        return $this->primaryEntityId;
    }

    /**
     * @param int $primaryEntityId
     * @return $this
     */
    public function setPrimaryEntityId($primaryEntityId): static {
        $this->primaryEntityId = $primaryEntityId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCollectionMaxPageSize() {
        return $this->collectionMaxPageSize;
    }

    /**
     * @param int $collectionMaxPageSize
     * @return $this
     */
    public function setCollectionMaxPageSize($collectionMaxPageSize): static {
        $this->collectionMaxPageSize = $collectionMaxPageSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getRelationshipsMaxDepth() {
        return $this->relationshipsMaxDepth;
    }

    /**
     * @param int $relationshipsMaxDepth
     * @return $this
     */
    public function setRelationshipsMaxDepth($relationshipsMaxDepth): static {
        $this->relationshipsMaxDepth = $relationshipsMaxDepth;

        return $this;
    }

    /**
     * @return $this
     */
    public function setCollectionConstraints(array $constraints): static {
        $this->primaryCollectionConstraints = [];
        $this->relationshipsCollectionConstraints = [];
        foreach ($constraints as $constraintType => $constraintTypeItems) {
            switch ($constraintType) {
                case 'filter':
                    foreach ($constraintTypeItems as $field => $fieldConstraints) {
                        if (!str_contains((string) $field, (string) static::RELATIONSHIPS_PATH_SEPARATOR)) {
                            $this->primaryCollectionConstraints[$constraintType][$field] = $fieldConstraints;
                        } else {
                            foreach ($fieldConstraints as $fieldConstraint) {
                                foreach ($fieldConstraint as $operator => $value) {
                                    $parts = explode(static::RELATIONSHIPS_PATH_SEPARATOR, (string) $field);
                                    $relationshipPath = implode(
                                        static::RELATIONSHIPS_PATH_SEPARATOR,
                                        array_slice($parts, 0, count($parts) - 1)
                                    );
                                    $field = $parts[count($parts) - 1];

                                    $this->relationshipsCollectionConstraints
                                        [$relationshipPath][$constraintType][$field][][$operator] = $value;
                                }
                            }
                        }
                    }

                    break;

                case 'sort_order':
                case 'page':
                    $this->primaryCollectionConstraints[$constraintType] = $constraintTypeItems;
                    break;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function setPrimaryCollectionConstraints(array $constraints): static {
        $this->primaryCollectionConstraints = $constraints;

        return $this;
    }

    /**
     * @return array
     */
    public function getPrimaryCollectionConstraints() {
        return $this->primaryCollectionConstraints;
    }

    /**
     * @return array
     */
    public function getRelationshipsCollectionConstraints() {
        return $this->relationshipsCollectionConstraints;
    }

    public function setRelationshipsCollectionConstraints(array $relationshipsCollectionConstraints): static {
        $this->relationshipsCollectionConstraints = $relationshipsCollectionConstraints;

        return $this;
    }

    /**
     * @return array
     */
    public function getIncludedRelationships() {
        return $this->includedRelationships;
    }

    /**
     * @return $this
     */
    public function setIncludedRelationships(array $includedRelationships): static {
        $this->includedRelationships = $includedRelationships;

        return $this;
    }
}
