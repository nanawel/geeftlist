<?php

namespace OliveOil\Core\Service\Rest\JsonApi\Encoder;


use Neomerx\JsonApi\Contracts\Parser\DocumentDataInterface;
use Neomerx\JsonApi\Contracts\Parser\IdentifierInterface;
use Neomerx\JsonApi\Contracts\Parser\ParserInterface;
use Neomerx\JsonApi\Contracts\Parser\ResourceInterface;
use Neomerx\JsonApi\Contracts\Representation\DocumentWriterInterface;
use Neomerx\JsonApi\Exceptions\InvalidArgumentException;
use OliveOil\Core\Model\Rest\JsonApi\Params;
use OliveOil\Core\Service\Rest\JsonApi\EditableContext;

class Encoder extends \Neomerx\JsonApi\Encoder\Encoder implements EncoderInterface
{
    /** @var Params */
    protected $params;

    /**
     * @params Params $params
     * @return $this
     */
    public function initFromParams(Params $params): static {
        $this->params = $params;
        $this->withIncludedPaths($params->getIncludedRelationships());

        return $this;
    }

    /**
     * @see \Neomerx\JsonApi\Encoder\Encoder::encodeDataToArray()
     *
     * @param object|iterable|null $data Data to encode.
     *
     *
     * @SuppressWarnings(PHPMD.ElseExpression)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function encodeDataToArray($data): array
    {
        if (\is_array($data) === false && \is_object($data) === false && $data !== null) {
            throw new InvalidArgumentException();
        }

        /** @var EditableContext $context */
        $context = $this->getFactory()->createParserContext($this->getFieldSets(), $this->getIncludePaths());

        // OliveOil-CUSTOM
        if ($params = $this->getParams()) {
            $context->setRelationshipsCollectionConstraints($params->getRelationshipsCollectionConstraints());
        }

        // OliveOil-CUSTOM

        $parser  = $this->getFactory()->createParser($this->getSchemaContainer(), $context);
        $writer  = $this->createDocumentWriter();
        $filter  = $this->getFactory()->createFieldSetFilter($this->getFieldSets());

        // write header
        $this->writeHeader($writer);

        // write body
        foreach ($parser->parse($data, $this->getIncludePaths()) as $item) {
            if ($item instanceof ResourceInterface) {
                if ($item->getPosition()->getLevel() > ParserInterface::ROOT_LEVEL) {
                    if ($filter->shouldOutputRelationship($item->getPosition())) {
                        $writer->addResourceToIncluded($item, $filter);
                    }
                } else {
                    $writer->addResourceToData($item, $filter);
                }
            } elseif ($item instanceof IdentifierInterface) {
                \assert($item->getPosition()->getLevel() <= ParserInterface::ROOT_LEVEL);
                $writer->addIdentifierToData($item);
            } else {
                \assert($item instanceof DocumentDataInterface);
                \assert($item->getPosition()->getLevel() === 0);
                if ($item->isCollection()) {
                    $writer->setDataAsArray();
                } elseif ($item->isNull()) {
                    $writer->setNullToData();
                }
            }
        }

        // write footer
        $this->writeFooter($writer);

        return $writer->getDocument();
    }

    private function createDocumentWriter(): DocumentWriterInterface
    {
        $writer = $this->getFactory()->createDocumentWriter();
        $writer->setUrlPrefix($this->getUrlPrefix());

        return $writer;
    }

    /**
     * @return Params
     */
    public function getParams() {
        return $this->params;
    }

    public function setParams(Params $params): static {
        $this->params = $params;

        return $this;
    }
}
