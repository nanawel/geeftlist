<?php

namespace OliveOil\Core\Service\Rest;

use OliveOil\Core\Exception\ApiException;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

interface RestInterface
{
    /**
     * @throws NoSuchEntityException
     * @throws ApiException
     */
    public function get(RequestInterface $request, ResponseInterface $response);

    /**
     * @throws NoSuchEntityException
     * @throws ApiException
     */
    public function head(RequestInterface $request, ResponseInterface $response);

    /**
     * @throws NoSuchEntityException
     * @throws ApiException
     */
    public function patch(RequestInterface $request, ResponseInterface $response);

    /**
     * @throws NoSuchEntityException
     * @throws ApiException
     */
    public function post(RequestInterface $request, ResponseInterface $response);

    /**
     * @throws NoSuchEntityException
     * @throws ApiException
     */
    public function put(RequestInterface $request, ResponseInterface $response);

    /**
     * @throws NoSuchEntityException
     * @throws ApiException
     */
    public function delete(RequestInterface $request, ResponseInterface $response);

    /**
     * @return string[]
     */
    public function getSupportedVerbs(array $params = []);

    /*
     * RELATIONSHIPS
     */
    public function dispatchRelationshipsRequest(RequestInterface $request, ResponseInterface $response);
}
