<?php

namespace OliveOil\Core\Service;


interface RouteInterface
{
    /**
     * Internal redirect to the given $route, then exit.
     */
    public function forward(string $route, ?array $args = null, ?array $headers = null, mixed $body = null);

    /**
     * @param string[] $args
     */
    public function parseRouteArgs(array $args): array;

    public function migrateRoutes(): static;

    public function registerRestMapsToRoutes(): static;
}
