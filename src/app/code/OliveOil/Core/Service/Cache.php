<?php

namespace OliveOil\Core\Service;


use Cache\Adapter\Common\Exception\InvalidArgumentException;
use Cache\Adapter\Common\PhpCacheItem;
use Cache\Hierarchy\HierarchicalPoolInterface;
use Cache\TagInterop\TaggableCacheItemInterface;

class Cache implements SimpleCacheInterface, TaggableCacheInterface
{
    public function __construct(
        protected \Cache\Adapter\Common\AbstractCachePool $pool,
        // Cannot inject Log service here because of circular dependency issue
        protected \Psr\Log\LoggerInterface $logger,
        protected ?string $namespace = '',
        protected ?int $defaultTtl = null,
        protected bool $enableLog = false
    ) {
    }

    /**
     * @inheritDoc
     */
    public function set($key, $value, $ttl = null, array $tags = []) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.set', ['key' => $key]);
        }

        $this->prefixValue($key);

        /** @var TaggableCacheItemInterface $item */
        $item = $this->pool->getItem($key);
        $item->set($value);
        $item->expiresAfter($ttl ?? $this->defaultTtl);
        $item->setTags($tags);

        return $this->pool->save($item);
    }

    /**
     * @inheritDoc
     */
    public function has($key) {
        $this->prefixValue($key);

        return $this->pool->has($key);
    }

    /**
     * @inheritDoc
     */
    public function get($key, $default = null) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.get', ['key' => $key]);
        }

        $this->prefixValue($key);

        return $this->pool->get($key, $default);
    }

    /**
     * @inheritDoc
     */
    public function delete($key) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.delete', ['key' => $key]);
        }

        $this->prefixValue($key);

        return $this->pool->delete($key);
    }

    /**
     * @inheritDoc
     */
    public function clear() {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.clear');
        }

        if ($this->namespace) {
            return $this->pool->clear();
        }

        return $this->pool->deleteItem(HierarchicalPoolInterface::HIERARCHY_SEPARATOR . $this->namespace);
    }

    /**
     * @inheritDoc
     */
    public function getMultiple($keys, $default = null) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.getMultiple', ['keys' => $keys]);
        }

        $this->prefixValues($keys);

        return $this->pool->getMultiple($keys, $default);
    }

    /**
     * @inheritDoc
     */
    public function setMultiple($values, $ttl = null, array $tags = []) {
        // Must prefix keys first
        $prefixedValues = [];
        foreach ($values as $key => $value) {
            $this->prefixValue($key);
            $prefixedValues[$key] = $value;
        }

        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.setMultiple', ['keys' => array_keys($values)]);
        }

        $values = $prefixedValues;

        if (!$this->pool->setMultiple($values, $ttl ?? $this->defaultTtl)) {
            return false;
        }

        $return = true;

        /** @var PhpCacheItem[] $items */
        $items = $this->pool->getItems(array_keys($values));
        foreach ($items as $item) {
            $item->setTags($tags);
            $return = $return && $this->pool->save($item);
        }

        return $return;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple($keys) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.deleteMultiple', ['keys' => $keys]);
        }

        $this->prefixValues($keys);

        return $this->pool->deleteMultiple($keys);
    }

    /**
     * @inheritDoc
     */
    public function invalidateTag($tag) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.invalidateTag', ['tag' => $tag]);
        }

        return $this->pool->invalidateTag($tag);
    }

    /**
     * @inheritDoc
     */
    public function invalidateTags(array $tags) {
        if ($this->enableLog) {
            $this->logger->debug($this->namespace . '.invalidateTags', ['tags' => $tags]);
        }

        return $this->pool->invalidateTags($tags);
    }

    /**
     * Add namespace prefix on the key.
     *
     * @see \Cache\Namespaced\NamespacedCachePool::prefixValue
     *
     * @param string $key
     */
    private function prefixValue(&$key): void
    {
        if ($this->namespace) {
            // |namespace|key
            $key = HierarchicalPoolInterface::HIERARCHY_SEPARATOR
                . $this->namespace
                . HierarchicalPoolInterface::HIERARCHY_SEPARATOR
                . $key;
        }
    }

    /**
     * @see \Cache\Namespaced\NamespacedCachePool::prefixValues
     */
    private function prefixValues(iterable &$keys): void
    {
        if (!is_iterable($keys)) {
            throw new InvalidArgumentException('$keys is not iterable.');
        }

        if ($this->namespace) {
            foreach ($keys as &$key) {
                $this->prefixValue($key);
            }
        }
    }

    /**
     * @return $this
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger): static {
        $this->logger = $logger;

        return $this;
    }
}
