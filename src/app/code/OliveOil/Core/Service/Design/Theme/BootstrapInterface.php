<?php
namespace OliveOil\Core\Service\Design\Theme;

use OliveOil\Core\Service\Design;

interface BootstrapInterface
{
    public function bootstrap(Design $design);
}
