<?php

namespace OliveOil\Core\Service\Design\Theme;


use OliveOil\Core\Service\Design;

class Bootstrap implements BootstrapInterface
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\View\LayoutInterface $layout,
        protected string $bootstrapFile = '_init.php'
    ) {
    }

    public function bootstrap(Design $design): void {
        if ($bootstrapFile = $design->resolveTemplateFile($this->bootstrapFile, [], true)) {
            include $bootstrapFile;
        }
    }
}
