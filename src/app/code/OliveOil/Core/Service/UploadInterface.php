<?php

namespace OliveOil\Core\Service;


use OliveOil\Core\Exception\UploadException;
use OliveOil\Core\Model\Upload\File;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

interface UploadInterface
{
    public const OPTION_FILENAME = 'filename';

    public const OPTION_ACCEPT_INCOMING_EXT = 'accept_incoming_extension';

    /**
     * @param string $inputName
     * @param string $saveDir
     * @param array $options
     * @return File|null
     * @throws UploadException
     */
    public function upload($inputName, $saveDir, ErrorAggregatorInterface $errorAggregator, $options = []);
}
