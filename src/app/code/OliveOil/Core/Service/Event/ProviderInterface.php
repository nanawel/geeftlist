<?php

namespace OliveOil\Core\Service\Event;


interface ProviderInterface
{
    /**
     * @return array [[id, class, method, operator, priority, eventName, callback], ...]
     */
    public function getDefinitions();
}
