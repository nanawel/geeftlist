<?php

namespace OliveOil\Core\Service\Event\Provider;


use OliveOil\Core\Service\Event\ProviderInterface;

class Hive implements ProviderInterface
{
    /**
     * @param string $hiveKey
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\Event\StringDefinitionParser $parser,
        protected string $hiveKey = 'events'
    ) {
    }

    /**
     * @return array [[id, class, method, operator, priority, eventName, callback], ...]
     */
    public function getDefinitions(): array {
        $hiveValue = $this->fw->get($this->hiveKey);
        if (! is_array($hiveValue)) {
            return [];
        }

        $observers = array_filter($hiveValue, 'is_string');
        if (! is_array($observers)) {
            return [];
        }

        return $this->parser->parse($observers);
    }
}
