<?php

namespace OliveOil\Core\Service\Event;


class StringDefinitionParser
{
    /**
     * @param string[] $definitions
     * @return string[][] [id, class, method, operator, priority, eventName, callback]
     */
    public function parse(array $definitions): array {
        $return = [];
        foreach ($definitions as $uniqueListenerId => $definition) {
            if ($definition === 'disabled') {
                continue;
            }

            if ($parsedDefinition = $this->parseListenerDefinition($definition)) {
                $return[$uniqueListenerId] = $parsedDefinition;
            }
        }

        return $return;
    }

    /**
     * @param string $definition
     * @return string[]|false
     */
    protected function parseListenerDefinition($definition): array|false {
        $callbackPattern = '(?:(?P<class>[\w\d\\\\_]+)(?P<operator>::|\->))?(?P<method>[\w\d_]+)';
        $globalPattern = sprintf('/\[(?P<identifier>.*)\](\((?P<priority>\d+)\))?\s*(?P<eventName>[\w\d\-_\.:\/]+)\s*=\s*(?P<callback>%s)\s*$/i', $callbackPattern);
        if (preg_match($globalPattern, $definition, $matches)) {
            return array_filter($matches, static fn($k): bool => ! is_numeric($k), ARRAY_FILTER_USE_KEY);
        }

        return false;
    }
}
