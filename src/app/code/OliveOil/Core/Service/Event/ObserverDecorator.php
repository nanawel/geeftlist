<?php

namespace OliveOil\Core\Service\Event;


use Laminas\EventManager\EventInterface;

/**
 * Class ObserverDecorator
 *
 * Originally, this class was immutable. But using Container->make() to generate instances
 * was a little bottleneck as this class is heavily used in the framework.
 * So instead the method \OliveOil\Core\Service\Event::getDecoratedObserver() uses only one
 * instance as a prototype and clones it as needed, which is much faster.
 */
class ObserverDecorator implements \Stringable
{
    /** @var string|callable */
    protected $observerCallback;

    /** @var string */
    protected $listenerId;

    /** @var bool */
    protected $initialized = false;

    public function __construct(protected \Base $fw, protected \OliveOil\Core\Service\EventInterface $eventService)
    {
    }

    public function __invoke(EventInterface $event) {
        if ($this->eventService->areListenersDisabled()) {
            return null;
        }

        return $this->fw->call($this->observerCallback, [$event]);
    }

    public function __toString(): string {
        return sprintf('%s[%s]: %s', static::class, $this->listenerId, $this->observerCallback);
    }

    /**
     * @param callable|string $observerCallback
     * @param string $listenerId
     * @return $this
     */
    public function init($observerCallback, $listenerId): static {
        if ($this->initialized) {
            throw new \RuntimeException('Already initialized.');
        }

        $this->observerCallback = $observerCallback;
        $this->listenerId = $listenerId;

        return $this;
    }
}
