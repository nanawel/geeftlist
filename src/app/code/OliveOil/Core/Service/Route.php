<?php

namespace OliveOil\Core\Service;


class Route implements RouteInterface
{
    public const DEFAULT_CONTROLLER = 'index';
    public const DEFAULT_ACTION     = 'index';

    public function __construct(
        protected \Base $fw,
        protected array $params = []
    ) {
    }

    public function forward(string $route, array $args = null, array $headers = null, $body = null): void {
        $fw = $this->fw;

        /* @see \Base::reroute() */
        if (
            ($handler = $fw->get('ONFORWARD'))
            && $fw->call($handler, func_get_args()) === false
        ) {
            return;
        }

        if (!preg_match('/^[A-Z]+ .*/', $route)) {
            $route = sprintf('GET %s', $route);
        }

        $fw->mock($route, $args, $headers, $body);

        /* @see \Base::reroute() too */
        if (
            ($handler = $fw->get('ONFORWARD_AFTER'))
            && $fw->call($handler, func_get_args()) === false
        ) {
            return;
        }

        exit;
    }

    /**
     * @inheritDoc
     */
    public function parseRouteArgs(array $args): array {
        $namedArgs = $args;
        unset($namedArgs[0]);

        $parts = explode('/', trim($args[0], '/'));

        $routeParts = [];
        $routeParts['prefix'] = $this->extractPrefix($namedArgs, $parts);
        $routeParts['controller'] = $this->extractController($namedArgs, $parts);
        $routeParts['action'] = $this->extractAction($namedArgs, $parts);
        $routeParts['route_params'] = $this->extractRouteParams($namedArgs, $parts);

        return $routeParts;
    }

    /**
     * @param string[] $namedArgs
     * @param string[] $routeParts
     */
    protected function extractPrefix(array &$namedArgs, array &$routeParts): ?string {
        if ($routeParts !== []) {
            foreach (array_keys($this->getRouteMapping()) as $prefix) {
                if ($routeParts[0] === trim($prefix, '/')) {
                    array_shift($routeParts);

                    return $prefix;
                }
            }
        }

        return null;
    }

    /**
     * @param string[] $namedArgs
     * @param string[] $routeParts
     */
    protected function extractController(array &$namedArgs, array &$routeParts): ?string {
        $controller = array_shift($routeParts);
        if ($controller === null || $controller === '') {
            $controller = static::DEFAULT_CONTROLLER;
        }

        return $controller;
    }

    /**
     * @param string[] $namedArgs
     * @param string[] $routeParts
     */
    protected function extractAction(array &$namedArgs, array &$routeParts): ?string {
        $action = array_shift($routeParts);
        if ($action === null) {
            $action = static::DEFAULT_ACTION;
        }

        return $action;
    }

    /**
     * @param string[] $namedArgs
     * @param string[] $routeParts
     */
    protected function extractRouteParams(array &$namedArgs, array &$routeParts): array {
        return array_column(array_chunk($routeParts, 2), 1, 0);
    }

    public function migrateRoutes(): static {
        $fw = $this->fw;

        // Adapt routes using prefix
        $routes =& $fw->ref('ROUTES');
        foreach ($this->getRouteMapping() as $prefix => $replace) {
            $replace = (string)$replace;
            if ($routes && $prefix != $replace) {
                foreach ($routes as $routeKey => $routeDefinition) {
                    $newRouteKey = preg_replace('#^' . preg_quote($prefix, '#') . '#', $replace, (string) $routeKey);
                    $routes[$newRouteKey] = $routeDefinition;
                    unset($routes[$routeKey]);
                }
            }
        }

        return $this;
    }

    public function registerRestMapsToRoutes(): static {
        foreach ($this->getRouteMapping() as $prefix => $replace) {
            // Convert REST map directives to routes
            if ($maps = $this->fw->get('rest_maps')) {
                foreach ($maps as $route => $handler) {
                    $mapArgs = is_string($handler) ? [$handler] : $handler;
                    $newRoute = preg_replace('/^' . preg_quote($prefix, '/') . '/', $replace, (string) $route);
                    array_unshift($mapArgs, $newRoute);
                    $this->fw->map(...$mapArgs);
                }
            }
        }

        return $this;
    }

    /**
     * @return string[]
     */
    protected function getRouteMapping(): array {
        if (! empty($this->params['route_mapping'])) {
            return $this->params['route_mapping'] ?? [];
        }

        return [];
    }
}
