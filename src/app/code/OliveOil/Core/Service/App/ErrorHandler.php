<?php

namespace OliveOil\Core\Service\App;


use OliveOil\Core\Model\App\Error;
use OliveOil\Core\Test\Util\Exception\Reroute;

/**
 * Custom error handler. Intended behavior is as follows:
 * IF handling_error=1 THEN
 *   return FALSE (let F3 handle the error for us)
 * ELSE
 *   Set handling_error=1
 *   IF CLI THEN
 *     Print error to STDERR
 *     <Exit> with code from error (F3 cannot do that)
 *   ELSE
 *     IF Non-AJAX HTTP request THEN
 *       Add the error message to the current session
 *       IF there's a referer THEN
 *         Redirect to it (message will be displayed in the callout area)
 *         <Exit> (implied by reroute)
 *       ELSE
 *         Redirect to error/index (which will display the error message in session)
 *         <Exit> (implied by reroute)
 *     ELSE (AJAX request)
 *
 *
 *   Set handling_error=0
 */
class ErrorHandler implements ErrorHandlerInterface
{
    protected \Psr\Log\LoggerInterface $logger;

    protected bool $handlingError = false;

    /**
     * @param callable[] $customErrorHandlers
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        protected \OliveOil\Core\Service\Http\ResponseWriterInterface $responseWriter,
        protected \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        protected \OliveOil\Core\Model\App\Error\Builder $errorBuilder,
        \OliveOil\Core\Service\Log $logService,
        protected array $customErrorHandlers = []
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @inheritDoc
     */
    final public function register($force = false) {
        set_error_handler([self::class, 'exceptionErrorHandler']);
        if ($force || ! $this->fw->get('ONERROR')) {
            $this->fw->set('ONERROR', $this->getErrorHandler());
        }

        return $this;
    }


    /**
     * @inheritDoc
     */
    public function unregister(): static {
        // Clear previous error in session if relative to a different request
        if (
            !$this->fw->get('CLI')
            && ($defaultSession = $this->sessionManager->getSession())
            && ($previousError = $defaultSession->getPreviousError())
        ) {
            /** @var Error $previousError */
            if ($previousError->getRequestId() != $this->errorBuilder->getRequestId()) {
                $defaultSession->unsPreviousError();
            }
        }

        // Only unregister our own handler
        if ($this->fw->get('ONERROR') === $this->getErrorHandler()) {
            $this->fw->clear('ONERROR');
        }

        return $this;
    }

    protected function getErrorHandler(): callable {
        return fn(\Base $fw): bool => $this->handle($fw);
    }

    /**
     * @throws \ErrorException
     */
    public static function exceptionErrorHandler(int $severity, string $message, string $file, int $line): void {
        if ((error_reporting() & $severity) === 0) {
            // This error code is not included in error_reporting
            return;
        }

        throw new \ErrorException($message, 0, $severity, $file, $line);
    }

    /**
     * @return bool TRUE to stop further processing by F3 (see \Base::error)
     * @throws Reroute Only while running UT
     */
    public function handle(\Base $fw) {
        $error = $this->errorBuilder->toError($fw->get('EXCEPTION') ?? $fw->get('ERROR'));
        if (! $this->canHandle($error)) {
            // Can't handle this error, hope F3 will
            return false;
        }

        try {
            $this->handlingError = true;

            $this->logError($error);

            if ($this->triggerCustomErrorHandlers($error)) {
                return true;
            }

            if ($this->fw->get('CLI')) {
                $this->handleErrorCli($error);
            }

            return $this->handleErrorHttp($error);
        }
        catch (Reroute $reroute) {
            // Special exception only for unit tests execution, must be ignored
            throw $reroute;
        }
        catch (\Throwable $e) {
            // Must clear the previous error otherwise F3 won't print it
            $this->fw->clear('ERROR');
            $this->logError(
                $this->errorBuilder->toError(new \Exception('Uh... got exception while handling error!', 0, $e))
            );

            return false;
        }
        finally {
            $this->handlingError = false;
        }
    }

    protected function triggerCustomErrorHandlers(Error $error): bool {
        foreach ($this->customErrorHandlers as $customErrorHandler) {
            if ($customErrorHandler($error)) {
                return true;
            }
        }

        return false;
    }

    protected function handleErrorCli(Error $error): never {
        $this->logError($error);

        $this->printErrorCli($error);

        // F3 does not let us return a custom value, so we die here
        die($error->getCode() ?: 255);
    }

    protected function printErrorCli(Error $error): static {
        fwrite(
            STDERR,
            sprintf(
                "%s (%s) %s\n%s\n%s\n",
                'ERROR',
                $error->getType(),
                print_r($error->getCode(), true),
                print_r($error->getMessage(), true),
                $error->getTrace()
            )
        );

        return $this;
    }

    protected function handleErrorHttp(Error $error): bool {
        if ($this->fw->get('AJAX')) {
            $this->printErrorJson($error);

            return true;
        }

        // Print HTTP errors directly
        if ($error->isHttp()) {
            $this->printErrorHttp($error);

            return true;
        }

        $session = $this->sessionManager->getSession();
        $session->addErrorMessage($error->getUserMessage());
        // Redirect to referer only if no previous error is present in session (avoids loop)
        if (($referer = $this->getReferer()) && !$session->getPreviousError()) {
            $session->setPreviousError($error);
            if ($this->appConfig->getValue('ERROR_USE_REDIRECT', true)) {
                $this->responseRedirect->rerouteUrl($referer);
            }
        }

        if ($this->isDeveloperMode()) {
            $this->printErrorHttp($error);

            return true;
        }

        $this->responseRedirect->reroute($this->appConfig->getValue('ERROR_STATIC_500_PAGE'));

        // Not used because of reroute() above, but just to be clean
        return true;
    }

    protected function getReferer(): string {
        $referer = $this->requestManager->getRequest()->getPostParam('referer')
            ?: $this->urlBuilder->getQueryReferer()
            ?: $this->urlBuilder->getReferer();
        $baseUrl = $this->appConfig->getValue('BASE_URL');

        if ($referer !== null && str_starts_with((string) $referer, (string) $baseUrl)) {
            return $referer;
        }

        return $this->getRefererFallbackUrl();
    }

    protected function getRefererFallbackUrl(): string {
        return $this->urlBuilder->getUrl(
            $this->appConfig->getValue('ERROR_FALLBACK_REDIRECT_ROUTE') ?: '/'
        );
    }

    /**
     * Prints an error in an HTTP context (i.e. as an HTML page)
     */
    protected function printErrorHttp(Error $error): static {
        $fw = $this->fw;

        http_response_code($error->getHttpCode());  // FIXME: Should be handled by a ResponseWriter instead

        $errorData = $error->toArray(true);
        echo preg_replace_callback('/(\${(?P<varName>[\w\.]+)})/', static function (array $matches) use ($errorData, $fw) {
            if (isset($matches['varName']) && ($matches['varName'] !== '' && $matches['varName'] !== '0') && isset($errorData[$matches['varName']])) {
                return $fw->encode($errorData[$matches['varName']]);
            }
        }, $this->getErrorTemplate());

        return $this;
    }

    /**
     * Prints an error as JSON in an HTTP/AJAX context
     */
    protected function printErrorJson(Error $error): static {
        if (! headers_sent()) {
            http_response_code($error->getHttpCode());  // FIXME: Should be handled by a ResponseWriter instead
            header('Content-Type: application/json');   // FIXME: Should be handled by a ResponseWriter instead
        }

        echo json_encode($error->toArray(true));        // FIXME: Should be handled by a ResponseWriter instead

        return $this;
    }

    protected function logError(Error $error): static {
        if (! $this->shouldLog($error)) {
            return $this;
        }

        if ($error->getSourceError() instanceof \Throwable) {
            $this->logger->critical(
                sprintf('[%s]', $error->getId()),   // No need to include message as it's already in trace below
                // Must use exception_full_trace wrapper to get inner exceptions
                // => \Monolog\Formatter\LineFormatter::normalizeException uses getTraceAsString() which does not
                //    return them.
                ['exception' => \OliveOil\exception_full_trace($error->getSourceError())]
            );
        }
        else {
            $this->logger->error(static::getContextAsString());
            $this->logger->error(sprintf(
                "[%s] %s (%s): %s\n%s\n%s",
                $error->getId(),
                $error->getType(),
                $error->getCode(),
                $error->getMessage(),
                $error->getDetail(),
                $error->getTrace()
            ));
        }

        return $this;
    }

    protected function getErrorTemplate(): string {
        return <<<'EOT'
<!DOCTYPE html>
<html>
    <head><title>${type}</title></head>
    <body>
        <h1>${userMessage}</h1>
        <p>${userDetail}</p>
        <pre>${userTrace}</pre>
        <pre>ID: ${id}</pre>
    </body>
</html>
EOT;
    }

    protected function canHandle(Error $error): bool {
        return ! $this->handlingError;
    }

    protected function shouldLog(Error $error): bool {
        return ! $error->isHttp()
            || $this->appConfig->getValue('LOG_HTTP_ERRORS');
    }

    public function isDeveloperMode(): bool {
        return $this->appConfig->getValue('MODE') === 'developer';
    }

    public static function getContextAsString(): string {
        return match (PHP_SAPI) {
            'cli' => sprintf(
                '[CLI] %s',
                implode(' ', $_SERVER['argv'] ?? [])
            ),
            default => sprintf(
                '[%s] %s %s %s',
                strtoupper(PHP_SAPI),
                $_SERVER['HTTP_HOST'] ?? '',
                $_SERVER['REQUEST_URI'] ?? '',
                $_SERVER['QUERY_STRING'] ?? ''
            ),
        };
    }
}
