<?php

namespace OliveOil\Core\Service\App\Api\Rest\JsonApi;


use OliveOil\Core\Model\App\Error;
use OliveOil\Core\Model\Rest\ResponseInterface;

class ErrorHandler extends \OliveOil\Core\Service\App\ErrorHandler
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \OliveOil\Core\Service\Http\ResponseWriterInterface $responseWriter,
        \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        \OliveOil\Core\Model\App\Error\Builder $errorBuilder,
        protected \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface $jsonApiEncoder,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $fw,
            $appConfig,
            $sessionManager,
            $urlBuilder,
            $requestManager,
            $responseWriter,
            $responseRedirect,
            $errorBuilder,
            $logService
        );
    }

    protected function handleErrorHttp(Error $error): bool {
        $this->printErrorHttp($error);

        // Stop further processing
        return true;
    }

    protected function printErrorHttp(Error $error): static {
        /** @var ResponseInterface $response */
        $response = $this->requestManager->newResponse();

        $response->setCode($error->getHttpCode())
            ->setData($this->jsonApiEncoder->encodeError($this->toJsonApiError($error)));

        $this->responseWriter->write($response);

        return $this;
    }

    protected function toJsonApiError(Error $error): \Neomerx\JsonApi\Schema\Error {
        /** @var \Neomerx\JsonApi\Schema\Error $jsonApiError */
        $jsonApiError = $this->coreFactory->make(\Neomerx\JsonApi\Schema\Error::class);

        return $jsonApiError->setStatus($error->getHttpCode())
            ->setCode($error->getType())
            ->setTitle($error->getUserMessage())
            ->setDetail($error->getUserDetail())
            ->setMeta([
                'trace' => explode("\n", $error->getUserTrace()),
            ]);
    }
}
