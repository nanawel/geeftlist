<?php

namespace OliveOil\Core\Service\App;

interface ErrorHandlerInterface
{
    /**
     * @return $this
     */
    public function register($force = false);

    /**
     * @return $this
     */
    public function unregister();

    /**
     * @return bool FALSE to stop further processing
     */
    public function handle(\Base $fw);
}
