<?php

namespace OliveOil\Core;


interface BootstrapInterface
{
    /**
     * @return mixed
     */
    public function run();
}
