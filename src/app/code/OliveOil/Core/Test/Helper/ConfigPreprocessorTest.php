<?php

namespace OliveOil\Core\Test\Helper;


use OliveOil\Core\Exception\InvalidConfigurationException;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigPreprocessorTest
 *
 * @group oliveoil
 * @group helpers
 */
class ConfigPreprocessorTest extends TestCase
{
    use TestCaseTrait;

    protected function getCut() {
        return $this->getContainer()->get(\OliveOil\Core\Helper\ConfigPreprocessor::class);
    }

    public function testRunVars(): void {
        $string = 'Lorem ipsum ${a} dolor ${b} sit ${a}met ${c}';
        $vars = [
            'a' => 'A',
            'b' => 'B',
            'd' => 'D'
        ];

        $this->assertEquals(
            'Lorem ipsum A dolor B sit Amet ${c}',
            $this->getCut()->run($string, $vars)
        );
    }

    public function testRunFwHive(): void {
        $prefix = preg_replace('/[^\w\d]/', '_', self::class);

        $string = 'Lorem ipsum ${' . $prefix . '.a} dolor ${' . $prefix . '.b} '
            . 'sit ${' . $prefix . '.d} met';
        $this->getFw()->mset(
            [
                '.a' => 'A',
                '.b' => 'B',
                '.d' => 'D'
            ],
            $prefix
        );

        $this->assertEquals(
            'Lorem ipsum A dolor B '
            . 'sit D met',
            $this->getCut()->run($string)
        );
    }

    public function testRunFwHive_missingValue(): void {
        $prefix = preg_replace('/[^\w\d]/', '_', self::class);

        $string = 'Lorem ipsum ${' . $prefix . '.e} dolor ${' . $prefix . '.f}';
        $this->getFw()->mset(
            [
                '.e' => 'E',
            ],
            $prefix
        );

        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage(sprintf(
            "Cannot resolve variable '%s.f'. Is it defined?",
            $prefix
        ));

        $this->getCut()->run($string);
    }
}
