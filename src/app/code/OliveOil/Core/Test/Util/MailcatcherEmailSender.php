<?php

namespace OliveOil\Core\Test\Util;


use OliveOil\Core\Service\Email\Sender\SymfonyMailerSender;

class MailcatcherEmailSender extends SymfonyMailerSender
{
    public function __construct(
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\Log $logService,
        \Symfony\Component\Mailer\MailerInterface $mailer,
        protected \OliveOil\Core\Test\Util\MailcatcherConnection $mailcatcherConnection
    ) {
        parent::__construct($appConfig, $logService, $mailer);
    }

    /**
     * @param bool $clear
     * @return array
     */
    public function getSentEmails($clear = false) {
        $messages = $this->mailcatcherConnection->getMessages();
        foreach (array_keys($messages) as $i) {
            foreach ($messages[$i]['recipients'] as $j => $recipient) {
                if (preg_match('/^((?P<name>.*?)\s+)?<(?P<address>[^ ]+)>/i', (string) $recipient, $matches)) {
                    $recipient = [
                        'address' => $matches['address'],
                        'name'    => $matches['name'] ?? $matches['address']
                    ];
                }

                $messages[$i]['recipients'][$j] = $recipient;
            }

            // Compatibility with the old implementation
            $messages[$i]['from'] = $this->toSymfonyAddress($messages[$i]['sender'])->getAddress();
            if  (count($messages[$i]['recipients']) == 1) {
                $messages[$i]['to'] = $messages[$i]['recipients'][0]['address'];
            }

            // Issue 2020-11-08
            if (!isset($messages[$i]['source'])) {
                throw new \UnexpectedValueException('Missing message source in: ' . json_encode($messages[$i]));
            }

            // Really *really* simple and optimistic email parsing
            preg_match('/^(?P<headers>.*?)\r\n\r\n(?P<body>.*)$/s', (string) $messages[$i]['source'], $matches);
            $messages[$i]['body'] = quoted_printable_decode(trim($matches['body']));
            foreach (explode("\r\n", $matches['headers']) as $header) {
                // Not a new line, just the continuation of the previous
                if (preg_match('/^\s+(.*)/i', $header, $matches) && isset($lastValue)) {
                    $lastValue .= $matches[1];
                }
                else {
                    unset($lastValue);

                    preg_match('/^([\w\-_]+): (.*)$/i', $header, $matches);
                    $headerName = strtolower(str_replace('-', '_', $matches[1]));
                    $headerValue = $matches[2];

                    if (! array_key_exists($headerName, $messages[$i])) {
                        $messages[$i][$headerName] = $headerValue;
                    }

                    $lastValue =& $headerValue;
                }
            }
        }

        if ($clear) {
            $this->clearSentEmails();
        }

        return $messages;
    }

    /**
     * @return $this
     */
    public function clearSentEmails(): static {
        $this->mailcatcherConnection->clearMessages();

        return $this;
    }
}
