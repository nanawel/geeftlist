<?php

namespace OliveOil\Core\Test\Util\PHPUnit;


use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\TestSuite;
use PHPUnit\Framework\Warning;

class TestListenerAdapter implements TestListener
{
    /**
     * An error occurred.
     *
     * @param float $time
     */
    public function addError(Test $test, \Throwable $t, $time): void {}

    /**
     * A warning occurred.
     *
     * @param float $time
     */
    public function addWarning(Test $test, Warning $e, $time): void {}

    /**
     * A failure occurred.
     *
     * @param float $time
     */
    public function addFailure(Test $test, AssertionFailedError $e, $time): void {}

    /**
     * Incomplete test.
     *
     * @param float $time
     */
    public function addIncompleteTest(Test $test, \Throwable $t, $time): void {}

    /**
     * Risky test.
     *
     * @param float $time
     */
    public function addRiskyTest(Test $test, \Throwable $t, $time): void {}

    /**
     * Skipped test.
     *
     * @param float $time
     */
    public function addSkippedTest(Test $test, \Throwable $t, $time): void {}

    /**
     * A test suite started.
     */
    public function startTestSuite(TestSuite $suite): void {}

    /**
     * A test suite ended.
     */
    public function endTestSuite(TestSuite $suite): void {}

    /**
     * A test started.
     */
    public function startTest(Test $test): void {}

    /**
     * A test ended.
     *
     * @param float $time
     */
    public function endTest(Test $test, $time): void {}
}
