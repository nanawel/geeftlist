<?php

namespace OliveOil\Core\Test\Util\Exception;

class Reroute extends \Exception
{
    /**
     * @param bool $permanent
     */
    public function __construct($url, protected $permanent, \Exception $previous = null) {
        parent::__construct($url, 0, $previous);
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->message;
    }

    /**
     * @return bool|\Exception
     */
    public function getPermanent() {
        return $this->permanent;
    }
}
