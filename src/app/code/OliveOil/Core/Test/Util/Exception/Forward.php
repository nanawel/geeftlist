<?php

namespace OliveOil\Core\Test\Util\Exception;

class Forward extends \Exception
{
    /** @var string */
    protected $route;

    public function __construct($route, protected array $args = [], \Exception $previous = null) {
        parent::__construct('Request has been forwarded to: ' . $route, 0, $previous);
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getRoute() {
        return $this->route;
    }

    public function getArgs(): array {
        return $this->args;
    }
}
