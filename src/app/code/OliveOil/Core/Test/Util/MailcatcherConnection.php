<?php

namespace OliveOil\Core\Test\Util;


/**
 * Class MailcatcherConnection
 *
 * @see https://mailcatcher.me/
 */
class MailcatcherConnection
{
    public const FORMAT_PLAIN  = 'plain';

    public const FORMAT_JSON   = 'json';

    public const FORMAT_HTML   = 'html';

    public const FORMAT_SOURCE = 'source';

    /**
     * MailcatcherConnection constructor.
     *
     * @param string $host
     * @param int $port WebUI port (default: 1080)
     * @param bool $tls
     */
    public function __construct(protected $host = 'localhost', protected $port = 1080, protected $tls = false)
    {
    }

    /**
     *
     * @return array
     */
    public function getMessages($metadataOnly = false) {
        $messages = json_decode($this->get("messages"), true);
        if (! $metadataOnly) {
            foreach ($messages as $i => $message) {
                $messages[$i] = json_decode(
                    $this->get(sprintf('messages/%s.', $message['id']) . self::FORMAT_JSON),
                    true
                );
                // Mailhog 7+: Retrieve source from dedicated URL
                if (!isset($messages[$i]['source'])) {
                    $messages[$i]['source'] = $this->get(sprintf('messages/%s.', $message['id']) . self::FORMAT_SOURCE);
                }
            }
        }

        return $messages;
    }

    public function clearMessages(): void {
        $this->delete('messages');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getMessage($id) {
        return json_decode($this->getMessageWithFormat($id, self::FORMAT_JSON));
    }

    /**
     * @param int $id
     * @param string $format
     * @return string
     */
    public function getMessageWithFormat($id, $format = self::FORMAT_JSON): string|false {
        return $this->get(sprintf('messages/%d.%s', $id, $format));
    }

    /**
     * @param string $req
     * @return string
     */
    protected function get($req): string|false {
        return file_get_contents($this->getUrl($req));
    }

    protected function delete($req): bool|string {
        $url = $this->getUrl($req);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $result = curl_exec($ch);

        if (!str_starts_with($httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE), '2')) {
            throw new \Exception(sprintf('Unexpected code %d while attempting to access %s', $httpCode, $url));
        }

        return $result;
    }

    protected function getUrl($req = ''): string {
        return ($this->tls ? 'https' : 'http') . sprintf('://%s:%d/%s', $this->host, $this->port, $req);
    }
}
