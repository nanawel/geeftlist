<?php


namespace OliveOil\Core\Test\Util\Rest;


use Art4\JsonApiClient\Accessable;
use Art4\JsonApiClient\Manager;
use OliveOil\Core\Model\Rest\ResponseInterface;

trait JsonApiTrait
{
    /**
     * @param ResponseInterface| string $response
     * @return Accessable
     * @throws \LogicException
     */
    public function toJsonApiDocument($response) {
        if ($response instanceof ResponseInterface) {
            $response = $response->getBody();
        }

        /** @var Manager $manager */
        $manager = $this->getContainer()->get(\Art4\JsonApiClient\Manager::class);

        return $manager->parse($this->getContainer()->make(
            \Art4\JsonApiClient\Input\ResponseStringInput::class,
            ['string' => $response]
        ));
    }
}