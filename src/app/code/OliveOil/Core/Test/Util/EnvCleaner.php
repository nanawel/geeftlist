<?php

namespace OliveOil\Core\Test\Util;

use OliveOil\Core\Model\Cache\VolatileArrayObject;
use OliveOil\Core\Model\Repository;
use OliveOil\Core\Service\EventInterface;
use OliveOil\Core\Test\Util\PHPUnit\TestListenerAdapter;

class EnvCleaner extends TestListenerAdapter
{
    /**
     * @param float $time
     */
    public function endTest(\PHPUnit\Framework\Test $test, $time): void {
        try {
            VolatileArrayObject::clearAllObjects();

            Repository::clearAllModelCache();

            if (is_callable([$test, 'getRegistry'])) {
                $test->getRegistry()->clear('current_controller');
            }

            if (is_callable([$test, 'deleteCutEntities'])) {
                $test->deleteCutEntities();
            }

            if (is_callable([$test, 'getEmailSender'])
                && is_callable([$test->getEmailSender(), 'clearSentEmails'])
            ) {
                $test->getEmailSender()->clearSentEmails();
            }

            if (is_callable([$test, 'enableListeners'])) {
                $test->enableListeners(EventInterface::EVENT_SPACE_WILDCARD);
            }
        } catch (\Throwable $e) {
            // Only printing the exception here because there might be another one raised
            // by a test itself, and we don't want to hide it.
            echo "An unexpected error has occurred in EnvCleaner::endTest(): {$e->getMessage()}";
            echo $e->getTraceAsString();
        }
    }
}
