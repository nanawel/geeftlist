<?php

namespace OliveOil\Core\Test\Util;

use OliveOil\Core\Helper\DateTime;
use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\Warning;

class TestLogger implements TestListener
{
    /** @var float[] */
    protected $testStartTime = [];

    /** @var float[] */
    protected $suiteStartTime = [];

    /**
     * @param string $outputFile
     */
    public function __construct(protected $outputFile)
    {
    }

    public function addError(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->writeToFile(
            sprintf("    ERROR: %s (%s:%d)\n", $t->getMessage(), $t->getFile(), $t->getLine())
        );
    }

    public function addWarning(Test $test, Warning $e, $time): void {
        $this->writeToFile(
            sprintf("    WARN: %s (%s:%d)\n", $e->getMessage(), $e->getFile(), $e->getLine())
        );
    }

    public function addFailure(\PHPUnit\Framework\Test $test, \PHPUnit\Framework\AssertionFailedError $e, $time): void {
        $this->writeToFile(
            sprintf("    FAILED: %s (%s:%d)\n", $e->getMessage(), $e->getFile(), $e->getLine())
        );
    }

    public function addIncompleteTest(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->writeToFile(
            "    INCOMPLETE\n"
        );
    }

    public function addRiskyTest(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->writeToFile(
            sprintf("    RISKY: %s (%s:%d)\n", $t->getMessage(), $t->getFile(), $t->getLine())
        );
    }

    public function addSkippedTest(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->writeToFile(
            sprintf("    SKIPPED: %s (%s:%d)\n", $t->getMessage(), $t->getFile(), $t->getLine())
        );
    }

    public function startTest(\PHPUnit\Framework\Test $test): void {
        $this->testStartTime[] = microtime(true);
        $this->writeToFile(sprintf(
            "  * %s [mem = %s]\n",
            $test instanceof TestCase ? $test->getName() : '<unknown>',
            round(memory_get_usage(true) / 1e3, 2)
        ));
    }

    public function endTest(\PHPUnit\Framework\Test $test, $time): void {
        $time = microtime(true) - array_pop($this->testStartTime);
        $this->writeToFile(sprintf(
            "    Finished (%s) [mem = %s]\n",
            \OliveOil\Core\Helper\DateTime::secondsToTime($time),
            round(memory_get_usage(true) / 1e3, 2)
        ));
    }

    public function startTestSuite(\PHPUnit\Framework\TestSuite $suite): void {
        $stackWasEmpty = $this->suiteStartTime === [];
        $this->suiteStartTime[] = microtime(true);
        if ($stackWasEmpty) {
            $this->printHeader();
            return;
        }

        $this->writeToFile(
            sprintf("¤ %s\n", $suite->getName())
        );
    }

    public function endTestSuite(\PHPUnit\Framework\TestSuite $suite): void {
        $time = microtime(true) - array_pop($this->suiteStartTime);
        if ($this->suiteStartTime === []) {
            $this->printFooter($time);
            return;
        }

        $this->writeToFile(
            sprintf("  Finished (%s)\n", \OliveOil\Core\Helper\DateTime::secondsToTime($time))
        );
    }

    protected function writeToFile($data, $opts = FILE_APPEND): static {
        file_put_contents(
            $this->getOutputFile(),
            $data,
            $opts
        );

        return $this;
    }

    protected function getOutputFile() {
        return $this->outputFile;
    }

    protected function printHeader($resetFile = true) {
        $this->writeToFile(
            sprintf(
                "===================== TEST EXECUTION STARTED on %s ======================\n",
                DateTime::getDateIso()
            ),
            $resetFile ? 0 : FILE_APPEND
        );
    }

    protected function printFooter($time) {
        $banner = sprintf(
            "===================== TEST EXECUTION FINISHED on %s =====================",
            DateTime::getDateIso()
        );
        $this->writeToFile(
            sprintf(
                '%s
 TOTAL TIME: %s
%s
',
                $banner,
                DateTime::secondsToTime($time),
                str_repeat('=', strlen($banner))
            ),
            FILE_APPEND
        );
    }
}
