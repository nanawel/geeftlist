<?php

namespace OliveOil\Core\Test\Util;

use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\Warning;

/**
 * Class CsvTestLogger
 */
class CsvTestLogger implements TestListener
{
    public const EMPTY_VALUE = '';

    /** @var resource */
    protected $outputFileHandler;

    /** @var bool */
    protected $headersWritten = false;

    /** @var float[] */
    protected $suiteStartTime = [];

    /** @var \PHPUnit\Framework\TestSuite|null */
    protected $currentTestSuite;

    /** @var array */
    protected $testResults = [];

    /** @var array */
    protected $currentTestData;

    /**
     * @param string $outputFile
     */
    public function __construct(protected $outputFile)
    {
    }

    public function addError(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->currentTestData['error'] = 1;
        $this->addException($t);
    }

    public function addWarning(Test $test, Warning $e, $time): void {
        $this->currentTestData['warning'] = 1;
        $this->addException($e);
    }

    public function addFailure(\PHPUnit\Framework\Test $test, \PHPUnit\Framework\AssertionFailedError $e, $time): void {
        $this->currentTestData['failure'] = 1;
        $this->addException($e);
    }

    public function addIncompleteTest(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->currentTestData['incomplete'] = 1;
        $this->addException($t);
    }

    public function addRiskyTest(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->currentTestData['risky'] = 1;
        $this->addException($t);
    }

    public function addSkippedTest(\PHPUnit\Framework\Test $test, \Throwable $t, $time): void {
        $this->currentTestData['skipped'] = 1;
        $this->addException($t);
    }

    public function startTest(\PHPUnit\Framework\Test $test): void {
        $this->startNewTestData();
        $this->currentTestData['suite'] = $this->currentTestSuite->getName();
        $this->currentTestData['test'] = $test instanceof TestCase ? $test->getName() : '<unknown>';
        $this->currentTestData['start_time'] = microtime(true);
        $this->currentTestData['start_mem'] = round(memory_get_usage(true) / 1e3, 2);

    }

    public function endTest(\PHPUnit\Framework\Test $test, $time): void {
        $this->currentTestData['end_time'] = microtime(true);
        $this->currentTestData['end_mem'] = round(memory_get_usage(true) / 1e3, 2);
        $this->currentTestData['delta_mem'] = $this->currentTestData['end_mem'] - $this->currentTestData['start_mem'];
        $this->currentTestData['#'] = count($this->testResults);
        $this->currentTestData['time'] = $this->currentTestData['end_time'] - $this->currentTestData['start_time'];

        $this->testResults[] = $this->currentTestData;
    }

    public function startTestSuite(\PHPUnit\Framework\TestSuite $suite): void {
        $this->suiteStartTime[] = microtime(true);
        $this->currentTestSuite = $suite;
    }

    public function endTestSuite(\PHPUnit\Framework\TestSuite $suite): void {
        $this->currentTestSuite = null;
        if (! $this->headersWritten) {
            $this->printHeader();
        }

        $this->writeToFile($this->testResults);
        $this->testResults = [];
    }

    protected function writeToFile($rows) {
        // Force locale to output numbers in English format
        $loc = setlocale(LC_NUMERIC, 0);
        setlocale(LC_NUMERIC, 'C');

        foreach ($rows as $row) {
            fputcsv($this->getOutputFileHandler(), $row, ',', '"');
        }

        // Restore locale
        setlocale(LC_NUMERIC, $loc);
    }

    /**
     * @return resource
     */
    protected function getOutputFileHandler($openMode = 'w') {
        if (! $this->outputFileHandler && $openMode) {
            $this->outputFileHandler = fopen($this->outputFile, $openMode);
        }

        return $this->outputFileHandler;
    }

    protected function printHeader() {
        $this->writeToFile([$this->getHeaders()]);
        $this->headersWritten = true;
    }

    protected function startNewTestData() {
        unset($this->currentTestData);
        $this->currentTestData = array_flip($this->getHeaders());
        foreach (array_keys($this->currentTestData) as $k) {
            $this->currentTestData[$k] = self::EMPTY_VALUE;
        }
    }

    /**
     * @return void
     */
    protected function addException(\Throwable $e) {
        if ($e instanceof AssertionFailedError) {
            $trace = $e->getTrace();
            $traceEl = array_shift($trace);
            // Workaround to get the *actual* location when $e is an assertion exception
            while (isset($traceEl['file'])
                && (str_contains($traceEl['file'], 'phpunit' . DIRECTORY_SEPARATOR . 'phpunit')
                || str_contains($traceEl['file'], 'OliveOil/Core/Test/Util'))
            ) {
                $traceEl = array_shift($trace);
            }
        }

        // Catch-all fallback
        if (!isset($traceEl)) {
            $traceEl = [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ];
        }

        $this->currentTestData['message'] = $e->getMessage();
        $this->currentTestData['file'] = $traceEl['file'];
        $this->currentTestData['line'] = $traceEl['line'];
    }

    protected function getHeaders(): array {
        return [
            '#',
            'suite',
            'test',
            'start_time',
            'end_time',
            'time',
            'start_mem',
            'end_mem',
            'delta_mem',
            'error',
            'warning',
            'incomplete',
            'failure',
            'risky',
            'skipped',
            'message',
            'file',
            'line'
        ];
    }
}
