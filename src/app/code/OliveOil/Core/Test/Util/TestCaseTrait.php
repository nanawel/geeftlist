<?php

namespace OliveOil\Core\Test\Util;

use Masterminds\HTML5;
use OliveOil\Core\Service\Di\Container;
use OliveOil\Core\Service\View;
use OliveOil\Core\Test\Util\Exception\Reroute;
use QueryPath\DOMQuery;

trait TestCaseTrait
{
    /** @var int|null */
    protected $outputBufferLevelOnSetup;

    /** @var string[]|null  */
    protected $serverVarsOnSetup;

    public function disableListeners($eventSpace = null) {
        $this->getEventService()->disableListeners($eventSpace);

        return $this;
    }

    public function enableListeners($eventSpace = null) {
        $this->getEventService()->enableListeners($eventSpace);

        return $this;
    }

    public function areListenersEnabled($eventSpace = null): bool {
        return ! $this->getEventService()->areListenersDisabled($eventSpace);
    }

    /**
     * @param string $eventSpace
     * @param bool $enableListeners
     * @return mixed
     */
    public function executeWithListeners(callable $callable, $eventSpace, $enableListeners = true) {
        $listenersEnabled = $this->areListenersEnabled($eventSpace);
        try {
            if ($enableListeners) {
                $this->enableListeners($eventSpace);
            }
            else {
                $this->disableListeners($eventSpace);
            }

            return $callable();
        }
        finally {
            if ($listenersEnabled) {
                $this->enableListeners($eventSpace);
            }
            else {
                $this->disableListeners($eventSpace);
            }
        }
    }

    /**
     * @param string $eventSpace
     * @return mixed
     */
    public function executeWithoutListeners(callable $callable, $eventSpace) {
        return $this->executeWithListeners($callable, $eventSpace, false);
    }

    /**
     * @param string[] $enabledEventSpaces
     * @return mixed
     */
    public function executeOnlyWithListeners(array $enabledEventSpaces, callable $callable) {
        $currentListenersStatus = $this->getEventService()->getAllListenersStatus();
        try {
            $newListenersStatus = $currentListenersStatus;
            array_walk($newListenersStatus, static function (&$el, $k) use ($enabledEventSpaces) : void {
                $el = in_array($k, $enabledEventSpaces) ?: false;
            });
            $this->getEventService()->setListenersStatus($newListenersStatus);

            return $callable();
        }
        finally {
            $this->getEventService()->setListenersStatus($currentListenersStatus);
        }
    }

    public function getSession($session = 'core') {
        return $this->getSessionManager()->getSession($session);
    }

    public function getUrl($path, $params = []) {
        return $this->getUrlBuilder()->getUrl($path, $params);
    }

    public function getOutput(): string {
        $output = '';
        if ($this->outputBufferLevelOnSetup !== null) {
            while (ob_get_level() > $this->outputBufferLevelOnSetup) {
                $output .= ob_get_clean();
            }

            $output .= ob_get_contents();
        }

        return $output;
    }

    public function clearOutput() {
        $this->getOutput();
        ob_clean();
        $this->outputBufferLevelOnSetup = ob_get_level();

        return $this;
    }

    public function handleControllerException() {
        $this->clearOutput();

        return $this;
    }

    public function handleReroute(Reroute $reroute) {
        return $this->handleControllerException();
    }

    public function getCsrfTokenName() {
        return $this->getAppConfig()->getValue('CSRF_TOKEN');
    }

    public function assertValidHtmlWithoutErrors($string, $message = ''): void {
        $this->assertValidHtml($string, $message);
        $this->assertStringNotContainsString(View::RENDERING_ERROR_MARKER, $string);
    }

    public function assertValidHtml($string, $message = ''): void {
        $this->assertIsString($string, 'Not valid HTML (not a string)');
        $validator = new HTML5();
        $validator->loadHTML($string);

        $message = array_merge($validator->getErrors(), [$message]);
        $this->assertFalse($validator->hasErrors(), implode("\n", $message));
    }

    /**
     * @param string $html
     * @return mixed|DOMQuery
     */
    public function getDomQuery($html) {
        return \QueryPath::withHtml($html, null, ['convert_to_encoding' => 'UTF-8']);
    }

    /**
     * @param string $xpath
     * @param string $html
     * @param string $message
     */
    public function assertHtmlElementExists($xpath, $html, $message = ''): void {
        $this->assertGreaterThanOrEqual(
            1,
            $this->getDomQuery($html)->xpath($xpath)->length,
            $message
        );
    }

    /**
     * @param string $expected
     * @param array $xpath [XPath, attributeName]
     * @param string $html
     * @param string $message
     */
    public function assertHtmlAttributeEquals($expected, array $xpath, $html, $message = ''): void {
        $qp = $this->getDomQuery($html);
        [$xpath, $attributeName] = $xpath;

        $this->assertEquals(
            $expected,
            $qp->xpath($xpath)->attr($attributeName),
            $message
        );
    }

    /**
     * @param string $expected
     * @param string $xpath
     * @param string $html
     * @param string $message
     */
    public function assertHtmlInnerHtmlEquals($expected, string $xpath, $html, $message = ''): void {
        $qp = $this->getDomQuery($html);

        $this->assertEquals(
            $expected,
            $qp->xpath($xpath)->innerHTML(),
            $message
        );
    }

    /**
     * @param string|array $xpath Full XPath or an array of the form [XPath, attributeName]
     * @param string $html
     * @param string $message
     */
    public function assertHtmlAttributesEqual(array $expected, $xpath, $html, $message = ''): void {
        $qp = $this->getDomQuery($html);

        // Compatibility with self::assertHtmlAttributeEquals()
        if (is_array($xpath)) {
            $xpath = sprintf('%s/%s', $xpath[0], $xpath[1]);
        }

        $actualAttributeValues = array_map(
            static fn(\DOMAttr $attr): string => $attr->value,
            $qp->xpath($xpath)->toArray()
        );

        $this->assertEqualsCanonicalizing(
            $expected,
            $actualAttributeValues,
            $message
        );
    }

    public function emulateProductionMode(callable $callable): void {
        $mode = $this->getAppConfig()->getValue('MODE');
        try {
            $this->getAppConfig()->setValue('MODE', 'production');
            $callable();
        }
        finally {
            $this->getAppConfig()->setValue('MODE', $mode);
        }
    }

    /**
     * @param string $rawResponse
     * @throws \Throwable
     */
    protected function captureOutput(&$rawResponse, callable $callable) {
        $obLevel = ob_get_level();
        ob_start();
        try {
            return $callable();
        }
        finally {
            $rawResponse = '';
            while (ob_get_level() > $obLevel) {
                $rawResponse .= ob_get_clean();
            }
        }
    }

    /**
     * @return \DI\Container
     */
    public function getContainer() {
        return Container::instance()->getContainer();
    }

    /**
     * @return \Base
     */
    public function getFw() {
        return $this->getContainer()->get(\Base::class);
    }

    /**
     * @return \OliveOil\Core\Service\RegistryInterface
     */
    public function getRegistry() {
        return $this->getContainer()->get(\OliveOil\Core\Service\RegistryInterface::class);
    }

    /**
     * @return \OliveOil\Core\Service\EventInterface
     */
    public function getEventService() {
        return $this->getContainer()->get(\OliveOil\Core\Service\EventInterface::class);
    }

    /**
     * @return MailcatcherEmailSender
     */
    public function getEmailSender() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Email\Sender\SenderInterface::class);
    }

    /**
     * @return \OliveOil\Core\Model\App\ConfigInterface
     */
    public function getAppConfig() {
        return $this->getContainer()->get(\OliveOil\Core\Model\App\ConfigInterface::class);
    }

    /**
     * @return \OliveOil\Core\Service\Url\BuilderInterface
     */
    public function getUrlBuilder() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Url\BuilderInterface::class);
    }

    /**
     * @return \OliveOil\Core\Service\Session\Manager
     */
    public function getSessionManager() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Session\Manager::class);
    }

    /**
     * @return \OliveOil\Core\Service\Crypt
     */
    public function getCryptService() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Crypt::class);
    }

    /**
     * @return \OliveOil\Core\Model\I18n
     */
    public function getI18n() {
        return $this->getContainer()->get(\OliveOil\Core\Model\I18n::class);
    }

    /**
     * @return \OliveOil\Core\Service\View
     */
    public function getView() {
        return $this->getContainer()->get(\OliveOil\Core\Service\View::class);
    }

    /**
     * @param int $width
     * @param int $height
     * @param string $format
     * @return string
     */
    public function getImagePlaceholderUrl($width, $height, $format = 'jpg') {
        return $this->getContainer()
            ->get('OliveOil\Core\Test\Util\ImagePlaceholderUrlGenerator')($width, $height, $format);
    }

    /**
     * @return string
     */
    public function getInvalidImagePlaceholderUrl() {
        return $this->getContainer()
            ->get('OliveOil\Core\Test\Util\InvalidImagePlaceholderUrlGenerator')();
    }
}
