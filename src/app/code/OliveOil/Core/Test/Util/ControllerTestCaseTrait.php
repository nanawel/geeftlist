<?php

namespace OliveOil\Core\Test\Util;

use OliveOil\Core\Exception\SystemException;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;

trait ControllerTestCaseTrait
{
    use \Geeftlist\Test\Util\TestCaseTrait;

    /**
     * @return \OliveOil\Core\Test\Service\Http\RequestManager
     * @throws \DI\NotFoundException
     */
    public function getRequestManager() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Http\RequestManagerInterface::class);
    }

    /**
     * @return \OliveOil\Core\Model\Http\RequestInterface
     * @throws \DI\NotFoundException
     */
    public function getRequest() {
        return $this->getRequestManager()->getRequest();
    }

    /**
     * @return \OliveOil\Core\Model\Http\ResponseInterface
     * @throws \DI\NotFoundException
     */
    public function getResponse() {
        return $this->getRequestManager()->getResponse();
    }

    public function setUpControllerTest(): void {
        $_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';

        $fw = $this->getFw();

        $fw->set('QUIET', true);
        $fw->set('ONREROUTE', static function ($url, $permanent) : void {
            throw new Reroute($url, $permanent);
        });
        $fw->set('ONFORWARD_AFTER', static function ($route) : void {
            throw new Forward($route);
        });

        $this->getRequestManager()->reset()
            ->clearGlobals();

        $this->outputBufferLevelOnSetup = ob_get_level();
        $this->serverVarsOnSetup = $fw->get('SERVER');
    }

    public function tearDownControllerTest(): void {
        if ($this->serverVarsOnSetup === null) {
            throw new SystemException(sprintf(
                'No servers vars found. Did you call %s twice?',
                __FUNCTION__
            ));
        }

        $fw = $this->getFw();

        $this->getRegistry()->clear('current_controller');

        $this->getRequestManager()->reset()
            ->clearGlobals();

        $this->clearOutput();
        $this->outputBufferLevelOnSetup = null;

        $fw->set('SERVER', $this->serverVarsOnSetup);
        $this->serverVarsOnSetup = null;

        $this->getSession()->unsPreviousError();
    }

    /**
     * Wrapper for F3's mock()
     *
     * @param string $pattern
     * @param array|null $args
     * @param array|null $headers
     * @param string|null $body
     */
    public function mock($pattern, array $args = null, array $headers = null, $body = null): void {
        try {
            $this->getFw()->mock(...func_get_args());
        } finally {
            $this->clearCacheArrayObjects();
        }
    }
}
