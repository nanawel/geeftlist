<?php

namespace OliveOil\Core\Test\Util;

trait FakerTrait
{
    /** @var \Faker\Generator|null */
    private $faker;

    /**
     * @return \Faker\Generator
     */
    public function faker() {
        if (! $this->faker) {
            $this->faker = \Faker\Factory::create();
            $this->faker->seed();
        }

        return $this->faker;
    }
}
