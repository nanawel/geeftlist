<?php

namespace OliveOil\Core\Test\Model;


class FakeHttpSession extends FakeSession
    implements \OliveOil\Core\Model\Session\HttpInterface
{
    /**
     * @inheritDoc
     */
    public function setType($type) {
        return parent::setType($type);
    }

    /**
     * @inheritDoc
     */
    public function getType() {
        return parent::getType();
    }
}