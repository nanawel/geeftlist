<?php


namespace OliveOil\Core\Test\Model\Rest;


use OliveOil\Core\Model\Rest\Response;

/**
 * Class TestResponse
 *
 * Special response class that add support for validation/assertion.
 */
class TestResponse extends Response
{
    /** @var array|null */
    protected $decodedBody;

    /**
     * @return array|null
     */
    public function getData() {
        return $this->getBodyAsArray()['data'] ?? null;
    }

    /**
     * @return array|null
     */
    public function getErrors() {
        return $this->getBodyAsArray()['errors'] ?? null;
    }

    /**
     * @return array|null
     */
    public function getMeta() {
        return $this->getBodyAsArray()['meta'] ?? null;
    }

    /**
     * @return array|null
     */
    public function getLinks() {
        return $this->getBodyAsArray()['links'] ?? null;
    }

    /**
     * @return array|null
     */
    public function getIncluded() {
        return $this->getBodyAsArray()['included'] ?? null;
    }

    /**
     * @return array
     * @throws \JsonException
     */
    protected function getBodyAsArray() {
        if (!$this->decodedBody) {
            $this->decodedBody = $this->getBody() === '' ? [] : json_decode($this->getBody(), true, 512, JSON_THROW_ON_ERROR);
        }

        return $this->decodedBody;
    }
}
