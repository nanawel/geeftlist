<?php

namespace OliveOil\Core\Test\Model;


use OliveOil\Core\Model\Session;

class FakeSession extends Session
{
    /** @var string */
    protected $id;

    /** @var array */
    protected static $staticData = [];

    /** @var array Special data that persists even after invalidate() */
    protected $persistentData = [];

    public function init($force = false): static {
        $this->invalidate();

        return $this;
    }

    /**
     * @return $this
     */
    public function invalidate(): static {
        $this->resetData()
            ->resetOrigData()
            ->injectPersistentData();
        $this->getMessageManager()->clearMessages();

        return $this;
    }

    public function regenerate(): static {
        $this->id = uniqid('FAKESESSIONID_');
        unset(self::$staticData['csrf_token']);

        return $this;
    }

    public function abortClose($clear = false): static {
        $this->id = '';
        unset(self::$staticData['csrf_token']);
        if ($clear) {
            $this->resetData()
                ->resetOrigData()
                ->injectPersistentData();
        }

        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function getCsrfToken() {
        if (! isset(self::$staticData['csrf_token'])) {
            $fw = $this->getFw();
            self::$staticData['csrf_token'] = $fw->get('SEED') . '.' . $fw->hash(mt_rand());
        }

        return self::$staticData['csrf_token'];
    }

    public function setPersistentData($key, $value): static {
        $this->persistentData[$key]
            = $this->_data[$key]
            = $value;

        return $this;
    }

    public function getPersistentData($key) {
        return $this->persistentData[$key] ?? null;
    }

    public function unsetPersistentData($key): static {
        unset($this->persistentData[$key]);

        return $this;
    }

    public function clearPersistentData(): static {
        $this->persistentData = [];

        return $this;
    }

    protected function injectPersistentData(): static {
        $this->_data = array_merge($this->_data, $this->persistentData);

        return $this;
    }
}
