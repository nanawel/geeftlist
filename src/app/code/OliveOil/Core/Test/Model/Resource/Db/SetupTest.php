<?php

namespace OliveOil\Core\Test\Model\ResourceModel\Db;


use OliveOil\Core\Model\ResourceModel\Db\Setup;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class SetupTest
 *
 * @group oliveoil
 * @group models
 */
class SetupTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    public function testIsScriptApplicable(): void {

        $cut = $this->getContainer()->get(Setup::class);

        // Simple install
        $this->assertTrue($cut->isScriptApplicable(
            '0.0.0', '0.1.0',
            '0.0.0', '0.1.0',
            Setup::DIRECTION_UPGRADE
        ));
        // Simple upgrade
        $this->assertTrue($cut->isScriptApplicable(
            '0.1.0', '0.1.1',
            '0.1.0', '0.1.1',
            Setup::DIRECTION_UPGRADE
        ));
        // Partially satisfied upgrade (wanted 0.2.0 but given 0.1.1)
        $this->assertTrue($cut->isScriptApplicable(
            '0.1.0', '0.1.1',
            '0.1.0', '0.2.0',
            Setup::DIRECTION_UPGRADE
        ));

        // Unsatisfied upgrade (applicable to higher version)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.0', '0.1.5',
            '0.1.1', '0.1.5',
            Setup::DIRECTION_UPGRADE
        ));
        // Unsatisfied upgrade (limited by specified target)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.0', '0.1.5',
            '0.1.0', '0.1.2',
            Setup::DIRECTION_UPGRADE
        ));
        // Unsatisfied upgrade (limited by current version)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.0', '0.1.5',
            '0.0.8', '0.1.5',
            Setup::DIRECTION_UPGRADE
        ));
        // Unsatisfied upgrade (wrong direction)
        $this->assertFalse($cut->isScriptApplicable(
            '0.2.0', '0.1.1',
            '0.2.0', '0.1.1',
            Setup::DIRECTION_UPGRADE
        ));
        // Unsatisfied upgrade (downgrade script given)
        $this->assertFalse($cut->isScriptApplicable(
            '0.2.0', '0.1.1',
            '0.2.0', '0.2.1',
            Setup::DIRECTION_UPGRADE
        ));

        // Simple downgrade
        $this->assertTrue($cut->isScriptApplicable(
            '0.2.0', '0.1.1',
            '0.2.0', '0.1.1',
            Setup::DIRECTION_DOWNGRADE
        ));
        // Partially satisfied downgrade (wanted 0.1.0 but given 0.1.1)
        $this->assertTrue($cut->isScriptApplicable(
            '0.2.0', '0.1.1',
            '0.2.0', '0.1.0',
            Setup::DIRECTION_DOWNGRADE
        ));

        // Unsatisfied downgrade (applicable to lower version)
        $this->assertFalse($cut->isScriptApplicable(
            '0.2.0', '0.1.5',
            '0.2.1', '0.1.5',
            Setup::DIRECTION_DOWNGRADE
        ));
        // Unsatisfied downgrade (limited by specified target)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.5', '0.1.0',
            '0.1.5', '0.1.2',
            Setup::DIRECTION_DOWNGRADE
        ));
        // Unsatisfied downgrade (limited by current version)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.5', '0.1.0',
            '0.1.6', '0.1.0',
            Setup::DIRECTION_DOWNGRADE
        ));
        // Unsatisfied downgrade (wrong direction)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.0', '0.1.5',
            '0.1.0', '0.1.5',
            Setup::DIRECTION_DOWNGRADE
        ));
        // Unsatisfied downgrade (upgrade script given)
        $this->assertFalse($cut->isScriptApplicable(
            '0.1.1', '0.2.0',
            '0.2.0', '0.1.1',
            Setup::DIRECTION_DOWNGRADE
        ));
    }
}
