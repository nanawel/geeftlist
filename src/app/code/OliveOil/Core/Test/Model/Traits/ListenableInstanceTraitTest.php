<?php

namespace OliveOil\Core\Test\Model\Traits;


use OliveOil\Core\Model\Traits\ListenableInstanceTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

class ListenableInstanceTraitTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $cut;

    protected function setUp(): void {
        parent::setUp();

        $this->cut = null;
    }

    public function testAttachInstanceListener(): void {
        $cut = $this->getCut();

        $callback = static fn(): int => 1;
        $cut->attachInstanceListener('testEvent1', $callback, 0);

        $this->assertCount(1, $cut->getInstanceListeners());
        $this->assertCount(1, $cut->getInstanceListeners('testEvent1')[0]);
        $this->assertEquals($callback, $cut->getInstanceListeners('testEvent1')[0][0]);
        $this->assertEquals(1, $cut->getInstanceListeners('testEvent1')[0][0]());

        $callback2 = static fn(): int => 2;
        $cut->attachInstanceListener('testEvent1', $callback2, 0);

        $this->assertCount(1, $cut->getInstanceListeners());
        $this->assertCount(2, $cut->getInstanceListeners('testEvent1')[0]);
        $this->assertEquals($callback2, $cut->getInstanceListeners('testEvent1')[0][1]);
        $this->assertEquals(2, $cut->getInstanceListeners('testEvent1')[0][1]());

        $callback3 = static fn(): int => 3;
        $cut->attachInstanceListener('testEvent2', $callback3, 0);

        $this->assertCount(2, $cut->getInstanceListeners());
        $this->assertEquals($callback3, $cut->getInstanceListeners('testEvent2')[0][0]);
        $this->assertEquals(3, $cut->getInstanceListeners('testEvent2')[0][0]());
    }

    public function testTriggerInstanceEvent(): void {
        $cut = $this->getCut();

        $callbackReturn = [];

        $callbackNotTriggered = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 0;
        };
        $cut->attachInstanceListener('testEventNotTriggered', $callbackNotTriggered, 0);

        $callback = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 1;
        };
        $cut->attachInstanceListener('testEvent', $callback, 0);

        $this->assertEmpty($callbackReturn);
        $cut->triggerInstanceEvent('testEvent');
        $this->assertCount(1, $callbackReturn);
        $this->assertEquals(1, $callbackReturn[0]); // @phpstan-ignore-line

        $callback2 = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 2;
        };
        $cut->attachInstanceListener('testEvent', $callback2, 0);

        $cut->triggerInstanceEvent('testEvent');
        $this->assertCount(3, $callbackReturn);
        $this->assertEquals(1, $callbackReturn[0]);
        $this->assertEquals(1, $callbackReturn[1]); // @phpstan-ignore-line
        $this->assertEquals(2, $callbackReturn[2]); // @phpstan-ignore-line
    }

    public function testTriggerInstanceEventPriority(): void {
        $cut = $this->getCut();

        $callbackReturn = [];
        $callback = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 1;
        };
        $callback2 = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 2;
        };

        $cut->attachInstanceListener('testEvent', $callback, 20);
        $cut->attachInstanceListener('testEvent', $callback2, 10);

        $cut->triggerInstanceEvent('testEvent');
        $this->assertCount(2, $callbackReturn);
        $this->assertEquals(2, $callbackReturn[0]);
        $this->assertEquals(1, $callbackReturn[1]); // @phpstan-ignore-line
    }

    public function testClearInstanceListeners(): void {
        $cut = $this->getCut();

        $callbackReturn = [];

        $callbackNotTriggered = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 0;
        };
        $cut->attachInstanceListener('testEvent1', $callbackNotTriggered, 0);

        $callback = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 1;
        };
        $cut->attachInstanceListener('testEvent2', $callback, 0);

        $callback2 = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 2;
        };
        $cut->attachInstanceListener('testEvent2', $callback2, 0);

        $this->assertCount(2, $cut->getInstanceListeners());

        $cut->clearInstanceListeners('testEvent2');

        $this->assertCount(1, $cut->getInstanceListeners());
        $this->assertArrayHasKey('testEvent1', $cut->getInstanceListeners());
    }

    public function testClearAllInstanceListeners(): void {
        $cut = $this->getCut();

        $callbackReturn = [];

        $callbackNotTriggered = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 0;
        };
        $cut->attachInstanceListener('testEvent1', $callbackNotTriggered, 0);

        $callback = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 1;
        };
        $cut->attachInstanceListener('testEvent2', $callback, 0);

        $callback2 = static function () use (&$callbackReturn) : void {
            $callbackReturn[] = 2;
        };
        $cut->attachInstanceListener('testEvent2', $callback2, 0);

        $cut->clearInstanceListeners('*');

        $this->assertEmpty($cut->getInstanceListeners());
    }

    /**
     * @return object|ListenableInstanceTrait
     */
    protected function getCut() {
        if (! $this->cut) {
            $this->cut = new class() {
                use ListenableInstanceTrait;
            };
        }

        return $this->cut;
    }
}
