<?php

namespace OliveOil\Core\Test\Model\App;

use OliveOil\Core\Model\App\Config;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 */
class ConfigTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return Config
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(Config::class);
    }

    /**
     * @group ticket-418
     */
    public function testGetValue(): void {
        $cut = $this->getCut();

        $uniqueKey = uniqid();
        $configPath = sprintf('%s.foo', $uniqueKey);

        $this->assertNull($cut->getValue($configPath));

        $cut->setValue($configPath, 'bar');
        $this->assertEquals('bar', $cut->getValue($configPath));
    }

    /**
     * @group ticket-418
     */
    public function testGetArrayValue(): void {
        $cut = $this->getCut();

        $uniqueKey = uniqid();
        $configPath = sprintf('%s.foo', $uniqueKey);

        $this->assertNull($cut->getValue($configPath));

        $cut->setValue($configPath . '.a', 'bar');
        $cut->setValue($configPath . '.b', 'baz');
        $this->assertEquals(['a' => 'bar', 'b' => 'baz'], $cut->getValue($configPath));
    }
}
