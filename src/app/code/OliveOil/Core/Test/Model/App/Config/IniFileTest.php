<?php

namespace OliveOil\Core\Test\Model\App\Config;


use PHPUnit\Framework\TestCase;

/**
 * Class IniFileTest
 *
 * @group oliveoil
 * @group models
 */
class IniFileTest extends TestCase
{
    /**
     * Check that a INI config file does not contain twice the definition of a same variable
     */
    public function testFilesDoNotContainSelfValueOverride(): void {
        $etcDirPath = \Base::instance()->get('ROOT') . '/etc';

        $etcDirIterator = new \RecursiveDirectoryIterator($etcDirPath);
        $etcDirIteratorIterator = new \RecursiveIteratorIterator($etcDirIterator);
        $regexIterator = new \RegexIterator(
            $etcDirIteratorIterator,
            '/^.+\.ini$/',
            \RecursiveRegexIterator::MATCH
        );

        /** @var \SplFileInfo $file */
        foreach ($regexIterator as $file) {
            $configVariables = array_map('trim', preg_filter(
                '/^\s*([^;].+?)=.*/',
                '\1',
                explode("\n", file_get_contents($file->getRealPath()))
            ));
            asort($configVariables);
            $uniqueConfigVariables = array_unique($configVariables);

            $this->assertEquals(
                $uniqueConfigVariables,
                $configVariables,
                sprintf('Config file %s contains duplicate values.', $file->getRealPath())
            );
        }
    }
}
