<?php

namespace OliveOil\Core\Test\Model;

use OliveOil\Core\Model\I18n;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class I18nTest
 */
class I18nTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();

        // Set fixed i18n config
        $this->getFw()->set('LANGUAGE', 'fr-FR');
        $this->getFw()->set('FALLBACK', 'en');
        $this->getAppConfig()->setValue('LANGUAGES', ['fr-FR', 'fr', 'en-US' ,'en']);
    }

    /**
     * @return I18n
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(I18n::class);
    }

    public function testSetPrimaryLanguage(): void {
        $cut = $this->getCut();

        $this->assertEquals('fr-FR,fr,en', $cut->getLanguageWithFallback());

        $cut->setLanguage('es-ES');

        $this->assertEquals('es-ES,es,en', $cut->getLanguageWithFallback());

        $cut->setLanguage('en-UK');

        $this->assertEquals('en-UK,en', $cut->getLanguageWithFallback());
    }

    public function testSetGlobalLanguage(): void {
        $fw = $this->getFw();
        $cut = $this->getCut();

        $this->assertEquals('fr-FR,fr,en', $fw->get('LANGUAGE'));

        $cut->setGlobalLanguage('es-ES');

        $this->assertEquals('es-ES,es,en', $fw->get('LANGUAGE'));

        $cut->setGlobalLanguage('en-UK');

        $this->assertEquals('en-UK,en', $fw->get('LANGUAGE'));

        $cut->setGlobalLanguage('en-UK', 'fr');

        $this->assertEquals('en-UK,en,fr', $fw->get('LANGUAGE'));
    }

    public function testCurrency(): void {
        $cut = $this->getCut();

        // Note: Starting from PHP 7.3.9, hundreds separator seems to be the *narrow* non-breaking space (xE280AF)
        // in fr_FR in replacement of the standard non-breaking space (xC2A0) still used before the currency symbol
        $this->assertEquals("123\xe2\x80\xaf456,78\xc2\xa0€", $cut->currency(123456.78, false, 'EUR'));

        $cut->setLanguage('en-US');

        $this->assertEquals('€123,456.45', $cut->currency(123456.45, false, 'EUR'));
    }

    public function testNumber(): void {
        $cut = $this->getCut();

        // Note: same as above
        $this->assertEquals("123\xe2\x80\xaf456,78", $cut->number(123456.78));

        $cut->setLanguage('en-US');

        $this->assertEquals('123,456.45', $cut->number(123456.45));
    }

    public function testDate(): void {
        $cut = $this->getCut();

        $this->assertEquals('09/07/1985', $cut->date('1985-07-09'));

        $cut->setLanguage('en-US');

        $this->assertEquals('7/9/85', $cut->date('1985-07-09'));
    }

    public function testDatetime(): void {
        $cut = $this->getCut();

        $this->assertEquals('09/07/1985 13:45', $cut->datetime('1985-07-09T13:45'));

        $cut->setLanguage('en-US');

        /**
         * @see https://en.wikipedia.org/wiki/Whitespace_character
         * @see https://www.php.net/manual/en/migration70.new-features.php#migration70.new-features.unicode-codepoint-escape-syntax
         */
        $narrowNonBreakSpace = "\u{202F}";
        $this->assertEquals(sprintf('7/9/85, 1:45%sPM', $narrowNonBreakSpace), $cut->datetime('1985-07-09T13:45'));
    }
}
