<?php

namespace OliveOil\Core\Test;


use OliveOil\Core\Exception\DatabaseException;
use OliveOil\Core\Model\SetupInterface;
use OliveOil\Core\Test\Exception\BootstrapException;

class Bootstrap implements BootstrapInterface
{
    /** @var string[] */
    protected $preTasks = [
        'clearCache',
        'resetLogService',
        'emptyTempFolder',
        'assertDbExists',
        'recreateDatabase',
        'upgradeSchema',
        'injectSampleData',
        'upgradeSampleData'
    ];

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\AppInterface $app,
        protected \OliveOil\Core\Model\App\Config $appConfig,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Design $design,
        protected \OliveOil\Core\Model\SetupInterface $setup,
        protected \OliveOil\Core\Service\RouteInterface $routeService,
        protected \OliveOil\Core\Service\Cache $cache,
        protected \OliveOil\Core\Service\Log $logService,
        array $preTasks = null
    ) {
        if (is_array($preTasks)) {
            $this->preTasks = $preTasks;
        }
    }

    public function bootstrap(): void {
        try {
            $this->clearOpcache();

            $this->installErrorHandler();
            $this->installProfiler();

            $this->routeService
                ->migrateRoutes()
                ->registerRestMapsToRoutes();

            $this->printDbInfo();
            $this->waitForDb();

            $this->executePreTasks();

            $this->app->setup();
        } catch (\Throwable $throwable) {
            // Make sure full trace is printed in the console
            fwrite(STDERR, \OliveOil\exception_full_trace($throwable) . "\n");

            throw $throwable;
        }
    }

    protected function installErrorHandler() {
        // Install error handler to display error message and return a status > 0
        $this->fw->set('ONERROR', function(\Base $fw): void {
            $this->log(sprintf('%s %s%s', $fw->get('ERROR.code'), $fw->get('ERROR.text'), PHP_EOL), STDERR);
            $this->log($fw->get('ERROR.trace') . PHP_EOL, STDERR);
            die(42);
        });
    }

    protected function installProfiler() {
        if ($this->readParam('XHPROF_ENABLE')) {
            if (function_exists('xhprof_enable')) {
                $this->log("Enabling Xhprof...", null, LOG_INFO);
                xhprof_enable(XHPROF_FLAGS_NO_BUILTINS | XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY);
                register_shutdown_function([$this, 'onProfilerShutdown']);
                $this->log("Xhprof enabled. Shutdown function registered for post-processing.", null, LOG_INFO);
            } else {
                $this->log("Profiling requested but Xhprof cannot be found! Ignoring.", null, LOG_WARNING);
            }
        }
    }

    protected function onProfilerShutdown() {
        try {
            $xhprofData = xhprof_disable();

            // Relies on a properly set include_path
            include_once __DIR__ . '/xhprof_lib/utils/xhprof_lib.php';
            include_once __DIR__ . '/xhprof_lib/utils/xhprof_runs.php';

            /** @phpstan-ignore-next-line */
            $xhprofRuns = new \XHProfRuns_Default();
            /** @phpstan-ignore-next-line */
            $runId = $xhprofRuns->save_run($xhprofData, 'phpunit');

            $this->log('Profiling created with ID ' . $runId, null, LOG_INFO);
        } catch (\Throwable  $throwable) {
            $this->log('ERROR: Could not create profiling ouput.', null, LOG_WARNING);
            $this->log((string) $throwable);
        }
    }

    protected function readParam(string $param) {
        if ($this->fw->get('SERVER.' . $param) !== null) {
            return $this->fw->get('SERVER.' . $param);
        }

        return $this->fw->get('unit_test.' . $param);
    }

    protected function log(mixed $msg, $stream = STDOUT, $level = null): static {
        if (!$stream) {
            $stream = $level > LOG_WARNING ? STDERR : STDOUT;
        }

        if (!$level) {
            $level = $stream == STDERR ? LOG_ERR : LOG_DEBUG;
        }

        $levelPrefix = match ($level) {
            LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR => 'ERROR | ',
            LOG_WARNING => 'WARN  | ',
            LOG_INFO => 'INFO  | ',
            default => 'DEBUG | ',
        };

        // Log only with DEBUG env variable (printing on stdout crashes controller tests)
        //if ($this->fw->get('unit_test.DEBUG') || $this->fw->get('SERVER.DEBUG')) {
            fwrite($stream, $levelPrefix . (is_scalar($msg)
                    ? $msg
                    : json_encode($msg, JSON_PRETTY_PRINT, 3)
                ) . "\n");
        //}

        return $this;
    }

    protected function executePreTasks() {
        foreach ($this->preTasks as $preTask) {
            if (is_callable($preTask)) {
                $preTask();
            }
            elseif (is_callable([$this, $preTask])) {
                $this->$preTask();
            }
            else {
                throw new \InvalidArgumentException('Invalid pretask callable.');
            }
        }

        $this->log("Bootstrap complete.\n", null, LOG_INFO);
    }

    protected function clearCache() {
        $this->log("Clearing cache...", null, LOG_INFO);
        $this->cache->clear();
        $this->log("Cache cleared.", null, LOG_INFO);
    }

    protected function resetLogService() {
        $this->log("Resetting log service...", null, LOG_INFO);
        $this->logService->reset();
        $this->log("Log service reset.", null, LOG_INFO);
    }

    protected function clearOpcache() {
        $this->log("Clearing opcache...", null, LOG_INFO);
        opcache_reset();
        $this->log("Opcache cleared.", null, LOG_INFO);
    }

    protected function emptyTempFolder() {
        $this->log("Clearing temporary folder...", null, LOG_INFO);
        $tmpDir = $this->fw->get('TEMP');
        if (is_dir($tmpDir)) {
            $di = new \DirectoryIterator($tmpDir);
            foreach ($di as $file) {
                if ($file->isFile()) {
                    unlink($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename());
                }
            }

            $this->log('Temporary folder cleared.', null, LOG_INFO);
        }
        else {
            $this->log(sprintf('Folder %s does not exist. Skipping.', $tmpDir), null, LOG_WARNING);
        }
    }

    protected function printDbInfo() {
        $setup = $this->setup;
        $this->log($setup->getDbInfo());
    }

    protected function assertDbExists() {
        if (! $this->setup->isInstalled()
            && ! $this->readParam('RECREATE_DATABASE')
        ) {
            throw new BootstrapException('Application is not installed but RECREATE_DATABASE=0.');
        }
    }

    protected function recreateDatabase() {
        try {
            if ($this->readParam('RECREATE_DATABASE')) {
                $this->log("(Re-)creating database...", null, LOG_INFO);
                $this->setup->install(true);
                $this->log("Database created.", null, LOG_INFO);
            }
        }
        catch (\Exception $exception) {
            $this->handleUnknownDatabase($exception);
            throw $exception;
        }
    }

    protected function upgradeSchema() {
        try {
            if ($this->readParam('UPGRADE_SCHEMA')) {
                $this->log("Upgrading schema...", null, LOG_INFO);
                $this->setup->upgrade(true);
                $this->log("Schema upgraded.", null, LOG_INFO);
            }
            else {
                $this->assertDbUpToDate(SetupInterface::TYPE_SCHEMA);
            }
        }
        catch (\Exception $exception) {
            $this->handleUnknownDatabase($exception);
            throw $exception;
        }
    }

    protected function injectSampleData() {
        try {
            if ($this->readParam('INJECT_SAMPLE_DATA')) {
                $this->log("Injecting sample data...", null, LOG_INFO);
                $this->setup->uninstallSampleData();
                $this->setup->installSampleData();
                $this->log("Sample data injected.", null, LOG_INFO);
            }
        }
        catch (\Exception $exception) {
            $this->handleUnknownDatabase($exception);
            throw $exception;
        }
    }

    protected function upgradeSampleData() {
        try {
            if ($this->readParam('UPGRADE_SAMPLE_DATA')) {
                $this->log("Upgrading sample data...", null, LOG_INFO);
                $this->setup->upgradeSampleData();
                $this->log("Sample data upgraded.", null, LOG_INFO);
            }
            else {
                $this->assertDbUpToDate(SetupInterface::TYPE_SAMPLEDATA);
            }
        }
        catch (\Exception $exception) {
            $this->handleUnknownDatabase($exception);
            throw $exception;
        }
    }

    /**
     * @throws BootstrapException
     */
    protected function assertDbUpToDate(string $type) {
        $codeVersion = $this->setup->getCodeVersion($type);
        $currentDbVersion = $this->setup->getDbVersion($type);

        if ($codeVersion != $currentDbVersion
            && !$this->readParam('IGNORE_VERSION_MISMATCH')
        ) {
            throw new BootstrapException(sprintf(
                'Versions do not match for type "%s"! Code: %s / Current: %s.',
                $type,
                $codeVersion,
                $currentDbVersion
            ));
        }
    }

    protected function handleUnknownDatabase(\Throwable $e) {
        if (str_contains($e->getMessage(), 'Unknown database')) {
            $this->log("\nDatabase doest not exist. Please create it first.\n", STDERR);
        }
    }

    /**
     * FIXME: Duplicated from \Geeftlist\Controller\Cli\System::_checkDb
     *
     * @throws DatabaseException
     */
    protected function waitForDb(): bool {
        $hardTimeout = time() + 600;
        $start = time();
        $timeout = (int) $this->appConfig->getValue('DB_CONNECT_TIMEOUT');
        if ($timeout !== 0) {
            if (($maxExecutionTime = (int) ini_get('max_execution_time')) !== 0) {
                ini_set('max_execution_time', $timeout + $maxExecutionTime);
            }

            $this->log("max_execution_time set to: " . ini_get('max_execution_time'), null, LOG_INFO);
            while (time() < $hardTimeout) {
                if ($this->setup->isDbUp()) {
                    return true;
                }

                if (time() > $start + $timeout) {
                    throw new DatabaseException(sprintf(
                        'Could not connect to server after %d seconds, giving up. (Check connectivity or try '
                        . 'increasing DB_CONNECT_TIMEOUT in local.ini)',
                        $timeout
                    ));
                }

                $this->log("Database is not available, waiting before retrying...", null, LOG_WARNING);
                sleep(2);
            }
        }

        return false;
    }
}
