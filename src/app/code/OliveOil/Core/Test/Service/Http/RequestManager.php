<?php


namespace OliveOil\Core\Test\Service\Http;


use OliveOil\Core\Model\Http\RequestInterface;
use OliveOil\Core\Model\Http\ResponseInterface;

class RequestManager extends \OliveOil\Core\Service\Http\RequestManager
{
    /**
     * @return $this
     */
    public function clearGlobals(): static {
        $this->fw->set('HEADERS', null); // Cannot use clear() because of the reference, see \Base::__construct()
        $this->fw->set('GET', []);
        $this->fw->set('POST', []);
        $this->fw->set('REQUEST', []);

        // Special F3 key for route params
        $this->fw->set('PARAMS', []);

        return $this;
    }

    /**
     * @return $this
     */
    public function setRequest(RequestInterface $request): static {
        $this->request = $request;

        return $this;
    }

    /**
     * @return $this
     */
    public function setResponse(ResponseInterface $response): static {
        $this->response = $response;

        return $this;
    }
}
