<?php

namespace OliveOil\Core\Test\Service\Rest\JsonApi;

use Narrowspark\HttpStatus\Exception\InternalServerErrorException;
use OliveOil\Core\Model\Rest\ResponseInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class ResponseWriterTest
 *
 * @group api
 * @group oliveoil
 * @group services
 */
class ResponseWriterTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \OliveOil\Core\Service\Rest\JsonApi\ResponseWriter
     */
    protected function getCut() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Rest\JsonApi\ResponseWriter::class);
    }

    /**
     * @return \OliveOil\Core\Model\Rest\Response
     */
    protected function newResponse() {
        return $this->getContainer()->make(ResponseInterface::class);
    }

    /**
     * @group ticket-331
     * @group ticket-396
     */
    public function testWrite_emptyResponse(): void {
        $cut = $this->getCut();

        $response = $this->newResponse();

        $rawResponse = '';
        try {
            $this->captureOutput($rawResponse, static function () use ($cut, $response) : void {
                $cut->write($response);
            });
            $this->fail('Failed throwing an exception when attempting to write a response with an empty body.');
        }
        catch (InternalServerErrorException) {}

        $this->assertEquals('', $rawResponse);
    }
}
