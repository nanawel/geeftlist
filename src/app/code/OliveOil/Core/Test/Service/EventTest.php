<?php

namespace OliveOil\Core\Test\Service;

use Laminas\EventManager\Event;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\MagicObject;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class EventTest
 *
 * @group oliveoil
 * @group services
 */
class EventTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /** @var mixed */
    protected $callbackResponse;

    /** @var mixed */
    protected static $staticCallbackResponse;

    /** @var \Laminas\EventManager\SharedEventManager|null */
    protected static $sharedEventManager;

    protected function setUp(): void {
        parent::setUp();
        $this->callbackResponse = uniqid();
        self::$staticCallbackResponse = uniqid();
        self::$sharedEventManager = null;

        // Treat this test class as a singleton (for event callbacks, to be able to use $this->callbackResponse)
        $this->getContainer()->set(self::class, $this);
    }

    protected function tearDown(): void {
        $this->callbackResponse = null;
        self::$staticCallbackResponse = null;
        self::$sharedEventManager = null;
        parent::tearDown();
    }

    /**
     * @param array $args
     * @return \OliveOil\Core\Service\Event
     */
    protected function newCut($args = []) {
        if (! self::$sharedEventManager) {
            self::$sharedEventManager = $this->getContainer()->make(\Laminas\EventManager\SharedEventManager::class);
        }

        $args += [
            'sharedEventManager' => self::$sharedEventManager,
            // Disable cache here
            'cache' => $this->getContainer()->get('OliveOil\Core\Service\Cache\Void'),
            'enableEventDebugListener' => false
        ];

        return $this->getContainer()->make(\OliveOil\Core\Service\Event::class, $args);
    }

    public function eventCallback(EventInterface $e): void {
        $e->getParam('payload')->setResponse($this->callbackResponse);
    }

    public static function staticEventCallback(EventInterface $e): void {
        $e->getParam('payload')->setResponse(self::$staticCallbackResponse);
    }

    protected function getObserverProviderMock($definitions = null) {
        if ($definitions === null) {
            $listeners1 = [
                'uniqueId1' => [
                    'identifier' => 'identifier1',
                    'priority' => '90',
                    'eventName' => 'my_event',
                    'callback' => self::class . '::staticEventCallback',
                ]
            ];
            $listeners2 = [
                'uniqueId2' => [
                    'identifier' => 'identifier2',
                    'priority' => '80',
                    'eventName' => 'my_other_event',
                    'callback' => self::class . '->eventCallback',
                ]
            ];
            $definitions = array_merge($listeners1, $listeners2);
        }

        $mock = $this->createMock(\OliveOil\Core\Service\Event\ProviderInterface::class);
        $mock->method('getDefinitions')
            ->willReturn($definitions);

        return $mock;
    }

    public function newObserver($id, &$testValue): object {
        return new class($id, $testValue) {
            private ?array $testValue = null;

            public function __construct(private $id, &$testValue) {
                $this->testValue =& $testValue;
            }

            public function callback(): void {
                $this->testValue[] = $this->id;
            }
        };
    }

    public function testSetupListeners(): void {
        $cut = $this->newCut([
            'observerProvider' => $this->getObserverProviderMock()
        ]);

        $allActualListeners1 = $cut->getSharedEventManager()->getListeners(['identifier1'], 'my_event');
        $allActualListeners2 = $cut->getSharedEventManager()->getListeners(['identifier2'], 'my_other_event');

        $this->assertEmpty($allActualListeners1);
        $this->assertEmpty($allActualListeners2);

        $cut->setupListeners();

        $allActualListeners1 = $cut->getSharedEventManager()->getListeners(['identifier1'], 'my_event');
        $allActualListeners2 = $cut->getSharedEventManager()->getListeners(['identifier2'], 'my_other_event');

        $this->assertCount(1, $allActualListeners1);
        $this->assertCount(1, $allActualListeners2);

        $callableObserver1 = current(current($allActualListeners1));
        $callableObserver2 = current(current($allActualListeners2));

        $this->assertIsCallable($callableObserver1);
        $this->assertIsCallable($callableObserver2);

        $transportObject = new MagicObject();
        $event = new Event();
        $event->setParam('payload', $transportObject);
        $callableObserver1($event);

        $this->assertEquals(self::$staticCallbackResponse, $transportObject->getResponse());

        $transportObject = new MagicObject();
        $event = new Event();
        $event->setParam('payload', $transportObject);
        $callableObserver2($event);

        $this->assertEquals($this->callbackResponse, $transportObject->getResponse());
    }

    public function testEnableDisableListeners(): void {
        $testValue = null;

        $cutDefault = $this->newCut([
            'observerProvider' => $this->getObserverProviderMock([
                'evDefaultIdentifier' => [
                    'identifier' => 'stdClass',
                    'priority' => '90',
                    'eventName' => 'my_event',
                    'callback' => [$this->newObserver('OBSERVER_DEFAULT', $testValue), 'callback'],
                ]
            ])
        ])->setupListeners();
        $cut1 = $this->newCut([
            'observerProvider' => $this->getObserverProviderMock([
                'evSpace1Identifier' => [
                    'identifier' => 'stdClass',
                    'priority' => '90',
                    'eventName' => 'my_event',
                    'callback' => [$this->newObserver('OBSERVER_SPACE1', $testValue), 'callback'],
                ]
            ]),
            'eventSpace' => 'EVENTSPACE1'
        ])->setupListeners();
        $cut2 = $this->newCut([
            'observerProvider' => $this->getObserverProviderMock([
                'evSpace2Identifier' => [
                    'identifier' => 'stdClass',
                    'priority' => '90',
                    'eventName' => 'my_event',
                    'callback' => [$this->newObserver('OBSERVER_SPACE2', $testValue), 'callback'],
                ]
            ]),
            'eventSpace' => 'EVENTSPACE2'
        ])->setupListeners();

        $target = new class() extends \stdClass {}; // "extends" only to have a valid identifier when triggering events

        $evManagerDefault = $cutDefault->getEventManager($target);

        $testValue = [];
        $this->assertEmpty($testValue);

        $evManagerDefault->trigger('my_event', $target);

        $this->assertEquals(
            ['OBSERVER_DEFAULT', 'OBSERVER_SPACE1', 'OBSERVER_SPACE2'],
            $testValue
        );

        // First test: disable own event space
        $cut2->disableListeners();

        $testValue = [];
        $this->assertEmpty($testValue);

        $evManagerDefault->trigger('my_event', $target);

        $this->assertEquals(
            ['OBSERVER_DEFAULT', 'OBSERVER_SPACE1'],
            $testValue
        );

        // Second test: disable other event space
        $cut2->disableListeners('EVENTSPACE1');

        $testValue = [];
        $this->assertEmpty($testValue);

        $evManagerDefault->trigger('my_event', $target);

        $this->assertEquals(
            ['OBSERVER_DEFAULT'],
            $testValue
        );

        // Third test: enable back listeners
        $cut1->enableListeners('EVENTSPACE1');
        $cut1->enableListeners('EVENTSPACE2');

        $testValue = [];
        $this->assertEmpty($testValue);

        $evManagerDefault->trigger('my_event', $target);

        $this->assertEquals(
            ['OBSERVER_DEFAULT', 'OBSERVER_SPACE1', 'OBSERVER_SPACE2'],
            $testValue
        );

        // Last test: disable all events
        $cut1->disableListeners('*');

        $testValue = [];
        $this->assertEmpty($testValue);

        $evManagerDefault->trigger('my_event', $target);

        $this->assertEquals(
            [],
            $testValue
        );
    }
}
