<?php

namespace OliveOil\Core\Test\Service\Log;

use OliveOil\Core\Service\Log\Resolver;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class ResolverTest
 *
 * @group oliveoil
 * @group services
 */
class ResolverTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    public function getCut() {
        return $this->getContainer()->get(Resolver::class);
    }

    public function testIdentifierMatchesDefinition(): void {
        $cut = $this->getCut();

        $this->assertTrue($cut->identifierMatchesDefinition('Foo.Bar', ['pattern' => 'Foo.Bar']));
        $this->assertTrue($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => 'Foo.Bar.*']));
        $this->assertTrue($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => '*.Bar.Baz']));
        $this->assertTrue($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => 'Foo.*.Baz']));

        $this->assertFalse($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => 'Foo.Bar']));
        $this->assertFalse($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => 'Bar.Baz']));
        $this->assertFalse($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => 'Foo.Baz']));
        $this->assertFalse($cut->identifierMatchesDefinition('Foo.Bar.Baz', ['pattern' => 'Foo.*.Bar']));
    }
}
