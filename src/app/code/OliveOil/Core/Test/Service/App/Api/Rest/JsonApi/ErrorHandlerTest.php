<?php

namespace OliveOil\Core\Test\Service\App\Api\Rest\JsonApi;

use Narrowspark\HttpStatus\Exception\NotFoundException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Service\App\Api\Rest\JsonApi\ErrorHandler;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group api
 * @group oliveoil
 * @group services
 */
class ErrorHandlerTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->setUpControllerTest();

        /** @see src/etc/test/di/di.php */
        $this->getRegistry()->set('skip_exception_after_write_response', true);
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->tearDownControllerTest();
        $this->getSession()->clearMessages();

        /** @see src/etc/test/di/di.php */
        $this->getRegistry()->clear('skip_exception_after_write_response');
    }

    /**
     * @return ErrorHandler
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(ErrorHandler::class);
    }

    /**
     * @group ticket-331
     */
    public function testHandle_exception_developer(): void {
        $fw = $this->getFw();

        $expectedExceptionMessage = uniqid('Exception message ');
        $controller = static function (\Base $fw, array $args) use ($expectedExceptionMessage) : void {
            throw new \Exception($expectedExceptionMessage);
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        try {
            $this->mock($routePattern);
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->assertEquals($expectedExceptionMessage, $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $out = '';
        $this->captureOutput($out, function() use ($fw): void {
            $this->assertTrue($this->getCut()->handle($fw));
        });

        $this->assertNotEmpty($out);
        $json = json_decode((string) $out, true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('errors', $json);
        $this->assertIsArray($json['errors']);
        $this->assertIsArray($json['errors'][0]);
        $this->assertArrayHasKey('status', $json['errors'][0]);

        $this->assertEquals(HttpStatus::STATUS_INTERNAL_SERVER_ERROR, $json['errors'][0]['status']);
        $this->assertEquals($expectedExceptionMessage, $json['errors'][0]['title']);
        $this->assertEquals(\Exception::class, $json['errors'][0]['code']);
        $this->assertEquals($expectedExceptionMessage, $json['errors'][0]['detail']);

        $this->assertArrayHasKey('meta', $json['errors'][0]);
        $this->assertArrayHasKey('trace', $json['errors'][0]['meta']);
        $this->assertNotEmpty($json['errors'][0]['meta']['trace']);
        $this->assertNotEquals('[trace not available]', $json['errors'][0]['meta']['trace']);
    }

    /**
     * @group ticket-331
     */
    public function testHandle_exception_production(): void {
        $fw = $this->getFw();

        $expectedExceptionMessage = uniqid('Exception message ');
        $controller = static function (\Base $fw, array $args) use ($expectedExceptionMessage) : void {
            throw new \Exception($expectedExceptionMessage);
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        try {
            $this->mock($routePattern);
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->assertEquals($expectedExceptionMessage, $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $out = '';
        $this->emulateProductionMode(function() use ($fw, &$out): void {
            $this->captureOutput($out, function() use ($fw): void {
                $this->assertTrue($this->getCut()->handle($fw));
            });
        });

        $this->assertNotEmpty($out);
        $json = json_decode((string) $out, true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('errors', $json);
        $this->assertIsArray($json['errors']);
        $this->assertIsArray($json['errors'][0]);
        $this->assertArrayHasKey('status', $json['errors'][0]);

        $this->assertEquals(HttpStatus::STATUS_INTERNAL_SERVER_ERROR, $json['errors'][0]['status']);
        $this->assertEquals(
            $this->getContainer()->get('errors.silent_message'),
            $json['errors'][0]['title']
        );
        $this->assertEquals(\Exception::class, $json['errors'][0]['code']);
        $this->assertEquals($this->getContainer()->get('errors.silent_detail'), $json['errors'][0]['detail']);
        $this->assertArrayHasKey('meta', $json['errors'][0]);
        $this->assertArrayHasKey('trace', $json['errors'][0]['meta']);
        $this->assertEquals([$this->getContainer()->get('errors.silent_trace')], $json['errors'][0]['meta']['trace']);
    }

    /**
     * @group ticket-331
     */
    public function testHandle_httpException_defaultMessage_developer(): void {
        $fw = $this->getFw();

        $exception = new NotFoundException();
        $controller = static function (\Base $fw, array $args) use ($exception) : void {
            throw $exception;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        try {
            $this->mock($routePattern);
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->assertInstanceOf(NotFoundException::class, $throwable);
            $fw->set('EXCEPTION', $throwable);
        }

        $out = '';
        $this->captureOutput($out, function() use ($fw): void {
            $this->assertTrue($this->getCut()->handle($fw));
        });

        $this->assertNotEmpty($out);
        $json = json_decode((string) $out, true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('errors', $json);
        $this->assertIsArray($json['errors']);
        $this->assertIsArray($json['errors'][0]);

        $this->assertArrayHasKey('status', $json['errors'][0]);
        $this->assertEquals(HttpStatus::STATUS_NOT_FOUND, $json['errors'][0]['status']);
        $this->assertEquals($exception->getMessage(), $json['errors'][0]['title']);
        $this->assertEquals(NotFoundException::class, $json['errors'][0]['code']);
        $this->assertEquals(HttpStatus::getReasonMessage($exception->getStatusCode()), $json['errors'][0]['detail']);
    }

    /**
     * @group ticket-331
     */
    public function testHandle_httpException_defaultMessage_production(): void {
        $fw = $this->getFw();

        $exception = new NotFoundException();
        $controller = static function (\Base $fw, array $args) use ($exception) : void {
            throw $exception;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        try {
            $this->mock($routePattern);
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->assertInstanceOf(NotFoundException::class, $throwable);
            $fw->set('EXCEPTION', $throwable);
        }

        $out = '';
        $this->emulateProductionMode(function() use ($fw, &$out): void {
            $this->captureOutput($out, function() use ($fw): void {
                $this->assertTrue($this->getCut()->handle($fw));
            });
        });

        $this->assertNotEmpty($out);
        $json = json_decode((string) $out, true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('errors', $json);
        $this->assertIsArray($json['errors']);
        $this->assertIsArray($json['errors'][0]);
        $this->assertArrayHasKey('status', $json['errors'][0]);

        $this->assertEquals(HttpStatus::STATUS_NOT_FOUND, $json['errors'][0]['status']);
        $this->assertEquals($exception->getMessage(), $json['errors'][0]['title']);
        $this->assertEquals(NotFoundException::class, $json['errors'][0]['code']);
        $this->assertEquals(HttpStatus::getReasonMessage($exception->getStatusCode()), $json['errors'][0]['detail']);
    }

    /**
     * @group ticket-331
     */
    public function testHandle_httpException_customMessage_production(): void {
        $fw = $this->getFw();

        $exception = new NotFoundException(uniqid('Exception message '));
        $controller = static function (\Base $fw, array $args) use ($exception) : void {
            throw $exception;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        try {
            $this->mock($routePattern);
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->assertEquals($exception->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $out = '';
        $this->emulateProductionMode(function() use ($fw, &$out): void {
            $this->captureOutput($out, function() use ($fw): void {
                $this->assertTrue($this->getCut()->handle($fw));
            });
        });

        $this->assertNotEmpty($out);
        $json = json_decode((string) $out, true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('errors', $json);
        $this->assertIsArray($json['errors']);
        $this->assertIsArray($json['errors'][0]);
        $this->assertArrayHasKey('status', $json['errors'][0]);

        $this->assertEquals($exception->getStatusCode(), $json['errors'][0]['status']);
        $this->assertEquals($exception->getMessage(), $json['errors'][0]['title']);
        $this->assertEquals(NotFoundException::class, $json['errors'][0]['code']);
        $this->assertEquals(HttpStatus::getReasonMessage($exception->getStatusCode()), $json['errors'][0]['detail']);
    }
}
