<?php

namespace OliveOil\Core\Test\Service\App;

use Narrowspark\HttpStatus\Exception\NotFoundException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Exception\AppException;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Service\App\ErrorHandler;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group oliveoil
 * @group services
 */
class ErrorHandlerTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->tearDownControllerTest();
        $this->getSession()->clearMessages();
    }

    /**
     * @return ErrorHandler
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(ErrorHandler::class);
    }

    /**
     * @group ticket-331
     */
    public function testHandle_http_noReferer_production(): void {
        $fw = $this->getFw();

        $expectedException = new NotFoundException();
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->emulateProductionMode(function() use ($routePattern): void {
                $this->mock($routePattern);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $this->emulateProductionMode(function() use ($fw): void {
            $this->assertTrue($this->getCut()->handle($fw));
        });
        $out = $this->getOutput();

        $this->assertNotEmpty($out);
        $this->assertValidHtmlWithoutErrors($out);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }

    /**
     * @group ticket-331
     */
    public function testHandle_http_withReferer_production(): void {
        $fw = $this->getFw();

        $expectedException = new NotFoundException(self::class);
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->emulateProductionMode(function() use ($routePattern): void {
                $this->mock($routePattern, [], ['Referer' => 'http://somewhere.local/']);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $this->emulateProductionMode(function() use ($fw): void {
            $this->assertTrue($this->getCut()->handle($fw));
        });
        $out = $this->getOutput();

        $this->assertNotEmpty($out);
        $this->assertValidHtmlWithoutErrors($out);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }

    /**
     * @group ticket-331
     */
    public function testHandle_http_ajax_production(): void {
        $fw = $this->getFw();

        $expectedException = new NotFoundException();
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__ . ' [ajax]';
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->emulateProductionMode(function() use ($routePattern): void {
                $this->mock($routePattern);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $this->emulateProductionMode(function() use ($fw): void {
            $this->assertTrue($this->getCut()->handle($fw));
        });
        $out = $this->getOutput();

        $this->assertNotEmpty($out);
        $json = json_decode($out, true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('id', $json);
        $this->assertNotEmpty($json['id']);
        $this->assertArrayHasKey('code', $json);
        $this->assertEquals($expectedException->getCode(), $json['code']);
        $this->assertArrayHasKey('httpCode', $json);
        $this->assertEquals($expectedException->getStatusCode(), $json['httpCode']);
        $this->assertArrayHasKey('level', $json);
        $this->assertArrayHasKey('userMessage', $json);
        $this->assertEquals($expectedException->getMessage(), $json['userMessage']);
        $this->assertArrayHasKey('userDetail', $json);
        $this->assertEquals(HttpStatus::getReasonMessage($expectedException->getStatusCode()), $json['userDetail']);
        $this->assertArrayHasKey('userTrace', $json);
        $this->assertEquals($this->getContainer()->get('errors.silent_trace'), $json['userTrace']);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }

    public function testHandle_app_noReferer_production(): void {
        $fw = $this->getFw();

        $expectedException = new AppException(uniqid('Exception message '));
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->emulateProductionMode(function() use ($routePattern): void {
                $this->mock($routePattern);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        try {
            $this->emulateProductionMode(function() use ($fw): void {
                $this->assertNull($this->getCut()->handle($fw));
            });
            $this->fail('Failed rerouting on app error.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getUrl($this->getAppConfig()->getValue('ERROR_FALLBACK_REDIRECT_ROUTE')),
                $reroute->getUrl()
            );
        }

        $out = $this->getOutput();

        $this->assertEmpty($out);
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertNotEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }

    public function testHandle_app_withInvalidReferer_production(): void {
        $fw = $this->getFw();

        $expectedException = new AppException(uniqid('Exception message '));
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->emulateProductionMode(function() use ($routePattern): void {
                $this->mock($routePattern, [], ['Referer' => 'http://somewhere.local/']);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        try {
            $this->emulateProductionMode(function() use ($fw): void {
                $this->assertNull($this->getCut()->handle($fw));
            });
            $this->fail('Failed rerouting on app error with invalid referer.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getUrl($this->getAppConfig()->getValue('ERROR_FALLBACK_REDIRECT_ROUTE')),
                $reroute->getUrl()
            );
        }

        $out = $this->getOutput();

        $this->assertEmpty($out);
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertNotEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }

    public function testHandle_app_withValidReferer_production(): void {
        $fw = $this->getFw();

        $expectedException = new AppException(uniqid('Exception message '));
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__;
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        $referer = $this->getUrl('/some/valid/referer');
        try {
            $this->emulateProductionMode(function() use ($routePattern, $referer): void {
                $this->mock($routePattern, [], ['Referer' => $referer]);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        try {
            $this->emulateProductionMode(function() use ($fw): void {
                $this->assertNull($this->getCut()->handle($fw));
            });
            $this->fail('Failed rerouting on app error with valid referer.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $referer,
                $reroute->getUrl()
            );
        }

        $out = $this->getOutput();

        $this->assertEmpty($out);
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertNotEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }

    /**
     * @group ticket-331
     */
    public function testHandle_app_ajax_production(): void {
        $fw = $this->getFw();

        $expectedException = new AppException(uniqid('Exception message '));
        $controller = static function (\Base $fw, array $args) use ($expectedException) : void {
            throw $expectedException;
        };

        $routePattern = 'GET /oliveoil-unit-tests/errorhandler/' . __FUNCTION__ . ' [ajax]';
        $this->getFw()->route($routePattern, $controller);

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->emulateProductionMode(function() use ($routePattern): void {
                $this->mock($routePattern);
            });
            $this->fail('Failed throwing exception.');
        }
        catch (\Throwable $throwable) {
            $this->handleControllerException();
            $this->assertEquals($expectedException->getMessage(), $throwable->getMessage());
            $fw->set('EXCEPTION', $throwable);
        }

        $this->emulateProductionMode(function() use ($fw): void {
            $this->assertTrue($this->getCut()->handle($fw));
        });
        $out = $this->getOutput();

        $this->assertNotEmpty($out);
        $this->assertIsArray(json_decode($out, true));

        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        $this->assertEmpty($this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
    }
}
