<?php

namespace OliveOil\Core\Test\Service\Url;

use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class BuilderTest
 *
 * @group oliveoil
 * @group services
 */
class BuilderTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();

        $this->getRegistry()->clear('current_controller');
        $this->getFw()->clear('PARAMS');
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getRegistry()->clear('current_controller');
    }

    /**
     * @return \OliveOil\Core\Service\Url\Builder
     */
    protected function newCut() {
        return $this->getContainer()->make(\OliveOil\Core\Service\Url\Builder::class);
    }

    public function testGetUrl(): void {
        $testData = [
            1 => [
                'path' => '',

                'resultPath' => ''
            ],
            2 => [
                'path' => '/',

                'resultPath' => ''
            ],
            3 => [
                'path' => 'my/path',

                'resultPath' => 'my/path'
            ],
            4 => [
                'path' => '*/*',

                'resultPath' => '1/2',
                'relativeController' => $this->getAutocompleteAwareController()
            ],
            5 => [
                'path' => null,
                'urlParams' => [
                    '_current' => true,
                ],

                'resultPath' => '1/2/3',
                'relativeController' => $this->getAutocompleteAwareController()
            ],
            6 => [
                'path' => null,
                'urlParams' => [
                    '_current' => true,
                ],

                'resultPath' => '1/2/3?myquery=somevalue',
                'GETquery' => ['myquery' => 'somevalue'],
                'relativeController' => $this->getAutocompleteAwareController()
            ],
            7 => [
                'path' => 'my/path',
                'urlParams' => [
                    '_referer' => true,
                ],

                'resultPath' => 'my/path?referer=' . \OliveOil\base64_url_encode($this->prependBaseUrl('1/2/3')),
                'relativeController' => $this->getAutocompleteAwareController()
            ],
            8 => [
                'path' => 'my/path',
                'GETquery' => ['referer' => \OliveOil\base64_url_encode($this->prependBaseUrl('my/referer_path'))],
                'urlParams' => [
                    '_referer' => '_keep',
                ],

                'resultPath' => 'my/path?referer=' . \OliveOil\base64_url_encode($this->prependBaseUrl('my/referer_path')),
                'relativeController' => $this->getAutocompleteAwareController()
            ],
            9 => [
                'path' => 'my/path',
                'GETquery' => ['referer' => \OliveOil\base64_url_encode('invalid/referer_path')],
                'urlParams' => [
                    '_referer' => '_keep',
                ],

                'resultPath' => 'my/path',
                'relativeController' => $this->getAutocompleteAwareController()
            ],
        ];

        foreach ($testData as $n => $td) {
            $cut = $this->newCut();
            if (isset($td['relativeController'])) {
                $this->getRegistry()->set('current_controller', $td['relativeController']);
            }
            else {
                $this->getRegistry()->clear('current_controller');
            }

            if (isset($td['GETquery'])) {
                $this->getFw()->set('GET', $td['GETquery']);
            }
            else {
                $this->getFw()->clear('GET');
            }

            $this->assertEquals(
                $this->prependBaseUrl($td['resultPath']),
                $cut->getUrl($td['path'], $td['urlParams'] ?? []),
                'Test case #' . $n
            );
        }
    }

    /**
     * @param string $path
     */
    protected function prependBaseUrl($path): string {
        return $this->getAppConfig()->getValue('BASE_URL') . trim($path, '/');
    }

    protected function getAutocompleteAwareController(): \OliveOil\Core\Controller\AutocompleteAwareController {
        return new class implements \OliveOil\Core\Controller\AutocompleteAwareController {
            public function autocompleteUrlPath($path): ?string {
                return preg_replace_callback('/\*/', static function ($matches) {
                    static $occ = 1;
                    return $occ++;
                }, $path);
            }
        };
    }
}
