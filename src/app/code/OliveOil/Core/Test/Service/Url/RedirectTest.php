<?php

namespace OliveOil\Core\Test\Service\Url;

use OliveOil\Core\Exception\Url\InvalidRedirectUrlException;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class RedirectTest
 *
 * @group oliveoil
 * @group services
 */
class RedirectTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();

        $this->getRegistry()->clear('current_controller');
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getRegistry()->clear('current_controller');
    }

    /**
     * @return \OliveOil\Core\Service\Url\Redirect
     * @throws \DI\NotFoundException
     */
    protected function newCut() {
        return $this->getContainer()->make(\OliveOil\Core\Service\Url\Redirect::class);
    }

    public function testGetUrlForRedirect(): void {
        $cut = $this->newCut();

        $this->assertEquals(
            $this->prependBaseUrl('redirect/index/to/' . \OliveOil\base64_url_encode('my/action?key=value#myfragment')),
            $cut->getUrlForRedirect(
                'my/action',
                [
                    '_query' => ['key' => 'value'],
                    '_fragment' => 'myfragment'
                ]
            )
        );
    }

    public function testToRedirectUrl(): void {
        $cut = $this->newCut();

        $this->assertEquals(
            $this->prependBaseUrl('redirect/index/to/' . \OliveOil\base64_url_encode('my/action?key=value#myfragment')),
            $cut->toRedirectUrl($this->getUrl(
                'my/action',
                [
                    '_query' => ['key' => 'value'],
                    '_fragment' => 'myfragment'
                ]
            ))
        );
    }

    public function testToRedirectUrl_invalid(): void {
        $cut = $this->newCut();

        try {
            $cut->toRedirectUrl('https://not.base.url/some/action');
            $this->fail('Failed preventing creating redirection for an invalid URL.');
        }
        catch (InvalidRedirectUrlException) {
            $this->assertTrue(true);
        }
    }

    public function testGetTargetRedirectUrl(): void {
        $cut = $this->newCut();

        $this->assertEquals(
            $this->prependBaseUrl('my/action?key=value#myfragment'),
            $cut->getTargetRedirectUrl(\OliveOil\base64_url_encode('my/action?key=value#myfragment'))
        );
    }

    protected function prependBaseUrl(string $path): string {
        return $this->getUrlBuilder()->getFullBaseUrl() . $path;
    }
}
