<?php

namespace OliveOil\Core\Test;


interface BootstrapInterface
{
    /**
     * @return void
     */
    public function bootstrap();
}
