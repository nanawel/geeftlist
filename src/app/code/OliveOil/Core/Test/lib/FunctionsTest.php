<?php

namespace OliveOil\Core\Test\lib;


use PHPUnit\Framework\TestCase;

class FunctionsTest extends TestCase
{
    public function testConvertToBytes_caseSensitive(): void {
        $testData = [
            '2' => 2,
            '2B' => 2,
            '4kB' => 4096,
            '4 kiB' => 4096,
            '6 GB' => 6442450944,
            '6 GiB' => 6442450944,
        ];

        foreach ($testData as $input => $expectedOutput) {
            $this->assertEquals($expectedOutput, \OliveOil\convertToBytes($input));
        }

        $testData = [
            'B' => 'Not a valid size.',
            '6.5 GB' => 'Not a valid size.',
            '2b' => 'Invalid size unit "b".',
            '4KB' => 'Invalid size unit "KB".',
            '4 x' => 'Invalid size unit "x".',
        ];

        foreach ($testData as $input => $expectedErrorMessage) {
            try {
                \OliveOil\convertToBytes($input);
                $this->fail(sprintf(
                    'Failed throwing exception with message "%s" for input "%s"',
                    $expectedErrorMessage,
                    $input
                ));
            }
            catch (\InvalidArgumentException $e) {
                $this->assertEquals($expectedErrorMessage, $e->getMessage());
            }
        }
    }

    public function testConvertToBytes_caseInsensitive(): void {
        $testData = [
            '2' => 2,
            '2b' => 2,
            '4KB' => 4096,
            '4 Kib' => 4096,
            '6 gb' => 6442450944,
            '6 giB' => 6442450944,
        ];

        foreach ($testData as $input => $expectedOutput) {
            $this->assertEquals($expectedOutput, \OliveOil\convertToBytes($input, false));
        }
    }
}