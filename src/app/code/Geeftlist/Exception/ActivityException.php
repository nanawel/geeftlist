<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class ActivityException extends AppException
{
}
