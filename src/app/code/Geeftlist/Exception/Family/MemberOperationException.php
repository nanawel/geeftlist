<?php

namespace Geeftlist\Exception\Family;


use OliveOil\Core\Exception\AppException;

class MemberOperationException extends AppException
{
}
