<?php

namespace Geeftlist\Exception\Family;


use Geeftlist\Exception\SecurityException;

class MembershipException extends SecurityException
{
}
