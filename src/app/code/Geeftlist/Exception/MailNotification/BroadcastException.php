<?php

namespace Geeftlist\Exception\MailNotification;


use OliveOil\Core\Exception\SystemException;

class BroadcastException extends SystemException
{
}
