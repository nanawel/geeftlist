<?php

namespace Geeftlist\Exception\Invitation;


use Geeftlist\Exception\InvitationException;

class UnavailableException extends InvitationException
{
}
