<?php

namespace Geeftlist\Exception\Invitation;


use Geeftlist\Exception\Security\PermissionException;

class MaxReachedException extends PermissionException
{
}
