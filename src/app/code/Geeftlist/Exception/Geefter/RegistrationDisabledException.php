<?php

namespace Geeftlist\Exception\Geefter;

use Geeftlist\Exception\GeefterException;

class RegistrationDisabledException extends GeefterException
{
}
