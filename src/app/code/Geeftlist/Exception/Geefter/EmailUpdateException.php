<?php

namespace Geeftlist\Exception\Geefter;


use OliveOil\Core\Exception\AppException;

class EmailUpdateException extends AppException
{
}
