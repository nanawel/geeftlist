<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class ReservationException extends AppException
{
}
