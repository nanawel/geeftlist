<?php

namespace Geeftlist\Exception\Gift;


use Geeftlist\Exception\Security\PermissionException;

class ManageException extends PermissionException
{
}
