<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class DiscussionException extends AppException
{
}
