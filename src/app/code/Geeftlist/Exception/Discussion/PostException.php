<?php

namespace Geeftlist\Exception\Discussion;


use Geeftlist\Exception\DiscussionException;

class PostException extends DiscussionException
{
}
