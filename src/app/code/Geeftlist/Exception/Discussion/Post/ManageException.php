<?php

namespace Geeftlist\Exception\Discussion\Post;


use Geeftlist\Exception\Security\PermissionException;

class ManageException extends PermissionException
{
}
