<?php

namespace Geeftlist\Exception\Discussion;


use Geeftlist\Exception\DiscussionException;

class TopicException extends DiscussionException
{
}
