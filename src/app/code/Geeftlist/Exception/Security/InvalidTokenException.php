<?php

namespace Geeftlist\Exception\Security;


use Geeftlist\Exception\SecurityException;

class InvalidTokenException extends SecurityException
{
}
