<?php

namespace Geeftlist\Exception\Security;


use Geeftlist\Exception\SecurityException;

class UnrelatedGeefteeException extends SecurityException
{
}
