<?php

namespace Geeftlist\Exception\Security;


use Geeftlist\Exception\SecurityException;

class AuthenticationException extends SecurityException
{
}
