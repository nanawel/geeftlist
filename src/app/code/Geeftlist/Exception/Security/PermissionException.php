<?php

namespace Geeftlist\Exception\Security;


use Geeftlist\Exception\SecurityException;

class PermissionException extends SecurityException
{
}
