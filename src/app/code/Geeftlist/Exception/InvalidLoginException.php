<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class InvalidLoginException extends AppException
{
}
