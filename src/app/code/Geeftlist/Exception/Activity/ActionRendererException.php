<?php

namespace Geeftlist\Exception\Activity;


use Geeftlist\Exception\ActivityException;

class ActionRendererException extends ActivityException
{
}
