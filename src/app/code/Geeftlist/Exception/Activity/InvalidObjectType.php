<?php

namespace Geeftlist\Exception\Activity;


use Geeftlist\Exception\ActivityException;

class InvalidObjectType extends ActivityException
{
}
