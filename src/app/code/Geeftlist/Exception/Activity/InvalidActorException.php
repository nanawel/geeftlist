<?php

namespace Geeftlist\Exception\Activity;


use Geeftlist\Exception\ActivityException;

class InvalidActorException extends ActivityException
{
}
