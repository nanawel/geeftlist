<?php

namespace Geeftlist\Exception\Geeftee;


use OliveOil\Core\Exception\AppException;

class ClaimRequestException extends AppException
{
}
