<?php

namespace Geeftlist\Exception\Geeftee\ClaimRequest;


use Geeftlist\Exception\Geeftee\ClaimRequestException;

class AlreadyExistsException extends ClaimRequestException
{
}
