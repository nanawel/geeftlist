<?php


namespace Geeftlist\Exception\Api\Rest;


use OliveOil\Core\Exception\ApiException;

class JsonApiException extends ApiException
{
}
