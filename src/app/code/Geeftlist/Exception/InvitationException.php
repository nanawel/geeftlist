<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class InvitationException extends AppException
{
}
