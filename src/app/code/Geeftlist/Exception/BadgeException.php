<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class BadgeException extends AppException
{
}
