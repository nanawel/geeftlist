<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class InvalidUploadedFile extends AppException
{
}
