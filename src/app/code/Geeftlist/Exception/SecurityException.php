<?php

namespace Geeftlist\Exception;


use OliveOil\Core\Exception\AppException;

class SecurityException extends AppException
{
}
