<?php

namespace Geeftlist\Helper\Gift\Discussion;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Gift;

class Topic
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function getTopicCollection(Gift $gift)
    {
        /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection $topics */
        $topics = $this->resourceModelFactory->resolveMakeCollection(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE)
            ->addFieldToFilter('entity', [\Geeftlist\Model\Gift::ENTITY_TYPE, $gift->getId()]);

        return $topics;
    }

    /**
     * @param bool $autoCreate
     * @return \Geeftlist\Model\Discussion\Topic
     */
    public function getPublicTopic(Gift $gift, $autoCreate = true) {
        return $this->getTopic($gift, Gift\Constants::TOPIC_VISIBILITY_PUBLIC, $autoCreate);
    }

    /**
     * @param bool $autoCreate
     * @return \Geeftlist\Model\Discussion\Topic
     */
    public function getProtectedTopic(Gift $gift, $autoCreate = true) {
        return $this->getTopic($gift, Gift\Constants::TOPIC_VISIBILITY_PROTECTED, $autoCreate);
    }

    /**
     * @param bool $autoCreate
     * @return \Geeftlist\Model\Discussion\Topic|null
     */
    protected function getTopic(Gift $gift, $visibility, $autoCreate = true) {
        if (
            ($currentGeeftee = $this->geefterSession->getGeeftee()) && ($visibility == Gift\Constants::TOPIC_VISIBILITY_PROTECTED
            && in_array($currentGeeftee->getId(), $gift->getGeefteeCollection()->getAllIds()))
        ) {
            // Do not return nor create a protected topic for a gift which is for the current geeftee
            return null;
        }

        /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection $topics */
        $topics = $this->resourceModelFactory->resolveMakeCollection(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE);
        $topics->addFieldToFilter('entity', [\Geeftlist\Model\Gift::ENTITY_TYPE, $gift->getId()])
            ->addFieldToFilter('metadata', [Gift\Constants::TOPIC_VISIBILITY_KEY, $visibility]);

        if (! $topics->count() && $autoCreate) {
            $topic = $this->modelFactory->resolveMake(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE, ['data' => [
                'title'              => sprintf('[GIFT %s (%s)]', $gift->getId(), $visibility),
                'author_id'          => $gift->getCreatorId(),
                'linked_entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'linked_entity_id'   => $gift->getId()
            ]]);

            try {
                $topic->setMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY, $visibility)
                    ->save();
            }
            catch (PermissionException $e) {
                // Possible reason: no permission to access linked entity
                $this->logger->notice('Could not save gift topic: ' . $e->getMessage());

                return null;
            }
        }
        else {
            $topic = $topics->getFirstItem();
        }

        return $topic;
    }
}
