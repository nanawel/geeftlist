<?php

namespace Geeftlist\Helper\Gift\Discussion;

use Geeftlist\Model\Gift;

class Post
{
    public function __construct(protected \Geeftlist\Model\ResourceFactory $resourceModelFactory, protected \Geeftlist\Helper\Gift\Discussion\Topic $topicHelper, protected \Geeftlist\Model\Session\GeefterInterface $geefterSession, protected \OliveOil\Core\Service\Cache $cacheService)
    {
    }

    public function getPostCount(Gift $gift)
    {
        $currentGeefterId = $this->geefterSession->isLoggedIn()
            ? $this->geefterSession->getGeefterId()
            : 0;

        // It's a pretty heavy process to retrieve this data for a large number of gifts, so cache here is necessary
        $cacheKey = sprintf(
            'discussion_post_count.gift_%d.geefter_%d',
            $gift->getId(),
            $currentGeefterId
        );
        if (($cnt = $this->cacheService->get($cacheKey)) === null) {
            /** @var \Geeftlist\Model\Discussion\Topic[] $topics */
            $topics = $this->topicHelper->getTopicCollection($gift)
                ->getItems();

            /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Post\Collection $posts */
            $posts = $this->resourceModelFactory->resolveMakeCollection(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE)
                ->addFieldToFilter('topics', ['in' => $topics]);
            $cnt = $posts->count();

            $this->cacheService->set($cacheKey, $cnt, null, array_merge(
                $gift->getIdentities(),
                ...array_map(static fn(\Geeftlist\Model\Discussion\Topic $topic): array => $topic->getIdentities(), $topics)
            ));
        }

        return $cnt;
    }
}
