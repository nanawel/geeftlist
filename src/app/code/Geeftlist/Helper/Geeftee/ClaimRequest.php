<?php

namespace Geeftlist\Helper\Geeftee;

use Geeftlist\Exception\Geeftee\ClaimRequest\AlreadyExistsException;
use Geeftlist\Exception\Geeftee\ClaimRequestException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Helper\DateTime;

class ClaimRequest
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \Geeftlist\Service\Security $securityService,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        \OliveOil\Core\Service\EventInterface $eventService,
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    /**
     * @throws AlreadyExistsException
     */
    public function request(int|Geeftee $geeftee, int|Geefter $geefter): Geeftee\ClaimRequest {
        $this->eventManager->trigger('request::before', $this, ['geeftee' => $geeftee, 'geefter' => $geefter]);

        $geeftee = $this->getGeeftee($geeftee);
        $geefter = $this->getGeefter($geefter);

        if ($this->hasPendingRequest($geefter, $geeftee)) {
            throw new AlreadyExistsException(sprintf(
                "Claim request for geeftee %d by geefter %d already exists",
                $geeftee->getId(),
                $geefter->getId()
            ));
        }

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->modelFactory->resolveMake(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geeftee_id'    => $geeftee->getId(),
            'geefter_id'    => $geefter->getId(),
            'decision_code' => Geeftee\ClaimRequest::DECISION_CODE_PENDING
        ]]);
        $this->eventManager->trigger('request::after', $this, ['claim_request' => $cr]);

        $cr->save();

        // Immediately accept CR if geeftee has no creator
        if (!$geeftee->getCreator()) {
            $this->securityService->callPrivileged(function () use ($cr): void {
                $this->acceptRequest($cr);
            });
        }

        return $cr;
    }

    public function acceptRequest(Geeftee\ClaimRequest $claimRequest): static {
        $this->eventManager->trigger('acceptRequest::before', $this, ['claim_request' => $claimRequest]);

        $geeftee = $claimRequest->getGeeftee();
        $geefter = $claimRequest->getGeefter();

        $this->checkRequest($claimRequest);

        $targetGeeftee = $geefter->getGeeftee();

        $this->transactionHelper->execute(function () use ($claimRequest, $targetGeeftee, $geeftee): void {
            // Need to bypass security checks as $targetGeeftee is now independant
            // (creator_id is null and geefter_id is not)
            $this->securityService->callPrivileged(
                function (
                    \Geeftlist\Model\Geeftee $geeftee,
                    \Geeftlist\Model\Geeftee $sourceGeeftee,
                    bool $persist = true
                ): void {
                    $this->geefteeHelper->merge($geeftee, $sourceGeeftee, $persist);
                },
                [$targetGeeftee, $geeftee]
            );

            $claimRequest->setDecisionCode(Geeftee\ClaimRequest::DECISION_CODE_ACCEPTED)
                ->setDecisionDate(DateTime::getDateSql())
                ->save();
        });

        $this->eventManager->trigger('acceptRequest::after', $this, ['claim_request' => $claimRequest]);

        return $this;
    }

    public function rejectRequest(Geeftee\ClaimRequest $claimRequest): static {
        $this->eventManager->trigger('rejectRequest::before', $this, ['claim_request' => $claimRequest]);

        $this->checkRequest($claimRequest);

        $claimRequest->setDecisionCode(Geeftee\ClaimRequest::DECISION_CODE_REJECTED)
            ->setDecisionDate(DateTime::getDateSql())
            ->save();

        $this->eventManager->trigger('rejectRequest::after', $this, ['claim_request' => $claimRequest]);
        return $this;
    }

    public function cancelRequest(Geeftee\ClaimRequest $claimRequest): static {
        $this->eventManager->trigger('cancelRequest::before', $this, ['claim_request' => $claimRequest]);

        $this->checkCancelRequest($claimRequest);

        $claimRequest->setDecisionCode(Geeftee\ClaimRequest::DECISION_CODE_CANCELED)
            ->setDecisionDate(DateTime::getDateSql())
            ->save();

        $this->eventManager->trigger('cancelRequest::after', $this, ['claim_request' => $claimRequest]);
        return $this;
    }

    protected function hasPendingRequest(Geefter $geefter, Geeftee $geeftee): bool {
        $collection = $this->resourceModelFactory->resolveMakeCollection(Geeftee\ClaimRequest::ENTITY_TYPE)
            ->addFieldToFilter('geefter_id', $geefter->getId())
            ->addFieldToFilter('geeftee_id', $geeftee->getId())
            ->addFieldToFilter('decision_code', Geeftee\ClaimRequest::DECISION_CODE_PENDING);
        return $collection->count() > 0;
    }

    /**
     * @throws ClaimRequestException
     * @throws NoSuchEntityException
     */
    protected function checkRequest(Geeftee\ClaimRequest $claimRequest): static {
        $geeftee = $claimRequest->getGeeftee();

        if (! $geeftee || ! $geeftee->getId() || $geeftee->getGeefterId()) {
            $claimRequest->setDecisionCode(Geeftee\ClaimRequest::DECISION_CODE_CANCELED)
                ->save();
            throw new ClaimRequestException('This request is invalid and has been cancelled (invalid geeftee).');
        }

        if ($claimRequest->getDecisionCode() != Geeftee\ClaimRequest::DECISION_CODE_PENDING) {
            throw new ClaimRequestException('Invalid request.');
        }

        if (! $geeftee->getCreator()) {
            $claimRequest->setDecisionCode(Geeftee\ClaimRequest::DECISION_CODE_CANCELED)
                ->save();
            throw new ClaimRequestException('This request is invalid and has been cancelled.');
        }

        return $this;
    }

    /**
     * @throws PermissionException
     */
    protected function checkCancelRequest(Geeftee\ClaimRequest $claimRequest): static {
        $geefter = $this->geefterSession->getGeefter();

        if ($claimRequest->getGeefterId() != $geefter->getId()) {
            throw new PermissionException('Must be the creator of the request to cancel it.');
        }

        return $this;
    }

    /**
     * @throws NoSuchEntityException
     */
    protected function getGeeftee(int|Geeftee $geeftee): Geeftee {
        if ($geeftee instanceof Geeftee) {
            return $geeftee;
        }

        if (is_numeric($geeftee)) {
            $geefteeModel = $this->modelFactory->resolveMake(Geeftee::ENTITY_TYPE)
                ->load((int) $geeftee);
            if ($geefteeModel->getId()) {
                return $geefteeModel;
            }
        }

        throw new NoSuchEntityException('Invalid or unknown geeftee!');
    }

    /**
     * @throws NoSuchEntityException
     */
    protected function getGeefter(int|Geefter $geefter): Geefter {
        if ($geefter instanceof Geefter) {
            return $geefter;
        }

        if (is_numeric($geefter)) {
            $geefterModel = $this->modelFactory->resolveMake(Geefter::ENTITY_TYPE)
                ->load((int) $geefter);
            if ($geefterModel->getId()) {
                return $geefterModel;
            }
        }

        throw new NoSuchEntityException('Invalid geefter');
    }
}
