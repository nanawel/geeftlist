<?php

namespace Geeftlist\Helper;

use Geeftlist\Model\Family as FamilyModel;
use Geeftlist\Model\Geefter as GeefterModel;

class Family
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        \OliveOil\Core\Service\Event $eventService,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \Geeftlist\Service\Security $securityService
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    public function transfer(FamilyModel $family, GeefterModel $newOwner): void {
        $this->eventManager->trigger('transfer::before', $family, [
            'new_owner' => $newOwner
        ]);

        // Need to disable security checks here
        $this->securityService->callPrivileged(function () use ($family, $newOwner): void {
            $this->doTransfer($family, $newOwner);
        });

        $this->eventManager->trigger('transfer::after_commit', $family, [
            'new_owner' => $newOwner
        ]);
    }

    protected function doTransfer(FamilyModel $family, GeefterModel $newOwner): static {
        $transaction = $this->transactionHelper->newTransaction();
        try {
            $transaction->begin();

            $this->eventManager->trigger('transfer::do', $family, [
                'new_owner' => $newOwner
            ]);

            // Add new owner as member first is necessary
            if (! $family->isMember($newOwner->getGeeftee())) {
                $family->addGeeftee($newOwner->getGeeftee());
            }

            $family->setOwnerId($newOwner->getId())
                ->setFlag('allow_update_owner', true)
                ->save();

            $this->eventManager->trigger('transfer::after', $family, [
                'new_owner' => $newOwner
            ]);

            $transaction->commit();
        }
        catch (\Throwable $throwable) {
            $transaction->rollback();

            throw $throwable;
        }

        return $this;
    }
}
