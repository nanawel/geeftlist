<?php

namespace Geeftlist\Helper;

use Geeftlist\Exception\Geefter\AccountDeletionException;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter as GeefterModel;

class Geefter
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        \OliveOil\Core\Service\Event $eventService,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \Geeftlist\Service\Security $securityService
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    /**
     * Create or update geeftees for the given geefters.
     */
    public function deleteAccount(GeefterModel $geefter, array $params = []): static {
        /** @var Geeftee $geeftee */
        $geeftee = $geefter->getGeeftee();
        $recovererGeefter = false;
        if (! empty($params['recoverer_geefter_id'])) {
            $recovererGeefter = $this->modelFactory->resolveMake(GeefterModel::ENTITY_TYPE)
                ->load($params['recoverer_geefter_id']);

            if (! $recovererGeefter->getId()) {
                throw new AccountDeletionException('Invalid recoverer geefter ID');
            }
        }

        $this->eventManager->trigger('deleteAccount::before', $geefter, [
            'geeftee' => $geeftee,
            'recoverer_geefter' => $recovererGeefter,
            'params' => $params
        ]);

        $this->transactionHelper->execute(function () use ($geefter, $geeftee, $recovererGeefter, $params): void {
            $this->eventManager->trigger('deleteAccount::do', $geefter, [
                'geeftee' => $geeftee,
                'recoverer_geefter' => $recovererGeefter,
                'params' => $params
            ]);

            // Need to bypass security checks as we update both creator_id and geefter_id at once
            $this->securityService->callPrivileged(static function () use ($geeftee, $recovererGeefter): void {
                if ($recovererGeefter) {
                    $geeftee->setCreatorId($recovererGeefter->getId());
                }

                $geeftee->setGeefterId(null)
                    ->setEmail(null)    // Make it GDPR compliant by also deleting the email from the geeftee
                    ->save();
            });

            // Finally delete the geefter itself
            $geefter->delete();

            $this->eventManager->trigger('deleteAccount::after', $geefter, [
                'geeftee' => $geeftee,
                'recoverer_geefter' => $recovererGeefter,
                'params' => $params
            ]);
        });
        $this->eventManager->trigger('deleteAccount::after_commit', $geefter, [
            'geeftee' => $geeftee,
            'recoverer_geefter' => $recovererGeefter,
            'params' => $params
        ]);
        $this->eventManager->trigger('geefter_delete_account', $geefter, [
            'geeftee' => $geeftee,
            'recoverer_geefter' => $recovererGeefter,
            'params' => $params
        ]);

        return $this;
    }
}
