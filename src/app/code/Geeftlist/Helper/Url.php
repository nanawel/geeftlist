<?php

namespace Geeftlist\Helper;


use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Service\Gift\Image;

class Url
{
    public const GEEFTEE_PROFILE_PATH                 = 'geeftee_profile/index';
    public const GIFT_VIEW_PATH                       = 'gift/view';
    public const GIFT_EDIT_PATH                       = 'gift/edit';
    public const GIFT_ARCHIVE_PATH                    = 'gift/archive';
    public const GIFT_DUPLICATE_PATH                  = 'gift/duplicate';
    public const GIFT_DUPLICATE_ARCHIVE_PATH          = 'gift/duplicateArchive';
    public const GIFT_CONFIRM_REQUEST_ARCHIVING_PATH  = 'gift/confirmRequestArchiving';
    public const GIFT_REQUEST_ARCHIVING_PATH          = 'gift/requestArchiving';
    public const GIFT_REPORT_PATH                     = 'gift/report';
    public const GIFT_DELETE_PATH                     = 'gift/delete';
    public const GIFT_RESERVE_PATH                    = 'gift_reservation/index';
    public const FAMILY_VIEW_PATH                     = 'family_explore/index';
    public const GIFTLIST_VIEW_PATH                   = 'giftlist/view';
    public const GIFTLIST_EDIT_PATH                   = 'giftlist_manage/edit';
    public const GIFTLIST_DELETE_PATH                 = 'giftlist_manage/delete';

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        protected \OliveOil\Core\Service\Url\Builder $urlBuilder,
        protected \Geeftlist\Service\Avatar\GeefteeInterface $avatarService,
        protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository,
        protected \Geeftlist\Service\Gift\Image $giftImageService,
        protected \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function getGeefteeProfileUrl($geefteeId, $params = []): string {
        try {
            if ($geefteeId instanceof Geefter) {
                $geefteeId = $geefteeId->getGeeftee();
            }
        }
        catch (PermissionException) {
            return '#some-geeftee-you-cant-see';
        }

        if ($geefteeId instanceof Geeftee) {
            $geefteeId = $geefteeId->getId();
        }

        if (! is_numeric($geefteeId)) {
            return '#no-such-geeftee';
        }

        return $this->urlBuilder->getUrl(
            self::GEEFTEE_PROFILE_PATH,
            array_merge($params, ['geeftee_id' => $geefteeId])
        );
    }

    public function getGeefterProfileUrl($geefterId, $params = []): string {
        if ($geefterId instanceof Geefter) {
            $geefterId = $geefterId->getId();
        }

        if (! is_numeric($geefterId)) {
            return '#no-such-geeftee';
        }

        return $this->urlBuilder->getUrl(
            self::GEEFTEE_PROFILE_PATH,
            array_merge($params, ['geefter_id' => $geefterId])
        );
    }

    /**
     * @param int|Geeftee $geefteeId
     * @param array $params
     * @return string
     */
    public function getGeefteeAvatarUrl($geefteeId, $params = []) {
        try {
            if ($geefteeId instanceof Geefter) {
                $geefteeId = $geefteeId->getGeeftee();
            }

            if (! $geefteeId instanceof Geeftee) {
                $geefteeId = $this->geefteeRepository->get($geefteeId);
            }
        }
        catch (\Throwable $e) {
            $this->logger->warning(sprintf(
                'Could not retrieve avatar for geeftee #%d: %s',
                $geefteeId,
                $e->getMessage()
            ));
            $this->logger->warning($e);

            return $this->avatarService->getPlaceholderUrl();
        }

        return $this->avatarService->getUrl($geefteeId, $params);
    }

    /**
     * @param int|Geefter $geefterId
     * @param array $params
     * @return string
     */
    public function getGeefterAvatarUrl($geefterId, $params = []) {
        if ($geefterId instanceof Geefter) {
            $geefterId = $geefterId->getId();
        }

        if (! is_numeric($geefterId)) {
            return $this->avatarService->getPlaceholderUrl();
        }

        try {
            return $this->getGeefteeAvatarUrl($this->geefteeRepository->get($geefterId, 'geefter_id'), $params);
        }
        catch (\Throwable $e) {
            $this->logger->warning(sprintf(
                'Could not retrieve avatar for geefter #%d: %s',
                $geefterId,
                $e->getMessage()
            ));
            $this->logger->warning($e);

            return $this->avatarService->getPlaceholderUrl();
        }
    }

    /**
     * @param Gift|int $gift
     * @param array $params
     */
    public function getGiftViewUrl($gift, $params = []): string {
        $giftId = is_object($gift) ? $gift->getId() : $gift;

        return $this->urlBuilder->getUrl(
            self::GIFT_VIEW_PATH,
            array_merge($params, ['gift_id' => $giftId])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftEditUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_EDIT_PATH,
            array_merge($params, ['gift_id' => $gift->getId()])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftArchiveUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_ARCHIVE_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftDuplicateArchiveUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_DUPLICATE_ARCHIVE_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftDuplicateUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_DUPLICATE_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getConfirmArchivingRequestUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_CONFIRM_REQUEST_ARCHIVING_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getArchivingRequestUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_REQUEST_ARCHIVING_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftReportUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_REPORT_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftDeleteUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_DELETE_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     */
    public function getGiftReserveUrl($gift, $params = []): string {
        return $this->urlBuilder->getUrl(
            self::GIFT_RESERVE_PATH,
            array_merge($params, ['gift_id' => $gift->getId(), '_csrf' => true])
        );
    }

    /**
     * @param Gift $gift
     * @param array $params
     * @return string
     */
    public function getGiftImageUrl(\Geeftlist\Model\Gift $gift, $type = Image::IMAGE_TYPE_FULL, $params = []) {
        return $this->giftImageService->getUrl($gift, $type, $params);
    }

    /**
     * @param Family|int $family
     * @param array $params
     */
    public function getFamilyViewUrl($family, $params = []): string {
        $familyId = is_object($family) ? $family->getId() : $family;

        return $this->urlBuilder->getUrl(
            self::FAMILY_VIEW_PATH,
            array_merge($params, ['family_id' => $familyId])
        );
    }

    /**
     * @param GiftList|int $giftList
     * @param array $params
     */
    public function getGiftListViewUrl($giftList, $params = []): string {
        $giftListId = is_object($giftList) ? $giftList->getId() : $giftList;

        return $this->urlBuilder->getUrl(
            self::GIFTLIST_VIEW_PATH,
            array_merge($params, ['giftlist_id' => $giftListId])
        );
    }

    /**
     * @param GiftList|int $giftList
     * @param array $params
     */
    public function getGiftListEditUrl($giftList, $params = []): string {
        $giftListId = is_object($giftList) ? $giftList->getId() : $giftList;

        return $this->urlBuilder->getUrl(
            self::GIFTLIST_EDIT_PATH,
            array_merge($params, ['giftlist_id' => $giftListId])
        );
    }

    /**
     * @param GiftList|int $giftList
     * @param array $params
     */
    public function getGiftListDeleteUrl($giftList, $params = []): string {
        $giftListId = is_object($giftList) ? $giftList->getId() : $giftList;

        return $this->urlBuilder->getUrl(
            self::GIFTLIST_DELETE_PATH,
            array_merge($params, ['giftlist_id' => $giftListId, '_csrf' => true])
        );
    }

    public function getDiscussionPostUrl(Post $post, $params = []): ?string {
        return $this->getDiscussionTopicUrl(
            $post->getTopic(),
            array_merge($params, ['_fragment' => $this->getDiscussionPostUrlFragment($post)])
        );
    }

    public function getDiscussionTopicUrl(Topic $topic, $params = []): ?string {
        /**
         * FIXME Not great, should be in a dedicated class
         * @see \Geeftlist\Service\MailNotification\Item\Discussion\Post::newToItemData()
         */
        switch ($topic->getLinkedEntityType()) {
            case Gift::ENTITY_TYPE:
                $path = self::GIFT_VIEW_PATH;
                $params = array_merge($params, ['gift_id' => $topic->getLinkedEntityId()]);
                break;

            default:
                $path = null;
        }

        if (! $path) {
            return null;
        }

        return $this->urlBuilder->getUrl(
            $path,
            $params
        );
    }

    public function getDiscussionPostUrlFragment($post): string {
        $postId = is_object($post) ? $post->getId() : $post;

        return 'discussion-post-' . $postId;
    }
}
