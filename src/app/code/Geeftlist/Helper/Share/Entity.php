<?php

namespace Geeftlist\Helper\Share;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Share\Entity\Rule;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use OliveOil\Core\Exception\Di\Exception;

class Entity
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \OliveOil\Core\Model\RepositoryInterface $shareEntityRuleRepository,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \OliveOil\Core\Service\GenericFactoryInterface $shareRuleTypeFactoryFactory,
        \OliveOil\Core\Service\Log $logService,
        protected array $defaultShareEntityRule
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @param string $entityType
     */
    public function applyDefaultShareRuleToAllEntities($entityType): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Share\Entity\Rule[] $entityShareRules */
        $entityShareRules = $this->shareEntityRuleRepository->find([
            'filter' => [
                'target_entity_type' => [['eq' => $entityType]]
            ]
        ]);

        $entityIdsWithADefaultRule = [];
        foreach ($entityShareRules as $entityShareRule) {
            $entityIdsWithADefaultRule[] = $entityShareRule->getTargetEntityId();
        }

        /** @var \OliveOil\Core\Model\RepositoryInterface $entityRepositoryFactory */
        $entityRepositoryFactory = $this->repositoryFactory->resolveGet($entityType);
        /** @var AbstractModel[] $entitiesWithoutADefaultRule */
        $entitiesWithoutADefaultRule = $entityRepositoryFactory->find([
            'filter' => [
                \OliveOil\Core\Model\RepositoryInterface::WILDCARD_ID_FIELD
                    => [['nin' => $entityIdsWithADefaultRule]]
            ]
        ]);

        $this->applyDefaultShareRule($entitiesWithoutADefaultRule);
    }

    /**
     * @param AbstractModel|AbstractModel[] $models
     * @return $this
     */
    public function applyDefaultShareRule($models): static {
        if (!is_array($models)) {
            $models = [$models];
        }

        foreach ($models as $model) {
            if ($model->getId()) {
                $this->applyDefaultShareRuleForEntity($model);
            }
        }

        return $this;
    }

    protected function applyDefaultShareRuleForEntity(AbstractModel $model) {
        $entityType = $model->getEntityType();
        $modelId = $model->getId();
        if (!$entityType || !$modelId) {
            throw new \InvalidArgumentException('The entity must have valid entity type and ID.');
        }

        if ($shareEntityRules = $this->getDefaultShareEntityRule($entityType)) {
            $entityShareRules = $this->shareEntityRuleRepository->find([
                'filter' => [
                    'target_entity_type' => [['eq' => $entityType]],
                    'target_entity_id'   => [['eq' => $modelId]]
                ]
            ]);
            if (!count($entityShareRules)) {
                foreach ($shareEntityRules as $shareEntityRule) {
                    $shareEntityRule = array_merge($shareEntityRule, [
                        'target_entity_type' => $entityType,
                        'target_entity_id'   => $modelId,
                    ]);
                    $defaultEntityShareRule = $this->shareEntityRuleRepository->newModel()
                        ->setData($shareEntityRule);
                    $this->shareEntityRuleRepository->save($defaultEntityShareRule);
                }
            }
        }
    }

    /**
     * @param array $shareRulesConfiguration|null
     * @return \Geeftlist\Model\Share\Entity\Rule[]|null
     */
    public function prepareEntityShareRules(
        \Geeftlist\Model\AbstractModel $entity,
        $shareRulesConfiguration
    ): ?array {
        // No new data, return NULL (= "do not change anything")
        if (! is_array($shareRulesConfiguration)) {
            return null;
        }

        $shareRules = [];
        foreach ($shareRulesConfiguration as $shareRuleConfiguration) {
            // Retrive rule's default priority
            if (
                ($shareRuleConfiguration['priority'] ?? null) === null
                && array_key_exists('rule_type', $shareRuleConfiguration)
            ) {
                try {
                    /** @var \Geeftlist\Model\Share\RuleTypeInterface $ruleTypeModel */
                    $ruleTypeModel = $this->shareRuleTypeFactoryFactory->getFromCode($entity->getEntityType())
                        ->resolveGet($shareRuleConfiguration['rule_type']);
                    $shareRuleConfiguration['priority'] = $ruleTypeModel->getDefaultPriority();
                }
                catch (Exception) {
                    $this->logger->warning(sprintf(
                        'Invalid rule type "%s" encountered on entity type "%s"',
                        $shareRuleConfiguration['rule_type'],
                        $entity->getEntityType()
                    ));
                }
            }

            /** @var \Geeftlist\Model\Share\Entity\Rule $shareRule */
            $shareRule = $this->shareEntityRuleRepository->newModel()
                ->addData([
                        'target_entity_type' => $entity->getEntityType(),
                        'target_entity_id' => $entity->getId(),
                        'target_entity' => $entity
                    ] + $shareRuleConfiguration);
            $shareRules[] = $shareRule;
        }

        return $shareRules;
    }

    /**
     * Save/update share rule according to entity's data.
     *
     * @param \Geeftlist\Model\Share\Entity\Rule[] $newShareEntityRules
     * @return mixed[]
     */
    public function saveShareRule(AbstractModel $entity, array $newShareEntityRules): array {
        $existingShareRules = $this->shareEntityRuleRepository->find([
            'filter' => [
                'target_entity_type' => [['eq' => $entity->getEntityType()]],
                'target_entity_id'   => [['eq' => $entity->getId()]]
            ]
        ]);

        $create = [];
        $update = [];
        $delete = [];
        /** @var \Geeftlist\Model\Share\Entity\Rule $existingShareRule */
        foreach ($existingShareRules as $existingShareRule) {
            $found = false;
            foreach ($newShareEntityRules as $i => $newShareRule) {
                if ($this->matchShareRule($newShareRule, $existingShareRule)) {
                    $update[] = $existingShareRule->addData($newShareRule->getData());

                    unset($newShareEntityRules[$i]);
                    $found = true;
                }
            }

            if (!$found) {
                $delete[] = $existingShareRule;
            }
        }

        // Remaining rules are to be created
        $create = $newShareEntityRules;

        $save = array_merge($create, $update);
        $this->transactionHelper->execute(function () use ($save, $delete): void {
            /** @var \Geeftlist\Model\Share\Entity\Rule $rule */
            foreach ($save as $rule) {
                $this->shareEntityRuleRepository->save($rule);
            }

            foreach ($delete as $rule) {
                $this->shareEntityRuleRepository->delete($rule);
            }
        });

        return $save;
    }

    /**
     * Load share rule into entity's data.
     */
    public function loadShareRule(AbstractModel $entity): void {
        /** @var Rule[] $shareRules */
        $shareRules = $this->shareEntityRuleRepository->find([
            'filter' => [
                'target_entity_type' => [['eq' => $entity->getEntityType()]],
                'target_entity_id'   => [['eq' => $entity->getId()]]
            ]
        ]);

        $entityShareRuleData = [];
        foreach ($shareRules as $shareRule) {
            $entityShareRuleData[] = $shareRule->toArray();
        }

        $entity->setData(Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY, $entityShareRuleData);
    }

    protected function matchShareRule(
        \Geeftlist\Model\Share\Entity\Rule $newShareRule,
        \Geeftlist\Model\Share\Entity\Rule $shareRule
    ): bool {
        // If ID match, that's enough
        if ($shareRule->getId() && $newShareRule->getId() == $shareRule->getId()) {
            return true;
        }

        // Else compare significant fields one by one
        $compareFields = [
            'rule_type',
            'params',
        ];
        foreach ($compareFields as $compareField) {
            if ($newShareRule->getDataUsingMethod($compareField) != $shareRule->getDataUsingMethod($compareField)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array|null
     */
    public function getDefaultShareEntityRule($entityType) {
        return $this->defaultShareEntityRule[$entityType] ?? null;
    }
}
