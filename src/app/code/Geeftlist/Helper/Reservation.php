<?php

namespace Geeftlist\Helper;

class Reservation
{
    public function __construct(
        protected \OliveOil\Core\Model\RepositoryInterface $reservationRepository,
        protected \Geeftlist\Service\Security $securityService
    ) {
    }

    /**
     * @return float|null
     */
    public function getRemainingAmount(\Geeftlist\Model\Gift $gift) {
        if (!$gift->getEstimatedPrice()) {
            return null;
        }

        if (($remainingAmount = $gift->getReservationsRemainingAmount()) === null) {
            $remainingAmount = $this->securityService->callPrivileged(function () use ($gift): int|float {
                $reservations = $this->reservationRepository->find([
                    'filter' => [
                        'main_table.gift_id' => [['eq' => $gift->getId()]]
                    ]
                ]);

                return array_reduce($reservations, static fn($carry, $r): int|float
                    => max(0, $carry - $r->getAmount()), $gift->getEstimatedPrice());
            });

            $gift->setReservationsRemainingAmount($remainingAmount);
        }

        return $remainingAmount;
    }
}
