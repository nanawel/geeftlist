<?php

namespace Geeftlist\Helper\Family;


use Geeftlist\Exception\Family\AlreadyMemberException;
use Geeftlist\Exception\Family\MembershipException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Model\TransportObject;

class Membership
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        protected \Geeftlist\Service\Security $securityService
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    /**
     * @param int|Family $family
     * @param int|Geeftee|Geeftee[] $geeftees
     * @return Family\MembershipRequest|Family\MembershipRequest[]
     */
    public function request($family, $geeftees, Geefter $sponsor = null) {
        $family = $this->getFamily($family);
        $geeftees = $this->getGeeftees($geeftees);

        $transportObject = new TransportObject();
        $this->eventManager->trigger('request::before', $this, [
            'family' => $family,
            'geeftees' => $geeftees,
            'sponsor' => $sponsor,
            'transport_object' => $transportObject
        ]);
        extract($transportObject->forExtract());

        $isInvitation = $sponsor && ! isset($geeftees[$sponsor->getGeeftee()->getId()]);

        if ($geeftees === []) {
            throw new \InvalidArgumentException('Missing geeftees.');
        }

        $return = [];
        foreach ($geeftees as $candidate) {
            if ($this->hasPendingRequest($family, $candidate)) {
                throw new MembershipException("There is already a pending request for this geeftee and that family.");
            }

            if ($family->isMember($candidate)) {
                throw new AlreadyMemberException(
                    sprintf('Geeftee %d is already a member of family %d', $candidate->getId(), $family->getId())
                );
            }

            $membershipRequest = $family->newMembershipRequest($candidate);
            if ($isInvitation) {
                $membershipRequest->setSponsorId($sponsor->getId());
            }

            switch ($family->getVisibility()) {
                case Family\Field\Visibility::PUBLIC:
                    if ($isInvitation && $candidate->getGeefterId() !== null) {
                        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_PENDING);
                    } else {
                        $this->securityService->callPrivileged(static function () use ($family, $candidate): void {
                            $family->addGeeftee($candidate);
                        });
                        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_ACCEPTED);
                    }

                    break;

                case Family\Field\Visibility::PROTECTED:
                    if ($isInvitation && $candidate->getGeefterId() === null) {
                        $this->securityService->callPrivileged(static function () use ($family, $candidate): void {
                            $family->addGeeftee($candidate);
                        });
                        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_ACCEPTED);
                    } else {
                        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_PENDING);
                    }

                    break;

                case Family\Field\Visibility::PRIVATE:
                    if ($isInvitation) {
                        if ($candidate->getGeefterId() === null) {
                            $this->securityService->callPrivileged(static function () use ($family, $candidate): void {
                                $family->addGeeftee($candidate);
                            });
                            $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_ACCEPTED);
                        } else {
                            $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_PENDING);
                        }
                    } else {
                        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_REJECTED);
                    }

                    break;
            }

            $membershipRequest->save();

            $return[] = $membershipRequest;
        }

        $this->eventManager->trigger('request::after', $this, ['membership_requests' => $return]);
        if (count($geeftees) == 1) {
            return current($return);
        }

        return $return;
    }

    /**
     * @return $this
     */
    public function acceptRequest(Family\MembershipRequest $membershipRequest): static {
        $this->eventManager->trigger('acceptRequest::before', $this, ['membership_request' => $membershipRequest]);
        $family = $this->getFamily($membershipRequest->getFamilyId());
        $geeftee = current($this->getGeeftees($membershipRequest->getGeefteeId()));

        $this->securityService->callPrivileged(static function () use ($family, $geeftee): void {
            $family->addGeeftee($geeftee);
        });
        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_ACCEPTED)
            ->save();
        $this->eventManager->trigger('acceptRequest::after', $this, ['membership_request' => $membershipRequest]);

        return $this;
    }

    /**
     * @return $this
     */
    public function rejectRequest(Family\MembershipRequest $membershipRequest): static {
        $this->eventManager->trigger('rejectRequest::before', $this, ['membership_request' => $membershipRequest]);

        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_REJECTED)
            ->save();

        $this->eventManager->trigger('rejectRequest::after', $this, ['membership_request' => $membershipRequest]);

        return $this;
    }

    /**
     * @return $this
     */
    public function cancelRequest(Family\MembershipRequest $membershipRequest): static {
        $this->eventManager->trigger('cancelRequest::before', $this, ['membership_request' => $membershipRequest]);

        $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_CANCELED)
            ->save();

        $this->eventManager->trigger('cancelRequest::after', $this, ['membership_request' => $membershipRequest]);

        return $this;
    }

    protected function hasPendingRequest(Family $family, Geeftee $geeftee): bool {
        $collection = $this->resourceModelFactory->resolveMakeCollection(Family\MembershipRequest::ENTITY_TYPE)
            ->addFieldToFilter('family_id', $family->getId())
            ->addFieldToFilter('geeftee_id', $geeftee->getId())
            ->addFieldToFilter('decision_code', Family\MembershipRequest::DECISION_CODE_PENDING);
        return $collection->count() > 0;
    }

    /**
     * @param int|Family $family
     * @return Family
     * @throws NoSuchEntityException
     */
    protected function getFamily($family) {
        if ($family instanceof Family) {
            return $family;
        }

        if (is_numeric($family)) {
            /** @var Family $familyModel */
            $familyModel = $this->modelFactory->resolveMake(Family::ENTITY_TYPE);
            $familyModel->load((int) $family);
            if ($familyModel->getId()) {
                return $familyModel;
            }
        }

        throw new NoSuchEntityException('Invalid family');
    }

    /**
     * TODO Might be improved/lighten using Collection instead
     *
     * @param int|Geeftee|Geeftee[] $geeftees
     * @return Geeftee[]
     */
    protected function getGeeftees($geeftees): array {
        if (! is_array($geeftees)) {
            $geeftees = [$geeftees];
        }

        $return = [];
        foreach ($geeftees as $geeftee) {
            if ($geeftee instanceof Geeftee) {
                $return[$geeftee->getId()] = $geeftee;
            } elseif (is_numeric($geeftee)) {
                $geefteeModel = $this->modelFactory->resolveMake(Geeftee::ENTITY_TYPE)
                    ->load((int) $geeftee);
                if ($geefteeId = $geefteeModel->getId()) {
                    $return[$geefteeId] = $geefteeModel;
                }
            } else {
                throw new NoSuchEntityException('Invalid or unknown geeftee!');
            }
        }

        return $return;
    }
}
