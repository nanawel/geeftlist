<?php

namespace Geeftlist\Helper;

use Geeftlist\Model\Geeftee as GeefteeModel;
use Geeftlist\Model\Geefter as GeefterModel;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee as FamilyGeefteeResource;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Model\ArrayObject;
use OliveOil\Core\Model\I18n;

class Geeftee
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \OliveOil\Core\Service\Event $eventService,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \Geeftlist\Service\Security $securityService,
        protected \OliveOil\Core\Model\I18n $i18n
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    public function getBirthDate(\Geeftlist\Model\Geeftee $geeftee, I18n $i18n = null) {
        if (!$i18n instanceof \OliveOil\Core\Model\I18n) {
            $i18n = $this->i18n;
        }

        if (!$dob = $geeftee->getDob()) {
            return null;
        }

        if (!$dob = strtotime($dob)) {
            return null;
        }

        return $i18n->getMonthDayFormatterInstance()->format($dob);
    }

    /**
     * Create or update geeftees for the given geefters.
     */
    public function syncFromGeefters(array $geefters): static {
        /** @var GeefteeModel $geeftee */
        $geeftee = $this->modelFactory->resolveMake(GeefteeModel::ENTITY_TYPE);
        foreach ($geefters as $geefter) {
            if (! $geefter->getPasswordHash()) {
                // Do NOT sync geefters with empty password (= not verified email)
                continue;
            }

            $geefter = $this->getGeefter($geefter);

            // First try to load geeftee with geefter_id (safe)
            $geeftee->load($geefter->getId(), 'geefter_id');
            if ($geeftee->getId()) {
                $this->updateGeefteeFromGeefter($geefter, $geeftee);
            }
            else {
                // Loading from ID failed, so try with email
                $geeftee->load($geefter->getEmail(), 'email');
                if ($geeftee->getId()) {
                    $this->updateGeefteeFromGeefter($geefter, $geeftee);
                }
                else {
                    // Geeftee does not exist, create it with geefter's data
                    $geeftee = $this->createGeefteeFromGeefter($geefter);
                    $geefter->setGeefteeId($geeftee->getId());
                }
            }

            // Be careful to clear the geeftee instance for the next loop/geefter
            $geeftee->clearInstance();
        }

        return $this;
    }

    /**
     * Merge $sourceGeeftee into $geeftee then delete $sourceGeeftee.
     *
     * @param bool $persist
     * @return $this
     * @throws \OliveOil\Core\Exception\DatabaseException
     * @throws \Throwable
     */
    public function merge(GeefteeModel $geeftee, GeefteeModel $sourceGeeftee, $persist = true): static {
        $geeftee->beforeMerge($sourceGeeftee);

        // Need to disable security checks here
        $this->securityService->callPrivileged(function () use ($geeftee, $sourceGeeftee, $persist): void {
            $this->doMerge($geeftee, $sourceGeeftee, $persist);
        });

        $geeftee->afterMerge($sourceGeeftee);

        return $this;
    }

    /**
     *
     * @param GeefteeModel $geeftee
     * @param GeefteeModel $sourceGeeftee
     * @param bool $persist
     * @return $this
     */
    protected function doMerge($geeftee, $sourceGeeftee, $persist = true): static {
        $transaction = $this->transactionHelper->newTransaction();
        try {
            $transaction->begin();

            $geeftee->triggerEvent('merge::do', [
                'target_geeftee' => $geeftee,
                'source_geeftee' => $sourceGeeftee
            ]);

            if (
                $sourceGeeftee->getCreatedAt()
                && $sourceGeeftee->getCreatedAt() < $geeftee->getCreatedAt()
            ) {
                $geeftee->setCreatedAt($sourceGeeftee->getCreatedAt());
            }

            if ($persist) {
                $sourceGeeftee->delete();
                $geeftee->save();
            }

            $transaction->commit();
        }
        catch (\Throwable $throwable) {
            $transaction->rollback();

            throw $throwable;
        }

        $geeftee->triggerEvent('merge::after_commit', [
            'target_geeftee' => $geeftee,
            'source_geeftee' => $sourceGeeftee
        ]);

        return $this;
    }

    /**
     * Return TRUE if the two geeftees have at least one family in common.
     *
     * FIXME: assert*() methods should throw an exception instead of returning a boolean.
     */
    public function assertIsGeefteeRelated(\Geeftlist\Model\Geeftee $geefteeA, \Geeftlist\Model\Geeftee $geefteeB): bool {
        return $this->assertIsGeefteeIdRelated($geefteeA->getId(), $geefteeB->getId());
    }

    /**
     * Return TRUE if the two geeftees have at least one family in common.
     *
     * FIXME: assert*() methods should throw an exception instead of returning a boolean.
     *
     * @param int $geefteeIdA
     * @param int $geefteeIdB
     */
    public function assertIsGeefteeIdRelated($geefteeIdA, $geefteeIdB): bool {
        if ($geefteeIdA == $geefteeIdB) {
            return true;
        }

        /** @phpstan-ignore-next-line */
        $familyCollection = $this->resourceModelFactory
            ->resolveMakeCollection(\Geeftlist\Model\Family::ENTITY_TYPE)
            ->joinTable(
                ['family_geeftee_1' => FamilyGeefteeResource::MAIN_TABLE],
                'main_table.family_id = family_geeftee_1.family_id'
            )
            ->joinTable(
                ['family_geeftee_2' => FamilyGeefteeResource::MAIN_TABLE],
                'main_table.family_id = family_geeftee_2.family_id'
            );
        $familyCollection->addFieldToFilter('family_geeftee_1.geeftee_id', $geefteeIdA)
            ->addFieldToFilter('family_geeftee_2.geeftee_id', $geefteeIdB);

        return (bool) $familyCollection->count();
    }

    /**
     * @param GeefterModel|int|string $geefter
     * @param GeefteeModel $geeftee
     * @return GeefteeModel
     * @throws \Exception
     */
    protected function updateGeefteeFromGeefter($geefter, $geeftee): \OliveOil\Core\Model\AbstractModel {
        $geefter = $this->getGeefter($geefter);
        $geeftee = $this->getGeeftee($geeftee);
        $dataMapping = new ArrayObject([
            // geeftee field => geefter field
            'geefter_id'  => 'geefter_id',
            'email'       => 'email',
            'name'        => 'username',
            'dob'         => 'dob',
            'avatar_path' => 'avatar_path',
            'creator_id'  => null,       //Set geeftee creator_id to NULL for those having corresponding geefters
        ]);
        $geefter->getEventManager()->trigger(
            'updateGeeftee::mapping',
            $geefter,
            ['geeftee' => $geeftee, 'mapping' => $dataMapping]
        );

        $this->copyGeefterDataToGeeftee($geefter, $geeftee, $dataMapping);

        return $geeftee->save();
    }

    /**
     * @param GeefterModel|int|string $geefter
     * @return GeefteeModel
     */
    protected function createGeefteeFromGeefter($geefter): \OliveOil\Core\Model\AbstractModel {
        $geefter = $this->getGeefter($geefter);
        /** @var GeefteeModel $geeftee */
        $geeftee = $this->modelFactory->resolveMake(GeefteeModel::ENTITY_TYPE);
        $dataMapping = [
            // geeftee field => geefter field
            'geefter_id' => 'geefter_id',
            'email'      => 'email',
            'name'       => 'username',
            'dob'        => 'dob',
        ];

        $this->copyGeefterDataToGeeftee($geefter, $geeftee, $dataMapping);

        return $geeftee->save();
    }

    protected function copyGeefterDataToGeeftee($geefter, $geeftee, iterable $dataMapping): static {
        $geefter = $this->getGeefter($geefter);
        $geeftee = $this->getGeeftee($geeftee);

        foreach ($dataMapping as $geefteeField => $geefterField) {
            if ($geefter->hasData($geefterField)) {
                $geeftee->setData($geefteeField, $geefterField === null ? null : $geefter->getData($geefterField));
            }
        }

        $geefter->getEventManager()->trigger(
            'updateGeeftee::copyData',
            $geefter,
            ['geeftee' => $geeftee, 'mapping' => $dataMapping]
        );

        return $this;
    }

    /**
     * @param GeefterModel|int|string $geefter
     * @return GeefterModel
     * @throws \Exception
     */
    protected function getGeefter($geefter) {
        if ($geefter instanceof GeefterModel) { // Model
            return $geefter;
        }

        /** @var \Geeftlist\Model\Geefter $return */
        $return = $this->modelFactory->resolveMake(GeefterModel::ENTITY_TYPE);
        if (is_numeric($geefter)) { // ID
            $return->load($geefter);
        }
        elseif (is_string($geefter)) { // Email
            $return->load($geefter, 'email');
        }

        if (! $return->getId()) {
            throw new NoSuchEntityException('Invalid geefter identifier');
        }

        return $return;
    }

    /**
     * @param GeefteeModel|int|string $geeftee
     * @return GeefteeModel
     * @throws \Exception
     */
    protected function getGeeftee($geeftee) {
        if ($geeftee instanceof GeefteeModel) { // Model
            return $geeftee;
        }

        /** @var GeefteeModel $return */
        $return = $this->modelFactory->resolveMake(GeefteeModel::ENTITY_TYPE);
        if (is_numeric($geeftee)) { // ID
            $return->load($geeftee);
        }
        elseif (is_string($geeftee)) { // Email
            $return->load($geeftee, 'email');
        }

        if (! $return->getId()) {
            throw new NoSuchEntityException('Invalid geeftee identifier');
        }

        return $return;
    }
}
