<?php

namespace Geeftlist\Helper;

use Geeftlist\Exception\GiftException;
use Geeftlist\Model\Gift as GiftModel;
use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use Geeftlist\Model\Share\Gift\RuleType\OpenGiftType;
use Geeftlist\Model\Share\Gift\RuleType\PrivateType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;

class Gift
{
    public function __construct(protected \Geeftlist\Model\ModelFactory $modelFactory, protected \OliveOil\Core\Model\RepositoryInterface $giftRepository, protected \OliveOil\Core\Model\RepositoryInterface $shareEntityRuleRepository, protected \Geeftlist\Service\PermissionInterface $permissionService, protected \Geeftlist\Service\Security $securityService, protected \Geeftlist\Model\Session\GeefterInterface $geefterSession)
    {
    }

    /**
     * @return bool
     */
    public function isOneselfGift(GiftModel $gift) {
        return $this->securityService->callPrivileged(fn(): bool => $gift->getCreator()
            && $gift->getCreator()->getGeeftee()
            && $this->isFor($gift, $gift->getCreator()->getGeeftee()));
    }

    /**
     * @param \Geeftlist\Model\Geeftee|\Geeftlist\Model\Geefter $geeftee
     * @return bool
     */
    public function isFor(GiftModel $gift, $geeftee) {
        $geeftee = $geeftee instanceof \Geeftlist\Model\Geefter
            ? $geeftee->getGeeftee()
            : $geeftee;

        return $this->securityService->callPrivileged(static fn(): bool => in_array($geeftee->getId(), $gift->getGeefteeIds()));
    }

    public function isPrivateGift(GiftModel $gift): bool {
        return $this->hasShareRule($gift, PrivateType::CODE);
    }

    public function isOpenGift(GiftModel $gift): bool {
        return $this->hasShareRule($gift, OpenGiftType::CODE);
    }

    /**
     * @param string $ruleCode
     */
    public function hasShareRule(GiftModel $gift, $ruleCode): bool {
        // Use model's cache config if present
        if (is_array($shareRuleConfiguration = $gift->getDataUsingMethod(Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY))) {
            foreach ($shareRuleConfiguration as $shareRuleData) {
                if (
                    is_array($shareRuleData)
                    && isset($shareRuleData['rule_type'])
                    && $shareRuleData['rule_type'] === $ruleCode
                ) {
                    return true;
                }
            }
        }

        if ($gift->getId()) {
            // Fallback: query DB
            $shareEntityRules = $this->shareEntityRuleRepository->find([
                'filter' => [
                    'target_entity_type' => [['eq' => $gift->getEntityType()]],
                    'target_entity_id' => [['eq' => $gift->getId()]],
                    'rule_type' => [['eq' => $ruleCode]]
                ]
            ]);

            return count($shareEntityRules) > 0;
        }

        return false;
    }

    public function canDuplicate(GiftModel $gift): bool {
        return (bool) $gift->getId();
    }

    public function canRequestArchiving(GiftModel $gift): bool {
        return (bool) $gift->getCreatorId()
            && $this->geefterSession->getGeefterId() != $gift->getCreatorId();
    }

    /**
     * @param bool $ignoreCurrentStatus
     */
    public function canArchive(\Geeftlist\Model\Gift $gift, $ignoreCurrentStatus = false): bool {
        return ($ignoreCurrentStatus || $gift->getStatus() != Status::ARCHIVED)
            && $this->permissionService->isAllowed([$gift], [TypeInterface::ACTION_ARCHIVE]);
    }

    /**
     * @param \Geeftlist\Model\Gift|int $gift
     */
    public function archive($gift): void {
        $giftId = is_object($gift) ? $gift->getId() : $gift;

        // Reload the model to make sure we only alter the "status" field before saving
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($giftId, null, ['use_cache' => false]);
        if (!$gift || !$this->canArchive($gift)) {
            throw new GiftException('Cannot archive this gift.');
        }

        $this->securityService->callPrivileged(function () use ($gift): void {
            $gift->setStatus(Status::ARCHIVED);
            $this->giftRepository->save($gift);
        });
    }

    /**
     * @param \Geeftlist\Model\Gift|int $gift
     */
    public function delete($gift): void {
        $giftId = is_object($gift) ? $gift->getId() : $gift;

        // Reload the model to make sure we only alter the "status" field before saving
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($giftId, null, ['use_cache' => false]);
        if (!$gift || !$this->canDelete($gift)) {
            throw new GiftException('Cannot delete this gift.');
        }

        $this->securityService->callPrivileged(function () use ($gift): void {
            $gift->markAsDeleted();
            $this->giftRepository->save($gift);
        });
    }

    /**
     * @return bool
     */
    public function canEdit(\Geeftlist\Model\Gift $gift) {
        return $this->permissionService->isAllowed([$gift], [TypeInterface::ACTION_EDIT]);
    }

    public function canDelete(\Geeftlist\Model\Gift $gift): bool {
        return $gift->getStatus() != Status::DELETED
            && $this->permissionService->isAllowed([$gift], [TypeInterface::ACTION_DELETE]);
    }

    /**
     * @return bool
     */
    public function canViewReservations(\Geeftlist\Model\Gift $gift) {
        return $this->permissionService->isAllowed([$gift], [TypeInterface::ACTION_VIEW_RESERVATIONS]);
    }

    /**
     * @return bool
     */
    public function canReservePurchase(\Geeftlist\Model\Gift $gift) {
        return $this->permissionService->isAllowed([$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]);
    }

    /**
     * @return bool
     */
    public function canCancelReservation(\Geeftlist\Model\Gift $gift) {
        return $this->permissionService->isAllowed([$gift], [TypeInterface::ACTION_CANCEL_RESERVATION]);
    }

    public function canReport(\Geeftlist\Model\Gift $gift): bool {
        return $this->geefterSession->getGeefterId() != $gift->getCreatorId();
    }
}
