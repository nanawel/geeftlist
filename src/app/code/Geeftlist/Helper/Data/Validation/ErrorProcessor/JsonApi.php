<?php

namespace Geeftlist\Helper\Data\Validation\ErrorProcessor;


use Narrowspark\HttpStatus\HttpStatus;
use Neomerx\JsonApi\Schema\Error;
use OliveOil\Core\Helper\Data\Validation\ErrorProcessor;
use OliveOil\Core\Model\Validation\ErrorInterface;

class JsonApi extends ErrorProcessor
{
    public function __construct(
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory
    ) {
        parent::__construct($i18n, $logService);
    }

    protected function processError(ErrorInterface $fieldError, array $fieldLabelMapping, array &$data, &$isError) {
        /** @var Error $error */
        $error = $this->coreFactory->make(Error::class);
        $error->setStatus(HttpStatus::STATUS_BAD_REQUEST)
            ->setCode($fieldError->getType())
            ->setTitle(parent::processError($fieldError, $fieldLabelMapping, $data, $isError))
            ->setMeta(['field' => $fieldError->getField()]);

        return $error;
    }
}
