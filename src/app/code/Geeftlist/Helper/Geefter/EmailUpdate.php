<?php

namespace Geeftlist\Helper\Geefter;


use Geeftlist\Exception\Geefter\EmailUpdateException;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;

class EmailUpdate
{
    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \Geeftlist\Service\Security $securityService,
        \OliveOil\Core\Service\Event $eventService
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    /**
     * @param Geefter $geefter
     * @param string $email
     * @return bool
     */
    public function requestEmailUpdate(Geefter $geefter, string $email): bool {
        if (
            $email
            && trim($email)
            && $email !== $geefter->getEmail()
            && $email !== $geefter->getPendingEmail()
        ) {
            if (! $this->isEmailUpdatable($geefter)) {
                throw new EmailUpdateException('Cannot update email.');
            }

            $geefter->setPendingEmail($email)
                ->generateUpdateEmailToken();

            return true;
        }

        return false;
    }

    public function cancelEmailUpdate(Geefter $geefter): bool {
        if ($geefter->getPendingEmail()) {
            $geefter->setPendingEmail(null)
                ->setPendingEmailToken(null);

            return true;
        }

        return false;
    }

    public function isEmailUpdatable(Geefter $geefter): bool {
        // Only allow email updates on geefters with a password defined
        return (bool) $geefter->getPasswordHash();
    }

    /**
     * @param string $token
     * @return Geefter
     * @throws EmailUpdateException
     */
    public function getGeefterFromToken(string $token): Geefter {
        /** @var Geefter $geefter */
        $geefter = $this->geefterRepository->get($token, 'pending_email_token');
        if (! $geefter) {
            $this->eventManager->trigger('geefter_update_email_token_failure', $this, [
                'token' => $token
            ]);
            throw new EmailUpdateException('Invalid or expired token.');
        }

        return $geefter;
    }

    /**
     * @param string $token
     * @return Geefter $geefter
     */
    public function applyEmailUpdate(string $token): Geefter {
        $geefter = $this->getGeefterFromToken($token);

        // Strange situations, but handle them anyway
        if (
            ! $geefter->getPendingEmail()
            || $geefter->getEmail() == $geefter->getPendingEmail()
        ) {
            $geefter->setPendingEmail(null)
                ->setPendingEmailToken(null);

            return $geefter;
        }

        $email = $geefter->getEmail();
        $pendingEmail = $geefter->getPendingEmail();

        $this->transactionHelper->execute(function () use ($geefter, $token, $email, $pendingEmail): void {
            $this->eventManager->trigger('apply::before', $this, [
                'geefter'       => $geefter,
                'token'         => $token,
                'current_email' => $email,
                'pending_email' => $pendingEmail
            ]);

            $this->doApplyEmailUpdate($geefter);

            $this->eventManager->trigger('apply::after', $this, [
                'geefter'   => $geefter,
                'token'     => $token,
                'old_email' => $email,
                'new_email' => $pendingEmail
            ]);
            $this->eventManager->trigger('geefter_update_email_success', $this, [
                'geefter'   => $geefter,
                'old_email' => $email,
                'new_email' => $pendingEmail
            ]);
        });

        return $geefter;
    }

    /**
     * @param Geefter $geefter
     * @return void
     */
    protected function doApplyEmailUpdate(Geefter $geefter): void {
        /** @var Geeftee $geefteeHavingPendingEmail */
        $geefteeHavingPendingEmail = $this->geefteeRepository->get($geefter->getPendingEmail(), 'email');

        $this->eventManager->trigger('updateEmail::before', $geefter, ['geeftee' => $geefteeHavingPendingEmail]);

        // A geeftee with the target email already exists
        if ($geefteeHavingPendingEmail) {
            // A *geefter* already exists with this email (it might have been created after the request was created)
            if ($geefteeHavingPendingEmail->getGeefterId() !== null) {
                throw new EmailUpdateException('This email is not available.');
            }

            $geeftee = $geefter->getGeeftee();

            // Need to disable security checks here
            $this->securityService->callPrivileged(function () use ($geeftee, $geefteeHavingPendingEmail): void {
                // $geefteeHavingPendingEmail is geefterless: merge first
                $this->geefteeHelper->merge($geeftee, $geefteeHavingPendingEmail);

                // Then delete it once merged
                $geefteeHavingPendingEmail->delete();
            });
        }

        // Need to disable security checks here too
        // See \Geeftlist\Test\Controller\AccountTest::testConfirmEmailUpdatePost_valid_loggedInAsAnotherGeefter()
        $this->securityService->callPrivileged(function () use ($geefter): void {
            $geefter->setEmail($geefter->getPendingEmail())
                ->setPendingEmail(null)
                ->setPendingEmailToken(null)
                ->save();
        });

        $this->eventManager->trigger('updateEmail::after', $geefter);
    }
}
