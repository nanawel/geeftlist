<?php

namespace Geeftlist\Helper\Discussion;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Share\Discussion\Post\RuleType\TypeInterface;
use OliveOil\Core\Exception\NoSuchEntityException;

class Post
{
    public function __construct(protected \Geeftlist\Model\ModelFactory $modelFactory, protected \OliveOil\Core\Model\RepositoryInterface $postRepository, protected \Geeftlist\Service\PermissionInterface $permissionService, protected \Geeftlist\Service\Security $securityService)
    {
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post|int $post
     */
    public function pin($post, $pin = true): void {
        $postId = is_object($post) ? $post->getId() : $post;

        // Reload the model to make sure we only alter the "is_pinned" field before saving
        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $this->postRepository->get($postId, null, ['use_cache' => false]);
        if (!$post) {
            throw new NoSuchEntityException('Invalid or illegal post ID.');
        }

        if (!$this->canPin($post)) {
            throw new PermissionException(sprintf(
                'The current geefter cannot change the "pinned" status of post #%d.',
                $post->getId()
            ));
        }

        $this->securityService->callPrivileged(function () use ($post, $pin): void {
            $post->setIsPinned((bool)$pin);
            $this->postRepository->save($post);
        });
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post|int $post
     */
    public function unpin($post): void {
        $this->pin($post, false);
    }

    /**
     * @return bool
     */
    public function canPin(\Geeftlist\Model\Discussion\Post $post) {
        return $this->permissionService->isAllowed([$post], [TypeInterface::ACTION_PIN]);
    }
}
