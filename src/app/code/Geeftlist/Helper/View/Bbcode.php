<?php

namespace Geeftlist\Helper\View;


class Bbcode
{
    public function __construct(protected \Decoda\Decoda $decoda)
    {
    }

    public function parseText($text) {
        return $this->decoda->reset((string) $text, false, $this->getTextCacheKey($text))
            ->parse();
    }

    public function stripText($text) {
        return $this->decoda->reset((string) $text, false, $this->getTextCacheKey($text))
            ->strip();
    }

    protected function getTextCacheKey($text) {
        return null;    // Disabled, not helping here
    }
}
