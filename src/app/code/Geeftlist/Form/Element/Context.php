<?php

namespace Geeftlist\Form\Element;


class Context extends \OliveOil\Core\Form\Element\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\View $view,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\DesignInterface $design,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\GenericFactory $modelFactory,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory
    ) {
        parent::__construct(
            $fw,
            $view,
            $i18n,
            $design,
            $logService,
            $modelFactory
        );
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }
}
