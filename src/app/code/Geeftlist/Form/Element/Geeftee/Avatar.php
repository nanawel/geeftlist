<?php

namespace Geeftlist\Form\Element\Geeftee;

use Geeftlist\Model\Geeftee;
use function OliveOil\array_mask;

/**
 * @method int|null getMaxFilesize()
 * @method int|null getMaxWidth()
 * @method int|null getMaxHeight()
 */
class Avatar extends \OliveOil\Core\Form\Element\File\Image
{
    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        protected \Geeftlist\Service\Avatar\Geeftee $geefteeAvatarService,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getImageUrl($avatarPath) {
        if ($avatarPath) {
            return $this->geefteeAvatarService->getUrlByAvatarPath($avatarPath);
        }

        return $this->geefteeAvatarService->getUrlByGeeftee($this->getGeeftee());
    }

    /**
     * @return Geeftee
     * @throws \RuntimeException
     */
    public function getGeeftee()
    {
        if (!$this->hasData('geeftee')) {
            throw new \RuntimeException('geeftee must be set on form element.');
        }

        return $this->getData('geeftee');
    }

    public function getJsConfig(): array {
        return array_mask($this->getData(), [
            'allowed_extensions',
            'max_filesize',
            'max_width',
            'max_height',
            'url',
            'post_data',
            'placeholder_url',
        ]);
    }
}
