<?php

namespace Geeftlist\Form\Element;


use Geeftlist\Block\Traits\BadgeTrait;
use Geeftlist\Model\Badge;
use OliveOil\Core\Form\Element\TemplateElement;

/**
 * @method string getEntityType()
 * @method $this setEntityType(string $entityType)
 * @method int getEntityId()
 * @method $this setEntityId(int $entityId)
 */
class BadgeSelect extends TemplateElement
{
    use BadgeTrait;

    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        protected \Geeftlist\Service\BadgeInterface $badgeService,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setTemplate('badge_select.phtml');
    }

    protected function renderHtml() {
        if ($this->getEntityType()) {
            return parent::renderHtml();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getConfigJson() {
        $badges = $this->getBadges();
        $selectedBadgeIds = $this->getSelectedBadgeIds();

        return json_encode([
            'badges'           => $this->badgeToArray($badges) ?? [],
            'selectedBadgeIds' => $selectedBadgeIds ?: []
        ]);
    }

    protected function getBadges($sorted = true) {
        if (!$entityType = $this->getEntityType()) {
            return null;
        }

        $badges = $this->badgeService->getBadgesForEntityType($entityType);
        if ($sorted) {
            usort($badges, static fn($a, $b): int => strcasecmp((string) $a->getLabel(), (string) $b->getLabel()));
        }

        return $badges;
    }

    protected function getSelectedBadgeIds(): ?array {
        if ((!$entityType = $this->getEntityType()) || (!$entityId = $this->getEntityId())) {
            return null;
        }

        return array_keys($this->badgeService->getBadgesForEntityId($entityType, $entityId));
    }

    /**
     * @param Badge|Badge[] $badge
     */
    public function badgeToArray($badge): array {
        if (is_array($badge)) {
            $return = [];
            foreach ($badge as $b) {
                $return[] = $this->badgeToArray($b);
            }

            return $return;
        }

        return [
            'badgeId'     => (int)$badge->getId(),      // Important! Comparisons are done with strict equality in KO
            'label'       => $badge->getLabel(),
            'description' => $badge->getDescription(),
            'iconHtml'    => $this->getIconHtml($badge)
        ];
    }
}
