<?php

namespace Geeftlist\Form\Element\Family;


use OliveOil\Core\Form\Element\RadioGroup;

class VisibilityRadioGroup extends RadioGroup
{
    public function __construct(
        \OliveOil\Core\Form\Element\Context $context,
        protected \OliveOil\Core\Model\Entity\OptionsFieldInterface $fieldModel,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setLabel('Visibility');
    }

    protected function renderHtml() {
        $this->prepareOptions();

        return parent::renderHtml();
    }

    protected function prepareOptions() {
        $options = [];
        foreach ($this->fieldModel->getAllOptions() as $code => $option) {
            $options[$code] = [
                'label'       => $this->i18n->tr(
                    '{0} {1}',
                    $this->i18n->tr($option['label']),
                    $code == \Geeftlist\Model\Family\Field\Visibility::PROTECTED ? $this->i18n->tr('(Recommended)') : ''
                ),
                'value'       => $code,
                'description' => $this->i18n->tr($option['description'])
            ];
        }

        $this->setOptions($options);
    }
}
