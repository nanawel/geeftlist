<?php

namespace Geeftlist\Form\Element;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Geeftee\Collection;
use OliveOil\Core\Form\Element\TemplateElement;
use OliveOil\Core\Model\RepositoryInterface;

/**
 * @method int[]|null getExclude()
 * @method int|null getCurrentGeefteeId()
 * @method string|null getCurrentGeefteeLabel()
 * @method array|null getRequiredGeefterActions()
 * @method bool|null getIsMultiselect()
 */
class GeefteeSelect extends TemplateElement
{
    public const DEFAULT_CONFIG = [
        'is_multiselect' => true
    ];

    /** @var Collection */
    protected $geefteeCollection;

    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        protected \Geeftlist\Model\Geeftee\RestrictionsAppenderInterface $geefteeRestrictionsAppender,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        array $data = []
    ) {
        parent::__construct($context, $data + static::DEFAULT_CONFIG);
    }

    protected function init(): void {
        parent::init();
        $this->setTemplate('geeftee_select.phtml');
    }

    /**
     * @return string
     */
    public function getWhitelistJson() {
        return json_encode($this->toArray($this->getGeefteeCollection()->getItems()));
    }

    public function getHtmlInputValue(): string {
        $value = array_filter((array) $this->getValue());

        return implode(',', array_map('intval', $value));
    }

    /**
     * @return string
     */
    public function getPlaceholder() {
        return $this->getIsMultiselect()
            ? $this->i18n->tr('Geeftees...')
            : $this->i18n->tr('Choose a geeftee');
    }

    /**
     * @return array
     */
    public function getSettings(): float|int|array {
        $settings = $this->hasData('tagify_settings')
            ? $this->getData('tagify_settings')
            : [];

        return $settings + $this->getDefaultSettings();
    }

    public function getDefaultSettings(): array {
        return [
            'mode' => $this->getIsMultiselect() ? null : 'select'
        ];
    }

    public function getInitJs(): string {
        return sprintf(
            'new GeefteeSelect("#%s", %s);',
            $this->getId(),
            json_encode($this->getSettings(), JSON_FORCE_OBJECT)
        );
    }

    /**
     * @param Geeftee|Geeftee[] $geeftee
     */
    public function toArray($geeftee): array {
        if (is_array($geeftee)) {
            $return = [];
            foreach ($geeftee as $b) {
                $return[] = $this->toArray($b);
            }

            return $return;
        }

        return [
            'geefteeId'   => (string)$geeftee->getId(),
            'value'       => (string)$geeftee->getId(),
            'name'        => $this->escapeHtml(
                $geeftee->getId() == $this->getCurrentGeefteeId()
                    ? $this->i18n->tr('(Yourself)')
                    : $geeftee->getName()
            ),
            'email'       => $this->escapeHtml($geeftee->getEmail()),
            'avatarUrl'   => $geeftee->getAvatarUrl()
        ];
    }

    /**
     * @return Collection
     */
    protected function getGeefteeCollection() {
        if (! $this->geefteeCollection) {
            /** @var Collection $collection */
            $collection = $this->resourceModelFactory->resolveMakeCollection(Geeftee::ENTITY_TYPE);
            $this->applyGeefteeFilters($collection);
            $collection->orderBy('main_table.name');

            $this->geefteeCollection = $collection;
        }

        return $this->geefteeCollection;
    }

    protected function applyGeefteeFilters(Collection $collection) {
        if ($exclude = $this->getExclude()) {
            $collection->addFieldToFilter('main_table.geeftee_id', ['nin' => $exclude]);
        }

        if ($requiredActionsByGeefter = $this->getRequiredGeefterActions()) {
            /** @var RepositoryInterface $geefterRepository */
            $geefterRepository = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE);
            foreach ($requiredActionsByGeefter as $geefterId => $requiredActions) {
                /** @var Geefter $geefter */
                $geefter = $geefterRepository->get($geefterId);
                if (!$geefter) {
                    $this->logger->warning('Cannot add restrictions for invalid geefter ID: ' . $geefterId);
                } else {
                    foreach ($requiredActions as $requiredAction) {
                        $this->geefteeRestrictionsAppender->addGeefterAccessRestrictions(
                            $collection,
                            $geefter,
                            $requiredAction
                        );
                    }
                }
            }
        }
    }

    public function filter(array &$formData): void {
        if (($formDatakey = $this->getFormDataKey()) && array_key_exists($formDatakey, $formData)) {
            $allowedValues = $this->getGeefteeCollection()->getAllIds();
            $formData[$formDatakey] = array_filter(
                is_array($formData[$formDatakey])
                    ? $formData[$formDatakey]
                    : explode(',', (string) ($formData[$formDatakey] ?: '')),
                static fn($v): bool => in_array($v, $allowedValues)
            );
        }
    }
}
