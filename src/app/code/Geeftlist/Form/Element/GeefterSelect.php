<?php

namespace Geeftlist\Form\Element;


use Geeftlist\Model\Geefter;
use OliveOil\Core\Form\Element\TemplateElement;

/**
 * @method int[]|null getExclude()
 * @method array|null getFilters()
 * @method int|null getCurrentGeefterId()
 * @method string|null getCurrentGeefterLabel()
 * @method array|null getRequiredGeefterActions()
 * @method bool|null getIsMultiselect()
 */
class GeefterSelect extends TemplateElement
{
    public const DEFAULT_CONFIG = [
        'is_multiselect' => true
    ];

    /** @var array */
    protected $defaultCriteria = [
        'filter' => [],
        'sort_order' => [
            'username' => SORT_ASC
        ]
    ];

    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        array $data = []
    ) {
        parent::__construct($context, $data + static::DEFAULT_CONFIG);
    }

    protected function init(): void {
        parent::init();
        $this->setTemplate('geefter_select.phtml');
    }

    /**
     * @return string
     */
    public function getWhitelistJson() {
        return json_encode($this->toArray($this->getGeefters()));
    }

    public function getHtmlInputValue(): string {
        $value = array_filter((array) $this->getValue());

        return implode(',', array_map('intval', $value));
    }

    /**
     * @return array
     */
    public function getPlaceholder() {
        return $this->getIsMultiselect()
            ? $this->i18n->tr('Geefters...')
            : $this->i18n->tr('Choose a geefter');
    }

    /**
     * @return array
     */
    public function getSettings(): float|int|array {
        $settings = $this->hasData('tagify_settings')
            ? $this->getData('tagify_settings')
            : [];

        return $settings + $this->getDefaultSettings();
    }

    public function getDefaultSettings(): array {
        return [
            'mode' => $this->getIsMultiselect() ? null : 'select'
        ];
    }

    /**
     * @return array
     */
    public function getDefaultCriteria() {
        return $this->defaultCriteria;
    }

    public function getInitJs(array $additionalSettings = []): string {
        $settings = array_merge_recursive($this->getSettings(), $additionalSettings);
        return sprintf(
            'new GeefterSelect("#%s", %s);',
            $this->getId(),
            json_encode($settings, JSON_FORCE_OBJECT)
        );
    }

    /**
     * @param Geefter|Geefter[] $geefter
     */
    public function toArray($geefter): array {
        if (is_array($geefter)) {
            $return = [];
            foreach ($geefter as $b) {
                $return[] = $this->toArray($b);
            }

            return $return;
        }

        return [
            'geefterId'   => (string)$geefter->getId(),
            'value'       => (string)$geefter->getId(),
            'name'        => $this->escapeHtml(
                $geefter->getId() == $this->getCurrentGeefterId()
                    ? $this->i18n->tr('(Yourself)')
                    : $geefter->getUsername()
            ),
            'email'       => $this->escapeHtml($geefter->getEmail()),
            'avatarUrl'   => $geefter->getAvatarUrl()
        ];
    }

    /**
     * @return Geefter[]
     */
    protected function getGeefters() {
        /** @var Geefter[] $geefters */
        $geefters = $this->geefterRepository->find($this->getCriteria());

        return $geefters;
    }

    /**
     * @return array
     */
    protected function getCriteria() {
        $criteria = $this->getDefaultCriteria();
        if ($exclude = $this->getExclude()) {
            $criteria['filter']['geefter_id'][] = ['nin' => $exclude];
        }

        if ($filters = $this->getFilters()) {
            foreach ($filters as $field => $conditions) {
                $criteria['filter'][$field] = array_merge($criteria['filter'][$field] ?? [], $conditions);
            }
        }

        return $criteria;
    }

    public function filter(array &$formData): void {
        if (($formDatakey = $this->getFormDataKey()) && array_key_exists($formDatakey, $formData)) {
            $allowedValues = array_keys($this->getGeefters());
            $formData[$formDatakey] = array_filter(
                is_array($formData[$formDatakey])
                    ? $formData[$formDatakey]
                    : explode(',', (string) ($formData[$formDatakey] ?: '')),
                static fn($v): bool => in_array($v, $allowedValues)
            );
        }
    }
}
