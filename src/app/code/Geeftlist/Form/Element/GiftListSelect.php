<?php

namespace Geeftlist\Form\Element;


use Geeftlist\Model\GiftList;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Form\Element\TemplateElement;

/**
 * @method int[]|null getExclude()
 * @method array|null getRequiredGeefterActions()
 */
class GiftListSelect extends TemplateElement
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $giftListRepository;

    /** @var GiftList[] */
    protected $items;

    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        protected \Geeftlist\Model\GiftList\RestrictionsAppenderInterface $giftListRestrictionsAppender,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftHelper,
        array $data = []
    ) {
        $this->giftListRepository = $repositoryFactory->resolveGet(GiftList::ENTITY_TYPE);
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setTemplate('giftlist_select.phtml');
    }

    public function getValue() {
        if (empty($value = parent::getValue()) && ($gift = $this->getGift())) {
            $value = $this->giftListGiftHelper->getGiftListIds($gift);
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getWhitelistJson() {
        return json_encode($this->toArray($this->getItems()));
    }

    /**
     * @return string
     */
    public function getSettingsJson() {
        return json_encode([
            'enforceWhitelist' => (bool) ($this->getEnforceWhitelist() ?? true)
        ]);
    }

    public function getHtmlInputValue(): string {
        return implode(',', array_map('intval', $this->getValue() ?? []));
    }

    public function getInitJs(): string {
        return <<<"EOJS"
            new GiftListSelect('#{$this->getId()}', {$this->getSettingsJson()});
        EOJS;
    }

    /**
     * @param GiftList|GiftList[] $giftList
     */
    public function toArray($giftList): array {
        if (is_array($giftList)) {
            $return = [];
            foreach ($giftList as $b) {
                $return[] = $this->toArray($b);
            }

            return $return;
        }

        return [
            'giftListId'  => (int)$giftList->getId(),
            'value'       => (int)$giftList->getId(),
            'name'        => $this->escapeHtml($giftList->getName()),
        ];
    }

    /**
     * @return GiftList[]
     */
    protected function getItems() {
        if (! $this->items) {
            $criteria = [
                'filter' => [
                    'status' => [['eq' => GiftList\Field\Status::ACTIVE]],
                ],
                'sort_order' => ['main_table.name' => SORT_ASC]
            ];
            $this->appendAdditionalCriteria($criteria);
            /** @var GiftList[] $items */
            $items = $this->giftListRepository->find($criteria);

            $this->items = $items;
        }

        return $this->items;
    }

    protected function appendAdditionalCriteria(array &$criteria) {
        if ($exclude = $this->getExclude()) {
            $criteria['filter'][] = ['main_table.giftlist_id', ['nin' => $exclude]];
        }

        if ($requiredActionsByGeefter = $this->getRequiredGeefterActions()) {
            throw new NotImplementedException(__METHOD__ . '::requiredGeefterActions');
        }
    }

    public function filter(array &$formData): void {
        if (($formDatakey = $this->getFormDataKey()) && array_key_exists($formDatakey, $formData)) {
            $formData[$formDatakey] = array_filter(
                is_array($formData[$formDatakey])
                    ? $formData[$formDatakey]
                    : explode(',', (string) ($formData[$formDatakey] ?: '')),
                static fn($v): bool => trim((string) $v) !== ''
            );
        }
    }
}
