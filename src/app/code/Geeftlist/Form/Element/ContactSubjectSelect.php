<?php

namespace Geeftlist\Form\Element;


use OliveOil\Core\Form\Element\Select;

class ContactSubjectSelect extends Select
{
    public function __construct(
        Context $context,
        /** @var array<string, string> */
        protected array $predefinedSubjects = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setLabel('Subject');

        $options = [];
        foreach ($this->getPredefinedSubjects() as $code => $label) {
            $options[$code] = ['label' => $this->i18n->tr($label)];
        }

        $this->setOptions($options);
    }

    /**
     * @return array<string, string>
     */
    public function getPredefinedSubjects(): array {
        return $this->predefinedSubjects;
    }
}
