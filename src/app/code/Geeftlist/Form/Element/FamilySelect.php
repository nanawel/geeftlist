<?php

namespace Geeftlist\Form\Element;


use Geeftlist\Model\Family;
use OliveOil\Core\Form\Element\Select;

class FamilySelect extends Select
{
    public function __construct(
        \Geeftlist\Form\Element\Context $context,
        protected \Geeftlist\Model\ResourceFactory $resourceFactory,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setLabel('Family');
    }

    protected function renderHtml() {
        $this->prepareOptions();

        return parent::renderHtml();
    }

    protected function prepareOptions() {
        $families = $this->getResourceFactory()->resolveMakeCollection(Family::ENTITY_TYPE);
        if (! $this->getAllFamilies()) {
            // FIXME Should not access session here
            $currentGeefter = $this->getGeefterSession()->getGeefter();
            if ($currentGeefter) {
                $familyIds = $currentGeefter->getFamilyCollection()->getAllIds();
                $families->addFieldToFilter('main_table.family_id', ['in' => $familyIds]);
            }
        }

        $families->orderBy('main_table.name');

        $options = [];
        foreach ($families as $id => $f) {
            $options[$id] = [
                'label' => $f->getName()
            ];
        }

        if ($this->getWithEmpty()) {
            $options = ['' => ['label' => $this->i18n->tr('(None)')]] + $options;
        }

        $this->setOptions($options);
    }

    public function getResourceFactory(): \Geeftlist\Model\ResourceFactory {
        return $this->resourceFactory;
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }
}
