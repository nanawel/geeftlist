<?php

namespace Geeftlist\Form\Element\Discussion;


use Geeftlist\Form\Element\GeefterSelect;
use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;
use OliveOil\Core\Exception\AppException;
use OliveOil\Core\Model\ResourceModel\AbstractCollection;

class PostRecipientsSelect extends GeefterSelect
{
    /** @var int[] */
    protected $notifiableGeefterIdsCache;

    public function __construct(
        \Geeftlist\Form\Element\TemplateElement\Context $context,
        \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $topicRepository,
        protected \Geeftlist\Service\PermissionInterface $permissionService,
        array $data = []
    ) {
        parent::__construct($context, $geefterRepository, $data);
    }

    public function setTopic(Topic $topic) {
        if ($this->hasData('topic')) {
            throw new AppException(
                'Cannot overwrite ' . \Geeftlist\Form\Element\Discussion\PostRecipientsSelect::class . '::$topic once set.'
            );
        }

        return $this->setData('topic', $topic);
    }

    /**
     * @return Topic|null
     */
    public function getTopic() {
        if (!($topic = parent::getTopic()) && ($topicId = $this->getTopicId())) {
            $this->setTopic($topic = $this->topicRepository->get($topicId));
        }

        return $topic;
    }

    public function getId(): string {
        $id = '';
        if ($topic = $this->getTopic()) {
            $id = sprintf(
                'topic_%d_',
                $topic->getId()
            );
        }

        return $id . ($this->getData('id') ?: 'post_recipients');
    }

    /**
     * @return array
     */
    public function getDefaultCriteria() {
        if (!$topic = $this->getTopic()) {
            throw new AppException('Missing ' . \Geeftlist\Form\Element\Discussion\PostRecipientsSelect::class . '::$topic.');
        }

        if ($this->notifiableGeefterIdsCache === null) {
            $this->notifiableGeefterIdsCache = AbstractCollection::toIdArray(
                $this->permissionService->getAllowed($topic, TypeInterface::ACTION_BE_NOTIFIED)
            );
        }

        $notifiableGeefterIds = $this->notifiableGeefterIdsCache;

        $criteria = $this->defaultCriteria;
        $criteria['filter']['geefter_id'][] = ['in' => $notifiableGeefterIds];

        return $criteria;
    }
}
