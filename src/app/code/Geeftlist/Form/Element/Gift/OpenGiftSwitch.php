<?php

namespace Geeftlist\Form\Element\Gift;


use Geeftlist\Model\Share\Gift\RuleType\OpenGiftType;

class OpenGiftSwitch extends AbstractShareRuleElement
{
    protected function init(): void {
        parent::init();
        $this->setTemplate('gift/open_gift_switch.phtml');
    }

    public function getCheckedHtml($optionCode = null, $negate = false): string {
        return parent::getCheckedHtml(OpenGiftType::CODE, $negate);
    }
}
