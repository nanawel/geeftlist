<?php

namespace Geeftlist\Form\Element\Gift;


use OliveOil\Core\Form\Element\RadioGroup;

class PriorityRadioGroup extends RadioGroup
{
    public function __construct(
        \Geeftlist\Form\Element\Context $context,
        protected \OliveOil\Core\Model\Entity\OptionsFieldInterface $fieldModel,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setLabel('Priority');
    }

    protected function renderHtml() {
        $this->prepareOptions();

        return parent::renderHtml();
    }

    protected function prepareOptions() {
        $options = [];
        foreach ($this->fieldModel->getAllOptions() as $code => $option) {
            $options[$code] = [
                'label'       => $this->i18n->tr($option['label']),
                'value'       => $code,
                'class'       => 'priority-' . $code
            ];
        }

        $this->setOptions($options);
    }
}
