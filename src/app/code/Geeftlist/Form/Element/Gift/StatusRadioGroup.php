<?php

namespace Geeftlist\Form\Element\Gift;


use Geeftlist\Model\Gift\Field\Status;
use OliveOil\Core\Form\Element\RadioGroup;

class StatusRadioGroup extends RadioGroup
{
    public function __construct(
        \Geeftlist\Form\Element\Context $context,
        protected \OliveOil\Core\Model\Entity\OptionsFieldInterface $fieldModel,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setLabel('Status');
    }

    protected function renderHtml() {
        $this->prepareOptions();

        return parent::renderHtml();
    }

    protected function prepareOptions() {
        $options = [];
        foreach ($this->fieldModel->getAllOptions() as $code => $option) {
            if ($code == Status::DELETED) {
                continue;
            }

            $options[$code] = [
                'label'       => $this->i18n->tr($option['label']),
                'value'       => $code,
                'description' => $this->i18n->tr($option['description'])
            ];
        }

        $this->setOptions($options);
    }
}
