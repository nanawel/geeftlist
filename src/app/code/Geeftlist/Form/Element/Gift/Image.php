<?php

namespace Geeftlist\Form\Element\Gift;

use Geeftlist\Service\Gift\Image as GiftImageService;
use function OliveOil\array_mask;

/**
 * @method int|null getMaxFilesize()
 * @method int|null getMaxWidth()
 * @method int|null getMaxHeight()
 */
class Image extends \OliveOil\Core\Form\Element\File\Image
{
    public function __construct(
        \OliveOil\Core\Form\Element\TemplateElement\Context $context,
        protected \Geeftlist\Service\Gift\Image $giftImageService,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @param string|null $imageId
     * @return string|null
     */
    public function getImageUrl($imageId) {
        if ($imageId) {
            if ($this->giftImageService->fileExists($imageId, GiftImageService::IMAGE_TYPE_FULL)) {
                return $this->giftImageService->getUrlByImageId($imageId, GiftImageService::IMAGE_TYPE_FULL);
            }

            if ($this->giftImageService->fileExists($imageId, GiftImageService::IMAGE_TYPE_TMP)) {
                return $this->giftImageService->getUrlByImageId($imageId, GiftImageService::IMAGE_TYPE_TMP);
            }
        }

        return '';
    }

    public function getJsConfig(): array {
        return array_mask($this->getData(), [
            'allowed_extensions',
            'max_filesize',
            'max_width',
            'max_height',
            'url',
            'post_data',
            'placeholder_url',
        ]);
    }
}
