<?php

namespace Geeftlist\Form\Element\Gift;


use Geeftlist\Model\Share\Gift\RuleType\RegularType;
use OliveOil\Core\Form\Element\TemplateElement;

/**
 * @method $this setGiftId(int $giftId)
 * @method int|null getGiftId()
 * @method $this setTemplate(string $template)
 * @method string|null getTemplate()
 */
abstract class AbstractShareRuleElement extends TemplateElement
{
    public const LABEL_PREFIX = 'sharerule_gift||';

    public const OPTION_VALUE_CUSTOM  = 'custom';

    public const DEFAULT_RULE_CODE    = RegularType::CODE;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $familyRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefterRepository;

    public function __construct(
        \Geeftlist\Form\Element\TemplateElement\Context $context,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getCheckedHtml(string $optionCode, bool $negate = false): string {
        return ($this->findRuleInValue($optionCode) xor $negate)
            ? 'checked="checked"'
            : '';
    }

    public function getOptionLabel(string $rawLabel) {
        return $this->i18n->tr(self::LABEL_PREFIX . $rawLabel);
    }

    protected function findRuleInValue(string $ruleType, ?string $field = null): mixed {
        $value = $this->getValue();
        if (is_array($value)) {
            foreach ($value as $ruleData) {
                if ($ruleData['rule_type'] == $ruleType) {
                    return ($field === null)
                        ? $ruleData
                        : $ruleData[$field] ?? null;
                }
            }
        }

        return null;
    }

    public function filter(array &$formData): void {
        $formDataKey = $this->getFormDataKey();

        if (
            !isset($formData[$formDataKey])
            || !is_array($formData[$formDataKey])
            || empty($formData[$formDataKey])
        ) {
            $formData[$formDataKey] = [];
        }
    }
}
