<?php

namespace Geeftlist\Form\Element\Gift;


use Geeftlist\Model\Share\Gift\RuleType\FamilyType;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use OliveOil\Core\Model\RepositoryInterface;
use function OliveOil\array_discard;

class ShareRuleRadioGroup extends AbstractShareRuleElement
{
    public function __construct(
        \Geeftlist\Form\Element\TemplateElement\Context $context,
        \OliveOil\Core\Model\RepositoryInterface $familyRepository,
        \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        array $data = []
    ) {
        $this->familyRepository = $familyRepository;
        $this->geefterRepository = $geefterRepository;
        parent::__construct($context, $geefterSession, $data);
    }

    protected function init(): void {
        parent::init();
        $this->setTemplate('gift/share_rule_radio_group.phtml');
    }

    public function getValue() {
        $value = parent::getValue();
        if (
            ! $value
            || !array_intersect($this->getHandledRuleTypes(), array_column($value, 'rule_type'))
        ) {
            $value = [['rule_type' => static::DEFAULT_RULE_CODE]];
        }

        return $value;
    }

    public function getFamilySelectedHtml($familyId): string {
        $params = $this->findRuleInValue(FamilyType::CODE, 'params') ?? [];

        return (isset($params['family_ids']) && in_array($familyId, $params['family_ids']))
            ? 'selected="selected"'
            : '';
    }

    public function getGeefterSelectedHtml($geefterId): string {
        $params = $this->findRuleInValue(GeefterType::CODE, 'params') ?? [];

        return isset($params['geefter_ids']) && in_array($geefterId, $params['geefter_ids'])
            ? 'selected="selected"'
            : '';
    }

    /**
     * @return array<mixed, array<'label'|'value', mixed>>
     */
    public function getFamilyOptions(): array {
        $families = $this->familyRepository->find([
            'filter' => [
                'geeftees' => [
                    ['in' => [$this->geefterSession->getGeeftee()->getId()]]
                ]
            ],
            'sort_order' => ['name' => SORT_ASC]
        ]);

        $options = [];
        foreach ($families as $family) {
            $options[] = [
                'label' => $this->escapeHtml($family->getName()),
                'value' => $family->getId()
            ];
        }

        return $options;
    }

    /**
     * @return array<mixed, array<'label'|'value', mixed>>
     */
    public function getGeefterOptions(): array {
        $filters = [];
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            $filters[RepositoryInterface::WILDCARD_ID_FIELD][] = ['neq' => $currentGeefter->getId()];
            $filters['related_geefter'][] = ['eq' => $currentGeefter->getId()];
        }

        $geefters = $this->geefterRepository->find([
            'filter' => $filters,
            'sort_order' => ['username' => SORT_ASC]
        ]);

        $options = [];
        foreach ($geefters as $geefter) {
            $options[] = [
                'label' => $this->escapeHtml($geefter->getName()),
                'value' => $geefter->getId()
            ];
        }

        return $options;
    }

    public function filter(array &$formData): void {
        parent::filter($formData);

        $shareRuleFormData = $formData[$this->getFormDataKey()] ?? [];
        if (
            $shareRuleFormData
            && ($selectedRuleType = $shareRuleFormData['selected_rule_type'] ?? false)
        ) {
            // Discard not selected rule types, but only of the types handled by the current class
            $unusedRuleTypes = array_diff(
                $this->getHandledRuleTypes(),
                [$selectedRuleType]
            );
            $shareRuleFormData = array_discard(
                $shareRuleFormData,
                $unusedRuleTypes
            );
            // Now also discard selection data
            unset($shareRuleFormData['selected_rule_type']);

            $formData[$this->getFormDataKey()] = $shareRuleFormData;
        }
    }

    /**
     * @return string[]
     */
    protected function getHandledRuleTypes(): array {
        return [
            \Geeftlist\Model\Share\Gift\RuleType\RegularType::CODE,
            \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
            \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
            \Geeftlist\Model\Share\Gift\RuleType\PrivateType::CODE
        ];
    }
}
