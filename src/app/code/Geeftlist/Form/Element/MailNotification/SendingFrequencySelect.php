<?php

namespace Geeftlist\Form\Element\MailNotification;


use Geeftlist\Model\MailNotification\Constants;
use OliveOil\Core\Form\Element\Select;

class SendingFrequencySelect extends Select
{
    protected function init(): void {
        parent::init();
        $this->setLabel('Sending frequency');
        $this->setOptions([
            Constants::SENDING_FREQ_REALTIME => [
                'label' => $this->i18n->tr("email_notififation_freq||Realtime")
            ],
            Constants::SENDING_FREQ_DAILY => [
                'label' => $this->i18n->tr("email_notififation_freq||Daily")
            ],
            Constants::SENDING_FREQ_WEEKLY => [
                'label' => $this->i18n->tr("email_notififation_freq||Weekly")
            ],
            Constants::SENDING_FREQ_DISABLED => [
                'label' => $this->i18n->tr("email_notififation_freq||Disabled")
            ],
        ]);
    }
}
