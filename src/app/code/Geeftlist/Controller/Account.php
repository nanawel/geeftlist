<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Geefter\EmailUpdateException;
use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Form\Element\BadgeSelect;
use Geeftlist\Form\Element\Geeftee\Avatar;
use Geeftlist\Form\Element\GeefterSelect;
use Geeftlist\Form\Element\MailNotification\SendingFrequencySelect;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Block\Dialog;
use OliveOil\Core\Form\Element\LanguageSelect;
use OliveOil\Core\Form\Element\YesNoSelect;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Account extends AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\Validation\Email $emailValidator,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreModelFactory,
        protected \Geeftlist\Helper\Geefter\EmailUpdate $emailUpdateHelper,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService,
        protected \Geeftlist\Helper\Geefter $geefterHelper,
        protected \Geeftlist\Service\Avatar\GeefteeInterface $geefteeAvatarService,
        protected \Geeftlist\Service\BadgeInterface $badgeService,
        protected \Geeftlist\Service\Authentication\AuthenticationInterface $authenticationService
    ) {
        parent::__construct($context);
    }

    protected function init() {
        parent::init();

        $this->nonRestrictedActions = [
            'confirmEmailUpdate',
            'confirmEmailUpdatePost',
            'confirmEmailUpdateSuccess'
        ];
    }

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb('My Account', '*/*');

        $languageSelect = $this->coreModelFactory
            ->make(LanguageSelect::class, ['data' => [
                'name' => 'language',
                'id'   => 'language'
            ]
        ]);
        if ($lang = $this->geefterSession->getGeefter()->getLanguage()) {
            $languageSelect->setValue($lang);
        }
        else {
            $languageSelect->setValue($this->i18n->getLanguage(false));
        }

        $avatarImage = $this->modelFactory
            ->make(Avatar::class, ['data' => [
                'name'      => 'avatar_path',
                'id'        => 'avatar_path',
                'value'     => $this->geefterSession->getGeeftee()->getAvatarPath(),
                'geeftee'   => $this->geefterSession->getGeeftee(),
                'url'       => $this->view->getUrl('account_avatarUpload/prepare'),
                'post_data' => [
                    $this->csrfService->getTokenName() => $this->csrfService->getTokenValue()
                ]
            ]
        ]);

        $badgeSelect = $this->modelFactory
            ->make(BadgeSelect::class, ['data' => [
                'name'        => 'badge_ids',
                'id'          => 'badge_ids',
                'entity_type' => Geeftee::ENTITY_TYPE,
                'entity_id'   => $this->geefterSession->getGeeftee()->getId()
            ]
        ]);

        $emailSendingFrequencySelect = $this->modelFactory
            ->make(SendingFrequencySelect::class, ['data' => [
                'name' => 'email_notification_freq',
                'id'   => 'email_notification_freq'
            ]
        ]);
        $staleReservationNotificationsYesNoSelect = $this->modelFactory
            ->make(YesNoSelect::class, ['data' => [
                'name' => 'stale_reservation_notification_enabled',
                'id'   => 'stale_reservation_notification_enabled'
            ]
        ]);

        $geefterSelect = $this->modelFactory
            ->make(GeefterSelect::class, ['data' => [
                'name'           => 'recoverer_geefter_id',
                'id'             => 'recoverer_geefter_id',
                'value'          => $this->getRequestParam('recoverer_geefter_id'),
                'exclude'        => [$this->geefterSession->getGeefter()->getId()],
                'with_empty'     => true,
                'is_multiselect' => false
            ]
        ]);

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('account/update'))
            ->setData('API_FORM_ACTION', $this->getUrl('account/regenerateApiKey'))
            ->setData('DELETE_FORM_ACTION', $this->getUrl('account/deleteAccountPost'))
            ->setData('UPDATEEMAIL_CANCEL_URL', $this->getUrl('account/cancelEmailUpdate'))
            ->setData('LANGUAGE_SELECT', $languageSelect)
            ->setData('AVATAR_IMAGE', $avatarImage)
            ->setData('BADGE_SELECT', $badgeSelect)
            ->setData('NOTIFICATION_FREQUENCY_SELECT', $emailSendingFrequencySelect)
            ->setData('STALE_RESERVATIONS_YESNO', $staleReservationNotificationsYesNoSelect)
            ->setData('RECOVERER_GEEFTER_SELECT', $geefterSelect)
            ->getLayout()
            ->setContentWrapperTemplate('page/2columns-right.phtml')
            ->setBlockTemplate('right', 'account/links.phtml')
            ->setBlockConfig('form', [
                'type' => \Geeftlist\Block\Account::class,
                'template' => 'account/form.phtml'
            ])
            ->setBlockConfig('password_validation_notice', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'password/rules.pmd',
                'password_min_length' => $this->appConfig->getValue('PASSWORD_MIN_LENGTH'),
                'css_classes' => ['validation-rules', 'info']
            ])
            ->setBlockConfig('stale_reservations_notice', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'account/stale-reservations-notifications.pmd',
                'css_classes' => ['form-element-help', 'info']
            ])
            ->setBlockTemplate('delete_form', 'account/delete_form.phtml');

        if ($this->appConfig->getValue('GEEFTER_SHOW_API_TOKEN_FORM')) {
            $this->view->getLayout()
                ->setBlockTemplate('api_form', 'account/api_form.phtml');
        }
    }

    public function updateAction(): void {
        $post = $this->getPost();
        $geefter = $this->geefterSession->getGeefter();
        $emailUpdateRequested = false;

        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->coreModelFactory->make(ErrorAggregatorInterface::class);

        $post = $this->filterGeefterFormPost($post);
        // Special behavior if email update requested
        if ($pendingEmail = $this->handleEmailUpdate($post, $geefter, $errorAggregator)) {
            $emailUpdateRequested = true;
        }

        $geefter->addData($post);
        $errors = $this->validateGeefterFormPost($post, $geefter, $errorAggregator);
        if ($errors !== []) {
            foreach ($errors as $e) {
                $this->geefterSession->addErrorMessage($e);
            }

            $this->reroute('account');
        }

        $this->getModelRepository(Geefter::ENTITY_TYPE)->save($geefter);

        // Update language immediately for the rest of the request
        if ($lang = $geefter->getLanguage()) {
            $this->geefterSession->setLanguage($lang);
        }

        $this->geefterSession->regenerate()
            ->addSuccessMessage('Profile updated successfully!');

        // Email update requested, redirect to success page
        if ($emailUpdateRequested) {
            $this->geefterEmailService->sendUpdateEmailConfirmationEmail($geefter);
            $this->geefterSession->setAccountEmailUpdate($pendingEmail);
            $this->reroute('account/updateSuccess');
        }

        $this->reroute('account');
    }

    public function updateSuccessAction(): void {
        $this->breadcrumbs->addCrumb('My Account');

        if (! $email = $this->geefterSession->getAccountEmailUpdate()) {
            $this->reroute('account');
        }

        $this->geefterSession->unsAccountEmailUpdate();

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_SUCCESS,
                'content_blocks' => ['dialog_content'],
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('/account'),
                    'text'  => $this->__('Back to my account'),
                    'class' => 'button back'
                ]]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'account/updateemail/request_success.pmd',
                'email'    => $email
            ]);
    }

    public function cancelEmailUpdateAction(): void {
        $geefter = $this->geefterSession->getGeefter();

        if ($this->emailUpdateHelper->cancelEmailUpdate($geefter)) {
            $this->getModelRepository(Geefter::ENTITY_TYPE)->save($geefter);

            $this->geefterSession->addSuccessMessage('Email update request has been cancelled.');
        }

        $this->reroute('*');
    }

    public function confirmEmailUpdateAction(): void {
        $token = $this->getRequestParam('token');

        try {
            $this->emailUpdateHelper->getGeefterFromToken($token);
        }
        catch (EmailUpdateException $emailUpdateException) {
            $this->getSession()->addErrorMessage($emailUpdateException->getMessage());
            $this->reroute('/');
        }

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/confirmEmailUpdatePost', ['token' => $token]))
            ->getLayout()
            ->removeBlock('header.nav')         // TODO Implement a removeBlock('header.*') instead
            ->removeBlock('header.login')
            ->removeBlock('header.login-mobile')
            ->setBlockTemplate('content', 'account/updateemail/index.phtml')
            ->setBlockConfig('form', [
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => 'account/updateemail/form.phtml'
            ]);
    }

    public function confirmEmailUpdatePostAction(): void {
        $token = $this->getRequestParam('token');
        $password = $this->getPost('password');

        try {
            $geefter = $this->emailUpdateHelper->getGeefterFromToken($token);
        }
        catch (EmailUpdateException $emailUpdateException) {
            $this->geefterSession->addErrorMessage($emailUpdateException->getMessage());
            $this->reroute('/');
        }

        if (! $this->validatePassword($geefter, $password)) {
            $this->getSession()->addErrorMessage('Missing or invalid password.');
            $this->redirectReferer();
        }

        $geefter = $this->emailUpdateHelper->applyEmailUpdate($token);

        $this->geefterEmailService->sendEmailUpdatedEmail($geefter);
        $this->getSession()->setUpdateEmailNewAddress($geefter->getEmail());
        $this->reroute('*/confirmEmailUpdateSuccess');
    }

    public function confirmEmailUpdateSuccessAction(): void {
        if (! $newEmail = $this->getSession()->getUpdateEmailNewAddress()) {
            $this->reroute('/');
        }

        $this->getSession()->unsUpdateEmailNewAddress();

        $this->view
            ->setData('EMAIL', $newEmail)
            ->getLayout()
            ->removeBlock('header.nav')         // TODO Implement a removeBlock('header.*') instead
            ->removeBlock('header.login')
            ->removeBlock('header.login-mobile')
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_SUCCESS,
                'content_blocks' => ['dialog_content'],
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('/'),
                    'text'  => $this->__('Back to home'),
                    'class' => 'button back'
                ]]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'account/updateemail/confirm_success.pmd'
            ]);
    }

    public function regenerateApiKeyAction(): void {
        $post = $this->getPost();

        if (! empty($post['confirm'])) {
            $geefter = $this->geefterSession->getGeefter();
            $geefter->regenerateApiKey();
            $this->getModelRepository(Geefter::ENTITY_TYPE)->save($geefter);
            $this->geefterSession->addSuccessMessage('API key regenerated successfully!');
        }

        $this->reroute('account');
    }

    public function deleteAccountPostAction(): void {
        $post = $this->getPost();

        $post = $this->filterDeleteFormPost($post);

        $geefter = $this->geefterSession->getGeefter();

        if (! $this->validatePassword($geefter, $post['current_password'])) {
            throw new AuthenticationException($this->__('Invalid password.'));
        }

        $this->geefterHelper->deleteAccount($geefter, $post);

        $this->geefterSession->logout();
        $this->getSession()->addSuccessMessage('Your account has been successfully deleted. Goodbye :(');

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => [
                    'status' => 'success',
                    'redirect' => $this->getUrl('/')
                ]
            ]);
    }

    /**
     * @return mixed[]
     */
    protected function filterGeefterFormPost(array &$geefterData): array {
        $return = \OliveOil\array_mask($geefterData, [
            'username',
            'dob',
            'current_password',
            'email',
            'password',
            'password_confirm',
            'language',
            'avatar_path',
            'badge_ids',
            'email_notification_freq',
            'stale_reservation_notification_enabled',
        ]);

        // Force NULL for empty date of birth
        if (isset($return['dob'])) {
            $return['dob'] = trim((string) $return['dob']) ?: null;
        }

        if (isset($return['username']) && ! trim((string) $return['username'])) {
            unset($return['username']);
        }

        return $return;
    }

    protected function filterDeleteFormPost(array &$formData): array {
        return \OliveOil\array_mask($formData, [
            'recoverer_geefter_id',
            'current_password'
        ]);
    }

    /**
     * @return string|bool The new pending email if any (errors might be present in $errorAggregator though),
     *                     or FALSE if no email update has been requested
     * @throws \Geeftlist\Exception\Geefter\EmailUpdateException
     */
    protected function handleEmailUpdate(
        array &$geefterData,
        Geefter $geefter,
        ErrorAggregatorInterface $errorAggregator
    ) {
        $pendingEmail = $geefterData['email'] ?? '';
        // Unset this data, it must not be saved with the geefter as is
        unset($geefterData['email']);

        if ($this->emailUpdateHelper->requestEmailUpdate($geefter, $pendingEmail)) {
            // Update email form validation
            if (! $this->validatePassword($geefter, $geefterData['current_password'] ?? null)) {
                $errorAggregator->addFieldError(
                    $this->coreModelFactory->make(ErrorInterface::class)
                        ->setField('email')
                        ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                        ->setLocalizableMessage('Invalid current password.')
                        ->setMessage('Invalid current password.')
                );
            }
            elseif (! $this->emailValidator->validate($pendingEmail)) {
                $errorAggregator->addFieldError(
                    $this->coreModelFactory->make(ErrorInterface::class)
                        ->setField('email')
                        ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                        ->setLocalizableMessage('Invalid email.')
                        ->setMessage('Invalid email.')
                );
            }

            return $pendingEmail;
        }

        return false;
    }

    protected function validateGeefterFormPost(
        array &$geefterData,
        Geefter $geefter,
        ErrorAggregatorInterface $errorAggregator
    ): array {
        // Create a copy with all required keys defined
        $formData = \OliveOil\array_union_intersect($geefterData, [
            'dob',
            'current_password',
            'password',
            'password_confirm',
            'badge_ids'
        ]);

        if (!is_array($formData['badge_ids'])) {
            // Force empty array to clear badges (the key does not appear if none has been selected)
            $geefter->setBadgeIds([]);
        }

        if (
            ! ($password = trim($formData['password'] ?? ''))
            && ! ($passwordConfirm = trim($formData['password_confirm'] ?? ''))
        ) {
            // Prevent saving an empty password!
            $geefter->unsPassword()
                ->unsPasswordConfirm();
            //Not necessary
        } elseif (! $this->validatePassword($geefter, $formData['current_password'] ?? '')) {
            $errorAggregator->addFieldError(
                $this->coreModelFactory->make(ErrorInterface::class)
                    ->setField('password')
                    ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                    ->setLocalizableMessage('Invalid current password.')
                    ->setMessage('Invalid current password.')
            );
        } elseif ($formData['password'] === '' || $formData['password_confirm'] === '') {
            $errorAggregator->addFieldError(
                $this->coreModelFactory->make(ErrorInterface::class)
                    ->setField('password')
                    ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                    ->setLocalizableMessage('Password must be confirmed.')
                    ->setMessage('Password must be confirmed.')
            );
        } elseif ($formData['password'] !== $formData['password_confirm']) {
            $errorAggregator->addFieldError(
                $this->coreModelFactory->make(ErrorInterface::class)
                    ->setField('password')
                    ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                    ->setLocalizableMessage('Passwords do not match.')
                    ->setMessage('Passwords do not match.')
            );
        }

        // Ignore errors regarding empty avatar_file
        if (
            ($avatarFileErrors = $errorAggregator->getFieldErrors('avatar_file')) && (count($avatarFileErrors) == 1
            && current($avatarFileErrors)->getType() == ErrorInterface::TYPE_FIELD_EMPTY)
        ) {
            // Not an error here, file can be empty
            $errorAggregator->clear('avatar_file');
        }

        $errorAggregator->merge($geefter->validate());


        return $this->dataValidationErrorProcessor->process(
            $errorAggregator,
            $geefterData,
            ['dob' => $this->i18n->tr('geeftee.field||dob')]
        );
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword(Geefter $geefter, $password) {
        try {
            $this->authenticationService->authenticateSubject($geefter, ['password' => $password]);

            return true;
        }
        catch (\Throwable) {
            return false;
        }
    }
}
