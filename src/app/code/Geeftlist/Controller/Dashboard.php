<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;

class Dashboard extends AbstractController
{
    public function indexAction(): void {
        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'dashboard/index.phtml')
            ->setBlockConfig('content_header', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'dashboard/header.pmd',
            ])
            ->setBlockConfig('dashboard_announce', [
                'type'     => \Geeftlist\Block\Cms::class,
                'code'     => 'dashboard_announce',
                'quiet'    => true,
            ])
            ->addChild('global_messages', 'dashboard_announce')
            ->setBlockConfig('hints', [
                'type'     => \Geeftlist\Block\Hint::class,
                'template' => 'hints/container.phtml',
            ])
            ->setBlockConfig('latest_reservations', [
                'type'         => \Geeftlist\Block\Widget\LatestReservations::class,
                'template'     => 'common/widgets/latest_reservations.phtml',
                'id'           => 'latest-reservations'
            ])
            ->setBlockConfig('latest_discussion_posts', [
                'type'             => \Geeftlist\Block\Widget\LatestDiscussionPosts::class,
                'template'         => 'common/widgets/latest_discussion_posts.phtml',
                'display_mode'     => 'short',
                'title_max_length' => 80
            ])
            ->setBlockConfig('recent_activity', [
                'type'          => \Geeftlist\Block\Widget\RecentActivity::class,
                'template'      => 'common/widgets/recent_activity.phtml'
            ])->setBlockConfig('pending_actions', [
                'type'     => \Geeftlist\Block\Widget\PendingActions::class,
                'template' => 'common/widgets/pending_actions.phtml'
            ])->setBlockConfig('upcoming_birthdays', [
                'type'     => \Geeftlist\Block\Widget\UpcomingBirthdays::class,
                'template' => 'common/widgets/upcoming_birthdays.phtml'
            ]);
        $this->addGeneralAnnounceMessageBlock();
    }
}
