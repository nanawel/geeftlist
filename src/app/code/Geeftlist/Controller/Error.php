<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use OliveOil\Core\Block\Dialog;
use OliveOil\Core\Model\SessionInterface;

class Error extends AbstractController
{
    protected function init() {
        $this->nonRestrictedActions = ['index'];
    }

    public function indexAction(): void {
        if ($msgs = $this->getSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR)) {
            $messages = [];
            foreach ($msgs as $msg) {
                $messages[] = $msg->getMessage();
            }

            $this->getSession()->clearMessages();
        }
        elseif ($previousError = $this->getSession()->getPreviousError()) {
            $messages = $previousError->getUserMessage();
        }
        else {
            $this->reroute('/');

            return;
        }

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_ERROR,
                'message' => $messages,
                'buttons' => [
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('/'),
                        'text'  => $this->__('Back to home'),
                        'class' => 'button back'
                    ]
                ]
            ]);
    }
}
