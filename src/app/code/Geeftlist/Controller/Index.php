<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;

class Index extends AbstractController
{
    protected function init() {
        $this->nonRestrictedActions = ['index'];
    }

    public function indexActionBefore() {
        if ($this->geefterSession->isLoggedIn()) {
            $this->reroute('dashboard');
            return false;
        }
    }

    /**
     * @param \Base $fw
     */
    public function indexAction($fw): void {
        $this->view->getLayout()
            ->setBlockTemplate('content', 'index/index.phtml')
            ->setBlockConfig('welcome', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'home/welcome.pmd'
            ])
            ->setBlockConfig('home_cms', [
                'type'     => \Geeftlist\Block\Cms::class,
                'code'     => 'home'
            ]);
        $this->addGeneralAnnounceMessageBlock();
    }
}
