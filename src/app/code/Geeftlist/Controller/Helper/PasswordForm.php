<?php

namespace Geeftlist\Controller\Helper;

use Geeftlist\Model\Geefter;
use OliveOil\Core\Exception\AppException;

class PasswordForm
{
    public const MIN_PASSWORD_LENGTH = 8;

    protected \OliveOil\Core\Service\View $view;

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \OliveOil\Core\Service\Response\Redirect $redirectResponse;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \Geeftlist\Model\Session\Http\GeefterInterface $geefterSession;

    protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory;

    protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor;

    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        \Geeftlist\Controller\Http\Context $controllerContext,
        protected \Geeftlist\Service\Geefter\Account $geefterAccountService,
    ) {
        $this->view = $controllerContext->getView();
        $this->appConfig = $controllerContext->getAppConfig();
        $this->urlBuilder = $controllerContext->getUrlBuilder();
        $this->redirectResponse = $controllerContext->getResponseRedirect();
        $this->i18n = $controllerContext->getI18n();
        $this->geefterSession = $controllerContext->getGeefterSession();
        $this->modelRepositoryFactory = $controllerContext->getModelRepositoryFactory();
        $this->dataValidationErrorProcessor = $controllerContext->getDataValidationErrorProcessor();
        $this->eventManager = $controllerContext->getEventService()->getEventManager($this);
    }

    public function passwordformAction(
        string $token,
        string $contentTemplate,
        string $postRoute,
        callable $errorCallback
    ) {
        try {
            $this->geefterAccountService->loadGeefterByToken($token);
        }
        catch (AppException $appException) {
            return call_user_func($errorCallback, $appException);
        }

        // TODO Asking to also confirm email could be more secure
        $this->view
            ->setData('FORM_ACTION', $this->urlBuilder->getUrl($postRoute))
            ->setData('TOKEN', $token)
            ->getLayout()
            ->setBlockConfig('content', ['template' => $contentTemplate])
            ->setBlockConfig('password_validation_notice', [
                'type'        => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template'    => 'password/rules.pmd',
                'password_min_length' => $this->appConfig->getValue('PASSWORD_MIN_LENGTH'),
                'css_classes' => ['validation-rules', 'info']
            ])
            ->setBlockConfig('form', ['template' => 'resetpassword/form.phtml']);
    }

    public function passwordformPostAction(
        array $post,
        callable $successCallback,
        callable $formFailCallback,
        callable $errorCallback
    ) {
        try {
            $geefter = $this->geefterAccountService->loadGeefterByToken($post['token'] ?? null);
        }
        catch (AppException $appException) {
            return call_user_func($errorCallback, $appException);
        }

        $geefterData = $this->filterPasswordFormPost($post);
        $geefterData['password_token'] = null;
        $geefterData['password_token_date'] = null;

        $geefter->addData($geefterData);
        if ($errors = $this->validatePasswordFormPost($post, $geefter)) {
            return call_user_func($formFailCallback, $errors);
        }

        $this->modelRepositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
            ->save($geefter);

        $this->geefterSession->regenerate();
        $this->eventManager->trigger('geefter_password_update', $this, [
            'geefter' => $geefter
        ]);

        return call_user_func($successCallback, $geefter);
    }

    protected function filterPasswordFormPost(array $geefterData): array {
        return array_map('trim', \OliveOil\array_mask($geefterData, [
            'password',
            'password_confirm'
        ]));
    }

    protected function validatePasswordFormPost(array &$geefterData, Geefter $geefter): array {
        return $this->dataValidationErrorProcessor->process(
            $geefter->validate(),
            $geefterData
        );
    }
}
