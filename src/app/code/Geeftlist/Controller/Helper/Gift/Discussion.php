<?php

namespace Geeftlist\Controller\Helper\Gift;

use Geeftlist\Form\Element\Discussion\PostRecipientsSelect;
use Geeftlist\Form\Element\GeefterSelect;
use Geeftlist\Model\Gift;
use OliveOil\Core\Form\Element\Referer;
use OliveOil\Core\Form\ElementInterface;

class Discussion
{
    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \Geeftlist\Model\ModelFactory $modelFactory;

    protected \Geeftlist\Model\RepositoryFactory $modelRepository;

    protected \OliveOil\Core\Service\View $view;

    /** @var \OliveOil\Core\Model\Http\RequestInterface */
    protected $request;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \Geeftlist\Service\Permission $permissionService;

    protected \Geeftlist\Model\Session\Http\GeefterInterface $geefterSession;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Helper\Gift\Discussion\Topic $topicHelper,
        protected \Geeftlist\Helper\Gift $giftHelper
    ) {
        $this->appConfig = $context->getAppConfig();
        $this->modelFactory = $context->getModelFactory();
        $this->modelRepository = $context->getModelRepositoryFactory();
        $this->view = $context->getView();
        $this->request = $context->getRequestManager()->getRequest();
        $this->i18n = $context->getI18n();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->permissionService = $context->getPermissionService();
        $this->geefterSession = $context->getGeefterSession();
    }

    public function appendPostForm(Gift $gift, array $formData = []): void {
        $currentGeeftee = $this->geefterSession->getGeeftee();
        $giftGeeftees = $gift->getGeefteeCollection();
        $isGiftForCurrentGeeftee = in_array($currentGeeftee->getId(), $giftGeeftees->getAllIds());

        $this->view->getLayout()
            ->setBlockConfig('discussion', [
                'template'       => 'gift/view/topics.phtml',
                'title_html_tag' => 'h2'
            ])
            ->setBlockConfig('discussion_pinned_post', [
                'type'        => \Geeftlist\Block\Discussion\Post::class,
                'template'    => 'discussion/post.phtml'
            ])
            ->setBlockConfig('discussion_post', [
                'type'        => \Geeftlist\Block\Discussion\Post::class,
                'template'    => 'discussion/post.phtml'
            ])
            ->setBlockConfig('discussion_form', [
                'template'    => 'discussion/post/form.phtml',
                'form_action' => $this->urlBuilder->getUrl('gift_discussion/postSave', [
                    'gift_id' => $gift->getId(),
                    '_referer' => true
                ]),
                'form_values' => $formData
            ]);
        $this->addFormElements($formData);

        /** @see ticket-593 */
        $shouldShowPublicTopic = $this->giftHelper->isOneselfGift($gift) || $this->giftHelper->isOpenGift($gift);

        $publicTopic = $this->topicHelper->getPublicTopic($gift);
        // Keep compatibility with pre-593 data and allow public topic display if it already has posts
        $hasPublicTopicPosts = $publicTopic && $publicTopic->getPostCount() > 0;

        if ($hasPublicTopicPosts || ($shouldShowPublicTopic && $publicTopic)) {
            $this->view->setData('PUBLIC_TOPIC', $publicTopic)
                ->getLayout()
                ->setBlockConfig('discussion_public_topic', [
                    'template' => 'discussion/topic.phtml',
                    'topic'    => $publicTopic
                ]);

            // Display notice only to other geefters
            if (! $isGiftForCurrentGeeftee) {
                $this->view->getLayout()
                    ->setBlockConfig(
                        'discussion_public_topic',
                        'before_form_block',
                        'public_topic_discussion_post_form_after'
                    )
                    ->setBlockConfig('public_topic_discussion_post_form_after', [
                        'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                        'template' => 'gift/discussion/notice_public_topic.pmd'
                    ]);
            }
        }

        if ($protectedTopic = $this->topicHelper->getProtectedTopic($gift)) {
            $this->view->setData('PROTECTED_TOPIC', $protectedTopic)
                ->getLayout()
                ->setBlockConfig('discussion_protected_topic', [
                    'template'          => 'discussion/topic.phtml',
                    'topic'             => $protectedTopic,
                    'before_form_block' => 'protected_topic_discussion_post_form_after'
                ])
                ->setBlockConfig('protected_topic_discussion_post_form_after', [
                    'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                    'template' => 'gift/discussion/notice_protected_topic.pmd'
                ]);
        }
    }

    public function filterFormPost(array $formData) {
        /** @var ElementInterface $formElement */
        foreach ($this->getFormElements($formData) as $formElement) {
            $formElement->filter($formData);
        }

        return $formData;
    }

    /**
     * @return PostRecipientsSelect
     */
    public function getPostRecipientsSelect(array $formData) {
        return $this->modelFactory->make(
            PostRecipientsSelect::class,
            [
                'data' => [
                    'name'       => 'recipient_ids',
                    'id'         => 'recipient_ids',
                    'value'      => $formData['recipient_ids'] ?? '',
                    'exclude'    => [$this->geefterSession->getGeefter()->getId()],
                    'with_empty' => true,
                    'topic_id'   => $formData['topic_id'] ?? null
                ]
            ]
        );
    }

    /**
     * @return ElementInterface[]
     */
    public function getFormElements(array $formData): array {
        $formElements = [
            'DISCUSSION_POST_REFERER' => $this->modelFactory->make(Referer::class)
        ];
        if ($this->appConfig->getValue('GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION')) {
            $postRecipientsSelectProvider = new class ($this, $formData) {
                private array $postRecipientsSelect = [];

                public function __construct(private $outer, private array $formData)
                {
                }

                /**
                 * @param $topicId
                 * @return GeefterSelect
                 */
                public function getPostRecipientsSelect($topicId = null) {
                    $topicId = is_object($topicId) ? $topicId->getId() : $topicId;
                    if (isset($this->formData['topic_id'])) {
                        if (!$topicId) {
                            $topicId = $this->formData['topic_id'];
                        }
                        elseif ($this->formData['topic_id'] != $topicId) {
                            throw new \InvalidArgumentException(
                                sprintf("Provided topic ID '%s' does not match form data.", $topicId)
                            );
                        }
                    }

                    if (!isset($this->postRecipientsSelect[$topicId])) {
                        $this->postRecipientsSelect[$topicId] = $this->outer->getPostRecipientsSelect(
                            $this->formData + ['topic_id' => $topicId]
                        );
                    }

                    return $this->postRecipientsSelect[$topicId];
                }

                /**
                 * Need to explicitly declare this method because of the passing-by-reference usage
                 */
                public function filter(array &$formData): void {
                    $this->getPostRecipientsSelect()->filter($formData);
                }

                public function __invoke($topicId = null) {
                    return $this->getPostRecipientsSelect($topicId);
                }

                public function __call($name, $args) {
                    return $this->getPostRecipientsSelect()->$name(...$args);
                }
            };

            $formElements['POST_RECIPIENTS_SELECT_PROVIDER'] = $postRecipientsSelectProvider;
        }

        return $formElements;
    }

    /**
     * @return $this
     */
    public function addFormElements(array $formData): static {
        foreach ($this->getFormElements($formData) as $name => $formElement) {
            $this->view->setData($name, $formElement);
        }

        return $this;
    }
}
