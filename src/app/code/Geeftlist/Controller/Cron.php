<?php

namespace Geeftlist\Controller;

use Narrowspark\HttpStatus\Exception\ForbiddenException;
use OliveOil\Core\Helper\DateTime;
use Wikimedia\IPUtils;

class Cron extends \OliveOil\Core\Controller\Http\AbstractController
{
    public const LOCK_NAME = 'cron';

    public function __construct(
        \OliveOil\Core\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\EventInterface $cronJobsEventService,
        protected \OliveOil\Core\Service\Design $design,
        protected \OliveOil\Core\Service\Lock $lockService,
        protected \OliveOil\Core\Model\App\FlagInterface $appFlag
    ) {
        // Prevent sessions startup in the following parent constructor call
        $context->getFw()->set('DISABLE_SESSIONS', true);

        parent::__construct($context);

        // No redirect on error
        $this->appConfig->setValue('ERROR_USE_REDIRECT', false);

        // Disable ActivityLog unless explicitly enabled
        if (!$this->appConfig->getValue('ACTIVITYLOG_CRON_ENABLED', false)) {
            $this->appConfig->setValue('ACTIVITYLOG_ENABLED', false);
        }
    }

    protected function onBeforeRoute(\Base $fw, array $args = []) {
        $allowedRanges = is_array($this->appConfig->getValue('CRON_ALLOWED_IPS'))
            ? $this->appConfig->getValue('CRON_ALLOWED_IPS')
            : explode(',', $this->appConfig->getValue('CRON_ALLOWED_IPS'));
        $clientIp = $this->fw->ip();
        if ($clientIp && !IPUtils::isInRanges($clientIp, $allowedRanges)) {
            throw new ForbiddenException();
        }

        // Clear session if any
        $this->getSession()->abortClose(true);

        return parent::onBeforeRoute($fw, $args);
    }

    public function runAction(): void {
        // Call run using mutex
        $this->lockService->synchronized(static::LOCK_NAME, fn() => $this->doRun(), 0);
        exit;
    }

    /**
     * Must be public for compatiblity with LockService::synchronized()
     */
    public function doRun(): void {
        try {
            $startTime = microtime(true);
            $this->logger->info('Starting cron jobs execution');

            $this->cronJobsEventService->setupListeners()
                ->getEventManager($this)->trigger('always', $this);

            $duration = microtime(true) - $startTime;
            $this->logger->info(sprintf(
                'Finished cron jobs execution (%s)',
                DateTime::secondsToTime($duration)
            ));
            $this->appFlag->set('cron/last_success', time());
            $this->appFlag->set('cron/last_success_duration', $duration);
        }
        catch (\Throwable $throwable) {
            $this->logger->critical($throwable);
            if (! $this->shouldUseSilentFailure()) {
                throw $throwable;
            }
        } finally {
            $this->appFlag->set('cron/last_run', time());
        }
    }

    public function runJobAction(): void {
        // Call run using mutex
        $this->lockService->synchronized(static::LOCK_NAME, fn() => $this->doRunJob(), 0);
        exit;
    }

    /**
     * Must be public for compatiblity with LockService::synchronized()
     */
    public function doRunJob(): void {
        try {
            if (! $job = $this->getRequestParam('job')) {
                throw new \InvalidArgumentException('Missing job name.');
            }

            if ($job === 'always') {
                throw new \InvalidArgumentException('Invalid job name.');
            }

            $params = $this->getRequest()->getRequestParams();

            $startTime = microtime(true);
            $this->logger->info('Starting cron job execution for ' . $job);

            $this->cronJobsEventService->setupListeners()
                ->getEventManager($this)->trigger($job, $this, $params);

            $this->logger->info(sprintf(
                'Finished cron job execution for %s (%s)',
                $job,
                DateTime::secondsToTime(microtime(true) - $startTime)
            ));
        }
        catch (\Throwable $throwable) {
            $this->logger->critical($throwable);
            if (! $this->shouldUseSilentFailure()) {
                throw $throwable;
            }
        }
    }

    public function shouldUseSilentFailure(): bool {
        return $this->appConfig->getValue('MODE') !== 'developer';
    }
}
