<?php
namespace Geeftlist\Controller\Discussion;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Controller\Traits\DiscussionTrait;
use Geeftlist\Exception\Security\PermissionException;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Exception\MissingValueException;
use OliveOil\Core\Exception\NoSuchEntityException;


class Post extends AbstractController
{
    use DiscussionTrait;

    /** @var \Geeftlist\Service\PermissionInterface */
    protected $permissionService;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Helper\Discussion\Post $postService
    ) {
        parent::__construct($context);
        $this->permissionService = $context->getPermissionService();
    }

    public function saveAction(): void {
        try {
            [$postModel, $isEdition] = $this->savePost();
        }
        catch (NoSuchEntityException | MissingValueException | PermissionException $e) {
            $this->geefterSession->addErrorMessage($e->getMessage());
            $this->redirectReferer();
        }
        catch (InvalidValueException) {
            $this->redirectReferer();
        }

        if ($isEdition) {
            $this->redirectReferer();
        }
        else {
            $this->redirectReferer('discussion-post-' . $postModel->getId());
        }
    }

    public function editAction(): void {
        $this->editPost();
    }

    public function deleteAction(): void {
        $this->deletePost();
        $this->redirectReferer();
    }

    public function pinAction(): void {
        try {
            $post = $this->getPostModel();
            $this->postService->pin($post);
            $this->geefterSession->addSuccessMessage('The post has been post pinned.');
        }
        catch (\Throwable $throwable) {
            $this->logger->warning($throwable);
            $this->geefterSession->addErrorMessage('Sorry, you cannot change the "pinned" status of this post.');
        }

        $this->redirectReferer();
    }

    public function unpinAction(): void {
        try {
            $post = $this->getPostModel();
            $this->postService->unpin($post);
            $this->geefterSession->addSuccessMessage('The post has been post unpinned.');
        }
        catch (\Throwable $throwable) {
            $this->logger->warning($throwable);
            $this->geefterSession->addErrorMessage('Sorry, you cannot change the "pinned" status of this post.');
        }

        $this->redirectReferer();
    }
}
