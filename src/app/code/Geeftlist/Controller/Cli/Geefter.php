<?php
namespace Geeftlist\Controller\Cli;


use OliveOil\Core\Model\RepositoryInterface;

class Geefter extends AbstractController
{
    public const RETURN_ENTITY_NOT_FOUND = 40;

    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\Crypt $cryptService
    ) {
        parent::__construct($context);
    }

    public function setPasswordAction(\Base $fw, array $args): int {
        /** @var RepositoryInterface $geefterRepository */
        $geefterRepository = $this->modelRepositoryFactory->resolveGet(\Geeftlist\Model\Geefter::ENTITY_TYPE);

        if ($id = $this->getRequestQuery('id')) {
            $geefter = $geefterRepository->get($id);
        }
        elseif ($email = $this->getRequestQuery('email')) {
            $geefter = $geefterRepository->get($email, 'email');
        }
        else {
            $this->errnl('You need to specify --id=<id> or --email=<email>');
            return self::RETURN_MISSING_ARGUMENT;
        }

        if (! $geefter) {
            $this->errnl('No such geefter.');
            return self::RETURN_ENTITY_NOT_FOUND;
        }

        /** @var \Geeftlist\Model\Geefter $geefter */
        $this->printnl('Geefter: ' . $geefter->getUsername());
        $this->printnl('Email  : ' . $geefter->getEmail());

        $password = $this->read('New password? > ');
        $passwordConfirm = $this->read('New password (confirmation)? > ');

        if ($password !== $passwordConfirm) {
            $this->errnl('Passwords do not match.');
            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->printnl('');
        $confirm = $this->read('Do you confirm? [Y/n] ') ?: 'y';
        if (!in_array($confirm, ['Y', 'y'])) {
            $this->printnl('Canceled.');
            return self::RETURN_SUCCESS;
        }

        $geefter->setPassword($password);
        $geefterRepository->save($geefter);

        $this->printnl('New hash: ' . $geefter->getPasswordHash());

        return self::RETURN_SUCCESS;
    }

    public function calculatePasswordHashAction(\Base $fw, array $args): int {
        $password = $this->read('Password? > ');
        $this->printnl('Hash: ' . $this->cryptService->hash($password));

        return self::RETURN_SUCCESS;
    }
}
