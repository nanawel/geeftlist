<?php
namespace Geeftlist\Controller\Cli;


use OliveOil\Core\Helper\DateTime;

class Housekeeping extends AbstractController
{
    public function __construct(
        Context $context,
        protected \Geeftlist\Service\System\Housekeeping $housekeepingService
    ) {
        parent::__construct($context);
    }

    public function pruneAllBrokenEntityLinks(\Base $fw, array $args): void {
        $startTime = microtime(true);

        $this->printnl("Pruning all broken entity links...");
        $this->housekeepingService->pruneAllBrokenEntityLinks();
        $this->printnl(sprintf('Finished in %s.', DateTime::secondsToTime(microtime(true) - $startTime)));
    }

    public function pruneBrokenEntityLinksAction(\Base $fw, array $args) {
        $entityType = $this->getRequestQuery('entityType');
        if (trim((string) $entityType) === '' || trim((string) $entityType) === '0') {
            $this->errnl('Missing --entityType option.');

            return self::RETURN_MISSING_ARGUMENT;
        }

        $startTime = microtime(true);

        $this->printnl(sprintf('Pruning broken entity links on %s...', $entityType));
        $this->housekeepingService->pruneBrokenEntityLinks($entityType);
        $this->printnl(sprintf('Finished in %s.', DateTime::secondsToTime(microtime(true) - $startTime)));
    }
}
