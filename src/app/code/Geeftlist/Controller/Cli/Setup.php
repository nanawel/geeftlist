<?php

namespace Geeftlist\Controller\Cli;

class Setup extends AbstractController
{
    public function __construct(
        Context $context,
        protected \OliveOil\Core\Model\SetupInterface $setup,
        protected \OliveOil\Core\Service\System $systemService,
        protected \OliveOil\Core\Service\Cache $cache
    ) {
        parent::__construct($context);
    }

    public function isInstalledAction(\Base $fw, array $args): int {
        try {
            if ($this->setup->isInstalled()) {
                $this->printnl('Application is installed.');
                $return = self::RETURN_YES;
            }
            else {
                $this->printnl('Application is NOT installed.');
                $return = self::RETURN_NO;
            }

            return $return;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }
        }

        return self::RETURN_UNDEFINED_FAILURE;
    }

    public function installAction(\Base $fw, array $args): int {
        $dropFirst = $this->getRequestQuery('dropFirst') !== null;
        $targetVersion = $this->getRequestQuery('targetVersion');

        if (! $dropFirst && $this->setup->isInstalled()) {
            $this->errnl('Application seems already installed. Try with "dropFirst" option to clear database first.');

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->clearCache();

        try {
            $this->checkVersionFormat($targetVersion);
            $this->printnl(
                $targetVersion !== null
                    ? sprintf('Installing application to %s...', $targetVersion)
                    : 'Installing application...'
            );

            if ($dropFirst) {
                $this->printnl('WARN Dropping existing installation first');
            }

            $this->setup->install($dropFirst, $targetVersion);

            $this->printSetupErrors();
            $this->printnl('Success.');

            return self::RETURN_SUCCESS;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }

            return self::RETURN_UNDEFINED_FAILURE;
        }
    }

    public function upgradeAction(\Base $fw, array $args): int {
        if (! $this->setup->isInstalled()) {
            $this->errnl("Install application first.");

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $enableMaintenance = $this->getRequestQuery('withMaintenance') !== null;
        $targetVersion = $this->getRequestQuery('targetVersion');
        $force = (bool) $this->getRequestQuery('force');

        $this->clearCache();

        try {
            $this->checkVersionFormat($targetVersion);
            if ($enableMaintenance) {
                $this->print('Enabling maintenance...');
                if (! $this->systemService->startMaintenance()) {
                    throw new \RuntimeException('Cannot start maintenance');
                }

                $this->print('OK.');
            }

            $this->printnl(
                $targetVersion !== null
                    ? sprintf('Upgrading application to %s...', $targetVersion)
                    : 'Upgrading application...'
            );
            $this->setup->upgrade($targetVersion, $force);
            $this->printSetupErrors();
            $this->printnl('Success.');
            if ($enableMaintenance) {
                $this->print('Disabling maintenance...');
                if (! $this->systemService->endMaintenance()) {
                    throw new \RuntimeException('Cannot end maintenance');
                }

                $this->print('OK.');
            }

            return self::RETURN_SUCCESS;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }

            return self::RETURN_UNDEFINED_FAILURE;
        }
    }

    public function uninstallAction(\Base $fw, array $args): int {
        if (! $this->setup->isInstalled()) {
            $this->errnl("Install application first.");

            return self::RETURN_UNDEFINED_FAILURE;
        }

        // Accept --all=1 or simply --all
        $all = ((int) $this->getRequestQuery('all')) || $this->getRequestQuery('all') === '';

        $targetVersion = $this->getRequestQuery('targetVersion');
        if (!$targetVersion) {
            $targetVersion = $this->getRequestQuery('to');  // Alias
        }

        $force = (bool) $this->getRequestQuery('force');

        if (!$targetVersion && !$all) {
            $this->errnl('Target version has not been specified. Use --all to uninstall application completely.');

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->clearCache();

        try {
            $this->checkVersionFormat($targetVersion);
            $this->printnl(
                $targetVersion !== null
                    ? sprintf('Downgrading to %s...', $targetVersion)
                    : 'Uninstalling application...'
            );
            $this->setup->uninstall($targetVersion, $force);
            $this->printSetupErrors();
            $this->printnl('Success.');

            return self::RETURN_SUCCESS;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }

            return self::RETURN_UNDEFINED_FAILURE;
        }
    }


    public function downgradeAction(...$args): int {
        return $this->uninstallAction(...$args);
    }

    public function installSampleDataAction(\Base $fw, array $args): int {
        if (! $this->setup->isInstalled()) {
            $this->errnl("Install application first.");

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $targetVersion = $this->getRequestQuery('targetVersion');

        $this->clearCache();

        try {
            $this->checkVersionFormat($targetVersion);
            $this->printnl(
                $targetVersion !== null
                    ? sprintf('Installing sample data to %s...', $targetVersion)
                    : 'Installing sample data...'
            );
            $this->setup->installSampleData($targetVersion);
            $this->printSetupErrors();
            $this->printnl('Success.');

            return self::RETURN_SUCCESS;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }

            return self::RETURN_UNDEFINED_FAILURE;
        }
    }

    public function upgradeSampleDataAction(\Base $fw, array $args): int {
        if (! $this->setup->isInstalled()) {
            $this->errnl("Install application first.");

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $targetVersion = $this->getRequestQuery('targetVersion');

        $this->clearCache();

        try {
            $this->checkVersionFormat($targetVersion);
            $this->printnl(
                $targetVersion !== null
                    ? sprintf('Upgrading sample data to %s...', $targetVersion)
                    : 'Upgrading sample data...'
            );
            $this->setup->upgradeSampleData($targetVersion);
            $this->printSetupErrors();
            $this->printnl('Success.');

            return self::RETURN_SUCCESS;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }

            return self::RETURN_UNDEFINED_FAILURE;
        }
    }

    public function uninstallSampleDataAction(\Base $fw, array $args): int {
        if (! $this->setup->isInstalled()) {
            $this->errnl("Install application first.");

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $targetVersion = $this->getRequestQuery('targetVersion');

        $this->clearCache();

        try {
            $this->checkVersionFormat($targetVersion);
            $this->printnl(
                $targetVersion !== null
                    ? sprintf('Downgrading sample data to %s...', $targetVersion)
                    : 'Uninstalling sample data...'
            );
            $this->setup->uninstallSampleData($targetVersion);
            $this->printSetupErrors();
            $this->printnl('Success.');

            return self::RETURN_SUCCESS;
        } catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            if ($fw->get('DEBUG') > 1) {
                throw $throwable;
            }

            return self::RETURN_UNDEFINED_FAILURE;
        }
    }

    /**
     * Print non-fatal errors that occured during setup execution
     *
     * @param array|null $errors
     * @return $this
     */
    protected function printSetupErrors(array $errors = null): static {
        if ($errors === null) {
            $errors = $this->setup->getErrors();
        }

        foreach ($errors as $err) {
            $this->errnl('WARN ' . $err['message']);
        }

        return $this;
    }

    protected function isValidVersionFormat($version): bool {
        return (bool)preg_match('/(\d+\.\d+\.\d+)/', (string) $version);
    }

    protected function checkVersionFormat($version): static {
        $version = trim((string) $version);
        if ($version && ! $this->isValidVersionFormat($version)) {
            throw new \InvalidArgumentException(sprintf("'%s' is not a valid version.", $version));
        }

        return $this;
    }

    protected function clearCache($exceptionOnError = false) {
        try {
            $this->cache->clear();
        }
        catch (\Throwable $throwable) {
            if ($exceptionOnError) {
                throw $throwable;
            }
        }
    }
}
