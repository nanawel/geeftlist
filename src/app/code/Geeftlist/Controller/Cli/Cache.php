<?php
namespace Geeftlist\Controller\Cli;

use Cache\Hierarchy\HierarchicalPoolInterface;

class Cache extends AbstractController
{
    public function __construct(
        Context $context,
        protected \Cache $f3Cache,
        protected \OliveOil\Core\Service\Cache $cache
    ) {
        parent::__construct($context);
    }

    /**
     * @param string[] $args
     */
    public function clearAction(\Base $fw, array $args): int {
        $success = $this->f3Cache->reset();

        if (!empty($args['*'])) {
            $namespaces = explode(',', $args['*']);
            foreach ($namespaces as $ns) {
                $success &= $this->cache->delete(HierarchicalPoolInterface::HIERARCHY_SEPARATOR . $ns);
                $this->errnl("INFO: Cleared namespace $ns");
            }
        } else {
            $success &= $this->cache->clear();
        }

        if ($success === 0) {
            $this->errnl('ERROR: Could not clear the cache!');

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->errnl('OK, cache has been cleared.');

        return self::RETURN_SUCCESS;
    }

    /**
     * @param string[] $args
     */
    public function cleanAction(\Base $fw, array $args): int {
        return $this->clearAction($fw, $args);
    }

    /**
     * @param string[] $args
     */
    public function flushAction(\Base $fw, array $args): int {
        return $this->clearAction($fw, $args);
    }

    /**
     * @param string[] $args
     */
    public function invalidateAction(\Base $fw, array $args): int {
        $tags = array_filter(explode(',', $args['*'] ?? ''));
        if (empty($tags)) {
            $this->errnl('ERROR: No tags specified.');

            return self::RETURN_UNDEFINED_FAILURE;
        }

        if (!$this->cache->invalidateTags($tags)) {
            $this->errnl('ERROR: Could not invalidate tags.');

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->errnl('OK, cache tags invalidated.');

        return self::RETURN_SUCCESS;
    }
}
