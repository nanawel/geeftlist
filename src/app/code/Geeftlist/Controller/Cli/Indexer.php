<?php
namespace Geeftlist\Controller\Cli;

use OliveOil\Core\Exception\Di\Exception;
use OliveOil\Core\Helper\DateTime;

class Indexer extends AbstractController
{
    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $indexerFactory,
        protected \OliveOil\Core\Util\Profiler $profiler,
        /** @var \Geeftlist\Indexer\IndexerInterface[] */
        protected array $indexers
    ) {
        parent::__construct($context);
    }

    public function reindexAction(\Base $fw, array $args) {
        $indexCode = $this->getRequestQuery('index');
        if (empty($indexCode)) {
            throw new \InvalidArgumentException('Missing --index argument.');
        }

        $entityIds = $this->parseEntityIdsArg($this->getRequestQuery('entityIds'));

        if ($this->getRequestQuery('profile')) {
            $this->profiler->setClearAfterSave(false);
        }

        try {
            /** @var \Geeftlist\Indexer\IndexerInterface $indexer */
            $indexer = $this->indexerFactory->resolveGet($indexCode);
        }
        catch (Exception) {
            $this->errnl('Invalid index code: ' . $indexCode);
            return self::RETURN_UNDEFINED_FAILURE;
        }

        $startTime = microtime(true);
        if ($entityIds === []) {
            $this->print(sprintf('Refreshing index %s... ', $indexCode));
            $indexer->reindexAll();
        }
        else {
            $this->print(sprintf('Refreshing index %s for %d entities... ', $indexCode, count($entityIds)));
            $indexer->reindexEntityIds($entityIds);
        }

        $this->printnl(sprintf('Finished in %s.', DateTime::secondsToTime(microtime(true) - $startTime)));

        if ($this->getRequestQuery('profile')) {
            $this->printnl('== Profiling Results ==');
            $profilingData = $this->profiler->getProfilingData('event');

            if (!$profilingData) {
                $this->errnl(<<<EOT
No profiling data found!
Please make sure:
 - the profiler is enabled in DB configuration and at least *one* event type too
 - the profiler event space is enabled in DI configuration under the key "event_services.all"
 - DEV.EVENT_ENABLE_TRIGGER_EVENT is enabled in the INI configuration file if you need to profile events
EOT
                );
                return self::RETURN_SUCCESS;
            }

            $this->printnl("type\ttime\tuserData");
            foreach ($profilingData as $item) {
                $userData = $item['userData'] ?? [];
                array_walk($userData, static function (&$v, $k): void {
                    $v = sprintf('%s=%s', $k, $v);
                });

                $this->printnl(sprintf(
                    "%s\t%s\t%s",
                    $item['type'],
                    round($item['time'] * 1000, 2),
                    implode(' ', $userData)
                ));
            }
        }
    }

    public function reindexAllAction(\Base $fw, array $args): void {
        foreach ($this->indexers as $indexerCode => $indexer) {
            $this->print(sprintf('Refreshing index %s... ', $indexerCode));
            $startTime = microtime(true);
            $indexer->reindexAll();
            $this->printnl(sprintf('Finished in %s.', DateTime::secondsToTime(microtime(true) - $startTime)));
        }
    }

    /**
     * @param string $arg
     * @return int[]
     */
    protected function parseEntityIdsArg($arg): array {
        $elements = array_filter(explode(',', (string) $arg));

        return array_unique(array_reduce($elements, static function (array $carry, string $el): array {
            if (is_numeric($el)) {
                return array_merge($carry, [(int) $el]);
            }

            if (preg_match('/(\d+)-(\d+)/', $el, $m)) {
                return array_merge($carry, range($m[1], $m[2]));
            }

            throw new \InvalidArgumentException('Invalid format: ' . $el);
        }, []));
    }
}
