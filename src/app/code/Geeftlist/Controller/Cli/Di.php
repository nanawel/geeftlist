<?php
namespace Geeftlist\Controller\Cli;

use Symfony\Component\Finder\Finder;

class Di extends AbstractController
{
    /**
     * @param string[] $args
     */
    public function clearAction(\Base $fw, array $args): int {
        $dirPath = $this->fw->get('CONTAINER_COMPILED_DIR_PATH');
        $finder = (new Finder())
            ->in($dirPath)
            ->files()
            ->ignoreDotFiles(true)
            ->name('*.php')
        ;
        $success = true;
        foreach ($finder as $diFile) {
            $success &= unlink($diFile->getRealPath());
        }

        if (!$success) {
            $this->errnl('ERROR: Could not clear compiled DI directory!');

            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->errnl('OK, compiled DI directory has been cleared.');

        return self::RETURN_SUCCESS;
    }

    /**
     * @param string[] $args
     */
    public function cleanAction(\Base $fw, array $args): int {
        return $this->clearAction($fw, $args);
    }
}
