<?php

namespace Geeftlist\Controller\Cli;


class AbstractController extends \OliveOil\Core\Controller\Cli\AbstractController
{
    protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory;

    public function __construct(Context $context) {
        parent::__construct($context);
        $this->modelRepositoryFactory = $context->getModelRepositoryFactory();
    }
}
