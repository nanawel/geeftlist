<?php

namespace Geeftlist\Controller\Cli;


class Context extends \OliveOil\Core\Controller\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Service\Design $design,
        protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $logService
        );
    }

    public function getDesign(): \OliveOil\Core\Service\Design {
        return $this->design;
    }

    public function getModelRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->modelRepositoryFactory;
    }
}
