<?php
namespace Geeftlist\Controller\Cli;

use OliveOil\Core\Exception\DatabaseException;

class System extends AbstractController
{
    public function __construct(
        Context $context,
        protected \OliveOil\Core\Model\SetupInterface $setup,
        protected \OliveOil\Core\Service\System $systemService
    ) {
        parent::__construct($context);
    }

    /**
     * @param string[] $args
     */
    public function waitForDbAction(\Base $fw, array $args): int {
        $timeout = $this->getRequestQuery('timeout');

        $this->printnl("** Checking database server access...");
        $this->printnl($this->setup->getDbInfo());
        if ($this->checkDb($timeout)) {
            $this->printnl("Database connection suceeded");
            return self::RETURN_SUCCESS;
        }

        return self::RETURN_UNDEFINED_FAILURE;
    }

    /**
     * @param string[] $args
     */
    public function startMaintenanceAction(\Base $fw, array $args): int {
        if ($this->systemService->startMaintenance()) {
            return self::RETURN_SUCCESS;
        }

        return self::RETURN_UNDEFINED_FAILURE;
    }

    /**
     * @param string[] $args
     */
    public function endMaintenanceAction(\Base $fw, array $args): int {
        if ($this->systemService->endMaintenance()) {
            return self::RETURN_SUCCESS;
        }

        return self::RETURN_UNDEFINED_FAILURE;
    }

    /**
     * @param string[] $args
     */
    public function sysinfoAction(\Base $fw, array $args): int {
        $hiveLite = [];
        foreach ($this->fw->hive() as $k => $v) {
            if (!is_object($v) && !is_array($v)) {
                $hiveLite[$k] = $v;
            }
        }

        $this->printnl(print_r($hiveLite, true));

        return self::RETURN_SUCCESS;
    }

    /**
     * @param string[] $args
     */
    public function dumpConfigAction(\Base $fw, array $args): int {
        $this->printnl(print_r($this->appConfig->getAllValues(), true));

        return self::RETURN_SUCCESS;
    }

    protected function checkDb($timeout = null): bool {
        $hardTimeout = time() + 600;
        $start = time();
        $timeout = (int) ($timeout ?? $this->appConfig->getValue('DB_CONNECT_TIMEOUT'));
        if ($timeout !== 0) {
            if (($maxExecutionTime = (int) ini_get('max_execution_time')) !== 0) {
                ini_set('max_execution_time', $timeout + $maxExecutionTime);
            }

            $this->printnl("max_execution_time set to: " . ini_get('max_execution_time'));
            while (time() < $hardTimeout) {
                if ($this->setup->isDbUp()) {
                    return true;
                }

                if (time() > $start + $timeout) {
                    throw new DatabaseException(sprintf(
                        'Could not connect to server after %d seconds, giving up. '
                        . '(Check connectivity or try increasing DB_CONNECT_TIMEOUT in local.ini)',
                        $timeout
                    ));
                }

                $this->printnl("Database is not available, waiting before retrying...");
                sleep(2);
            }
        }

        return false;
    }
}
