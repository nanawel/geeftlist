<?php
namespace Geeftlist\Controller\Cli;


use OliveOil\Core\Model\I18n;

class Dev extends AbstractController
{
    public const FILTERED_HIVE_KEYS = [
        'LANG',
        'ROUTES',
        'GET',
        'POST',
        'COOKIE',
        'REQUEST',
        'SESSION',
        'FILES',
        'app.RESOURCE_CONFIG.db.password',
        'app.RESOURCE_CONFIG.redis_cache.password',
        'events',
    ];

    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $emailFactory,
        protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService
    ) {
        parent::__construct($context);
    }

    public function dictSyncAction(\Base $fw, array $args): int {
        $lang = $this->urlBuilder->getRequestQuery('language');
        if (! $lang) {
            $this->errnl('Missing language');
            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->printnl(sprintf("Generating dict skeleton for language '%s'...", $lang));

        try {
            $dictDir = rtrim((string) $this->fw->get('LOCALES'), DIRECTORY_SEPARATOR);
            $skelLang = $this->appConfig->getValue('LANGUAGE_SKEL');
            $skelLangDictHash = sha1(sprintf('%s/%s.php', $dictDir, $skelLang));
            $skelLangDict = \OliveOil\Core\Model\I18n::fromF3Dict(include sprintf('%s/%s.php', $dictDir, $skelLang));
            $newLangFile = sprintf('%s/%s.php', $dictDir, $lang);
            $newLangFixedFile = sprintf('%s/%s.fixed.php', $dictDir, $lang);

            if (is_file($newLangFile)) {
                $this->printnl(sprintf('Language file %s exists. Checking hash.', $newLangFile));
                $line = fgets(fopen($newLangFile, 'r'));
                if (preg_match('/SKELDICTHASH@(.*?)@([0-9a-f]{40})@/', $line, $m) && ($m[1] == $skelLang && $m[2] == $skelLangDictHash)) {
                    $this->printnl("Already up-to-date.");
                    return self::RETURN_SUCCESS;
                }
            }

            if (is_file($newLangFixedFile)) {
                $this->printnl(sprintf('Language file %s exists, using it as base and adding missing entries.', $newLangFixedFile));
                $langDict = \OliveOil\Core\Model\I18n::fromF3Dict(include $newLangFixedFile);
            }
            else {
                $langDict = [];
            }

            foreach ($skelLangDict as $baseString => $translatedString) {
                if (!array_key_exists($baseString, $langDict)) {
                    if (($pos = strpos($baseString, I18n::STRING_CONTEXT_SPECIFICATION_SEPARATOR)) !== false) {
                        $translatedString = substr($baseString, $pos + 2);
                    }
                    else {
                        $translatedString = $baseString;
                    }

                    $langDict[$baseString] = $translatedString;
                }
            }

            $newDictContent = var_export($langDict, true);

            if (is_file($newLangFile)) {
                rename($newLangFile, $backupLangFile = $newLangFile . '.bak-' . time());
                $this->printnl(sprintf('Backup created at %s.', $backupLangFile));
            }

            file_put_contents($newLangFile, <<<EODICT
<?php //SKELDICTHASH@{$skelLang}@{$skelLangDictHash}@
return \OliveOil\Core\Model\I18n::toF3Dict({$newDictContent});
EODICT
            );
            $this->printnl("Done.");
            return self::RETURN_SUCCESS;
        }
        catch (\Throwable $throwable) {
            $this->errnl($throwable->getMessage());
            return self::RETURN_UNDEFINED_FAILURE;
        }
    }

    public function sendTestEmailAction(\Base $fw, array $args): int {
        if (!($adminEmail = $this->getRequestQuery('email')) && ! $adminEmail = $this->appConfig->getValue('EMAIL_ADMIN')) {
            $this->errnl('No EMAIL_ADMIN defined in configuration. Please set it first.');
            return self::RETURN_UNDEFINED_FAILURE;
        }

        $lang = $this->getRequestQuery('lang');
        if (! $lang) {
            $lang = $this->i18n->getLanguage();
        }

        $this->printnl(sprintf('Sending test mail to %s with language %s...', $adminEmail, $lang));

        $sysinfo = [
            'datetime'              => date('r'),
            'hostname'              => gethostname(),
            'app_title'             => $this->appConfig->getValue('TITLE'),
            'version'               => $this->appConfig->getValue('VERSION'),
            'db_version_schema'     => $this->appConfig->getValue('DB_VERSION_SCHEMA'),
            'db_version_data'       => $this->appConfig->getValue('DB_VERSION_DATA'),
            'db_version_sampledata' => $this->appConfig->getValue('DB_VERSION_SAMPLEDATA'),
            'lang'                  => $lang
        ];
        ob_start();
        phpinfo();
        $phpinfo = ob_get_clean();

        $i18n = clone $this->i18n;
        $i18n->setLanguage($lang);

        $email = $this->emailFactory->make(\OliveOil\Core\Model\Email\Template::class)
            ->setTemplate('dev/sysinfo.phtml')
            ->setI18n($i18n)
            ->setTo($adminEmail)
            ->setSysinfo($sysinfo)
            ->setLang($lang)
            ->setHive(print_r($this->getLiteHive(), true))
            ->setPhpinfo($phpinfo);
        $this->senderService->send($email);

        if ($error = $email->getError()) {
            $this->errnl($error);
            return self::RETURN_UNDEFINED_FAILURE;
        }

        $this->printnl("Done.");
        return self::RETURN_SUCCESS;
    }

    /**
     * @return array
     */
    public function getLiteHive() {
        return self::dumpRecursive($this->fw->hive());
    }

    /**
     * @param array|mixed $item
     * @param int $maxDepth
     * @param array $parentKeys
     * @return array|string|mixed
     */
    protected static function dumpRecursive($item, $maxDepth = 3, $parentKeys = []) {
        $isFilteredKey = in_array(implode('.', $parentKeys), self::FILTERED_HIVE_KEYS);

        if (is_array($item)) {
            if ($maxDepth === 0 || $isFilteredKey) {
                return sprintf('{array}(%d)', count($item));
            }

            foreach ($item as $k => $v) {
                $item[$k] = self::dumpRecursive($v, $maxDepth - 1, array_merge($parentKeys, [$k]));
            }
        } elseif (is_object($item)) {
            return sprintf('{object(%s)}', $item::class);
        }

        if ($isFilteredKey) {
            return '**HIDDEN**';
        }

        return $item;
    }
}
