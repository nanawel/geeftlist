<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Form\Element\ContactSubjectSelect;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Model\Security\Captcha;
use OliveOil\Core\Model\SessionInterface;
use function OliveOil\array_union_intersect;

class Contact extends AbstractController
{
    public const EMAIL_TEMPLATE_CONTACT = 'admin/contact/request.phtml';

    public const MESSAGE_MAX_LENGTH = 2000;

    /** @var string[] */
    protected array $nonRestrictedActions = ['index', 'post'];

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $emailFactory,
        protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreModelFactory,
        protected \OliveOil\Core\Service\Security\Captcha $captchaService,
        protected \OliveOil\Core\Service\Validation\Email $emailValidator
    ) {
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        $this->breadcrumbs->addCrumb('Contact', '*/*');

        return true;
    }

    public function indexAction(): void {
        $formData = $this->getFormDataSession()->getContactFormData() ?? [];

        $subjectSelect = $this->getSubjectFormElement()
            ->setValue($formData['subject'] ?? '');

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/post'))
            ->setData('FORM_DATA', $formData)
            ->setData('SUBJECT_SELECT', $subjectSelect)
            ->setData('SHOW_EMAIL_FIELD', !$this->geefterSession->isLoggedIn())
            ->setData('SHOW_CAPTCHA', !$this->geefterSession->isLoggedIn())
            ->getLayout()
            ->setBlockTemplate('content', 'contact/index.phtml')
            ->setBlockTemplate('form', 'contact/form.phtml');

        if ($this->isCaptchaEnabled()) {
            $captcha = $this->captchaService->generate();
            $this->getFormDataSession()->setContactFormCaptcha($captcha);

            $this->view
                ->setData('CAPTCHA', $this->getCaptchaFormElement($captcha));
        }
    }

    public function postAction(): void {
        $post = array_union_intersect($this->getPost(), [
            'subject',
            'custom_subject',
            'message',
            'requester_email',
            'captcha'
        ]);

        // Save all POST data but captcha
        $this->getFormDataSession()->setContactFormData(array_diff_key($post, ['captcha' => true]));

        if ($this->isCaptchaEnabled()) {
            if (!$captcha = $this->getFormDataSession()->getContactFormCaptcha()) {
                $this->getSession()->addErrorMessage('Invalid captcha. Please retry.');
                $this->reroute('*/index');
            }

            /** @var Captcha $captcha */
            $captchaFormElement = $this->getCaptchaFormElement($captcha);

            $captchaAnswer = $captchaFormElement->getAnswer($post);

            if (!$this->captchaService->validate($captcha, $captchaAnswer)) {
                $this->getSession()->addErrorMessage('Invalid captcha answer. Please retry.');
                $this->reroute('*/index');
            }
        }

        if ($errors = $this->validateContactFormPost($post)) {
            foreach ($errors as $e) {
                $this->getSession()->addErrorMessage($e);
            }

            $this->reroute('*/index');
        }

        try {
            /** @var Geefter|null $geefter */
            $geefter = $this->geefterSession->getGeefter();

            $email = $this->emailFactory->make(\OliveOil\Core\Model\Email\Template::class)
                ->setTo($this->appConfig->getValue('EMAIL_CONTACT'))
                ->setTemplate(self::EMAIL_TEMPLATE_CONTACT)
                ->setReplyTo($post['requester_email']);

            $email->setGeefter($geefter)
                ->setRequesterEmail($post['requester_email'])
                ->setContactSubject($post['subject'])
                ->setContactMessage($post['message'])
                ->setContactDate(date('c'));
            $this->senderService->send($email);

            $this->eventManager->trigger('send_contact_request', $this, [
                'requester_email' => $post['requester_email'],
                'geefter' => $geefter,
                'subject' => $post['subject']
            ]);
        }
        catch (\Throwable $throwable) {
            $this->getLogger()->critical($throwable);
            $this->error(
                'Oops, we have experienced an unexpected error. ' .
                'You might want to try again later or contact us at <a href="mailto:{0}">{1}</a> if the problem persists.',
                [
                    $this->appConfig->getValue('EMAIL_CONTACT'),
                    $this->appConfig->getValue('EMAIL_CONTACT')
                ],
                ['no_escape' => true]
            );
            $this->reroute('*/index');
        }

        $this->getFormDataSession()->unsContactFormData();
        $this->success("Message sent successfully! We'll get back to you shortly.")
            ->reroute('/');
    }

    protected function validateContactFormPost(array &$post): array {
        $errors = [];

        $post = \OliveOil\array_mask($post, [
            'subject',
            'custom_subject',
            'message',
            'requester_email'
        ]);

        if (!in_array($post['subject'], array_keys($this->getSubjectFormElement()->getOptions()))) {
            $errors[] = $this->__('Invalid field: {0}.', $this->__('subject'));
        }

        if (! isset($post['message']) || ! trim((string) $post['message'])) {
            $errors[] = $this->__('Missing field: {0}.', $this->__('message'));
        }
        elseif (mb_strlen((string) $post['message']) > self::MESSAGE_MAX_LENGTH) {
            $errors[] = $this->__('Message is too long!');
        }

        // Use current geefter email as requester_email if logged in
        if ($this->geefterSession->isLoggedIn()) {
            $post['requester_email'] = $this->geefterSession->getGeefter()->getEmail();
        } elseif (! isset($post['requester_email']) || ! trim((string) $post['requester_email'])) {
            $errors[] = $this->__('Missing field: {0}.', $this->__('email address'));
        } elseif (!$this->emailValidator->validate($post['requester_email'])) {
            $errors[] = $this->__('Invalid field: {0}.', $this->__('email address'));
        }

        return $errors;
    }

    /**
     * @return \Geeftlist\Form\Element\ContactSubjectSelect
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function getSubjectFormElement() {
        return $this->modelFactory
            ->make(ContactSubjectSelect::class, ['data' => [
                'name'       => 'subject',
                'id'         => 'subject',
                'with_empty' => false
            ]
        ]);
    }

    /**
     * @return \OliveOil\Core\Form\Element\Captcha
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function getCaptchaFormElement(Captcha $captchaModel) {
        return $this->coreModelFactory
            ->make(\OliveOil\Core\Form\Element\Captcha::class, ['data' => [
                'name'          => 'captcha',
                'id'            => 'captcha',
                'captcha_model' => $captchaModel
            ]
            ]);
    }

    protected function isCaptchaEnabled(): bool {
        if ($this->geefterSession->isLoggedIn()) {
            return (bool) $this->appConfig->getValue('CAPTCHA_CONTACT_GEEFTER_ENABLE');
        }

        return (bool) $this->appConfig->getValue('CAPTCHA_CONTACT_VISITOR_ENABLE');
    }

    /**
     * Return the instance of session used to store the data from the form in case of errors.
     *
     * @return SessionInterface
     */
    protected function getFormDataSession() {
        return $this->geefterSession->isLoggedIn()
            ? $this->geefterSession
            : $this->getSession();
    }
}
