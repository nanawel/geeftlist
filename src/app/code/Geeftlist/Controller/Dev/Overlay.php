<?php

namespace Geeftlist\Controller\Dev;


use Geeftlist\Controller\Http\AbstractController;

class Overlay extends AbstractController
{
    protected array $nonRestrictedActions = [
        'index',
        'long'
    ];

    public function indexAction() {
    }

    public function longAction(): void {
        $sleepDuration = ((int) $this->getRequestQuery('duration')) ?: 5;
        sleep($sleepDuration);

        $this->geefterSession->addInfoMessage(sprintf('Slept for %d seconds.', $sleepDuration));

        $this->reroute('*/index');
    }
}
