<?php

namespace Geeftlist\Controller\Dev;


use Geeftlist\Controller\Http\AbstractController;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Exception\AppException;
use OliveOil\Core\Exception\SystemException;

class Error extends AbstractController
{
    protected function init() {
        $this->nonRestrictedActions = ['*'];
    }

    public function indexAction(): void {
        if ($this->getRequestParam('forceError')) {
            throw new AppException('Force error loop to redirect to static 500 error page '
            . '(in production mode ONLY; in developer mode it should only be displayed on a clean page).');
        }

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => \OliveOil\Core\Block\Dialog::class,
                'dialog_type' => \OliveOil\Core\Block\Dialog::TYPE_QUESTION,
                'message' => 'Choose an error to trigger:',
                'buttons' => [
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/catchedError'),
                        'text'  => $this->__('Catched Error'),
                        'class' => 'button'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/nativeError'),
                        'text'  => $this->__('Native Error'),
                        'class' => 'button'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/appException'),
                        'text'  => $this->__('App Exception'),
                        'class' => 'button'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/systemException'),
                        'text'  => $this->__('System Exception'),
                        'class' => 'button'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/httpException'),
                        'text'  => $this->__('HTTP Exception'),
                        'class' => 'button'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/invalidRoute'),
                        'text'  => $this->__('HTTP Error'),
                        'class' => 'button'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl(
                            '*/*',
                            ['forceError' => 1, '_referer' => $this->getUrl('*/*', ['forceError' => 1])]
                        ),
                        'text'  => $this->__('Error loop'),
                        'class' => 'button'
                    ]
                ]
            ]);
    }

    public function catchedErrorAction(): void {
        try {
            throw new AppException('This is a catched error.');
        }
        catch (\Throwable $throwable) {
            $this->getLogger()->critical($throwable);
            $this->getLogger()->error('Custom log with attached exception', ['exception' => $throwable]);
        }

        $this->getSession()->addSuccessMessage('OK, error has been catched and logged.');
        $this->reroute('*/index');
    }

    public function nativeErrorAction(): void {
        assert(false);
    }

    public function appExceptionAction(): never {
        throw new AppException('This is a AppException message.');
    }

    public function systemExceptionAction(): never {
        throw new SystemException('This message should not be displayed IN PRODUCTION MODE (not visible to user).');
    }

    public function httpExceptionAction(): void {
        HttpStatus::getReasonException(HttpStatus::STATUS_NOT_FOUND);
    }
}
