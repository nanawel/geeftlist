<?php

namespace Geeftlist\Controller\Dev;


use Geeftlist\Controller\Http\AbstractController;

class Dialog extends AbstractController
{
    public function successAction(): void {
        $this->prepareLayout('success', 'dev/dialog/success.pmd');
    }

    public function questionAction(): void {
        $this->prepareLayout('question', 'dev/dialog/question.pmd');
    }

    public function errorAction(): void {
        $this->prepareLayout('error', 'dev/dialog/error.pmd');
    }

    protected function prepareLayout($type, $template) {
        $this->getSession()
            ->addInfoMessage('This is an info message.')
            ->addSuccessMessage('This is an success message.')
            ->addWarningMessage('This is a warning message.')
            ->addErrorMessage('This is an error message.')
            ->addInfoMessage(str_repeat('This is a very long info message.', 5))
        ;

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => \OliveOil\Core\Block\Dialog::class,
                'dialog_type' => $type,
                'content_blocks' => ['dialog_content'],
                'buttons' => $this->getButtons()
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => $template
            ]);
    }

    protected function getButtons(): array {
        return [
            [
                'type'  => 'a',
                'href'  => $this->getUrl('*/success'),
                'text'  => $this->__('Success'),
                'class' => 'button submit'
            ],
            [
                'type'  => 'a',
                'href'  => $this->getUrl('*/question'),
                'text'  => $this->__('Question'),
                'class' => 'button'
            ],
            [
                'type'  => 'a',
                'href'  => $this->getUrl('*/error'),
                'text'  => $this->__('Error'),
                'class' => 'button cancel'
            ]
        ];
    }
}
