<?php

namespace Geeftlist\Controller\Dev;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Narrowspark\HttpStatus\Exception\NotFoundException;

class Dump extends AbstractController
{
    public const FIND_DEFAULT_LIMIT = 100;

    protected array $nonRestrictedActions = [
        'geefters',
        'geeftees',
        'families',
    ];

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Service\Security $securityService
    ) {
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        if (!$this->appConfig->getValue('DEV.DUMP_ENTITIES_CONTROLLER_ENABLED')) {
            throw new NotFoundException();
        }

        return true;
    }

    public function geeftersAction(): void {
        $this->securityService->callPrivileged(function (): void {
            $items = $this->getModelRepository(Geefter::ENTITY_TYPE)->find([
                'sort_order' => ['username' => SORT_ASC],
                'limit' => $this->getFindLimit()
            ]);

            $this->renderResponse($this->itemsToArray($items));
        });
    }

    public function geefteesAction(): void {
        $this->securityService->callPrivileged(function (): void {
            $items = $this->getModelRepository(Geeftee::ENTITY_TYPE)->find([
                'sort_order' => ['name' => SORT_ASC],
                'limit' => $this->getFindLimit()
            ]);

            $this->renderResponse($this->itemsToArray($items));
        });
    }

    public function familiesAction(): void {
        $this->securityService->callPrivileged(function (): void {
            $items = $this->getModelRepository(Family::ENTITY_TYPE)->find([
                'sort_order' => ['name' => SORT_ASC],
                'limit' => $this->getFindLimit()
            ]);

            $this->renderResponse($this->itemsToArray($items));
        });
    }

    protected function getFindLimit(): int {
        return ((int) $this->getRequest()->getQueryParam('limit')) ?: self::FIND_DEFAULT_LIMIT;
    }

    /**
     * @return mixed[]
     */
    protected function itemsToArray(array $items): array {
        $itemsData = [];
        /** @var AbstractModel $item */
        foreach ($items as $k => $item) {
            $itemsData[$k] = $item->getData();
        }

        return $itemsData;
    }

    protected function renderResponse(array $responseData) {
        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $responseData,
                'params'   => ['prettyPrint' => true]
            ]);
    }
}
