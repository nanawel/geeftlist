<?php

namespace Geeftlist\Controller\Dev;


use Geeftlist\Controller\Http\AbstractController;

class Toast extends AbstractController
{
    protected array $nonRestrictedActions = [
        'index'
    ];

    public function indexAction(): void {
        $this->geefterSession->addInfoMessage('Some info message that should appear on load.');
    }
}
