<?php

namespace Geeftlist\Controller\Traits;

use Geeftlist\Model\Share\Constants;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Exception\MissingValueException;
use OliveOil\Core\Exception\NoSuchEntityException;

trait DiscussionTrait
{
    /**
     * @return array(Post, bool)
     * @throws NoSuchEntityException
     * @throws MissingValueException
     * @throws InvalidValueException
     */
    public function savePost(): array {
        $postRepository = $this->getModelRepository(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE);
        $isEdition = false;

        if ($postId = $this->getPost('post_id')) {
            /** @var \Geeftlist\Model\Discussion\Post $postModel */
            $postModel = $postRepository->get($postId);
            if (!$postModel) {
                throw new NoSuchEntityException('Invalid post ID.');
            }
        }
        else {
            /** @var \Geeftlist\Model\Discussion\Post $postModel */
            $postModel = $postRepository->newModel();
        }

        $formData = $this->filterPostFormPost($this->getPost());
        $postModel->addData($formData);

        $errors = $this->validatePostFormPost($postModel, $formData);
        if ($errors) {
            $this->setDiscussionPostFormValues($formData);

            foreach ($errors as $error) {
                $this->geefterSession->addErrorMessage($error);
            }

            throw new InvalidValueException($this->i18n->tr('{0} error(s) detected.', count($errors)));
        }

        $postRepository->save($postModel);

        $this->geefterSession->addSuccessMessage('Message posted successfully!');
        $this->clearDiscussionPostFormValues();

        return [$postModel, $isEdition];
    }

    /**
     * @throws NoSuchEntityException
     */
    public function editPost(): void {
        if (!$postId = $this->getRequestParam('post_id')) {
            $this->geefterSession->addErrorMessage('Missing post ID');
            $this->redirectReferer();
        }

        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $this->getPostModel();
        $this->assertCanEdit($post);

        $this->setDiscussionPostFormValues($post->getData());

        $this->view
            ->setData('POST', $post)
            ->setData('FORM_ACTION', $this->getUrl('*/save', ['_referer' => '_keep']))
            ->getLayout()
            ->setBlockTemplate('content', 'discussion/post/edit/index.phtml')
            ->setBlockConfig('toolbar', [
                'type'       => \Geeftlist\Block\Form\Toolbar::class,
                'delete_url' => $this->getUrl('*/delete', [
                    'post_id' => $postId,
                    '_referer' => '_keep',
                    '_csrf' => true
                ])
            ])
            ->setBlockConfig('form', [
                'template'    => 'discussion/post/form.phtml',
                'form_values' => $this->getDiscussionPostFormValues(),
                'standalone'  => false
            ]);
    }

    /**
     * @throws NoSuchEntityException
     */
    public function deletePost(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $this->getPostModel();
        $this->getModelRepository(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE)
            ->delete($post);

        $this->geefterSession->addSuccessMessage('Post deleted successfully.');
    }

    public function filterPostFormPost(array $formData): array {
        $allowedFields = [
            'title',
            'message',
            'post_id',
            'topic_id',
        ];
        if ($this->appConfig->getValue('GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION')) {
            $allowedFields[] = 'recipient_ids';
        }

        $formData = \OliveOil\array_mask($formData, $allowedFields);

        if (is_callable([$this, 'getPostFormElements'])) {
            foreach ($this->getPostFormElements($formData) ?? [] as $formElement) {
                $formElement->filter($formData);
            }
        }

        return $formData;
    }

    /**
     * @return mixed[]
     */
    protected function validatePostFormPost(\Geeftlist\Model\Discussion\Post $post, array &$formData) {
        return $this->dataValidationErrorProcessor->process(
            $post->validate(),
            $formData,
            [
                'message' => $this->i18n->tr('discussion_post.field||message'),
                'recipient_ids' => $this->i18n->tr('discussion_post.field||recipient_ids')
            ]
        );
    }

    /**
     * @return \Geeftlist\Model\Discussion\Post
     * @throws NoSuchEntityException
     */
    public function getPostModel() {
        if ($postId = $this->getRequestParam('post_id')) {
            /** @var \Geeftlist\Model\Discussion\Post|null $postModel */
            $postModel = $this->getModelRepository(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE)
                ->get($postId);

            if (! $postModel) {
                throw new NoSuchEntityException('This post does not exists.');
            }
        }
        else {
            throw new MissingValueException('Missing post ID.');
        }

        return $postModel;
    }

    protected function assertCanEdit($post = null) {
        if ($post === null) {
            $post = $this->getPostModel();
        }

        if (!$this->permissionService->isAllowed($post, Constants::ACTION_EDIT)) {
            $this->geefterSession->addErrorMessage('You are not allowed to edit this post.');
            $this->redirectReferer();
        }
    }

    public function setDiscussionPostFormValues(array $formData) {
        $this->geefterSession->setData($this->getDiscussionPostFormValuesSessionKey(), $formData);

        return $this;
    }

    public function getDiscussionPostFormValues() {
        return $this->geefterSession->getData($this->getDiscussionPostFormValuesSessionKey()) ?: [];
    }

    public function clearDiscussionPostFormValues() {
        $this->geefterSession->unsetData($this->getDiscussionPostFormValuesSessionKey());

        return $this;
    }

    protected function getDiscussionPostFormValuesSessionKey(): string {
        if ($topicId = $this->getRequest()->getRequestParam('topic_id')) {
            return sprintf('discussion_post_form_values_%d', $topicId);
        }

        // Fallback: return a unique key per request path to prevent hazardous override
        return 'discussion_post_form_values__' . \OliveOil\idfy($this->getRequest()->getRoutePath());
    }
}
