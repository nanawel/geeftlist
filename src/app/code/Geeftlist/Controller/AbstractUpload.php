<?php

namespace Geeftlist\Controller;


use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Controller\Http\Context;

abstract class AbstractUpload extends AbstractController
{
    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\UploadInterface $uploadService
    ) {
        parent::__construct($context);
    }

    abstract public function prepareAction(\Base $fw, array $args);

    public function getUploadService(): \OliveOil\Core\Service\UploadInterface {
        return $this->uploadService;
    }
}
