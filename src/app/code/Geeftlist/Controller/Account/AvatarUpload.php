<?php

namespace Geeftlist\Controller\Account;


use Geeftlist\Controller\AbstractUpload;
use Geeftlist\Controller\Http\Context;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

class AvatarUpload extends AbstractUpload
{
    public const FORM_INPUT_NAME = 'avatar_path';

    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \OliveOil\Core\Service\UploadInterface $uploadService,          // Not used here
        protected \Geeftlist\Service\Avatar\Geeftee $geefteeAvatarService
    ) {
        parent::__construct($context, $uploadService);
    }

    public function prepareAction(\Base $fw, array $args): void {
        /** @var ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->getCoreFactory()->make(ErrorAggregatorInterface::class);

        try {
            $file = $this->getGeefteeAvatarService()->upload(
                $this->geefterSession->getGeeftee(),
                self::FORM_INPUT_NAME,
                $errorAggregator
            );

            $data = [
                'status' => 'success',
                'messages' => [$this->__('Image uploaded successfully.')],
                'imageId' => $file->getData('avatar_path'),
                'imageUrl' => $this->getGeefteeAvatarService()->getUrl($this->geefterSession->getGeeftee())
            ];
        }
        catch (\Throwable $throwable) {
            $messages = [];

            // Regular errors, that can be shown to the user
            foreach ($errorAggregator->getFieldErrors(self::FORM_INPUT_NAME) as $error) {
                $messages[] = $this->__($error->getLocalizableMessage());
            }

            // Unexpected exception, add generic message for user and log error
            if ($messages === []) {
                $this->getLogger()->error($throwable, ['exception' => $throwable]);
                $messages[] = $this->__('Unknown error on upload. Please try again later.');
            }

            $data = [
                'status' => 'failure',
                'messages' => $messages,
            ];
        }

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $data
            ]);
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getGeefteeAvatarService(): \Geeftlist\Service\Avatar\Geeftee {
        return $this->geefteeAvatarService;
    }
}
