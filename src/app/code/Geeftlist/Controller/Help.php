<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;

class Help extends AbstractController
{
    /** @inheritdoc */
    protected array $nonRestrictedActions = ['index'];

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb('Help');

        $this->view->getLayout()
            ->setBlockConfig('content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'help/index.pmd'
            ]);
    }
}
