<?php

namespace Geeftlist\Controller\Geeftee\Manage;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Geeftee\ClaimRequestException;

class ClaimRequest extends AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Helper\Geeftee\ClaimRequest $claimRequestHelper,
        protected \Geeftlist\View\Grid\Geeftee\ClaimRequest $claimRequestGrid
    ) {
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        $this->breadcrumbs->addCrumb(
            $this->__('Manage {0}', $this->__('Geeftee Claim Requests')),
            '*/index'
        );

        return true;
    }

    public function indexAction(): void {
        $grid = $this->getClaimRequestGrid();

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/post'))
            ->getLayout()
            ->setBlockTemplate('content', 'geeftee/manage/claimrequests/index.phtml')
            ->setBlockTemplate('toolbar', 'geeftee/manage/claimrequests/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    public function gridAction(): void {
        $grid = $this->getClaimRequestGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    public function postAcceptAction(): void {
        $this->setPost('action', 'accept');

        $this->postAction();
    }

    public function postRejectAction(): void {
        $this->setPost('action', 'reject');

        $this->postAction();
    }

    public function postCancelAction(): void {
        $this->setPost('action', 'cancel');

        $this->postAction();
    }

    public function postAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $action = $this->getPost('action');
        if (! $action || ! in_array($action, ['accept', 'reject', 'cancel'])) {
            $this->geefterSession->addErrorMessage('Invalid action.');
            $this->redirectReferer();
        }

        if ($claimRequestId = $this->getRequestParam('request_id')) {
            $claimRequestIds = [$claimRequestId];
        }
        elseif ($claimRequestIds = $this->getPost('selected_grid_rows')) {
            if (! is_array($claimRequestIds)) {
                $claimRequestIds = false;
            }
        }

        if (! $claimRequestIds) {
            $this->geefterSession->addErrorMessage('Please select requests first.');
            $this->redirectReferer();
        }

        $claimRequests = $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Geeftee\ClaimRequest::ENTITY_TYPE)
            ->addFieldToFilter('claim_request_id', ['in' => $claimRequestIds]);

        $success = 0;
        $errors = [];
        foreach ($claimRequests as $cr) {
            try {
                switch ($action) {
                    case 'accept':
                        $this->getClaimRequestHelper()->acceptRequest($cr);
                        break;
                    case 'reject':
                        $this->getClaimRequestHelper()->rejectRequest($cr);
                        break;
                    case 'cancel':
                        $this->getClaimRequestHelper()->cancelRequest($cr);
                        break;
                }

                ++$success;
            }
            catch (ClaimRequestException $e) {
                $this->getLogger()->critical($e);
                $errors[] = $e;
            }
        }

        if ($success !== 0) {
            $this->geefterSession->addSuccessMessage('{0} request(s) processed successfully.', [$success]);
        }

        foreach ($errors as $e) {
            $this->geefterSession->addErrorMessage($e->getMessage());
        }

        $this->redirectReferer();
    }

    public function getClaimRequestHelper(): \Geeftlist\Helper\Geeftee\ClaimRequest {
        return $this->claimRequestHelper;
    }

    public function getClaimRequestGrid(): \Geeftlist\View\Grid\Geeftee\ClaimRequest {
        $grid = $this->claimRequestGrid;

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'ajax_url' => $this->getUrl('*/grid', ['_referer' => true]),
                'current_geefter' => $this->geefterSession->getGeefter(),
                'has_selectable_rows' => true
            ]);

            $grid->attachInstanceListener('collection::new', static function ($target, array $args): void {
                $args['collection']->addFieldToFilter(
                    'main_table.decision_code',
                    \Geeftlist\Model\Geeftee\ClaimRequest::DECISION_CODE_PENDING
                );
            });
        }

        return $grid;
    }
}
