<?php

namespace Geeftlist\Controller\Geeftee;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Form\Element\FamilySelect;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use OliveOil\Core\Block\Dialog;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Manage extends AbstractController
{
    protected \Geeftlist\Service\PermissionInterface $permissionService;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\Validation\Email $emailValidator,
        protected \Geeftlist\Helper\Family\Membership $membershipHelper,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreModelFactory
    ) {
        parent::__construct($context);
        $this->permissionService = $context->getPermissionService();
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        $this->breadcrumbs->addCrumb(
            $this->__('Manage {0}', 'Geeftees'),
            '*/search'
        );

        if (($geeftee = $this->getGeeftee()) && $geeftee->getId()) {
            $this->breadcrumbs->addCrumb($geeftee->getName(), '*/*/*');
        }

        return true;
    }

    public function searchAction(): void {
        // Clear edit form data if any
        $this->getSession()->unsEditGeefteeFormData();

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/searchPost', ['_query' => '*']))
            ->setData('SKIP_STEP_URL', $this->getUrl('*/new', ['_query' => '*']))
            ->getLayout()
            ->setBlockTemplate('content', 'geeftee/search.phtml')
            ->setBlockTemplate('form', 'geeftee/search/form.phtml')
            ->setBlockConfig('why_email', [
                'type'        => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template'    => 'geeftee/why_email.pmd',
                'css_classes' => ['notice-text']
            ]);
    }

    public function searchPostAction(): void {
        $post = $this->getPost();

        if (! isset($post['email']) || (trim((string) $post['email']) === '' || trim((string) $post['email']) === '0')) {
            $this->geefterSession->addErrorMessage('Missing field: {0}.', $this->__('email'));
            $this->redirectReferer();
        }

        /** @var \Geeftlist\Model\Geeftee|null $geeftee */
        $geeftee = $this->getModelRepository(Geeftee::ENTITY_TYPE)->get($post['email'], 'email');
        if ($geeftee) {
            $this->geefterSession->addWarningMessage('A geeftee with this email already exists!');

            if ($familyId = $this->urlBuilder->getRequestQuery('family_id')) {
                /** @var \Geeftlist\Model\Family|null $family */
                $family = $this->getModelRepository(Family::ENTITY_TYPE)
                    ->get($familyId);

                if ($family) {
                    if ($family->isMember($geeftee)) {
                        $this->geefterSession->addWarningMessage(
                            'This geeftee is already a member of family <strong>{0}</strong>!',
                            [$this->escapeHtml($family->getName())],
                            ['no_escape' => true]
                        );
                    }
                    else {
                        $this->geefterSession->addWarningMessage(
                            '<a href="{0}">Click here</a> to invite him/her to the family.',
                            [
                                $this->getUrl(
                                    'family_manage_invite/confirm',
                                    [
                                        'family_id'  => $familyId,
                                        'geeftee_id' => $geeftee->getId(),
                                        '_csrf'      => true
                                    ]
                                )
                            ],
                            ['no_escape' => true]
                        );
                    }
                }
            }

            $this->redirectReferer();
        }

        $this->geefterSession->addSuccessMessage('Good! This email does not match any existing geeftee, you may proceed.');
        $this->reroute('*/new', ['_query' => [
            'email'     => $post['email'],
            'family_id' => $this->urlBuilder->getRequestQuery('family_id'),
            'referer'   => $this->urlBuilder->getRequestQuery('referer')
        ]]);
    }

    public function editAction(): void {
        $geeftee = $this->getGeeftee([TypeInterface::ACTION_VIEW, TypeInterface::ACTION_EDIT]);
        $isUpdate = (bool) $geeftee->getId();

        // Redirect to account if trying to edit the current geeftee
        if ($geeftee->getId() == $this->geefterSession->getGeeftee()->getId()) {
            $this->reroute('account/index');
        }

        $formActionParams = ['_query' => '*'];
        // Update
        if ($isUpdate) {
            $formData = [
                'name'     => $geeftee->getName(),
                'email'    => $geeftee->getEmail(),
                'no_email' => $geeftee->getEmail() === null
            ];
            $formActionParams += ['geeftee_id' => $geeftee->getId()];
        }
        // Creation
        else {
            $email = (string) $this->urlBuilder->getRequestQuery('email');
            $formData = $this->getSession()->getEditGeefteeFormData() ?: [];
            if (empty($formData['email']) && ($email !== '' && $email !== '0')) {
                $formData['email'] = $email;
            }

            if (! array_key_exists('no_email', $formData) && empty($formData['email'])) {
                $formData['no_email'] = $this->getActionName() != 'new';
            }

            if (
                ! isset($formData['family_id'])
                && ($familyId = (int) $this->urlBuilder->getRequestQuery('family_id'))
            ) {
                $formData['family_id'] = $familyId;
            }
        }

        if ($isUpdate) {
            // Family membership change on update is not supported
            $familySelect = false;
        }
        else {
            $familySelect = $this->modelFactory->make(FamilySelect::class, ['data' => [
                'name'  => 'family_id',
                'id'    => 'family_id',
                'value' => $formData['family_id'] ?? ''
            ]]);
        }

        $this->view
            ->setData('FORM_DATA', $formData)
            ->setData('FORM_ACTION', $this->getUrl('*/save', $formActionParams))
            ->setData('GEEFTEE', $geeftee)
            ->setData('FAMILY_SELECT', $familySelect)
            ->getLayout()
            ->setBlockTemplate('content', 'geeftee/manage/edit.phtml')
            ->setBlockTemplate('form', 'geeftee/manage/edit/form.phtml')
            ->setBlockConfig('why_email', [
                'type' => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'geeftee/why_email.pmd',
                'css_classes' => ['notice-text']
            ]);
    }

    public function saveAction(): void {
        $geeftee = $this->getGeeftee([TypeInterface::ACTION_VIEW, TypeInterface::ACTION_EDIT]);
        $isUpdate = (bool) $geeftee->getId();

        // Prevent current geeftee self-edit here
        if ($geeftee->getId() == $this->geefterSession->getGeeftee()->getId()) {
            $this->geefterSession->addErrorMessage('Cannot do that. Use the account page instead.');
            $this->reroute('account/index');
        }

        $post = $this->getPost();
        $post = $this->filterGeefteeFormPost($post);

        $this->getSession()->setEditGeefteeFormData($post);

        $geeftee->addData($post);
        $errors = $this->validateGeefteeFormPost($post, $geeftee);
        if ($errors !== []) {
            foreach ($errors as $e) {
                $this->geefterSession->addErrorMessage($e);
            }

            $this->redirectReferer();
        }

        $this->getModelRepository(Geeftee::ENTITY_TYPE)->save($geeftee);

        if ($isUpdate) {
            $this->geefterSession->addSuccessMessage(
                'Geeftee <strong>{0}</strong> updated successfully!',
                [$this->escapeHtml($geeftee->getName())],
                ['no_escape' => true]
            );
        }
        else {
            $this->geefterSession->addSuccessMessage(
                'Geeftee <strong>{0}</strong> created successfully!',
                [$this->escapeHtml($geeftee->getName())],
                ['no_escape' => true]
            );

            /** @var Family $family */
            $family = $this->getModelRepository(Family::ENTITY_TYPE)
                ->get($post['family_id']);
            $this->membershipHelper->request($family, $geeftee);
        }

        $this->getSession()->unsEditGeefteeFormData();
        $this->redirectReferer();
    }

    /**
     * @return mixed[]
     */
    protected function filterGeefteeFormPost($geefteeData): array {
        $geefteeData = array_map('trim', \OliveOil\array_mask($geefteeData, [
            'name',
            'email',
            'no_email',
            'family_id'
        ]));

        if ($geefteeData['no_email'] ?? false) {
            unset($geefteeData['email']);
        }

        return $geefteeData;
    }

    protected function validateGeefteeFormPost(&$geefteeData, Geeftee $geeftee): array {
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->coreModelFactory->make(ErrorAggregatorInterface::class);
        if (! $geeftee->getId() && trim($geefteeData['family_id'] ?? '') === '') {
            $errorAggregator->addFieldError(
                $this->coreModelFactory->make(ErrorInterface::class)
                    ->setField('family_id')
                    ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                    ->setMessage('Please select a family.')
            );
        }

        $errorAggregator->merge($geeftee->validate());

        return $this->dataValidationErrorProcessor->process(
            $errorAggregator,
            $geefteeData,
            [
                'name'  => $this->i18n->tr('geeftee.field||name'),
                'email' => $this->i18n->tr('geeftee.field||email'),
            ]
        );
    }

    public function confirmDeleteAction(): void {
        $geeftee = $this->getGeeftee([TypeInterface::ACTION_VIEW, TypeInterface::ACTION_DELETE]);

        $this->view
            ->setData('GEEFTEE', $geeftee)
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_QUESTION,
                'message' => $this->__('Do you want to delete this geeftee?'),
                'content_blocks' => ['dialog_content', 'deletion_warning'],
                'buttons' => [
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/delete', [
                            'geeftee_id' => $geeftee->getId(),
                            '_csrf' => true,
                            '_referer' => '_keep'
                        ]),
                        'text'  => $this->__('Yes'),
                        'class' => 'button yes'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->urlBuilder->getQueryReferer() ?: $this->getUrl('dashboard'),
                        'text'  => $this->__('No'),
                        'class' => 'button no'
                    ]
                ]
            ])
            ->setBlockConfig('dialog_content', [
                'type'                => \Geeftlist\Block\Geeftee::class,
                'template'            => 'geeftee/card.phtml',
                'show_profile_link'   => false,
                'show_action_buttons' => false,
            ])
            ->setBlockConfig('deletion_warning', [
                'type'                => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template'            => 'geeftee/deletion_warning.pmd',
            ])
        ;
    }

    public function deleteAction(): void {
        $geeftee = $this->getGeeftee([TypeInterface::ACTION_VIEW, TypeInterface::ACTION_DELETE]);

        // Token is required to access this action
        $this->checkCsrf(['GET']);

        $geefteeRepository = $this->getModelRepository($geeftee->getEntityType());
        $geefteeRepository->delete($geeftee);

        $this->geefterSession->addSuccessMessage('Geeftee deleted successfully!');

        $this->redirectReferer();
    }

    /**
     * @param string|string[] $requiredActions
     * @return Geeftee
     */
    public function getGeeftee($requiredActions = TypeInterface::ACTION_VIEW) {
        $geefteeRepository = $this->getModelRepository(Geeftee::ENTITY_TYPE);
        if ($geefteeId = $this->getRequestParam('geeftee_id')) {
            try {
                /** @var Geeftee $geeftee */
                $geeftee = $geefteeRepository->get($geefteeId);
            }
            catch (PermissionException) {
                // ignore, see below
            }

            if (
                !isset($geeftee)
                || !$this->permissionService->isAllowed($geeftee, $requiredActions)
            ) {
                $this->geefterSession->addErrorMessage('Invalid or unknown geeftee!');
                $this->redirectReferer();
            }
        }
        else {
            /** @var Geeftee $geeftee */
            $geeftee = $geefteeRepository->newModel();
        }

        return $geeftee;
    }
}
