<?php

namespace Geeftlist\Controller\Geeftee;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Geeftee\ClaimRequest\AlreadyExistsException;
use Geeftlist\Exception\SecurityException;
use Geeftlist\Model\Geeftee;
use OliveOil\Core\Block\Dialog;
use OliveOil\Core\Exception\NoSuchEntityException;

class ClaimRequest extends AbstractController
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefteeRepository;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Helper\Geeftee\ClaimRequest $claimRequestHelper
    ) {
        $this->geefteeRepository = $context->getModelRepositoryFactory()->resolveGet(Geeftee::ENTITY_TYPE);
        parent::__construct($context);
    }

    public function createAction(): void {
        $geeftee = $this->getGeeftee();

        $this->view
            ->setData('GEEFTEE', $geeftee)
            ->setData(
                'CREATOR_IS_CURRENT_GEEFTER',
                $geeftee->getCreatorId() == $this->geefterSession->getGeefterId()
            )
            ->getLayout()
            ->setBlockTemplate('content', 'geeftee/claim/create.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type'    => Dialog::TYPE_QUESTION,
                'content_blocks' => ['dialog_content'],
                'form_action'    => $this->getUrl('*/createPost/*', ['_referer' => '_keep']),
                'buttons'        => [
                    [
                        'type'  => 'button',
                        'href'  => $this->getUrl('*/success'),
                        'text'  => $this->__('OK'),
                        'class' => 'button submit',
                        'name' => 'confirmation',
                        'value' => 'ok'
                    ],
                    [
                        'type'  => 'button',
                        'href'  => $this->getUrl('*/question'),
                        'text'  => $this->__('Cancel'),
                        'class' => 'button cancel',
                        'name' => 'confirmation',
                        'value' => 'cancel'
                    ]
                ]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'geeftee/claim/create.pmd'
            ])
            ->setBlockConfig('card', [
                'type'                => \Geeftlist\Block\Geeftee::class,
                'template'            => 'geeftee/card.phtml',
                'show_claim_link'     => false,
                'show_action_buttons' => false
            ]);
    }

    public function createPostAction(): void {
        if ($this->getPost('confirmation') != 'ok') {
            $this->geefterSession->addInfoMessage('Request cancelled.');
            $this->redirectReferer();
        }

        $geeftee = $this->getGeeftee();
        $geefter = $this->geefterSession->getGeefter();

        try {
            $request = $this->getClaimRequestHelper()->request($geeftee, $geefter);

            // Creator is also the current geefter: accept request immediately
            if ($geeftee->getCreatorId() == $this->geefterSession->getGeefterId()) {
                $this->getClaimRequestHelper()->acceptRequest($request);
                $this->geefterSession->addSuccessMessage('Request processed successfully.');

                // Force rerouting to dashboard in case the referer was the old geeftee's profile page
                $this->reroute('/');
            } else {
                $this->geefterSession->addSuccessMessage(
                    'Your request has been saved successfully. <a href="{0}">{1}</a> will be notified.',
                    [$geeftee->getCreator()->getProfileUrl(), $this->escapeHtml($geeftee->getCreator()->getUsername())],
                    ['no_escape' => true]
                );
                $this->redirectReferer();
            }
        }
        catch (AlreadyExistsException) {
            $this->geefterSession->addErrorMessage('You already have a pending claim request for this geeftee.');
            $this->redirectReferer();
        }
    }

    /**
     * @return \Geeftlist\Model\Geeftee|null
     * @throws NoSuchEntityException
     * @throws SecurityException
     */
    public function getGeeftee() {
        if ($geefteeId = $this->getRequestParam('geeftee_id')) {
            /** @var Geeftee $geeftee */
            $geeftee = $this->getGeefteeRepository()->get($geefteeId);
        }

        if (!isset($geeftee)) {
            throw new NoSuchEntityException('Invalid or unknown geeftee!');
        }

        if ($geeftee->getGeefterId()) {
            throw new SecurityException('Invalid or unknown geeftee!');
        }

        return $geeftee;
    }

    public function getClaimRequestHelper(): \Geeftlist\Helper\Geeftee\ClaimRequest {
        return $this->claimRequestHelper;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefteeRepository() {
        return $this->geefteeRepository;
    }
}
