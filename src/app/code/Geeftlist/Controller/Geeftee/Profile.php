<?php

namespace Geeftlist\Controller\Geeftee;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geeftee\ClaimRequest;
use Geeftlist\Model\Gift\Field\Status;
use OliveOil\Core\Exception\NoSuchEntityException;

class Profile extends AbstractController
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefteeRepository;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context
    ) {
        $this->geefteeRepository = $context->getModelRepositoryFactory()->getFromCode(Geeftee::ENTITY_TYPE);
        parent::__construct($context);
    }

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb(
            $this->__("{0}'s Profile", $this->getGeeftee()->getName()),
            '*/*/*'
        );

        $gifts = $this->getGeeftee()->getGiftCollection()
            ->addFieldToFilter('status', Status::AVAILABLE)
            ->orderBy('label', SORT_ASC);

        $claimRequestCollection = $this->getGeeftee()->getClaimRequestCollection()
            ->addFieldToFilter('geefter_id', $this->geefterSession->getGeefter()->getId())
            ->addFieldToFilter('decision_code', ClaimRequest::DECISION_CODE_PENDING);
        $claimRequest = $claimRequestCollection->getFirstItem();

        $this->view
            ->setData('GEEFTEE', $this->getGeeftee())
            ->setData('GEEFTEES', [$this->getGeeftee()])
            ->setData('GIFTS', $gifts)
            ->getLayout()
            ->setBlockTemplate('content', 'geeftee/profile.phtml')
            ->setBlockConfig('card', [
                'type'               => \Geeftlist\Block\Geeftee::class,
                'template'           => 'geeftee/card.phtml',
                'claim_request'      => $claimRequest
            ])
            ->setBlockConfig('geeftee_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml'
            ])
            ->setBlockTemplate('gifts', 'geeftee/profile/gifts.phtml')
            ->setBlockConfig('gift_list', [
                'template'    => 'common/gift/list.phtml',
                'show_header' => false
            ])
            ->setBlockTemplate('gift_list_header', 'common/gift/list/header.phtml')
            ->setBlockConfig('gift', [
                'type'                 => \Geeftlist\Block\Gift::class,
                'template'             => 'common/gift/list/item.phtml',
                'display_reservations' => true,
                'badges_block_name'    => 'gift_badges'
            ])
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small']
            ])
            ->setBlockConfig('gift_actions', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/actions.phtml'
            ])
            ->setBlockConfig('gift_reservations', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/reservations.phtml'
            ])
        ;
    }

    public function getGeeftee() {
        if ($geefteeId = $this->getRequestParam('geeftee_id')) {
            /** @var Geeftee|null $geeftee */
            $geeftee = $this->geefteeRepository->get($geefteeId);
            if (!isset($geeftee)) {
                throw new NoSuchEntityException('Invalid or unknown geeftee!');
            }
        }
        elseif ($geefterId = $this->getRequestParam('geefter_id')) {
            /** @var Geeftee|null $geeftee */
            $geeftee = $this->geefteeRepository->get($geefterId, 'geefter_id');
            if (!isset($geeftee)) {
                throw new NoSuchEntityException('Invalid or unknown geeftee!');
            }
        }

        return $geeftee ?? $this->geefterSession->getGeeftee();
    }
}
