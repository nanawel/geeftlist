<?php

namespace Geeftlist\Controller\Api\Rest\Service\Token;


use Geeftlist\Controller\Api\Rest\AbstractController;
use Geeftlist\Controller\Api\Rest\Traits\PostVerb;
use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\Di\Exception;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

class Jwt extends AbstractController
{
    use PostVerb;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefterRepository;

    protected array $nonRestrictedActions = [Http::VERB_POST];

    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $allAuthenticationMethodFactory
    ) {
        $this->geefterRepository = $context->getModelRepositoryFactory()->resolveGet(Geefter::ENTITY_TYPE);
        parent::__construct($context);
    }

    protected function handlePost(RequestInterface $request, ResponseInterface $response) {
        $data = $request->getData();

        try {
            if (!isset($data['method']) || empty($data['method']) || !is_string($data['method'])) {
                throw new AuthenticationException('Missing or invalid authentication method.');
            }

            try {
                $authMethod = $this->getAllAuthenticationMethodFactory()->getFromCode($data['method']);
            }
            catch (Exception) {
                throw new AuthenticationException('Unsupported authentication method: ' . $data['method']);
            }
        }
        catch (\Throwable $throwable) {
            throw new BadRequestException($throwable->getMessage(), $throwable);
        }

        try {
            /** @var \Geeftlist\Model\Geefter $geefter */
            $geefter = $authMethod->authenticate($data);
        }
        catch (\InvalidArgumentException $e) {
            throw new BadRequestException($e->getMessage(), $e);
        }
        catch (\Throwable $e) {
            throw new UnauthorizedException($e->getMessage(), $e);
        }

        if (!$geefter->getApiKey()) {
            $geefter->regenerateApiKey();
        }

        $this->geefterSession->setGeefterAsLoggedIn($geefter);

        $this->getGeefterRepository()->save($geefter);

        $response->setData($this->getJsonApiEncoder()
            ->withMeta(['api_key' => $geefter->getApiKey()])
            ->encodeData($geefter));
    }

    public function getAllAuthenticationMethodFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->allAuthenticationMethodFactory;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefterRepository() {
        return $this->geefterRepository;
    }
}
