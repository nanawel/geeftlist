<?php
namespace Geeftlist\Controller\Api\Rest\Service\System;

use Geeftlist\Controller\Api\Rest\AbstractController;
use Geeftlist\Controller\Api\Rest\Traits\GetVerb;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

class Ping extends AbstractController
{
    use GetVerb;

    protected array $nonRestrictedActions = [Http::VERB_GET];

    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context,
        protected \OliveOil\Core\Service\System $systemService
    ) {
        parent::__construct($context);
    }

    public function handleGet(RequestInterface $request, ResponseInterface $response): void {
        $body = ($geefter = $this->geefterSession->getGeefter()) ? sprintf('pong %s!', $geefter->getUsername()) : 'pong!';

        $response->setHeaders(['Content-Type' => 'text/plain'])
            ->setData($body);
    }

    public function getSystemService(): \OliveOil\Core\Service\System {
        return $this->systemService;
    }
}
