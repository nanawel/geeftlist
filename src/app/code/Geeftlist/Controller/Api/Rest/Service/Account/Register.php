<?php

namespace Geeftlist\Controller\Api\Rest\Service\Account;


use Geeftlist\Controller\Api\Rest\AbstractController;
use Geeftlist\Controller\Api\Rest\Traits\PostVerb;
use Geeftlist\Exception\Geefter\RegistrationDisabledException;
use Geeftlist\Model\Geefter;
use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

class Register extends AbstractController
{
    use PostVerb;

    public const META_REGISTRATION_STATUS_CONFIRMATION_REQUIRED = 'confirmation-required';

    protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor;

    protected array $nonRestrictedActions = [Http::VERB_POST];

    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        protected \Geeftlist\Service\Geefter\Account $geefterAccountService
    ) {
        $this->dataValidationErrorProcessor = $context->getDataValidationErrorProcessor();
        parent::__construct($context);
    }

    protected function handlePost(RequestInterface $request, ResponseInterface $response) {
        $data = $request->getData();

        $this->waitForSensitiveActionsDelay(3);

        try {
            /** @var Geefter $geefter */
            /** @var ErrorAggregatorInterface $errorAggregator */
            [$geefter, $errorAggregator] = $this->getGeefterAccountService()->signup($data);
        }
        catch (RegistrationDisabledException $registrationDisabledException) {
            throw new ServiceUnavailableException($registrationDisabledException->getMessage(), $registrationDisabledException);
        }

        if ($errors = $this->dataValidationErrorProcessor->process($errorAggregator, $data)) {
            $response->setData($this->getJsonApiEncoder()->encodeErrors($errors))
                ->setCode(HttpStatus::STATUS_BAD_REQUEST);
        }
        else {
            $meta = [
                'messages' => ['Account created successfully.']
            ];
            if ($geefter->getFlag(\Geeftlist\Service\Geefter\Account::SIGNUP_SUCCESS_CONFIRMATION_REQUIRED)) {
                $meta['messages'][] = 'Confirmation email has been sent.';
                $meta['registration-status'] = self::META_REGISTRATION_STATUS_CONFIRMATION_REQUIRED;
            }

            $response->setData(
                $this->getJsonApiEncoder()
                    ->withMeta($meta)
                    ->encodeData($geefter)
            );
        }
    }

    public function getGeefterRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefterRepository;
    }

    public function getGeefterAccountService(): \Geeftlist\Service\Geefter\Account {
        return $this->geefterAccountService;
    }

    public function getDataValidationErrorProcessor(): \OliveOil\Core\Helper\Data\Validation\ErrorProcessor {
        return $this->dataValidationErrorProcessor;
    }
}
