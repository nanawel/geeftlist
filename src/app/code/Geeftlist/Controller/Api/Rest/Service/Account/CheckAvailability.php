<?php

namespace Geeftlist\Controller\Api\Rest\Service\Account;


use Geeftlist\Controller\Api\Rest\AbstractController;
use Geeftlist\Controller\Api\Rest\Traits\PostVerb;
use Geeftlist\Exception\Geefter\RegistrationDisabledException;
use Geeftlist\Model\Geefter;
use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

class CheckAvailability extends AbstractController
{
    use PostVerb;

    public const META_REGISTRATION_STATUS_OK_AVAILABLE = 'available';

    public const META_REGISTRATION_STATUS_NOT_AVAILABLE = 'not-available';

    protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor;

    protected array $nonRestrictedActions = [Http::VERB_POST];

    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context,
        protected \Geeftlist\Service\Geefter\Account $geefterAccountService
    ) {
        $this->dataValidationErrorProcessor = $context->getDataValidationErrorProcessor();
        parent::__construct($context);
    }

    protected function handlePost(RequestInterface $request, ResponseInterface $response) {
        $data = $request->getData();

        $this->waitForSensitiveActionsDelay(3);

        try {
            /** @var Geefter $geefter */
            /** @var ErrorAggregatorInterface $errorAggregator */
            [$geefter, $errorAggregator] = $this->getGeefterAccountService()->signup($data, null, true);
        }
        catch (RegistrationDisabledException $registrationDisabledException) {
            throw new ServiceUnavailableException($registrationDisabledException->getMessage(), $registrationDisabledException);
        }

        if ($errors = $this->dataValidationErrorProcessor->process($errorAggregator, $data)) {
            $response->setData(
                $this->getJsonApiEncoder()
                    ->withMeta(['registration-status' => self::META_REGISTRATION_STATUS_NOT_AVAILABLE])
                    ->encodeErrors($errors)
            );
        }
        else {
            $response->setData(
                $this->getJsonApiEncoder()
                    ->encodeMeta([
                        'messages' => ['OK, username and email are available.'],
                        'registration-status' => self::META_REGISTRATION_STATUS_OK_AVAILABLE
                    ])
            );
        }
    }

    public function getDataValidationErrorProcessor(): \OliveOil\Core\Helper\Data\Validation\ErrorProcessor {
        return $this->dataValidationErrorProcessor;
    }

    public function getGeefterAccountService(): \Geeftlist\Service\Geefter\Account {
        return $this->geefterAccountService;
    }
}
