<?php

namespace Geeftlist\Controller\Api\Rest\Gift;

use Geeftlist\Controller\Api\Rest\AbstractModelController;
use Geeftlist\Controller\Api\Rest\Traits\EntityRelationships;

class Relationships extends AbstractModelController
{
    use EntityRelationships;
}
