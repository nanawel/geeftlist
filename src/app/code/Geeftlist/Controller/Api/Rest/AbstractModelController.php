<?php

namespace Geeftlist\Controller\Api\Rest;

/**
 * @see {GITLAB}/wikis/feature/rest_api
 */
class AbstractModelController extends AbstractController
{
    /**
     * @param string $entityType
     */
    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context,
        protected string $entityType
    ) {
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getEntityType() {
        if (! $this->entityType) {
            throw new \UnexpectedValueException('Missing entity type.');
        }

        return $this->entityType;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @throws \Narrowspark\HttpStatus\Contract\Exception\HttpException
     */
    public function __call(string $name, array $arguments) {
        if (in_array(strtoupper($name), $this->getAllowedVerbs())) {
            $this->callWithHttpErrorWrapper(function () use ($name): void {
                $request = $this->getRequest();
                $response = $this->getResponse();

                /**
                 * If a method named "handle{Name}" exists, call it instead of the default REST helper
                 * @see \Geeftlist\Controller\Api\Rest\Traits\EntityRelationships::dispatchRelationshipsAction()
                 **/
                $handlerMethod = sprintf('handle%s', \OliveOil\snakecase2camelcase($name));
                if (method_exists($this, $handlerMethod)) {
                    $this->{$handlerMethod}($request, $response);
                }
                else {
                    $this->getRestService()->{$name}($request, $response);
                }
            });
        }
        else {
            parent::__call($name, $arguments);
        }
    }

    /**
     * @override
     * @return string[]
     */
    protected function getAllowedVerbs(array $params = []): array {
        return $this->getRestService()->getSupportedVerbs($params);
    }

    /**
     * @param string|null $entityType
     * @return \OliveOil\Core\Service\Rest\RestInterface
     */
    public function getRestService($entityType = null) {
        if ($entityType === null) {
            $entityType = $this->getEntityType();
        }

        return $this->restServiceFactory->getFromCode($entityType);
    }
}
