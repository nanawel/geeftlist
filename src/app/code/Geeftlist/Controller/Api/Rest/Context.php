<?php

namespace Geeftlist\Controller\Api\Rest;


class Context extends \Geeftlist\Controller\Api\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Model\Api\ConfigInterface $apiConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \Geeftlist\Service\Url\Builder\Api $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        \OliveOil\Core\Service\Log $logService,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \OliveOil\Core\Helper\Data\Factory $helperDataFactory,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        \OliveOil\Core\Service\GenericFactoryInterface $authenticationMethodFactory,
        \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor,
        \OliveOil\Core\Service\Http\ResponseWriterInterface $responseWriter,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \OliveOil\Core\Service\GenericFactoryInterface $restServiceFactory,
        protected \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface $jsonApiEncoder,
        protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $apiConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $sessionManager,
            $responseRedirect,
            $logService,
            $helperDataFactory,
            $geefterSession,
            $repositoryFactory,
            $authenticationMethodFactory,
            $dataValidationErrorProcessor,
            $responseWriter
        );
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getRestServiceFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->restServiceFactory;
    }

    public function getJsonApiEncoder(): \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface {
        return $this->jsonApiEncoder;
    }

    public function getModelRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->modelRepositoryFactory;
    }
}
