<?php

namespace Geeftlist\Controller\Api\Rest\Traits;


use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;
use OliveOil\Core\Service\Rest\RestInterface;

/**
 * Trait EntityRelationships
 *
 * @method RestInterface getRestService(string $entityType = null)
 */
trait EntityRelationships
{
    /**
     * NOTICE: We override the PROTECTED method here, as the parent options() deals with headers, CORS, etc.
     *         and we need to keep it that way.
     */
    protected function handleOptions(RequestInterface $request, ResponseInterface $response): void {
        $this->callWithHttpErrorWrapper(function () use ($request, $response): void {
            $this->getRestService()->dispatchRelationshipsRequest($request, $response);
        });
    }

    /**
     * @param string[] $args
     */
    public function get(\Base $fw, array $args) {
        return $this->dispatchRelationshipsAction($fw, $args);
    }

    /**
     * @param string[] $args
     */
    public function patch(\Base $fw, array $args) {
        return $this->dispatchRelationshipsAction($fw, $args);
    }

    /**
     * @param string[] $args
     */
    public function post(\Base $fw, array $args) {
        return $this->dispatchRelationshipsAction($fw, $args);
    }

    /**
     * @param string[] $args
     */
    public function put(\Base $fw, array $args) {
        return $this->dispatchRelationshipsAction($fw, $args);
    }

    /**
     * @param string[] $args
     */
    public function delete(\Base $fw, array $args) {
        return $this->dispatchRelationshipsAction($fw, $args);
    }

    /**
     * @param string[] $args
     * @return mixed|null
     */
    protected function dispatchRelationshipsAction(\Base $fw, array $args) {
        return $this->callWithHttpErrorWrapper(function (): void {
            $request = $this->getRequest();
            $response = $this->getResponse();

            /**
             * If a method named "{verb}{Relationship}Relationships" exists, call it instead of the default REST helper
             * @see \Geeftlist\Controller\Api\Rest\AbstractModelController::__call()
             **/
            $method = $this->getRelationshipMethod($request->getVerb(), $request->getRouteParam('rel'));
            if (method_exists($this, $method)) {
                $this->{$method}($request, $response);
            }
            else {
                $this->getRestService()->dispatchRelationshipsRequest($request, $response);
            }
        });
    }

    /**
     * @see Similar to \Geeftlist\Service\Rest\JsonApi::getRelationshipMethod() but indirectly related
     *
     * @param string $verb
     * @param string $rel
     */
    protected function getRelationshipMethod($verb, $rel): string {
        return strtolower($verb) . ucfirst(\OliveOil\snakecase2camelcase($rel)) . 'Relationships';
    }
}
