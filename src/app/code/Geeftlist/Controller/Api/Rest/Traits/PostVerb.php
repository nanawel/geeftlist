<?php

namespace Geeftlist\Controller\Api\Rest\Traits;


trait PostVerb
{
    public function post(\Base $fw, array $args) {
        return $this->callWithHttpErrorWrapper(fn() => $this->handlePost($this->getRequest(), $this->getResponse()));
    }
}
