<?php

namespace Geeftlist\Controller\Api\Rest\Traits;


trait GetVerb
{
    public function get(\Base $fw, array $args) {
        return $this->callWithHttpErrorWrapper(fn() => $this->handleGet($this->getRequest(), $this->getResponse()));
    }
}
