<?php

namespace Geeftlist\Controller\Api\Rest\Traits;


use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

trait EntityCustomActions
{
    /** @var string[] */
    protected $allowedVerbs = [Http::VERB_POST, Http::VERB_OPTIONS];    // Default for custom actions

    protected function handleOptions(RequestInterface $request, ResponseInterface $response): void {
        parent::handleOptions($request, $response);

        // Add special "Allow-Actions" header to publish supported actions
        $response->addHeaders([
            'Allow-Actions' => implode(',', $this->getActionMethods())
        ]);
    }

    /**
     * @return string[]
     * @throws \ReflectionException
     */
    protected function getActionMethods(): array {
        $actions = [];
        $ref = new \ReflectionClass($this);
        foreach ($ref->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            if (preg_match('/^(\w+)Action$/', $method->getName(), $matches)) {
                $actions[] = $matches[1];
            }
        }

        return $actions;
    }

    protected function forwardToActionMethod(
        string $actionName,
        RequestInterface $request,
        ResponseInterface $response
    ) {
        $methodName = $actionName . 'Action';

        return $this->{$methodName}($request, $response);
    }

    /**
     * @return string[]
     */
    protected function getAllowedVerbs(array $params = []): array {
        return $this->allowedVerbs;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \ReflectionException
     */
    public function __call(string $name, array $arguments) {
        [$fw, $args] = $arguments;
        if (isset($args['action'])) {
            $availableMethods = $this->getActionMethods();
            if (in_array($args['action'], $availableMethods)) {
                return $this->forwardToActionMethod($args['action'], $this->getRequest(), $this->getResponse());
            }
        }

        throw new MethodNotAllowedException();
    }
}
