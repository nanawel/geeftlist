<?php

namespace Geeftlist\Controller\Api\Rest\Family\MembershipRequest;

use Geeftlist\Controller\Api\Rest\AbstractModelController;
use Geeftlist\Controller\Api\Rest\Traits\EntityCustomActions;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

/**
 * @method \Geeftlist\Service\Rest\JsonApi\ModelHandler\Family\MembershipRequest getRestService($entityType = null)
 */
class CustomActions extends AbstractModelController
{
    use EntityCustomActions;

    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context,
        protected \Geeftlist\Helper\Family\Membership $membershipHelper,
        string $entityType
    ) {
        parent::__construct($context, $entityType);
    }

    public function acceptAction(RequestInterface $request, ResponseInterface $response): void {
        $this->callWithHttpErrorWrapper(function () use ($request, $response): void {
            $this->getRestService()->acceptAction($request, $response);
        });
    }

    public function cancelAction(RequestInterface $request, ResponseInterface $response): void {
        $this->callWithHttpErrorWrapper(function () use ($request, $response): void {
            $this->getRestService()->cancelAction($request, $response);
        });
    }

    public function rejectAction(RequestInterface $request, ResponseInterface $response): void {
        $this->callWithHttpErrorWrapper(function () use ($request, $response): void {
            $this->getRestService()->rejectAction($request, $response);
        });
    }

    public function getMembershipHelper(): \Geeftlist\Helper\Family\Membership {
        return $this->membershipHelper;
    }
}
