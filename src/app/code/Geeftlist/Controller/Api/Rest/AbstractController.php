<?php

namespace Geeftlist\Controller\Api\Rest;

use Geeftlist\Exception\SecurityException;
use Narrowspark\HttpStatus\Contract\Exception\HttpException;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use Narrowspark\HttpStatus\Exception\NotFoundException;
use Narrowspark\HttpStatus\Exception\UnsupportedMediaTypeException;
use Neomerx\JsonApi\Schema\Error;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\AppException;
use OliveOil\Core\Exception\MissingValueException;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

/**
 * @method RequestInterface getRequest()
 * @method ResponseInterface getResponse()
 *
 * @see https://nanawel-gitlab.dyndns.org/geeftlist/webapp/wikis/feature/rest_api
 */
class AbstractController extends \Geeftlist\Controller\Api\AbstractController
{
    //protected array $nonRestrictedActions = [Http::VERB_GET]; //DEBUG
    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    protected \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface $jsonApiEncoder;

    protected \OliveOil\Core\Service\GenericFactoryInterface $restServiceFactory;

    /**
     * Force overriding $context for DI autowiring
     */
    public function __construct(
        \Geeftlist\Controller\Api\Rest\Context $context
    ) {
        $this->coreFactory = $context->getCoreFactory();
        $this->jsonApiEncoder = $context->getJsonApiEncoder();
        $this->restServiceFactory = $context->getRestServiceFactory();
        parent::__construct($context);
    }

    protected function setupCors(): static {
        $this->fw->set('CORS.expose', $this->getAllowedVerbs());

        $exposedHeaders = $this->getExposedHeaders();
        $this->requestManager->getResponse()
            ->addHeaders([
                'Access-Control-Expose-Headers' => implode(',', $exposedHeaders)
            ]);

        return $this;
    }

    /**
     * @return string[]
     */
    protected function getAllowedVerbs(array $params = []): array {
        $allowed = [];
        foreach (Http::VERBS as $verb) {
            if (method_exists($this, $this->getMethodForVerb($verb, $params))) {
                $allowed[] = $verb;
            }
        }

        return $allowed;
    }

    /**
     * @return string[]
     */
    protected function getAllowedHeaders(array $params = []): array {
        $allowed = $this->fw->get('CORS.headers');

        return is_array($allowed) ? $allowed : [$allowed];
    }

    /**
     * @return string[]
     */
    protected function getExposedHeaders(array $params = []): array {
        $allowed = $this->fw->get('CORS.exposed_headers');

        return is_array($allowed) ? $allowed : [$allowed];
    }

    /**
     * @param string $verb
     */
    protected function getMethodForVerb($verb, array $params = []): string {
        return strtolower($verb);
    }

    protected function getAcceptContentTypes(array $params = []): array {
        return ['application/vnd.api+json'];
    }

    protected function onBeforeRoute(\Base $fw, array $args = []) {
        $this->enforceContentType();

        return parent::onBeforeRoute($fw, $args);
    }

    public function options(\Base $fw, array $args = []): void {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $this->handleOptions($request, $response);
        if ($this->isCorsEnabled()) {
            $this->handleOptionsCors($request, $response);
        }
    }

    protected function handleOptions(RequestInterface $request, ResponseInterface $response): void {
        $allowedVerbs = $this->getAllowedVerbs();
        $acceptContentTypes = $this->getAcceptContentTypes();
        $response->setHeaders([
            'Allow' => implode(',', $allowedVerbs),
            'Accept' => implode(',', $acceptContentTypes),
        ]);
    }

    public function handleOptionsCors(RequestInterface $request, ResponseInterface $response): void {
        $allowedHeaders = $this->getAllowedHeaders();
        $exposedHeaders = $this->getExposedHeaders();

        // Use "Allow" header as a source for "Access-Control-Allow-Methods"
        $response->addHeaders([
            'Access-Control-Allow-Methods'  => $response->getHeaders()['Allow'] ?? '',
            'Access-Control-Allow-Headers'  => implode(',', $allowedHeaders),
            'Access-Control-Expose-Headers' => implode(',', $exposedHeaders)
        ]);
    }

    protected function enforceContentType(): static {
        // GET and OPTIONS requests don't use a body, so skip this check (it also avoids errors with some
        // irritating clients like Kitsu/Axios that won't send the Content-Type header in those cases)
        $skipCheckForVerbs = [
            Http::VERB_HEAD,
            Http::VERB_GET,
            Http::VERB_OPTIONS
        ];
        if (in_array($this->getRequest()->getVerb(), $skipCheckForVerbs)) {
            return $this;
        }

        $valid = false;
        switch ($mime = $this->getRequest()->getMimetype()) {
            case 'application/x-www-form-urlencoded':
            case 'multipart/form-data':
                $valid = strcasecmp($this->getActionName(), Http::VERB_POST) === 0;
                break;

            case 'application/json':
            case 'application/vnd.api+json':
                $valid = true;
                break;
        }

        // OPTIONS verb does not need valid Content-Type
        if (! $valid) {
            if ($mime) {
                throw new UnsupportedMediaTypeException('Unsupported Content-Type: ' . $mime);
            }

            throw new UnsupportedMediaTypeException('Missing Content-Type.');
        }

        return $this;
    }

    /**
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     */
    public function callWithHttpErrorWrapper(callable $callable) {
        try {
            return $callable();
        }
        catch (NoSuchEntityException $e) {
            throw new NotFoundException($e->getMessage(), $e);
        }
        catch (NotImplementedException $e) {
            throw new \Narrowspark\HttpStatus\Exception\NotImplementedException(null, $e);
        }
        catch (SecurityException $e) {
            throw new ForbiddenException(null, $e);
        }
        catch (\InvalidArgumentException | MissingValueException | AppException $e) {
            throw new BadRequestException($e->getMessage(), $e);
        }
        catch (HttpException $e) {
            throw $e;
        }
        catch (\Throwable $e) {
            // Default handling by \OliveOil\Core\Service\App\Api\Rest\JsonApi\ErrorHandler (HTTP 500 usually)
            throw $e;
        }
    }

    public function __call(string $name, array $arguments) {
        throw new MethodNotAllowedException($name);
    }

    /**
     * /!\ Do *not* cache request in the controller's attributes as it prevents
     * unit tests from working.
     *
     * @param bool $initFromGlobals
     * @return \OliveOil\Core\Model\Http\RequestInterface
     */
    protected function newRequest($initFromGlobals = true) {
        return $this->requestManager->newRequest($initFromGlobals);
    }

    /**
     * @return \OliveOil\Core\Model\Http\ResponseInterface
     */
    protected function newResponse() {
        return $this->requestManager->newResponse();
    }

    /**
     * @return \Neomerx\JsonApi\Schema\Error
     */
    protected function newJsonApiError() {
        return $this->coreFactory->make(Error::class);
    }

    /**
     * @return \ArrayObject
     */
    protected function newJsonApiMeta() {
        return $this->coreFactory->make(\ArrayObject::class);
    }

    /**
     * @param bool $new Whether to return a new instance or not
     * @return \Neomerx\JsonApi\Contracts\Encoder\EncoderInterface
     */
    public function getJsonApiEncoder($new = true) {
        $encoder = $this->jsonApiEncoder;
        if ($new) {
            $encoder = clone $encoder;
        }

        return $encoder;
    }
}
