<?php

namespace Geeftlist\Controller\Api;


use Geeftlist\Exception\Security\AuthenticationException;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\Di\Exception;

class AbstractController extends \OliveOil\Core\Controller\Api\AbstractController
{
    protected \Geeftlist\Model\Session\GeefterInterface $geefterSession;

    protected \Geeftlist\Model\RepositoryFactory $geeftlistRepositoryFactory;

    protected \OliveOil\Core\Service\GenericFactoryInterface $authenticationMethodFactory;

    protected \OliveOil\Core\Service\Http\ResponseWriterInterface $responseWriter;

    protected array $nonRestrictedActions = [];

    protected string $authorizationHeaderServerVar;

    public function __construct(
        \Geeftlist\Controller\Api\Context $context
    ) {
        $this->geefterSession = $context->getGeefterSession();
        $this->geeftlistRepositoryFactory = $context->getGeeftlistRepositoryFactory();
        $this->authenticationMethodFactory = $context->getAuthenticationMethodFactory();
        $this->responseWriter = $context->getResponseWriter();
        parent::__construct($context);

        $this->authorizationHeaderServerVar = sprintf(
            'HTTP_%s',
            strtoupper(str_replace('-', '_', $this->apiConfig->getValue('AUTHORIZATION_HEADER', 'Authorization')))
        );
    }

    /**
     * @inheritDoc
     */
    protected function onBeforeRoute(\Base $fw, array $args = []) {
        if (!$this->apiConfig->getValue('ENABLE')) {
            throw new ServiceUnavailableException('The API is not available at the moment.');
        }

        $this->setupCors()
            ->initSession()
            ->setupSecurity()
            ->setupI18n();

        return parent::onBeforeRoute($fw, $args);
    }

    /**
     * @inheritDoc
     */
    protected function onAfterRoute(\Base $fw, array $args = []): bool {
        parent::onAfterRoute($fw, $args);
        $this->responseWriter->write($this->requestManager->getResponse());

        return true;
    }

    protected function setupCors(): static {
        return $this;
    }

    public function isCorsEnabled(): bool {
        return $this->fw->get('CORS.origin') && $this->fw->get('HEADERS.Origin');
    }

    protected function initSession(): static {
        if ([$method, $args] = $this->getAuthorization()) {
            try {
                /** @var \Geeftlist\Service\Authentication\AuthenticationInterface $authMethod */
                $authMethod = $this->authenticationMethodFactory->getFromCode(strtolower((string) $method));
            }
            catch (Exception $e) {
                throw new BadRequestException(sprintf('Invalid authentication method "%s"', $method));
            }

            try {
                $geefter = $authMethod->authenticate($args);
                $this->sessionManager->getSession('geefter')->setGeefterAsLoggedIn($geefter);
            }
            catch (AuthenticationException $e) {
                $this->getLogger()->critical($e);
                throw new UnauthorizedException('Invalid credentials.', $e);
            }
        }

        return $this;
    }

    protected function setupSecurity(): static {
        $geefterSession = $this->geefterSession;
        if (
            !$geefterSession->isLoggedIn()
            && !$this->isNonRestrictedAction($this->getActionName())
        ) {
            throw new UnauthorizedException($this->fw->get('SERVER.REQUEST_URI'));
        }

        return $this;
    }

    /**
     * @param string $action
     */
    protected function isNonRestrictedAction($action): bool {
        // Make sure actions (which are HTTP verbs here) have the right case
        $nonRestrictedActions = array_map('strtoupper', $this->nonRestrictedActions);
        $action = strtoupper($action);

        // Let's assume that OPTIONS is always allowed
        return $action === Http::VERB_OPTIONS
            || in_array($action, $nonRestrictedActions, true)
            || in_array('*', $nonRestrictedActions, true);
    }

    protected function setupI18n(): static {
        $geefterSession = $this->geefterSession;
        $lang = $geefterSession->getLanguage();
        if ($lang && $this->i18n->isValidLanguage($lang)) {
            $this->i18n->setLanguage($lang)
                ->setGlobalLanguage($lang);
        }

        $this->i18n->setCurrencyCode($geefterSession->getCurrencyCode())
            ->setTimezone($geefterSession->getTimezone());

        return $this;
    }

    /**
     * @return array|null [method, value]
     */
    protected function getAuthorization(): ?array {
        $auth = null;
        foreach ($this->fw->get('SERVER') as $header => $value) {
            if ($header === $this->authorizationHeaderServerVar) {
                $auth = $value;
                break;
            }
        }

        if ($auth && preg_match('/^(?P<method>[\w-]+)\s+(?P<value>.*)$/', (string) $auth, $matches)) {
            return [
                $matches['method'],
                $matches['value']
            ];
        }

        return null;
    }

    /**
     * Force waiting for a predefinied delay on sensitive actions to mitigate easy brute-forcing.
     */
    protected function waitForSensitiveActionsDelay($weight = 1) {
        if (($delay = (int) $this->apiConfig->getValue('SENSITIVE_ACTIONS_DELAY')) !== 0) {
            usleep($delay * 1000 * $weight);
        }
    }
}
