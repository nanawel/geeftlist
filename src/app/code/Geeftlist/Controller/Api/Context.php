<?php

namespace Geeftlist\Controller\Api;


class Context extends \OliveOil\Core\Controller\Api\Context
{
    /** @var \Geeftlist\Model\ResourceFactory */
    protected $geeftlistResourceFactory;

    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Model\Api\ConfigInterface $apiConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \Geeftlist\Service\Url\Builder\Api $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Helper\Data\Factory $helperDataFactory,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Model\RepositoryFactory $geeftlistRepositoryFactory,
        protected \OliveOil\Core\Service\GenericFactoryInterface $authenticationMethodFactory,
        protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor,
        protected \OliveOil\Core\Service\Http\ResponseWriterInterface $responseWriter
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $apiConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $sessionManager,
            $responseRedirect,
            $logService,
            $helperDataFactory
        );
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }

    public function getGeeftlistRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->geeftlistRepositoryFactory;
    }

    public function getAuthenticationMethodFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->authenticationMethodFactory;
    }

    public function getDataValidationErrorProcessor(): \OliveOil\Core\Helper\Data\Validation\ErrorProcessor {
        return $this->dataValidationErrorProcessor;
    }

    public function getResponseWriter(): \OliveOil\Core\Service\Http\ResponseWriterInterface {
        return $this->responseWriter;
    }
}
