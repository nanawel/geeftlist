<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Narrowspark\HttpStatus\Exception\NotFoundException;

class Dev extends AbstractController
{
    protected function init() {
        $this->nonRestrictedActions = ['*'];
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        if ($this->appConfig->getValue('MODE') != 'developer') {
            throw new NotFoundException();
        }

        return true;
    }

    public function configAction(): void {
        printf(
            '<pre>%s</pre>',
            print_r($this->appConfig->getAllValues(), true)
        );
        exit;
    }

    public function phpinfoAction(): void {
        phpinfo();
        exit;
    }
}
