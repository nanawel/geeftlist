<?php

namespace Geeftlist\Controller\Reservation;

use Geeftlist\Form\Element\GiftListSelect;
use Geeftlist\Model\GiftList;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Form\ElementInterface;

class Edit extends AbstractReservationController
{
    /** @var \OliveOil\Core\Model\RepositoryInterface<\Geeftlist\Model\GiftList> */
    protected $giftListRepository;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftHelper
    ) {
        parent::__construct($context);
        $this->giftListRepository = $context->getModelRepositoryFactory()->resolveGet(GiftList::ENTITY_TYPE);
    }

    public function indexAction(): void {
        $gift = $this->getGift();
        if (! $gift->getId()) {
            $this->geefterSession->addErrorMessage('Invalid gift.');
            $this->redirectReferer();
        }

        if (! $this->canManageReservation($gift)) {
            $this->geefterSession->addErrorMessage(
                'You cannot create or change your reservation for this gift at the moment.'
            );
            $this->redirectReferer();
        }

        $allCurrentReservations = $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Reservation::ENTITY_TYPE)
            ->addJoinRelation('geefter')
            ->addFieldToFilter('main_table.gift_id', $gift->getId())
            ->addFieldToFilter('main_table.geefter_id', ['ne' => $this->geefterSession->getGeefter()->getId()]);

        $currentReservations = $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Reservation::ENTITY_TYPE)
            ->addFieldToFilter('main_table.gift_id', $gift->getId())
            ->addFieldToFilter('main_table.geefter_id', $this->geefterSession->getGeefter()->getId());
        $currentReservation = $currentReservations->getFirstItem()
            ?: $this->getModelRepository(\Geeftlist\Model\Reservation::ENTITY_TYPE)->newModel();

        $this->view
            ->setData('GIFT', $gift)
            ->setData('RESERVATIONS', $allCurrentReservations)
            ->setData('FORM_ACTION', $this->getUrl('*/post', ['gift_id' => $gift->getId(), '_query' => '*']))
            ->setData('CURRENT_RESERVATION', $currentReservation)
            ->getLayout()
            ->setBlockConfig('gift', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift.phtml',
                'display_mode'           => 'full',
                'display_page_link'      => false,
                'use_gift_by_geeftee_preload'     => false,
                'use_reservation_by_gift_preload' => false,
                'badges_block_name'      => 'gift_badges'
            ])
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml'
            ])
            ->setBlockConfig('gift_actions', [
                'type'                => \Geeftlist\Block\Gift::class,
                'template'            => 'common/gift/actions.phtml',
                'show_action_buttons' => [
                    'edit',
                    'duplicate',
                    'discuss'
                ]
            ])
            ->setBlockTemplate('content', 'reservation/index.phtml')
            ->setBlockTemplate('toolbar', 'reservation/form/toolbar.phtml')
            ->setBlockTemplate('current_reservations', 'common/current_reservations.phtml')
            ->setBlockTemplate('form', 'reservation/form.phtml');

        // We don't restore previously entered values even in the case of an error here
        $this->addFormElements([]);
    }

    public function postAction(): void {
        $reservationRepository = $this->getModelRepository(\Geeftlist\Model\Reservation::ENTITY_TYPE);
        $reservation = $this->getReservation();
        $isUpdate = (bool) $reservation->getId();

        $post = $this->getPost();
        $post = $this->filterReservationData($post);

        if (! empty($post['type']) && $post['type'] === 'cancel') {
            if (! $reservation->getId()) {
                $this->geefterSession->addErrorMessage('Invalid reservation.');
                $this->redirectReferer();
            }

            $reservationRepository->delete($reservation);

            $this->geefterSession->addSuccessMessage('Reservation cancelled successfully.');
        }
        else {
            $reservation->addData($post);

            $errors = $this->validateReservationData($post, $reservation);
            if ($errors !== []) {
                foreach ($errors as $e) {
                    $this->geefterSession->addErrorMessage($e);
                }

                $this->redirectReferer();
                return;
            }

            $reservationRepository->save($reservation);

            if ($isUpdate) {
                $this->geefterSession->addSuccessMessage('Reservation updated successfully!');
            }
            else {
                $this->geefterSession->addSuccessMessage('Reservation created successfully!');
            }
        }

        $giftLists = [];
        if (isset($post['giftlist_ids'])) {
            foreach ($post['giftlist_ids'] as $giftListId) {
                if (!$giftList = $this->giftListRepository->get($giftListId)) {
                    // Not a valid ID and numeric => error
                    if (is_numeric($giftListId)) {
                        throw new NoSuchEntityException('Unknown gift list.');
                    }

                    $giftListId = trim((string) $giftListId);
                    // Not a valid ID but not numeric => search for an existing list with this name
                    $giftList = $this->giftListRepository->findOne([
                        'filter' => [
                            'name' => [['eq' => $giftListId]]
                        ]
                    ]);
                    // Not found? Create the list on-the-fly
                    if (!$giftList) {
                        /** @var GiftList $giftList */
                        $giftList = $this->giftListRepository->newModel();
                        $giftList->setName($giftListId);
                        $this->giftListRepository->save($giftList);
                    }
                }

                $giftLists[] = $giftList;
            }
        }

        if ($this->giftListGiftHelper->replaceLinks([$this->getGift()], $giftLists) !== 0) {
            $this->geefterSession->addSuccessMessage("Gift's list(s) updated successfully!");
        }

        $this->redirectReferer();
    }

    protected function getFormElements(array $formData): array {
        return [
            'GIFTLIST_SELECT' => $this->modelFactory->make(GiftListSelect::class, ['data' => [
                'name'              => 'giftlist_ids',
                'id'                => 'giftlist_ids',
                'value'             => $formData['giftlist_ids'] ?? null,
                'gift'              => $this->getGift(),
                'enforce_whitelist' => false
            ]]),
        ];
    }

    protected function addFormElements(array $formData): static {
        foreach ($this->getFormElements($formData) as $name => $formElement) {
            $this->view->setData($name, $formElement);
        }

        return $this;
    }

    protected function filterReservationData($formData): array {
        $return = \OliveOil\array_mask($formData, [
            'type',
            'amount'
        ]);

        // Force NULL for empty amount
        $return['amount'] = trim($return['amount'] ?? '') ?: null;

        // Keep only data used by complex form elements in the next step
        $formData = \OliveOil\array_discard($formData, array_keys($return));

        /** @var ElementInterface $formElement */
        foreach ($this->getFormElements($formData) as $formElement) {
            $formElement->filter($formData);
        }

        return array_merge($return, $formData);
    }

    protected function validateReservationData(array &$reservationData, \Geeftlist\Model\Reservation $reservation): array {
        return $this->dataValidationErrorProcessor->process(
            $reservation->validate(),
            $reservationData
        );
    }
}
