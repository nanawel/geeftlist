<?php

namespace Geeftlist\Controller\Reservation;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\ResourceModel\Db\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;

abstract class AbstractReservationController extends AbstractController
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $giftRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $reservationRepository;

    protected \Geeftlist\Service\Permission $permissionService;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context
    ) {
        $this->giftRepository = $context->getModelRepositoryFactory()->resolveGet(\Geeftlist\Model\Gift::ENTITY_TYPE);
        $this->reservationRepository = $context->getModelRepositoryFactory()->resolveGet(Reservation::ENTITY_TYPE);
        $this->permissionService = $context->getPermissionService();
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        $this->breadcrumbs->addCrumb('My Reservations', '*/*');

        return true;
    }

    /**
     * @param bool $showUnavailable
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    protected function getReservationGeefteeCollection($showUnavailable) {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
        /** @phpstan-ignore-next-line */
        $collection = $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->addJoinRelation('reservation')
            ->joinTable(                      // FIXME There should be no joinTable() outside of the Resource package
                ['gift' => Gift::MAIN_TABLE],
                'reservation.gift_id = gift.gift_id',
                []
            )
            ->addFieldToFilter('reservation.geefter_id', $this->geefterSession->getGeefter()->getId())
            ->orderBy('main_table.name');

        // Show/Hide unavailable gifts
        if (! $showUnavailable) {
            $collection->addFieldToFilter('gift.status', ['nin' => [
                Status::ARCHIVED,
                Status::DELETED
            ]]);
        }

        return $collection;
    }

    /**
     * @return \Geeftlist\Model\Gift|null
     */
    public function getGift() {
        if ($giftId = $this->getRequestParam('gift_id')) {
            try {
                /** @var \Geeftlist\Model\Gift|null $gift */
                $gift = $this->getGiftRepository()->get($giftId);
            }
            catch (PermissionException) {
            }
        }

        if (!isset($gift) || !$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        return $gift;
    }

    /**
     * @return \Geeftlist\Model\Reservation
     */
    public function getReservation() {
        /** @var int $geefterId */
        $geefterId = $this->geefterSession->getGeefter()->getId();
        /** @var int $giftId */
        $giftId = $this->getGift()->getId();

        /** @var \Geeftlist\Model\Reservation|null $reservation */
        $reservation = $this->getReservationRepository()->findOne([
            'filter' => [
                'main_table.geefter_id' => [['eq' => $geefterId]],
                'main_table.gift_id' => [['eq' => $giftId]],
            ]]);

        if (!$reservation) {
            /** @var \Geeftlist\Model\Reservation|null $reservation */
            $reservation = $this->getReservationRepository()->newModel()
                ->addData([ // Pre-fill key IDs on model
                    'geefter_id' => $geefterId,
                    'gift_id' => $giftId
                ]);
        }

        return $reservation;
    }

    protected function canManageReservation(\Geeftlist\Model\Gift $gift): bool {
        return $this->getPermissionService()->isAllowed($gift, TypeInterface::ACTION_RESERVE_PURCHASE)
            || $this->getPermissionService()->isAllowed($gift, TypeInterface::ACTION_EDIT_RESERVATION)
            || $this->getPermissionService()->isAllowed($gift, TypeInterface::ACTION_CANCEL_RESERVATION);
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGiftRepository() {
        return $this->giftRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getReservationRepository() {
        return $this->reservationRepository;
    }

    /**
     * @return \Geeftlist\Service\Permission
     */
    public function getPermissionService() {
        return $this->permissionService;
    }
}
