<?php

namespace Geeftlist\Controller\Reservation;

use Geeftlist\Model\Gift\Field\Status;
use OliveOil\Core\Block\Dialog;

class ListController extends AbstractReservationController
{
    public function listAction(): void {
        $geefter = $this->geefterSession->getGeefter();

        $showUnavailable = (bool) $this->urlBuilder->getRequestQuery('showUnavailable');

        $criteriaAppenders = [
            static function ($entityType, array &$criteria) use ($geefter, $showUnavailable): void {
                if ($entityType == \Geeftlist\Model\Gift::ENTITY_TYPE) {
                    // Only include gifts with reservation
                    $criteria['filter']['reserver'][] = ['eq' => $geefter->getId()];
                    // Show/Hide unavailable gifts
                    if (! $showUnavailable) {
                        $criteria['filter']['status'] = [
                            ['ne' => Status::ARCHIVED]
                        ];
                    }
                }
            }
        ];

        // FIXME See https://nanawel-gitlab.dyndns.org/geeftlist/webapp/issues/408
        $hasReservations = (bool) $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Reservation::ENTITY_TYPE)
            ->addFieldToFilter('main_table.geefter_id', $geefter->getId())
            ->count();

        $noReservationMessages = [
            $this->__("It seems like you haven't reserved any gift yet :("),
        ];
        if ($this->geefterSession->getFamilyIds()) {
            $noReservationMessages[] = $this->__(
                'Why not <a href="{0}">browsing your families</a> and picking up some gifts?',
                $this->getUrl('relative_explore')
            );
        }

        $this->view
            ->setData('HAS_RESERVATIONS', $hasReservations)
            ->getLayout()
            ->setBlockTemplate('content', 'reservation/list.phtml')
            ->setBlockConfig('geeftee_list', [
                'type'               => \Geeftlist\Block\Gift::class,
                'template'           => 'common/geeftee_list.phtml',
                'geeftees'           => $this->getReservationGeefteeCollection($showUnavailable),
                'criteria_appenders' => $criteriaAppenders,
                'show_empty_tabs'    => false
            ])
            ->setBlockConfig('gift_list', [
                'type'               => \Geeftlist\Block\Gift::class,
                'template'           => 'common/gift/list.phtml',
                'criteria_appenders' => $criteriaAppenders
            ])
            ->setBlockTemplate('gift_list_header', 'common/gift/list/header.phtml')
            ->setBlockConfig('geeftee_card', [
                'type'                => \Geeftlist\Block\Geeftee::class,
                'template'            => 'geeftee/card.phtml',
                'show_claim_link'     => false,
                'show_gift_count'     => false,
                'show_action_buttons' => false,
            ])
            ->setBlockConfig('gift', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/list/item.phtml',
                'display_reservations'   => true
            ])
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small']
            ])
            ->setBlockConfig('gift_actions', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/actions.phtml',
                'reserve_label'          => 'Change reservation'
            ])
            ->setBlockConfig('gift_reservations', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/reservations.phtml'
            ])
            ->setBlockConfig('no_reservation_dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_ERROR,
                'message' => $noReservationMessages,
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('dashboard'),
                    'text'  => $this->__('Back to dashboard'),
                    'class' => 'button back'
                ]]
            ]);
    }
}
