<?php

namespace Geeftlist\Controller\Http;


class Context extends \OliveOil\Core\Controller\Http\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\View $view,
        \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        \OliveOil\Core\Model\View\Breadcrumbs $breadcrumbs,
        protected \OliveOil\Core\Service\Design $design,
        protected \Geeftlist\Model\Session\Http\GeefterInterface $geefterSession,
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \Geeftlist\Model\ResourceFactory $modelResourceFactory,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\Security\Csrf $csrfService,
        protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor,
        protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory,
        protected \Geeftlist\Service\Security $securityService,
        protected \Geeftlist\Service\Permission $permissionService
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $sessionManager,
            $view,
            $responseRedirect,
            $breadcrumbs,
            $logService,
            $csrfService
        );
    }

    public function getDesign(): \OliveOil\Core\Service\Design {
        return $this->design;
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\Http\GeefterInterface {
        return $this->geefterSession;
    }

    public function getModelFactory(): \Geeftlist\Model\ModelFactory {
        return $this->modelFactory;
    }

    public function getModelResourceFactory(): \Geeftlist\Model\ResourceFactory {
        return $this->modelResourceFactory;
    }

    public function getDataValidationErrorProcessor(): \OliveOil\Core\Helper\Data\Validation\ErrorProcessor {
        return $this->dataValidationErrorProcessor;
    }

    public function getModelRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->modelRepositoryFactory;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    public function getPermissionService(): \Geeftlist\Service\Permission {
        return $this->permissionService;
    }
}
