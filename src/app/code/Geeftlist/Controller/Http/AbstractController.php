<?php

namespace Geeftlist\Controller\Http;

use OliveOil\Core\Block\HtmlRootContainer;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\Security\CsrfException;

abstract class AbstractController extends \OliveOil\Core\Controller\Http\AbstractController
{
    /** @var string[] */
    protected array $nonRestrictedActions = [];

    protected \OliveOil\Core\Service\Design $design;

    protected \Geeftlist\Model\Session\Http\GeefterInterface $geefterSession;

    protected \Geeftlist\Model\ModelFactory $modelFactory;

    protected \Geeftlist\Model\ResourceFactory $modelResourceFactory;

    protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory;

    protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context
    ) {
        parent::__construct($context);
        $this->design = $context->getDesign();
        $this->geefterSession = $context->getGeefterSession();
        $this->modelFactory = $context->getModelFactory();
        $this->modelResourceFactory = $context->getModelResourceFactory();
        $this->modelRepositoryFactory = $context->getModelRepositoryFactory();
        $this->dataValidationErrorProcessor = $context->getDataValidationErrorProcessor();
    }

    /**
     * @inheritDoc
     */
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        $this->setupSecurity()
            ->setupDesign()
            ->setupI18n();

        $this->setPageTitle($fw->get('app.TITLE'));

        return true;
    }

    protected function initLayout(): static {
        parent::initLayout();
        $this->view->getLayout()
            ->setBlockConfig('head', [
                'type'     => \OliveOil\Core\Block\HtmlRootContainer::class,
                'template' => 'page/head.phtml',
            ])
            ->setBlockConfig('footer', [
                'type'     => \OliveOil\Core\Block\HtmlRootContainer::class,
                'template' => 'page/footer.phtml',
            ])
            ->setBlockConfig('header', [
                'type' => \Geeftlist\Block\Page\Header::class,
                'template' => 'page/header.phtml'
            ])
            ->setBlockConfig('header.nav', [
                'type' => \Geeftlist\Block\Page\Nav::class,
                'template' => 'page/header/nav.phtml'
            ])
            ->setBlockConfig('global_messages', [
                'type'     => \OliveOil\Core\Block\Container::class,
                'code'     => 'global_messages',
            ])
            ->setBlockCachePlaceholder('footer')
            ->setBlockConfig('breadcrumbs', [
                'type'     => \OliveOil\Core\Block\Breadcrumbs::class,
                'template' => 'page/breadcrumbs.phtml'
            ])
            ->setBlockConfig('csrf_token', [
                'type'     => \OliveOil\Core\Block\SpecialInput\CsrfToken::class
            ])
            ->setBlockConfig('before_body_end', [
                'type' => \OliveOil\Core\Block\Container::class
            ])
            ->setBlockCachePlaceholder('messages');

        $js = <<<"EOJS"
var BASE_URL = document.BASE_URL ='{$this->urlBuilder->getBaseUrl()}';
var BASE_JS_URL = document.BASE_JS_URL = '{$this->design->getJsUrl('', ['_skip_avid' => true])}';
EOJS;

        /** @var HtmlRootContainer $head */
        $head = $this->view->getLayout()
            ->getBlock('head');
        $head->addInlineJs($js);

        return $this;
    }

    protected function addGeneralAnnounceMessageBlock() {
        $this->view->getLayout()
            ->setBlockConfig('general_announce', [
                'type'     => \Geeftlist\Block\Cms::class,
                'code'     => 'general_announce',
                'quiet'    => true
            ])
            ->addChild('global_messages', 'general_announce');
    }

    protected function setupSecurity(): static {
        $geefterSession = $this->geefterSession;
        if (
            !$geefterSession->isLoggedIn()
            && !$this->isNonRestrictedAction($this->getActionName())
        ) {
            $geefterSession->setAfterLoginUrl($this->getUrl('', ['_current' => true]));
            $this->reroute('login');
        }

        // Force CSRF check on all POST requests
        if (strtoupper((string) $this->fw->get('VERB')) === Http::VERB_POST) {
            try {
                $this->checkFormPost();
            }
            catch (CsrfException $e) {
                $this->getLogger('security')->error($e->getMessage());
                throw $e;
            }
        }

        return $this;
    }

    protected function isNonRestrictedAction(string $action): bool {
        return in_array($action, $this->nonRestrictedActions, true)
            || in_array('*', $this->nonRestrictedActions, true);
    }

    protected function setupDesign(): static {
        $design = $this->design;
        if ($theme = $this->getSession()->getTheme()) {
            $design->setTheme($theme);
        }

        $design->bootstrapTheme();

        $this->view->getLayout()
            ->setPageConfig('body_class', \OliveOil\idfy($this->getFullActionName()));

        return $this;
    }

    protected function setupI18n(): static {
        $geefterSession = $this->geefterSession;
        $lang = $geefterSession->getLanguage();
        if ($lang && $this->i18n->isValidLanguage($lang)) {
            $this->i18n->setLanguage($lang)
                ->setGlobalLanguage($lang);
        }

        $this->i18n->setCurrencyCode($geefterSession->getCurrencyCode())
            ->setTimezone($geefterSession->getTimezone());

        // Set HTML lang according to defined locale
        $this->fw->set('HTML_LANG', $this->i18n->getLocaleCountryCodeISO2());

        $translationLoadPath = $this->design->getAssetUrl('i18n/{{lng}}/{{ns}}.json');

        $langWithFallback = json_encode($this->i18n->getLanguageWithFallback(true));
        $js = <<<"EOJS"
var LANGUAGE_WITH_FALLBACK = window.LANGUAGE_WITH_FALLBACK = {$langWithFallback};
var LANGUAGE = window.LANGUAGE = LANGUAGE_WITH_FALLBACK[0];
var CURRENCY = window.CURRENCY = '{$this->i18n->getCurrencyCode()}';
var I18N_TRANSLATION_PATH = '{$translationLoadPath}';
EOJS;

        /** @var HtmlRootContainer $head */
        $head = $this->view->getLayout()
            ->getBlock('head');
        $head->addInlineJs($js);

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function beforeRender(\Base $fw, array $args = []): static {
        parent::beforeRender($fw, $args);
        $this->setPageTitleFromBreadcrumbs();

        return $this;
    }

    protected function setPageTitleFromBreadcrumbs() {
        $breadcrumbs = $this->breadcrumbs;
        if ($crumbs = $breadcrumbs->getCrumbs()) {
            $this->setPageTitle(sprintf(
                '%s - %s',
                $this->fw->get('PAGE_TITLE'),
                implode(' / ', array_map(
                    fn(string $string, string|array|null $vars = null) => $this->i18n->tr($string, $vars),
                    array_column($crumbs, 'label')
                ))
            ));
        }
    }

    public function getModelRepository(string $entityType): \OliveOil\Core\Model\RepositoryInterface {
        return $this->modelRepositoryFactory->resolveGet($entityType);
    }
}
