<?php

namespace Geeftlist\Controller\Family\Manage;

use Geeftlist\Controller\Family\Manage;
use Geeftlist\Model\Family\MembershipRequest;
use Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest as MembershipRequestResource;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;


class Invite extends Manage
{
    public function indexAction(): void {
        $family = $this->getFamily();
        $this->breadcrumbs->addCrumb(
            $this->__('Invite to {0}', $family->getName()),
            '*/*/*'
        );

        $grid = $this->getGeefteeGrid();

        $this->view
            ->setData('FAMILY', $family)
            ->setData('FORM_ACTION', $this->getUrl('*/post', ['family_id' => $this->getFamily()->getId()]))
            ->getLayout()
            ->setBlockTemplate('content', 'family/manage/invite/index.phtml')
            ->setBlockTemplate('toolbar', 'family/manage/invite/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    public function gridAction(): void {
        $grid = $this->getGeefteeGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    /**
     * Used by link in \Geeftlist\Controller\Geeftee\Manage::searchPostAction()
     */
    public function confirmAction(): void {
        $this->checkCsrf(['GET']);

        if (!$geefteeId = $this->getRequestParam('geeftee_id')) {
            $this->geefterSession->addErrorMessage('Missing geeftee.');
            $this->redirectReferer();
        }

        $this->setRequestParam('geeftee_id', $geefteeId);

        // Forward to postAction()
        $this->postAction();
    }

    public function postAction(): void {
        if ($geefteeId = $this->getRequestParam('geeftee_id')) {
            $geefteeIds = [$geefteeId];
        }
        elseif ($geefteeIds = $this->getPost('selected_grid_rows')) {
            if (! is_array($geefteeIds)) {
                $geefteeIds = false;
            }
        }

        if (! $geefteeIds) {
            $this->geefterSession->addErrorMessage('Please select geeftees first.');
            $this->redirectReferer();
        }

        $family = $this->getFamily();
        $geeftees = $this->modelResourceFactory->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->addFieldToFilter('geeftee_id', ['in' => $geefteeIds])
            ->getItems();

        if (!$geeftees) {
            $this->geefterSession->addErrorMessage('Missing or invalid provided geeftee.');
            $this->redirectReferer();
        }

        $this->membershipHelper->request($family, $geeftees);
        if (count($geefteeIds) == 1) {
            $this->geefterSession->addSuccessMessage(
                '{0} has been invited to {1} successfully!',
                [current($geeftees)->getName(), $family->getName()]
            );
        }
        else {
            $this->geefterSession->addSuccessMessage(
                '{0} geeftees have been invited to {1} successfully!',
                [count($geefteeIds), $family->getName()]
            );
        }

        $this->redirectReferer();
    }

    public function getGeefteeGrid(): \Geeftlist\View\Grid\Geeftee {
        $grid = parent::getGeefteeGrid();

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'ajax_url' => $this->getUrl('*/grid', ['family_id' => $this->getFamily()->getId(), '_referer' => true]),
                'family' => $this->getFamily(),
                'has_selectable_rows' => true
            ]);
            $grid->attachInstanceListener('collection::new', function ($target, array $args): void {
                /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $geefteeCollection */
                $geefteeCollection = $args['collection'];

                $familyFilter = array_diff($this->getFamilyCollection()->getAllIds(), [$this->getFamily()->getId()]);
                $geefteeCollection->addFieldToFilter('families', ['in' => $familyFilter])
                    ->addFieldToFilter('family', ['neq' => $this->getFamily()->getId()])
                    ->addFieldToFilter('main_table.geeftee_id', ['ne' => $this->geefterSession->getGeeftee()->getId()]);

                // Exclude geeftees with pending membership requests
                $select = new Select();
                $select->from(['membership_request' => MembershipRequestResource::MAIN_TABLE])
                    ->where->equalTo(
                        'membership_request.geeftee_id',
                        'main_table.geeftee_id',
                        Predicate::TYPE_IDENTIFIER,
                        Predicate::TYPE_IDENTIFIER
                    )
                    ->AND
                    ->equalTo('membership_request.family_id', $this->getFamily()->getId())
                    ->AND
                    ->equalTo('membership_request.decision_code', MembershipRequest::DECISION_CODE_PENDING);
                $geefteeCollection->getSelect()->where(['NOT EXISTS (?)' => $select]);
            });

            $isConfigured = true;
        }

        return $grid;
    }
}
