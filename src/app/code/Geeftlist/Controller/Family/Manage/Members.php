<?php

namespace Geeftlist\Controller\Family\Manage;

use Geeftlist\Controller\Family\Manage;
use Geeftlist\Model\Family;


class Members extends Manage
{
    public function indexAction(): void {
        $family = $this->getFamily();
        $this->breadcrumbs->addCrumb(
            $this->__('Members of {0}', $family->getName()),
            '*/*/*'
        );

        $grid = $this->getGeefteeGrid();

        $this->view
            ->setData('FAMILY', $family)
            ->setData('FORM_ACTION', $this->getUrl('*/postBan', ['family_id' => $family->getId()]))
            ->setData('GRID_ACTIONS_PARAMS', ['family_id' => $family->getId()])
            ->setData('DISPLAY_SELECTION_COLUMN', true)
            ->getLayout()
            ->setBlockTemplate('content', 'family/manage/members/index.phtml')
            ->setBlockTemplate('toolbar', 'family/manage/members/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    public function gridAction(): void {
        $grid = $this->getGeefteeGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    public function postBanAction(): void {
        $this->checkCsrf();

        $geefteeIds = [];
        if ($geefteeId = $this->getRequestParam('geeftee_id')) {
            $geefteeIds = [$geefteeId];
        }
        elseif ($geefteeIds = $this->getPost('selected_grid_rows')) {
            if (! is_array($geefteeIds)) {
                $geefteeIds = false;
            }
        }

        if (! $geefteeIds) {
            $this->geefterSession->addErrorMessage('Please select geeftees first.');
            $this->redirectReferer();
        }

        $family = $this->getFamily();
        $geeftees = $family->getGeefteeCollection()
            ->addFieldToFilter('main_table.geeftee_id', ['in' => $geefteeIds])
            ->getItems();
        if (empty($geeftees)) {
            $this->geefterSession->addErrorMessage('No valid geeftees to ban.');
            $this->redirectReferer();
        }

        $family->removeGeeftee($geeftees);

        if (count($geefteeIds) == 1) {
            $this->geefterSession->addSuccessMessage(
                '{0} has been banned from {1} successfully.',
                [current($geeftees)->getName(), $family->getName()]
            );
        }
        else {
            $this->geefterSession->addSuccessMessage(
                '{0} geeftees have been banned from {1} successfully.',
                [count($geefteeIds), $family->getName()]
            );
        }

        $this->redirectReferer();
    }

    public function getFamily(bool $skipMembershipCheck = false, bool $errorIfMissing = true): Family {
        if (! $this->family) {
            $family = parent::getFamily($skipMembershipCheck, $errorIfMissing);
            if (! $family->isOwner($this->geefterSession->getGeefter())) {
                $this->geefterSession->addErrorMessage('Invalid family');
                $this->reroute('dashboard');
            }
        }

        return $this->family;
    }

    public function getGeefteeGrid(): \Geeftlist\View\Grid\Geeftee {
        $grid = parent::getGeefteeGrid();

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'ajax_url' => $this->getUrl('*/grid', ['family_id' => $this->getFamily()->getId(), '_referer' => true]),
                'family' => $this->getFamily()
            ]);
            $grid->attachInstanceListener('collection::new', function ($target, array $args): void {
                /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $geefteeCollection */
                $geefteeCollection = $args['collection'];

                $geefteeCollection
                    ->addFieldToFilter('family', $this->getFamily()->getId())
                    ->addFieldToFilter(
                        'main_table.geeftee_id',
                        ['nin' => [$this->geefterSession->getGeeftee()->getId()]]
                    );
            });

            $isConfigured = true;
        }

        return $grid;
    }
}
