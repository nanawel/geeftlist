<?php

namespace Geeftlist\Controller\Family\Manage;

use Geeftlist\Controller\Family\Manage;
use Geeftlist\Exception\Family\AlreadyMemberException;
use Geeftlist\Model\Geefter;

class InviteByEmail extends Manage
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        \Geeftlist\View\Grid\Family $familyGrid,
        \Geeftlist\Helper\Family $familyHelper,
        \Geeftlist\View\Grid\Geeftee $geefteeGrid,
        \Geeftlist\Helper\Family\Membership $membershipHelper,
        protected \OliveOil\Core\Service\Validation\Email $emailValidator,
        protected \Geeftlist\Service\Security $securityService
    ) {
        parent::__construct(
            $context,
            $familyGrid,
            $familyHelper,
            $membershipHelper,
            $geefteeGrid
        );
    }

    public function indexAction(): void {
        $family = $this->getFamily();
        $this->breadcrumbs->addCrumb(
            $this->__('Invite by e-mail to {0}', $family->getName()),
            '*/*/*'
        );

        if (! $family || ! $family->getId()) {
            $this->geefterSession->addErrorMessage('Missing family.');
            $this->redirectReferer();
        }

        $this->view
            ->setData('FAMILY', $family)
            ->setData('FORM_ACTION', $this->getUrl('*/post', ['family_id' => $family->getId(), '_query' => '*']))
            ->getLayout()
            ->setBlockTemplate('content', 'family/manage/invitebyemail/index.phtml')
            ->setBlockTemplate('form', 'family/manage/invitebyemail/form.phtml');
    }

    public function postAction(): void {
        $family = $this->getFamily();
        if (! $family || ! $family->getId()) {
            $this->geefterSession->addErrorMessage('Missing family.');
            $this->redirectReferer();
        }

        $post = $this->getPost();

        if ($errors = $this->validatePost($post)) {
            foreach ($errors as $error) {
                $this->geefterSession->addErrorMessage($error);
            }

            $this->redirectReferer();
        }

        $geefterRepository = $this->getModelRepository(Geefter::ENTITY_TYPE);
        $geefter = $geefterRepository->get($post['email'], 'email');
        if (! $geefter) {
            $this->geefterSession->addErrorMessage('No registered geefter has been found with this email address.');
            $this->redirectReferer();
        }

        try {
            // Need to call privileged in case (most of the time) current and target geefters are not related
            $geeftee = $this->getSecurityService()->callPrivileged(static fn() => $geefter->getGeeftee());
            $this->membershipHelper->request(
                $this->getFamily(),
                $geeftee
            );

            $this->geefterSession->addSuccessMessage('Geefter invited successfully!');
            $this->redirectReferer();
        }
        catch (AlreadyMemberException) {
            $this->geefterSession->addErrorMessage('This geefter is already a member of the family.');
            $this->redirectReferer();
        }
    }

    protected function validatePost(array &$post): array {
        $errors = [];
        if (! isset($post['email']) || ! ($email = trim((string) $post['email']))) {
            $errors[] = $this->__('Missing field: {0}.', $this->__('email'));
        }
        elseif (! $this->getEmailValidator()->validate($email)) {
            $errors[] = $this->__('Invalid field: {0}.', $this->__('email'));
            unset($post['email']);
        }

        return $errors;
    }

    public function getEmailValidator(): \OliveOil\Core\Service\Validation\Email {
        return $this->emailValidator;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }
}
