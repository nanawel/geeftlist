<?php

namespace Geeftlist\Controller\Family\Manage;

use Geeftlist\Controller\Family\Manage;
use Geeftlist\Exception\Family\MemberOperationException;


class MembershipRequest extends Manage
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        \Geeftlist\View\Grid\Family $familyGrid,
        \Geeftlist\Helper\Family $familyHelper,
        \Geeftlist\Helper\Family\Membership $membershipHelper,
        \Geeftlist\View\Grid\Geeftee $geefteeGrid,
        protected \Geeftlist\View\Grid\Family\MembershipRequest $membershipRequestGrid
    ) {
        parent::__construct(
            $context,
            $familyGrid,
            $familyHelper,
            $membershipHelper,
            $geefteeGrid
        );
    }

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb('Membership Requests / Invitations', '*/*/*');

        $grid = $this->getMembershipRequestGrid();

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/post'))
            ->getLayout()
            ->setBlockTemplate('content', 'family/manage/membershiprequests/index.phtml')
            ->setBlockTemplate('toolbar', 'family/manage/membershiprequests/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    public function gridAction(): void {
        $grid = $this->getMembershipRequestGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    public function postAcceptAction() {
        $this->setPost('action', 'accept');

        $this->postAction();
    }

    public function postRejectAction() {
        $this->setPost('action', 'reject');

        $this->postAction();
    }

    public function postCancelAction() {
        $this->setPost('action', 'cancel');

        $this->postAction();
    }

    public function postAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $action = $this->getPost('action');
        if (! $action || ! in_array($action, ['accept', 'reject', 'cancel'])) {
            $this->geefterSession->addErrorMessage('Invalid action.');
            $this->redirectReferer();
        }

        if ($membershipRequestId = $this->getRequestParam('request_id')) {
            $membershipRequestIds = [$membershipRequestId];
        }
        elseif ($membershipRequestIds = $this->getPost('selected_grid_rows')) {
            if (! is_array($membershipRequestIds)) {
                $membershipRequestIds = false;
            }
        }

        if (! $membershipRequestIds) {
            $this->geefterSession->addErrorMessage('Please select requests first.');
            $this->redirectReferer();
        }

        $membershipRequests = $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE)
            ->addFieldToFilter('membership_request_id', ['in' => $membershipRequestIds]);

        $success = 0;
        $errors = [];
        foreach ($membershipRequests as $mr) {
            try {
                switch ($action) {
                    case 'accept':
                        $this->membershipHelper->acceptRequest($mr);
                        break;
                    case 'reject':
                        $this->membershipHelper->rejectRequest($mr);
                        break;
                    case 'cancel':
                        $this->membershipHelper->cancelRequest($mr);
                        break;
                }

                ++$success;
            }
            catch (MemberOperationException $e) {
                $this->getLogger()->critical($e);
                $errors[] = $e;
            }
        }

        if ($success !== 0) {
            $this->geefterSession->addSuccessMessage('{0} request(s) processed successfully.', [$success]);
        }

        foreach ($errors as $e) {
            $this->geefterSession->addErrorMessage($e->getMessage());
        }

        $this->redirectReferer();
    }

    public function getMembershipRequestGrid(): \Geeftlist\View\Grid\Family\MembershipRequest {
        $grid = $this->membershipRequestGrid;

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'ajax_url' => $this->getUrl('*/grid', ['_referer' => true]),
                'current_geefter' => $this->geefterSession->getGeefter(),
                'current_geeftee' => $this->geefterSession->getGeeftee(),
                'has_selectable_rows' => true
            ]);

            $grid->attachInstanceListener('collection::new', static function ($target, array $args): void {
                $args['collection']->addFieldToFilter(
                    'main_table.decision_code',
                    \Geeftlist\Model\Family\MembershipRequest::DECISION_CODE_PENDING
                );
            });
        }

        return $grid;
    }
}
