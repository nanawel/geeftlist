<?php

namespace Geeftlist\Controller\Family\Manage;

use Geeftlist\Controller\Family\Manage;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Geeftee;

class Transfer extends Manage
{
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        $family = $this->getFamily();
        $this->breadcrumbs->addCrumb(
            $this->__('Transfer {0}', $family->getName()),
            '*/index/*'
        );

        if (! $family->isOwner($this->geefterSession->getGeefter())) {
            $this->geefterSession->addErrorMessage('Not the owner of this family.');
            $this->reroute('/');
        }

        return true;
    }

    public function indexAction(): void {
        $grid = $this->getGeefteeGrid();

        $this->view
            ->setData('FAMILY', $this->getFamily())
            ->setData('GRID_ACTIONS_PARAMS', ['family_id' => $this->getFamily()->getId()])
            ->getLayout()
            ->setBlockTemplate('content', 'family/manage/transfer/index.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    public function gridAction(): void {
        $grid = $this->getGeefteeGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    public function confirmAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $this->breadcrumbs->addCrumb('Confirmation');

        $family = $this->getFamily();
        $geefteeId = $this->getRequestParam('geeftee_id');

        try {
            /** @var Geeftee $geeftee */
            $geeftee = $this->getModelRepository(Geeftee::ENTITY_TYPE)
                ->get($geefteeId);
        }
        catch (PermissionException) {
            $this->geefterSession->addErrorMessage('Invalid geeftee.');
            $this->redirectReferer();
        }

        if (! $family->isMember($geeftee)) {
            $this->geefterSession->addErrorMessage('The new owner must be a member of the family first.');
            $this->redirectReferer();
        }

        $this->familyHelper->transfer($family, $geeftee->getGeefter());

        $this->geefterSession->addSuccessMessage(
            'Family transfered successfully! {0} is now its new owner.',
            [$geeftee->getName()]
        );

        $this->reroute('family_manage');
    }

    public function getGeefteeGrid(): \Geeftlist\View\Grid\Geeftee {
        $grid = parent::getGeefteeGrid();

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'ajax_url' => $this->getUrl('*/grid', ['family_id' => $this->getFamily()->getId(), '_referer' => true]),
                'family' => $this->getFamily()
            ]);
            $grid->attachInstanceListener('collection::new', function ($target, array $args): void {
                /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $geefteeCollection */
                $geefteeCollection = $args['collection'];

                $geefteeCollection
                    ->addFieldToFilter('family', $this->getFamily()->getId())
                    ->addFieldToFilter('geefter_id', ['null' => false])
                    ->addFieldToFilter(
                        'main_table.geeftee_id',
                        ['nin' => [$this->geefterSession->getGeeftee()->getId()]]
                    );
            });

            $isConfigured = true;
        }

        return $grid;
    }
}
