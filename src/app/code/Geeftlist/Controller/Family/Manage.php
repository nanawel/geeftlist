<?php

namespace Geeftlist\Controller\Family;

use Geeftlist\Form\Element\Family\VisibilityRadioGroup;
use Geeftlist\Model\Family as FamilyModel;
use Geeftlist\Model\Family\MembershipRequest;
use Geeftlist\Model\ResourceModel\Db\Family\Collection;
use Geeftlist\Model\Share\Constants;

class Manage extends Family
{
    protected \Geeftlist\Service\PermissionInterface $permissionService;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        \Geeftlist\View\Grid\Family $familyGrid,
        protected \Geeftlist\Helper\Family $familyHelper,
        protected \Geeftlist\Helper\Family\Membership $membershipHelper,
        protected \Geeftlist\View\Grid\Geeftee $geefteeGrid
    ) {
        $this->permissionService = $context->getPermissionService();
        parent::__construct($context, $familyGrid);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        $this->breadcrumbs->addCrumb(
            $this->__('Manage {0}', $this->__('Families')),
            'family_manage/index'
        );

        return true;
    }

    public function getFamily(bool $skipMembershipCheck = false, bool $errorIfMissing = true): FamilyModel {
        if (! $this->family) {
            $family = parent::getFamily();
            if ($family->getId() && (! $skipMembershipCheck && ! $family->isMember($this->geefterSession->getGeeftee()))) {
                $this->geefterSession->addErrorMessage('Invalid family');
                $this->redirectReferer();
            }
        }

        if (! $this->family->getId() && $errorIfMissing) {
            $this->geefterSession->addErrorMessage('Invalid family');
            $this->redirectReferer();
        }

        return $this->family;
    }

    public function indexAction(): void {
        $grid = $this->getFamilyGrid();
        $grid->configure([
            'ajax_url' => $this->getUrl('*/grid', ['_referer' => true]),
            'has_selectable_rows' => false
        ]);

        $this->view->getLayout()
            ->setBlockTemplate('content', 'family/manage/index.phtml')
            ->setBlockTemplate('toolbar', 'family/manage/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    public function gridAction(): void {
        $grid = $this->getFamilyGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    public function editAction(): void {
        $family = $this->getFamily(false, false);
        $this->assertCanEdit();

        $this->breadcrumbs->addCrumb(
            $family->getId() ? $family->getName() : 'New family',
            '*/*/*'
        );

        $formAction = $this->getUrl(
            '*/save',
            $family->getId() ? ['family_id' => $family->getId()] : []
        );

        $visibilityRadioGroup = $this->modelFactory->make(
            VisibilityRadioGroup::class,
            [
                'data' => [
                    'name'   => 'visibility',
                    'id'     => 'visibility',
                    'value'  => $family->getVisibility() ?: \Geeftlist\Model\Family\Field\Visibility::PROTECTED
                ]
            ]
        );

        $this->view
            ->setData('FORM_ACTION', $formAction)
            ->setData('FAMILY', $family)
            ->setData('VISIBILITY_RADIOGROUP', $visibilityRadioGroup)
            ->getLayout()
            ->setBlockTemplate('content', 'family/manage/edit.phtml')
            ->setBlockTemplate('form', 'family/manage/edit/form.phtml')
            ->setBlockConfig('toolbar', [
                'type'       => \Geeftlist\Block\Form\Toolbar\Family::class,
                'family'     => $family,
                'delete_url' => $this->getUrl('*/delete', [
                    'family_id' => $family->getId(),
                    '_referer' => '_keep',
                    '_csrf' => true
                ])
            ]);
    }

    public function saveAction(): void {
        $family = $this->getFamily(false, false);
        $this->assertCanEdit();

        $isUpdate = (bool) $family->getId();
        $post = $this->getPost();

        $post = $this->filterFamilyFormPost($post);

        $family->addData($post);

        $errors = $this->validateFamilyFormPost($post, $family);
        if ($errors !== []) {
            foreach ($errors as $e) {
                $this->geefterSession->addErrorMessage($e);
            }

            $this->redirectReferer();
            return;
        }

        $this->getModelRepository(\Geeftlist\Model\Family::ENTITY_TYPE)->save($family);

        if ($isUpdate) {
            $this->geefterSession->addSuccessMessage(
                'Family <strong>{0}</strong> updated successfully!',
                [$this->escapeHtml($family->getName())],
                ['no_escape' => true]
            );
        }
        else {
            $this->geefterSession->addSuccessMessage(
                'Family <strong>{0}</strong> created successfully!',
                [$this->escapeHtml($family->getName())],
                ['no_escape' => true]
            );
        }

        $this->reroute('*/index');
    }

    public function deleteAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $family = $this->getFamily();
        $this->assertCanEdit();

        $this->getModelRepository(\Geeftlist\Model\Family::ENTITY_TYPE)
            ->delete($family);
        $this->geefterSession->addSuccessMessage('Family deleted successfully!');

        $this->redirectReferer();
    }

    public function joinAction(): void {
        $family = $this->getFamily(true);

        $membershipRequest = $this->membershipHelper->request(
            $family,
            $this->geefterSession->getGeeftee()
        );
        match ($membershipRequest->getDecisionCode()) {
            MembershipRequest::DECISION_CODE_ACCEPTED => $this->geefterSession->addSuccessMessage(
                "You're now a member of {0}! Check out its <a href=\"{1}\">gifts list</a> now!",
                [
                    $this->escapeHtml($family->getName()),
                    $this->getUrl('family_explore/index', ['family_id' => $family->getId()])
                ],
                ['no_escape' => true]
            ),
            MembershipRequest::DECISION_CODE_PENDING => $this->geefterSession
                ->addSuccessMessage("Your request has been processed successfully.")
                ->addWarningMessage(
                    "The owner of the family {0} must approve your membership first.",
                    [$family->getName()]
                ),
            MembershipRequest::DECISION_CODE_REJECTED => $this->geefterSession
                ->addErrorMessage("Membership rejected :("),
            default => throw new \UnexpectedValueException('Something went wrong. You might want to try again later.'),
        };

        $this->redirectReferer();
    }

    public function leaveAction(): void {
        $family = $this->getFamily();
        $geeftee = $this->geefterSession->getGeeftee();

        if (! $family->getId() || ! $family->isMember($geeftee)) {
            $this->geefterSession->addErrorMessage('Invalid family');
            $this->redirectReferer();
            return;
        }

        $family->removeGeeftee($geeftee);
        $this->geefterSession->addSuccessMessage(
            "You're no longer a member of the family <strong>{0}</strong>.",
            [$this->escapeHtml($family->getName())],
            ['no_escape' => true]
        );

        $this->redirectReferer();
    }

    protected function getFamilyCollection(): Collection {
        /** @var Collection $collection */
        $collection = $this->modelResourceFactory->resolveMakeCollection(FamilyModel::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', $this->geefterSession->getGeeftee()->getId())
            ->orderBy('name');

        return $collection;
    }

    protected function filterFamilyFormPost(array $familyData): array {
        return array_map('trim', \OliveOil\array_mask($familyData, [
            'name',
            'visibility'
        ]));
    }

    protected function validateFamilyFormPost(array &$familyData, FamilyModel $family): array {
        return $this->dataValidationErrorProcessor->process(
            $family->validate(),
            $familyData,
            [
                'name'       => $this->i18n->tr('family.field||name'),
                'visibility' => $this->i18n->tr('family.field||visibility'),
            ]
        );
    }

    protected function assertCanEdit($family = null): void {
        if ($family === null) {
            $family = $this->getFamily(true, false);
        }

        if (!$this->permissionService->isAllowed($family, Constants::ACTION_EDIT)) {
            $this->geefterSession->addErrorMessage('You are not allowed to edit this family.');
            $this->redirectReferer();
        }
    }

    public function getFamilyGrid(): \Geeftlist\View\Grid\Family {
        $grid = parent::getFamilyGrid();

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->attachInstanceListener('collection::new', function ($target, array $args): void {
                /** @var \Geeftlist\Model\ResourceModel\Db\Family\Collection $familyCollection */
                $familyCollection = $args['collection'];

                $familyCollection->addFieldToFilter('geeftee', $this->geefterSession->getGeeftee()->getId());
            });
        }

        return $grid;
    }

    /**
     * To be overridden by child classes
     */
    public function getGeefteeGrid(): \Geeftlist\View\Grid\Geeftee {
        return $this->geefteeGrid;
    }
}
