<?php
namespace Geeftlist\Controller\Family;

use OliveOil\Core\Exception\NoSuchEntityException;

class Family extends \Geeftlist\Controller\Http\AbstractController
{
    protected ?\Geeftlist\Model\Family $family = null;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\View\Grid\Family $familyGrid
    ) {
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        // For UT
        $this->family = null;

        return true;
    }

    public function getFamily(): \Geeftlist\Model\Family {
        if (! $this->family) {
            if ($familyId = $this->getRequestParam('family_id')) {
                /** @var \Geeftlist\Model\Family|null $family */
                $family = $this->getModelRepository(\Geeftlist\Model\Family::ENTITY_TYPE)
                    ->get($familyId);
                $this->family = $family;

                if (! $this->family) {
                    throw new NoSuchEntityException('Invalid family');
                }
            }
            else {
                /** @var \Geeftlist\Model\Family $family */
                $family = $this->getModelRepository(\Geeftlist\Model\Family::ENTITY_TYPE)->newModel();
                $this->family = $family;
            }
        }

        return $this->family;
    }

    /**
     * @return \Geeftlist\View\Grid\Family
     */
    public function getFamilyGrid() {
        $grid = $this->familyGrid;

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'current_geefter' => $this->geefterSession->getGeefter(),
                'current_geeftee' => $this->geefterSession->getGeeftee(),
            ]);
        }

        return $grid;
    }
}
