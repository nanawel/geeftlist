<?php

namespace Geeftlist\Controller\Family;

use Geeftlist\Model\Gift\Field\Status;

class Explore extends Family
{
    public function getFamily(): \Geeftlist\Model\Family {
        $family = parent::getFamily();
        if (! $family->getId()) {
            $this->redirectReferer();
        }

        if (! $family->isMember($this->geefterSession->getGeeftee())) {
            $this->geefterSession->addErrorMessage('You are not a member of this family :(');
            $this->redirectReferer();
        }

        return $family;
    }

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb(
            $this->__('{0} Family', $this->getFamily()->getName()),
            '*/*/*'
        );

        $criteriaAppenders = [
            static function ($entityType, array &$criteria): void {
                // Hide unavailable gifts
                if ($entityType == \Geeftlist\Model\Gift::ENTITY_TYPE) {
                    $criteria['filter']['status'][] = ['eq' => Status::AVAILABLE];
                }
            },
            function ($entityType, array &$criteria): void {
                if ($entityType == \Geeftlist\Model\Geeftee::ENTITY_TYPE) {
                    $criteria['filter']['family'][] = ['eq' => $this->getFamily()->getId()];
                }
            }
        ];

        $this->view
            ->setData('FAMILY', $this->getFamily())
            ->setData('IS_OWNER', $this->getFamily()->isOwner($this->geefterSession->getGeefter()))
            ->getLayout()
            ->setBlockTemplate('toolbar', 'family/explore/toolbar.phtml')
            ->setBlockTemplate('list', 'family/explore/list.phtml')
            ->setBlockConfig('geeftee_list', [
                'type'               => \Geeftlist\Block\Gift::class,
                'template'           => 'common/geeftee_list.phtml',
                'criteria_appenders' => $criteriaAppenders
            ])
            ->setBlockTemplate('gift_list', 'common/gift/list.phtml')
            ->setBlockTemplate('gift_list_header', 'common/gift/list/header.phtml')
            ->setBlockConfig('geeftee_card', [
                'type'               => \Geeftlist\Block\Geeftee::class,
                'template'           => 'geeftee/card.phtml',
                'show_claim_link'    => false,
            ])
            ->setBlockConfig('geeftee_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small']
            ])
            ->setBlockConfig('gift', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/list/item.phtml',
                'display_reservations'   => true,
                'badges_block_name'      => 'gift_badges'
            ])
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small']
            ])
            ->setBlockConfig('gift_actions', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/actions.phtml'
            ])
            ->setBlockConfig('gift_reservations', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/reservations.phtml'
            ])
        ;
    }
}
