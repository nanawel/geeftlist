<?php

namespace Geeftlist\Controller\Family;

use Geeftlist\Model\Family as FamilyModel;
use Geeftlist\Model\Family\MembershipRequest;
use Geeftlist\Model\ResourceModel\Db\Family as FamilyResourceModel;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;

class Search extends Family
{
    public function indexAction(): void {
        $this->breadcrumbs->addCrumb(
            'Browse families',
            '*/index'
        );

        $grid = $this->getFamilyGrid();

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'family/search/index.phtml')
            ->setBlockTemplate('toolbar', 'family/search/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig());
    }

    protected function getFamilyCollection() {
        return $this->modelResourceFactory->resolveMakeCollection(FamilyModel::ENTITY_TYPE);
    }

    public function gridAction(): void {
        $grid = $this->getFamilyGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    /**
     * @return \Geeftlist\View\Grid\Family
     */
    public function getFamilyGrid() {
        $grid = parent::getFamilyGrid();

        static $isConfigured = false;
        if (!$isConfigured) {
            $grid->configure([
                'ajax_url' => $this->getUrl('*/grid', ['_referer' => true])
            ]);
            $grid->attachInstanceListener('collection::new', function ($target, array $args): void {
                /** @var \Geeftlist\Model\ResourceModel\Db\Family\Collection $familyCollection */
                $familyCollection = $args['collection'];

                $familyCollection->addJoinRelation('owner')
                    ->addMembersCount();

                // Exclude families with pending membership requests
                $select = new Select();
                $select->from(['membership_request' => FamilyResourceModel\MembershipRequest::MAIN_TABLE])
                    ->where->equalTo(
                        'membership_request.family_id',
                        'main_table.family_id',
                        Predicate::TYPE_IDENTIFIER,
                        Predicate::TYPE_IDENTIFIER
                    )
                    ->AND
                    ->equalTo('membership_request.geeftee_id', $this->geefterSession->getGeeftee()->getId())
                    ->AND
                    ->equalTo('membership_request.decision_code', MembershipRequest::DECISION_CODE_PENDING);
                $familyCollection->getSelect()->where(['NOT EXISTS (?)' => $select]);

                // Exclude families the current geeftee is already a member of
                $familyCollection->addFieldToFilter(
                    'main_table.family_id',
                    ['nin' => $this->geefterSession->getFamilyIds()]
                );
            });

            $isConfigured = true;
        }

        return $grid;
    }
}
