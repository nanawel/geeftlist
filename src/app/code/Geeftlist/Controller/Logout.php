<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;

class Logout extends AbstractController
{
    protected function init()
    {
        $this->nonRestrictedActions = [
            'index',
        ];
    }

    /**
     * @param \Base $fw
     */
    public function indexAction($fw): void {
        $session = $this->geefterSession;
        if ($session->isLoggedIn()) {
            $geefter = $session->getGeefter();
            $session->logout();
            $session->addSuccessMessage('You signed out successfully, see you soon!');

            $this->eventManager->trigger('geefter_logout', $this, [
                'geefter' => $geefter
            ]);
        }

        $this->reroute('/');
    }
}
