<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Geefter\RegistrationDisabledException;
use Geeftlist\Exception\InvitationException;
use OliveOil\Core\Block\Dialog;
use OliveOil\Core\Exception\AppException;
use OliveOil\Core\Model\Security\Captcha;

class Signup extends AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\Validation\Email $emailValidator,
        protected \Geeftlist\Controller\Helper\PasswordForm $passwordFormHelper,
        protected \Geeftlist\Service\Geefter\Account $geefterAccountService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreModelFactory,
        protected \OliveOil\Core\Service\Security\Captcha $captchaService,
        protected \Geeftlist\Service\Invitation $invitationService
    ) {
        parent::__construct($context);
    }

    protected function init(): static
    {
        $this->nonRestrictedActions = [
            'index',
            'post',
            'success',
            'confirm',
            'confirmPost'
        ];

        return $this;
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        if ($this->geefterSession->isLoggedIn()) {
            $this->reroute('dashboard');
            return false;
        }

        return true;
    }

    public function indexAction(): void {
        try {
            $this->checkRegistrationAndInvitation();
        } catch (AppException $e) {
            $this->renderErrorDialog($e->getMessage());
            return;
        }

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/post', ['_query' => '*']))
            ->setData('FORM_DATA', $this->getSession()->getCreateAccountFormData())
            ->getLayout()
            ->setBlockConfig('form', ['template' => 'signup/form.phtml']);

        if ($this->isCaptchaEnabled()) {
            $captcha = $this->captchaService->generate();
            $this->getSession()->setCreateAccountFormCaptcha($captcha);

            $this->view
                ->setData('CAPTCHA', $this->getCaptchaFormElement($captcha));
        }
    }

    public function postAction(): void {
        try {
            $invitation = $this->checkRegistrationAndInvitation();
        } catch (AppException $e) {
            $this->geefterSession->addErrorMessage($e->getMessage());
            $this->reroute('*/index', ['_query' => '*']);
        }
        $post = $this->getPost();

        // Save all POST data but captcha
        $this->getSession()->setCreateAccountFormData(array_diff_key($post, ['captcha' => true]));

        if ($this->isCaptchaEnabled()) {
            if (!$captcha = $this->getSession()->getCreateAccountFormCaptcha()) {
                $this->getSession()->addErrorMessage('Invalid captcha. Please retry.');
                $this->reroute('*/index', ['_query' => '*']);
            }

            /** @var Captcha $captcha */
            $captchaFormElement = $this->getCaptchaFormElement($captcha);

            $captchaAnswer = $captchaFormElement->getAnswer($post);

            if (!$this->captchaService->validate($captcha, $captchaAnswer)) {
                $this->getSession()->addErrorMessage('Invalid captcha answer. Please retry.');
                $this->reroute('*/index', ['_query' => '*']);
            }
        }

        [$geefter, $errorAggregator] = $this->geefterAccountService->signup($post, $invitation);

        if ($errors = $this->dataValidationErrorProcessor->process($errorAggregator, $post)) {
            foreach ($errors as $e) {
                $this->getSession()->addErrorMessage($e);
            }

            $this->reroute('*/index', ['_query' => '*']);
        }
        else {
            $this->getSession()->unsCreateAccountFormData();
            $this->getSession()->unsCreateAccountFormCaptcha();
        }

        if ($geefter->getFlag(\Geeftlist\Service\Geefter\Account::SIGNUP_SUCCESS_NO_CONFIRMATION)) {
            $this->reroute('*/confirm', ['token' => $geefter->getPasswordToken()]);
        }

        $this->getSession()->setSignupSuccessEmail($post['email']);
        $this->reroute('*/success');
    }

    public function successAction(): void {
        if (! $email = $this->getSession()->getSignupSuccessEmail()) {
            $this->reroute('/');
        }

        $this->getSession()->unsSignupSuccessEmail();

        $this->view
            ->setData('EMAIL', $email)
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_SUCCESS,
                'content_blocks' => ['dialog_content'],
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('/'),
                    'text'  => $this->__('Back to home'),
                    'class' => 'button back'
                ]]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'signup/success.pmd'
            ]);
    }

    public function confirmAction() {
        return $this->passwordFormHelper->passwordformAction(
            $this->getRequestParam('token'),
            'signup/confirm/index.phtml',
            '*/confirmPost',
            function ($e): void {
                $this->getSession()->addErrorMessage($e->getMessage());
                $this->responseRedirect->reroute('/');
            }
        );
    }

    public function confirmPostAction() {
        return $this->passwordFormHelper->passwordformPostAction(
            $this->getPost(),
            function ($geefter): void {
                $this->geefterSession->setGeefterAsLoggedIn($geefter);

                $this->getSession()->addSuccessMessage(
                    'Password updated successfully. Welcome {0}!',
                    [$geefter->getUsername()]
                );
                $this->responseRedirect->reroute('dashboard');
            },
            function ($errors): void {
                foreach ($errors as $e) {
                    $this->getSession()->addErrorMessage($e);
                }

                $this->responseRedirect->reroute('*/confirm', ['token' => $this->getPost('token')]);
            },
            function ($e): void {
                $this->getSession()->addErrorMessage($e);
                $this->responseRedirect->reroute('/');
            }
        );
    }

    protected function isCaptchaEnabled(): bool {
        return (bool) $this->appConfig->getValue('CAPTCHA_GEEFTER_SIGNUP_ENABLE');
    }

    protected function getCaptchaFormElement(Captcha $captchaModel): \OliveOil\Core\Form\Element\Captcha {
        return $this->coreModelFactory
            ->make(\OliveOil\Core\Form\Element\Captcha::class, ['data' => [
                'name'          => 'captcha',
                'id'            => 'captcha',
                'captcha_model' => $captchaModel
            ]
        ]);
    }

    protected function checkRegistrationAndInvitation(): ?\Geeftlist\Model\Invitation {
        if ($this->invitationService->isEnabled()) {
            try {
                if ($invitation = $this->invitationService->validateTokenFromRequest($this->getRequest())) {
                    return $invitation;
                }
            } catch (InvitationException $invitationException) {
            }
        }

        if (!$this->appConfig->getValue('GEEFTER_REGISTRATION_ENABLE')) {
            // If there was an error on invitation processing, throw it instead
            if (isset($invitationException)) {
                throw $invitationException;
            }
            throw new RegistrationDisabledException('Sorry, registration is disabled.');
        }

        if (isset($invitationException)) {
            // Notify that the given invitation was invalid, but registration is open so we'll ignore it
            $this->getSession()->addWarningMessage($invitationException->getMessage());
        }

        return null;
    }

    protected function renderErrorDialog(string $message): void {
        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_ERROR,
                'message' => $this->__($message),
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('/'),
                    'text'  => $this->__('Back to home'),
                    'class' => 'button back'
                ]]
            ]);
    }
}
