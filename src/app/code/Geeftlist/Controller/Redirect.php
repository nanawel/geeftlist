<?php

namespace Geeftlist\Controller;


use Geeftlist\Controller\Http\AbstractController;

class Redirect extends AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\Url\RedirectInterface $redirectService
    ) {
        parent::__construct($context);
    }

    public function indexAction(\Base $fw, array $args): void {
        if (! $to = $this->getRequestParam('to')) {
            $this->reroute('/');
        }

        $this->rerouteUrl($this->getRedirectService()->getTargetRedirectUrl($to));
    }

    public function getRedirectService(): \OliveOil\Core\Service\Url\RedirectInterface {
        return $this->redirectService;
    }
}
