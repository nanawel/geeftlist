<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;

class About extends AbstractController
{
    /** @inheritdoc */
    protected array $nonRestrictedActions = ['index'];

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb('About', '*/*');

        $this->view->getLayout()
            ->setBlockConfig('content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'about/index.pmd'
            ])
            ->setBlockConfig('notes', [
                'type' => \Geeftlist\Block\Cms::class,
                'code' => 'home'
            ]);
    }
}
