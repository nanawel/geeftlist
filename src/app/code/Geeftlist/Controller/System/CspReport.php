<?php

namespace Geeftlist\Controller\System;

use Narrowspark\HttpStatus\Exception\UnsupportedMediaTypeException;

class CspReport extends \OliveOil\Core\Controller\Http\AbstractController
{
    public function postAction()
    {
        $logger = $this->getLogger('security');

        $contentType = $this->getRequest()->getHeader('Content-Type');
        if ($contentType === 'application/csp-report') {
            try {
                $logger->notice(json_encode(json_decode($this->fw->get('BODY'), flags: JSON_THROW_ON_ERROR)));
            } catch (\Throwable $throwable) {
                $logger->error('Failed processing CSP report content. Is it valid JSON?');
                $logger->error($throwable, ['exception' => $throwable]);
            }
        } else {
            $logger->warning('Invalid Content-Type sent to report CSP violations: ' . $contentType);

            throw new UnsupportedMediaTypeException($contentType);
        }

        return false;
    }
}
