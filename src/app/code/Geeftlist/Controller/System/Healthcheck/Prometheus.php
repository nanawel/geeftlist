<?php

namespace Geeftlist\Controller\System\Healthcheck;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Geeftlist\Model\Reservation;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use OliveOil\Core\Helper\DateTime;
use Prometheus\RenderTextFormat;
use Wikimedia\IPUtils;

class Prometheus extends \OliveOil\Core\Controller\Http\AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Prometheus\RegistryInterface $collectorRegistry,
        protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        protected \OliveOil\Core\Service\Cache $cacheService,
        protected \OliveOil\Core\Model\App\FlagInterface $appFlag
    ) {
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []) {
        $allowedRanges = is_array($this->appConfig->getValue('HEALTHCHECK_ALLOWED_IPS'))
            ? $this->appConfig->getValue('HEALTHCHECK_ALLOWED_IPS')
            : explode(',', $this->appConfig->getValue('HEALTHCHECK_ALLOWED_IPS'));
        $clientIp = $this->fw->ip();
        if ($clientIp && !IPUtils::isInRanges($clientIp, $allowedRanges)) {
            throw new ForbiddenException();
        }

        // Clear session if any
        $this->getSession()->abortClose(true);

        return parent::onBeforeRoute($fw, $args);
    }

    public function indexAction() {
        try {
            ob_start();
            $this->collectMetrics();

            $result = (new RenderTextFormat())
                ->render($this->collectorRegistry->getMetricFamilySamples());
            ob_end_clean();

            $this->view
                ->getLayout()
                ->setPageConfig([
                    'type'     => \OliveOil\Core\Block\Data::class,
                    'mimetype' => RenderTextFormat::MIME_TYPE,
                    'data'     => $result
                ]);
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable);
            echo 'An error occured. Please check the logs for more information.';

            return false;
        }
    }

    protected function collectMetrics() {
        $this->collectSystemMetrics();
        $this->collectGiftMetrics();
        $this->collectReservationMetrics();
        $this->collectFamilyMetrics();
        $this->collectGeefterMetrics();
        $this->collectGeefteeMetrics();
        $this->collectNotificationMetrics();
        $this->collectCronMetrics();
        $this->collectConnectivityMetrics();

        // Must be at the end
        $this->collectOwnMetrics();
    }

    protected function collectSystemMetrics() {
        $dummyGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'system_info',
            '',
            ['version', 'version_full', 'version_date']
        );
        $appVersion = $this->appConfig->getValue('VERSION');
        $appDate = $this->appConfig->getValue('VERSION_DATE')
            ?: filemtime('vendor/');
        $dummyGauge->set(
            1,
            [
                'version' => preg_replace('/^(.*?)(-[a-f0-9]+)?$/', '\1', (string) $appVersion),
                'version_full' => $appVersion,
                'version_date' => $appDate ? (new \DateTime('@' . $appDate))->format('c') : '',
            ]
        );
    }

    protected function collectGiftMetrics() {
        $entityCountGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'entity_count',
            '',
            ['entity_code', 'count_type']
        );

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Gift::ENTITY_TYPE);
        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Gift::ENTITY_TYPE, 'count_type' => 'total']
        );

        $collection->clear();
        $collection->addFieldToFilter('status', Gift\Field\Status::AVAILABLE);

        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Gift::ENTITY_TYPE, 'count_type' => Gift\Field\Status::AVAILABLE]
        );

        $collection->clear();
        $collection->addFieldToFilter('status', Gift\Field\Status::DELETED);

        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Gift::ENTITY_TYPE, 'count_type' => Gift\Field\Status::DELETED]
        );

        $collection->clear();
        $collection->addFieldToFilter('status', Gift\Field\Status::ARCHIVED);

        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Gift::ENTITY_TYPE, 'count_type' => Gift\Field\Status::ARCHIVED]
        );
    }

    protected function collectReservationMetrics() {
        $entityCountGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'entity_count',
            '',
            ['entity_code', 'count_type']
        );

        /** @var \Geeftlist\Model\ResourceModel\Db\Reservation\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Reservation::ENTITY_TYPE);
        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Reservation::ENTITY_TYPE, 'count_type' => 'total']
        );

        $collection->clear();
        $collection->addFieldToFilter('type', Reservation::TYPE_RESERVATION);

        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Reservation::ENTITY_TYPE, 'count_type' => Reservation::TYPE_RESERVATION]
        );

        $collection->clear();
        $collection->addFieldToFilter('type', Reservation::TYPE_PURCHASE);

        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Reservation::ENTITY_TYPE, 'count_type' => Reservation::TYPE_PURCHASE]
        );
    }

    protected function collectFamilyMetrics() {
        $entityCountGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'entity_count',
            '',
            ['entity_code', 'count_type']
        );

        /** @var \Geeftlist\Model\ResourceModel\Db\Family\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Family::ENTITY_TYPE);
        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Family::ENTITY_TYPE, 'count_type' => 'total']
        );
    }

    protected function collectGeefterMetrics() {
        $entityCountGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'entity_count',
            '',
            ['entity_code', 'count_type']
        );

        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Geefter::ENTITY_TYPE);
        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Geefter::ENTITY_TYPE, 'count_type' => 'total']
        );
    }

    protected function collectGeefteeMetrics() {
        $entityCountGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'entity_count',
            '',
            ['entity_code', 'count_type']
        );

        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Geeftee::ENTITY_TYPE);
        $entityCountGauge->set(
            $collection->count(),
            ['entity_code' => Geeftee::ENTITY_TYPE, 'count_type' => 'total']
        );
    }

    protected function collectNotificationMetrics() {
        $notificationCountGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'notification_count',
            '',
            ['count_type', 'recipient_type']
        );

        /** @var \Geeftlist\Model\ResourceModel\Db\Notification\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Notification::ENTITY_TYPE);
        $notificationCountGauge->set(
            $collection->count(),
            [
                'count_type' => 'total',
                'recipient_type' => 'all'
            ]
        );

        $countByRecipientTypeAndStatus = [];
        /** @var Notification $notification */
        foreach ($collection as $notification) {
            if (!isset($countByRecipientTypeAndStatus[$notification->getRecipientType()][$notification->getStatus()])) {
                $countByRecipientTypeAndStatus[$notification->getRecipientType()][$notification->getStatus()] = 0;
            }

            ++$countByRecipientTypeAndStatus[$notification->getRecipientType()][$notification->getStatus()];
        }

        foreach ($countByRecipientTypeAndStatus as $recipientType => $countByStatus) {
            foreach ($countByStatus as $status => $count) {
                $notificationCountGauge->set(
                    $count,
                    [
                        'count_type' => $status,
                        'recipient_type' => $recipientType
                    ]
                );
            }
        }
    }

    protected function collectCronMetrics() {
        $cronLastRunGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'cron_last_run',
            '',
            ['date_iso']
        );
        $lastRun = $this->appFlag->get('cron/last_run') ?? 0;
        $cronLastRunGauge->set(
            $lastRun,
            ['date_iso' => DateTime::getDateIso('@' . $lastRun)]
        );

        $cronLastSuccessGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'cron_last_success',
            '',
            ['date_iso']
        );
        $lastSuccess = $this->appFlag->get('cron/last_success') ?? 0;
        $cronLastSuccessGauge->set(
            $lastSuccess,
            ['date_iso' => DateTime::getDateIso('@' . $lastSuccess)]
        );

        $cronLastSuccessDurationGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'cron_last_success_duration',
            '',
            []
        );
        $lastSuccessDuration = $this->appFlag->get('cron/last_success_duration') ?? 0;
        $cronLastSuccessDurationGauge->set($lastSuccessDuration);
    }

    protected function collectConnectivityMetrics() {
        $httpOutgoingConnection = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'outgoing_http_connection',
            '',
            ['url', 'delay']
        );

        $url = $this->appConfig->getValue('HEALTHCHECK_REMOTE_HTTP_URL', 'http://example.org');
        $expectedCode = $this->appConfig->getValue('HEALTHCHECK_REMOTE_HTTP_CODE', 200);

        $start = microtime(true);
        $time = 0;
        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $time = microtime(true) - $start;
            if ($result === false) {
                $this->logger->warning('cURL error: ' . curl_error($ch));
                $value = 0;
            } else {
                $value = curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == $expectedCode;
            }
        } catch (\Throwable $throwable) {
            $this->logger->warning($throwable);
            $value = 0;
        }

        $httpOutgoingConnection->set(
            $value,
            ['url' => $url, 'delay' => $time]
        );
    }

    /**
     * @throws \Prometheus\Exception\MetricsRegistrationException
     */
    protected function collectOwnMetrics() {
        $dummyGauge = $this->collectorRegistry->getOrRegisterGauge(
            'geeftlist',
            'collect_time',
            '',
            []
        );
        $dummyGauge->set(microtime(true) - $this->fw->get('TIME'));
    }
}
