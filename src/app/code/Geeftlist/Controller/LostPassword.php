<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Block\Dialog;

class LostPassword extends AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\Validation\Email $emailValidator,
        protected \Geeftlist\Controller\Helper\PasswordForm $passwordFormHelper,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService
    ) {
        parent::__construct($context);
    }

    protected function init(): static
    {
        $this->nonRestrictedActions = [
            'index',
            'post',
            'success',
            'reset',
            'resetPost',
        ];

        return $this;
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        if ($this->geefterSession->isLoggedIn()) {
            $this->reroute('dashboard');
            return false;
        }

        return true;
    }


    /**
     * @param \Base $fw
     */
    public function indexAction($fw): void {
        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/post'))
            ->getLayout()
            ->setBlockConfig('content', ['template' => 'lostpassword/index.phtml'])
            ->setBlockConfig('form', ['template' => 'lostpassword/form.phtml']);
    }

    /**
     * @param \Base $fw
     */
    public function postAction($fw): void {
        $post = $this->getPost();

        if (!isset($post['email']) || !$post['email']) {
            $this->getSession()->addErrorMessage('Missing field: {0}.', [$this->__('email')]);
            $this->reroute('*');
        }

        $repository = $this->getModelRepository(Geefter::ENTITY_TYPE);
        /** @var Geefter $geefter */
        $geefter = $repository->get($post['email'], 'email');
        if ($geefter) {
            $geefter->generatePasswordToken();
            $repository->save($geefter);
            $this->getGeefterEmailService()->sendLostPasswordEmail($geefter);

            $this->eventManager->trigger('reset_password_request', $this, [
                'geefter' => $geefter,
                'email'   => $post['email']
            ]);
        }
        else {
            $this->eventManager->trigger('invalid_reset_password_request', $this, [
                'email' => $post['email']
            ]);
            $this->getLogger()->notice('Password reset requested for non existing account: ' . $post['email']);
        }

        $this->getSession()->regenerate()
            ->setData('lostpassword_success', true);
        $this->reroute('*/success');
    }

    public function successAction(): void {
        if (! $this->getSession()->getData('lostpassword_success')) {
            $this->reroute('login');
        }

        $this->getSession()->unsetData('lostpassword_success');

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_SUCCESS,
                'content_blocks' => ['dialog_content'],
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('/'),
                    'text'  => $this->__('Back to home'),
                    'class' => 'button back'
                ]]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \OliveOil\Core\Block\LocalizedTemplate::class,
                'template' => 'lostpassword/success.pmd'
            ]);
    }

    public function resetAction() {
        return $this->getPasswordFormHelper()->passwordformAction(
            $this->getRequestParam('token'),
            'resetpassword/index.phtml',
            '*/resetPost',
            function ($e): void {
                $this->getSession()->addErrorMessage($e->getMessage());
                $this->responseRedirect->reroute('/');
            }
        );
    }

    public function resetPostAction() {
        return $this->getPasswordFormHelper()->passwordformPostAction(
            $this->getPost(),
            function ($geefter): void {
                $this->getSession()->addSuccessMessage('Password updated successfully, you may now log in.');
                $this->responseRedirect->reroute('login');
            },
            function ($errors): void {
                foreach ($errors as $e) {
                    $this->getSession()->addErrorMessage($e);
                }

                $this->responseRedirect->reroute('*/reset', ['token' => $this->getPost('token')]);
            },
            function ($e): void {
                $this->getSession()->addErrorMessage($e);
                $this->responseRedirect->reroute('/');
            }
        );
    }

    public function getEmailValidator(): \OliveOil\Core\Service\Validation\Email {
        return $this->emailValidator;
    }

    public function getPasswordFormHelper(): \Geeftlist\Controller\Helper\PasswordForm {
        return $this->passwordFormHelper;
    }

    public function getGeefterEmailService(): \Geeftlist\Service\Geefter\Email {
        return $this->geefterEmailService;
    }
}
