<?php

namespace Geeftlist\Controller\GiftList;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Form\Element\GiftList\StatusRadioGroup;
use Geeftlist\Form\Element\GiftList\VisibilityRadioGroup;
use Geeftlist\Model\GiftList as GiftListModel;
use Geeftlist\Model\GiftList\Field\Status;
use Geeftlist\Model\Share\Constants;
use OliveOil\Core\Exception\NoSuchEntityException;

class Manage extends AbstractController
{
    protected \Geeftlist\Service\Permission $permissionService;

    /** @var \Geeftlist\Model\GiftList|null */
    protected $giftList;

    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\View\Grid\GiftList $giftListGrid
    ) {
        $this->permissionService = $context->getPermissionService();
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        // Needed for UT
        $this->giftList = null;
        $this->giftListGrid->reset();

        return parent::onBeforeRoute($fw, $args);
    }

    public function indexAction(): void {
        $grid = $this->getGiftListGrid();
        $grid->configure('ajax_url', $this->getUrl('*/grid/*', ['_referer' => true]));

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'giftlist/manage/index.phtml')
            ->setBlockTemplate('toolbar', 'giftlist/manage/toolbar.phtml')
            ->setBlockConfig('grid', $grid->getTableBlockConfig())
        ;
    }

    public function gridAction(): void {
        $grid = $this->getGiftListGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    public function editAction(): void {
        $giftList = $this->getGiftList();
        $this->assertCanEdit();

        $this->breadcrumbs->addCrumb(
            $giftList->getId() ? $giftList->getName() : 'New gift list',
            '*/*/*'
        );

        // Not used yet but included in the form anyway
        $visibilityRadioGroup = $this->modelFactory->make(
            VisibilityRadioGroup::class,
            [
                'data' => [
                    'name'   => 'visibility',
                    'id'     => 'visibility',
                    'value'  => $giftList->getVisibility() ?: \Geeftlist\Model\GiftList\Field\Visibility::PRIVATE
                ]
            ]
        );
        $statusRadioGroup = $this->modelFactory->make(
            StatusRadioGroup::class,
            [
                'data' => [
                    'name'   => 'status',
                    'id'     => 'status',
                    'value'  => $giftList->getStatus() ?? Status::ACTIVE
                ]
            ]
        );

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/save', ['_referer' => '_keep']))
            ->setData('GIFTLIST', $giftList)
            ->setData('VISIBILITY_RADIOGROUP', $visibilityRadioGroup)
            ->setData('STATUS_RADIOGROUP', $statusRadioGroup)
            ->getLayout()
            ->setBlockTemplate('content', 'giftlist/manage/edit.phtml')
            ->setBlockTemplate('form', 'giftlist/manage/edit/form.phtml')
            ->setBlockConfig('toolbar', [
                'type'       => \Geeftlist\Block\Form\Toolbar\GiftList::class,
                'giftList'   => $giftList,
                'delete_url' => $this->getUrl(
                    '*/delete',
                    ['giftlist_id' => $giftList->getId(), '_referer' => $this->getUrl('*/index'), '_csrf' => true]
                )
            ])
            ->setBlockConfig('confirm-leave', [
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => 'form/confirm-leave.phtml',
            ])
            ->addChild('before_body_end', 'confirm-leave')
        ;
    }

    public function saveAction(): void {
        $giftList = $this->getGiftList();
        $this->assertCanEdit();

        $isUpdate = (bool) $giftList->getId();
        $post = $this->getPost();

        $post = $this->filterGiftListFormPost($post);

        $giftList->addData($post);

        $errors = $this->validateGiftListFormPost($post, $giftList);
        if ($errors !== []) {
            foreach ($errors as $e) {
                $this->geefterSession->addErrorMessage($e);
            }

            $this->redirectReferer();
            return;
        }

        $this->getModelRepository(\Geeftlist\Model\GiftList::ENTITY_TYPE)
            ->save($giftList);

        if ($isUpdate) {
            $this->geefterSession->addSuccessMessage(
                'List <strong>{0}</strong> updated successfully!',
                [$this->escapeHtml($giftList->getName())],
                ['no_escape' => true]
            );
        }
        else {
            $this->geefterSession->addSuccessMessage(
                'List <strong>{0}</strong> created successfully!',
                [$this->escapeHtml($giftList->getName())],
                ['no_escape' => true]
            );
        }

        $this->redirectReferer();
    }

    public function deleteAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $giftList = $this->getGiftList();

        $this->getModelRepository(\Geeftlist\Model\GiftList::ENTITY_TYPE)
            ->delete($giftList);
        $this->geefterSession->addSuccessMessage('List deleted successfully!');

        $this->redirectReferer();
    }

    public function getGiftList() {
        if (! $this->giftList) {
            if (
                ($giftListId = $this->getPost('giftlist_id'))
                || ($giftListId = $this->getRequestParam('giftlist_id'))
            ) {
                try {
                    /** @var \Geeftlist\Model\GiftList|null $giftList */
                    $giftList = $this->getModelRepository(\Geeftlist\Model\GiftList::ENTITY_TYPE)
                        ->get($giftListId);
                } catch (PermissionException) {
                    $this->geefterSession->addErrorMessage('You are not allowed to view this list.');
                    $this->redirectReferer();
                }

                if (! $giftList) {
                    throw new NoSuchEntityException('Invalid list');
                }

                $this->giftList = $giftList;
            }
            else {
                /** @var \Geeftlist\Model\GiftList $giftList */
                $giftList = $this->getModelRepository(\Geeftlist\Model\GiftList::ENTITY_TYPE)->newModel();
                $this->giftList = $giftList;
            }
        }

        return $this->giftList;
    }

    public function getGiftListGrid(): \Geeftlist\View\Grid\GiftList {
        if (!$this->giftListGrid->getFlag(self::class . '::isConfigured')) {
            $this->giftListGrid->configure('visible_columns', [
                    'name',
                    'description',
                    'visibility',
                    'status',
                    'actions'
                ]);

            $this->giftListGrid->setFlag(self::class . '::isConfigured', true);
        }

        return $this->giftListGrid;
    }

    protected function filterGiftListFormPost(array $giftlistData): array {
        return array_map('trim', \OliveOil\array_mask($giftlistData, [
            'name',
            'visibility',
            'status',
        ]));
    }

    protected function validateGiftListFormPost(array &$giftlistData, GiftListModel $giftlist): array {
        return $this->dataValidationErrorProcessor->process(
            $giftlist->validate(),
            $giftlistData,
            [
                'name'       => $this->i18n->tr('giftlist.field||name'),
                'visibility' => $this->i18n->tr('giftlist.field||visibility'),
                'status'     => $this->i18n->tr('giftlist.field||status'),
            ]
        );
    }

    protected function assertCanEdit($giftList = null) {
        if ($giftList === null) {
            $giftList = $this->getGiftList();
        }

        if (!$this->permissionService->isAllowed($giftList, Constants::ACTION_EDIT)) {
            $this->geefterSession->addErrorMessage('You are not allowed to edit this list.');
            $this->redirectReferer();
        }
    }
}
