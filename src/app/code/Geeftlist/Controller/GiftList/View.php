<?php

namespace Geeftlist\Controller\GiftList;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Model\GiftList\Field\Status;
use OliveOil\Core\Block\Dialog;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Model\RepositoryInterface;

class View extends AbstractController
{
    /** @var \Geeftlist\Model\GiftList|null */
    protected $giftList;

    /**
     * @inheritDoc
     */
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        // Needed for UT
        $this->giftList = null;

        return parent::onBeforeRoute($fw, $args);
    }

    public function indexAction(): void {
        try {
            $giftList = $this->getGiftList();
        }
        catch (\Throwable) {
            $this->geefterSession->addErrorMessage($this->__('Sorry, you cannot view this list.'));
            $this->redirectReferer();
        }

        $this->breadcrumbs->addCrumb('My lists')
            ->addCrumb($giftList->getName());

        $giftIds = $giftList->getGiftIds();
        $criteriaAppenders = [
            static function ($entityType, array &$criteria) use ($giftIds, $giftList): void {
                switch ($entityType) {
                    case \Geeftlist\Model\Geeftee::ENTITY_TYPE:
                        $criteria['filter']['gifts'][] = ['in' => $giftIds];
                        break;

                    case \Geeftlist\Model\Gift::ENTITY_TYPE:
                        $criteria['filter'][RepositoryInterface::WILDCARD_ID_FIELD][] = ['in' => $giftIds];
                        // Hide unavailable gifts here, unless the list itself is archived
                        if ($giftList->getStatus() == Status::ACTIVE) {
                            $criteria['filter']['status'][] = ['eq' => \Geeftlist\Model\Gift\Field\Status::AVAILABLE];
                        }

                        break;
                }
            }
        ];

        $isEmpty = !(bool) $this->modelResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE)
            ->addFieldToFilter('giftlist', $giftList->getId())
            ->count();

        $emptyMessages = [
            $this->__('This list is currently empty :('),
            $this->__(
                'Why not <a href="{0}">browsing your families</a> and adding some gifts to it?',
                $this->getUrl('relative_explore')
            )
        ];

        $this->view
            ->setData('GIFTLIST', $giftList)
            ->setData('IS_EMPTY', $isEmpty)
            ->getLayout()
            ->setBlockTemplate('toolbar', 'giftlist/view/toolbar.phtml')
            ->setBlockConfig('list', [
                'type'               => \Geeftlist\Block\Gift::class,
                'template'           => 'relative/explore/list.phtml',
                'criteria_appenders' => $criteriaAppenders
            ])
            ->setBlockConfig('geeftee_list', [
                'type'                       => \Geeftlist\Block\Gift::class,
                'template'                   => 'common/geeftee_list.phtml',
                'criteria_appenders'         => $criteriaAppenders,
                'show_empty_tabs'            => false,
                'show_current_geeftee_first' => false
            ])
            ->setBlockTemplate('gift_list', 'common/gift/list.phtml')
            ->setBlockTemplate('gift_list_header', 'common/gift/list/header.phtml')
            ->setBlockConfig('geeftee_card', [
                'type'               => \Geeftlist\Block\Geeftee::class,
                'template'           => 'geeftee/card.phtml',
                'show_claim_link'    => false
            ])
            ->setBlockConfig('geeftee_badges', [
                'type'                   => \Geeftlist\Block\Badges::class,
                'template'               => 'common/badge_list.phtml',
                'badge_classes'          => ['small'],
            ])
            ->setBlockConfig('gift', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/list/item.phtml',
                'display_reservations'   => true,
                'badges_block_name'      => 'gift_badges'
            ])
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small'],
            ])
            ->setBlockConfig('gift_actions', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/actions.phtml'
            ])
            ->setBlockConfig('gift_reservations', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/reservations.phtml'
            ])
            ->setBlockConfig('empty_dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_ERROR,
                'message' => $emptyMessages,
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('dashboard'),
                    'text'  => $this->__('Back to dashboard'),
                    'class' => 'button back'
                ]]
            ])
        ;
    }

    public function getGiftList() {
        if (! $this->giftList) {
            $giftListId = $this->getRequestParam('giftlist_id');
            /** @var \Geeftlist\Model\GiftList|null $giftList */
            $giftList = $this->getModelRepository(\Geeftlist\Model\GiftList::ENTITY_TYPE)
                ->get($giftListId);

            if (! $giftList) {
                throw new NoSuchEntityException('Invalid list');
            }

            $this->giftList = $giftList;
        }

        return $this->giftList;
    }
}
