<?php

namespace Geeftlist\Controller\Relative;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Model\Gift\Field\Status;

class Explore extends AbstractController
{
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);

        return true;
    }

    public function indexAction(): void {
        $this->breadcrumbs->addCrumb('My relatives', '*/*');

        $criteriaAppenders = [
            static function ($entityType, array &$criteria): void {
                // Hide unavailable gifts
                if ($entityType == \Geeftlist\Model\Gift::ENTITY_TYPE) {
                    $criteria['filter']['status'][] = ['eq' => Status::AVAILABLE];
                }
            }
        ];

        $this->view
            ->getLayout()
            ->setBlockTemplate('toolbar', 'relative/explore/toolbar.phtml')
            ->setBlockConfig('list', [
                'type'     => \Geeftlist\Block\Gift::class,
                'template' => 'relative/explore/list.phtml',
                'criteria_appenders' => $criteriaAppenders
            ])
            ->setBlockConfig('geeftee_list', [
                'type'     => \Geeftlist\Block\Gift::class,
                'template' => 'common/geeftee_list.phtml',
                'criteria_appenders' => $criteriaAppenders
            ])
            ->setBlockTemplate('gift_list', 'common/gift/list.phtml')
            ->setBlockTemplate('gift_list_header', 'common/gift/list/header.phtml')
            ->setBlockConfig('geeftee_card', [
                'type'               => \Geeftlist\Block\Geeftee::class,
                'template'           => 'geeftee/card.phtml',
                'show_claim_link'    => false
            ])
            ->setBlockConfig('geeftee_badges', [
                'type'                   => \Geeftlist\Block\Badges::class,
                'template'               => 'common/badge_list.phtml',
                'badge_classes'          => ['small'],
            ])
            ->setBlockConfig('gift', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/list/item.phtml',
                'display_reservations'   => true,
                'badges_block_name'      => 'gift_badges'
            ])
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small'],
            ])
            ->setBlockConfig('gift_actions', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/actions.phtml'
            ])
            ->setBlockConfig('gift_reservations', [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/reservations.phtml'
            ])
        ;
    }
}
