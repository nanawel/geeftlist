<?php

namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Invitation\UnavailableException;
use OliveOil\Core\Model\Security\Captcha;

class Invitation extends AbstractController
{
    protected \Geeftlist\Service\PermissionInterface $permissionService;

    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreModelFactory,
        protected \OliveOil\Core\Service\Security\Captcha $captchaService,
        protected \Geeftlist\Service\Invitation $invitationService
    ) {
        $this->permissionService = $context->getPermissionService();
        parent::__construct($context);
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        $this->assertCanCreateInvitation();

        return true;
    }

    public function indexAction(): void {
        $this->forward('*/create');
    }

    public function createAction(): void {
        $this->breadcrumbs->addCrumb(
            $this->__('Create an invitation'),
        );

        $formAction = $this->getUrl('*/generate');

        $this->view
            ->setData('FORM_ACTION', $formAction)
            ->getLayout()
            ->setBlockTemplate('content', 'invitation/create.phtml')
            ->setBlockTemplate('form', 'invitation/create/form.phtml')
        ;

        if ($this->isCaptchaEnabled()) {
            $captcha = $this->captchaService->generate();
            $this->getSession()->setCreateInvitationFormCaptcha($captcha);

            $this->view
                ->setData('CAPTCHA', $this->getCaptchaFormElement($captcha));
        }

        $this->getSession()->setCreateInvitationFormNonce(time());
    }

    public function generateAction(): void {
        $post = $this->getPost();

        $this->breadcrumbs->addCrumb(
            $this->__('Create an invitation'),
        );

        if (!$this->getSession()->getCreateInvitationFormNonce()) {
            // Silently redirect to form
            $this->reroute('*/create');
        }
        $this->getSession()->unsCreateInvitationFormNonce();

        if ($this->isCaptchaEnabled()) {
            if (!$captcha = $this->getSession()->getCreateInvitationFormCaptcha()) {
                $this->getSession()->addErrorMessage('Invalid captcha. Please retry.');
                $this->reroute('*/create');
            }

            /** @var Captcha $captcha */
            $captchaFormElement = $this->getCaptchaFormElement($captcha);

            $captchaAnswer = $captchaFormElement->getAnswer($post);

            if (!$this->captchaService->validate($captcha, $captchaAnswer)) {
                $this->getSession()->addErrorMessage('Invalid captcha answer. Please retry.');
                $this->reroute('*/create');
            }

            $this->getSession()->unsCreateInvitationFormCaptcha();
        }

        $invitationLink = $this->invitationService->generateSignupLink($this->geefterSession->getGeefter());

        $this->getSession()->addSuccessMessage('Invitation link generated successfully!');

        $this->view
            ->setData('INVITATION_LINK', $invitationLink)
            ->getLayout()
            ->setBlockTemplate('content', 'invitation/generate.phtml')
        ;
    }

    protected function getCaptchaFormElement(Captcha $captchaModel): \OliveOil\Core\Form\Element\Captcha {
        return $this->coreModelFactory
            ->make(\OliveOil\Core\Form\Element\Captcha::class, ['data' => [
                'name'          => 'captcha',
                'id'            => 'captcha',
                'captcha_model' => $captchaModel
            ]
        ]);
    }

    protected function isCaptchaEnabled(): bool {
        return (bool) $this->appConfig->getValue('CAPTCHA_INVITATION_CREATION_ENABLE');
    }

    /**
     * @throws UnavailableException
     */
    protected function assertCanCreateInvitation(): void {
        $this->invitationService->assertEnabled();
    }
}
