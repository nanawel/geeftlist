<?php
namespace Geeftlist\Controller;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Model\SessionInterface;

class Login extends AbstractController
{
    public function __construct(
        \Geeftlist\Controller\Http\Context $context,
        protected \Geeftlist\Service\Authentication\AuthenticationInterface $authenticationService
    ) {
        parent::__construct($context);
    }

    protected function init() {
        $this->nonRestrictedActions = [
            'index',
            'post'
        ];
    }

    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        if ($this->geefterSession->isLoggedIn()) {
            $this->rerouteOnLoggedIn();
        }

        return true;
    }

    /**
     * @param \Base $fw
     */
    public function indexAction($fw): void {
        $this->view
            ->setData('LOST_PASSWORD_URL', $this->getUrl('lostpassword'))
            ->setData('CREATE_ACCOUNT_URL', $this->getUrl('signup'))
            ->setData('FORM_ACTION', $this->getUrl('login/post'))
            ->setData('FORM_DATA', $this->getSession()->getLoginFormData())
            ->getLayout()
            ->setBlockConfig('content', [
                'template' => 'login/form.phtml'
            ])
            ->setBlockConfig('login_announce', [
                'type'     => \Geeftlist\Block\Cms::class,
                'code'     => 'login_announce',
                'quiet'    => true,
            ])
            ->addChild('global_messages', 'login_announce');
    }

    /**
     * @param \Base $fw
     */
    public function postAction($fw): void {
        $post = $this->getPost();
        $this->getSession()->setLoginFormData(['email' => $this->getPost('email')]);

        $geefterSession = $this->geefterSession;
        if (!isset($post['email']) || !isset($post['password']) || !$post['email'] || !$post['password']) {
            $this->getSession()->addErrorMessage('Missing field: {0}.', [$this->__('email or password')]);
            $this->reroute('*');
        }

        try {
            /** @var Geefter $geefter */
            $geefter = $this->getAuthenticationService()->authenticate([
                'email' => $post['email'],
                'password' => $post['password']
            ]);
        }
        catch (AuthenticationException) {
            $this->eventManager->trigger('geefter_failed_login', $this, [
                'email'   => $post['email']
            ]);
            $geefterSession->addErrorMessage(
                'Invalid email or password. <a href="{0}">Forgot password?</a>',
                [$this->getUrl('lostpassword')],
                ['no_escape' => true]
            );
            $this->reroute('*');
        }

        $options = [
            'type' => ($post['remember_me'] ?? false)
                ? SessionInterface::TYPE_PERSISTENT
                : SessionInterface::TYPE_SESSION
        ];
        if (! $geefterSession->setGeefterAsLoggedIn($geefter, $options)) {
            $this->getSession()->addErrorMessage('Could not log in.');
            $this->reroute('*');
        }

        $this->eventManager->trigger('geefter_login', $this, [
            'geefter' => $geefterSession->getGeefter(),
            'email'   => $post['email']
        ]);

        // Update language immediately for the rest of the request
        if ($lang = $geefterSession->getLanguage()) {
            $this->geefterSession->setLanguage($lang);
        }

        $this->getSession()->unsLoginFormData();
        $geefterSession->addSuccessMessage('Welcome {0}!', [$geefterSession->getGeefter()->getUsername()]);

        $this->rerouteOnLoggedIn();
    }

    protected function rerouteOnLoggedIn() {
        $geefterSession = $this->geefterSession;
        $coreSession = $this->sessionManager->getSession();

        $afterLoginUrl = $geefterSession->getAfterLoginUrl();
        /**
         * Fallback when logging in after session expiration
         * @see \Geeftlist\Service\App\ErrorHandler::handleErrorHttp()
         */
        if (!$afterLoginUrl) {
            $afterLoginUrl = $coreSession->getCurrentUrlOnExpiredSession();
        }

        if ($afterLoginUrl) {
            // Clear saved URL before redirect
            $geefterSession->unsAfterLoginUrl();
            $coreSession->unsCurrentUrlOnExpiredSession();
            $this->rerouteUrl($afterLoginUrl);
        }

        $this->reroute('dashboard');
    }

    public function getAuthenticationService(): \Geeftlist\Service\Authentication\AuthenticationInterface {
        return $this->authenticationService;
    }
}
