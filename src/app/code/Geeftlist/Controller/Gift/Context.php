<?php

namespace Geeftlist\Controller\Gift;


class Context extends \Geeftlist\Controller\Http\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\RegistryInterface $registry,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\View $view,
        \OliveOil\Core\Service\Response\Redirect $responseRedirect,
        \OliveOil\Core\Model\View\Breadcrumbs $breadcrumbs,
        \OliveOil\Core\Service\Design $design,
        \Geeftlist\Model\Session\Http\GeefterInterface $geefterSession,
        \Geeftlist\Model\ModelFactory $modelFactory,
        \Geeftlist\Model\ResourceFactory $modelResourceFactory,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\Security\Csrf $csrfService,
        \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor,
        \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory,
        \Geeftlist\Service\Security $securityService,
        \Geeftlist\Service\Permission $permissionService,
        protected \Geeftlist\Helper\Gift $giftHelper
    ) {
        parent::__construct(
            $fw,
            $registry,
            $app,
            $appConfig,
            $routeService,
            $requestManager,
            $urlBuilder,
            $i18n,
            $eventService,
            $sessionManager,
            $view,
            $responseRedirect,
            $breadcrumbs,
            $design,
            $geefterSession,
            $modelFactory,
            $modelResourceFactory,
            $logService,
            $csrfService,
            $dataValidationErrorProcessor,
            $modelRepositoryFactory,
            $securityService,
            $permissionService
        );
    }

    public function getGiftHelper(): \Geeftlist\Helper\Gift {
        return $this->giftHelper;
    }
}
