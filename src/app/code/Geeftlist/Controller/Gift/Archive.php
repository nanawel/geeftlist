<?php

namespace Geeftlist\Controller\Gift;


use Geeftlist\Model\Gift\Field\Status;
use OliveOil\Core\Block\Dialog;

class Archive extends AbstractGiftController
{
    public function confirmArchiveAction(): void {
        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        if ($gift->getStatus() == Status::ARCHIVED) {
            $this->geefterSession->addErrorMessage('This gift has already been archived.');
            $this->redirectReferer();
        }

        if (!$this->giftHelper->canArchive($gift, true)) {
            $this->geefterSession->addErrorMessage('Sorry, you cannot archive this gift.');
            $this->redirectReferer();
        }

        $this->view
            ->setData('GIFT', $gift)
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_QUESTION,
                'message' => $this->__('Do you want to archive this gift?'),
                'content_blocks' => ['dialog_content'],
                'buttons' => [
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/archive', [
                            'gift_id' => $gift->getId(),
                            '_csrf' => true,
                            '_referer' => '_keep'
                        ]),
                        'text'  => $this->__('Yes'),
                        'class' => 'button yes'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->urlBuilder->getQueryReferer() ?: $this->getUrl('dashboard'),
                        'text'  => $this->__('No'),
                        'class' => 'button no'
                    ]
                ]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \Geeftlist\Block\Gift::class,
                'template' => 'common/gift.phtml',
                'display_actions' => false,
                'display_reservations' => false,
            ]);
    }

    public function archiveAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        $this->giftHelper->archive($gift);

        $this->geefterSession->addSuccessMessage(
            'Gift <a href="{0}">{1}</a> archived successfully!',
            [$gift->getViewUrl(), $this->escapeHtml($gift->getLabel())],
            ['no_escape' => true]
        );

        $this->redirectReferer();
    }
}
