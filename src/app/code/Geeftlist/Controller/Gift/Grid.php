<?php

namespace Geeftlist\Controller\Gift;


use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Block\Dialog;

class Grid extends AbstractGiftController
{
    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\View\Grid\Gift $giftGrid
    ) {
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        $this->giftGrid->reset();  // Needed for UT

        return parent::onBeforeRoute($fw, $args);
    }

    /*
     * INDEX
     */

    public function indexAction(): void {
        $grid = $this->getGiftGrid();
        $grid->configure('ajax_url', $this->getUrl('*/grid/*', ['_referer' => true]));

        $this->view
            ->getLayout()
            ->setBlockConfig('grid', $grid->getTableBlockConfig())
            ->setBlockTemplate('toolbar', 'gift/toolbar.phtml');

        $this->callInContextMethod(__FUNCTION__);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function indexAction_forMe(): void {
        $this->breadcrumbs->addCrumb(
            'My gifts for me',
            '*/index'
        );

        $this->view->getLayout()
            ->setBlockTemplate('content', 'gift/mine/index.phtml');
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function indexAction_forRelatives(): void {
        if (! $this->checkGeefterFamilies()) {
            $this->indexNoFamily();
            return;
        }

        $this->breadcrumbs->addCrumb(
            'My gifts for my relatives',
            '*/index'
        );

        $this->view->getLayout()
            ->setBlockTemplate('content', 'gift/relatives/index.phtml');
    }

    /**
     * Explicitly return 404 when "for" is not present
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function indexAction_default(): void {
        $this->fw->error(HttpStatus::STATUS_NOT_FOUND);
    }

    protected function indexNoFamily() {
        $this->view->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_ERROR,
                'message' => $this->__(
                    'You must <a href="{0}">create</a> or <a href="{1}">join</a> '
                    . 'a family before creating gifts for relatives.',
                    $this->getUrl('family_manage/new'),
                    $this->getUrl('family_search')
                ),
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => $this->getUrl('dashboard'),
                    'text'  => $this->__('Back to dashboard'),
                    'class' => 'button back'
                ]]
            ]);
    }

    /*
     * GRID
     */

    public function gridAction(): void {
        $this->callInContextMethod(__FUNCTION__);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function gridAction_forMe(): void {
        $grid = $this->getGiftGrid();
        $grid->applyQuery($this->urlBuilder->getRequestQuery());

        $response = $grid->getGridData();

        $this->view->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function gridAction_forRelatives(): void {
        if ($this->checkGeefterFamilies()) {
            $grid = $this->getGiftGrid();
            $grid->applyQuery($this->urlBuilder->getRequestQuery());
            $response = $grid->getGridData();
        }
        else {
            $response = [];
        }

        $this->view->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $response
            ]);
    }

    /**
     * Explicitly return 404 when "for" is not present
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function gridAction_default(): void {
        $this->fw->error(HttpStatus::STATUS_NOT_FOUND);
    }

    public function getGiftGrid(): \Geeftlist\View\Grid\Gift {
        $this->callInContextMethod(__FUNCTION__);

        return $this->giftGrid;
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function getGiftGrid_forMe() {
        if (!$this->giftGrid->getFlag(self::class . '::isConfigured')) {
            $currentGeefter = $this->geefterSession->getGeefter();
            $currentGeeftee = $this->geefterSession->getGeeftee();

            $this->giftGrid->configure('visible_columns', [
                    'image',
                    'label',
                    'estimated_price',
                    'status',
                    'priority',
                    'actions'
                ]);

            $this->giftGrid->attachInstanceListener(
                'collection::new',
                static function ($target, array $args) use ($currentGeefter, $currentGeeftee): void {
                    $args['collection']->addFieldToFilter('geeftee', $currentGeeftee)
                        ->addFieldToFilter('main_table.creator_id', $currentGeefter->getId());
                }
            );

            $this->giftGrid->setFlag(self::class . '::isConfigured', true);
        }
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function getGiftGrid_forRelatives() {
        if (!$this->giftGrid->getFlag(self::class . '::isConfigured')) {
            $currentGeefter = $this->geefterSession->getGeefter();
            $currentGeeftee = $this->geefterSession->getGeeftee();

            $this->giftGrid->configure('visible_columns', [
                    'image',
                    'label',
                    'estimated_price',
                    'geeftees',
                    'status',
                    'priority',
                    'actions'
                ]);

            $this->giftGrid->attachInstanceListener(
                'collection::new',
                static function ($target, array $args) use ($currentGeefter, $currentGeeftee): void {
                    $args['collection']->addFieldToFilter('geeftee', ['neq' => $currentGeeftee])
                        ->addFieldToFilter('main_table.creator_id', $currentGeefter->getId());
                }
            );

            $this->giftGrid->setFlag(self::class . '::isConfigured', true);
        }
    }

    /**
     * Explicitly return 404 when "for" is not present
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function getGiftGrid_default(): void {
        $this->fw->error(HttpStatus::STATUS_NOT_FOUND);
    }
}
