<?php

namespace Geeftlist\Controller\Gift;


use Geeftlist\Controller\Traits\DiscussionTrait;
use Geeftlist\Model\Gift;

class View extends AbstractGiftController
{
    use DiscussionTrait;

    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\Helper\Gift $giftHelper,
        protected \Geeftlist\Controller\Helper\Gift\Discussion $discussionHelper
    ) {
        parent::__construct($context);
    }

    public function viewAction(): void {
        /** @var Gift $gift */
        $gift = $this->getGift();

        $this->breadcrumbs->addCrumb($gift->getLabel(), '*/*/*');

        $allGiftBlockParams = [
            'show_discussion_message_count'   => false,
            'use_gift_by_geeftee_preload'     => false,
            'use_reservation_by_gift_preload' => false,
        ];
        $giftBlockParams = $allGiftBlockParams + [
            'type'                  => \Geeftlist\Block\Gift::class,
            'template'              => 'common/gift.phtml',
            'display_mode'          => 'full',
            'display_page_link'     => false,
            'display_reservations'  => true
        ];

        $this->view
            ->setData('GIFT', $gift)
            ->getLayout()
            ->setBlockTemplate('content', 'gift/view/index.phtml')
            ->setBlockConfig('geeftee_card', [
                'type'                  => \Geeftlist\Block\Geeftee::class,
                'template'              => 'geeftee/card.phtml',
                'show_claim_link'       => false,
                'geeftee_name_html_tag' => 'div'
            ])
            ->setBlockConfig('geeftee_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml',
                'badge_classes'      => ['small']
            ])
            ->setBlockConfig('gift', $giftBlockParams)
            ->setBlockConfig('gift_badges', [
                'type'               => \Geeftlist\Block\Badges::class,
                'template'           => 'common/badge_list.phtml'
            ])
            ->setBlockConfig('gift_actions', $allGiftBlockParams + [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/actions.phtml',
                'hide_action_buttons'    => ['view']
            ])
            ->setBlockConfig('gift_reservations', $allGiftBlockParams + [
                'type'                   => \Geeftlist\Block\Gift::class,
                'template'               => 'common/gift/reservations.phtml'
            ])
        ;

        $this->discussionHelper->appendPostForm($gift, $this->getDiscussionPostFormValues());
    }
}
