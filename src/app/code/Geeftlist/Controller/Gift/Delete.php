<?php

namespace Geeftlist\Controller\Gift;


use OliveOil\Core\Block\Dialog;

class Delete extends AbstractGiftController
{
    public function confirmDeleteAction(): void {
        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        if (!$this->giftHelper->canDelete($gift)) {
            $this->geefterSession->addErrorMessage('Sorry, you cannot delete this gift.');
            $this->redirectReferer();
        }

        $this->view
            ->setData('GIFT', $gift)
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_QUESTION,
                'message' => $this->__('Do you want to delete this gift?'),
                'content_blocks' => ['dialog_content'],
                'buttons' => [
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/delete', [
                            'gift_id' => $gift->getId(),
                            '_csrf' => true,
                            '_referer' => '_keep'
                        ]),
                        'text'  => $this->__('Yes'),
                        'class' => 'button yes'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->urlBuilder->getQueryReferer() ?: $this->getUrl('dashboard'),
                        'text'  => $this->__('No'),
                        'class' => 'button no'
                    ]
                ]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \Geeftlist\Block\Gift::class,
                'template' => 'common/gift.phtml',
                'display_actions' => false,
                'display_reservations' => false,
            ]);
    }

    public function deleteAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        if (!$this->giftHelper->canDelete($gift)) {
            $this->geefterSession->addErrorMessage('Sorry, you cannot delete this gift.');
            $this->redirectReferer();
        }

        $this->giftHelper->delete($gift);

        $this->geefterSession->addSuccessMessage(
            'Gift <strong>{0}</strong> deleted successfully!',
            [$this->escapeHtml($gift->getLabel())],
            ['no_escape' => true]
        );

        $this->redirectReferer();
    }
}
