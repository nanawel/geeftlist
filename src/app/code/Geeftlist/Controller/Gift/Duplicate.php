<?php

namespace Geeftlist\Controller\Gift;


class Duplicate extends AbstractGiftController
{
    public function duplicateAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        if (!$this->giftHelper->canDuplicate($gift)) {
            $this->geefterSession->addErrorMessage('Sorry, you cannot duplicate this gift.');
            $this->redirectReferer();
        }

        $this->reroute('*/new', ['copyOf' => $gift->getId(), '_referer' => '_keep']);
    }

    public function duplicateArchiveAction(): void {
        $this->checkCsrf();    //Check CSRF token also in GET

        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->forward('/gift/notfound');
        }

        if (!$this->giftHelper->canArchive($gift, true)) {
            $this->geefterSession->addErrorMessage('Sorry, you cannot archive this gift.');
            $this->redirectReferer();
        }

        $this->giftHelper->archive($gift);

        $this->geefterSession->addSuccessMessage(
            'Gift <a href="{0}">{1}</a> archived successfully!',
            [$gift->getViewUrl(), $this->escapeHtml($gift->getLabel())],
            ['no_escape' => true]
        );

        $this->duplicateAction();
    }
}
