<?php

namespace Geeftlist\Controller\Gift;


use Geeftlist\Model\Gift as GiftModel;

class Report extends AbstractGiftController
{
    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\Service\Gift\Email $giftEmailService
    ) {
        parent::__construct($context);
    }

    public function reportAction(): void {
        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->geefterSession->addErrorMessage('Invalid gift.');
            $this->redirectReferer();
        }

        $this->view
            ->setData('GIFT', $gift)
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => \OliveOil\Core\Block\Dialog::class,
                'dialog_type' => 'report',
                'message' => $this->__('Reporting a gift'),
                'content_blocks' => ['gift', 'form'],
                'form_action' => $this->getUrl('*/reportSubmit', [
                    'gift_id' => $gift->getId(),
                    '_referer' => '_keep'
                ]),
                'message_max_length' => $this->appConfig->getValue('GIFT_REPORT_MESSAGE_MAXLENGTH'),
                'buttons' => [
                    [
                        'type'  => 'button',
                        'text'  => $this->__('Submit'),
                        'class' => 'button submit'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->urlBuilder->getQueryReferer() ?: $this->getUrl('dashboard'),
                        'text'  => $this->__('Cancel'),
                        'class' => 'button cancel'
                    ]
                ]
            ])
            ->setBlockConfig('gift', [
                'type'     => \Geeftlist\Block\Gift::class,
                'template' => 'common/gift.phtml',
                'display_actions' => false,
                'display_reservations' => false,
            ])
            ->setBlockConfig('form', [
                'template' => 'gift/report/form.phtml',
            ]);
    }

    public function reportSubmitAction(): void {
        $gift = $this->getGift();

        $recipient = $this->getPost('recipient');
        $message = trim((string) $this->getPost('message'));
        if ($message === '') {
            $this->geefterSession->addErrorMessage('Please add a message with your report.');
            $this->redirectReferer();
        }

        if (mb_strlen($message) > $this->appConfig->getValue('GIFT_REPORT_MESSAGE_MAXLENGTH')) {
            $this->geefterSession->addErrorMessage('Invalid message.');
            $this->redirectReferer();
        }

        switch ($recipient) {
            case GiftModel\Constants::REPORT_CREATOR:
                $this->getGiftEmailService()->sendReportToCreator(
                    $gift,
                    $this->geefterSession->getGeefter(),
                    $message
                );
                break;

            case GiftModel\Constants::REPORT_ADMINISTRATOR:
                $this->getGiftEmailService()->sendReportToAdministrator(
                    $gift,
                    $this->geefterSession->getGeefter(),
                    $message
                );
                break;

            default:
                $this->geefterSession->addErrorMessage('Invalid recipient.');
                $this->redirectReferer();
        }

        $this->geefterSession->addSuccessMessage('Report sent successfully.');
        $this->redirectReferer();
    }

    public function getGiftEmailService(): \Geeftlist\Service\Gift\Email {
        return $this->giftEmailService;
    }
}
