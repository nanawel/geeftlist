<?php

namespace Geeftlist\Controller\Gift;


use Geeftlist\Controller\Traits\DiscussionTrait;
use OliveOil\Core\Form\ElementInterface;

class Discussion extends AbstractGiftController
{
    use DiscussionTrait;

    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\Helper\Gift $giftHelper,
        protected \Geeftlist\Controller\Helper\Gift\Discussion $discussionHelper
    ) {
        parent::__construct($context);
    }

    public function postSaveAction(): void {
        [$post] = $this->savePost();
        $this->redirectReferer('discussion-post-' . $post->getId());
    }

    /**
     * @return ElementInterface[]
     */
    public function getPostFormElements(array $formData): array {
        return $this->discussionHelper->getFormElements($formData);
    }
}
