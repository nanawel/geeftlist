<?php

namespace Geeftlist\Controller\Gift;

use Geeftlist\Form\Element\BadgeSelect;
use Geeftlist\Form\Element\GeefteeSelect;
use Geeftlist\Form\Element\Gift\Image;
use Geeftlist\Form\Element\Gift\OpenGiftSwitch;
use Geeftlist\Form\Element\Gift\PriorityRadioGroup;
use Geeftlist\Form\Element\Gift\ShareRuleRadioGroup;
use Geeftlist\Form\Element\Gift\StatusRadioGroup;
use Geeftlist\Form\Element\GiftListSelect;
use Geeftlist\Model\Gift\Field\Priority;
use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Form\ElementInterface;
use OliveOil\Core\Model\MagicObject;
use function OliveOil\array_discard;

class Edit extends AbstractGiftController
{
    /** @var string[] */
    protected $allowedEditFormFields = [
        'geeftee_id',
        'label',
        'estimated_price',
        'description',
        'status',
        'badge_ids',
        'priority',
        'image_id'
    ];

    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        protected \Geeftlist\Model\GiftList\Gift $giftlistGiftHelper,
        protected \Geeftlist\Helper\Share\Entity $shareEntityRuleHelper
    ) {
        parent::__construct($context);
    }

    /*
     * NEW
     */

    protected function newActionBefore() {
        $this->breadcrumbs->addCrumb('New gift idea');
    }

    public function newAction(): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->newModel();

        if ($this->getRequestParam('copyOf')) {
            $sourceGift = $this->getGift('copyOf');

            // Attach share rules to model for the form
            $this->shareEntityRuleHelper->loadShareRule($sourceGift);

            $formData = \OliveOil\array_mask($sourceGift->getData(), $this->getUsableFieldsForCopy());
            $formData['label'] = $this->__('Copy of {0}', $formData['label']);
            $gift->setData($formData);

            $this->geefterSession->addInfoMessage(
                "The gift \"{0}\" has been duplicated.\n"
                . 'Notice: the advanced settings (status, share rules, ...) have also been copied out.',
                [$sourceGift->getLabel()]
            );
        }
        // We come from saveAction but errors were found: use saved data in session
        elseif ($this->getRequestQuery('errors')) {
            $formData = $this->callInContextMethod('getGiftEditFormValues');
        }
        else {
            $formData = [];
        }

        $this->callInContextMethod('setGiftEditFormValues', $formData);

        $this->callInContextMethod(__FUNCTION__);

        $this->handleEditAction($gift);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function newAction_forMe() {
        $formData = $this->callInContextMethod('getGiftEditFormValues');
        if (empty($formData['geeftee_ids'])) {
            $formData['geeftee_ids'] = [$this->geefterSession->getGeeftee()->getId()];
        }

        $this->callInContextMethod('setGiftEditFormValues', $formData);
    }

    /*
     * EDIT
     */

    protected function editActionBefore() {
        $gift = $this->getGift();
        if ($gift->getId()) {
            $this->breadcrumbs->addCrumb(
                $this->__('My gifts: {0}', $gift->getLabel()),
                '*/*/*'
            );
        }
    }

    public function editAction(): void {
        $gift = $this->getGift();
        $this->checkManagePermission($gift);

        $formData = [];

        // We come from saveAction but errors were found: use saved data in session
        if ($this->getRequestQuery('errors')) {
            $formData = $this->callInContextMethod('getGiftEditFormValues');
        }
        // We come from a simple "edit" link, load data from specified gift
        elseif ($gift->getId()) {
            // Attach share rules to model for the form
            $this->shareEntityRuleHelper->loadShareRule($gift);

            $formData = $gift->getData();

            // Special: add geeftee_ids to form data
            $geefteeIds = $gift->getGeefteeIds();
            $formData['geeftee_ids'] = $geefteeIds ?: [];
        }

        $this->callInContextMethod('setGiftEditFormValues', $formData);

        $this->handleEditAction($gift);
    }

    protected function handleEditAction(\Geeftlist\Model\Gift $gift) {
        $formData = $this->callInContextMethod('getGiftEditFormValues') ?: [];

        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/save', ['_query' => '*']))
            ->setData('FORM_DATA', new MagicObject($formData))
            ->setData('GIFT', $gift)
            ->getLayout()
            ->setBlockTemplate('content', 'gift/edit.phtml')
            ->setBlockConfig('form', [
                'type'          => \Geeftlist\Block\Form\Gift::class,
            ])
            ->setBlockConfig('toolbar', [
                'type'          => \Geeftlist\Block\Form\Toolbar\Gift::class,
                'gift'          => $gift,
                'duplicate_url' => $gift->getDuplicateUrl(['_referer' => '_keep', '_csrf' => true]),
                'delete_url'    => $gift->getDeleteUrl(['_referer' => '_keep', '_csrf' => true])
            ])
            ->setBlockConfig('confirm-leave', [
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => 'form/confirm-leave.phtml',
            ])
            ->addChild('before_body_end', 'confirm-leave')
        ;
        $this->addFormElements($formData);

        $this->callInContextMethod('editAction');
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function getGiftEditFormValues_default() {
        return $this->geefterSession->getGiftEditFormValues() ?: [];
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function setGiftEditFormValues_default(array $formData) {
        $this->geefterSession->setGiftEditFormValues($formData);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function getGiftEditFormValues_forMe() {
        return $this->geefterSession->getGiftForMeEditFormValues() ?: [];
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function setGiftEditFormValues_forMe(array $formData) {
        $this->geefterSession->setGiftForMeEditFormValues($formData);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function getGiftEditFormValues_forRelatives() {
        return $this->geefterSession->getGiftForRelativesEditFormValues() ?: [];
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function setGiftEditFormValues_forRelatives(array $formData) {
        $this->geefterSession->setGiftForRelativesEditFormValues($formData);
    }

    /*
     * SAVE
     */

    public function saveAction(): void {
        $gift = $this->getGift();
        $this->checkManagePermission($gift);

        $isUpdate = (bool) $gift->getId();
        $post = $this->getPost();

        $post = $this->filterFormPost($post);

        $gift->addData($post);
        $errors = $this->validateGiftFormPost($gift, $post);
        $this->geefterSession->setGiftEditFormValues($post);
        if ($errors !== []) {
            foreach ($errors as $e) {
                $this->geefterSession->addErrorMessage($e);
            }

            if ($gift->getId()) {
                $this->reroute('*/edit', [
                    'gift_id' => $gift->getId(),
                    '_query' => ['errors' => 1],
                    '_referer' => '_keep'
                ]);
            }

            $this->reroute('*/new', [
                '_query' => ['errors' => 1],
                '_referer' => '_keep'
            ]);
        }

        $this->getModelRepository(\Geeftlist\Model\Gift::ENTITY_TYPE)->save($gift);

        $geeftees = [];
        if (isset($post['geeftee_ids'])) {
            foreach ($post['geeftee_ids'] as $geefteeId) {
                if (!$geeftee = $this->geefteeRepository->get($geefteeId)) {
                    throw new NoSuchEntityException('Unknown geeftee.');
                }

                $geeftees[] = $geeftee;
            }
        }

        $this->geefteeGiftHelper->replaceLinks([$gift], $geeftees);

        /**
         * Notice: Gift's lists are handled by an external observer.
         * @see \Geeftlist\Observer\GiftList::saveGiftLists()
         */

        $this->geefterSession->unsGiftEditFormValues();

        if ($isUpdate) {
            $this->geefterSession->addSuccessMessage(
                'Gift updated successfully! <a href="{0}">Check it out</a>.',
                [$gift->getViewUrl()],
                ['no_escape' => true]
            );
            $this->redirectReferer();
        }
        else {
            $this->geefterSession->addSuccessMessage('Gift created successfully!');
            $this->rerouteUrl($gift->getViewUrl());
        }
    }

    protected function getFormElements(array $formData): array {
        $geefteeIds = $formData['geeftee_ids']
            ?? $this->getRequestParam('geeftee_ids')
            ?? [];
        if (!is_array($geefteeIds)) {
            $geefteeIds = empty($geefteeIds) ? [] : explode(',', (string) $geefteeIds);
        }

        /** @var int $currentGeefterId */
        $currentGeefterId = $this->geefterSession->getGeefterId();

        return [
            'GEEFTEE_SELECT' => $this->modelFactory->make(
                GeefteeSelect::class,
                [
                    'data' => [
                        'name'               => 'geeftee_ids',
                        'id'                 => 'geeftee_ids',
                        'required'           => false,
                        'value'              => $geefteeIds,
                        'current_geeftee_id' => $this->geefterSession->getGeeftee()->getId(),
                        'required_geefter_actions' => [
                            $currentGeefterId => [
                                \Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface::ACTION_ADD_GIFT
                            ]
                        ]
                    ]
                ]
            ),
            'FILE_IMAGE' => $this->modelFactory->make(
                Image::class,
                [
                    'data' => [
                        'name'   => 'image_id', // Entity's field name
                        'id'     => 'image',
                        'value'  => $formData['image_id'] ?? null,
                        'url' => $this->view->getUrl('gift_upload/prepare'),
                        'post_data' => [
                            $this->csrfService->getTokenName() => $this->csrfService->getTokenValue()
                        ],
                        'placeholder_url' => $this->design->getImageUrl('gift/placeholder.png')
                    ]
                ]
            ),
            'PRIORITY_RADIOGROUP' => $this->modelFactory->make(
                PriorityRadioGroup::class,
                [
                    'data' => [
                        'name'   => 'priority',
                        'id'     => 'priority',
                        'value'  => $formData['priority'] ?? Priority::PRIORITY_NONE
                    ]
                ]
            ),
            'STATUS_RADIOGROUP' => $this->modelFactory->make(
                StatusRadioGroup::class,
                [
                    'data' => [
                        'name'   => 'status',
                        'id'     => 'status',
                        'value'  => $formData['status'] ?? Status::AVAILABLE
                    ]
                ]
            ),
            'SHARERULE_RADIOGROUP' => $this->modelFactory->make(
                ShareRuleRadioGroup::class,
                [
                    'data' => [
                        'name'   => Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY,
                        'id'     => 'share_rule',
                        'value'  => $formData[Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY] ?? null
                    ]
                ]
            ),
            'OPENGIFT_SWITCH' => $this->modelFactory->make(
                OpenGiftSwitch::class,
                [
                    'data' => [
                        'name'   => Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY,
                        'id'     => 'open_gift',
                        'value'  => $formData[Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY] ?? null
                    ]
                ]
            ),
            'BADGE_SELECT' => $this->modelFactory->make(
                BadgeSelect::class,
                [
                    'data' => [
                        'name'        => 'badge_ids',
                        'id'          => 'badge_ids',
                        'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                        'entity_id'   => $formData['gift_id'] ?? null
                    ]
                ]
            ),
            'GIFTLIST_SELECT' => $this->modelFactory->make(
                GiftListSelect::class,
                [
                    'data' => [
                        'name'  => 'giftlist_ids',
                        'id'    => 'giftlist_ids',
                        'value' => $formData['giftlist_ids']
                            ?? $this->giftlistGiftHelper->getGiftListIds($this->getGift())
                    ]
                ]
            )
        ];
    }

    protected function addFormElements(array $formData): static {
        foreach ($this->getFormElements($formData) as $name => $formElement) {
            $this->view->setData($name, $formElement);
        }

        return $this;
    }

    protected function filterFormPost($formData): array {
        $return = \OliveOil\array_mask($formData, $this->allowedEditFormFields);

        // Force NULL for empty price
        $return['estimated_price'] = trim($return['estimated_price'] ?? '');
        if ($return['estimated_price'] === '') {
            $return['estimated_price'] = null;
        }

        // Keep only data used by complex form elements in the next step
        $formData = array_discard($formData, array_keys($return));

        /** @var ElementInterface $formElement */
        foreach ($this->getFormElements($formData) as $formElement) {
            $formElement->filter($formData);
        }

        return array_merge($return, $formData);
    }

    /**
     * @return string[]
     */
    protected function getUsableFieldsForCopy() {
        return \OliveOil\array_diff(
            array_merge(
                $this->allowedEditFormFields,
                [Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY]
            ),
            ['status'],   // Reset status to make the new gift available by default
        );
    }
}
