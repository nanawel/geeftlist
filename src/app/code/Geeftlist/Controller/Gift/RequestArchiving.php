<?php

namespace Geeftlist\Controller\Gift;


use OliveOil\Core\Block\Dialog;

class RequestArchiving extends AbstractGiftController
{
    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\Service\Gift\Email $giftEmailService
    ) {
        parent::__construct($context);
    }

    public function confirmRequestArchivingAction(): void {
        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->geefterSession->addErrorMessage('Invalid gift.');
            $this->redirectReferer();
        }

        $this->view
            ->setData('GIFT', $gift)
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_QUESTION,
                'message' => $this->__('Do you want to send an archiving request for this gift to its creator?'),
                'content_blocks' => ['dialog_content'],
                'buttons' => [
                    [
                        'type'  => 'a',
                        'href'  => $this->getUrl('*/requestArchiving', [
                            'gift_id' => $gift->getId(),
                            '_csrf' => true,
                            '_referer' => '_keep'
                        ]),
                        'text'  => $this->__('Yes'),
                        'class' => 'button yes'
                    ],
                    [
                        'type'  => 'a',
                        'href'  => $this->urlBuilder->getQueryReferer() ?: $this->getUrl('dashboard'),
                        'text'  => $this->__('No'),
                        'class' => 'button no'
                    ]
                ]
            ])
            ->setBlockConfig('dialog_content', [
                'type'     => \Geeftlist\Block\Gift::class,
                'template' => 'common/gift.phtml',
                'display_actions' => false
            ]);
    }

    public function requestArchivingAction(): void {
        $this->checkCsrf(['GET']);
        $gift = $this->getGift();

        if (!$gift->getId()) {
            $this->geefterSession->addErrorMessage('Invalid gift.');
            $this->redirectReferer();
        }

        $this->getGiftEmailService()
            ->sendArchivingRequest($gift, $this->geefterSession->getGeefter());

        $this->geefterSession->addSuccessMessage('Your request has been successfully sent.');
        $this->redirectReferer();
    }

    public function getGiftEmailService(): \Geeftlist\Service\Gift\Email {
        return $this->giftEmailService;
    }
}
