<?php

namespace Geeftlist\Controller\Gift;


use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift as GiftModel;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;

abstract class AbstractGiftController extends AbstractController
{
    public const CONTEXT_FOR_PARAM     = 'for';
    public const CONTEXT_FOR_ME        = 'me';
    public const CONTEXT_FOR_RELATIVES = 'relatives';

    protected \Geeftlist\Service\PermissionInterface $permissionService;

    protected \OliveOil\Core\Model\RepositoryInterface $giftRepository;

    protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository;

    protected \Geeftlist\Helper\Gift $giftHelper;

    public function __construct(\Geeftlist\Controller\Gift\Context $context) {
        $this->permissionService = $context->getPermissionService();
        $this->giftRepository = $context->getModelRepositoryFactory()->getFromCode(\Geeftlist\Model\Gift::ENTITY_TYPE);
        $this->geefteeRepository = $context->getModelRepositoryFactory()->getFromCode(Geeftee::ENTITY_TYPE);
        $this->giftHelper = $context->getGiftHelper();
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    protected function onBeforeRoute(\Base $fw, array $args = []): bool {
        parent::onBeforeRoute($fw, $args);
        $this->callInContextMethod(__FUNCTION__, ...func_get_args());

        return true;
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    protected function onBeforeRoute_forMe(\Base $fw, array $args = []) {
        $this->setRequestParam('geeftee_id', $this->geefterSession->getGeeftee()->getId());
    }

    public function getGift(string $param = 'gift_id'): \Geeftlist\Model\Gift {
        if (
            ($giftId = $this->getPost($param))
            || ($giftId = $this->getRequestParam($param))
        ) {
            try {
                /** @var \Geeftlist\Model\Gift|null $gift */
                $gift = $this->giftRepository->get($giftId);
            }
            catch (PermissionException) {
            }

            if (!isset($gift)) {
                $this->forward('/gift/notfound');
            }
        }
        else {
            /** @var \Geeftlist\Model\Gift $gift */
            $gift = $this->giftRepository->newModel();
        }

        return $gift;
    }

    protected function checkGeefterFamilies(): bool {
        if (empty($this->geefterSession->getFamilyIds())) {
            if ($this->getActionName() !== 'index' && !$this->fw->get('AJAX')) {
                $this->reroute('*/index');
            }

            return false;
        }

        return true;
    }

    protected function checkManagePermission(GiftModel $gift, string|array $actions = TypeInterface::MANAGE_ACTIONS) {
        if ($gift->getId() && !$this->permissionService->isAllowed($gift, $actions)) {
            throw new PermissionException("Invalid gift.");
        }
    }

    protected function callInContextMethod(string $method, mixed ...$params): mixed {
        if (($contextMethod = $this->getContextMethod($method)) && method_exists($this, $contextMethod)) {
            return call_user_func([$this, $contextMethod], ...$params);
        }

        return null;
    }

    protected function getContextMethod(string $method): string {
        return match ($this->getRequestParam(static::CONTEXT_FOR_PARAM)) {
            static::CONTEXT_FOR_ME => sprintf("%s_forMe", $method),
            static::CONTEXT_FOR_RELATIVES => sprintf("%s_forRelatives", $method),
            default => sprintf("%s_default", $method),
        };
    }

    /**
     * Used in both Edit and MassAdd subcontrollers, so defined here
     *
     * @return mixed[]
     * @throws \OliveOil\Core\Exception\InvalidFieldValueException
     */
    protected function validateGiftFormPost(GiftModel $gift, array &$giftData): array {
        return $this->dataValidationErrorProcessor->process(
            $gift->validate(),
            $giftData,
            [
                'label' => $this->i18n->tr('gift.field||label'),
                'estimated_price' => $this->i18n->tr('gift.field||estimated_price'),
                'image_id' => $this->i18n->tr('gift.field||image_id')
            ]
        );
    }
}
