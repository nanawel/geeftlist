<?php

namespace Geeftlist\Controller\Gift;

use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Block\Dialog;

class Error extends AbstractGiftController
{
    public function notFoundAction(): void {
        $causesListHtml = sprintf('<ul>%s</ul>', implode('', array_map(fn($c): string => sprintf('<li>%s</li>', $this->escapeHtml($c)), [
            $this->__('It does not exist (anymore)'),
            $this->__('You do not have the permissions to see it'),
            $this->__('It has been archived or deleted by its creator'),
        ])));

        $this->view
            ->getLayout()
            ->setBlockTemplate('content', 'common/dialog_container.phtml')
            ->setBlockConfig('dialog', [
                'type' => Dialog::class,
                'dialog_type' => Dialog::TYPE_ERROR,
                'title' => $this->__('Oops! Sorry but...'),
                'message' => [
                    $this->__('You cannot see this gift. Possible causes are:'),
                    $causesListHtml
                ],
                'buttons' => [[
                    'type'  => 'a',
                    'href'  => sprintf(
                        "javascript: window.history.length > 1 ? history.back() : document.location = '%s'",
                        $this->getUrl('dashboard')
                    ),
                    'text'  => $this->__('Back'),
                    'class' => 'button back'
                ]]
            ]);

        $this->getResponse()->setCode(HttpStatus::STATUS_NOT_FOUND);
    }
}
