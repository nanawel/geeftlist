<?php

namespace Geeftlist\Controller\Gift;

use Geeftlist\Form\Element\GeefteeSelect;
use Geeftlist\Model\Geeftee;
use OliveOil\Core\Exception\FormException;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Form\ElementInterface;

class MassAdd extends AbstractGiftController
{
    public function __construct(
        \Geeftlist\Controller\Gift\Context $context,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper
    ) {
        parent::__construct($context);
    }

    public function massAddAction(): void {
        $this->breadcrumbs->addCrumb('Mass Add', '*/*');

        $formData = $this->geefterSession->getMassAddFormValues() ?: [];
        $this->view
            ->setData('FORM_ACTION', $this->getUrl('*/massSave', ['_query' => '*']))
            ->setData('FORM_DATA', $formData)
            ->getLayout()
            ->setBlockTemplate('content', 'gift/massAdd/index.phtml')
            ->setBlockTemplate('form', 'gift/massAdd/form.phtml')
            ->setBlockConfig('toolbar', [
                'type' => \Geeftlist\Block\Form\Toolbar::class
            ])
            ->setBlockConfig('confirm-leave', [
                'type'     => \OliveOil\Core\Block\Template::class,
                'template' => 'form/confirm-leave.phtml',
            ])
            ->addChild('before_body_end', 'confirm-leave')
        ;
        $this->addFormElements($formData);

        $this->callInContextMethod(__FUNCTION__);
    }

    /**
     * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
     */
    public function massAddAction_forMe(): void {
        $formData = $this->view->getData('FORM_DATA');
        if (empty($formData['geeftee_ids'])) {
            $formData['geeftee_ids'] = $this->geefterSession->getGeeftee()->getId();
        }

        $this->view->setData('FORM_DATA', $formData);
    }

    /*
     * MASS SAVE
     */

    public function massSaveAction(): void {
        $post = $this->filterFormPost($this->getPost());

        $this->geefterSession->unsMassAddFormValues();
        $errors = [];

        $invalidFormData = [];
        /** @var Geeftee[] $geeftees */
        $geeftees = [];
        if (isset($post['geeftee_ids'])) {
            foreach ($post['geeftee_ids'] as $geefteeId) {
                if (!$geeftee = $this->geefteeRepository->get($geefteeId)) {
                    $errors[] = new NoSuchEntityException('Unknown geeftee.');
                } else {
                    $geeftees[] = $geeftee;
                }
            }
        }

        $useCommonGeeftees = !empty($geeftees);

        $success = 0;
        foreach ($post['gifts'] as $giftData) {
            try {
                if (! $giftData) {
                    // Empty row => skip
                    continue;
                }

                /** @var \Geeftlist\Model\Gift $gift */
                $gift = $this->giftRepository->newModel();

                $gift->addData($giftData);
                $giftErrors = $this->validateGiftFormPost($gift, $giftData);

                if ($giftErrors !== []) {
                    throw new FormException(implode("\n", $giftErrors));
                }

                $this->getModelRepository(\Geeftlist\Model\Gift::ENTITY_TYPE)->save($gift);

                if (! $useCommonGeeftees) {
                    $geeftees = [];
                    if (isset($giftData['geeftee_ids'])) {
                        foreach ($giftData['geeftee_ids'] as $geefteeId) {
                            if (! $geeftee = $this->geefteeRepository->get($geefteeId)) {
                                throw new NoSuchEntityException('Could not find geeftee with ID ' . $geefteeId);
                            }

                            $geeftees[] = $geeftee;
                        }
                    }
                }

                $this->getGeefteeGiftHelper()->replaceLinks([$gift], $geeftees);

                ++$success;
            }
            catch (FormException $e) {
                $errors[] = $e;
                $invalidFormData['gifts'][] = $giftData;
            }
        }

        if (! $success && ! $errors) {
            $this->geefterSession->addErrorMessage('You must at least add one gift!');
            $this->reroute('*/massAdd', ['_referer' => '_keep']);
        }

        if ($success !== 0) {
            $this->geefterSession->addSuccessMessage('{0} gift(s) successfully created!', [$success]);
        }

        if ($errors !== []) {
            $this->geefterSession->setMassAddFormValues($invalidFormData);
            $this->geefterSession->addErrorMessage(
                '{0} invalid gift(s) could not be saved. Check the form and try again.',
                [count($errors)]
            );
            foreach ($errors as $e) {
                $this->geefterSession->addErrorMessage($e);
            }

            $this->reroute('*/massAdd', ['_referer' => '_keep']);
        }
        else {
            $this->geefterSession->unsMassAddFormValues();
            $this->redirectReferer();
        }
    }

    protected function addFormElements(array $formData): static {
        foreach ($this->getFormElements($formData) as $name => $formElement) {
            $this->view->setData($name, $formElement);
        }

        return $this;
    }

    protected function getFormElements(array $formData, $includeForm = true, $includeRow = true): array {
        $return = [];

        /** @var int $currentGeefterId */
        $currentGeefterId = $this->geefterSession->getGeefterId();

        // Filter geeftees keeping only those whom adding gift to is possible
        $geefteeSelectRequiredActions = [
            $currentGeefterId => [
                \Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface::ACTION_ADD_GIFT
            ]
        ];

        if ($includeForm) {
            $return['GEEFTEE_SELECT'] = $this->modelFactory->make(GeefteeSelect::class, ['data' => [
                'name'       => 'geeftee_ids',
                'id'         => 'geeftee_ids',
                'required_geefter_actions' => $geefteeSelectRequiredActions
            ]]);
        }

        if ($includeRow) {
            $return['ROW_GEEFTEE_SELECT'] = $this->modelFactory->make(GeefteeSelect::class, ['data' => [
                'required_geefter_actions' => $geefteeSelectRequiredActions
            ]]);
        }

        return $return;
    }

    /**
     * @return array
     */
    protected function filterFormPost(array $formData) {
        $filteredData = \OliveOil\array_mask($formData, [
            'geeftee_ids',
            'gifts'
        ]);

        /** @var ElementInterface $formElement */
        foreach ($this->getFormElements($filteredData, true, false) as $formElement) {
            $formElement->filter($filteredData);
        }

        foreach ($formData['gifts'] as $i => $rowData) {
            $filteredData['gifts'][$i] = $this->filterFormPostRow($rowData);
        }

        return $filteredData;
    }

    protected function filterFormPostRow(array $rowData) {
        $filteredData = array_map('trim', \OliveOil\array_mask($rowData, [
            'label',
            'estimated_price',
            'geeftee_ids'
        ]));

        // Force NULL for empty price
        $filteredData['estimated_price'] = trim($filteredData['estimated_price'] ?? '');
        if ($filteredData['estimated_price'] === '') {
            $filteredData['estimated_price'] = null;
        }

        if ($filteredData['label'] === '' && empty($filteredData['estimated_price'])) {
            return false;
        }

        /** @var ElementInterface $formElement */
        foreach ($this->getFormElements($filteredData, false, true) as $formElement) {
            $formElement->setName('geeftee_ids')
                ->filter($filteredData);
        }

        return $filteredData;
    }

    public function getGeefteeGiftHelper(): \Geeftlist\Model\Geeftee\Gift {
        return $this->geefteeGiftHelper;
    }
}
