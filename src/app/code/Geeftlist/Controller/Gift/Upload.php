<?php

namespace Geeftlist\Controller\Gift;


use Geeftlist\Controller\AbstractUpload;
use Geeftlist\Controller\Http\Context;
use Geeftlist\Service\Gift\Image;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

class Upload extends AbstractUpload
{
    public const FORM_INPUT_NAME = 'image_id';

    public function __construct(
        Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \OliveOil\Core\Service\UploadInterface $uploadService,          // Not used here
        protected \Geeftlist\Service\Gift\Image $giftImageService
    ) {
        parent::__construct($context, $uploadService);
    }

    public function prepareAction(\Base $fw, array $args): void {
        /** @var ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->coreFactory->make(ErrorAggregatorInterface::class);

        try {
            $file = $this->giftImageService->upload(self::FORM_INPUT_NAME, $errorAggregator);

            $data = [
                'status' => 'success',
                'messages' => [$this->__('Image uploaded successfully.')],
                'imageId' => $file->getData('image_id'),
                'imageUrl' => $this->giftImageService->getUrlByImageId(
                    $file->getData('image_id'),
                    Image::IMAGE_TYPE_TMP
                )
            ];
        }
        catch (\Throwable $throwable) {
            $messages = [];

            // Regular errors, that can be shown to the user
            foreach ($errorAggregator->getFieldErrors(self::FORM_INPUT_NAME) as $error) {
                $messages[] = $this->__($error->getLocalizableMessage());
            }

            // Unexpected exception, add generic message for user and log error
            if ($messages === []) {
                $this->logger->error($throwable, ['exception' => $throwable]);
                $messages[] = $this->__('Unknown error on upload. Please try again later.');
            }

            $data = [
                'status' => 'failure',
                'messages' => $messages,
            ];
        }

        $this->view
            ->getLayout()
            ->setPageConfig([
                'type'     => \OliveOil\Core\Block\Data::class,
                'mimetype' => 'application/json',
                'data'     => $data
            ]);
    }
}
