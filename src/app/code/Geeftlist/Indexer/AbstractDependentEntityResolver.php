<?php

namespace Geeftlist\Indexer;


abstract class AbstractDependentEntityResolver implements DependentEntityResolverInterface
{
    /** @var string[] */
    private array $handlerIndexerEntityTypes = [];

    public function __construct(
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory
    ) {
        $this->init();
    }

    protected function init() {
        // to be overridden
    }

    protected function addHandledIndexerEntityTypes(array $indexerEntityTypes): static {
        foreach ($indexerEntityTypes as $indexerEntityType) {
            $this->handlerIndexerEntityTypes[] = implode('|', $indexerEntityType);
        }

        return $this;
    }

    /**
     * @param string $indexerCode
     * @param string $entityType
     */
    public function match($indexerCode, $entityType): bool {
        return in_array(
            implode('|', [$indexerCode, $entityType]),
            $this->handlerIndexerEntityTypes
        );
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }
}
