<?php

namespace Geeftlist\Indexer;


class Context
{
    public function __construct(protected \OliveOil\Core\Service\RegistryInterface $registry, protected \Geeftlist\Model\RepositoryFactory $repositoryFactory, protected \Geeftlist\Service\Security $securityService, protected \OliveOil\Core\Service\Event $eventService, protected \OliveOil\Core\Service\Log $logService)
    {
    }

    public function getRegistry(): \OliveOil\Core\Service\RegistryInterface {
        return $this->registry;
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    public function getEventService(): \OliveOil\Core\Service\Event {
        return $this->eventService;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }
}
