<?php

namespace Geeftlist\Indexer\Gift;


use Geeftlist\Indexer\AbstractIndexer;
use Geeftlist\Model\Gift;

class GeefterAccess extends AbstractIndexer
{
    public const CODE = 'gift/geefterAccess';

    public function __construct(
        \Geeftlist\Indexer\Context $context,
        \Geeftlist\Model\ResourceModel\Iface\IndexerInterface $indexerResource
    ) {
        parent::__construct($context, $indexerResource, Gift::ENTITY_TYPE);
    }
}
