<?php

namespace Geeftlist\Indexer;


use Geeftlist\Model\AbstractModel;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\RepositoryInterface;

abstract class AbstractIndexer implements IndexerInterface
{
    public const CODE = '_MISSING_INDEXER_CODE_';

    public const BATCH_SIZE = 500;

    protected \OliveOil\Core\Service\RegistryInterface $registry;

    protected \Geeftlist\Model\RepositoryFactory $repositoryFactory;

    protected \Geeftlist\Service\Security $securityService;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \Psr\Log\LoggerInterface $logger;

    public function __construct(
        \Geeftlist\Indexer\Context $context,
        protected \Geeftlist\Model\ResourceModel\Iface\IndexerInterface $indexerResource,
        protected string $entityType
    ) {
        $this->registry = $context->getRegistry();
        $this->repositoryFactory = $context->getRepositoryFactory();
        $this->securityService = $context->getSecurityService();
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    public function getCode(): string {
        return static::CODE;
    }

    public function reindexAll(): void {
        if (! $this->canRun()) {
            return;
        }

        $startTime = microtime(true);
        $this->logger->info(sprintf(
            '%s::%s Starting full reindex',
            $this->getEntityType(),
            $this->getCode()
        ));

        $this->securityService->callPrivileged(function (): void {
            $evParams = [
                'entity_type' => $this->getEntityType(),
                'indexer' => $this
            ];

            $this->eventManager->trigger('reindexAll::before', $this, $evParams);
            $this->eventManager->trigger(
                sprintf('reindexAll::%s::before', $this->getEntityType()),
                $this,
                $evParams
            );

            /** @var RepositoryInterface $repository */
            $repository = $this->repositoryFactory->resolveGet($this->getEntityType());
            $entityIds = $repository->findAllIds();
            $this->logger->info(sprintf(
                '%s::%s Found %d entities to reindex',
                $this->getEntityType(),
                $this->getCode(),
                count($entityIds)
            ));
            $batches = array_chunk($entityIds, static::BATCH_SIZE);
            foreach ($batches as $batch) {
                $this->reindexEntityIds($batch);
            }

            $this->eventManager->trigger(
                sprintf('reindexAll::%s::after', $this->getEntityType()),
                $this,
                $evParams
            );
            $this->eventManager->trigger('reindexAll::after', $this, $evParams);
        });

        $this->logger->info(sprintf(
            '%s::%s Full reindex completed in %s',
            $this->getEntityType(),
            $this->getCode(),
            DateTime::secondsToTime(microtime(true) - $startTime)
        ));
    }

    /**
     * @inheritDoc
     */
    public function reindexEntityIds(array $entityIds): void {
        $this->securityService->callPrivileged(function () use ($entityIds): void {
            $entities = $this->repositoryFactory->resolveGet($this->getEntityType())
                ->find([
                    'filter' => [
                        RepositoryInterface::WILDCARD_ID_FIELD => [['in' => $entityIds]]
                    ]
                ]);

            $this->reindexEntities($entities);
        });
    }

    /**
     * @inheritDoc
     */
    public function reindexEntities(array $entities): void {
        if (! $this->canRun()) {
            return;
        }

        $this->securityService->callPrivileged(function () use ($entities): void {
            $startTime = microtime(true);
            $evParams = [
                'entity_type' => $this->getEntityType(),
                'entities' => $entities,
                'indexer' => $this
            ];

            $this->eventManager->trigger('reindexEntities::before', $this, $evParams);
            $this->eventManager->trigger(
                sprintf('reindexEntities::%s::before', $this->getEntityType()),
                $this,
                $evParams
            );

            $this->getIndexerResource()->reindexEntities($entities);

            $this->logger->info(sprintf(
                '%s::%s Reindexed %d entities in %s',
                $this->getEntityType(),
                $this->getCode(),
                count($entities),
                DateTime::secondsToTime(microtime(true) - $startTime)
            ));

            $this->eventManager->trigger(
                sprintf('reindexEntities::%s::after', $this->getEntityType()),
                $this,
                $evParams
            );
            $this->eventManager->trigger('reindexEntities::after', $this, $evParams);
        });
    }

    /**
     * @inheritDoc
     */
    public function reindexEntity(AbstractModel $entity): void {
        if (! $this->canRun()) {
            return;
        }

        $this->securityService->callPrivileged(function () use ($entity): void {
            $evParams = [
                'entity_type' => $this->getEntityType(),
                'entity' => $entity,
                'indexer' => $this
            ];

            $this->eventManager->trigger('reindexEntity::before', $this, $evParams);
            $this->eventManager->trigger(
                sprintf('reindexEntity::%s::before', $this->getEntityType()),
                $this,
                $evParams
            );

            $this->getIndexerResource()->reindexEntity($entity);

            $this->eventManager->trigger(
                sprintf('reindexEntity::%s::after', $this->getEntityType()),
                $this,
                $evParams
            );
            $this->eventManager->trigger('reindexEntity::after', $this, $evParams);
        });
    }

    protected function canRun(): bool {
        if (
            ! $this->registry->exists(sprintf('indexer::%s::disabled', $this->getCode()))
            || ! $this->registry->get(sprintf('indexer::%s::disabled', $this->getCode()))
        ) {
            return true;
        }

        $this->logger->notice(sprintf(
            '%s::%s Cannot run indexer (registry flag set)',
            $this->getEntityType(),
            $this->getCode()
        ));

        return false;
    }

    public function getIndexerResource(): \Geeftlist\Model\ResourceModel\Iface\IndexerInterface {
        return $this->indexerResource;
    }

    public function getEntityType(): string {
        return $this->entityType;
    }
}
