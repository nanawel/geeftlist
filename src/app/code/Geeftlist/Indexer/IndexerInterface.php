<?php

namespace Geeftlist\Indexer;


use Geeftlist\Model\AbstractModel;

interface IndexerInterface
{
    public function getCode(): string;

    public function reindexAll(): void;

    /**
     * @param int[] $entityIds
     */
    public function reindexEntityIds(array $entityIds): void;

    /**
     * @param AbstractModel[] $entities
     */
    public function reindexEntities(array $entities): void;

    public function reindexEntity(AbstractModel $entity): void;
}
