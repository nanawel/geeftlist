<?php

namespace Geeftlist\Indexer\Discussion\Topic\GeefterAccess;


use Geeftlist\Indexer\AbstractDependentEntityResolver;
use Geeftlist\Indexer\Discussion\Topic\GeefterAccess as TopicGeefterAccess;
use Geeftlist\Indexer\Gift\GeefterAccess as GiftGeefterAccess;
use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Gift;

class DependentEntitiesResolver extends AbstractDependentEntityResolver
{
    public function __construct(
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Helper\Gift\Discussion\Topic $giftTopicHelper
    ) {
        parent::__construct($repositoryFactory);

        $this->addHandledIndexerEntityTypes([
            [GiftGeefterAccess::CODE, Gift::ENTITY_TYPE]
        ]);
    }

    public function getDependentEntities($indexerCode, $entities) {
        /** @var AbstractModel[][] $dependentEntities */
        $dependentEntities = [];
        if ($indexerCode == GiftGeefterAccess::CODE) {
            foreach ($entities as $entity) {
                if (!$entity->getId()) {
                    throw new \InvalidArgumentException('Entity has no ID.');
                }

                $dependentEntities[TopicGeefterAccess::CODE] = array_merge(
                    $dependentEntities[TopicGeefterAccess::CODE] ?? [],
                    $this->getGiftTopicHelper()->getTopicCollection($entity)->getItems()
                );
            }
        }

        return $dependentEntities;
    }

    public function getGiftTopicHelper(): \Geeftlist\Helper\Gift\Discussion\Topic {
        return $this->giftTopicHelper;
    }
}
