<?php

namespace Geeftlist\Indexer\Discussion\Topic;


use Geeftlist\Indexer\AbstractIndexer;
use Geeftlist\Model\Discussion\Topic;

class GeefterAccess extends AbstractIndexer
{
    public const CODE = 'discussion/topic/geefterAccess';

    public function __construct(
        \Geeftlist\Indexer\Context $context,
        \Geeftlist\Model\ResourceModel\Iface\IndexerInterface $indexerResource
    ) {
        parent::__construct($context, $indexerResource, Topic::ENTITY_TYPE);
    }
}
