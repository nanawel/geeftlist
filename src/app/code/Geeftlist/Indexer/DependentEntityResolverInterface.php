<?php

namespace Geeftlist\Indexer;


use Geeftlist\Model\AbstractModel;

interface DependentEntityResolverInterface
{
    /**
     * @param string $indexerCode
     * @param string $entityType
     * @return bool
     */
    public function match($indexerCode, $entityType);

    /**
     * @param string $indexerCode
     * @param AbstractModel[] $entities
     * @return AbstractModel[][]
     */
    public function getDependentEntities($indexerCode, $entities);
}
