<?php

namespace Geeftlist\View\Grid;


use Laminas\Db\Sql\Predicate\Like;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Iface\Configurable;
use OliveOil\Core\Model\Traits\ConfigurableTrait;
use OliveOil\Core\Model\Traits\FlagTrait;
use OliveOil\Core\Model\Traits\ListenableInstanceTrait;

abstract class AbstractDataTableGrid implements Configurable
{
    use ConfigurableTrait;
    use ListenableInstanceTrait;
    use FlagTrait;

    public const RESULTS_MAX_LIMIT = 100;

    public const RESULTS_DEFAULT_LIMIT = 10;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection|null */
    protected $collection;

    /** @var array */
    protected $columnFieldMapping = [];

    /** @var array */
    protected $searchFields = [];

    /** @var array */
    protected $defaults = [];

    public function __construct(
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\View\Grid\Renderer\AbstractDataTableRenderer $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);

        $this->init();
    }

    protected function init() {
        $this->configure($this->defaults);
    }

    public function onConfigurationChange(array $config): void {
        $this->getRenderer()->configure($config, null, false);
    }

    /**
     * @return array
     */
    public function getTableBlockConfig() {
        return $this->getRenderer()->getTableBlockConfig()
            + ['grid' => $this];
    }

    /**
     * @return $this
     */
    public function applyQuery(array $query): static {
        $query = $this->cleanupQuery($query);
        if (! is_array($query)) {
            return $this;
        }

        $query = $this->setQueryDefaults($query);

        $constraintTypes = ['filters', 'orders', 'search', 'limit']; // Apply limit last!
        foreach ($constraintTypes as $ct) {
            try {
                $method = 'apply' . ucfirst($ct);
                $this->$method($query);
            }
            catch (\Throwable $e) {
                $this->getLogger()->warning(sprintf('Could not apply %s: ', $ct) . $e->getMessage(), ['exception' => $e]);
            }
        }

        return $this;
    }

    protected function cleanupQuery(array $query): array {
        $columnsConfig = $this->getColumnsConfig();

        if (!empty($query['order'])) {
            foreach ($query['order'] as $o => $order) {
                if (
                    !isset($order['column'])
                    || !isset($columnsConfig[$order['column']])
                    || (isset($columnsConfig[$order['column']]['orderable'])
                        && !$columnsConfig[$order['column']]['orderable'])
                ) {
                    unset($query['order'][$o]);
                }
            }
        }

        // TODO also cleanup filters/limit/etc.

        return $query;
    }

    protected function setQueryDefaults(array $query): array {
        $columnsConfig = $this->getColumnsConfig();

        $columnNames = array_column($columnsConfig, 'data');
        if (empty($query['order']) && !empty($this->defaults['order'])) {
            foreach ($this->defaults['order'] as $col => $dir) {
                if (false === ($colId = array_search($col, $columnNames, true))) {
                    throw new InvalidValueException('Invalid default order column: ' . $col);
                }

                $query['order'][] = [
                    'column' => $colId,
                    'dir' => $dir
                ];
            }
        }

        return $query;
    }

    protected function applyFilters($query) {
        // TODO Handle column filters
    }

    protected function applyOrders(array $query) {
        $orderApplied = false;
        if (! empty($query['order'])) {
            $columnNames = array_column($this->getColumnsConfig(), 'data');

            foreach ($query['order'] as $o) {
                if (isset($o['column']) && array_key_exists($o['column'], $columnNames)) {
                    // "column" contains the # of the column (not its name) so we need to map
                    // it using data provided in $query['columns'][x]['data']
                    $column = $columnNames[$o['column']];

                    $this->getCollection()->orderBy(
                        $this->columnIdToSelectField($column),
                        $o['dir'] === 'desc' ? SORT_DESC : SORT_ASC
                    );
                    $orderApplied = true;
                }
            }
        }

        if (! $orderApplied) {
            $this->getLogger()->warning('No order applied on grid. Invalid column provided?');
        }
    }

    protected function applySearch(array $query) {
        $searchTerm = trim($query['search']['value'] ?? '');
        if ($searchTerm !== '') {
            $orPredicate = new Predicate([], Predicate::COMBINED_BY_OR);
            foreach ($this->searchFields as $field) {
                $orPredicate->addPredicate(new Like($field, sprintf('%%%s%%', $searchTerm)));
            }

            $this->getCollection()->getSelect()
                ->where->addPredicate($orPredicate);
        }
    }

    protected function applyLimit($query) {
        $offset = max((int) ($query['start'] ?? 0), 0);
        $limit = min(
            static::RESULTS_MAX_LIMIT,
            max(1, (int) ($query['length'] ?? static::RESULTS_DEFAULT_LIMIT))
        );

        // If the offset exceeds the total size, return the last page instead
        if (($offset + $limit) > $this->getFilteredRecords()) {
            $offset = ((int) ($this->getFilteredRecords() / $limit)) * $limit;
        }

        $this->getCollection()->setOffset($offset)
            ->setLimit($limit);
    }

    protected function columnIdToSelectField(string $column) {
        $selectFields = $this->columnFieldMapping;
        /* DISABLED - Makes it impossible to set a default sort order using an invisible column (created_at)
        if ($visibleColumns = $this->getConfiguration('visible_columns')) {
            $selectFields = \OliveOil\array_mask($selectFields, $visibleColumns);
        }*/

        if (!array_key_exists($column, $selectFields)) {
            throw new \InvalidArgumentException($column . ' is not a valid column');
        }

        return $selectFields[$column];
    }

    /**
     * @return \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection
     */
    public function getCollection($recreate = false) {
        if ($recreate || ! $this->collection) {
            $this->collection = $this->newCollection();
        }

        return $this->collection;
    }

    /**
     * @return \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection
     */
    abstract protected function newCollection();

    protected function getTotalRecords(): int {
        return $this->newCollection()->count();
    }

    protected function getFilteredRecords(): int {
        $coll = clone $this->getCollection();
        $coll->setLimit(false)
            ->setOffset(false);

        return $coll->count();
    }

    /**
     * @return array
     */
    public function getGridData() {
        return [
                'recordsTotal' => $this->getTotalRecords(),
                'recordsFiltered' => $this->getFilteredRecords(),
                'start' => $this->getCollection()->getOffset(),
                'length' => $this->getCollection()->getLimit(),
            ] + $this->getRenderer()->renderEntityCollection($this->getCollection());
    }

    /**
     * @inheritDoc
     */
    public function reset(): static {
        $this->collection = null;
        $this->clearInstanceListeners('*');
        $this->resetFlags();

        return $this;
    }

    /**
     * @return array
     */
    protected function getColumnsConfig() {
        return $this->getRenderer()->getDataTableConfig()['columns'];
    }

    public function getResourceModelFactory(): \Geeftlist\Model\ResourceFactory {
        return $this->resourceModelFactory;
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }

    public function getRenderer(): \Geeftlist\View\Grid\Renderer\AbstractDataTableRenderer {
        return $this->renderer;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }
}
