<?php

namespace Geeftlist\View\Grid;


/**
 * NOTE: This class is a bit of a mess because it mixes up different roles that should
 *       be delegated to companion classes (adapter for query, adapter to DataTable link, etc.).
 *       TODO Fix it some day
 */
class Family extends AbstractDataTableGrid
{
    public function __construct(
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\View\Grid\Renderer\Family $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->columnFieldMapping = [
            'name'            => 'main_table.name',
            'members_count'   => 'members_count',
            'owner'           => 'owner.username',
            'created_at'      => 'main_table.created_at',
            'visibility'      => 'main_table.visibility',
        ];
        $this->searchFields  = [
            'main_table.name',
            'owner.username',
        ];
        $this->defaults = [
            'order' => [
                'name' => 'asc',
            ]
        ];
        parent::__construct(
            $resourceModelFactory,
            $geefterSession,
            $renderer,
            $logService
        );
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Family\Collection
     */
    protected function newCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Family\Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Family::ENTITY_TYPE);
        $collection->addMembersCount()
            ->addJoinRelation('owner');

        $this->triggerInstanceEvent('collection::new', $this, ['collection' => $collection]);

        return $collection;
    }
}
