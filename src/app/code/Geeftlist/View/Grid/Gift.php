<?php

namespace Geeftlist\View\Grid;


use Geeftlist\Model\Gift\Field\Status;

/**
 * Class Gift
 *
 * NOTE: This class is a bit of a mess because it mixes up different roles that should
 *       be delegated to companion classes (adapter for query, adapter to DataTable link, etc.).
 *       TODO Fix it some day
 */
class Gift extends AbstractDataTableGrid
{
    public function __construct(
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\View\Grid\Renderer\Gift $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->columnFieldMapping = [
            'label'           => 'main_table.label',
            'description'     => 'main_table.description',
            'estimated_price' => 'main_table.estimated_price',
            'creator'         => 'creator.username',
            'geeftee'         => 'geeftee.name',
            'status'          => 'main_table.status',
            'priority'        => 'main_table.priority',
            'created_at'      => 'main_table.created_at'
        ];
        $this->searchFields  = [
            'main_table.label',
            'main_table.description',
            'creator.username',
            'geeftee.name'
        ];
        $this->defaults = [
            'order' => [
                'label' => 'asc',
            ]
        ];
        parent::__construct(
            $resourceModelFactory,
            $geefterSession,
            $renderer,
            $logService
        );
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Gift\Collection
     */
    protected function newCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE);
        $collection
            ->addJoinRelation('creator')
            ->addJoinRelation('geeftees')
        ;

        $this->triggerInstanceEvent('collection::new', $this, ['collection' => $collection]);

        return $collection;
    }

    protected function applyFilters($query) {
        // TODO Handle column filters
        parent::applyFilters($query);

        $showArchived = ($query['custom_filters']['show_archived'] ?? 'false') == 'true';
        if (! $showArchived) {
            $this->getCollection()
                ->addFieldToFilter('status', ['ne' => Status::ARCHIVED]);
        }
    }
}
