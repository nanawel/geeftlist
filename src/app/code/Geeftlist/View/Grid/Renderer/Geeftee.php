<?php

namespace Geeftlist\View\Grid\Renderer;


use Geeftlist\Model\AbstractModel;

class Geeftee extends AbstractDataTableRenderer
{
    /** @var string[] */
    protected array $renderableColumns = [
        'name',
        'email',
        'actions',
        'geeftee_id',
    ];

    public function getTableBlockConfig(): array {
        return [
            'type'     => \OliveOil\Core\Block\Template::class,
            'template' => 'geeftee/grid.phtml'
        ];
    }

    /**
     * @param \Geeftlist\Model\Geeftee $entity
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = parent::renderEntityRow($entity);

        $row['actions'] = $this->renderActionsColumn(null, $entity);

        return $row;
    }

    public function getDataTableConfig(): array {
        $i18n = $this->getI18n();
        return array_merge_recursive(parent::getDataTableConfig(), [
            'serverSide' => true,
            'ajax' => [
                'url'     => $this->getConfiguration('ajax_url'),
                'dataSrc' => 'items'
            ],
            'columns' => [
                [
                    'title' => $i18n->tr('Name'),
                    'data' => 'name',
                    'visible' => $this->isColumnVisible('name'),
                    'className' => 'col-name',
                    'responsivePriority' => 10
                ],
                [
                    'title' => $i18n->tr('Email'),
                    'data' => 'email',
                    'visible' => $this->isColumnVisible('email'),
                    'className' => 'col-email',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Actions'),
                    'data'    => 'actions',
                    'visible' => $this->isColumnVisible('actions'),
                    'className' => 'col-actions',
                    'orderable' => false,
                    'responsivePriority' => 50
                ],
            ]
        ]);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Geeftee $entity
     */
    protected function renderNameColumn($value, $entity): string {
        return sprintf(
            '<img class="profile-avatar" src="%s">%s',
            $entity->getAvatarUrl(),
            $this->getViewService()->escapeHtml($entity->getName())
        );
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Geeftee $entity
     */
    protected function renderEmailColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($entity->getEmail());
    }

    /**
     * @param null $value Not used here
     * @param AbstractModel $entity
     */
    protected function renderActionsColumn($value, $entity): string {
        return '';
    }
}
