<?php

namespace Geeftlist\View\Grid\Renderer;


use Geeftlist\Service\Gift\Image;

class Gift extends AbstractDataTableRenderer
{
    /** @var string[] */
    protected array $renderableColumns = [
        'image',
        'label',
        //'description',
        'estimated_price',
        'creator',
        'geeftees',
        'status',
        'priority',
        'created_at',
        'actions',
        'gift_id',  // Not displayed, but used in UT
    ];

    public function getTableBlockConfig(): array {
        return [
            'type'     => \OliveOil\Core\Block\Template::class,
            'template' => 'gift/grid.phtml'
        ];
    }

    /**
     * @param \Geeftlist\Model\Gift $entity
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = parent::renderEntityRow($entity);

        $row['image'] = $this->renderImageColumn(null, $entity);
        $row['creator'] = $this->renderCreatorColumn(null, $entity);
        $row['geeftees'] = $this->renderGeefteesColumn(null, $entity);
        $row['actions'] = $this->renderActionsColumn(null, $entity);

        return $row;
    }

    public function getDataTableConfig(): array {
        $i18n = $this->getI18n();
        return array_merge_recursive(parent::getDataTableConfig(), [
            'serverSide' => true,
            'ajax' => [
                'url'     => $this->getConfiguration('ajax_url'),
                'dataSrc' => 'items'
            ],
            'columns' => [
                [
                    'title' => $i18n->tr('Image'),
                    'data' => 'image',
                    'visible' => $this->isColumnVisible('image'),
                    'className' => 'col-image',
                    'orderable' => false,
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Label'),
                    'data' => 'label',
                    'visible' => $this->isColumnVisible('label'),
                    'className' => 'col-label',
                    'responsivePriority' => 10
                ],
                /*[
                    'title' => $i18n->tr('Description'),
                    'data' => 'description',
                    'visible' => $this->isColumnVisible('description'),
                    'className' => 'col-description',
                    'orderable' => false,
                    'responsivePriority' => 90
                ],*/
                [
                    'title' => $i18n->tr('Est. Price'),
                    'data' => 'estimated_price',
                    'visible' => $this->isColumnVisible('estimated_price'),
                    'className' => 'col-estimated-price',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Creator'),
                    'data' => 'creator',
                    'visible' => $this->isColumnVisible('creator'),
                    'className' => 'col-creator',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Geeftees'),
                    'data' => 'geeftees',
                    'visible' => $this->isColumnVisible('geeftees'),
                    'className' => 'col-geeftees',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Status'),
                    'data' => 'status',
                    'visible' => $this->isColumnVisible('status'),
                    'className' => 'col-status',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Priority'),
                    'data' => 'priority',
                    'visible' => $this->isColumnVisible('priority'),
                    'className' => 'col-priority',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Created at'),
                    'data' => 'created_at',
                    'visible' => $this->isColumnVisible('created_at'),
                    'className' => 'col-created-at date',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Actions'),
                    'data' => 'actions',
                    'visible' => $this->isColumnVisible('actions'),
                    'className' => 'col-actions',
                    'orderable' => false,
                    'responsivePriority' => 50
                ],
            ]
        ]);
    }

    /**
     * @param string|null $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderImageColumn($value, $entity): string {
        if ($url = $entity->getImageUrl(Image::IMAGE_TYPE_THUMBNAIL)) {
            return sprintf('<img class="gift-thumbnail" src="%s">', $url);
        }

        return '';
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderLabelColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($value);
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderDescriptionColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($value);
    }

    /**
     * @param string|null $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderEstimatedPriceColumn($value, $entity) {
        return $value !== null
            ? $this->getI18n()->currency($value, true)
            : sprintf('<span class="not-set">%s</span>', $this->getI18n()->tr('Not set'));
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderCreatorColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($entity->getCreatorUsername());
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderGeefteesColumn($value, $entity): string {
        $geeftees = $entity->getGeefteeCollection();
        $formattedValue = [];
        foreach ($geeftees as $geeftee) {
            $formattedValue[] = sprintf(
                '<span class="geeftee">
                    <img class="profile-avatar" src="%s"><span>%s</span>
                 </span>',
                $geeftee->getAvatarUrl(),
                $this->getViewService()->escapeHtml($geeftee->getName())
            );
        }

        return implode("\n", $formattedValue);
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderStatusColumn($value, $entity): string {
        $label = $this->getI18n()->tr($entity->getFrontendValue('status'));

        return sprintf(
            '<span class="status-label status-%s" title="%s">
                <span>%s</span>
            </span>',
            $value,
            $this->getViewService()->escapeHtml($label),
            $this->getViewService()->escapeHtml($label)
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderPriorityColumn($value, $entity): string {
        return sprintf(
            '<span class="priority-label priority-%s">
                <span>%s</span>
            </span>',
            $value,
            $this->getI18n()->tr($entity->getFrontendValue('priority'))
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderCreatedAtColumn($value, $entity): string {
        return $this->renderDateTime($value);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Gift $entity
     */
    protected function renderActionsColumn($value, $entity) {
        return $this->newGiftBlock()
            ->setConfig('template', 'gift/grid/actions.phtml')
            ->render([
                'RENDERER' => $this,
                'GIFT' => $entity
            ]);
    }

    /**
     * @return \OliveOil\Core\Block\BlockInterface
     */
    protected function newGiftBlock() {
        return $this->getViewService()->getLayout()->newBlock(\Geeftlist\Block\Gift::class);
    }
}
