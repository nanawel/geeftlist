<?php

namespace Geeftlist\View\Grid\Renderer\Geeftee;


use Geeftlist\View\Grid\Renderer\AbstractDataTableRenderer;

class ClaimRequest extends AbstractDataTableRenderer
{
    /** @var string[] */
    protected array $renderableColumns = [
        'geefter',
        'geeftee',
        'created_at',
        'actions',
        'claim_request_id',  // Not displayed, but used in UT
    ];

    public function getTableBlockConfig(): array {
        return [
            'type'     => \OliveOil\Core\Block\Template::class,
            'template' => 'geeftee/manage/claimrequests/grid.phtml'
        ];
    }

    /**
     * @param \Geeftlist\Model\Geeftee\ClaimRequest $entity
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = parent::renderEntityRow($entity);

        $row['geefter'] = $this->renderGeefterColumn(null, $entity);
        $row['geeftee'] = $this->renderGeefteeColumn(null, $entity);
        $row['actions'] = $this->renderActionsColumn(null, $entity);

        return $row;
    }

    public function getDataTableConfig(): array {
        $i18n = $this->getI18n();
        return array_merge_recursive(parent::getDataTableConfig(), [
            'serverSide' => true,
            'ajax' => [
                'url'     => $this->getConfiguration('ajax_url'),
                'dataSrc' => 'items'
            ],
            'columns' => [
                [
                    'title' => $i18n->tr('Claimed by'),
                    'data' => 'geefter',
                    'visible' => $this->isColumnVisible('geefter'),
                    'className' => 'col-geefter',
                    'responsivePriority' => 10
                ],
                [
                    'title' => $i18n->tr('Geeftee'),
                    'data' => 'geeftee',
                    'visible' => $this->isColumnVisible('geeftee'),
                    'className' => 'col-geeftee',
                    'responsivePriority' => 20
                ],
                [
                    'title' => $i18n->tr('Created at'),
                    'data' => 'created_at',
                    'visible' => $this->isColumnVisible('created_at'),
                    'className' => 'col-created-at date',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Actions'),
                    'data'    => 'actions',
                    'visible' => $this->isColumnVisible('actions'),
                    'className' => 'col-actions',
                    'orderable' => false,
                    'responsivePriority' => 50
                ],
            ]
        ]);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Geeftee\ClaimRequest $entity
     */
    protected function renderGeefterColumn($value, $entity): string {
        if ($this->getConfiguration('current_geefter')->getId() == $entity->getGeefterId()) {
            return sprintf(
                '<strong>%s</strong>',
                $this->getI18n()->tr('You')
            );
        }

        $geefter = $entity->getGeefter();

        return sprintf(
            '<span class="name geefter-name">
                <a class="profile-link" href="%s">
                    <img class="profile-avatar" src="%s"/>
                    <span>%s</span>
                </a>
            </span>
            <span class="email geefter-email">%s</span>',
            $geefter->getProfileUrl(),
            $geefter->getAvatarUrl(),
            $this->getViewService()->escapeHtml($geefter->getUsername()),
            $geefter->getEmail()
        );
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Geeftee\ClaimRequest $entity
     */
    protected function renderGeefteeColumn($value, $entity): string {
        $geeftee = $entity->getGeeftee();

        return sprintf(
            '<span class="name geeftee-name">
                <a class="profile-link" href="%s">
                    <img class="profile-avatar" src="%s"/>
                    <span>%s</span>
                </a>
            </span>
            <span class="email geeftee-email">%s</span>',
            $geeftee->getProfileUrl(),
            $geeftee->getAvatarUrl(),
            $this->getViewService()->escapeHtml($geeftee->getName()),
            $this->getI18n()->tr('({0})', $geeftee->getEmail() ?: $this->getI18n()->tr('no email'))
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Geeftee\ClaimRequest $entity
     */
    protected function renderCreatedAtColumn($value, $entity): string {
        return $this->renderDateTime($value);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Geeftee\ClaimRequest $entity
     */
    protected function renderActionsColumn($value, $entity) {
        if ($this->getConfiguration('current_geefter')->getId() == $entity->getGeefterId()) {
            $template = 'geeftee/manage/claimrequests/grid/claimer_actions.phtml';
        }
        else {
            $template = 'geeftee/manage/claimrequests/grid/manager_actions.phtml';
        }

        return $this->getViewService()->render(
            $template,
            'text/html',
            [
                'CLAIMREQUEST' => $entity,
            ]
        );
    }
}
