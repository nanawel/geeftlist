<?php

namespace Geeftlist\View\Grid\Renderer;


class Family extends AbstractDataTableRenderer
{
    /** @var string[] */
    protected array $renderableColumns = [
        'name',
        'members_count',
        'owner',
        'created_at',
        'visibility',
        'actions',
        'family_id',  // Not displayed, but used in UT
    ];

    public function getTableBlockConfig(): array {
        return [
            'type'     => \OliveOil\Core\Block\Template::class,
            'template' => 'family/grid.phtml'
        ];
    }

    /**
     * @param \Geeftlist\Model\Family $entity
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = parent::renderEntityRow($entity);

        $row['owner'] = $this->renderOwnerColumn(null, $entity);
        $row['actions'] = $this->renderActionsColumn(null, $entity);

        return $row;
    }

    public function getDataTableConfig(): array {
        $i18n = $this->getI18n();
        return array_merge_recursive(parent::getDataTableConfig(), [
            'serverSide' => true,
            'ajax' => [
                'url'     => $this->getConfiguration('ajax_url'),
                'dataSrc' => 'items'
            ],
            'columns' => [
                [
                    'title'     => $i18n->tr('Name'),
                    'data'    => 'name',
                    'visible' => $this->isColumnVisible('name'),
                    'className' => 'col-name',
                    'responsivePriority' => 10
                ],
                [
                    'title'     => $i18n->tr('Members'),
                    'data'    => 'members_count',
                    'visible' => $this->isColumnVisible('members_count'),
                    'className' => 'col-members-count',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Owner'),
                    'data'    => 'owner',
                    'visible' => $this->isColumnVisible('owner'),
                    'className' => 'col-owner',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Created at'),
                    'data'    => 'created_at',
                    'visible' => $this->isColumnVisible('created_at'),
                    'className' => 'col-created-at date',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Visibility'),
                    'data'    => 'visibility',
                    'visible' => $this->isColumnVisible('visibility'),
                    'className' => 'col-visibility',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Actions'),
                    'data'    => 'actions',
                    'visible' => $this->isColumnVisible('actions'),
                    'className' => 'col-actions',
                    'orderable' => false,
                    'responsivePriority' => 50
                ],
            ]
        ]);
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Family $entity
     */
    protected function renderNameColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($value);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Family $entity
     */
    protected function renderOwnerColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($entity->getOwnerUsername());
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Family $entity
     */
    protected function renderVisibilityColumn($value, $entity): string {
        return sprintf(
            '<span class="visibility-label visibility-%s">
                <span>%s</span>
            </span>',
            $value,
            $this->getI18n()->tr($entity->getFrontendValue('visibility'))
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\Family $entity
     */
    protected function renderCreatedAtColumn($value, $entity): string {
        return $this->renderDateTime($value);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\Family $entity
     */
    protected function renderActionsColumn($value, $entity) {
        return $this->getViewService()->render(
            'family/manage/grid/actions.phtml',
            'text/html',
            [
                'FAMILY' => $entity,
                'CURRENT_GEEFTER' => $this->getConfiguration('current_geefter'),
                'CURRENT_GEEFTEE' => $this->getConfiguration('current_geeftee'),
            ]
        );
    }
}
