<?php

namespace Geeftlist\View\Grid\Renderer;


use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\View\Grid\Renderer\DataTableRenderer;

abstract class AbstractDataTableRenderer extends DataTableRenderer
{
    protected $specialRenderingMethods = [
        self::SELECTED_GRID_ROWS_NAME => 'renderSelectionColumn'
    ];

    public function __construct(
        \OliveOil\Core\Helper\Data\Factory $dataHelperFactory,
        \OliveOil\Core\Service\View $viewService,
        \OliveOil\Core\Model\I18n $i18n,
        protected \Geeftlist\Helper\Url $urlHelper
    ) {
        parent::__construct($dataHelperFactory, $viewService, $i18n);
    }

    /**
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = [];

        if ($this->getConfiguration('has_selectable_rows')) {
            $row[static::SELECTED_GRID_ROWS_NAME] = $this->renderSelectionCheckbox($entity);
        }

        foreach ($entity->getData() as $field => $value) {
            $method = $this->getRenderMethod($field);
            $row[$field] = is_callable([$this, $method]) ? call_user_func([$this, $method], $value, $entity) : $value;
        }

        return $row;
    }

    protected function getRenderMethod($field) {
        static $cache = [];
        if (! isset($cache[$field])) {
            if (in_array($field, $this->specialRenderingMethods)) {
                $cache[$field] = $this->specialRenderingMethods[$field];
            }
            else {
                $cache[$field] = sprintf('render%sColumn', \OliveOil\snakecase2camelcase($field));
            }
        }

        return $cache[$field];
    }

    protected function renderSelectionCheckbox(\OliveOil\Core\Model\AbstractModel $entity): string {
        return sprintf(
            '<input type="checkbox" name="%s[]" value="%d">',
            static::SELECTED_GRID_ROWS_NAME,
            $entity->getId()
        );
    }

    protected function renderDateTime($dateTimeValue): string {
        return sprintf(
            '<time datetime="%s">%s</time>',
            DateTime::getDateIso($dateTimeValue),
            $this->getI18n()->datetime($dateTimeValue)
        );
    }

    public function getUrlHelper(): \Geeftlist\Helper\Url {
        return $this->urlHelper;
    }
}
