<?php

namespace Geeftlist\View\Grid\Renderer\Family;


use Geeftlist\View\Grid\Renderer\AbstractDataTableRenderer;

class MembershipRequest extends AbstractDataTableRenderer
{
    public const ACTION_ACCEPT = 'accept';

    public const ACTION_REJECT = 'reject';

    public const ACTION_CANCEL = 'cancel';

    /** @var string[] */
    protected array $renderableColumns = [
        'geeftee',
        'email',
        'family',
        'owner',
        'created_at',
        'actions',
        'membership_request_id',  // Not displayed, but used in UT
    ];

    public function getTableBlockConfig(): array {
        return [
            'type'     => \OliveOil\Core\Block\Template::class,
            'template' => 'family/manage/membershiprequests/grid.phtml'
        ];
    }

    /**
     * @param \Geeftlist\Model\Family\MembershipRequest $entity
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = parent::renderEntityRow($entity);

        $row['geeftee'] = $this->renderGeefteeColumn(null, $entity);
        $row['email'] = $this->renderEmailColumn(null, $entity);
        $row['family'] = $this->renderFamilyColumn(null, $entity);
        $row['owner'] = $this->renderOwnerColumn(null, $entity);
        $row['actions'] = $this->renderActionsColumn(null, $entity);

        return $row;
    }

    public function getDataTableConfig(): array {
        $i18n = $this->getI18n();
        return array_merge_recursive(parent::getDataTableConfig(), [
            'serverSide' => true,
            'ajax' => [
                'url'     => $this->getConfiguration('ajax_url'),
                'dataSrc' => 'items'
            ],
            'columns' => [
                [
                    'title' => $i18n->tr('Geeftee'),
                    'data' => 'geeftee',
                    'visible' => $this->isColumnVisible('geeftee'),
                    'className' => 'col-geeftee',
                    'responsivePriority' => 10
                ],
                [
                    'title' => $i18n->tr('Email'),
                    'data' => 'email',
                    'visible' => $this->isColumnVisible('email'),
                    'className' => 'col-email',
                    'responsivePriority' => 60
                ],
                [
                    'title' => $i18n->tr('Family'),
                    'data' => 'family',
                    'visible' => $this->isColumnVisible('family'),
                    'className' => 'col-family',
                    'responsivePriority' => 20
                ],
                [
                    'title' => $i18n->tr('Owner'),
                    'data' => 'owner',
                    'visible' => $this->isColumnVisible('owner'),
                    'className' => 'col-owner',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Request date'),
                    'data' => 'created_at',
                    'visible' => $this->isColumnVisible('created_at'),
                    'className' => 'col-created-at date',
                    'responsivePriority' => 90
                ],
                [
                    'title'     => $i18n->tr('Actions'),
                    'data'    => 'actions',
                    'visible' => $this->isColumnVisible('actions'),
                    'className' => 'col-actions',
                    'orderable' => false,
                    'responsivePriority' => 50
                ],
            ]
        ]);
    }

    /**
     * @param null $value Not used here
     */
    protected function renderGeefteeColumn(mixed $value, \Geeftlist\Model\Family\MembershipRequest $entity): string {
        if ($this->getConfiguration('current_geeftee')->getId() == $entity->getGeefteeId()) {
            return sprintf(
                '<strong>%s</strong>',
                $this->getI18n()->tr('You')
            );
        }

        $geeftee = $entity->getGeeftee();

        return sprintf(
            '<span class="name geeftee-name">
                <a class="profile-link" href="%s">
                    <img class="profile-avatar" src="%s"/>
                    <span>%s</span>
                </a>
            </span>
            <span class="email geefter-email">%s</span>',
            $geeftee->getProfileUrl(),
            $geeftee->getAvatarUrl(),
            $this->getViewService()->escapeHtml($geeftee->getName()),
            $geeftee->getEmail()
        );
    }

    /**
     * @param null $value Not used here
     */
    protected function renderEmailColumn(mixed $value, \Geeftlist\Model\Family\MembershipRequest $entity) {
        return $entity->getGeefteeEmail();
    }

    /**
     * @param null $value Not used here
     */
    protected function renderFamilyColumn(mixed $value, \Geeftlist\Model\Family\MembershipRequest $entity) {
        return $entity->getFamilyName();
    }

    /**
     * @param null $value Not used here
     */
    protected function renderOwnerColumn(mixed $value, \Geeftlist\Model\Family\MembershipRequest $entity): string {
        if ($this->getConfiguration('current_geefter')->getId() == $entity->getGeefterId()) {
            return sprintf(
                '<strong>%s</strong>',
                $this->getI18n()->tr('You')
            );
        }

        return sprintf(
            '<a class="owner-email-link" href="mailto:%s">%s</a>',
            $entity->getFamilyOwnerEmail(),
            $this->getViewService()->escapeHtml($entity->getFamilyOwnerUsername())
        );
    }

    protected function renderCreatedAtColumn(string $value, \Geeftlist\Model\Family\MembershipRequest $entity): string {
        return $this->renderDateTime($value);
    }

    /**
     * @param null $value Not used here
     */
    protected function renderActionsColumn(mixed $value, \Geeftlist\Model\Family\MembershipRequest $entity) {
        return $this->getViewService()->render(
            'family/manage/membershiprequests/grid/actions.phtml',
            'text/html',
            [
                'MEMBERSHIPREQUEST' => $entity,
                'AVAILABLE_ACTIONS' => $this->getAvailableActions($entity)
            ]
        );
    }

    /**
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    protected function getAvailableActions(\Geeftlist\Model\Family\MembershipRequest $membershipRequest): array {
        $currentGeefterId = $this->getConfiguration('current_geefter')->getId();
        $currentGeefteeId = $this->getConfiguration('current_geeftee')->getId();

        $actions = [];

        // Invitation
        if ($membershipRequest->isInvitation()) {
            if (
                $membershipRequest->getFamilyOwnerId() == $currentGeefterId
                || $membershipRequest->getSponsorId() == $currentGeefterId
            ) {
                $actions[] = MembershipRequest::ACTION_CANCEL;
            }
            elseif ($membershipRequest->getGeefteeId() == $currentGeefteeId) {
                $actions[] = MembershipRequest::ACTION_ACCEPT;
                $actions[] = MembershipRequest::ACTION_REJECT;
            }
        } elseif ($membershipRequest->getFamilyOwnerId() == $currentGeefterId) {
            $actions[] = MembershipRequest::ACTION_ACCEPT;
            $actions[] = MembershipRequest::ACTION_REJECT;
        } elseif ($membershipRequest->getGeefteeId() == $currentGeefteeId) {
            $actions[] = MembershipRequest::ACTION_CANCEL;
        }

        return $actions;
    }
}
