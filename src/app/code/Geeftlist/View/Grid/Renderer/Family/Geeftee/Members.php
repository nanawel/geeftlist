<?php

namespace Geeftlist\View\Grid\Renderer\Family\Geeftee;


use Geeftlist\Model\AbstractModel;
use Geeftlist\View\Grid\Renderer\Geeftee;

class Members extends Geeftee
{
    /**
     * @param null $value Not used here
     * @param AbstractModel $entity
     */
    protected function renderActionsColumn($value, $entity): string {
        return $this->getViewService()->render(
            'family/manage/members/grid/actions.phtml',
            'text/html',
            [
                'FAMILY' => $this->getConfiguration('family'),
                'GEEFTEE' => $entity
            ]
        );
    }
}
