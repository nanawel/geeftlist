<?php

namespace Geeftlist\View\Grid\Renderer;


class GiftList extends AbstractDataTableRenderer
{
    /** @var string[] */
    protected array $renderableColumns = [
        'name',
        'description',
        'visibility',
        'status',
        'created_at',
        'actions',
        'giftlist_id',  // Not displayed, but used in UT
    ];

    public function getTableBlockConfig(): array {
        return [
            'type'     => \OliveOil\Core\Block\Template::class,
            'template' => 'giftlist/grid.phtml'
        ];
    }

    /**
     * @param \Geeftlist\Model\GiftList $entity
     * @return string[]
     */
    public function renderEntityRow(\OliveOil\Core\Model\AbstractModel $entity): array {
        $row = parent::renderEntityRow($entity);

        $row['actions'] = $this->renderActionsColumn(null, $entity);

        return $row;
    }

    public function getDataTableConfig(): array {
        $i18n = $this->getI18n();
        return array_merge_recursive(parent::getDataTableConfig(), [
            'serverSide' => true,
            'ajax' => [
                'url' => $this->getConfiguration('ajax_url'),
                'dataSrc' => 'items'
            ],
            'columns' => [
                [
                    'title' => $i18n->tr('Name'),
                    'data' => 'name',
                    'visible' => $this->isColumnVisible('name'),
                    'className' => 'col-name',
                    'responsivePriority' => 10
                ],
                [
                    'title' => $i18n->tr('Description'),
                    'data' => 'description',
                    'visible' => $this->isColumnVisible('description'),
                    'className' => 'col-description',
                    'orderable' => false,
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Visibility'),
                    'data' => 'visibility',
                    'visible' => $this->isColumnVisible('visibility'),
                    'className' => 'col-visibility',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Status'),
                    'data' => 'status',
                    'visible' => $this->isColumnVisible('status'),
                    'className' => 'col-status',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Created at'),
                    'data' => 'created_at',
                    'visible' => $this->isColumnVisible('created_at'),
                    'className' => 'col-created-at date',
                    'responsivePriority' => 90
                ],
                [
                    'title' => $i18n->tr('Actions'),
                    'data' => 'actions',
                    'visible' => $this->isColumnVisible('actions'),
                    'className' => 'col-actions',
                    'orderable' => false,
                    'responsivePriority' => 50
                ],
            ]
        ]);
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\GiftList $entity
     */
    protected function renderLabelColumn($value, $entity) {
        return $this->getViewService()->escapeHtml($value);
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\GiftList $entity
     */
    protected function renderDescriptionColumn($value, $entity): string {
        return $this->getViewService()->truncateText(
            $this->getViewService()->escapeHtml($value),
            30
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\GiftList $entity
     */
    protected function renderVisibilityColumn($value, $entity): string {
        return sprintf(
            '<span class="visibility-label visibility-%s">
                <span>%s</span>
            </span>',
            $value,
            $this->getI18n()->tr($entity->getFrontendValue('visibility'))
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\GiftList $entity
     */
    protected function renderStatusColumn($value, $entity): string {
        $label = $this->getI18n()->tr($entity->getFrontendValue('status'));

        return sprintf(
            '<span class="status-label status-%s" title="%s">
                <span>%s</span>
            </span>',
            $value,
            $this->getViewService()->escapeHtml($label),
            $this->getViewService()->escapeHtml($label)
        );
    }

    /**
     * @param string $value
     * @param \Geeftlist\Model\GiftList $entity
     */
    protected function renderCreatedAtColumn($value, $entity): string {
        return $this->renderDateTime($value);
    }

    /**
     * @param null $value Not used here
     * @param \Geeftlist\Model\GiftList $entity
     */
    protected function renderActionsColumn($value, $entity) {
        return $this->newGiftListBlock()
            ->setConfig('template', 'giftlist/manage/grid/actions.phtml')
            ->render([
                'RENDERER' => $this,
                'GIFTLIST' => $entity
            ]);
    }

    /**
     * @return \OliveOil\Core\Block\BlockInterface
     */
    protected function newGiftListBlock() {
        return $this->getViewService()->getLayout()->newBlock(\Geeftlist\Block\GiftList::class);
    }
}
