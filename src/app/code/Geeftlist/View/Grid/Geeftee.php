<?php

namespace Geeftlist\View\Grid;


/**
 * Class Geeftee
 *
 * NOTE: This class is a bit of a mess because it mixes up different roles that should
 *       be delegated to companion classes (adapter for query, adapter to DataTable link, etc.).
 *       TODO Fix it some day
 */
class Geeftee extends AbstractDataTableGrid
{
    public function __construct(
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\View\Grid\Renderer\Geeftee $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->columnFieldMapping = [
            'name'  => 'main_table.name',
            'email' => 'main_table.email',
        ];
        $this->searchFields  = [
            'main_table.name',
            'main_table.email',
        ];
        $this->defaults = [
            'order' => [
                'name' => 'asc',
            ]
        ];
        parent::__construct(
            $resourceModelFactory,
            $geefterSession,
            $renderer,
            $logService
        );
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    protected function newCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE);

        $this->triggerInstanceEvent('collection::new', $this, ['collection' => $collection]);

        return $collection;
    }
}
