<?php

namespace Geeftlist\View\Grid;


use Geeftlist\Model\GiftList\Field\Status;

/**
 * Class GiftList
 *
 * NOTE: This class is a bit of a mess because it mixes up different roles that should
 *       be delegated to companion classes (adapter for query, adapter to DataTable link, etc.).
 *       TODO Fix it some day
 */
class GiftList extends AbstractDataTableGrid
{
    public function __construct(
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\View\Grid\Renderer\GiftList $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->columnFieldMapping = [
            'name'            => 'main_table.name',
            'description'     => 'main_table.description',
            'visibility'      => 'main_table.visibility',
            'status'          => 'main_table.status',
            'created_at'      => 'main_table.created_at'
        ];
        $this->searchFields  = [
            'main_table.name',
            'main_table.description',
        ];
        $this->defaults = [
            'order' => [
                'name' => 'asc',
            ]
        ];
        parent::__construct(
            $resourceModelFactory,
            $geefterSession,
            $renderer,
            $logService
        );
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\GiftList\Collection
     */
    protected function newCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\GiftList\Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\GiftList::ENTITY_TYPE);
        $collection->addJoinRelation('owner');

        $this->triggerInstanceEvent('collection::new', $this, ['collection' => $collection]);

        return $collection;
    }

    protected function applyFilters($query) {
        // TODO Handle column filters
        parent::applyFilters($query);

        $showArchived = ($query['custom_filters']['show_archived'] ?? 'false') == 'true';
        if (! $showArchived) {
            $this->getCollection()
                ->addFieldToFilter('status', ['ne' => Status::ARCHIVED]);
        }
    }
}
