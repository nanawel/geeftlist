<?php

namespace Geeftlist\View\Grid\Geeftee;

use Geeftlist\View\Grid\AbstractDataTableGrid;


/**
 * Class ClaimRequest
 *
 * NOTE: This class is a bit of a mess because it mixes up different roles that should
 *       be delegated to companion classes (adapter for query, adapter to DataTable link, etc.).
 *       TODO Fix it some day
 */
class ClaimRequest extends AbstractDataTableGrid
{
    /** @var array */
    protected $columnFieldMapping = [
        'geefter'    => 'geefter.username',
        'geeftee'    => 'geeftee.name',
        'created_at' => 'main_table.created_at',
    ];

    protected $searchFields = [
        'geefter.username',
        'geefter.email',
        'geeftee.name',
        'geeftee.email',
    ];

    protected $defaults = [
        'order' => [
            'created_at' => 'desc',
        ]
    ];

    public function __construct(
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\View\Grid\Renderer\Geeftee\ClaimRequest $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $resourceModelFactory,
            $geefterSession,
            $renderer,
            $logService
        );
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Collection
     */
    protected function newCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Collection $collection */
        $collection = $this->getResourceModelFactory()
            ->resolveMakeCollection(\Geeftlist\Model\Geeftee\ClaimRequest::ENTITY_TYPE)
            ->addJoinRelation('geefter')
            ->addJoinRelation('geeftee')
        ;

        $this->triggerInstanceEvent('collection::new', $this, ['collection' => $collection]);

        return $collection;
    }
}
