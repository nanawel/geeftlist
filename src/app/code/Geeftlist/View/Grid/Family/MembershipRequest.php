<?php

namespace Geeftlist\View\Grid\Family;

use Geeftlist\View\Grid\AbstractDataTableGrid;


/**
 * Class MembershipRequest
 *
 * NOTE: This class is a bit of a mess because it mixes up different roles that should
 *       be delegated to companion classes (adapter for query, adapter to DataTable link, etc.).
 *       TODO Fix it some day
 */
class MembershipRequest extends AbstractDataTableGrid
{
    /** @var array */
    protected $columnFieldMapping = [
        'geeftee'    => 'geeftee.name',
        'email'      => 'geeftee.email',
        'family'     => 'family.name',
        'owner'      => 'family_owner.username',
        'created_at' => 'main_table.created_at',
    ];

    protected $searchFields = [
        'geeftee.name',
        'geeftee.email',
        'family.name',
        'family_owner.username',
    ];

    protected $defaults = [
        'order' => [
            'created_at' => 'desc',
        ]
    ];

    public function __construct(
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\View\Grid\Renderer\Family\MembershipRequest $renderer,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $resourceModelFactory,
            $geefterSession,
            $renderer,
            $logService
        );
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Collection
     */
    protected function newCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Collection $collection */
        $collection = $this->getResourceModelFactory()
            ->resolveMakeCollection(\Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE);
        $collection
            ->addJoinRelation('family')
            ->addJoinRelation('family_owner')
            ->addJoinRelation('geeftee')
            ->addJoinRelation('sponsor')
        ;

        $this->triggerInstanceEvent('collection::new', $this, ['collection' => $collection]);

        return $collection;
    }
}
