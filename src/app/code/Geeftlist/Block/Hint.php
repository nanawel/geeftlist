<?php

namespace Geeftlist\Block;


use Geeftlist\Model\Cms\Block as CmsBlock;
use OliveOil\Core\Block\Template;
use OliveOil\Core\Model\ResourceModel\Iface\Collection;

class Hint extends Template
{
    protected \Geeftlist\Model\ResourceFactory $resourceModelFactory;

    public function __construct(Context $context) {
        $this->resourceModelFactory = $context->getResourceFactory();
        parent::__construct($context);
    }

    public function getHintHtml($id = null) {
        if ($cmsBlock = $this->getHintCms($id ?: $this->getConfig('hint_block_id'))) {
            return $cmsBlock->render();
        }

        return '';
    }

    /**
     * @param string|null $id
     * @return Cms|null
     */
    public function getHintCms($id = null) {
        /** @var \Geeftlist\Model\ResourceModel\Db\Cms\Block\Collection $collection  */
        $collection = $this->resourceModelFactory->resolveMakeCollection(CmsBlock::ENTITY_TYPE);

        if ($id === null) {
            $id = '%';
            $collection->orderBy(Collection::ORDER_BY_RANDOM);
        }

        $code = $this->formatBlockCode($id);

        $i18n = $this->view->getI18n();
        $collection->setLanguage($i18n->getSortedAvailableLanguages())
            ->addFieldToFilter('code', ['like' => $code])
            ->addFieldToFilter('language', ['in' => $i18n->getLanguageWithFallback(true)])
            ->setLimit(1);

        $model = $collection->getFirstItem();

        if ($model) {
            /** @var \Geeftlist\Block\Cms $block */
            $block = $this->view->getLayout()->newBlock(Cms::class);

            return $block->setModel($model);
        }

        return null;
    }

    /**
     * @param string|int $id
     */
    protected function formatBlockCode($id): string {
        return \Geeftlist\Model\Cms\Constants::HINT_BLOCK_CODE_PREFIX . $id;
    }
}
