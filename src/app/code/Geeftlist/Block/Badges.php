<?php

namespace Geeftlist\Block;


use Geeftlist\Block\Traits\BadgeTrait;
use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Badge;
use OliveOil\Core\Block\Template;

class Badges extends Template
{
    use BadgeTrait;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Service\Badge $badgeService
    ) {
        parent::__construct($context);
    }

    /**
     * @return Badge[]
     */
    public function getBadges(AbstractModel $entity = null): array {
        return $this->badgeService->getBadges($entity ?? $this->getConfig('entity'));
    }
}
