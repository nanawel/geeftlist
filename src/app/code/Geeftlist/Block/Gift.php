<?php

namespace Geeftlist\Block;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use OliveOil\Core\Block\Template;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\RepositoryInterface;

class Gift extends Template
{
    public const DISPLAY_MODE_SHORT = 'short';

    public const DISPLAY_MODE_FULL  = 'full';

    /** @var \Geeftlist\Model\Gift[][] */
    protected $giftsByGeeftee;

    /** @var \Geeftlist\Model\Geeftee[] */
    protected $geeftees;

    /** @var \Geeftlist\Model\Reservation[][] */
    protected $reservationsByGift;

    /** @var int[] */
    protected $discussionPostCountByGift;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $giftRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $reservationRepository,
        protected \Geeftlist\Helper\View\Bbcode $bbcodeHelper,
        protected \Geeftlist\Helper\Gift $giftHelper,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        protected \Geeftlist\Helper\Gift\Discussion\Post $discussionPostHelper,
        protected \Geeftlist\Service\PermissionInterface $permissionService,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Helper\Reservation $reservationHelper
    ) {
        parent::__construct($context);
    }

    public function init($name, &$config = []): void {
        parent::init($name, $config);

        // Preset default config
        $this->config += [
            'display_mode'                    => self::DISPLAY_MODE_FULL,
            'show_current_geeftee_first'      => true,
            'show_discussion_message_count'   => true,
            'show_empty_tabs'                 => true,
            'use_gift_by_geeftee_preload'     => true,
            'use_reservation_by_gift_preload' => true,
        ];
    }

    public function renderDescription(\Geeftlist\Model\Gift $gift): string {
        $description = $gift->getDescription();
        $truncated = false;
        if ($this->getConfig('display_mode') === self::DISPLAY_MODE_SHORT) {
            $description = $this->outputHelper->truncateText(
                $description,
                $this->getConfig('description_max_length', 200),
                '...',
                true,
                $truncated
            );
        }

        $return = trim((string) $this->bbcodeHelper->parseText($description));
        if ($truncated) {
            $return .= sprintf(
                ' <a href="%s">%s</a>',
                $gift->getViewUrl(['_referer' => true]),
                $this->__('(Read more)')
            );
        }

        return $return;
    }

    /**
     * @param AbstractModel $model
     */
    public function getCssClasses($model): string {
        $classes = [];
        if ($model instanceof Geeftee && $model->getId() == $this->geefterSession->getGeeftee()->getId()) {
            $classes[] = 'its-you';
        }

        return implode(' ', $classes);
    }

    /**
     * @return \Geeftlist\Model\Gift[]
     */
    public function getGifts(\Geeftlist\Model\Geeftee $geeftee) {
        if (! $this->getConfig('use_gift_by_geeftee_preload')) {
            $criteria = [
                'join' => [
                    'creator' => null,
                    'reservation_stats' => null
                ],
                'sort_order' => ['label' => SORT_ASC],
                'filter' => [
                    'geeftee' => [
                        ['eq' => $geeftee->getId()]
                    ]
                ]
            ];
            $this->collectCriteria(\Geeftlist\Model\Gift::ENTITY_TYPE, $criteria);

            /** @var \Geeftlist\Model\Gift[] $return */
            $return = $this->giftRepository->find($criteria);
        }
        else {
            $giftsByGeeftee = $this->getGiftsPerGeeftee();
            /** @var \Geeftlist\Model\Gift[] $return */
            $return = $giftsByGeeftee[$geeftee->getId()] ?? [];
        }

        return $return;
    }

    /**
     * @return \Geeftlist\Model\Geeftee[]
     */
    public function getGeeftees() {
        if ($this->geeftees === null) {
            if ($geeftees = $this->getConfig('geeftees')) {
                /** @var Geeftee[] $geeftees */
                $this->geeftees = $geeftees;
            }
            else {
                $criteria = ['sort_order' => ['name' => SORT_ASC]];
                $this->collectCriteria(\Geeftlist\Model\Geeftee::ENTITY_TYPE, $criteria);

                if (
                    $this->getConfig('show_current_geeftee_first')
                    && ($currentGeeftee = $this->geefterSession->getGeeftee())
                ) {
                    $criteria['filter'][RepositoryInterface::WILDCARD_ID_FIELD][] = ['ne' => $currentGeeftee->getId()];
                    $criteria['flags']['required_actions'] = TypeInterface::ACTION_INTERACT;

                    /** @var Geeftee[] $geeftees */
                    $geeftees = array_merge(
                        [$currentGeeftee],
                        $this->geefteeRepository->find($criteria)
                    );
                }
                else {
                    /** @var Geeftee[] $geeftees */
                    $geeftees = $this->geefteeRepository->find($criteria);
                }

                $this->geeftees = $geeftees;
            }
        }

        return $this->geeftees;
    }

    protected function getGiftsPerGeeftee() {
        if ($this->giftsByGeeftee === null) {
            $criteria = [
                'join' => [
                    'creator' => null,
                    'reservation_stats' => null
                ]
            ];
            $this->collectCriteria(\Geeftlist\Model\Gift::ENTITY_TYPE, $criteria);
            $gifts = $this->giftRepository->find($criteria);

            $this->giftsByGeeftee = $this->geefteeGiftHelper->getGiftsPerGeeftee($gifts);
        }

        return $this->giftsByGeeftee;
    }

    /**
     * @return \Geeftlist\Model\Reservation[]
     */
    public function getReservations(\Geeftlist\Model\Gift $gift) {
        if (! $this->getConfig('use_reservation_by_gift_preload')) {
            $criteria = [
                'join'       => ['geefter' => null],
                'sort_order' => ['geefter_username' => SORT_ASC],
                'filter'     => [
                    'main_table.gift_id' => [['eq' => $gift->getId()]]
                ]
            ];
            $this->collectCriteria(\Geeftlist\Model\Reservation::ENTITY_TYPE, $criteria);

            /** @var \Geeftlist\Model\Reservation[] $return */
            $return = $this->reservationRepository->find($criteria);
        }
        else {
            /** @var \Geeftlist\Model\Reservation[][] $reservationsByGift */
            $reservationsByGift = $this->getReservationsByGift();
            $return = $reservationsByGift[$gift->getId()] ?? [];
        }

        return $return;
    }

    /**
     * @param Reservation[] $reservations
     */
    public function hasCurrentGeefterReservation(array $reservations): bool {
        return array_reduce(
            $reservations,
            fn($carry, Reservation $item): bool => $carry || ($item->getGeefterId() == $this->geefterSession->getGeefterId()),
            false
        );
    }

    /**
     * @return array|\Geeftlist\Model\Reservation[][]
     */
    protected function getReservationsByGift() {
        if ($this->reservationsByGift === null) {
            $criteria = [
                'join' => ['geefter' => null],
                'sort_order' => ['geefter_username' => SORT_ASC]
            ];
            $this->collectCriteria(\Geeftlist\Model\Reservation::ENTITY_TYPE, $criteria);
            $reservations = $this->reservationRepository->find($criteria);

            $this->reservationsByGift = [];
            /** @var \Geeftlist\Model\Reservation $reservation */
            foreach ($reservations as $reservation) {
                if (! isset($this->reservationsByGift[$reservation->getGiftId()])) {
                    $this->reservationsByGift[$reservation->getGiftId()] = [$reservation];
                }
                else {
                    $this->reservationsByGift[$reservation->getGiftId()][] = $reservation;
                }
            }
        }

        return $this->reservationsByGift;
    }

    /**
     * @return string
     */
    public function getReservationLabel(Reservation $reservation)
    {
        return match ($reservation->getType()) {
            Reservation::TYPE_RESERVATION => $this->i18n->tr(
                'Reserved on {0}',
                $this->i18n->datetime($reservation->getReservedAt())
            ),
            Reservation::TYPE_PURCHASE => $this->i18n->tr(
                'Purchased on {0}',
                $this->i18n->datetime($reservation->getReservedAt())
            ),
            default => '',
        };
    }

    /**
     * @return int|null
     */
    public function getDiscussionMessageCount(\Geeftlist\Model\Gift $gift) {
        if (!$this->getConfig('show_discussion_message_count')) {
            return null;
        }

        return $this->discussionPostHelper->getPostCount($gift);
    }

    public function isAllowed($gift, $actions) {
        return $this->permissionService->isAllowed([$gift], $actions);
    }

    public function canDuplicate(\Geeftlist\Model\Gift $gift): bool {
        return $this->giftHelper->canDuplicate($gift);
    }

    public function canArchive(\Geeftlist\Model\Gift $gift): bool {
        return $this->giftHelper->canArchive($gift);
    }

    public function canRequestArchiving(\Geeftlist\Model\Gift $gift): bool {
        return $this->giftHelper->canRequestArchiving($gift);
    }

    /**
     * @return bool
     */
    public function canEdit(\Geeftlist\Model\Gift $gift) {
        return $this->giftHelper->canEdit($gift);
    }

    public function canDelete(\Geeftlist\Model\Gift $gift): bool {
        return $this->giftHelper->canDelete($gift);
    }

    /**
     * @return bool
     */
    public function canViewReservations(\Geeftlist\Model\Gift $gift) {
        return $this->giftHelper->canViewReservations($gift);
    }

    /**
     * @return bool
     */
    public function canReservePurchase(\Geeftlist\Model\Gift $gift) {
        return $this->giftHelper->canReservePurchase($gift);
    }

    /**
     * @return bool
     */
    public function canCancelReservation(\Geeftlist\Model\Gift $gift) {
        return $this->giftHelper->canCancelReservation($gift);
    }

    public function canReport(\Geeftlist\Model\Gift $gift): bool {
        return $this->giftHelper->canReport($gift);
    }

    public function isPrivate(\Geeftlist\Model\Gift $gift): bool {
        return $this->giftHelper->isPrivateGift($gift);
    }

    /**
     * @return float|null
     */
    public function getReservationsRemainingAmount(\Geeftlist\Model\Gift $gift) {
        return $this->reservationHelper->getRemainingAmount($gift);
    }

    public function getDataAttributesHtml(\Geeftlist\Model\Gift $gift): string {
        $data = [
            'entity-type'        => $gift->getEntityType(),
            'id'                 => $gift->getId(),
            'label'              => $gift->getLabel(),
            'estimated-price'    => $gift->getEstimatedPrice(),
            'priority'           => $gift->getPriority(),
            'created-at'         => $gift->getCreatedAt(),
            'own-gift'           => $gift->getCreatorId() == $this->geefterSession->getGeefterId(),
        ];
        $reserverIds = [];
        $latestReservationDate = 0;
        foreach ($this->getReservations($gift) as $reservation) {
            $reserverIds[] = $reservation->getGeefterId();
            $latestReservationDate = max($latestReservationDate, $reservation->getReservedAt());
        }

        if ($latestReservationDate) {
            $data['reservation-geefters'] = implode(',', $reserverIds);
            $data['latest-reservation-date'] = DateTime::getDateIso($latestReservationDate);
            $data['own-reservation'] = in_array($this->geefterSession->getGeefterId(), $reserverIds);
        }

        $html = '';
        array_walk($data, function ($v, $k) use (&$html): void {
            $html .= sprintf(' data-%s="%s"', $k, $this->view->escapeHtml($v));
        });

        return $html;
    }

    protected function collectCriteria($entityType, array &$criteria) {
        if (isset($this->config['criteria_appenders']) && is_array($this->config['criteria_appenders'])) {
            foreach ($this->config['criteria_appenders'] as $callback) {
                call_user_func_array($callback, [$entityType, &$criteria, $this]);
            }
        }
    }
}
