<?php

namespace Geeftlist\Block\Form\Toolbar;


use Geeftlist\Block\Form\Toolbar;

class Family extends Toolbar
{
    protected function buildDeleteElement() {
        $family = $this->getConfig('family');
        if (! $family || ! $family->getId()) {
            return null;
        }

        $config = $this->getElementConfig('delete');
        $deleteButton = $this->getDOMDocument()->createElement('a');
        $deleteButton->setAttribute('href', 'javascript:void(0)');
        $deleteButton->setAttribute('class', $config['class'] ?? 'button delete');
        $deleteButton->setAttribute('onclick', sprintf(
            "oliveOil.confirmAction('%s', '%s')",
            $this->view->inlineJsQuoteEscape($this->__(
                'Are you sure you want to delete this family?\n\n\t{0}',
                $this->view->escapeHtml($family->getLabel())
            )),
            $this->getConfig('delete_url')
        ));
        $this->injectLabel($deleteButton, $config['label'] ?? $this->__('Delete'));

        return $deleteButton;
    }
}
