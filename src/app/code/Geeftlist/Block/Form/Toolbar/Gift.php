<?php

namespace Geeftlist\Block\Form\Toolbar;


use Geeftlist\Block\Form\Toolbar;

class Gift extends Toolbar
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Helper\Gift $giftHelper
    ) {
        parent::__construct($context);
    }

    public function init($name, &$config = []): void {
        parent::init($name, $config);

        $this->setConfig('elements', [
            'back',
            'separator',
            'duplicate',
            'save',
            'reset',
            'separator',
            'delete'
        ]);
    }

    protected function buildDeleteElement() {
        $gift = $this->getConfig('gift');
        if (! $gift || !$gift->getId()) {
            return null;
        }

        $config = $this->getElementConfig('delete');
        $deleteButton = $this->getDOMDocument()->createElement('a');
        $deleteButton->setAttribute('href', 'javascript:void(0)');
        $deleteButton->setAttribute('class', $config['class'] ?? 'button delete');
        $deleteButton->setAttribute('onclick', sprintf(
            "oliveOil.confirmAction('%s', '%s')",
            $this->view->jsQuoteEscape($this->__(
                'Are you sure you want to delete this gift?\n\t{0}\n\n' .
                'Notice: The current reservers (if any) will be notified.',
                $this->view->escapeHtml($gift->getLabel())
            )),
            $this->getConfig('delete_url')
        ));
        $this->injectLabel($deleteButton, $config['label'] ?? $this->__('Delete'));

        return $deleteButton;
    }

    protected function buildDuplicateElement() {
        $gift = $this->getConfig('gift');
        if (!$gift || !$gift->getId() || !$this->giftHelper->canDuplicate($gift)) {
            return null;
        }

        $config = $this->getElementConfig('duplicate');
        $duplicateButton = $this->getDOMDocument()->createElement('a');
        $duplicateButton->setAttribute('href', 'javascript:void(0)');
        $duplicateButton->setAttribute('class', $config['class'] ?? 'button duplicate');
        $duplicateButton->setAttribute('onclick', sprintf(
            "oliveOil.confirmAction('%s', '%s')",
            $this->view->jsQuoteEscape($this->__(
                'Are you sure you want to duplicate this gift?\n\t{0}\n\n' .
                'Unsaved modifications on the current gift WILL be lost.',
                $this->view->escapeHtml($gift->getLabel())
            )),
            $this->getConfig('duplicate_url')
        ));
        $this->injectLabel($duplicateButton, $config['label'] ?? $this->__('Duplicate'));

        return $duplicateButton;
    }
}
