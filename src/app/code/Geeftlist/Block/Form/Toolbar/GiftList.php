<?php

namespace Geeftlist\Block\Form\Toolbar;


use Geeftlist\Block\Form\Toolbar;

class GiftList extends Toolbar
{
    public function init($name, &$config = []): void {
        parent::init($name, $config);

        $this->setConfig('elements', [
            'back',
            'separator',
            'save',
            'reset',
            'separator',
            'delete'
        ]);
    }

    protected function buildDeleteElement() {
        $giftList = $this->getConfig('giftList');
        if (! $giftList || !$giftList->getId()) {
            return null;
        }

        $config = $this->getElementConfig('delete');
        $deleteButton = $this->getDOMDocument()->createElement('a');
        $deleteButton->setAttribute('href', 'javascript:void(0)');
        $deleteButton->setAttribute('class', $config['class'] ?? 'button delete');
        $deleteButton->setAttribute('onclick', sprintf(
            "oliveOil.confirmAction('%s', '%s')",
            $this->view->jsQuoteEscape($this->__(
                'Are you sure you want to delete this list?\n\t{0}',
                $this->view->escapeHtml($giftList->getName())
            )),
            $this->getConfig('delete_url')
        ));
        $this->injectLabel($deleteButton, $config['label'] ?? $this->__('Delete'));

        return $deleteButton;
    }
}
