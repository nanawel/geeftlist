<?php

namespace Geeftlist\Block\Form;


use OliveOil\Core\Block\Template;

class Gift extends Template
{
    public function init($name, &$config = []): void {
        parent::init($name, $config);

        $this->extendConfig([
            'template' => 'gift/edit/form.phtml',
        ]);
    }
}
