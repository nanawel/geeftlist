<?php

namespace Geeftlist\Block\Form;


use OliveOil\Core\Block\Template;

class Toolbar extends Template
{
    /** @var \DOMDocument */
    protected $domDocument;

    public function init($name, &$config = []): void {
        parent::init($name, $config);

        $this->setConfig('template', 'common/form/toolbar.phtml');
        $this->setConfig('elements', [
            'back',
            'separator',
            'save',
            'reset',
            'separator',
            'delete'
        ]);
        $this->setConfig('element_back', [
            'tag'   => 'a',
            'href'  => 'javascript:void(0)',
            'class' => 'button back back-button',
            'label' => $this->__('Return')
        ]);
        $this->setConfig('element_save', [
            'class' => 'button save',
            'label' => $this->__('Save')
        ]);
        $this->setConfig('element_reset', [
            'tag'   => 'a',
            'href'  => 'javascript:void(0)',
            'class' => 'button reset',
            'label' => $this->__('Reset')
        ]);
        $this->setConfig('element_delete', [
            'tag'   => 'a',
            'href'  => 'javascript:void(0)',
            'class' => 'button delete',
            'label' => $this->__('Delete')
        ]);
    }

    public function removeElement($name): static {
        $this->setConfig(
            'elements',
            array_diff($this->getConfig('elements'), [$name])
        );

        return $this;
    }

    /**
     * @return string
     */
    public function renderButtons() {
        $elements = $this->getConfig('elements');
        if (empty($elements)) {
            return '';
        }

        $document = $this->getDOMDocument(true);

        foreach ($elements as $elementCode) {
            if ($element = $this->buildElement($elementCode)) {
                $document->appendChild($element);
            }
        }

        return $document->saveHTML();
    }

    /**
     * @param string $code
     * @return \DOMElement|null
     */
    protected function buildElement($code) {
        $config = $this->getElementConfig($code);

        if ($config === false) {
            return null;
        }

        $buildMethod = sprintf('build%sElement', ucfirst($code));
        if (is_callable([$this, $buildMethod])) {
            /** @var \DOMElement $element */
            $element = $this->$buildMethod();
        }
        else {
            // Fallback: create a default link
            $element = $this->getDOMDocument()->createElement($config['tag'] ?? 'a');
            $element->setAttribute('href', $config['href'] ?? '#');
            $element->setAttribute('class', $config['class'] ?? '');
            $this->injectLabel($element, $config['label'] ?? $code);
        }

        return $element;
    }

    protected function buildSeparatorElement() {
        $separator = $this->getDOMDocument()->createElement('span');
        $separator->setAttribute('class', 'separator');

        return $separator;
    }

    protected function buildSaveElement() {
        $config = $this->getElementConfig('save');
        $saveButton = $this->getDOMDocument()->createElement('button');
        $saveButton->setAttribute('type', 'submit');
        $saveButton->setAttribute('class', $config['class'] ?? 'button save');
        $this->injectLabel($saveButton, $config['label'] ?? $this->__('Save'));

        return $saveButton;
    }

    protected function buildDeleteElement() {
        $deleteUrl = $this->getConfig('delete_url');
        if (! $deleteUrl) {
            return null;
        }

        $config = $this->getElementConfig('delete');
        $deleteButton = $this->getDOMDocument()->createElement('a');
        $deleteButton->setAttribute('href', 'javascript:void(0)');
        $deleteButton->setAttribute('class', $config['class'] ?? 'button delete');
        $deleteButton->setAttribute('onclick', sprintf(
            "oliveOil.confirmAction('%s', '%s')",
            $this->view->jsQuoteEscape($this->__('Are you sure?')),
            $deleteUrl
        ));
        $this->injectLabel($deleteButton, $config['label'] ?? $this->__('Delete'));

        return $deleteButton;
    }

    protected function injectLabel(\DOMElement $element, $label): static {
        $labelNode = $this->getDOMDocument()->createElement('span', $this->__($label));
        $element->appendChild($labelNode);

        return $this;
    }

    /**
     * @param string $elementCode
     * @param string|null $key
     * @return array|mixed|null
     */
    protected function getElementConfig($elementCode, $key = null) {
        $config = $this->getConfig(sprintf('element_%s', $elementCode));
        if ($key === null) {
            return $config;
        }

        return $config[$key] ?? null;
    }

    /**
     * @return \DOMDocument
     */
    protected function getDOMDocument($new = false) {
        if (! $this->domDocument || $new) {
            $this->domDocument = new \DOMDocument();
        }

        return $this->domDocument;
    }
}
