<?php

namespace Geeftlist\Block;


class Context extends \OliveOil\Core\Block\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Service\View $view,
        \OliveOil\Core\Service\Design $design,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \OliveOil\Core\Helper\Output $outputHelper,
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \Geeftlist\Model\ResourceFactory $resourceFactory
    ) {
        parent::__construct(
            $fw,
            $view,
            $design,
            $i18n,
            $urlBuilder,
            $logService,
            $coreFactory,
            $outputHelper
        );
    }

    public function getModelFactory(): \Geeftlist\Model\ModelFactory {
        return $this->modelFactory;
    }

    public function getResourceFactory(): \Geeftlist\Model\ResourceFactory {
        return $this->resourceFactory;
    }
}
