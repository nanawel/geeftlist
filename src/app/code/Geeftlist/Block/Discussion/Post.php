<?php

namespace Geeftlist\Block\Discussion;


use Geeftlist\Helper\View\Bbcode;
use Geeftlist\Model\Share\Discussion\Post\RuleType\TypeInterface;
use OliveOil\Core\Block\Template;

class Post extends Template
{
    public const DELETED_ACCOUNT_PLACEHOLDER = '[Deleted Account]';

    /** @var \Geeftlist\Model\Discussion\Post */
    protected $post;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Service\PermissionInterface $permissionService,
        array $config = []
    ) {
        parent::__construct($context, $config);
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post|null $post
     * @return string
     */
    public function renderTitle($post = null) {
        if ($post === null) {
            $post = $this->getPost();
        }

        if (! $post) {
            return '';
        }

        /** @var \Geeftlist\Helper\View\Bbcode $bbcodeHelper */
        $bbcodeHelper = $this->coreFactory->make(Bbcode::class);

        $title = $post->getTitle() ?: $post->getMessage();
        if ($this->getConfig('display_mode') === 'short') {
            $title = $this->outputHelper->truncateText(
                $title,
                $this->getConfig('title_max_length', 24)
            );
        }

        return $bbcodeHelper->stripText($title);
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post|null $post
     * @return string
     */
    public function renderMessage($post = null) {
        if ($post === null) {
            $post = $this->getPost();
        }

        if (! $post) {
            return '';
        }

        /** @var \Geeftlist\Helper\View\Bbcode $bbcodeHelper */
        $bbcodeHelper = $this->coreFactory->make(Bbcode::class);

        $message = $post->getMessage();
        if ($this->getConfig('display_mode') === 'short') {
            $message = $this->outputHelper->truncateText(
                $message,
                $this->getConfig('message_max_length', 200)
            );
        }

        return $bbcodeHelper->parseText($message);
    }

    /**
     * @return \Geeftlist\Model\Discussion\Post|null
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post $post
     */
    public function setPost($post): static {
        $this->post = $post;

        return $this;
    }

    /**
     * @return bool
     */
    public function canView(\Geeftlist\Model\Discussion\Post $post) {
        return $this->permissionService->isAllowed($post, TypeInterface::ACTION_VIEW);
    }

    /**
     * @return bool
     */
    public function canEdit(\Geeftlist\Model\Discussion\Post $post) {
        return $this->permissionService->isAllowed($post, TypeInterface::ACTION_EDIT);
    }

    /**
     * @return bool
     */
    public function canDelete(\Geeftlist\Model\Discussion\Post $post) {
        return $this->permissionService->isAllowed($post, TypeInterface::ACTION_DELETE);
    }

    /**
     * @return bool
     */
    public function canPin(\Geeftlist\Model\Discussion\Post $post) {
        return $this->permissionService->isAllowed($post, TypeInterface::ACTION_PIN);
    }
}
