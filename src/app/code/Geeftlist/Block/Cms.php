<?php

namespace Geeftlist\Block;

use Geeftlist\Model\Cms\Block as CmsBlock;
use OliveOil\Core\Block\AbstractBlock;

class Cms extends AbstractBlock
{
    protected ?\Geeftlist\Model\Cms\Block $model = null;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Model\ModelFactory $modelFactory,
        protected \OliveOil\Core\Service\View\Template\Renderer\RendererInterface $templateRenderer
    ) {
        parent::__construct($context);
    }

    public function render(array $hive = null, ?int $ttl = 0): string {
        if ($model = $this->getModel()) {
            $hive = $this->getFullRenderingHive($hive);
            $output = $this->postProcessOutput($model->getContent(), $hive);
            if (! $this->getConfig('skip_wrapper_div')) {
                $classes = [
                    'cms-block',
                    'cms-block-' . $this->getConfig('code')
                ];
                if ($cssClasses = $this->getConfig('css_classes')) {
                    $classes = array_merge($classes, $cssClasses);
                }

                return '<div class="' . implode(' ', array_unique($classes)) . '">' . $output . '</div>';
            }

            return $output;
        }

        return '';
    }

    protected function getModel(): ?CmsBlock {
        if (!$this->model) {
            // FIXME Use Repository instead
            /** @var CmsBlock $model */
            $model = $this->modelFactory->resolveMake(CmsBlock::ENTITY_TYPE);

            if ($blockId = $this->getConfig('id')) {
                $model->load($blockId);

                if (!$model->getId() && ! $this->getConfig('quiet')) {
                    $this->logger->warning(
                        sprintf(
                            'Could not load CMS block with Id "%d", ignoring.',
                            $blockId
                        )
                    );
                }
            }
            elseif ($blockCode = $this->getConfig('code')) {
                $language = $this->getConfig('language', $this->view->getI18n()->getSortedAvailableLanguages());
                $model->loadByCodeAndLanguage($blockCode, $language);

                if (!$model->getId() && ! $this->getConfig('quiet')) {
                    $this->logger->warning(
                        sprintf(
                            'Could not load CMS block with code "%s" and languages [%s], ignoring.',
                            $blockCode,
                            implode(', ', $language)
                        )
                    );
                }
            }
            else {
                $this->logger->warning('Missing CMS block code, ignoring.');
            }

            if (!$model || !$model->getId()) {
                $model = null;
            }

            $this->model = $model;
        }

        return $this->model;
    }

    public function setModel(CmsBlock $model): static {
        $this->model = $model;

        return $this;
    }

    protected function postProcessOutput(string $content, ?array $hive = []): string {
        if ($model = $this->getModel()) {
            return $this->templateRenderer->resolve(
                $content,
                $hive,
                $model->getContentType(),
                ['i18n' => $this->view->getI18n()]
            );
        }

        return '';
    }
}
