<?php

namespace Geeftlist\Block;


use OliveOil\Core\Block\Template;

class Account extends Template
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
        parent::__construct($context);
    }

    public function getAvatarMaxFilesize() {
        return $this->appConfig->getValue('AVATAR_MAX_FILESIZE');
    }

    public function getAvatarMaxWidth() {
        return $this->appConfig->getValue('AVATAR_MAX_HEIGHT');
    }

    public function getAvatarMaxHeight() {
        return $this->appConfig->getValue('AVATAR_MAX_WIDTH');
    }
}
