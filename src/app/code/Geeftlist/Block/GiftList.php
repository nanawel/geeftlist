<?php

namespace Geeftlist\Block;


use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Share\GiftList\RuleType\TypeInterface;
use OliveOil\Core\Block\Template;

class GiftList extends Template
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Service\PermissionInterface $permissionService
    ) {
        parent::__construct($context);
    }

    public function canArchive(\Geeftlist\Model\GiftList $giftList): bool {
        return $giftList->getStatus() != Status::ARCHIVED
            && $this->permissionService->isAllowed(
                [$giftList],
                [TypeInterface::ACTION_ARCHIVE]
            );
    }

    /**
     * @return bool
     */
    public function canView(\Geeftlist\Model\GiftList $giftList) {
        return $this->permissionService->isAllowed([$giftList], [TypeInterface::ACTION_VIEW]);
    }

    /**
     * @return bool
     */
    public function canEdit(\Geeftlist\Model\GiftList $giftList) {
        return $this->permissionService->isAllowed([$giftList], [TypeInterface::ACTION_EDIT]);
    }

    /**
     * @return bool
     */
    public function canDelete(\Geeftlist\Model\GiftList $giftList) {
        return $this->permissionService->isAllowed([$giftList], [TypeInterface::ACTION_DELETE]);
    }
}
