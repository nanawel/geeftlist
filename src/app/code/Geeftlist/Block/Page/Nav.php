<?php

namespace Geeftlist\Block\Page;


use Geeftlist\Model\GiftList;
use OliveOil\Core\Block\HtmlRootContainer;

class Nav extends HtmlRootContainer
{
    protected \OliveOil\Core\Model\RepositoryInterface $giftListRepository;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Service\Invitation $invitationService,
        \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory
    ) {
        $this->giftListRepository = $modelRepositoryFactory->resolveGet(GiftList::ENTITY_TYPE);
        parent::__construct($context);
    }

    public function getFamilyCollection(): ?\Geeftlist\Model\ResourceModel\Db\Family\Collection {
        if ($this->geefterSession->isLoggedIn()) {
            return $this->geefterSession->getFamilyCollection()
                ->orderBy('name');
        }

        return null;
    }

    /**
     * @return GiftList[]|null
     */
    public function getGiftLists(): ?array {
        if ($this->geefterSession->isLoggedIn()) {
            /** @var GiftList[] $giftLists */
            $giftLists = $this->giftListRepository->find([
                'filter' => [
                    'owner_id' => [['eq' => $this->geefterSession->getGeefterId()]],
                    'status' => [['eq' => GiftList\Field\Status::ACTIVE]]
                ]
            ]);

            return $giftLists;
        }

        return null;
    }

    public function isInvitationEnabled(): bool {
        return $this->invitationService->isEnabled();
    }
}
