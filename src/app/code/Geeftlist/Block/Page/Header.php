<?php

namespace Geeftlist\Block\Page;


use OliveOil\Core\Block\HtmlRootContainer;

class Header extends HtmlRootContainer
{
    public function shouldAddLinkToLogo(): bool {
        $currentUrl = $this->urlBuilder->getUrl('', ['_current' => true]);

        $dashboardUrl = $this->urlBuilder->getUrl('dashboard');
        $homeUrl = $this->urlBuilder->getUrl('/');

        return !in_array($currentUrl, [$homeUrl, $dashboardUrl]);
    }
}
