<?php


namespace Geeftlist\Block\Traits;


use Geeftlist\Model\Badge;

trait BadgeTrait
{
    /**
     * @return string
     */
    public function getIconHtml(Badge $badge) {
        switch ($badge->getIconType()) {
            case 'html':
            case 'svg':
                return $badge->getIconData();

            case 'theme_image':
                return sprintf(
                    '<img src="%s" alt="%s"/>',
                    $this->design->getImageUrl($badge->getIconData()),
                    $badge->getCode()
                );

            default:
                $this->logger->warning(sprintf('Unknown type of icon definition: %s', $badge->getIconType()));
        }

        return '';
    }
}
