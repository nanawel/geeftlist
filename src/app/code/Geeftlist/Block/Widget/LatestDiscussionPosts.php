<?php

namespace Geeftlist\Block\Widget;


use Geeftlist\Block\Discussion\Post;

class LatestDiscussionPosts extends Post
{
    protected \Geeftlist\Model\ResourceFactory $resourceFactory;

    public function __construct(
        \Geeftlist\Block\Context $context,
        \Geeftlist\Service\PermissionInterface $permissionService,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
        $this->resourceFactory = $context->getResourceFactory();
        parent::__construct($context, $permissionService);
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Discussion\Post\Collection
     */
    public function getLatestPosts() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Post\Collection $postCollection */
        $postCollection = $this->resourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE)
            ->addJoinRelation('author')
            ->orderBy('main_table.post_id', SORT_DESC)
            ->setLimit($this->appConfig->getValue('DASHBOARD_WIDGET.LATEST_DISCUSSION_POSTS.items_count'));

        // FIXME Can't work because the sorting is done *after* grouping
        //$postCollection->getSelect()
        //    ->group('main_table.topic_id');

        return $postCollection;
    }
}
