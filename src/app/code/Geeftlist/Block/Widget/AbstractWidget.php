<?php

namespace Geeftlist\Block\Widget;


use OliveOil\Core\Block\Template;

abstract class AbstractWidget extends Template
{
    public function __construct(
        \Geeftlist\Block\Context $context,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
        parent::__construct($context);
    }
}
