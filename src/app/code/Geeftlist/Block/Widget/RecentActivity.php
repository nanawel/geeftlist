<?php

namespace Geeftlist\Block\Widget;


use Geeftlist\Model\Activity;

class RecentActivity extends AbstractWidget
{
    protected \Geeftlist\Model\ResourceFactory $resourceModelFactory;

    public function __construct(
        \Geeftlist\Block\Context $context,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \Geeftlist\Block\Widget\RecentActivity\Action\ActionRendererFactory $activityRendererFactory
    ) {
        $this->resourceModelFactory = $context->getResourceFactory();
        parent::__construct($context, $appConfig);
    }

    public function getActivities() {
        return $this->resourceModelFactory->resolveMakeCollection(\Geeftlist\Model\Activity::ENTITY_TYPE)
            ->orderBy('main_table.created_at', SORT_DESC)
            ->setLimit($this->appConfig->getValue('DASHBOARD_WIDGET.RECENT_ACTIVITY.items_count'));
    }

    /**
     * @return string
     */
    public function renderActivityItem(Activity $activity) {
        try {
            if ($renderer = $this->activityRendererFactory->getByModel($activity)) {
                return $renderer->render(['activity' => $activity]);
            }
        }
        catch (\Throwable $throwable) {
            $this->logger->warning(sprintf(
                'Cannot render activity item %s#%d: %s',
                $activity->getTypeId(),
                $activity->getId(),
                $throwable->getMessage()
            ));
            $this->logger->warning($throwable);
        }

        return '';
    }
}
