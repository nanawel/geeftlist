<?php

namespace Geeftlist\Block\Widget;


use Geeftlist\Block\ApiDependentBlockInterface;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Reservation;

class LatestReservations extends AbstractWidget implements ApiDependentBlockInterface
{
    protected \Geeftlist\Model\ResourceFactory $resourceFactory;

    public function __construct(
        \Geeftlist\Block\Context $context,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
        $this->resourceFactory = $context->getResourceFactory();
        parent::__construct($context, $appConfig);
    }

    /**
     * @return Geeftee[]
     */
    public function getGeeftees(Reservation $reservation) {
        /** @var Geeftee[] $geeftees */
        $geeftees = array_values($reservation->getGeeftees()->getItems());

        return $geeftees;
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Reservation\Collection
     */
    public function getLatestReservations() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Reservation\Collection $collection */
        $collection = $this->resourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Reservation::ENTITY_TYPE)
            ->addJoinRelation('geefter')
            ->addJoinRelation('gift')
            ->addJoinRelation('geeftee')
            ->orderBy('main_table.reserved_at', SORT_DESC)
            ->setLimit($this->appConfig->getValue('DASHBOARD_WIDGET.LATEST_RESERVATIONS.items_count'));

        return $collection;
    }
}
