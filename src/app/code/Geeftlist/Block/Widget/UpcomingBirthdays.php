<?php

namespace Geeftlist\Block\Widget;


use Geeftlist\Model\Geeftee;
use Laminas\Db\Sql\Expression;

class UpcomingBirthdays extends AbstractWidget
{
    protected \Geeftlist\Model\ResourceFactory $resourceFactory;

    public function __construct(
        \Geeftlist\Block\Context $context,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
        $this->resourceFactory = $context->getResourceFactory();
        parent::__construct($context, $appConfig);
    }

    /**
     * @see https://stackoverflow.com/a/28000048/5519643
     */
    public function getBirthdaysData(): array {
        // FIXME There should be no SQL here
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $geefteeCollection */
        $geefteeCollection = $this->resourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->addFieldToFilter('dob', ['null' => false])
            ->addFieldToSelect([
                'next_birthday' => new Expression(<<<EOSQL
                    DATE_ADD(
                        dob,
                        INTERVAL IF(DAYOFYEAR(dob) >= DAYOFYEAR(CURDATE()),
                            YEAR(CURDATE())-YEAR(dob),
                            YEAR(CURDATE())-YEAR(dob)+1
                        ) YEAR
                    )
EOSQL
                )])
            ->orderBy('next_birthday');

        $select = $geefteeCollection->getSelect();
        $select->having->between(
            'next_birthday',
            new Expression('CURDATE()'),
            new Expression(sprintf(
                'DATE_ADD(CURDATE(), INTERVAL %s)',
                $this->getTimeLimitInterval()['sql']
            ))
        );

        $data = [];
        /** @var Geeftee $geeftee */
        foreach ($geefteeCollection as $geeftee) {
            $data[] = $this->getBirthdayData($geeftee);
        }

        return $data;
    }

    protected function getTimeLimit(): \DateTime {
        $timeLimit = new \DateTime();
        $timeLimit->add($this->getTimeLimitInterval()['interval']);

        return $timeLimit;
    }

    protected function getTimeLimitInterval(): array {
        return [
            'interval' => new \DateInterval('P1M'),
            'sql'      => '1 MONTH'
        ];
    }

    protected function getBirthdayData(Geeftee $geeftee): array {
        $term = $this->getTimeLimit();
        $dob = new \DateTime($geeftee->getDob());
        $age = $term->diff($dob);

        return [
            'geeftee_id' => $geeftee->getId(),
            'geeftee_profile_url' => $geeftee->getProfileUrl(),
            'geeftee_name' => $geeftee->getName(),
            'geeftee_dob' => $dob,
            'geeftee_age' => $age->y
        ];
    }
}
