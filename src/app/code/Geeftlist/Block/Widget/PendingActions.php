<?php

namespace Geeftlist\Block\Widget;


use Geeftlist\Model\Family\MembershipRequest;
use Geeftlist\Model\Geeftee\ClaimRequest;

class PendingActions extends AbstractWidget
{
    protected \Geeftlist\Model\ResourceFactory $resourceFactory;

    /** @var string[] */
    protected $pendingActions;

    public function __construct(
        \Geeftlist\Block\Context $context,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig
    ) {
        $this->resourceFactory = $context->getResourceFactory();
        parent::__construct($context, $appConfig);
    }

    protected function doRender(array $hive = null, ?int $ttl = 0): string {
        if ($this->getPendingActions()) {
            return parent::doRender($hive, $ttl);
        }

        return '';
    }

    /**
     * @return array
     */
    public function getPendingActions() {
        if ($this->pendingActions === null) {
            $this->pendingActions = [];

            $claimRequestCount = $this->resourceFactory
                ->resolveMakeCollection(ClaimRequest::ENTITY_TYPE)
                ->addFieldToFilter('decision_code', ClaimRequest::DECISION_CODE_PENDING)
                ->count();
            if ($claimRequestCount) {
                $this->pendingActions[] = $this->__(
                    'You have <strong>{0}</strong> pending geeftee claim request(s). <a href="{1}">Check {2,plural,one{it},other{them}} now!</a>',
                    $claimRequestCount,
                    $this->urlBuilder->getUrl('geeftee_manage_claim_request'),
                    $claimRequestCount
                );
            }

            $membershipRequestCount = $this->resourceFactory
                ->resolveMakeCollection(MembershipRequest::ENTITY_TYPE)
                ->addFieldToFilter('decision_code', MembershipRequest::DECISION_CODE_PENDING)
                ->addFieldToFilter('sponsor_id', ['null' => true])
                ->count();
            if ($membershipRequestCount) {
                $this->pendingActions[] = $this->__(
                    'You have <strong>{0}</strong> pending membership request(s). <a href="{1}">Check {2,plural,one{it},other{them}} now!</a>',
                    $membershipRequestCount,
                    $this->urlBuilder->getUrl('family_manage_membership_request'),
                    $membershipRequestCount
                );
            }

            $invitationCount = $this->resourceFactory
                ->resolveMakeCollection(MembershipRequest::ENTITY_TYPE)
                ->addFieldToFilter('decision_code', MembershipRequest::DECISION_CODE_PENDING)
                ->addFieldToFilter('sponsor_id', ['null' => false])
                ->count();
            if ($invitationCount) {
                $this->pendingActions[] = $this->__(
                    'You have <strong>{0}</strong> pending invitation(s). <a href="{1}">Check {2,plural,one{it},other{them}} now!</a>',
                    $invitationCount,
                    $this->urlBuilder->getUrl('family_manage_membership_request'),
                    $invitationCount
                );
            }
        }

        return $this->pendingActions;
    }
}
