<?php

namespace Geeftlist\Block\Widget\RecentActivity\Action;

use Geeftlist\Model\Activity;
use OliveOil\Core\Block\Template;

class Family extends Template implements ActionInterface
{
    protected function doRender(array $hive = null, ?int $ttl = 0): string {
        return parent::doRender($this->getTemplateVars($hive['activity']), $ttl);
    }

    protected function getTemplateVars(Activity $activity): array {
        $geeftee = $activity->getActor();
        $family = $activity->getObject();

        return [
            'ACTION'              => $activity->getAction(),
            'GEEFTEE_NAME'        => $this->view->escapeHtml($geeftee->getName()),
            'GEEFTEE_PROFILE_URL' => $geeftee->getProfileUrl(),
            'FAMILY_NAME'         => $this->view->escapeHtml($family->getName()),
            'FAMILY_VIEW_URL'     => $family->getViewUrl()
        ];
    }
}
