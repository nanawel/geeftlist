<?php

namespace Geeftlist\Block\Widget\RecentActivity\Action;

use Geeftlist\Model\Activity;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Block\Template;

class Gift extends Template implements ActionInterface
{
    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        array $config = []
    ) {
        parent::__construct($context, $config);
    }

    protected function doRender(array $hive = null, ?int $ttl = 0): string {
        if (!$this->canRender($hive['activity'])) {
            return '';
        }

        return parent::doRender($this->getTemplateVars($hive['activity']), $ttl);
    }

    protected function getTemplateVars(Activity $activity): array {
        $actor = $activity->getActor();
        $gift = $activity->getObject();

        $geeftees = array_values($gift->getGeefteeCollection()->getItems());
        $geefteeCount = count($geeftees);

        if ($geefteeCount == 0) {
            $geeteesData = [
                [
                    'name' => $this->__('(nobody)'),
                    'profile_url' => null,
                ]
            ];
        }
        else {
            $geeteesData = array_map(fn($geeftee) => $this->geefteeToArray($geeftee, $actor), $geeftees);
        }

        return [
            'ACTION'              => $activity->getAction(),
            'GEEFTER_PROFILE_URL' => $actor->getProfileUrl(),
            'GEEFTER_USERNAME'    => $this->view->escapeHtml($actor->getUsername()),
            'GEEFTEES_DATA'       => $geeteesData,
            'GIFT_VIEW_URL'       => $gift->getViewUrl(),
            'GIFT_LABEL'          => $this->view->escapeHtml($gift->getLabel())
        ];
    }

    /**
     * @return array
     */
    public function geefteeToArray(Geeftee $geeftee, Geefter $actor) {
        $currentGeefteeId = $this->geefterSession->getGeefteeId();
        switch ($geeftee->getId()) {
            case $currentGeefteeId:
                $geeteeData = [
                    'name' => $this->view->escapeHtml($this->__('you')),
                    'profile_url' => $geeftee->getProfileUrl(),
                ];
                break;

            case $actor->getGeeftee()->getId():
                $geeteeData = [
                    'name' => $this->view->escapeHtml($this->__('himself/herself')),
                    'profile_url' => $geeftee->getProfileUrl(),
                ];
                break;

            default:
                $geeteeData = [
                    'name' => $this->view->escapeHtml($geeftee->getName()),
                    'profile_url' => $geeftee->getProfileUrl(),
                ];
                break;
        }

        return $geeteeData;
    }

    /**
     * @return bool
     */
    public function canRender(Activity $activity) {
        try {
            $activity->getActor();
            $gift = $activity->getObject();

            return (bool) count($gift->getGeefteeCollection());
        }
        catch (\Throwable $throwable) {
            $this->logger->error($throwable);

            return false;
        }
    }
}
