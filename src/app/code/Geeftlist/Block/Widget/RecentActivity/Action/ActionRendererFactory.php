<?php

namespace Geeftlist\Block\Widget\RecentActivity\Action;

use Geeftlist\Model\Activity;
use OliveOil\Core\Block\BlockInterface;
use OliveOil\Core\Service\GenericFactory;

class ActionRendererFactory extends GenericFactory
{
    /**
     * @return BlockInterface
     */
    public function getByModel(Activity $activity) {
        return $this->getFromCode(implode('-', [
            $activity->getActorType(),
            $activity->getAction(),
            $activity->getObjectType()
        ]));
    }
}
