<?php

namespace Geeftlist\Block;


use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use OliveOil\Core\Block\Template;
use OliveOil\Core\Model\I18n;

class Geeftee extends Template
{
    /** @var \Geeftlist\Model\Geeftee */
    protected $geeftee;

    public function __construct(
        \OliveOil\Core\Block\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Service\PermissionInterface $permissionService
    ) {
        parent::__construct($context);
    }

    public function init($name, &$config = []): void {
        parent::init($name, $config);

        // Preset default config
        $this->config += [
            'badges_block_name'     => 'geeftee_badges',
            'geeftee_name_html_tag' => 'h2',
            'show_add_gift_button'  => true,
            'show_action_buttons'   => true,
            'show_badges'           => true,
            'show_claim_link'       => true,
            'show_edit_button'      => true,
            'show_delete_button'    => true,
            'show_profile_link'     => true,
        ];
    }

    public function shouldShowClaimLink(\Geeftlist\Model\Geeftee $geeftee): bool {
        return $this->getConfig('show_claim_link')
            && $this->permissionService->isAllowed([$geeftee], [TypeInterface::ACTION_CREATE_CLAIM_REQUEST]);
    }

    public function shouldShowEditButton(\Geeftlist\Model\Geeftee $geeftee): bool {
        return $this->getConfig('show_edit_button')
            && $this->geefterSession->getGeeftee()->getId() != $geeftee->getId()
            && $this->permissionService->isAllowed([$geeftee], [TypeInterface::ACTION_EDIT]);
    }

    public function shouldShowDeleteButton(\Geeftlist\Model\Geeftee $geeftee): bool {
        return $this->getConfig('show_delete_button')
            && $this->geefterSession->getGeeftee()->getId() != $geeftee->getId()
            && $this->permissionService->isAllowed([$geeftee], [TypeInterface::ACTION_DELETE]);
    }

    public function shouldShowAddGiftButton(\Geeftlist\Model\Geeftee $geeftee): bool {
        return $this->getConfig('show_add_gift_button')
            && $this->permissionService->isAllowed([$geeftee], [TypeInterface::ACTION_ADD_GIFT]);
    }

    /**
     * @param I18n|null $i18n
     * @return string
     */
    public function getBirthDate(\Geeftlist\Model\Geeftee $geeftee, I18n $i18n = null) {
        return $this->geefteeHelper->getBirthDate($geeftee, $i18n);
    }

    public function getGiftCount(\Geeftlist\Model\Geeftee $geeftee): int {
        return $this->geefteeGiftHelper->getGiftCollection($geeftee)
            ->addFieldToFilter('status', Status::AVAILABLE)
            ->count();
    }
}
