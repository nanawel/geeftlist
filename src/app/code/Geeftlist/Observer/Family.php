<?php

namespace Geeftlist\Observer;

use Geeftlist\Exception\FamilyException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family\Field\Visibility;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee;
use Geeftlist\Model\Share\Family\RuleType\TypeInterface;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Family extends AbstractObserver
{
    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected \Geeftlist\Model\Family\RestrictionsAppenderInterface $familyRestrictionsAppender
    ) {
        $this->coreFactory = $context->getCoreFactory();
        parent::__construct($context);
    }

    public function beforeSaveSetOwner(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        if (! $family->getFlag('skip_autoset_owner') && ($currentGeefter = $this->geefterSession->getGeefter()) && ! $family->getOwnerId()) {
            $family->setOwnerId($currentGeefter->getId());
        }
    }

    public function preventOwnerRemoval(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        /** @var \Geeftlist\Model\Geeftee[] $geefteesForRemoval */
        $geefteesForRemoval = $ev->getParam('geeftees');

        if ($family->getOwnerId()) {
            $ownerGeefteeId = $family->getOwner()->getGeeftee()->getId();
            foreach ($geefteesForRemoval as $geefteeForRemoval) {
                if ($geefteeForRemoval->getId() == $ownerGeefteeId) {
                    throw new FamilyException('Cannot remove the owner of a family. He/she must transfer it first.');
                }
            }
        }
    }

    /**
     * Set default values if missing
     */
    public function setDefaultValues(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        if (!$family->getVisibility()) {
            $family->setVisibility(Visibility::PROTECTED);
        }
    }

    public function beforeSavePermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        if (!$this->permissionService->isAllowed($family, TypeInterface::ACTION_EDIT)) {
            throw new PermissionException('Cannot edit this family.');
        }
    }

    public function afterSaveAddOwnerToFamily(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        if (
            ($currentGeefter = $this->geefterSession->getGeefter()) && ($currentGeeftee = $this->geefterSession->getGeeftee()) && (! $family->getFlag('skip_autoadd_owner')
            && $family->getOrigData('family_id') === null
            && $family->getData('family_id') !== null
            && $family->getOwnerId() == $currentGeefter->getId())
        ) {
            $this->securityService->callPrivileged(static function () use ($family, $currentGeeftee): void {
                $family->addGeeftee($currentGeeftee);
            });
        }
    }

    public function beforeDeletePermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        if (!$this->permissionService->isAllowed($family, TypeInterface::ACTION_DELETE)) {
            throw new PermissionException('Cannot delete this family.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function addGeefteesPermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        if (!$this->permissionService->isAllowed($family, TypeInterface::ACTION_ADD_GEEFTEE)) {
            throw new PermissionException('Cannot add geeftee to this family.');
        }
    }

    /**
     * @throws NoSuchEntityException
     * @throws PermissionException
     */
    public function removeGeefteesPermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        $geeftees = $ev->getParam('geeftees');
        $currentGeeftee = $this->geefterSession->getGeeftee();

        $allowed = false;
        if (
            $currentGeeftee
            && count($geeftees) === 1
            && current($geeftees)->getId() == $currentGeeftee->getId()
        ) {
            $allowed = true;
        }
        elseif ($this->permissionService->isAllowed($family, TypeInterface::ACTION_REMOVE_GEEFTEE)) {
            $allowed = true;
        }

        if (!$allowed) {
            throw new PermissionException('Cannot remove geeftee from this family.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function replaceGeefteesPermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        $actions = [TypeInterface::ACTION_ADD_GEEFTEE, TypeInterface::ACTION_REMOVE_GEEFTEE];
        if (!$this->permissionService->isAllowed($family, $actions)) {
            throw new PermissionException('Cannot change geeftees on this family.');
        }
    }

    /**
     * @throws FamilyException
     */
    public function assertNotRemoveAll(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee[] $geeftees */
        $geeftees = $ev->getParam('geeftees');

        if (! $ev->getParam('negate') && empty($geeftees)) {
            throw new FamilyException('Must set the geeftees to be removed.');
        }
    }

    public function beforeTransferPermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        /** @var Geefter $newOwner */
        $newOwner = $ev->getParam('new_owner');

        if (! $newOwner->getId()) {
            throw new NoSuchEntityException('Unknown geefter');
        }

        if (! $this->permissionService->isAllowed($family, TypeInterface::ACTION_TRANSFER)) {
            throw new PermissionException('Cannot transfer this family.');
        }

        // Should also check that old and new owners are related but it is enforced by the
        // common family membership requirement
        if (! $family->isMember($newOwner->getGeeftee())) {
            throw new PermissionException('The new owner must be a member of the family.');
        }
    }

    /**
     * Exclude families according to current geeftee.
     */
    public function addGeefterAccessRestrictionsToCollection(EventInterface $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
            $collection = $ev->getTarget();

            $this->familyRestrictionsAppender
                ->addGeefterAccessRestrictions(
                    $collection,
                    $currentGeefter,
                    $collection->getFlag('required_actions') ?: TypeInterface::ACTION_VIEW
                );
        }
    }

    /**
     * Assign sourge geeftee's families to target's.
     */
    public function reassignGeefteeFamilies(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $sourceGeeftee */
        $sourceGeeftee = $ev->getParam('source_geeftee');
        /** @var \Geeftlist\Model\Geeftee $targetGeeftee */
        $targetGeeftee = $ev->getTarget();

        // Select and delete family/geeftee associations for families already assigned to $targetGeeftee
        // AND also to $sourceGeeftee (updating those rows would throw a unicity constraint error)
        $select = $this->connection->select(Geeftee::MAIN_TABLE)
            ->columns(['family_id'])
            ->where(['geeftee_id' => $targetGeeftee->getId()]);
        $resultSet = $this->connection->query($select);
        $alreadyAssignedFamilies = array_column($resultSet->toArray(), 'family_id');

        if ($alreadyAssignedFamilies !== []) {
            $delete = $this->connection->delete(Geeftee::MAIN_TABLE);
            $delete->where
                ->in('family_id', $alreadyAssignedFamilies)
                ->AND
                ->equalTo('geeftee_id', $sourceGeeftee->getId());
            $this->connection->query($delete);
        }

        // Now update safely family/geeftee associations from $sourceGeeftee to $targetGeeftee
        $update = $this->connection->update(Geeftee::MAIN_TABLE)
            ->set(['geeftee_id' => $targetGeeftee->getId()])
            ->where(['geeftee_id' => $sourceGeeftee->getId()]);
        $this->connection->query($update);
    }

    public function validateModel(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $model */
        $model = $ev->getTarget();

        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $constraints = [
            'name'       => [
                ErrorInterface::TYPE_FIELD_EMPTY,
                function ($value) use ($model) {
                    if (! $model->assertNameUnique()) {
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This family name is already taken!')
                            ->setMessage('This family name is already taken!');
                    }
                }
            ],
            'visibility' => [static fn($value): bool => empty($value) || in_array($value, \Geeftlist\Model\Family\Field\Visibility::VALUES)],
            'owner_id' => [function ($value) use ($model) {
                if (
                    ! $model->getFlag('allow_update_owner')
                    && $model->getOrigData('owner_id')
                    && $model->getOrigData('owner_id') != $value
                ) {
                    return $this->coreFactory->make(ErrorInterface::class)
                        ->setType(ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE)
                        ->setLocalizableMessage('Cannot update this field.')
                        ->setMessage('Cannot update this field.');
                }
            }]
        ];

        $this->modelValidator->validate($model, $constraints, $errorAggregator);
    }
}
