<?php

namespace Geeftlist\Observer;


use Geeftlist\Model\AbstractModel;
use Laminas\EventManager\Event;

class ActivityLog
{
    public function __construct(protected \Geeftlist\Service\ActivityLogInterface $activityLogService)
    {
    }

    public function onGeefterLogin(Event $ev): void {
        $this->activityLogService->log([
            'action'     => 'geefter_login',
            'geefter_id' => $ev->getParam('geefter')->getId(),
            'email'      => $ev->getParam('email')
        ]);
    }

    public function onGeefterFailedLogin(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'geefter_failed_login',
            'email'  => $ev->getParam('email')
        ]);
    }

    public function onGeefterLogout(Event $ev): void {
        $this->activityLogService->log([
            'action'     => 'geefter_logout',
            'geefter_id' => $ev->getParam('geefter')->getId()
        ]);
    }

    public function onSignupSuccess(Event $ev): void {
        $this->activityLogService->log([
            'action'               => 'geefter_signup_success',
            'geefter_id'           => $ev->getParam('geefter')->getId(),
            'username'             => $ev->getParam('geefter')->getUsername(),
            'email'                => $ev->getParam('email'),
            'pending_confirmation' => $ev->getParam('confirmation_needed'),
        ]);
    }

    public function onResetPasswordRequest(Event $ev): void {
        $this->activityLogService->log([
            'action'     => 'reset_password_request',
            'geefter_id' => $ev->getParam('geefter')->getId(),
            'email'      => $ev->getParam('email')
        ]);
    }

    public function onInvalidResetPasswordRequest(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'failed_reset_password_request',
            'email'  => $ev->getParam('email')
        ]);
    }

    public function onGeefterPasswordUpdate(Event $ev): void {
        $this->activityLogService->log([
            'action'     => 'geefter_password_update',
            'geefter_id' => $ev->getParam('geefter')->getId(),
            'email'      => $ev->getParam('email')
        ]);
    }

    public function onGeefterPasswordTokenFailure(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'geefter_password_token_failure',
            'email'  => $ev->getParam('email')
        ]);
    }

    public function onGeefterUpdateEmailTokenFailure(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'geefter_update_email_token_failure',
            'token'  => $ev->getParam('token')
        ]);
    }

    public function onGeefterUpdateEmailSuccess(Event $ev): void {
        $this->activityLogService->log([
            'action'     => 'geefter_update_email_success',
            'geefter_id' => $ev->getParam('geefter')->getId(),
            'old_email'  => $ev->getParam('old_email'),
            'new_email'  => $ev->getParam('new_email')
        ]);
    }

    public function onGeefterDeleteAccount(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $model */
        $model = $ev->getTarget();
        $this->activityLogService->log([
            'action'               => 'geefter_delete_account',
            'entity_type'          => $model->getEntityType(),
            'entity_id'            => $model->getId(),
            'username'             => $model->getUsername(),
            'email'                => $model->getEmail(),
            'recoverer_geefter_id' => $ev->getParam('recoverer_geefter')
                ? $ev->getParam('recoverer_geefter')->getId()
                : '<none>',
            'recoverer_geefter_email' => $ev->getParam('recoverer_geefter')
                ? $ev->getParam('recoverer_geefter')->getEmail()
                : '<none>'
        ]);
    }

    public function onSendContactRequest(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter|null $geefter */
        $geefter = $ev->getParam('geefter');

        $this->activityLogService->log([
            'action'          => 'send_contact_request',
            'requester_email' => $ev->getParam('requester_email'),
            'geefter_id'      => $geefter ? $geefter->getId() : null,
            'subject'         => $ev->getParam('subject')
        ]);
    }

    public function onModelSave(Event $ev): void {
        /** @var AbstractModel $target */
        $target = $ev->getTarget();

        $this->activityLogService->log(
            $this->getSaveModelData($target)
        );
    }

    public function onReservationSave(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $model */
        $model = $ev->getTarget();
        $data = $this->getSaveModelData($model);
        $data += [
            'geefter_id'  => $model->getGeefterId(),
            'gift_id'     => $model->getGiftId(),
            'orig_type'   => $model->getOrigData('type'),
            'type'        => $model->getType(),
            'orig_amount' => $model->getOrigData('amount'),
            'amount'      => $model->getAmount()
        ];
        $this->activityLogService->log($data);
    }

    public function onReservationCancel(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $model */
        $model = $ev->getTarget();
        $data = $this->getDeleteModelData($model);
        $data += [
            'geefter_id' => $model->getGeefterId(),
            'gift_id'    => $model->getGiftId(),
            'type'       => $model->getType(),
            'amount'     => $model->getAmount()
        ];
        $this->activityLogService->log($data);
    }

    public function onGeefteeAddGifts(Event $ev): void {
        foreach ($ev->getParam('gifts') as $gift) {
            /** @var \Geeftlist\Model\Geeftee $geeftee */
            $geeftee = $ev->getTarget();
            $this->activityLogService->log([
                'action'     => 'geeftee_add_gift',
                'geeftee_id' => $geeftee->getId(),
                'gift_id'    => $gift->getId()
            ]);
        }
    }

    public function onGeefteeRemoveGifts(Event $ev): void {
        foreach ($ev->getParam('gifts') as $gift) {
            /** @var \Geeftlist\Model\Geeftee $geeftee */
            $geeftee = $ev->getTarget();
            $this->activityLogService->log([
                'action'     => 'geeftee_remove_gift',
                'geeftee_id' => $geeftee->getId(),
                'gift_id'    => $gift->getId()
            ]);
        }
    }

    public function onGeefteeMerge(Event $ev): void {
        /** @var \Geeftlist\Model\Geeftee $targetGeeftee */
        $targetGeeftee = $ev->getParam('target_geeftee');  // "To"   => persisted afterwards
        /** @var \Geeftlist\Model\Geeftee $sourceGeeftee */
        $sourceGeeftee = $ev->getParam('source_geeftee');  // "From" => deleted afterwards

        $this->activityLogService->log([
            'action'               => 'geeftee_merge',
            'geeftee_id'           => $targetGeeftee->getId(),
            'source_geeftee_id'    => $sourceGeeftee->getId(),
            'source_geeftee_email' => $sourceGeeftee->getEmail(),
        ]);
    }

    public function onFamilyAddGeeftees(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        foreach ($ev->getParam('geeftees') as $geeftee) {
            $this->activityLogService->log([
                'action'     => 'family_add_geeftee',
                'family_id'  => $family->getId(),
                'geeftee_id' => $geeftee->getId()
            ]);
        }
    }

    public function onFamilyRemoveGeeftees(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        foreach ($ev->getParam('geeftees') as $geeftee) {
            $this->activityLogService->log([
                'action'     => 'family_remove_geeftee',
                'family_id'  => $family->getId(),
                'geeftee_id' => $geeftee->getId()
            ]);
        }
    }

    public function onSendGiftArchivingRequest(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'send_gift_archiving_request',
            'gift_id' => $ev->getParam('gift')->getId(),
            'gift_creator_username' => $ev->getParam('gift')->getCreator()->getUsername(),
            'gift_creator_email' => $ev->getParam('gift')->getCreator()->getEmail(),
            'requester_id' => $ev->getParam('requester')->getId(),
            'requester_username' => $ev->getParam('requester')->getUsername(),
            'requester_email' => $ev->getParam('requester')->getEmail()
        ]);
    }

    public function onSendGiftReportToCreator(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'send_gift_report_to_creator',
            'gift_id' => $ev->getParam('gift')->getId(),
            'gift_creator_username' => $ev->getParam('gift')->getCreator()->getUsername(),
            'gift_creator_email' => $ev->getParam('gift')->getCreator()->getEmail(),
            'requester_id' => $ev->getParam('requester')->getId(),
            'requester_username' => $ev->getParam('requester')->getUsername(),
            'requester_email' => $ev->getParam('requester')->getEmail(),
            'message' => $ev->getParam('message')
        ]);
    }

    public function onSendGiftReportToAdministrator(Event $ev): void {
        $this->activityLogService->log([
            'action' => 'send_gift_report_to_administrator',
            'gift_id' => $ev->getParam('gift')->getId(),
            'gift_label' => $ev->getParam('gift')->getLabel(),
            'gift_description' => $ev->getParam('gift')->getDescription(),
            'requester_id' => $ev->getParam('requester')->getId(),
            'requester_username' => $ev->getParam('requester')->getUsername(),
            'requester_email' => $ev->getParam('requester')->getEmail(),
            'message' => $ev->getParam('message')
        ]);
    }

    protected function getSaveModelData(\Geeftlist\Model\AbstractModel $model): array {
        return [
            'action'      => 'model_save',
            'entity_type' => $model->getEntityType(),
            'entity_id'   => $model->getId(),
            'is_new'      => $model->isObjectNew(),
        ];
    }

    protected function getDeleteModelData(\Geeftlist\Model\AbstractModel $model): array {
        return [
            'action'      => 'model_delete',
            'entity_type' => $model->getEntityType(),
            'entity_id'   => $model->getOrigData($model->getIdField()),
        ];
    }
}
