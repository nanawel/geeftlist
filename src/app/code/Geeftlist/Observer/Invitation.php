<?php

namespace Geeftlist\Observer;


use Geeftlist\Exception\Invitation\MaxReachedException;
use Geeftlist\Exception\Security\PermissionException;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Helper\DateTime;

class Invitation extends AbstractObserver
{
    protected \OliveOil\Core\Model\RepositoryInterface $invitationRepository;

    public function __construct(
        \Geeftlist\Observer\Context $context,
    ) {
        parent::__construct($context);
        $this->invitationRepository = $context->getModelRepositoryFactory()
            ->resolveGet(\Geeftlist\Model\Invitation::ENTITY_TYPE);
    }

    public function onSignupMarkInvitationAsUsed(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getParam('geefter');
        /** @var \Geeftlist\Model\Invitation|null $invitation */
        $invitation = $ev->getParam('invitation');

        if ($invitation) {
            $invitation->setUsedAt(DateTime::getDateSql())
                ->setGeefterId($geefter->getId());
            $this->invitationRepository->save($invitation);
        }
    }

    public function beforeSavePermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Invitation $invitation */
        $invitation = $ev->getTarget();

        if ($invitation->isObjectNew() && !$this->geefterSession->isLoggedIn()) {
            throw new PermissionException('Cannot create invitation.');
        }
    }

    public function onBeforeSaveCheckMaxInvitationReachedForGeefter(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Invitation $invitation */
        $invitation = $ev->getTarget();

        if (($maxInvitationPerGeefter = $this->appConfig->getValue('INVITATION_MAX_PER_GEEFTER')) > 0) {
            if ($creatorId = $invitation->getCreatorId()) {
                $invitationsCount = $this->geeftlistResourceFactory
                    ->resolveMakeCollection(\Geeftlist\Model\Invitation::ENTITY_TYPE)
                    ->addFieldToFilter('creator_id', ['eq' => $creatorId])
                    ->count();

                if ($invitationsCount >= $maxInvitationPerGeefter) {
                    throw new MaxReachedException('Sorry, the maximum invitations limit is reached.');
                }
            }
        }
    }
}
