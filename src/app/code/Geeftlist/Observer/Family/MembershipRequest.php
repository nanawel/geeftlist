<?php

namespace Geeftlist\Observer\Family;

use Geeftlist\Exception\Family\AlreadyMemberException;
use Geeftlist\Exception\Family\MembershipException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Family as FamilyResource;
use Geeftlist\Observer\AbstractObserver;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;
use Laminas\EventManager\Event;
use OliveOil\Core\Model\TransportObject;

class MembershipRequest extends AbstractObserver
{
    /**
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function addCurrentGeefterFilter(Event $ev): void {
        if (
            ($currentGeefter = $this->geefterSession->getGeefter())
            && ($currentGeeftee = $this->geefterSession->getGeeftee())
        ) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Collection $collection */
            $collection = $ev->getTarget();
            if (! $collection->getFlag('skip_current_geefter_restrictions')) {
                $select = new Select();
                $select->from(['family' => FamilyResource::MAIN_TABLE])
                    ->where
                        ->equalTo('main_table.geeftee_id', $currentGeeftee->getId())
                        ->or->equalTo('main_table.sponsor_id', $currentGeefter->getId())
                        ->or->NEST
                            ->equalTo('main_table.family_id', 'family.family_id', Predicate::TYPE_IDENTIFIER, Predicate::TYPE_IDENTIFIER)
                            ->AND
                            ->equalTo('family.owner_id', $currentGeefter->getId())
                        ->UNNEST;
                $collection->getSelect()->where(['EXISTS (?)' => $select]);
            }
        }
    }

    /**
     * @throws PermissionException
     */
    public function checkRequestSponsor(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $sponsor */
        $sponsor = $ev->getParam('sponsor');

        $currentGeefter = $this->geefterSession->getGeefter();
        if ($sponsor && $currentGeefter && $sponsor->getId() != $currentGeefter->getId()) {
            throw new PermissionException('Cannot impersonate another geefter as a sponsor.');
        }
    }

    public function setRequestSponsor(Event $ev): void {
        /** @var TransportObject $transportObject */
        $transportObject = $ev->getParam('transport_object');

        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            $ev->setParam('sponsor', $currentGeefter);
            $transportObject->setData('sponsor', $currentGeefter);
        }
    }

    public function setRequestGeeftee(Event $ev): void {
        /** @var TransportObject $transportObject */
        $transportObject = $ev->getParam('transport_object');
        /** @var Geeftee|null $geeftee */
        $geeftee = $ev->getParam('geeftee');

        if (empty($geeftee) && ($currentGeeftee = $this->geefterSession->getGeeftee())) {
            $ev->setParam('geeftee', $currentGeeftee);
            $transportObject->setData('geeftee', $currentGeeftee);
        }
    }

    /**
     * @throws MembershipException
     */
    public function preventProcessingClosedRequest(Event $ev): void {
        /** @var \Geeftlist\Model\Family\MembershipRequest $membershipRequest */
        $membershipRequest = $ev->getParam('membership_request');

        if ($membershipRequest->getDecisionCode() != Family\MembershipRequest::DECISION_CODE_PENDING) {
            throw new MembershipException('Cannot process a closed request.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanRequest(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getParam('family');
        /** @var Geeftee[] $geeftees */
        $geeftees = $ev->getParam('geeftees');
        /** @var Geefter $sponsor */
        $sponsor = $ev->getParam('sponsor');

        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            if ($sponsor->getId() != $currentGeefter->getId()) {
                throw new PermissionException('Cannot impersonate sponsor when creating a request.');
            }

            $isInvitation = ! isset($geeftees[$sponsor->getGeeftee()->getId()]);

            if ($isInvitation && ! $family->isMember($sponsor->getGeeftee())) {
                throw new PermissionException(
                    sprintf('Geefter %d is not a member of family %d', $sponsor->getId(), $family->getId())
                );
            }
        }
    }

    /**
     * @throws AlreadyMemberException
     * @throws PermissionException
     * @throws MembershipException
     */
    public function assertCanAccept(Event $ev): void {
        $this->assertCanAcceptOrReject($ev);
    }

    /**
     * @throws AlreadyMemberException
     * @throws PermissionException
     * @throws MembershipException
     */
    public function assertCanReject(Event $ev): void {
        $this->assertCanAcceptOrReject($ev);
    }

    /**
     * @throws AlreadyMemberException
     * @throws PermissionException
     * @throws MembershipException
     */
    public function assertCanAcceptOrReject(Event $ev): void {
        /** @var \Geeftlist\Model\Family\MembershipRequest $membershipRequest */
        $membershipRequest = $ev->getParam('membership_request');

        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            $family = $membershipRequest->getFamily();
            $geeftee = $membershipRequest->getGeeftee();
            $isInvitation = $membershipRequest->getSponsorId() !== null;

            if ($family->isMember($geeftee)) {
                $membershipRequest->setDecisionCode(Family\MembershipRequest::DECISION_CODE_CANCELED)
                    ->save();
                throw new AlreadyMemberException(
                    sprintf('Geeftee %d is already a member of family %d', $geeftee->getId(), $family->getId())
                );
            }

            if ($isInvitation) {
                if ($currentGeefter->getGeeftee()->getId() != $membershipRequest->getGeefteeId()) {
                    throw new MembershipException('Only the geeftee himself can accept or reject invitations.');
                }
            }
            elseif (! $family->isOwner($currentGeefter)) {
                throw new PermissionException('Must be owner of the family to accept or reject pending memberships.');
            }
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanCancel(Event $ev): void {
        /** @var \Geeftlist\Model\Family\MembershipRequest $membershipRequest */
        $membershipRequest = $ev->getParam('membership_request');

        $currentGeefter = $this->geefterSession->getGeefter();
        $currentGeeftee = $this->geefterSession->getGeeftee();
        $isInvitation = $membershipRequest->getSponsorId() !== null;

        if ($isInvitation) {
            if (
                $membershipRequest->getSponsorId() != $currentGeefter->getId()
                && $membershipRequest->getFamily()->getOwnerId() != $currentGeefter->getId()
            ) {
                throw new PermissionException(
                    'Must be the owner of the family or the creator of the request to cancel it.'
                );
            }
        }
        elseif ($membershipRequest->getGeefteeId() != $currentGeeftee->getId()) {
            throw new PermissionException('Must be the creator of the request to cancel it.');
        }
    }
}
