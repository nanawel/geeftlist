<?php

namespace Geeftlist\Observer\Family\MembershipRequest;


use Geeftlist\Model\Family;
use Laminas\EventManager\Event;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    public function onRequest(Event $ev): void {
        /** @var Family\MembershipRequest[] $requests */
        $requests = $ev->getParam('membership_requests');

        foreach ($requests as $request) {
            if ($request->getDecisionCode() == Family\MembershipRequest::DECISION_CODE_PENDING) {
                // Request
                if ($request->getSponsorId() === null) {
                    $this->securityService->callPrivileged(
                        fn(\Geeftlist\Model\Family\MembershipRequest $request) => $this->sendOwnerNewPendingMembershipRequestEmail($request),
                        [$request]
                    );
                }
                // Invitation
                else {
                    $this->securityService->callPrivileged(
                        fn(\Geeftlist\Model\Family\MembershipRequest $request) => $this->sendGeefteeNewPendingMembershipRequestEmail($request),
                        [$request]
                    );
                }
            }
        }
    }

    public function onReject(Event $ev): void {
        /** @var Family\MembershipRequest $request */
        $request = $ev->getParam('membership_request');

        if ($request->getSponsorId() === null) {
            $this->securityService->callPrivileged(
                fn(\Geeftlist\Model\Family\MembershipRequest $request) => $this->sendGeefteeRejectedMembershipRequestEmail($request),
                [$request]
            );
        }
    }

    public function sendOwnerNewPendingMembershipRequestEmail(Family\MembershipRequest $request): void {
        $owner = $request->getFamily()->getOwner();

        $email = $this->geefterEmailService->getPreparedEmail($owner)
            ->setTemplate('notification/family/membership_request/new_pending_owner.phtml')
            ->setRequest($request);
        $this->senderService->send($email);

        $this->logger->debug(
            sprintf('New membership request notification email sent to owner %s (request #%s)', $owner->getEmail(), $request->getId())
        );
    }

    public function sendGeefteeNewPendingMembershipRequestEmail(Family\MembershipRequest $request): void {
        $geeftee = $request->getGeeftee();

        if ($geefter = $geeftee->getGeefter()) {
            $email = $this->geefterEmailService->getPreparedEmail($geefter)
                ->setTemplate('notification/family/membership_request/new_pending_geeftee.phtml')
                ->setRequest($request);
            $this->senderService->send($email);

            $this->logger->debug(
                sprintf('New membership request notification email sent to geeftee %s (request #%s)', $geefter->getEmail(), $request->getId())
            );
        }
    }

    public function sendGeefteeRejectedMembershipRequestEmail(Family\MembershipRequest $request): void {
        $geeftee = $request->getGeeftee();

        if ($geefter = $geeftee->getGeefter()) {
            $email = $this->geefterEmailService->getPreparedEmail($geefter)
                ->setTemplate('notification/family/membership_request/rejected_geeftee.phtml')
                ->setRequest($request);
            $this->senderService->send($email);

            $this->logger->debug(
                sprintf('Rejected membership request notification email sent to geeftee %s (request #%s)', $geefter->getEmail(), $request->getId())
            );
        }
    }
}
