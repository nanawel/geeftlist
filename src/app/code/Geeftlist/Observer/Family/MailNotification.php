<?php

namespace Geeftlist\Observer\Family;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Notification;
use Laminas\EventManager\Event;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    public function onAddGeefteeNotifyGeeftee(Event $ev): void {
        $this->securityService->callPrivileged(
            fn(\Geeftlist\Model\Family $family, array $geeftees) => $this->sendNewMemberEmail($family, $geeftees),
            [$ev->getTarget(), $ev->getParam('geeftees')]
        );
    }

    public function onAddGeefteeNotifyFamily(Event $ev): void {
        /** @var Family $family */
        $family = $ev->getTarget();

        $geefteeIds = [];
        foreach ($ev->getParam('geeftees') as $geeftee) {
            $geefteeIds[] = $geeftee->getId();
        }

        /** @var Notification $notification */
        $notification = $this->geeftlistModelFactory->resolveMake(Notification::ENTITY_TYPE);
        $notification->setType(Notification::TYPE_EMAIL)
            ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
            ->setTargetType(Family::ENTITY_TYPE)
            ->setTargetId($family->getId())
            ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
            ->setEventName(Notification\Family\Constants::EVENT_JOIN)
            ->setEventParams(['geeftee_ids' => $geefteeIds])
            ->save();
    }

    /**
     * NOTICE: This notification is NOT asynchronous and must be sent when the geeftee joins the family.
     */
    public function onNewMemberNotifyOwner(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();
        /** @var Geeftee[] $geeftees */
        $geeftees = $ev->getParam('geeftees');

        $this->sendOwnerNewMemberEmail($family, $geeftees);
    }

    public function onRemoveGeefteeNotifyGeeftee(Event $ev): void {
        $this->securityService->callPrivileged(
            fn(\Geeftlist\Model\Family $family, array $geeftees) => $this->sendFamilyLeaveEmail($family, $geeftees),
            [$ev->getTarget(), $ev->getParam('geeftees')]
        );
    }

    public function onRemoveGeefteeNotifyFamily(Event $ev): void {
        /** @var Family $family */
        $family = $ev->getTarget();

        $geefteeIds = [];
        foreach ($ev->getParam('geeftees') as $geeftee) {
            $geefteeIds[] = $geeftee->getId();
        }

        /** @var Notification $notification */
        $notification = $this->geeftlistModelFactory->resolveMake(Notification::ENTITY_TYPE);
        $notification->setType(Notification::TYPE_EMAIL)
            ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
            ->setTargetType(Family::ENTITY_TYPE)
            ->setTargetId($family->getId())
            ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
            ->setEventName(Notification\Family\Constants::EVENT_LEAVE)
            ->setEventParams(['geeftee_ids' => $geefteeIds])
            ->save();
    }

    /**
     * @param Geeftee[] $geeftees
     */
    public function sendNewMemberEmail(Family $family, array $geeftees): void {
        foreach ($geeftees as $geeftee) {
            if ($geefter = $geeftee->getGeefter()) {
                $email = $this->geefterEmailService->getPreparedEmail($geefter)
                    ->setTemplate('notification/family/new_member.phtml')
                    ->setFamily($family);
                $this->senderService->send($email);

                $this->logger->debug(
                    sprintf('New member notification email sent to %s (family #%s)', $geefter->getEmail(), $family->getId())
                );
            }
        }
    }

    /**
     * @param Geeftee[] $geeftees
     */
    public function sendOwnerNewMemberEmail(Family $family, array $geeftees): void {
        if ($family->getOwnerId()) {
            $owner = $family->getOwner();
            foreach ($geeftees as $geeftee) {
                $email = $this->geefterEmailService->getPreparedEmail($owner)
                    ->setTemplate('notification/family/new_member_owner.phtml')
                    ->setGeeftee($geeftee)
                    ->setFamily($family);
                $this->senderService->send($email);

                $this->logger->debug(sprintf(
                    "New member notification email sent to owner %s (family #%d / geeftee #%d)",
                    $owner->getEmail(),
                    $family->getId(),
                    $geeftee->getId()
                ));
            }
        }
    }

    /**
     * @param Geeftee[] $geeftees
     */
    public function sendFamilyLeaveEmail(Family $family, array $geeftees): void {
        foreach ($geeftees as $geeftee) {
            if ($geefter = $geeftee->getGeefter()) {
                $email = $this->geefterEmailService->getPreparedEmail($geefter)
                    ->setTemplate('notification/family/leave_member.phtml')
                    ->setFamily($family);
                $this->senderService->send($email);

                $this->logger->debug(
                    sprintf('Family leave notification email sent to %s (family #%s)', $geefter->getEmail(), $family->getId())
                );
            }
        }
    }

    public function onOwnerChangeNotifyNewOwner(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getTarget();

        $origOwnerId = $family->getOrigData('owner_id');

        if (
            ! $origOwnerId                            // Did not have an owner
            || $family->getOwnerId() == $origOwnerId  // OR owner is un changed
        ) {
            // Then just don't do anything
            return;
        }

        $oldOwner = $this->geeftlistModelFactory->resolveMake(Geefter::ENTITY_TYPE);
        $oldOwner->load($origOwnerId);
        if (! $oldOwner->getId()) {
            return;
        }

        $newOwner = $family->getOwner();

        $email = $this->geefterEmailService->getPreparedEmail($newOwner)
            ->setTemplate('notification/family/transfer_new_owner.phtml')
            ->setFamily($family)
            ->setOldOwner($oldOwner);
        $this->senderService->send($email);

        $this->logger->debug(
            sprintf('Family transfer email sent to %s (family #%s)', $newOwner->getEmail(), $family->getId())
        );
    }
}
