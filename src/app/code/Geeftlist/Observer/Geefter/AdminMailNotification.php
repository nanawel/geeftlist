<?php

namespace Geeftlist\Observer\Geefter;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Notification;
use Laminas\EventManager\Event;

class AdminMailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    public function onNewGeefter(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        if ($geefter->isObjectNew()) {
            /** @var Notification $notification */
            $notification = $this->geeftlistModelFactory->resolveMake(Notification::ENTITY_TYPE);
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_ADMIN)
                ->setTargetType(Geefter::ENTITY_TYPE)
                ->setTargetId($geefter->getId())
                ->setCategory(Notification::CATEGORY_INSTANCE_ACTIVITY)
                ->setEventName(Notification\Geefter\Constants::EVENT_NEW)
                ->setEventParams([
                    'remote_ip'     => $this->fw->get('SERVER.REMOTE_ADDR'),
                    'forwarded_for' => $this->fw->get('SERVER.HTTP_X_FORWARDED_FOR')
                ])
                ->save();
        }
    }

    public function onDeleteGeefter(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        /** @var Notification $notification */
        $notification = $this->geeftlistModelFactory->resolveMake(Notification::ENTITY_TYPE);
        $notification->setType(Notification::TYPE_EMAIL)
            ->setRecipientType(Notification::RECIPIENT_TYPE_ADMIN)
            ->setTargetType(Geefter::ENTITY_TYPE)
            ->setTargetId($geefter->getId())
            ->setCategory(Notification::CATEGORY_INSTANCE_ACTIVITY)
            ->setEventName(Notification\Geefter\Constants::EVENT_DELETE)
            ->setEventParams([
                'username' => $geefter->getUsername(),
                'email' => $this->scrambleEmail($geefter->getEmail()),
                'remote_ip'     => $this->fw->get('SERVER.REMOTE_ADDR'),
                'forwarded_for' => $this->fw->get('SERVER.HTTP_X_FORWARDED_FOR')
            ])
            ->save();
    }

    /**
     * @param string $email
     */
    protected function scrambleEmail($email): string {
        if (!preg_match('/^(.+)@(.+)\.(.+)$/', $email, $matches)) {
            return '[invalid-email]';
        }

        return sprintf(
            '%s...%s@%s...%s.%s',
            substr($matches[1], 0, 1),
            substr($matches[1], -1),
            substr($matches[2], 0, 1),
            substr($matches[2], -1),
            $matches[3]
        );
    }
}
