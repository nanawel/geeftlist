<?php

namespace Geeftlist\Observer\Geefter;


use Geeftlist\Model\Geefter;
use Geeftlist\Observer\AbstractObserver;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Exception\Session\ExpiredException;

class PasswordBasedSessionValidation extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected string $geefterSessionSecretKey = ''
    ) {
        parent::__construct($context);
    }


    public function processSessionHash(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Session\Http\Geefter $session */
        $session = $ev->getTarget();

        if (!$this->appConfig->getValue('GEEFTER_VALIDATE_PASSWORD_HASH')) {
            return;
        }

        if ($geefter = $session->getGeefter()) {
            $sessionValidationHash = $session->getPasswordValidationHash();

            // Is validation possible for the current geefter?
            if ($this->shouldValidate($geefter)) {
                if (! $sessionValidationHash) {
                    // Validation hash not set yet, so set it if possible
                    $session->setPasswordValidationHash($this->getPasswordValidationHash($geefter));
                } elseif ($sessionValidationHash !== $this->getPasswordValidationHash($geefter)) {
                    // No need to be timing-attacks proof here
                    throw new ExpiredException('Password-based session validation failed.');
                }
            }
        }
    }

    public function updateSessionHash(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        if ($geefter->getPasswordHash() != $geefter->getOrigData('password_hash')) {
            $this->geefterSession->setPasswordValidationHash($this->getPasswordValidationHash($geefter));
        }
    }

    public function shouldValidate(Geefter $geefter): bool {
        return (bool)$geefter->getPasswordHash();
    }

    public function getPasswordValidationHash(Geefter $geefter): string {
        return sha1($this->geefterSessionSecretKey . $geefter->getPasswordHash());
    }
}
