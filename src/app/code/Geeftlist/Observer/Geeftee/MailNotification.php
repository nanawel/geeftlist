<?php

namespace Geeftlist\Observer\Geeftee;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Laminas\EventManager\Event;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        \Geeftlist\Service\Geefter\Email $geefterEmailService,
        protected \Geeftlist\Helper\Gift $giftHelper
    ) {
        parent::__construct($context, $senderService, $geefterEmailService);
    }

    public function onAddGiftsNotifyRelatives(Event $ev): void {
        foreach ($ev->getParam('gifts') as $gift) {
            /** @var Notification $notification */
            $notification = $this->geeftlistModelFactory->resolveMake(Notification::ENTITY_TYPE);
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                ->setTargetType(Gift::ENTITY_TYPE)
                ->setTargetId($gift->getId())
                ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                ->setEventName(Notification\Gift\Constants::EVENT_ADD_TO_GEEFTEES)
                ->setEventParams(['actor_id' => $this->geefterSession->getGeefterId()])
                ->save();
        }
    }

    public function onAddOpenGiftsNotifyGeeftees(Event $ev): void {
        foreach ($ev->getParam('gifts') as $gift) {
            if ($this->giftHelper->isOpenGift($gift)) {
                /** @var Notification $notification */
                $notification = $this->geeftlistModelFactory->resolveMake(Notification::ENTITY_TYPE);
                $notification->setType(Notification::TYPE_EMAIL)
                    ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                    ->setTargetType(Gift::ENTITY_TYPE)
                    ->setTargetId($gift->getId())
                    ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                    ->setEventName(Notification\Gift\Constants::EVENT_ADD_TO_ME)
                    ->setEventParams(['actor_id' => $this->geefterSession->getGeefterId()])
                    ->save();
            }
        }
    }

    public function onExistingGeefteeSignup(Event $ev): void {
        /** @var Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        $origCreatorId = $geeftee->getOrigData('creator_id');

        if (
            ! $origCreatorId                        // Did not have a creator
            || $geeftee->getCreatorId()             // OR has a creator now
            || $geeftee->getOrigData('geefter_id')  // OR has a geefter linked
            || ! $geeftee->getGeefterId()           // OR did not have geefter linked
        ) {
            // Then just don't do anything
            return;
        }

        $creator = $this->geeftlistModelFactory->resolveMake(Geefter::ENTITY_TYPE);
        $creator->load($origCreatorId);
        if (! $creator->getId()) {
            return;
        }

        $email = $this->geefterEmailService->getPreparedEmail($creator)
            ->setTemplate('notification/geeftee/registered_geeftee.phtml')
            ->setGeefteeEmail($geeftee->getEmail())
            ->setGeefteeProfileUrl($geeftee->getProfileUrl());
        $this->senderService->send($email);

        $this->logger->debug(
            sprintf('Registered geeftee email sent to %s (geeftee #%s)', $creator->getEmail(), $geeftee->getId())
        );
    }
}
