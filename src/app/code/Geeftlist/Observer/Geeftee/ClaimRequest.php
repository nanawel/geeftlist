<?php

namespace Geeftlist\Observer\Geeftee;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;
use Laminas\EventManager\Event;

class ClaimRequest extends \Geeftlist\Observer\AbstractObserver
{
    /**
     * @throws PermissionException
     */
    public function assertCanCreate(Event $ev): void {
        /** @var \Geeftlist\Model\Geeftee\ClaimRequest $claimRequest */
        $claimRequest = $ev->getTarget();
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $claimRequest->getGeeftee();

        if (!$claimRequest->getId()) {
            // FIXME Should be handled by a GeefterAccess class for ClaimRequests
            if (($currentGeefterId = $this->geefterSession->getGeefterId()) && $claimRequest->getGeefterId() != $currentGeefterId) {
                throw new PermissionException('Cannot create claim request.');
            }

            if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_CREATE_CLAIM_REQUEST)) {
                throw new PermissionException('Cannot create claim request.');
            }
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanAccept(Event $ev): void {
        /** @var \Geeftlist\Model\Geeftee\ClaimRequest $claimRequest */
        $claimRequest = $ev->getParam('claim_request');
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $claimRequest->getGeeftee();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST)) {
            throw new PermissionException('Cannot accept this claim request.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanReject(Event $ev): void {
        /** @var \Geeftlist\Model\Geeftee\ClaimRequest $claimRequest */
        $claimRequest = $ev->getParam('claim_request');
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $claimRequest->getGeeftee();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_REJECT_CLAIM_REQUEST)) {
            throw new PermissionException('Cannot reject this claim request.');
        }
    }

    public function addCurrentGeefterFilter(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Collection $collection */
            $collection = $ev->getTarget();
            if (! $collection->getFlag('skip_current_geefter_restrictions')) {
                $select = new Select();
                $select->from(['geeftee' => Geeftee::MAIN_TABLE])
                    ->where
                        ->equalTo('main_table.geefter_id', $currentGeefter->getId())
                        ->OR->NEST
                            ->equalTo(
                                'main_table.geeftee_id',
                                'geeftee.geeftee_id',
                                Predicate::TYPE_IDENTIFIER,
                                Predicate::TYPE_IDENTIFIER
                            )
                            ->AND
                            ->equalTo('geeftee.creator_id', $currentGeefter->getId())
                        ->UNNEST;
                $collection->getSelect()->where(['EXISTS (?)' => $select]);
            }
        }
    }
}
