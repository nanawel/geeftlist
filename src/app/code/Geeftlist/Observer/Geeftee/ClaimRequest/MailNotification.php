<?php

namespace Geeftlist\Observer\Geeftee\ClaimRequest;


use Geeftlist\Model\Geeftee;
use Laminas\EventManager\Event;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    public function onRequest(Event $ev): void {
        /** @var Geeftee\ClaimRequest $request */
        $request = $ev->getParam('claim_request');

        if ($request->getDecisionCode() == Geeftee\ClaimRequest::DECISION_CODE_PENDING) {
            $this->securityService->callPrivileged(
                fn(\Geeftlist\Model\Geeftee\ClaimRequest $request) => $this->sendCreatorNewPendingClaimRequestEmail($request),
                [$request]
            );
        }
    }

    public function onAccept(Event $ev): void {
        /** @var Geeftee\ClaimRequest $request */
        $request = $ev->getParam('claim_request');

        $this->securityService->callPrivileged(
            fn(\Geeftlist\Model\Geeftee\ClaimRequest $request) => $this->sendGeefteeAcceptedClaimRequestEmail($request),
            [$request]
        );
    }

    public function sendCreatorNewPendingClaimRequestEmail(Geeftee\ClaimRequest $request): void {
        if ($creator = $request->getGeeftee()->getCreator()) {
            if ($creator->getId() == $request->getGeefterId()) {
                // Geeftee's creator is also the geefter from the claim request: skip notification
                return;
            }

            $email = $this->geefterEmailService->getPreparedEmail($creator)
                ->setTemplate('notification/geeftee/claim_request/new_pending_creator.phtml')
                ->setRequest($request);
            $this->senderService->send($email);

            $this->logger->debug(
                sprintf('New claim request notification email sent to creator %s (request #%s)', $creator->getEmail(), $request->getId())
            );
        }
    }

    public function sendGeefteeAcceptedClaimRequestEmail(Geeftee\ClaimRequest $request): void {
        $geefter = $request->getGeefter();

        $email = $this->geefterEmailService->getPreparedEmail($geefter)
            ->setTemplate('notification/geeftee/claim_request/accepted_geeftee.phtml')
            ->setRequest($request);
        $this->senderService->send($email);

        $this->logger->debug(
            "New accepted claim request notification email sent to geefter "
            . sprintf('%s (request #%s)', $geefter->getEmail(), $request->getId())
        );
    }
}
