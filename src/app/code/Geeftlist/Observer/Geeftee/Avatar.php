<?php

namespace Geeftlist\Observer\Geeftee;


use Geeftlist\Model\Geeftee;
use Laminas\EventManager\EventInterface;

class Avatar
{
    public function __construct(protected \Geeftlist\Service\Avatar\Geeftee $geefteeAvatarService)
    {
    }

    public function applyAvatarOnSave(EventInterface $ev): void {
        /** @var Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        if (($avatarPath = $geeftee->getAvatarPath()) && $geeftee->getOrigData('avatar_path') != $avatarPath) {
            $this->geefteeAvatarService->apply($geeftee, $avatarPath);
        }
    }
}
