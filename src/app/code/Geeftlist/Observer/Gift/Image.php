<?php

namespace Geeftlist\Observer\Gift;


use Geeftlist\Model\Gift;
use Laminas\EventManager\EventInterface;

class Image
{
    public function __construct(protected \Geeftlist\Service\Gift\Image $giftImageService)
    {
    }

    public function applyImageOnSave(EventInterface $ev): void {
        /** @var Gift $gift */
        $gift = $ev->getTarget();

        if (($imageId = $gift->getImageId()) && $gift->getOrigData('image_id') != $imageId) {
            $this->giftImageService->apply($gift, $imageId);
        }
    }
}
