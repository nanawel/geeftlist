<?php

namespace Geeftlist\Observer\Gift;

use Geeftlist\Exception\Discussion\TopicException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Gift\Constants;
use Geeftlist\Model\ResourceModel\Db\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Observer\AbstractObserver;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\EventManager\Event;

class Topic extends AbstractObserver
{
    public function addTopicDefaultMetadata(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getTarget();

        if ($topic->getLinkedEntityType() == \Geeftlist\Model\Gift::ENTITY_TYPE) {
            /** @var \OliveOil\Core\Model\MagicObject $defaultMetadata */
            $defaultMetadata = $ev->getParam('default_metadata');
            foreach (Constants::TOPIC_DEFAULT_METADATA as $key => $value) {
                $defaultMetadata->setData($key, $value);
            }
        }
    }

    /**
     * @throws PermissionException
     * @throws TopicException
     */
    public function assertCanViewGift(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getTarget();

        // $topic->getEntity() might throw a PermissionException itself
        if ($topic->getLinkedEntityType() == \Geeftlist\Model\Gift::ENTITY_TYPE && ! $this->permissionService->isAllowed($topic->getEntity(), TypeInterface::ACTION_VIEW)) {
            throw new PermissionException('The current geefter cannot see this gift.');
        }
    }

    /**
     * @throws TopicException
     */
    public function assertGiftTopicUnicity(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getParam('object');

        if (! $topic->getId() && $topic->getLinkedEntityType() == \Geeftlist\Model\Gift::ENTITY_TYPE) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection $collection */
            $collection = $this->geeftlistResourceFactory
                ->resolveMakeCollection(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE)
                ->addFieldToFilter('linked_entity_id', $topic->getLinkedEntityId())
                ->addFieldToFilter('metadata', [
                    Constants::TOPIC_VISIBILITY_KEY,
                    $topic->getMetadataByKey(Constants::TOPIC_VISIBILITY_KEY)
                ])
                ->setFlag('skip_discussion_topic_gift_restrictions')
                ->setFlag('skip_discussion_topic_gift_related_geeftee_gifts_restrictions');

            if ($collection->count()) {
                /** @var \Geeftlist\Model\Discussion\Topic $existingTopic */
                $existingTopic = $collection->getFirstItem();

                throw new TopicException(sprintf(
                    'Topic #%d on the same gift (%d) with the same visibility (%s) already exists.',
                    $existingTopic->getId(),
                    $existingTopic->getLinkedEntityId(),
                    $existingTopic->getMetadataByKey(Constants::TOPIC_VISIBILITY_KEY)
                ));
            }
        }
    }

    public function giftTopicEntityInstanciate(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getTarget();

        if ($topic->getLinkedEntityType() == \Geeftlist\Model\Gift::ENTITY_TYPE) {
            $transportObject = $ev->getParam('transport_object');
            $gift = $this->modelRepositoryFactory->resolveGet(\Geeftlist\Model\Gift::ENTITY_TYPE)
                ->get($topic->getLinkedEntityId());
            if ($gift) {
                $transportObject->setEntity($gift);
            }
        }
    }

    /**
     * @throws TopicException
     */
    public function overrideTopicTitleWithGiftLabel(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getTarget();
        if ($topic->getLinkedEntityType() == \Geeftlist\Model\Gift::ENTITY_TYPE) {
            $topic->setTitle($topic->getEntity()->getLabel());
        }
    }

    public function overrideTopicCollectionTitleWithGiftLabelBeforeLoad(Event $ev): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection $collection */
        $collection = $ev->getTarget();
        $collection->getSelect()->join(
            ['topic_gift' => Gift::MAIN_TABLE],
            new Expression(
                'main_table.linked_entity_type = ? AND main_table.linked_entity_id = topic_gift.gift_id',
                \Geeftlist\Model\Gift::ENTITY_TYPE
            ),
            ['gift_topic_title' => 'label'],
            Select::JOIN_LEFT
        );
    }

    public function overrideTopicCollectionTitleWithGiftLabelAfterLoad(Event $ev): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection $collection */
        $collection = $ev->getTarget();
        foreach ($collection as $topic) {
            if (($title = $topic->getGiftTopicTitle()) !== null) {
                $topic->setTitle($title);
            }
        }
    }

    public function getUrl(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getTarget();

        if ($topic->getLinkedEntityType() == \Geeftlist\Model\Gift::ENTITY_TYPE) {
            /** @var \Geeftlist\Model\Gift $gift */
            $gift = $topic->getEntity();
            $ev->getParam('transport_object')
                ->setUrl($gift->getViewUrl());
        }
    }
}
