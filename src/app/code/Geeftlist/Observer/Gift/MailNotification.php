<?php

namespace Geeftlist\Observer\Gift;

use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Geeftlist\Model\Reservation;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\TransportObject;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function getNotificationRepository() {
        /** @var \OliveOil\Core\Model\RepositoryInterface $repositoryFactory */
        $repositoryFactory = $this->modelRepositoryFactory->resolveGet(Notification::ENTITY_TYPE);

        return $repositoryFactory;
    }

    public function onGiftArchivedNotifyReservationGeefters(Event $ev): void {
        /** @var Gift $gift */
        $gift = $ev->getTarget();

        if ($gift->getStatus() != Gift\Field\Status::ARCHIVED) {
            return;
        }

        $this->notifyReservationGeeftersGiftNotAvailableAnymore(
            $gift,
            'notification/reservation/archived_gift.phtml'
        );
    }

    public function onGiftMarkedAsDeletedNotifyReservationGeefters(Event $ev): void {
        /** @var Gift $gift */
        $gift = $ev->getTarget();

        $this->notifyReservationGeeftersGiftNotAvailableAnymore(
            $gift,
            'notification/reservation/deleted_gift.phtml'
        );
    }

    /**
     * @param string $emailTemplate
     */
    protected function notifyReservationGeeftersGiftNotAvailableAnymore(Gift $gift, $emailTemplate) {
        // Skip notification if the gift was previously not available
        if ($gift->getOrigData('status') != Gift\Field\Status::AVAILABLE) {
            return;
        }

        // Must be privileged to be sure we retrieve *all* geefters with reservations
        $this->securityService->callPrivileged(function () use ($gift, $emailTemplate): void {
            $creator = $gift->getCreator();
            $reservations = $gift->getReservationCollection();

            /** @var Reservation $reservation */
            foreach ($reservations as $reservation) {
                $geefter = $reservation->getGeefter();
                $i18n = $geefter->getI18n();

                // Skip notification for the current geefter
                if ($geefter->getId() == $this->geefterSession->getGeefterId()) {
                    continue;
                }

                $geefteeNames = [];
                foreach ($gift->getGeefteeCollection() as $giftGeeftee) {
                    $geefteeNames[] = $creator->getGeeftee()->getId() == $giftGeeftee->getId()
                        ? $i18n->tr('himself/herself')
                        : $giftGeeftee->getName();
                }

                $email = $this->geefterEmailService->getPreparedEmail($geefter)
                    ->setTemplate($emailTemplate)
                    ->setGiftLabel($gift->getLabel())
                    ->setGeefteeNames($geefteeNames)
                    ->setReservationDate($i18n->date($reservation->getReservedAt()))
                    ->setCreatorName($creator->getUsername())
                    ->setCreatorEmail($creator->getEmail())
                ;
                $this->senderService->send($email);

                $this->logger->debug(sprintf(
                    "Notified %s that reserved gift #%d is not available anymore (status = %s)",
                    $geefter->getEmail(),
                    $gift->getId(),
                    $gift->getStatus()
                ));
            }
        });
    }

    public function onGiftChangeNotifyReservationGeefters(Event $ev): void {
        /** @var Gift $gift */
        $gift = $ev->getTarget();

        // Skip notification if the gift is not available or if it is new
        if (
            $gift->getStatus() != Gift\Field\Status::AVAILABLE
            || $gift->isObjectNew()
        ) {
            return;
        }

        $fieldChanges = $this->getSignificantFieldChanges($gift);
        if ($fieldChanges === []) {
            return;
        }

        /** @var Notification $notification */
        $notification = $this->getNotificationRepository()->newModel();
        $notification->setType(Notification::TYPE_EMAIL)
            ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
            ->setTargetType(Gift::ENTITY_TYPE)
            ->setTargetId($gift->getId())
            ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
            ->setEventName(Notification\Gift\Constants::EVENT_CHANGE)
            ->setEventParams([
                'actor_id'       => $this->geefterSession->getGeefterId(),
                'changed_fields' => $fieldChanges,
            ]);
        $this->getNotificationRepository()->save($notification);
    }

    public function onGiftGeefteesChangeNotifyReservationGeefters(Event $ev): void {
        /** @var Gift $gift */
        $gift = $ev->getTarget();

        // Skip notification if the gift is not available
        if ($gift->getStatus() != Gift\Field\Status::AVAILABLE) {
            return;
        }

        /** @var Notification $notification */
        $notification = $this->getNotificationRepository()->newModel();
        $notification->setType(Notification::TYPE_EMAIL)
            ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
            ->setTargetType(Gift::ENTITY_TYPE)
            ->setTargetId($gift->getId())
            ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
            ->setEventName(Notification\Gift\Constants::EVENT_CHANGE)
            ->setEventParams([
                'actor_id'       => $this->geefterSession->getGeefterId(),
                'changed_fields' => ['geeftees']  // Not an actual field, but used as such
            ]);
        $this->getNotificationRepository()->save($notification);
    }

    protected function getSignificantFieldChanges(Gift $gift): array {
        $significantFields = [
            'label',
            'description',
            'estimated_price',
            'priority'
        ];

        $oldValues = \OliveOil\array_union_intersect($gift->getOrigData(), $significantFields);
        $newValues = \OliveOil\array_union_intersect($gift->getData(), $significantFields);

        $diffKeys = [];
        foreach ($oldValues as $field => $oldValue) {
            if ($newValues[$field] != $oldValue) {
                $diffKeys[] = $field;
            }
        }

        return $diffKeys;
    }

    /**
     * @see prepareEmailNotifications event
     */
    public function mergeGiftChangeNotifications(EventInterface $ev): void {
        /** @var TransportObject $transportObject */
        $transportObject = $ev->getParam('transport_object');
        /** @var Notification[] $notifications */
        $notifications = $transportObject->getData('notifications');
        /** @var Notification[] $notificationsForDeletion */
        $notificationsForDeletion = $transportObject->getData('notifications_for_deletion');

        // Keep only GIFT::CHANGE notifications
        $mergeableGiftNotifications = array_filter($notifications, static fn(Notification $notification): bool => $notification->getTargetType() == Gift::ENTITY_TYPE
            && $notification->getEventName() == Notification\Gift\Constants::EVENT_CHANGE);

        if ($mergeableGiftNotifications !== []) {
            // Group by gift_id
            /** @var Notification[][] $notificationsByGifts */
            $notificationsByGifts = [];
            foreach ($mergeableGiftNotifications as $mergeableGiftNotification) {
                $notificationsByGifts[$mergeableGiftNotification->getTargetId()][] = $mergeableGiftNotification;
            }

            // Do merge
            $newNotificationsForDeletion = [];
            foreach ($notificationsByGifts as $giftNotifications) {
                if (count($giftNotifications) > 1) {
                    $referenceNotification = array_shift($giftNotifications);

                    // Create a new notification using the first found
                    $mergedNotification = $referenceNotification->cloneAsNew();
                    $mergedNotificationEventParams = $referenceNotification->getEventParams();

                    // Convert and merge event params into $mergedNotification
                    $newNotificationsForDeletion[] = $referenceNotification;
                    while ($n = array_shift($giftNotifications)) {
                        $mergedNotificationEventParams['changed_fields'] = array_merge(
                            $mergedNotificationEventParams['changed_fields'],
                            $n->getEventParams()['changed_fields']
                        );

                        // Delete old notification
                        $newNotificationsForDeletion[] = $n;
                    }

                    $mergedNotificationEventParams['changed_fields'] = array_values(array_unique(
                        $mergedNotificationEventParams['changed_fields']
                    ));

                    $mergedNotification->setEventParams($mergedNotificationEventParams);

                    // Add new "merged" notification for save
                    $notifications[] = $mergedNotification;
                }
            }

            // Update data in transport object
            $transportObject->setData('notifications', $notifications);
            if ($newNotificationsForDeletion !== []) {
                $transportObject->setData('notifications_for_deletion', array_merge(
                    $notificationsForDeletion,
                    $newNotificationsForDeletion
                ));
            }
        }
    }
}
