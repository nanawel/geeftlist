<?php

namespace Geeftlist\Observer\Share\Entity;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use Laminas\EventManager\Event;
use OliveOil\Core\Exception\Di\Exception;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Rule
{
    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory, protected \OliveOil\Core\Service\GenericFactoryInterface $shareRuleTypeFactoryFactory, protected \OliveOil\Core\Model\RepositoryInterface $shareEntityRuleRepository, protected \Geeftlist\Helper\Share\Entity $shareEntityRuleHelper, protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator)
    {
    }

    public function validateEntityShareRules(Event $ev): void {
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        if ($shareRules = $this->extractShareRules($ev)) {
            /** @var \Geeftlist\Model\Share\Entity\Rule $shareRule */
            foreach ($shareRules as $shareRule) {
                $errorAggregator->merge($shareRule->validate());
            }
        }
    }

    public function validateModel(Event $ev): void {
        /** @var \Geeftlist\Model\Share\Entity\Rule $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $validationRules = [
            'target_entity_type' => [
                ErrorInterface::TYPE_FIELD_EMPTY
            ],
            'rule_type' => [
                ErrorInterface::TYPE_FIELD_EMPTY
            ]
        ];
        $this->modelValidator->validate($model, $validationRules, $errorAggregator);
        if (!$errorAggregator->isEmpty()) {
            return;
        }

        // Then check type-specific data

        try {
            /** @var \OliveOil\Core\Service\GenericFactoryInterface $typeFactory */
            $typeFactory = $this->shareRuleTypeFactoryFactory->getFromCode($model->getTargetEntityType());
        }
        catch (Exception $exception) {
            $errorAggregator->addFieldError($this->coreFactory->make(ErrorInterface::class)
                ->setField('target_entity_type')
                ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                ->setLevel(ErrorInterface::LEVEL_ERROR)
                ->setLocalizableMessage('Invalid rule target entity type.')
                ->setMessage('Invalid rule target entity type.'));

            return;
        }

        try {
            /** @var \Geeftlist\Model\Share\RuleTypeInterface $ruleType */
            $ruleType = $typeFactory->resolveGet($model->getRuleType());
        }
        catch (Exception) {
            $errorAggregator->addFieldError($this->coreFactory->make(ErrorInterface::class)
                ->setField('rule_type')
                ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                ->setLevel(ErrorInterface::LEVEL_ERROR)
                ->setLocalizableMessage('Invalid rule type.')
                ->setMessage('Invalid rule type.'));

            return;
        }

        $ruleType->validateRule($model, $errorAggregator);
    }

    /**
     * Save/update share rule according to event params or model's data.
     */
    public function saveEntityShareRule(Event $ev): void {
        /** @var \Geeftlist\Model\AbstractModel $model */
        $model = $ev->getTarget();

        $shareRules = $this->extractShareRules($ev);

        // Only save share rules if new data has been found
        if ($shareRules !== null) {
            $this->shareEntityRuleHelper->saveShareRule($model, $shareRules);
        }
    }

    public function deleteShareRuleWithEntity(Event $ev): void {
        /** @var AbstractModel $target */
        $target = $ev->getTarget();

        $this->shareEntityRuleRepository->deleteAll([
            'filter' => [
                'target_entity_type' => [['eq' => $target->getEntityType()]],
                'target_entity_id' => [['eq' => $target->getId()]],
            ]
        ]);
    }

    protected function extractShareRules(Event $ev) {
        /** @var \Geeftlist\Model\AbstractModel $model */
        $model = $ev->getTarget();
        $shareRules = $ev->getParam('share_rules');

        if (! $shareRules) {
            $shareRuleConfiguration = $model->getDataUsingMethod(Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY);
            if (is_array($shareRuleConfiguration)) {
                $shareRules = $this->shareEntityRuleHelper
                    ->prepareEntityShareRules($model, $shareRuleConfiguration);
                $model->setShareRules($shareRules)
                    ->unsetData(Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY);
            }
            elseif (is_array($shareRules = $model->getShareRules())) {
                foreach ($shareRules as $shareRule) {
                    // Force target entity ID (the object may have been created when we didn't have this data yet)
                    $shareRule->setTargetEntityId($model->getId());
                }
            }
            else {
                $shareRules = null;
            }
        }

        return $shareRules;
    }

    public function applyDefaultShareRuleToAllEntities(Event $e): void {
        $this->shareEntityRuleHelper->applyDefaultShareRuleToAllEntities($e->getParam('entity_type'));
    }

    public function applyDefaultShareRuleToEntities(Event $e): void {
        $this->doApplyDefaultShareRuleToEntities($e->getParam('entities'));
    }

    public function applyDefaultShareRuleToEntity(Event $e): void {
        $this->doApplyDefaultShareRuleToEntities([$e->getParam('entity')]);
    }

    protected function doApplyDefaultShareRuleToEntities(array $entities) {
        $this->shareEntityRuleHelper->applyDefaultShareRule($entities);
    }
}
