<?php

namespace Geeftlist\Observer;


class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\Session\Manager $sessionManager,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Model\ModelFactory $geeftlistModelFactory,
        protected \Geeftlist\Model\ResourceFactory $geeftlistResourceFactory,
        protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory,
        protected \Geeftlist\Service\Security $securityService,
        protected \OliveOil\Core\Service\Log $logService,
        protected \Geeftlist\Service\Permission $permissionService
    ) {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getConnection(): \OliveOil\Core\Model\ResourceModel\Db\Connection {
        return $this->connection;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getSessionManager(): \OliveOil\Core\Service\Session\Manager {
        return $this->sessionManager;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }

    public function getGeeftlistModelFactory(): \Geeftlist\Model\ModelFactory {
        return $this->geeftlistModelFactory;
    }

    public function getGeeftlistResourceFactory(): \Geeftlist\Model\ResourceFactory {
        return $this->geeftlistResourceFactory;
    }

    public function getModelRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->modelRepositoryFactory;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }

    public function getPermissionService(): \Geeftlist\Service\Permission {
        return $this->permissionService;
    }
}
