<?php

namespace Geeftlist\Observer;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Exception\Security\UnrelatedGeefteeException;
use Geeftlist\Model\MailNotification\Constants;
use Laminas\EventManager\Event;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Geefter extends AbstractObserver
{
    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    /**
     * @param string $usernameValidationPattern
     */
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected $usernameValidationPattern
    ) {
        $this->coreFactory = $context->getCoreFactory();
        parent::__construct($context);
    }

    public function updateLastLoginDate(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getParam('geefter');
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter $geefterResource */
        $geefterResource = $this->geeftlistResourceFactory->resolveGet(\Geeftlist\Model\Geefter::ENTITY_TYPE);

        $lastLoginAt = DateTime::getDate();
        $geefterResource->updateLastLoginAt($geefter, $lastLoginAt);
        $geefter->setLastLoginAt(DateTime::getDateSql($lastLoginAt));
    }

    public function addRelatedGeeftersFilter(Event $ev): void {
        if (($currentGeefter = $this->geefterSession->getGeefter())) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $collection */
            $collection = $ev->getTarget();
            if (! $collection->getFlag('skip_current_geefter_related_geefter_restrictions')) {
                $collection->addFieldToFilter('related_geefter', ['eq' => $currentGeefter->getId()]);
            }
        }
    }

    public function validateModel(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $overwritableFields = [
            'username',
            'email',
            'dob',
            'password',
            'language'
        ];

        $validationRules = [
            'username' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                ErrorInterface::TYPE_FIELD_EMPTY,
                function ($value) use ($model) {
                    if (! $model->assertUsernameUnique()) {
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This username is already taken!')
                            ->setMessage('This username is already taken!');
                    }
                },
                function ($value) use ($model) {
                    // #559 Skip validation only for existing geefters (pre-#140) if username has not been changed
                    if (
                        $model->getOrigData('username') !== $value
                        && ! preg_match($this->usernameValidationPattern, $value)
                    ) {
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This username is not valid.')
                            ->setMessage('This username is not valid.');
                    }
                }
            ],
            'email' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                ErrorInterface::TYPE_FIELD_EMPTY,
                $this->coreFactory->make(\OliveOil\Core\Service\Validation\Email::class),
                function ($value) use ($model) {
                    $error = $this->coreFactory->make(ErrorInterface::class);
                    if (! $model->assertEmailUnique()) {
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This email already exists!')
                            ->setMessage('This email already exists!');
                    }
                }
            ],
            'dob' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                static function ($value) {
                    if ($value && trim($value)) {
                        return preg_match('/^\d{4}-\d{2}-\d{2}/', $value) === 1;
                    }
                }
            ],
            'password' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                function ($value) {
                    if ($value && ($value = trim($value))) {
                        /** @var \OliveOil\Core\Model\Validation\ErrorInterface $error */
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        if (mb_strlen($value) < ($minLength = $this->appConfig->getValue('PASSWORD_MIN_LENGTH'))) {
                            return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                                ->setLocalizableMessage($this->i18n->tr(
                                    'The password must be at least {0} characters long.',
                                    $minLength
                                ))
                                ->setMessage(sprintf(
                                    'The password must be at least %d characters long.',
                                    $minLength
                                ));
                        }
                    }
                }
            ],
            'language' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                function ($value) {
                    if ($value && ($value = trim($value))) {
                        return $this->i18n->isValidLanguage($value);
                    }
                }
            ],
            'email_notification_freq' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                static function ($value) {
                    if ($value && ($value = trim($value))) {
                        return in_array($value, Constants::SENDING_FREQ);
                    }
                }
            ],
            '*' => function ($value, $field) use ($model, $overwritableFields) {
                if (! in_array($field, $overwritableFields)) {
                    if (is_scalar($value)) {
                        $value = trim($value);
                    }

                    if ($model->hasData($field) && $value != $model->getData($field)) {
                        /** @var \OliveOil\Core\Model\Validation\ErrorInterface $error */
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE)
                            ->setField($field)
                            ->setLevel(ErrorInterface::LEVEL_ERROR)
                            ->setLocalizableMessage('Field "{0}" cannot be overwritten.')
                            ->setMessage(sprintf('Field "%s" cannot be overwritten.', $field));
                    }
                }
            }
        ];

        $this->modelValidator->validate($model, $validationRules, $errorAggregator);
    }

    /**
     * @throws PermissionException
     */
    public function beforeSavePermissionCheck(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        $this->assertIsCurrentGeefter($geefter);
    }

    /**
     * @throws PermissionException
     */
    public function beforeDeletePermissionCheck(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        $this->assertIsCurrentGeefter($geefter);
    }

    /**
     * @throws PermissionException
     */
    public function assertIsCurrentGeefter(\Geeftlist\Model\Geefter $geefter): void {
        if (($currentGeefter = $this->geefterSession->getGeefter()) && $geefter->getId() != $currentGeefter->getId()) {
            throw new PermissionException('Cannot modify this geefter.');
        }
    }

    /**
     * @throws UnrelatedGeefteeException
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function assertRecovererRelated(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        if ($recovererGeefter = $ev->getParam('recoverer_geefter')) {
            /** @var \Geeftlist\Model\Geefter $recovererGeefter */
            if (
                ! $recovererGeefter->getGeeftee()->getId()
                || ! $this->geefteeHelper->assertIsGeefteeRelated(
                    $recovererGeefter->getGeeftee(),
                    $geefter->getGeeftee()
                )
            ) {
                throw new UnrelatedGeefteeException('The specified geefter is not a relative.');
            }
        }
    }

    /**
     * Temporary, will be customizable by geefter in the future
     * @see \Geeftlist\Model\Geefter::configureI18n()
     */
    public function forceTimezone(Event $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();

        $geefter->setTimezone($this->appConfig->getValue('TIMEZONE'));
    }
}
