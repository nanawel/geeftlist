<?php

namespace Geeftlist\Observer\Discussion\Post;

use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Notification;
use Laminas\EventManager\Event;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    public function onPostAddNotifyFamily(Event $ev): void {
        /** @var Post $post */
        $post = $ev->getTarget();

        if (! $post->getOrigData('post_id') && $post->getId()) {
            /** @var Notification $notification */
            $notification = $this->notificationRepository->newModel();
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                ->setTargetType(Post::ENTITY_TYPE)
                ->setTargetId($post->getId())
                ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                ->setEventName(Notification\Discussion\Post\Constants::EVENT_NEW);
            $this->notificationRepository->save($notification);
        }
    }

    public function onPostAddNotifyRecipients(Event $ev): void {
        /** @var Post $post */
        $post = $ev->getTarget();

        // Only for new Post entities
        if (
            ! $post->getOrigData('post_id')
            && $post->getId()
            && ($recipientIds = $post->getRecipientIds())
        ) {
            /** @var Notification $notification */
            $notification = $this->notificationRepository->newModel();
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                ->setTargetType(Post::ENTITY_TYPE)
                ->setTargetId($post->getId())
                ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                ->setDeferrable(0)
                ->setEventName(Notification\Discussion\Post\Constants::EVENT_NEW_RECIPIENTS)
                ->setEventParams([
                    'recipient_ids' => $recipientIds,
                    'topic_title' => $post->getTopic()->getTitle(),
                    'topic_url' => $post->getTopic()->getUrl(),
                    'title' => $post->getTitle(),
                    'message' => $post->getMessage(),
                    'author_name' => $post->getAuthor()->getName(),
                    'author_link' => $post->getAuthor()->getProfileUrl(),
                    'post_date' => $post->getCreatedAt()
                ]);
            $this->notificationRepository->save($notification);
        }
    }
}
