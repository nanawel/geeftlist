<?php

namespace Geeftlist\Observer\Discussion;

use Geeftlist\Exception\Discussion\TopicException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Exception\SecurityException;
use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;
use Geeftlist\Observer\AbstractObserver;
use Laminas\EventManager\Event;

class Topic extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $topicRepository,
        protected \Geeftlist\Indexer\Discussion\Topic\GeefterAccess $topicGeefterAccessIndexer,
        protected \Geeftlist\Model\Discussion\Topic\RestrictionsAppenderInterface $topicRestrictionsAppender,
        protected \Geeftlist\Helper\Gift\Discussion\Topic $topicHelper
    ) {
        parent::__construct($context);
    }

    public function beforeDeletePermissionCheck(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            throw new TopicException('You cannot delete this topic.');
        }
    }

    public function deleteTopicWithLinkedEntity(Event $ev): void {
        /** @var AbstractModel $target */
        $target = $ev->getTarget();

        $this->securityService->callPrivileged(function () use ($target): void {
            $this->topicRepository->deleteAll([
                'filter' => [
                    'linked_entity_type' => [['eq' => $target->getEntityType()]],
                    'linked_entity_id' => [['eq' => $target->getId()]],
                ]
            ]);
        });
    }

    /**
     * Set gift's creator if missing
     */
    public function setAuthor(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $ev->getTarget();
        if (! $post->getFlag('skip_autoset_author') && ($currentGeefter = $this->geefterSession->getGeefter()) && ! $post->getAuthorId()) {
            $post->setAuthorId($currentGeefter->getId());
        }
    }

    public function addGeefterAccessRestrictionsToCollection(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Collection $collection */
            $collection = $ev->getTarget();

            $this->topicRestrictionsAppender
                ->addGeefterAccessRestrictions($collection, $currentGeefter, TypeInterface::ACTION_VIEW);
        }
    }

    public function updateIndexes(Event $ev): void {
        /** @var AbstractModel $target */
        $target = $ev->getTarget();

        /** @var \Geeftlist\Model\Discussion\Topic[] $topics */
        $topics = $this->getAllRelatedTopics([$target]);
        if ($ev->getParam('topics')) {
            $topics = array_merge($topics, $ev->getParam('topics'));
        }

        if ($ev->getParam('topic')) {
            $topics = array_merge($topics, [$ev->getParam('topic')]);
        }

        foreach ($topics as $i => $topic) {
            if ($topic->getFlag('skip_reindex')) {
                unset($topic[$i]);
            }
        }

        $this->topicGeefterAccessIndexer->reindexEntities($topics);
    }

    public function prepareIndexesUpdateOnFamilyDeletion(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getParam('family');

        $this->securityService->callPrivileged(function () use ($family, $ev): void {
            $geeftees = $family->getGeefteeCollection();
            $family->attachInstanceListener('delete::after', function () use ($geeftees, $ev): void {
                $ev->setParam('geeftees', $geeftees);
                $this->onFamilyChangeUpdateIndexes($ev);
            });
        });
    }

    public function onFamilyChangeUpdateIndexes(Event $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getParam('family');
        /** @var \Geeftlist\Model\Geeftee[] $geeftees */
        $geeftees = $ev->getParam('geeftees', []);

        $topics = [];
        $this->securityService->callPrivileged(function () use ($family, $geeftees, &$topics): void {
            $topics = array_merge(
                $this->getAllRelatedTopics([$family]),
                $this->getAllRelatedTopics($geeftees)
            );
        });

        $this->topicGeefterAccessIndexer->reindexEntities($topics);
    }

    /**
     * @param AbstractModel[] $entities
     * @return \Geeftlist\Model\Discussion\Topic[]
     */
    protected function getAllRelatedTopics($entities): array {
        $topics = [];
        foreach ($entities as $entity) {
            if ($entity instanceof Family) {
                $topics = array_merge(
                    $topics,
                    $this->getAllRelatedTopics($entity->getGeefteeCollection()->getItems())
                );
            }

            if ($entity instanceof Geeftee) {
                $topics = array_merge(
                    $topics,
                    $this->getAllRelatedTopics($entity->getGiftCollection()->getItems())
                );
            }

            if ($entity instanceof Gift) {
                $topics = array_merge(
                    $topics,
                    $this->getAllRelatedTopics($this->topicHelper->getTopicCollection($entity))
                );
            }

            if ($entity instanceof Topic) {
                $topics = array_merge(
                    $topics,
                    [$entity]
                );
            }
        }

        return $topics;
    }

    /**
     * @throws PermissionException
     */
    public function assertCanView(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $ev->getTarget();
        if (
            $topic->getId()
            && ! $this->permissionService->isAllowed($topic, TypeInterface::ACTION_VIEW)
        ) {
            throw new PermissionException('The current geefter cannot see this topic.');
        }
    }

    /**
     * @throws SecurityException
     */
    public function preventLinkedEntityUpdate(Event $ev): void {
        if (($currentGeefter = $this->geefterSession->getGeefter())) {
            /** @var \Geeftlist\Model\Discussion\Topic $topic */
            $topic = $ev->getTarget();
            if (
                $topic->getId() && ($topic->getLinkedEntityId() != $topic->getOrigData('linked_entity_id')
                || $topic->getLinkedEntityType() != $topic->getOrigData('linked_entity_type'))
            ) {
                throw new SecurityException('Cannot update linked entity link.');
            }
        }
    }
}
