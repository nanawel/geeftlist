<?php

namespace Geeftlist\Observer\Discussion;

use Geeftlist\Exception\Discussion\Post\ManageException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;
use Geeftlist\Observer\AbstractObserver;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Post extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected \Geeftlist\Model\Discussion\Post\RestrictionsAppenderInterface $postRestrictionsAppender
    ) {
        parent::__construct($context);
    }

    /**
     * @throws ManageException
     * @throws PermissionException
     */
    public function beforeSavePermissionCheck(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $target */
        $target = $ev->getTarget();

        $this->assertIsAuthor($target);
        $this->assertCanPost($target);
    }

    /**
     * @throws ManageException
     */
    public function beforeDeletePermissionCheck(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $target */
        $target = $ev->getTarget();

        $this->assertIsAuthor($target);
    }

    /**
     * Set gift's creator if missing
     */
    public function setAuthor(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $ev->getTarget();
        if (! $post->getFlag('skip_autoset_author') && ($currentGeefter = $this->geefterSession->getGeefter()) && ! $post->getAuthorId()) {
            $post->setAuthorId($currentGeefter->getId());
        }
    }

    /**
     * @throws ManageException
     */
    protected function assertIsAuthor(\Geeftlist\Model\Discussion\Post $post) {
        if (($currentGeefter = $this->geefterSession->getGeefter()) && ($post->getAuthorId() && ! $post->getFlag('skip_current_geefter_author_restrictions')) && $post->getAuthorId() != $currentGeefter->getId()) {
            throw new ManageException('Current geefter is not the author of this post.');
        }
    }

    public function addGeefterTopicRestrictions(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Post\Collection $collection */
            $collection = $ev->getTarget();

            if (! $collection->getFlag('skip_post_topic_restrictions')) {
                $this->postRestrictionsAppender
                    ->addGeefterAccessRestrictions($collection, $currentGeefter, TypeInterface::ACTION_VIEW);
            }
        }
    }

    public function addCurrentGeefterAllowedActionFlags(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Discussion\Post\Collection $collection */
            $collection = $ev->getTarget();

            if ($collection->getFlag('skip_append_geefter_access_permissions')) {
                return;
            }

            $this->postRestrictionsAppender->appendGeefterAccessPermissions($collection, $currentGeefter);
        }
    }

    /**
     * @throws PermissionException
     */
    protected function assertCanPost(\Geeftlist\Model\Discussion\Post $post) {
        /** @var \Geeftlist\Model\Discussion\Topic|null $topic */
        $topic = $post->getTopic();
        if (!$topic) {
            throw new PermissionException('Cannot save the post: missing or invalid topic.');
        }

        if (!$this->permissionService->isAllowed($topic, TypeInterface::ACTION_POST)) {
            throw new PermissionException('The current geefter cannot post in this topic.');
        }
    }

    public function validateModel(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $constraints = [
            'topic_id'  => [ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE],
            'author_id' => [ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE],
            'message'   => [ErrorInterface::TYPE_FIELD_EMPTY],
        ];

        $this->modelValidator->validate($model, $constraints, $errorAggregator);
    }
}
