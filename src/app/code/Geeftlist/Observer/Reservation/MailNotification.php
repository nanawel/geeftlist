<?php

namespace Geeftlist\Observer\Reservation;

use Geeftlist\Model\Notification;
use Geeftlist\Model\Reservation;
use Laminas\EventManager\Event;

class MailNotification extends \Geeftlist\Observer\MailNotification\AbstractObserver
{
    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    protected function getNotificationRepository() {
        /** @var \OliveOil\Core\Model\RepositoryInterface $repositoryFactory */
        $repositoryFactory = $this->modelRepositoryFactory->resolveGet(Notification::ENTITY_TYPE);

        return $repositoryFactory;
    }

    public function onReservationSaveNotifyFamily(Event $ev): void {
        /** @var Reservation $reservation */
        $reservation = $ev->getTarget();

        $eventParams = [
            'type'   => $reservation->getType(),
            'amount' => $reservation->getAmount()
        ];
        if (! $reservation->getOrigData('reservation_id') && $reservation->getId()) {
            /** @var Notification $notification */
            $notification = $this->getNotificationRepository()->newModel();
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                ->setTargetType(Reservation::ENTITY_TYPE)
                ->setTargetId($reservation->getId())
                ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                ->setEventName(Notification\Reservation\Constants::EVENT_NEW)
                ->setEventParams($eventParams);
            $this->getNotificationRepository()->save($notification);
        } elseif ($this->hasSignificantChange($reservation)) {
            $eventParams += [
                'old_type'   => $reservation->getOrigData('type'),
                'old_amount' => $reservation->getOrigData('amount'),
            ];
            /** @var Notification $notification */
            $notification = $this->getNotificationRepository()->newModel();
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                ->setTargetType(Reservation::ENTITY_TYPE)
                ->setTargetId($reservation->getId())
                ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                ->setEventName(Notification\Reservation\Constants::EVENT_UPDATE)
                ->setEventParams($eventParams);
            $this->getNotificationRepository()->save($notification);
        }
    }

    /**
     * @param \Geeftlist\Model\Reservation $reservation
     */
    protected function hasSignificantChange($reservation): bool {
        $oldRelevantData = \OliveOil\array_mask(
            $reservation->getOrigData(),
            [
                'type',
                'amount'
            ]
        );
        $newRelevantData = \OliveOil\array_mask(
            $reservation->getData(),
            [
                'type',
                'amount'
            ]
        );

        return $oldRelevantData !== $newRelevantData;
    }

    public function onReservationDeleteNotifyFamily(Event $ev): void {
        /** @var Reservation $reservation */
        $reservation = $ev->getTarget();

        if ($reservation->getOrigData('reservation_id')) {
            /** @var Notification $notification */
            $notification = $this->getNotificationRepository()->newModel();
            $notification->setType(Notification::TYPE_EMAIL)
                ->setRecipientType(Notification::RECIPIENT_TYPE_GEEFTER)
                ->setTargetType(Reservation::ENTITY_TYPE)
                ->setTargetId($reservation->getId())
                ->setCategory(Notification::CATEGORY_FAMILIES_ACTIVITY)
                ->setEventName(Notification\Reservation\Constants::EVENT_DELETE)
                ->setEventParams([
                    'gift_id' => $reservation->getGiftId(),
                    'geefter_id' => $reservation->getGeefterId()
                ]);
            $this->getNotificationRepository()->save($notification);
        }
    }
}
