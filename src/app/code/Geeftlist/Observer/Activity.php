<?php

namespace Geeftlist\Observer;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Activity\Field\Action;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\EventManager\Event;

class Activity extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $activityRepository,
        protected \Geeftlist\Model\Gift\RestrictionsAppenderInterface $giftRestrictionsAppender
    ) {
        parent::__construct($context);
    }

    public function deleteActivityWithEntity(Event $ev): void {
        /** @var AbstractModel $target */
        $target = $ev->getTarget();

        if ($target->getEntityType() && $target->getId()) {
            $this->activityRepository->deleteAll([
                'filter' => [
                    'object_type' => [['eq' => $target->getEntityType()]],
                    'object_id' => [['eq' => $target->getId()]],
                ]
            ]);
        }
    }

    public function onGiftSave(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();

        if (! in_array($gift->getStatus(), [Gift\Field\Status::ARCHIVED, Gift\Field\Status::DELETED])) {
            $this->newActivityInstance([
                'actor_type'  => Geefter::ENTITY_TYPE,
                'actor_id'    => $gift->getCreatorId(),
                'object_type' => Gift::ENTITY_TYPE,
                'object_id'   => $gift->getId(),
                'action'      => $gift->isObjectNew() ? Action::CREATE : Action::UPDATE
            ])->save();
        }
    }

    public function onGiftMarkAsDeleted(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();

        $this->newActivityInstance([
            'actor_type'  => Geefter::ENTITY_TYPE,
            'actor_id'    => $gift->getCreatorId(),
            'object_type' => Gift::ENTITY_TYPE,
            'object_id'   => $gift->getId(),
            'action'      => Action::DELETE
        ])->save();
    }

    public function onFamilyAddGeeftee(Event $ev): void {
        /** @var \Geeftlist\Model\Activity $activity */
        $activity = $ev->getTarget();

        /** @var Geeftee $geeftee */
        foreach ($ev->getParam('geeftees') as $geeftee) {
            $this->newActivityInstance([
                'actor_type'  => Geeftee::ENTITY_TYPE,
                'actor_id'    => $geeftee->getId(),
                'object_type' => Family::ENTITY_TYPE,
                'object_id'   => $activity->getId(),
                'action'      => Action::JOIN
            ])->save();
        }
    }


    public function onFamilyRemoveGeeftee(Event $ev): void {
        /** @var \Geeftlist\Model\Activity $activity */
        $activity = $ev->getTarget();

        /** @var Geeftee $geeftee */
        foreach ($ev->getParam('geeftees') as $geeftee) {
            $this->newActivityInstance([
                'actor_type'  => Geeftee::ENTITY_TYPE,
                'actor_id'    => $geeftee->getId(),
                'object_type' => Family::ENTITY_TYPE,
                'object_id'   => $activity->getId(),
                'action'      => Action::LEAVE
            ])->save();
        }
    }

    public function onFamilyDelete(Event $ev): void {
        // Delete all associated activities
        $this->securityService->callPrivileged(function (Family $family): void {
            $this->geeftlistResourceFactory->resolveMakeCollection(\Geeftlist\Model\Activity::ENTITY_TYPE)
                ->addFieldToFilter('object_type', Family::ENTITY_TYPE)
                ->addFieldToFilter('object_id', $family->getId())
                ->delete();
        }, [$ev->getTarget()]);
    }

    /**
     * Add a filter to the activity collection so that to exclude items with gift objects
     * intended to the current geeftee unless their creator is the current geefter.
     */
    public function addCurrentGeefterGiftsFilter(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Activity\Collection $collection */
            $collection = $ev->getTarget();
            if (! $collection->getFlag('skip_activity_gift_current_geefter_gifts_restrictions')) {
                $this->giftRestrictionsAppender->addGeefterAccessRestrictionsToCollection(
                    $collection,
                    $currentGeefter,
                    'main_table',
                    'object_id',
                    'object_type',
                    TypeInterface::ACTION_VIEW
                );
            }
        }
    }

    public function excludeGeefteelessGifts(Event $ev): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection */
        $collection = $ev->getTarget();

        if ($collection->getFlag('skip_exclude_geefteeless_gifts')) {
            return;
        }

        $subSelect = $this->connection
            ->select(['geeftee_gift' => \Geeftlist\Model\ResourceModel\Db\Gift::GEEFTEE_GIFT_TABLE]);
        $subSelect->where
            ->equalTo(
                'main_table.object_id',
                'geeftee_gift.gift_id',
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_IDENTIFIER
            );

        $select = $collection->getSelect();
        $select->where->NEST
            ->expression(
                'main_table.object_type != ? XOR (main_table.object_type = ? AND (EXISTS (?)))',
                [Gift::ENTITY_TYPE, Gift::ENTITY_TYPE, $subSelect]
            )
            ->UNNEST;
    }

    /**
     * Add a filter to the activity collection so that to exclude items with family objects
     * the current geeftee is not a member of.
     */
    public function addFamiliesFilter(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Activity\Collection $collection */
            $collection = $ev->getTarget();
            if (!$collection->getFlag('skip_activity_family_current_geeftee_restrictions')) {
                if ($familyIds = $this->geefterSession->getFamilyIds()) {
                    $select = $collection->getSelect();
                    $select->where->NEST
                            ->notEqualTo('main_table.object_type', Family::ENTITY_TYPE)
                            ->OR->NEST
                                ->equalTo('main_table.object_type', Family::ENTITY_TYPE)
                                ->AND
                                ->in('main_table.object_id', $familyIds)
                            ->UNNEST
                        ->UNNEST;
                }
                else {
                    $select = $collection->getSelect();
                    $select->where
                        ->notEqualTo('main_table.object_type', Family::ENTITY_TYPE);
                }
            }
        }
    }

    /**
     * Assign sourge geeftee's activities to target's.
     */
    public function reassignGeefteeActivities(Event $ev): void {
        /** @var \Geeftlist\Model\Geeftee $sourceGeeftee */
        $sourceGeeftee = $ev->getParam('source_geeftee');
        /** @var \Geeftlist\Model\Geeftee $targetGeeftee */
        $targetGeeftee = $ev->getTarget();

        $update = $this->connection->update(\Geeftlist\Model\ResourceModel\Db\Activity::MAIN_TABLE)
            ->set(['actor_id' => $targetGeeftee->getId()])
            ->where(['actor_id' => $sourceGeeftee->getId()])
            ->where(['actor_type' => Geeftee::ENTITY_TYPE]);
        $this->connection->query($update);

        $update = $this->connection->update(\Geeftlist\Model\ResourceModel\Db\Activity::MAIN_TABLE)
            ->set(['object_id' => $targetGeeftee->getId()])
            ->where(['object_id' => $sourceGeeftee->getId()])
            ->where(['object_type' => Geeftee::ENTITY_TYPE]);
        $this->connection->query($update);
    }

    protected function newActivityInstance($data = []) {
        return $this->geeftlistModelFactory
            ->resolveMake(\Geeftlist\Model\Activity::ENTITY_TYPE, ['data' => $data]);
    }
}
