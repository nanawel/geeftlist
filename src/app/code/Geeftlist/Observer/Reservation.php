<?php

namespace Geeftlist\Observer;

use Geeftlist\Exception\ReservationException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Reservation extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected \Geeftlist\Model\Reservation\RestrictionsAppenderInterface $restrictionsAppender,
        protected \Geeftlist\Indexer\Gift\GeefterAccess $giftGeefterAccessIndexer
    ) {
        parent::__construct($context);
    }

    /**
     * Exclude gifts for the current geeftee.
     */
    public function addGeefterAccessRestrictionsToCollection(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Reservation\Collection $collection */
            $collection = $ev->getTarget();

            $this->restrictionsAppender->addGeefterAccessRestrictions(
                $collection,
                $currentGeefter,
                \Geeftlist\Model\Share\Reservation\RuleType\TypeInterface::ACTION_VIEW
            );
        }
    }

    /**
     * Append allowed actions to the gift collection.
     */
    public function appendCurrentGeefterAccessPermissions(Event $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Reservation\Collection $collection */
            $collection = $ev->getTarget();

            $this->restrictionsAppender->appendGeefterAccessPermissions($collection, $currentGeefter);
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanView(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $reservation */
        $reservation = $ev->getTarget();

        if (!$gift = $reservation->getGift()) {
            throw new PermissionException('The current geefter cannot see this reservation.');
        }

        if (! $this->permissionService->isAllowed($gift, TypeInterface::ACTION_VIEW)) {
            throw new PermissionException('The current geefter cannot see this reservation.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanReserve(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $reservation */
        $reservation = $ev->getTarget();

        if (!$gift = $reservation->getGift()) {
            throw new PermissionException('The current geefter cannot see this reservation.');
        }

        if (!count($gift->getGeefteeIds())) {
            throw new PermissionException('The gift must be assigned to a geeftee before reservation.');
        }

        // NEW reservation: check that this is allowed for all specified geeftees
        if (!$reservation->getId()) {
            if (
                ! $this->permissionService->isAllowed(
                    $gift->getGeefteeCollection()->getItems(),
                    \Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface::ACTION_RESERVE_GIFT
                )
            ) {
                throw new PermissionException('The current geefter cannot reserve a gift for the selected geeftee(s).');
            }

            // Now check that reservation is allowed on the gift itself
            if (! $this->permissionService->isAllowed($gift, TypeInterface::ACTION_RESERVE_PURCHASE)) {
                throw new PermissionException('The current geefter cannot create a reservation for this gift.');
            }
        }
        else {
            // Only check that we're dealing with one of the geefter's own reservations
            // FIXME Should be handled by the PermissionService too
            $currentGeefterId = $this->geefterSession->getGeefterId();
            if ($currentGeefterId && $reservation->getGeefterId() != $currentGeefterId) {
                throw new PermissionException('The current geefter cannot modify this reservation.');
            }
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanDelete(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $reservation */
        $reservation = $ev->getTarget();

        if (!$gift = $reservation->getGift()) {
            throw new PermissionException('The current geefter cannot see this reservation.');
        }

        if (! $this->permissionService->isAllowed($gift, TypeInterface::ACTION_CANCEL_RESERVATION)) {
            throw new PermissionException('The current geefter cannot delete this reservation.');
        }
    }

    /**
     * @throws ReservationException
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function preventReservationOfOwnGift(Event $ev): void {
        if ($currentGeeftee = $this->geefterSession->getGeeftee()) {
            /** @var \Geeftlist\Model\Reservation $reservation */
            $reservation = $ev->getTarget();
            if ($gift = $reservation->getGift()) {
                $giftGeeftees = $gift->getGeefteeCollection()->getAllIds();
                if (in_array($currentGeeftee->getId(), $giftGeeftees)) {
                    throw new ReservationException('The current geefter cannot create a reservation for this gift.');
                }
            }
        }
    }

    public function validateModel(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $constraints = [
            'gift_id' => [ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE],
            'type' => [static fn($value): bool => in_array($value, \Geeftlist\Model\Reservation::AVAILABLE_TYPES)],
            'amount' => [static fn($value): bool => empty($value) || (float) $value]
        ];

        $this->modelValidator->validate($model, $constraints, $errorAggregator);
    }

    public function updateIndexes(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Reservation[] $reservations */
        $reservations = $ev->getParam('reservations') ?: [$ev->getParam('reservation')];

        $giftIds = [];
        foreach ($reservations as $i => $reservation) {
            if ($reservation->getFlag('skip_reindex')) {
                unset($reservations[$i]);
            }
            else {
                $giftIds[] = $reservation->getGiftId();
            }
        }

        $gifts = $this->geeftlistResourceFactory->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE)
            ->addFieldToFilter('gift_id', ['in' => $giftIds]);

        $this->giftGeefterAccessIndexer->reindexEntities($gifts->getItems());
    }
}
