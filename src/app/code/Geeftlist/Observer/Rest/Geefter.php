<?php

namespace Geeftlist\Observer\Rest;


use Laminas\EventManager\EventInterface;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Rest\JsonApi\Params;

class Geefter
{
    public function __construct(protected \Geeftlist\Model\Session\GeefterInterface $geefterSession)
    {
    }

    public function preventOtherGeefterDataRetrieval(EventInterface $ev): void {
        /** @var Params $params */
        $params = $ev->getParam('params');

        $id = $params->getPrimaryEntityId();
        if ($id && $id != $this->geefterSession->getGeefterId()) {
            throw new ForbiddenException('Cannot get geefter data.');
        }
    }

    public function forceReturningOnlyOwnGeefterData(EventInterface $ev): void {
        if ($geefterId = $this->geefterSession->getGeefterId()) {
            /** @var Params $params */
            $params = $ev->getParam('params');

            // Completely overwrite collection constraints to return only current geefter
            $params->setPrimaryCollectionConstraints([
                'filter' => [
                    RepositoryInterface::WILDCARD_ID_FIELD => [
                        ['eq' => $geefterId]
                    ]
                ]
            ]);
        }
    }
}
