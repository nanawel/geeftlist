<?php

namespace Geeftlist\Observer\Cron\MailNotification;


use Geeftlist\Model\Notification;
use OliveOil\Core\Helper\DateTime;

class Cleanup extends \Geeftlist\Observer\AbstractObserver
{
    /**
     * @param string $notificationRecipientType
     */
    public function __construct(
        \Geeftlist\Observer\Cron\MailNotification\Context $context,
        protected $notificationRecipientType
    ) {
        parent::__construct($context);
    }

    /**
     * Delete old processed notifications from the notification and notification_* tables.
     */
    public function execute(): static {
        $this->cleanupSuccessNotifications();
        $this->cleanupFailedNotifications();

        return $this;
    }

    protected function cleanupSuccessNotifications(): static {
        $successKeepTimeLimit = new \DateTime();
        $successKeepTimeLimit->sub(new \DateInterval(sprintf(
            "PT%dH",
            (int) $this->appConfig->getValue('NOTIFICATION_SUCCESS_CLEANUP_HISTORY_KEEP')
        )));
        $processedNotifications = $this->geeftlistResourceFactory->resolveMakeCollection(Notification::ENTITY_TYPE)
            ->addFieldToFilter('recipient_type', $this->notificationRecipientType)
            ->addFieldToFilter('status', Notification::STATUS_PROCESSED)
            ->addFieldToFilter('updated_at', ['lt' => DateTime::getDateSql($successKeepTimeLimit)]);

        if ($cnt = $processedNotifications->count()) {
            $this->logger->debug(sprintf('Cleaning up %d successfully processed notifications...', $cnt));
            $processedNotifications->delete();
            $this->logger->debug(sprintf('%d notifications have been deleted.', $cnt));
        }

        return $this;
    }

    protected function cleanupFailedNotifications(): static {
        $failureKeepTimeLimit = new \DateTime();
        $failureKeepTimeLimit->sub(new \DateInterval(sprintf(
            "PT%dH",
            (int) $this->appConfig->getValue('NOTIFICATION_FAILURE_CLEANUP_HISTORY_KEEP')
        )));
        $failedNotifications = $this->geeftlistResourceFactory->resolveMakeCollection(Notification::ENTITY_TYPE)
            ->addFieldToFilter('recipient_type', $this->notificationRecipientType)
            ->addFieldToFilter('status', Notification::STATUS_FAILED)
            ->addFieldToFilter('updated_at', ['lt' => DateTime::getDateSql($failureKeepTimeLimit)]);

        if ($cnt = $failedNotifications->count()) {
            $this->logger->debug(sprintf('Cleaning up %d failed notifications...', $cnt));
            $failedNotifications->delete();
            $this->logger->debug(sprintf('%d notifications have been deleted.', $cnt));
        }

        return $this;
    }
}
