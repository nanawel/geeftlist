<?php

namespace Geeftlist\Observer\Cron\MailNotification;


use Geeftlist\Model\MailNotification\MailData\ItemList;
use Geeftlist\Model\Notification;
use OliveOil\Core\Helper\DateTime;

/**
 * Status Workflow: CREATED => PROCESSED|FAILED
 */
class Admin
{
    public const EMAIL_TEMPLATE = 'admin/notification/latest.phtml';

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \Geeftlist\Model\ResourceFactory $geeftlistResourceFactory;

    protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService;

    protected \OliveOil\Core\Service\GenericFactoryInterface $mailNotificationItemServiceFactory;

    protected \OliveOil\Core\Model\RepositoryInterface $notificationRepository;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        \Geeftlist\Observer\Cron\MailNotification\Context $context,
        protected \Geeftlist\Service\Admin\Email $adminEmailService
    ) {
        $this->appConfig = $context->getAppConfig();
        $this->geeftlistResourceFactory = $context->getGeeftlistResourceFactory();
        $this->senderService = $context->getSenderService();
        $this->mailNotificationItemServiceFactory = $context->getMailNotificationItemServiceFactory();
        $this->notificationRepository = $context->getNotificationRepository();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    /**
     * Workflow: CREATED => PROCESSED|FAILED
     * Note: READY status is not used with admin notifications atm
     */
    public function sendMails(): static {
        try {
            // Calculate notifiability by delay
            $timeLimit = new \DateTime();
            $timeLimit->sub(new \DateInterval(sprintf(
                "PT%dM",
                (int) $this->appConfig->getValue('NOTIFICATION_DEFER_DELAY')
            )));

            $pendingNotifications = $this->geeftlistResourceFactory->resolveMakeCollection(Notification::ENTITY_TYPE)
                ->addFieldToFilter('type', Notification::TYPE_EMAIL)
                ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_ADMIN)
                ->addFieldToFilter('status', Notification::STATUS_CREATED)
                ->addFieldToFilter('created_at', ['lt' => DateTime::getDateSql($timeLimit)]);

            if (count($pendingNotifications) === 0) {
                $this->logger->info('No pending admin notification found.');
                return $this;
            }

            $itemLists = $this->getMailNotificationItems($pendingNotifications->getItems());

            try {
                $email = $this->adminEmailService->getPreparedEmail()
                    ->setTemplate(self::EMAIL_TEMPLATE)
                    ->setData('itemLists', $itemLists);
                $this->senderService->send($email);

                foreach ($itemLists as $itemList) {
                    foreach ($itemList->getItemsNotifications() as $notification) {
                        $notification->setStatus(Notification::STATUS_PROCESSED);
                        $this->notificationRepository->save($notification);
                    }
                }

                $this->logger->debug(sprintf(
                    "New grouped admin notification email sent to %s (%d items)",
                    $email->getTo(),
                    count($itemLists)
                ));
            }
            catch (\Throwable $e) {
                $this->logger->critical(sprintf(
                    'Could not send admin notification emails: %s',
                    $e->getMessage()
                ));
                $this->logger->critical($e);
                foreach ($itemLists as $itemList) {
                    foreach ($itemList->getItemsNotifications() as $notification) {
                        $notification->setStatus(Notification::STATUS_FAILED);
                        $this->notificationRepository->save($notification);
                    }

                    if (isset($notification)) {
                        $notification->setMessage($e->getMessage());
                        $this->notificationRepository->save($notification);
                    }
                }
            }
        }
        catch (\Throwable $throwable) {
            $this->logger->critical('Could not send admin notification emails: ' . $throwable->getMessage());
            $this->logger->critical($throwable);
        }

        return $this;
    }

    /**
     * @param Notification[] $notifications
     * @return ItemList[]
     */
    protected function getMailNotificationItems(array $notifications) {
        $notificationsByTargetType = [];
        foreach ($notifications as $notification) {
            $notificationsByTargetType[$notification->getTargetType()][] = $notification;
        }

        /** @var \Geeftlist\Model\MailNotification\MailData\ItemList[] $itemLists */
        $itemLists = [];
        foreach ($notificationsByTargetType as $targetType => $targetNotifications) {
            /** @var \Geeftlist\Model\MailNotification\MailData\ItemList $itemList */
            $itemList = $this->mailNotificationItemServiceFactory
                ->make(\Geeftlist\Model\MailNotification\MailData\ItemList::class);
            $itemList->setTargetType($targetType);

            $items = $this->mailNotificationItemServiceFactory->resolveGet($targetType)
                ->toItems($targetNotifications);
            if ($items) {
                $itemList->addItems($items);
                $itemLists[$targetType] = $itemList;
            }
        }

        return $itemLists;
    }
}
