<?php

namespace Geeftlist\Observer\Cron\MailNotification\Geefter;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Notification;

abstract class AbstractMailNotification
{
    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $notificationRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefterRepository;

    protected \Geeftlist\Service\MailNotificationInterface $mailNotificationService;

    protected \OliveOil\Core\Service\GenericFactoryInterface $mailNotificationItemServiceFactory;

    protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService;

    protected \Geeftlist\Model\ResourceFactory $geeftlistResourceFactory;

    protected \Geeftlist\Service\Security $securityService;

    protected \Geeftlist\Service\Geefter\Email $geefterEmailService;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(\Geeftlist\Observer\Cron\MailNotification\Context $context) {
        $this->appConfig = $context->getAppConfig();
        $this->notificationRepository = $context->getModelRepositoryFactory()->resolveGet(Notification::ENTITY_TYPE);
        $this->geefterRepository = $context->getModelRepositoryFactory()->resolveGet(Geefter::ENTITY_TYPE);
        $this->mailNotificationService = $context->getMailNotificationService();
        $this->mailNotificationItemServiceFactory = $context->getMailNotificationItemServiceFactory();
        $this->senderService = $context->getSenderService();
        $this->geeftlistResourceFactory = $context->getGeeftlistResourceFactory();
        $this->securityService = $context->getSecurityService();
        $this->geefterEmailService = $context->getGeefterEmailService();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    /**
     * @param int[] $notificationIds
     * @return $this
     */
    protected function updateNotificationStatusFromGeefterStatus(array $notificationIds): static {
        // Once all emails have been sent, reload all pending notifications and check if all geefters have been
        // notified (status <> "pending") for each of them and if so change their status to "processed".
        $pendingNotifications = $this->geeftlistResourceFactory
            ->resolveMakeCollection(Notification::ENTITY_TYPE)
            ->addFieldToFilter('notification_id', ['in' => $notificationIds])
            ->addFieldToFilter('type', Notification::TYPE_EMAIL)
            ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_GEEFTER)
            ->addFieldToFilter('status', Notification::STATUS_PENDING);
        /** @var Notification $pendingNotification */
        foreach ($pendingNotifications as $pendingNotification) {
            $processed = true;
            foreach ($pendingNotification->getAllRecipientStatus() as $status) {
                if ($status == Notification::STATUS_PENDING) {
                    $processed = false;
                    break;
                }
            }

            if ($processed) {
                $pendingNotification->setStatus(Notification::STATUS_PROCESSED);
                $this->notificationRepository->save($pendingNotification);
            }
        }

        return $this;
    }
}
