<?php

namespace Geeftlist\Observer\Cron\MailNotification\Geefter\Discussion;

use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Notification;
use Geeftlist\Observer\Cron\MailNotification\Geefter\AbstractMailNotification;
use OliveOil\Core\Exception\NoSuchEntityException;

class PostMentionNotification extends AbstractMailNotification
{
    public const EMAIL_TEMPLATE = 'notification/discussion/post/mention.phtml';

    protected bool $enabled;

    /**
     * @param int $batchSize
     */
    public function __construct(
        \Geeftlist\Observer\Cron\MailNotification\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $postRepository,
        $enabled = true,
        protected $batchSize = 100
    ) {
        parent::__construct($context);
        $this->enabled = (bool) $enabled;
    }

    public function sendMessageToRecipients(): void {
        if (!$this->enabled) {
            $this->logger->info('Post message sending to recipients is disabled, skipping.');

            return;
        }

        /** @var Notification[] $pendingNotifications */
        $pendingNotifications = $this->notificationRepository->find([
            'filter' => [
                'target_type' => [['eq' => Post::ENTITY_TYPE]],
                'event_name' => [['eq' => Notification\Discussion\Post\Constants::EVENT_NEW_RECIPIENTS]],
                'status' => [['eq' => Notification::STATUS_PENDING]],
            ]
        ]);

        foreach ($pendingNotifications as $pendingNotification) {
            try {
                /** @var Post|null $post */
                $post = $this->postRepository->get($pendingNotification->getTargetId());
                if (!$post) {
                    throw new NoSuchEntityException(sprintf(
                        "Post %d does not exist (anymore).",
                        $pendingNotification->getTargetId()
                    ));
                }

                foreach ($pendingNotification->getAllRecipientStatus() as $geefterId => $status) {
                    if ($status !== Notification::STATUS_PENDING) {
                        continue;
                    }

                    $recipient = $this->geefterRepository->get($geefterId);
                    if (!$recipient) {
                        // Should not happen with FK cascade but oh...
                        $this->logger->warning(sprintf(
                            'Cannot find geefter with ID #%d for notification #%d',
                            $geefterId,
                            $pendingNotification->getId()
                        ));
                        continue;
                    }

                    try {
                        $this->sendMessage($recipient, $post);
                        $pendingNotification->updateRecipientStatus(
                            $recipient,
                            Notification::STATUS_PROCESSED
                        );
                    } catch (\Throwable $e) {
                        $this->logger->error(
                            sprintf(
                                "Could not send discussion post mention notification for entity #%d to geefter #%d: %s",
                                $pendingNotification->getId(),
                                $geefterId,
                                $e->getMessage()
                            ),
                            ['exception' => $e]
                        );
                        $pendingNotification->updateRecipientStatus(
                            $recipient,
                            Notification::STATUS_FAILED
                        );
                    }
                }

                $pendingNotification->setStatus(Notification::STATUS_PROCESSED);
                $this->notificationRepository->save($pendingNotification);
            }
            catch (\Throwable $e) {
                $this->logger->error(
                    sprintf(
                        "Could not send discussion post mention notification for entity #%d: %s",
                        $pendingNotification->getId(),
                        $e->getMessage()
                    ),
                    ['exception' => $e]
                );
                $pendingNotification->setStatus(Notification::STATUS_FAILED);
                $this->notificationRepository->save($pendingNotification);
            }
        }
    }

    protected function sendMessage(Geefter $geefter, Post $post) {
        $topic = $post->getTopic();

        $email = $this->geefterEmailService->getPreparedEmail($geefter)
            ->setTemplate(self::EMAIL_TEMPLATE)
            ->addData([
                'topic_title' => $topic->getTitle(),
                'topic_url' => $topic->getUrl(),
                'recipient' => $geefter,
                'author_name' => $post->getAuthor()->getUsername(),
                'title' => $post->getTitle(),
                'message' => $post->getMessage(),
                'post_date' => $post->getCreatedAt(),
            ])
        ;
        $this->senderService->send($email);

        $this->logger->debug(sprintf(
            "New discussion post mention notification email sent to %s (post #%d)",
            $geefter->getEmail(),
            $post->getId()
        ));
    }
}
