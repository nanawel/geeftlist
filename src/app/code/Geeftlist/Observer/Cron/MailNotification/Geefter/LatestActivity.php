<?php

namespace Geeftlist\Observer\Cron\MailNotification\Geefter;


use Geeftlist\Model\MailNotification\Constants;
use Geeftlist\Model\MailNotification\MailData\ItemList;
use Geeftlist\Model\Notification;
use Geeftlist\Model\ResourceModel\Db\Notification\Collection;
use OliveOil\Core\Helper\DateTime;

/**
 * Handles "latest activity" notifications that can be grouped and scheduled differently
 * according to the "email_notification_freq" setting on each geefter.
 */
class LatestActivity extends AbstractMailNotification
{
    public const EMAIL_TEMPLATE = 'notification/latest.phtml';

    /** @var \OliveOil\Core\Model\Cache\ArrayObjectInterface */
    protected $geefterNotifiableStatus;

    public function __construct(
        \Geeftlist\Observer\Cron\MailNotification\Context $context,
        \OliveOil\Core\Model\Cache\ArrayObjectInterface $geefterNotifiableStatus
    ) {
        parent::__construct($context);
        $this->geefterNotifiableStatus = $geefterNotifiableStatus;
    }

    /**
     * Retrieve pending notifications from the notification_geefter table, assert if geefter
     * is notifiable (= if the latest notification is old enough to allow sending) then send
     * a recap email with all matching notifications to each geefter.
     *
     * Workflow: PENDING
     *             => PROCESSED (default)
     *             => FAILED (in case of error)
     */
    public function sendMails() {
        return $this->securityService->callPrivileged(fn(): static => $this->doSendMails());
    }

    /**
     * @return $this
     */
    protected function doSendMails(): static {
        try {
            $batches = $this->getGeefterNotificationBatches();

            if ($batches === []) {
                $this->logger->info('No pending notification batch found.');
                return $this;
            }

            $processedNotificationIds = [];
            foreach ($batches as $geefterId => $itemLists) {
                try {
                    /** @var \Geeftlist\Model\Geefter $geefter */
                    $geefter = $this->geefterRepository->get($geefterId);

                    if (! $this->canNotify($geefter)) {
                        $this->logger->debug(sprintf(
                            'Deferring sending of notifications for geefter %d',
                            $geefterId
                        ));
                        continue;
                    }

                    $email = $this->geefterEmailService->getPreparedEmail($geefter)
                        ->setTemplate(self::EMAIL_TEMPLATE)
                        ->setData('itemLists', $itemLists);
                    $this->senderService->send($email);

                    foreach ($itemLists as $itemList) {
                        foreach ($itemList->getItemsNotifications() as $notification) {
                            // Set the notification/geefter item as processed
                            $notification->updateRecipientStatus(
                                $this->geefterRepository->get($geefterId),
                                Notification::STATUS_PROCESSED
                            );

                            $processedNotificationIds[] = $notification->getId();
                        }
                    }

                    // Update geefter's last notification date
                    $geefter->setEmailNotificationLast(DateTime::getDateSql());
                    $this->geefterRepository->save($geefter);

                    $this->logger->debug(sprintf(
                        'New grouped notification email sent to %s (%d items)',
                        $geefter->getEmail(),
                        count($itemLists)
                    ));
                }
                catch (\Throwable $e) {
                    $this->logger->critical(sprintf(
                        'Could not send notification emails to geefter #%d: %s',
                        $geefterId,
                        $e->getMessage()
                    ), ['exception' => $e]);
                    foreach ($itemLists as $itemList) {
                        foreach ($itemList->getItemsNotifications() as $notification) {
                            $notification->updateRecipientStatus(
                                $this->geefterRepository->get($geefterId),
                                Notification::STATUS_FAILED
                            );
                        }

                        if (isset($notification)) {
                            $notification->setMessage($e->getMessage());
                            $this->notificationRepository->save($notification);
                        }
                    }
                }
            }

            $this->updateNotificationStatusFromGeefterStatus($processedNotificationIds);
        }
        catch (\Throwable $throwable) {
            $this->logger->critical('Could not send notification emails: ' . $throwable->getMessage());
            $this->logger->critical($throwable);
        }

        return $this;
    }

    /**
     * Return notification item lists by geefter for grouped sending.
     * Only process notifications flagged as "deferrable = true".
     *
     * @return \Geeftlist\Model\MailNotification\MailData\ItemList[][]
     */
    protected function getGeefterNotificationBatches(): array {
        $pendingNotifications = $this->geeftlistResourceFactory->resolveMakeCollection(Notification::ENTITY_TYPE)
            ->addFieldToFilter('type', Notification::TYPE_EMAIL)
            ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_GEEFTER)
            ->addFieldToFilter('deferrable', 1)
            ->addFieldToFilter('status', Notification::STATUS_PENDING);

        /** @var Notification[][] $notificationsByGeefter */
        $notificationsByGeefter = [];
        /** @var Notification $pendingNotification */
        foreach ($pendingNotifications as $pendingNotification) {
            foreach ($pendingNotification->getAllRecipientStatus() as $geefterId => $status) {
                if ($status == Notification::STATUS_PENDING) {
                    if (! array_key_exists($geefterId, $notificationsByGeefter)) {
                        $notificationsByGeefter[$geefterId] = [];
                    }

                    $notificationsByGeefter[$geefterId][] = $pendingNotification;
                }
            }
        }

        $itemsListByGeefter = [];
        foreach ($notificationsByGeefter as $geefterId => $notifications) {
            /** @var \Geeftlist\Model\Geefter $geefter */
            $geefter = $this->geefterRepository->get($geefterId);
            $itemsListByGeefter[$geefterId] = $this->getMailNotificationItems($notifications, $geefter);
        }

        return $itemsListByGeefter;
    }

    /**
     * @param Notification[] $notifications
     * @return ItemList[]
     */
    protected function getMailNotificationItems(array $notifications, \Geeftlist\Model\Geefter $geefter) {
        $notificationsByTargetType = [];
        foreach ($notifications as $notification) {
            $notificationsByTargetType[$notification->getTargetType()][] = $notification;
        }

        /** @var \Geeftlist\Model\MailNotification\MailData\ItemList[] $itemLists */
        $itemLists = [];
        foreach ($notificationsByTargetType as $targetType => $targetNotifications) {
            /** @var \Geeftlist\Model\MailNotification\MailData\ItemList $itemList */
            $itemList = $this->mailNotificationItemServiceFactory
                ->make(\Geeftlist\Model\MailNotification\MailData\ItemList::class);
            $itemList->setTargetType($targetType);

            $items = $this->mailNotificationItemServiceFactory->resolveGet($targetType)
                ->toItems($targetNotifications, $geefter);
            if ($items) {
                $itemList->addItems($items);
                $itemLists[$targetType] = $itemList;
            }
        }

        return $itemLists;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function canNotify(\Geeftlist\Model\Geefter $geefter) {
        if (! $this->canNotifyGeefterByFreq($geefter)) {
            return false;
        }

        if (! isset($this->geefterNotifiableStatus['init'])) {
            // We use an object to keep this class stateless and get an external access to the cache (see UT)
            $this->geefterNotifiableStatus->clear();
            /** @var \Geeftlist\Model\ResourceModel\Db\Notification $notificationResource */
            $notificationResource = $this->geeftlistResourceFactory->resolveGet(Notification::ENTITY_TYPE);

            // Calculate notifiability by delay
            $lastEventTimeLimit = new \DateTime();
            $lastEventTimeLimit->sub(new \DateInterval(sprintf(
                "PT%dM",
                (int) $this->appConfig->getValue('NOTIFICATION_DEFER_DELAY')
            )));
            if (! $lastEventTimeLimit) {
                return false;
            }

            $lastEventTimeByGeefter = $notificationResource->getLastEventTimeByRecipient(
                Notification::RECIPIENT_TYPE_GEEFTER,
                Notification::STATUS_PENDING,
                null,
                $lastEventTimeLimit
            );
            foreach ($lastEventTimeByGeefter as $geefterId => $lastEventTime) {
                $this->geefterNotifiableStatus[$geefterId] = true;
            }

            // Combine notifiability with max pending event count
            $maxPendingEventCount = (int) $this->appConfig->getValue('NOTIFICATION_DEFER_MAX_COUNT');
            $pendingEventCountByGeefter = $notificationResource->getEventCountByRecipient(
                Notification::RECIPIENT_TYPE_GEEFTER,
                Notification::STATUS_PENDING
            );
            foreach ($pendingEventCountByGeefter as $geefterId => $pendingEventCount) {
                $this->geefterNotifiableStatus[$geefterId] = (
                    $this->geefterNotifiableStatus->offsetExists($geefterId)
                        ? $this->geefterNotifiableStatus[$geefterId]
                        : false
                    )
                    || $pendingEventCount >= $maxPendingEventCount;
            }

            $this->geefterNotifiableStatus['init'] = true;
        }

        return $this->geefterNotifiableStatus[$geefter->getId()] ?? false;
    }

    /**
     * @throws \Exception
     */
    public function canNotifyGeefterByFreq(\Geeftlist\Model\Geefter $geefter): bool {
        $timeLimit = new \DateTime();
        switch ($geefter->getEmailNotificationFreq()) {
            case Constants::SENDING_FREQ_DAILY:
                $timeLimit->sub(new \DateInterval("P1D"));
                break;

            case Constants::SENDING_FREQ_WEEKLY:
                $timeLimit->sub(new \DateInterval("P1W"));
                break;

            case Constants::SENDING_FREQ_DISABLED:
                $timeLimit = false;
                break;

            case Constants::SENDING_FREQ_REALTIME:
            default:
                return true;
        }

        $lastNotificationDate = $geefter->getEmailNotificationLast() ?: '@0';

        return $timeLimit
            && DateTime::getDate($lastNotificationDate) < $timeLimit;
    }

    /**
     * Update partially or totally failed notifications according to their geefter recipients statuses.
     *
     * Workflow: PENDING
     *             => PENDING (= no change, if at least one geefter recipient is still PENDING
     *             => PROCESSED (if at least one PROCESSED geefter recipient is found)
     *             => FAILED (if all geefter recipients failed)
     *
     * @return $this
     */
    public function cleanupPendingFailedNotifications(): static {
        /** @var Collection $pendingNotifications */
        $pendingNotifications = $this->geeftlistResourceFactory
            ->resolveMakeCollection(Notification::ENTITY_TYPE)
            ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_GEEFTER)
            ->addFieldToFilter('status', Notification::STATUS_PENDING)
            ->addFieldToFilter('deferrable', 1);

        /** @var Notification $pendingNotification */
        foreach ($pendingNotifications as $pendingNotification) {
            $statuses = [];
            foreach ($pendingNotification->getAllRecipientStatus() as $recipientStatus) {
                if (!array_key_exists($recipientStatus, $statuses)) {
                    $statuses[$recipientStatus] = 0;
                }

                ++$statuses[$recipientStatus];
            }

            if (isset($statuses[Notification::STATUS_PENDING]) && $statuses[Notification::STATUS_PENDING] !== 0) {
                continue;
            }

            if (isset($statuses[Notification::STATUS_PROCESSED]) && $statuses[Notification::STATUS_PROCESSED] !== 0) {
                $pendingNotification->setStatus(Notification::STATUS_PROCESSED);
                $this->logger->debug(sprintf(
                    'Pending notification #%d %s::%s: has been (partially?) processed, marking it as such.',
                    $pendingNotification->getId(),
                    $pendingNotification->getEntityType(),
                    $pendingNotification->getEventName()
                ));
            }
            elseif (isset($statuses[Notification::STATUS_FAILED]) && $statuses[Notification::STATUS_FAILED] !== 0) {
                $pendingNotification->setStatus(Notification::STATUS_FAILED);
                $this->logger->debug(sprintf(
                    'Pending notification #%d %s::%s: has entirely failed, marking it as such.',
                    $pendingNotification->getId(),
                    $pendingNotification->getEntityType(),
                    $pendingNotification->getEventName()
                ));
            }
            else {
                $this->logger->notice(sprintf(
                    'Unexpected state for notification #%d %s::%s: no geefter recipient found. Deleting.',
                    $pendingNotification->getId(),
                    $pendingNotification->getEntityType(),
                    $pendingNotification->getEventName()
                ));
                $this->notificationRepository->delete($pendingNotification);
                continue;
            }

            $this->notificationRepository->save($pendingNotification);
        }

        return $this;
    }

    /**
     * Remove pending notifications for geefters with email_notification_freq = disabled
     *
     * @return $this
     */
    public function cleanupDisabledNotifications(): static {
        // First, retrieve all geefter IDs with pending notifications
        /** @var Collection $pendingNotificationGeefters */
        $pendingNotificationGeefters = $this->geeftlistResourceFactory
            ->resolveMakeCollection(Notification::ENTITY_TYPE);
        $pendingNotificationGeefterIds = $pendingNotificationGeefters
            ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_GEEFTER)
            ->addFieldToFilter('status', Notification::STATUS_PENDING)
            ->addFieldToFilter('deferrable', 1)
            ->getTargetIds();

        if ($pendingNotificationGeefterIds) {
            // Then, get only geefters using email_notification_freq = disabled
            $geefterIds = $this->geeftlistResourceFactory
                ->resolveMakeCollection(\Geeftlist\Model\Geefter::ENTITY_TYPE)
                ->addFieldToFilter('geefter_id', ['in' => $pendingNotificationGeefterIds])
                ->addFieldToFilter('email_notification_freq', Constants::SENDING_FREQ_DISABLED)
                ->getAllIds();

            // Finally, retrieve notifications using the previous geefter IDs
            $invalidPendingNotifications = $this->geeftlistResourceFactory
                ->resolveMakeCollection(Notification::ENTITY_TYPE)
                ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_GEEFTER)
                ->addFieldToFilter('status', Notification::STATUS_PENDING)
                ->addFieldToFilter('target_id', ['in' => $geefterIds]);

            if ($cnt = $invalidPendingNotifications->count()) {
                $this->logger->debug(sprintf(
                    'Cleaning up %d pending notifications for geefters with frequency set to disabled...',
                    $cnt
                ));
                $invalidPendingNotifications->delete();
                $this->logger->debug(sprintf('%d notifications have been deleted.', $cnt));
            }
        }

        return $this;
    }
}
