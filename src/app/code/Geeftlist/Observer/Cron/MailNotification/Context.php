<?php

namespace Geeftlist\Observer\Cron\MailNotification;

/**
 * FIXME Should probably not extend \Geeftlist\Observer\Context
 */
class Context extends \Geeftlist\Observer\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Model\ResourceModel\Db\Connection $connection,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Session\Manager $sessionManager,
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        \Geeftlist\Model\ModelFactory $geeftlistModelFactory,
        \Geeftlist\Model\ResourceFactory $geeftlistResourceFactory,
        \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory,
        \Geeftlist\Service\Security $securityService,
        \OliveOil\Core\Service\Log $logService,
        \Geeftlist\Service\Permission $permissionService,
        protected \Geeftlist\Service\MailNotificationInterface $mailNotificationService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $mailNotificationItemServiceFactory,
        protected \OliveOil\Core\Model\RepositoryInterface $notificationRepository,
        protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService
    ) {
        parent::__construct(
            $fw,
            $appConfig,
            $connection,
            $urlBuilder,
            $i18n,
            $eventService,
            $sessionManager,
            $coreFactory,
            $geefterSession,
            $geeftlistModelFactory,
            $geeftlistResourceFactory,
            $modelRepositoryFactory,
            $securityService,
            $logService,
            $permissionService
        );
    }

    public function getMailNotificationService(): \Geeftlist\Service\MailNotificationInterface {
        return $this->mailNotificationService;
    }

    public function getMailNotificationItemServiceFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->mailNotificationItemServiceFactory;
    }

    public function getNotificationRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->notificationRepository;
    }

    public function getSenderService(): \OliveOil\Core\Service\Email\Sender\SenderInterface {
        return $this->senderService;
    }

    public function getGeefterEmailService(): \Geeftlist\Service\Geefter\Email {
        return $this->geefterEmailService;
    }
}
