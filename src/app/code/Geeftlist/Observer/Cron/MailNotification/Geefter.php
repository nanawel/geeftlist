<?php

namespace Geeftlist\Observer\Cron\MailNotification;


use Geeftlist\Model\Notification;

/**
 * Status Workflow: CREATED => READY => PENDING [=> FAILED]
 */
class Geefter
{
    protected \Geeftlist\Service\Security $securityService;

    protected \Geeftlist\Service\MailNotificationInterface $mailNotificationService;

    protected \Geeftlist\Model\ResourceFactory $geeftlistResourceFactory;

    protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory;

    protected \OliveOil\Core\Model\RepositoryInterface $notificationRepository;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        \Geeftlist\Observer\Cron\MailNotification\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService
    ) {
        $this->securityService = $context->getSecurityService();
        $this->mailNotificationService = $context->getMailNotificationService();
        $this->geeftlistResourceFactory = $context->getGeeftlistResourceFactory();
        $this->modelRepositoryFactory = $context->getModelRepositoryFactory();
        $this->notificationRepository = $context->getNotificationRepository();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    /**
     * Prepare notifications for sending (used to merge notifications if needed).
     *
     * Workflow: CREATED => READY
     */
    public function prepareNotifications(): static {
        $this->securityService->callPrivileged(function (): void {
            $this->mailNotificationService->prepareNotifications(Notification::RECIPIENT_TYPE_GEEFTER);
        });

        return $this;
    }

    public function prepareMailRecipients() {
        return $this->securityService->callPrivileged(fn(): static => $this->doPrepareMailRecipients());
    }

    /**
     * Retrieve all "ready" notifications and populate notification_geefter table with the
     * recipients for each one. Does not send mail directly.
     *
     * Workflow: READY
     *             => PENDING (default)
     *             => PROCESSED (if no geefter recipient is found)
     *             => FAILED (in case of error)
     *
     * @see \Geeftlist\Observer\Cron\MailNotification\Geefter\LatestActivity::sendMails()
     */
    protected function doPrepareMailRecipients(): static {
        $notifications = $this->geeftlistResourceFactory->resolveMakeCollection(Notification::ENTITY_TYPE)
            ->addFieldToFilter('type', Notification::TYPE_EMAIL)
            ->addFieldToFilter('recipient_type', Notification::RECIPIENT_TYPE_GEEFTER)
            ->addFieldToFilter('status', Notification::STATUS_READY);

        if (! $notifications->count()) {
            $this->logger->info('No pending geefter notification found.');
        }
        else {
            /** @var Notification $notification */
            foreach ($notifications as $notification) {
                try {
                    $target = $this->modelRepositoryFactory->resolveGet($notification->getTargetType())
                        ->get($notification->getTargetId());
                    if (!$target) {
                        // Not an error (in the case of a deletion for example)
                        // but we need to have a valid object when triggering the event next
                        $target = $this->modelRepositoryFactory->resolveGet($notification->getTargetType())
                            ->newModel();
                    }

                    $recipients = $this->mailNotificationService->getNotifiableGeefters(
                        $notification->getEventName(),
                        $target,
                        $notification->getEventParams()
                    );
                    if (!$recipients || !$recipients->count()) {
                        $this->logger->debug(sprintf(
                            'No geefters returned for notification #%d %s::%s, marking as processed.',
                            $notification->getId(),
                            $notification->getTargetType(),
                            $notification->getEventName()
                        ));
                        $notification->setStatus(Notification::STATUS_PROCESSED);
                    } else {
                        $notification->addRecipient($recipients->getItems(), Notification::STATUS_PENDING)
                            ->setStatus(Notification::STATUS_PENDING);
                    }

                    $this->notificationRepository->save($notification);
                }
                catch (\Throwable $e) {
                    $this->logger->critical('Could not send notification emails: ' . $e->getMessage());
                    $this->logger->critical($e);
                    $notification->setStatus(Notification::STATUS_FAILED)
                        ->setMessage($e->getMessage());
                    $this->notificationRepository->save($notification);
                }
            }
        }

        return $this;
    }
}
