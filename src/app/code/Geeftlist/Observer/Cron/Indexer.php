<?php

namespace Geeftlist\Observer\Cron;


use Laminas\EventManager\EventInterface;

class Indexer extends \Geeftlist\Observer\AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        \OliveOil\Core\Service\Log $logService,
        /** @var \Geeftlist\Indexer\IndexerInterface[] */
        protected array $indexers
    ) {
        parent::__construct($context);
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function reindexAll(EventInterface $ev): static {
        foreach ($this->indexers as $indexer) {
            $indexer->reindexAll();
        }

        return $this;
    }

    public function reindex(EventInterface $ev): static {
        if (!$indexerCode = $ev->getParam('indexer')) {
            $this->logger->error('Missing indexer code. Ignoring.');
            return $this;
        }

        // Translate URL-friendly indexer code if needed
        $indexerCode = strtr($indexerCode, ['_' => '/']);

        if (!isset($this->indexers[$indexerCode])) {
            $this->logger->error(sprintf('Invalid indexer code specified: %s. Ignoring.', $indexerCode));
            return $this;
        }

        $this->indexers[$indexerCode]->reindexAll();

        return $this;
    }
}
