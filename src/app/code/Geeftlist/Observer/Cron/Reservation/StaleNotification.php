<?php

namespace Geeftlist\Observer\Cron\Reservation;

use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use OliveOil\Core\Helper\DateTime;

class StaleNotification
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    protected bool $enabled;

    /**
     * @param int $staleReservationDelay
     * @param int $stalePurchaseDelay
     * @param int $batchSize
     */
    public function __construct(
        protected \OliveOil\Core\Model\RepositoryInterface $reservationRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $giftRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService,
        protected \Geeftlist\Service\Security $securityService,
        \OliveOil\Core\Service\Log $logService,
        $enabled = true,
        protected $staleReservationDelay = 24 * 45,       // 45 days
        protected $stalePurchaseDelay = 24 * 90,          // 90 days
        protected $batchSize = 100
    ) {
        $this->logger = $logService->getLoggerForClass($this);
        $this->enabled = (bool) $enabled;
    }

    public function sendStaleReservationNotifications(): void {
        if (!$this->enabled) {
            $this->logger->info('Stale reservation notifications are disabled, skip sending.');
            ;

            return;
        }

        $timeLimit = time() - ($this->staleReservationDelay * 60 * 60);

        $notifiableReservations = $this->retrieveNotifiableReservations(Reservation::TYPE_RESERVATION, $timeLimit);
        foreach ($notifiableReservations as $notifiableReservation) {
            $this->logger->debug(sprintf(
                'Stale reservation #%d found for gift #%d from geefter #%d, created at %s.',
                $notifiableReservation->getId(),
                $notifiableReservation->getGiftId(),
                $notifiableReservation->getGeefterId(),
                $notifiableReservation->getReservedAt()
            ));

            try {
                // Flag reservation BEFORE sending notification, to prevent notification loop
                // in case something goes wrong.
                $notifiableReservation->setStaleNotificationSentAt(DateTime::getDateSql());

                $this->securityService->callPrivileged(function () use ($notifiableReservation): void {
                    $this->reservationRepository->save($notifiableReservation);
                });

                $this->notifyStaleReservation($notifiableReservation);
            }
            catch (\Throwable $e) {
                $this->logger->error(
                    sprintf(
                        "Could not send stale reservation notification for entity #%d: %s",
                        $notifiableReservation->getId(),
                        $e->getMessage()
                    ),
                    ['exception' => $e]
                );
            }
        }
    }

    protected function notifyStaleReservation(Reservation $reservation) {
        /** @var Gift $gift */
        $gift = $this->giftRepository->get($reservation->getGiftId());
        /** @var Geefter $geefter */
        $geefter = $this->geefterRepository->get($reservation->getGeefterId());

        if (!$geefter->getStaleReservationNotificationEnabled()) {
            return;
        }

        $i18n = $geefter->getI18n();

        $geefteeNames = [];
        foreach ($gift->getGeefteeCollection() as $giftGeeftee) {
            $geefteeNames[] = $giftGeeftee->getName();
        }

        $email = $this->geefterEmailService->getPreparedEmail($geefter)
            ->setTemplate('notification/reservation/stale-reservation.phtml')
            ->addData([
                'gift_url' => $gift->getViewUrl(),
                'gift_label' => $gift->getLabel(),
                'geeftee_names' => $geefteeNames,
                'reservation_date' => $i18n->date($reservation->getReservedAt()),
                'reservation_url' => $gift->getReserveUrl(),
            ])
        ;
        $this->senderService->send($email);

        $this->logger->debug(
            sprintf('New stale reservations notification email sent to %s (gift #%s)', $geefter->getEmail(), $gift->getId())
        );
    }

    public function sendStalePurchaseNotifications(): void {
        if (!$this->enabled) {
            $this->logger->info('Stale purchase notifications are disabled, skip sending.');
            ;

            return;
        }

        $timeLimit = time() - ($this->stalePurchaseDelay * 60 * 60);

        $notifiableReservations = $this->retrieveNotifiableReservations(Reservation::TYPE_PURCHASE, $timeLimit);
        foreach ($notifiableReservations as $notifiableReservation) {
            $this->logger->debug(sprintf(
                'Stale purchase #%d found for gift #%d from geefter #%d, created at %s.',
                $notifiableReservation->getId(),
                $notifiableReservation->getGiftId(),
                $notifiableReservation->getGeefterId(),
                $notifiableReservation->getReservedAt()
            ));

            try {
                // Flag reservation BEFORE sending notification, to prevent notification loop
                // in case something goes wrong.
                $notifiableReservation->setStaleNotificationSentAt(DateTime::getDateSql());
                $this->securityService->callPrivileged(function () use ($notifiableReservation): void {
                    $this->reservationRepository->save($notifiableReservation);
                });

                $this->notifyStalePurchase($notifiableReservation);
            }
            catch (\Throwable $e) {
                $this->logger->error(
                    sprintf(
                        "Could not send stale purchase notification for entity #%d: %s",
                        $notifiableReservation->getId(),
                        $e->getMessage()
                    ),
                    ['exception' => $e]
                );
            }
        }
    }

    protected function notifyStalePurchase(Reservation $reservation) {
        /** @var Gift $gift */
        $gift = $this->giftRepository->get($reservation->getGiftId());
        /** @var Geefter $geefter */
        $geefter = $this->geefterRepository->get($reservation->getGeefterId());

        if (!$geefter->getStaleReservationNotificationEnabled()) {
            return;
        }

        $i18n = $geefter->getI18n();

        $geefteeNames = [];
        foreach ($gift->getGeefteeCollection() as $giftGeeftee) {
            $geefteeNames[] = $giftGeeftee->getName();
        }

        $email = $this->geefterEmailService->getPreparedEmail($geefter)
            ->setTemplate('notification/reservation/stale-purchase.phtml')
            ->addData([
                'gift_url' => $gift->getViewUrl(),
                'gift_label' => $gift->getLabel(),
                'geeftee_names' => $geefteeNames,
                'reservation_date' => $i18n->date($reservation->getReservedAt()),
                'reservation_url' => $gift->getReserveUrl(),
                'request_archiving_url' => $gift->getConfirmArchivingRequestUrl(),
            ])
        ;
        $this->senderService->send($email);

        $this->logger->debug(
            sprintf('New stale purchase notification email sent to %s (gift #%s)', $geefter->getEmail(), $gift->getId())
        );
    }

    /**
     * @param string $type
     * @param int $timeLimit
     * @return Reservation[]
     */
    protected function retrieveNotifiableReservations($type, $timeLimit) {
        /** @var Reservation[] $notifiableReservations */
        $notifiableReservations = $this->reservationRepository->find([
            'join' => ['gift' => null],
            'filter' => [
                'main_table.type' => [['eq' => $type]],
                'main_table.reserved_at' => [['lt' => DateTime::getDateSql($timeLimit)]],
                'gift.status' => [['eq' => Gift\Field\Status::AVAILABLE]],
                'main_table.stale_notification_sent_at' => [['null' => true]],
            ],
            'limit' => $this->batchSize
        ]);

        // Also add reservations already notified N-times the delay
        if (count($notifiableReservations) < $this->batchSize) {
            /** @var Reservation[] $recurrentNotifiableReservations */
            $recurrentNotifiableReservations = $this->reservationRepository->find([
                'join' => ['gift' => null],
                'filter' => [
                    'main_table.type' => [['eq' => $type]],
                    'main_table.reserved_at' => [['lt' => DateTime::getDateSql($timeLimit)]],
                    'gift.status' => [['eq' => Gift\Field\Status::AVAILABLE]],
                    'main_table.stale_notification_sent_at' => [['lt' => DateTime::getDateSql($timeLimit)]],
                ],
                'limit' => $this->batchSize - count($notifiableReservations)
            ]);
            $notifiableReservations = array_merge($notifiableReservations, $recurrentNotifiableReservations);
        }

        return $notifiableReservations;
    }
}
