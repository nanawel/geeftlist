<?php

namespace Geeftlist\Observer\Cron;


use OliveOil\Core\Helper\DateTime;

class ActivityLog extends \Geeftlist\Observer\AbstractObserver
{
    /**
     * Delete old activity log items.
     */
    public function cleanup(): static {
        $timeLimit = new \DateTime();
        $timeLimit->sub(new \DateInterval(sprintf(
            "P%dD",
            (int) $this->appConfig->getValue('ACTIVITYLOG_HISTORY_KEEP')
        )));
        $activityLogs = $this->geeftlistResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\ActivityLog::ENTITY_TYPE)
            ->addFieldToFilter('logged_at', ['lt' => DateTime::getDateSql($timeLimit)]);

        if ($cnt = $activityLogs->count()) {
            $this->logger->debug(sprintf('Cleaning up %d old activity log items...', $cnt));
            $activityLogs->delete();
            $this->logger->debug(sprintf('%d old activity log items have been deleted.', $cnt));
        }

        return $this;
    }
}
