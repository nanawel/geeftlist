<?php

namespace Geeftlist\Observer\Cron\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Gift\Collection;
use Symfony\Component\Finder\Finder;

class Image
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /**
     * @param string $imageBasePath
     * @param int $batchSize
     * @param int $mTimeDelta
     */
    public function __construct(
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection,
        \OliveOil\Core\Service\Log $logService,
        protected \Geeftlist\Service\Gift\Image $giftImageService,
        protected $imageBasePath,
        protected $batchSize = 100,
        protected $mTimeDelta = 60 * 60 * 24
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function deleteOrphanFiles(): void {
        clearstatcache();
        $deletedFiles = 0;

        $bn = 1;
        foreach ($this->getFileBatch() as $batch) {
            $this->logger->debug(sprintf('Processing orphan files batch #%d...', $bn++));
            $deletedFiles += $this->processBatch($batch);
            $this->logger->debug(sprintf('Orphan files batch #%d processed successfully.', $bn++));
        }

        $this->logger->info(sprintf('%d orphan files have been deleted.', $deletedFiles));

        $this->deleteEmptyDirectories();
    }

    public function deleteEmptyDirectories(): void {
        $deletedDirs = 0;

        $bn = 1;
        foreach ($this->getDirBatch() as $batch) {
            $this->logger->debug(sprintf('Processing empty directories batch #%d...', $bn++));

            foreach ($batch as $directory) {
                if ($this->deleteEmptyDirectoryAndParents($directory)) {
                    ++$deletedDirs;
                }
            }

            $this->logger->debug(sprintf('Empty directories batch #%d processed successfully.', $bn++));
        }

        $this->logger->info(sprintf('%d empty directories have been deleted.', $deletedDirs));
    }

    protected function getFileBatch() {
        $n = 0;
        $currentBatch = [];
        foreach ($this->getFileFinder() as $file) {
            $currentBatch[] = $file;

            if (++$n >= $this->batchSize) {
                yield $currentBatch;
            }
        }

        yield $currentBatch;
    }

    protected function getDirBatch() {
        $n = 0;
        $currentBatch = [];
        foreach ($this->getDirFinder() as $file) {
            $currentBatch[] = $file;

            if (++$n >= $this->batchSize) {
                yield $currentBatch;
            }
        }

        yield $currentBatch;
    }

    /**
     * @return Finder
     */
    protected function getFileFinder() {
        $referenceTime = time() - $this->mTimeDelta;

        return Finder::create()
            ->filter(static fn(\SplFileInfo $file): bool => $file->getMTime() < $referenceTime)
            ->in($this->imageBasePath)
            ->files();
    }

    /**
     * @return Finder
     */
    protected function getDirFinder() {
        return Finder::create()
            ->filter(fn(\SplFileInfo $file): bool => $this->isDirEmpty($file))
            ->in($this->imageBasePath)
            ->directories();
    }

    /**
     * @param \SplFileInfo[] $files
     */
    protected function processBatch(array $files): int {
        $deletedFiles = 0;
        $filesByImageId = [];
        foreach ($files as $file) {
            $pathinfo = pathinfo($file->getPathname());
            if ($pathinfo['extension'] == 'lock') {
                if ($this->deleteFile($file)) {
                    ++$deletedFiles;
                }
            }
            elseif ($imageId = $this->giftImageService->getImageIdFromFile($file->getBasename())) {
                $filesByImageId[$imageId][] = $file;
            }
            else {
                $this->logger->warning('Unknown file skipped: ' . $file->getPathname());
            }
        }

        if ($filesByImageId !== []) {
            $orphanImageIds = $this->filterOrphanImageIds(array_keys($filesByImageId));
            /** @var \SplFileInfo[][] $orphanFilesByImageId */
            $orphanFilesByImageId = \OliveOil\array_mask($filesByImageId, $orphanImageIds);

            if ($orphanFilesByImageId) {
                $this->logger->debug(sprintf('Found %d orphan image IDs.', count($orphanFilesByImageId)));

                foreach ($orphanFilesByImageId as $imageId => $orphanFiles) {
                    foreach ($orphanFiles as $orphanFile) {
                        if ($this->deleteFile($orphanFile, $imageId)) {
                            ++$deletedFiles;
                        }
                    }
                }
            }
            else {
                $this->logger->debug('No orphan image IDs found.');
            }
        }

        return $deletedFiles;
    }

    /**
     * @param string[] $imageIds
     * @return string[]
     * @throws \Throwable
     */
    protected function filterOrphanImageIds(array $imageIds): array {
        $giftCollection = $this->getGiftCollection();
        $select = $giftCollection
            ->addFieldToFilter('image_id', ['in' => $imageIds])
            ->getSelect();

        return array_diff(
            $imageIds,
            $this->connection->fetchCol($this->connection->query($select), 'image_id')
        );
    }

    /**
     * @return Collection
     */
    protected function getGiftCollection() {
        static $giftCollection;
        if (!$giftCollection) {
            $giftCollection = $this->resourceModelFactory->resolveMakeCollection(Gift::ENTITY_TYPE);
        }

        return $giftCollection->clear();
    }

    /**
     * @param string|null $imageId
     */
    protected function deleteFile(\SplFileInfo $file, $imageId = null): bool {
        if ($file->isFile()) {
            $this->logger->info(sprintf(
                'Deleting file %s (mtime=%s, imageId=%s)',
                $file->getPathname(),
                (new \DateTime('@' . $file->getMTime()))->format('c'),
                $imageId ?: '<none>'
            ));

            try {
                return unlink($file->getPathname());
            }
            catch (\Throwable $e) {
                $this->logger->error(sprintf(
                    'Could not delete %s: %s',
                    $file->getPathname(),
                    $e->getMessage()
                ));
            }
        }

        return false;
    }

    protected function deleteEmptyDirectoryAndParents(\SplFileInfo $file): bool {
        if (!$file->isDir()) {
            $file = $file->getPathInfo();
        }

        try {
            do {
                if ($this->isDirEmpty($file)) {
                    rmdir($file->getPathname());
                }

                $file = $file->getPathInfo();
            } while ($file->getRealPath() != realpath($this->imageBasePath));
        }
        catch (\Throwable $throwable) {
            $this->logger->error(sprintf(
                'Could not delete %s: %s',
                $file->getPathname(),
                $throwable->getMessage()
            ));

            return false;
        }

        return true;
    }

    protected function isDirEmpty(\SplFileInfo $file): bool {
        return !(new \FilesystemIterator($file))->valid();
    }
}
