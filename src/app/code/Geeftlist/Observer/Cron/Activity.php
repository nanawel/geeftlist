<?php

namespace Geeftlist\Observer\Cron;


use OliveOil\Core\Helper\DateTime;

class Activity extends \Geeftlist\Observer\AbstractObserver
{
    /**
     * Delete old activity items.
     */
    public function cleanup(): static {
        $timeLimit = new \DateTime();
        $timeLimit->sub(new \DateInterval(sprintf(
            "P%dD",
            (int) $this->appConfig->getValue('ACTIVITY_HISTORY_KEEP')
        )));
        $activities = $this->geeftlistResourceFactory
            ->resolveMakeCollection(\Geeftlist\Model\Activity::ENTITY_TYPE)
            ->addFieldToFilter('created_at', ['lt' => DateTime::getDateSql($timeLimit)]);

        if ($cnt = $activities->count()) {
            $this->logger->debug(sprintf('Cleaning up %d old activity items...', $cnt));
            $activities->delete();
            $this->logger->debug(sprintf('%d old activity items have been deleted.', $cnt));
        }

        return $this;
    }
}
