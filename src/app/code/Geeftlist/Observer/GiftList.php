<?php

namespace Geeftlist\Observer;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\GiftList\Field\Status;
use Geeftlist\Model\GiftList\Field\Visibility;
use Geeftlist\Model\Share\GiftList\RuleType\TypeInterface;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class GiftList extends AbstractObserver
{
    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected \Geeftlist\Model\GiftList\RestrictionsAppenderInterface $giftListRestrictionsAppender,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftHelper
    ) {
        $this->coreFactory = $context->getCoreFactory();
        parent::__construct($context);
    }

    public function beforeSaveSetOwner(Event $ev): void {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $ev->getTarget();
        if (
            ! $giftList->getFlag('skip_autoset_owner') && ($currentGeefter = $this->geefterSession->getGeefter()) && ! $giftList->getOwnerId()
        ) {
            $giftList->setOwnerId($currentGeefter->getId());
        }
    }

    /**
     * Set default values if missing
     */
    public function setDefaultValues(Event $ev): void {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $ev->getTarget();

        if (!$giftList->getVisibility()) {
            $giftList->setVisibility(Visibility::PRIVATE);
        }

        if (!$giftList->getStatus()) {
            $giftList->setStatus(Status::ACTIVE);
        }
    }

    public function saveGiftLists(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getParam('model');

        if (is_array($giftListIds = $gift->getGiftlistIds())) {
            $giftLists = $this->modelRepositoryFactory->resolveGet(\Geeftlist\Model\GiftList::ENTITY_TYPE)
                ->find([
                    'filter' => [
                        RepositoryInterface::WILDCARD_ID_FIELD => [['in' => $giftListIds]]
                    ]
                ])
            ;
            $this->giftListGiftHelper->replaceLinks([$gift], $giftLists);
        }
    }

    public function assertCanView(Event $ev): void {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $ev->getTarget();

        if (
            !$this->permissionService->isAllowed($giftList, TypeInterface::ACTION_VIEW)
        ) {
            throw new PermissionException('Cannot view this list.');
        }
    }

    public function assertCanSave(Event $ev): void {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $ev->getTarget();

        if (!$this->permissionService->isAllowed($giftList, TypeInterface::ACTION_EDIT)) {
            throw new PermissionException('Cannot modify this list.');
        }
    }

    public function assertCanDelete(Event $ev): void {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $ev->getTarget();

        if (!$this->permissionService->isAllowed($giftList, TypeInterface::ACTION_DELETE)) {
            throw new PermissionException('Cannot delete this list.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function addRemoveGiftsPermissionCheck(EventInterface $ev): void {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $ev->getTarget();

        if (!$this->permissionService->isAllowed($giftList, TypeInterface::ACTION_ADD_REMOVE_GIFT)) {
            throw new PermissionException('Cannot add nor remove gifts from/to this list.');
        }
    }

    /**
     * Exclude lists according to current geeftee.
     */
    public function addGeefterAccessRestrictionsToCollection(EventInterface $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\GiftList\Collection $collection */
            $collection = $ev->getTarget();

            $this->giftListRestrictionsAppender
                ->addGeefterAccessRestrictions(
                    $collection,
                    $currentGeefter,
                    $collection->getFlag('required_actions') ?: TypeInterface::ACTION_VIEW
                );
        }
    }

    public function validateModel(Event $ev): void {
        /** @var \Geeftlist\Model\GiftList $model */
        $model = $ev->getTarget();

        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $constraints = [
            'name'       => [ErrorInterface::TYPE_FIELD_EMPTY],
            'visibility' => [static fn($value): bool => empty($value) || in_array($value, \Geeftlist\Model\GiftList\Field\Visibility::VALUES)],
            'status' => [static fn($value): bool => empty($value) || in_array($value, \Geeftlist\Model\GiftList\Field\Status::VALUES)],
            'owner_id' => [function ($value) use ($model) {
                if (
                    ! $model->getFlag('allow_update_owner')
                    && $model->getOrigData('owner_id')
                    && $model->getOrigData('owner_id') != $value
                ) {
                    return $this->coreFactory->make(ErrorInterface::class)
                        ->setType(ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE)
                        ->setLocalizableMessage('Cannot update this field.')
                        ->setMessage('Cannot update this field.');
                }
            }]
        ];

        $this->modelValidator->validate($model, $constraints, $errorAggregator);
    }
}
