<?php

namespace Geeftlist\Observer\Controller\Http;

use Geeftlist\Controller\Http\AbstractController;
use Geeftlist\Observer\AbstractObserver;
use Laminas\EventManager\EventInterface;

class LoggedInGeefter extends AbstractObserver
{
    public function addNoFamilyBanner(EventInterface $ev): void {
        /** @var AbstractController $controller */
        $controller = $ev->getParam('controller');

        if (
            $this->geefterSession->isLoggedIn()
            && !$this->geefterSession->getFamilyIds()
            && !str_starts_with($controller->getFullActionName(), 'family')
        ) {
            $this->geefterSession->addInfoMessage(
                "You don't have any family yet! You may wish to <a href=\"{0}\">create</a> or "
                . '<a href="{1}">join</a> one now. <a href="{2}">Need help?</a>',
                [
                    $this->urlBuilder->getUrl('family_manage/new'),
                    $this->urlBuilder->getUrl('family_search'),
                    $this->urlBuilder->getUrl('help', ['_fragment' => 'definition-family']),
                ],
                ['no_escape' => true]
            );
        }
    }
}
