<?php

namespace Geeftlist\Observer\Controller\Http;

use Geeftlist\Observer\AbstractObserver;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Block\HtmlRootContainer;

class XmasLogo extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\View $view,
        protected \OliveOil\Core\Service\DesignInterface $designService
    ) {
        parent::__construct($context);
    }

    public function addXmasHatLogo(EventInterface $ev): void {
        $isXmasPeriod = in_array(date('n'), [11, 12, 1]);

        // Must check block's type (regression after #275)
        if ($isXmasPeriod && ($head = $this->view->getLayout()->getBlock('head')) instanceof HtmlRootContainer) {
            $head->addInlineCss(<<<"EOCSS"
.top-bar-title span::after {
    display: inline-block;
    position: relative;
    content: url({$this->designService->getImageUrl('xmas-hat-logo.png')});
    z-index: 100;
    top: 10px;
    margin-left: -16px;
    margin-top: -10px;
}
EOCSS
            );
        }
    }
}
