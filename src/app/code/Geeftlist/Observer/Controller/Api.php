<?php

namespace Geeftlist\Observer\Controller;


use Geeftlist\Observer\AbstractObserver;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Block\HtmlRootContainer;
use OliveOil\Core\Exception\InvalidConfigurationException;
use OliveOil\Core\Model\View\LayoutInterface;

class Api extends AbstractObserver
{
    protected string $authorizationHeader;

    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Model\Api\Config $apiConfig,
        protected \Geeftlist\Service\Url\Builder\Api $apiUrlBuilder,
        protected \Geeftlist\Service\Authentication\Geefter\JwtMethod $jwtService
    ) {
        parent::__construct($context);

        $this->authorizationHeader = $this->apiConfig->getValue('AUTHORIZATION_HEADER', 'Authorization');
        if (!preg_match('/^[\w-]+$/', $this->authorizationHeader)) {
            throw new InvalidConfigurationException('Invalid Authorization header: ' . $this->authorizationHeader);
        }
    }

    /**
     * @context api
     */
    public function addJwtTokenToHeaders(EventInterface $ev): void {
        if (
            ($geefter = $this->geefterSession->getGeefter())
            && ($token = $this->jwtService->generateToken($geefter))
        ) {
            $ev->getParam('response')->addHeaders([
                'Authentication' => 'Token ' . $token
            ]);
        }
    }

    /**
     * @context http
     */
    public function addApiJsVars(EventInterface $ev): void {
        /** @var LayoutInterface $layout */
        $layout = $ev->getParam('layout');

        /** @var HtmlRootContainer $headBlock */
        $headBlock = $layout->getBlock('head');
        if (!$headBlock instanceof HtmlRootContainer) {
            return;
        }

        $headBlock->addInlineJs(sprintf(
            "var API_BASE_URL = document.API_BASE_URL = '%s';",
            $this->apiUrlBuilder->getBaseUrl()
        ));
        if (
            ($geefter = $this->geefterSession->getGeefter())
            && ($token = $this->jwtService->generateToken($geefter))
        ) {
            $headBlock->addInlineJs(sprintf(
                "var API_AUTHORIZATION = document.API_AUTHORIZATION = {'%s': 'Bearer %s'};",
                $this->authorizationHeader,
                $token
            ));
        }
    }
}
