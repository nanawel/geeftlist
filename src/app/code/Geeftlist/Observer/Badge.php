<?php

namespace Geeftlist\Observer;


use Geeftlist\Model\AbstractModel;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\ArrayObject;

class Badge extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \Geeftlist\Service\Badge $badgeService,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator
    ) {
        parent::__construct($context);
    }

    public function saveEntityBadges(EventInterface $ev): void {
        /** @var AbstractModel $model */
        $model = $ev->getParam('model');

        if (is_array($badgeIds = $model->getBadgeIds())) {
            $this->badgeService->setBadgeIds($model, $badgeIds);
        }
    }

    public function deleteEntityBadges(EventInterface $ev): void {
        /** @var AbstractModel $model */
        $model = $ev->getParam('model');

        $this->badgeService->setBadgeIds($model, []);
    }

    public function addBadgesToGeefterGeefteeDataMapping(EventInterface $ev): void {
        /** @var ArrayObject $dataMapping */
        $dataMapping = $ev->getParam('mapping');

        $dataMapping['badge_ids'] = 'badge_ids';
        $ev->setParam('mapping', $dataMapping);
    }

    public function validateEntityBadges(EventInterface $ev): void {
        /** @var \Geeftlist\Model\AbstractModel $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $validationRules = [
            'badge_ids' => [
                static function ($value): bool {
                    if ($value !== null) {
                        if (!is_array($value)) {
                            return false;
                        }

                        foreach ($value as $badgeId) {
                            if (!is_numeric($badgeId) || ! (int) $badgeId) {
                                return false;
                            }
                        }
                    }

                    return true;
                }
            ]
        ];

        $this->modelValidator->validate($model, $validationRules, $errorAggregator);
    }
}
