<?php

namespace Geeftlist\Observer;

use Geeftlist\Model\AbstractModel;
use Laminas\EventManager\EventInterface;

class Indexer extends AbstractObserver
{
    /** @var int[][] */
    protected static $reindexingEntityIds = [];

    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \Geeftlist\Service\System\Housekeeping $housekeepingService,
        /** @var \Geeftlist\Indexer\DependentEntityResolverInterface[] */
        protected array $dependentEntityResolvers,
        /** @var \Geeftlist\Indexer\IndexerInterface[] */
        protected array $indexers
    ) {
        parent::__construct($context);
    }

    public function pruneBrokenEntityLinksBeforeReindex(EventInterface $ev): void {
        try {
            $this->housekeepingService->pruneBrokenEntityLinks($ev->getParam('entity_type'));
        }
        catch (\InvalidArgumentException) {
            // OK, just ignore
        }
    }

    /**
     * @phpcs:disable Squiz.Functions.MultiLineFunctionDeclaration.SpaceBeforeUse
     * @phpcs:disable Squiz.ControlStructures.ForEachLoopDeclaration.SpacingBeforeAs
     * @phpcs:disable PSR12.ControlStructures.ControlStructureSpacing.FirstExpressionLine
     */
    public function reindexDependentEntities(EventInterface $ev): void {
        /** @var string $entityType */
        $entityType = $ev->getParam('entity_type');
        /** @var AbstractModel[] $entities */
        $entities = $ev->getParam('entities') ?? [$ev->getParam('entity')];
        /** @var \Geeftlist\Indexer\IndexerInterface $indexer */
        $indexer = $ev->getParam('indexer');

        $currentIndexerCode = $indexer->getCode();

        $entityIds = [];
        foreach ($entities as $entity) {
            if (in_array($entity->getId(), self::$reindexingEntityIds[$entityType] ?? [])) {
                $this->logger->notice(sprintf('Skipping %s #%s, already reindexing.', $entityType, $entity->getId()));
            }
            else {
                $entityIds[] = $entity->getId();
            }
        }

        $this->securityService->callPrivileged(
            function () use ($currentIndexerCode, $entities, $entityType, $entityIds): void {
                foreach ($this->getDependentEntitiesByIndexer($currentIndexerCode, $entityType, $entities)
                         as $indexerCode => $dependentEntities
                ) {
                    try {
                        self::$reindexingEntityIds[$entityType] = array_merge(
                            self::$reindexingEntityIds[$entityType] ?? [],
                            $entityIds
                        );

                        $this->logger->debug(sprintf(
                            'Reindexing %d dependent entities with indexer %s',
                            count($dependentEntities),
                            $indexerCode
                        ));
                        $this->getIndexer($indexerCode)->reindexEntities($dependentEntities);
                    }
                    finally {
                        self::$reindexingEntityIds[$entityType] = array_diff(
                            self::$reindexingEntityIds[$entityType],
                            $entityIds
                        );
                    }
                }
            }
        );
    }

    /**
     * @param string $indexerCode
     * @param string $entityType
     * @param AbstractModel[] $entities
     * @return AbstractModel[][]
     */
    protected function getDependentEntitiesByIndexer($indexerCode, $entityType, $entities): array {
        $dependentEntitiesByIndexer = [];
        foreach ($this->getDependentEntityResolvers() as $resolver) {
            if ($resolver->match($indexerCode, $entityType)) {
                $dependentEntitiesByIndexer = array_merge_recursive(
                    $dependentEntitiesByIndexer,
                    $resolver->getDependentEntities($indexerCode, $entities)
                );
            }
        }

        return $dependentEntitiesByIndexer;
    }

    /**
     * @return \Geeftlist\Indexer\DependentEntityResolverInterface[]
     */
    public function getDependentEntityResolvers(): array {
        return $this->dependentEntityResolvers;
    }

    /**
     * @return \Geeftlist\Indexer\IndexerInterface
     */
    public function getIndexer($code) {
        return $this->indexers[$code] ?? null;
    }
}
