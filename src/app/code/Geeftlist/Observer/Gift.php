<?php

namespace Geeftlist\Observer;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Gift\Field\Priority;
use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\ResourceModel\Db\Gift as GiftResource;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Service\Gift\Image;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Gift extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected \Geeftlist\Indexer\Gift\GeefterAccess $giftGeefterAccessIndexer,
        protected \Geeftlist\Model\Gift\RestrictionsAppenderInterface $giftRestrictionsAppender,
        protected \Geeftlist\Service\Gift\Image $giftImageService
    ) {
        parent::__construct($context);
    }

    /**
     * Set gift's creator if missing
     */
    public function setCreator(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();
        if (
            ! $gift->getFlag('skip_autoset_creator')
            && ($currentGeefter = $this->geefterSession->getGeefter())
            && ! $gift->getCreatorId()
        ) {
            $gift->setCreatorId($currentGeefter->getId());
        }
    }

    /**
     * Set default values if missing
     */
    public function setDefaultValues(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();

        if (!$gift->getStatus()) {
            $gift->setStatus(Status::AVAILABLE);
        }

        if (!$gift->getPriority()) {
            $gift->setPriority(Priority::PRIORITY_NONE);
        }
    }

    /**
     * Emulate the "onDelete" event when the gift has been flagged as deleted and about to
     * be saved
     */
    public function onBeforeSaveCheckTriggerMarkAsDeleted(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();

        if (
            $gift->getOrigData('status') != Status::DELETED
            && $gift->getStatus() == Status::DELETED
        ) {
            $gift->triggerEvent('markAsDeleted::before');
        }
    }

    /**
     * Emulate the "onDelete" event when the gift has just been flagged as deleted
     * and saved (so not *really* deleted)
     */
    public function onAfterSaveCheckTriggerMarkAsDeleted(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();

        if (
            $gift->getOrigData('status') != Status::DELETED
            && $gift->getStatus() == Status::DELETED
        ) {
            $gift->triggerEvent('markAsDeleted::after', ['old_status' => $gift->getOrigData('status')]);
        }
    }

    /**
     * Exclude gifts for the current geeftee.
     */
    public function addGeefterAccessRestrictionsToCollection(EventInterface $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection */
            $collection = $ev->getTarget();

            $this->giftRestrictionsAppender
                ->addGeefterAccessRestrictions($collection, $currentGeefter, TypeInterface::ACTION_VIEW);
        }
    }

    /**
     * Append allowed actions to the gift collection.
     */
    public function appendCurrentGeefterAccessPermissions(EventInterface $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection */
            $collection = $ev->getTarget();

            if ($collection->getFlag('skip_append_geefter_access_permissions')) {
                return;
            }

            $this->giftRestrictionsAppender->appendGeefterAccessPermissions($collection, $currentGeefter);
        }
    }

    public function excludeGeefteelessGifts(EventInterface $ev): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection */
        $collection = $ev->getTarget();

        if ($collection->getFlag('skip_exclude_geefteeless_gifts')) {
            return;
        }

        $collection->addFieldToFilter('geeftee', ['null' => false]);
    }

    public function updateIndexes(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift[] $gifts */
        $gifts = $ev->getParam('gifts') ?: [$ev->getParam('gift')];

        foreach ($gifts as $i => $gift) {
            if ($gift->getFlag('skip_reindex')) {
                unset($gifts[$i]);
            }
        }

        $this->giftGeefterAccessIndexer->reindexEntities($gifts);
    }

    public function prepareIndexesUpdateOnFamilyDeletion(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getParam('family');

        $this->securityService->callPrivileged(function () use ($family, $ev): void {
            $geeftees = $family->getGeefteeCollection();
            $family->attachInstanceListener('delete::after', function () use ($geeftees, $ev): void {
                $ev->setParam('geeftees', $geeftees);
                $this->onFamilyChangeUpdateIndexes($ev);
            });
        });
    }

    public function onFamilyChangeUpdateIndexes(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Family $family */
        $family = $ev->getParam('family');
        /** @var \Geeftlist\Model\Geeftee[] $geeftees */
        $geeftees = $ev->getParam('geeftees', []);

        if ($family->isObjectNew() && count($geeftees) === 1) {
            // #402 Optimization: skip reindexing a new family with only 1 member in it
            return;
        }

        $gifts = [];

        $this->securityService->callPrivileged(static function () use ($family, $geeftees, &$gifts): void {
            foreach ($family->getGeefteeCollection() as $geeftee) {
                $gifts = array_merge($gifts, $geeftee->getGiftCollection()->getItems());
            }

            // Also reindex gifts from related geeftees (who might now be out of the family)
            foreach ($geeftees as $geeftee) {
                $gifts = array_merge($gifts, $geeftee->getGiftCollection()->getItems());
            }
        });

        $this->giftGeefterAccessIndexer->reindexEntities($gifts);
    }

    public function onGeefteeSaveUpdateIndexes(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getParam('geeftee');

        if (
            $geeftee->isObjectNew()
            || $geeftee->getOrigData('geefter_id')
            || !$geeftee->getGeefterId()
        ) {
            // Only reindex if a geefter has just been assigned to this geeftee
            // (upon subscription when the email was already used by a geefterless geeftee)
            return;
        }

        $gifts = [];

        $this->securityService->callPrivileged(static function () use ($geeftee, &$gifts): void {
            $families = $geeftee->getFamilyCollection();
            foreach ($families as $family) {
                foreach ($family->getGeefteeCollection() as $geeftee) {
                    $gifts = array_merge($gifts, $geeftee->getGiftCollection()->getItems());
                }
            }
        });

        $this->giftGeefterAccessIndexer->reindexEntities($gifts);
    }

    /**
     * @throws PermissionException
     */
    public function assertCanEdit(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();
        if (
            $gift->getId()
            && $gift->getOrigData('creator_id')
            && ! $this->permissionService->isAllowed($gift, TypeInterface::ACTION_EDIT)
        ) {
            throw new PermissionException('The current geefter cannot edit this gift.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanView(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();
        if (
            $gift->getId()
            && ! $this->permissionService->isAllowed($gift, TypeInterface::ACTION_VIEW)
        ) {
            throw new PermissionException('The current geefter cannot see this gift.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanMarkAsDeleted(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $ev->getTarget();
        if (
            $gift->getId()
            && $gift->getOrigData('creator_id')
            && ! $this->permissionService->isAllowed($gift, TypeInterface::ACTION_DELETE)
        ) {
            throw new PermissionException('The current geefter cannot delete this gift.');
        }
    }

    public function assertCanDelete(EventInterface $ev): never {
        // Currently nobody can delete a gift
        throw new PermissionException('Deletion forbidden.');
    }

    /**
     * Assign sourge geeftee's gifts to target's.
     */
    public function reassignGeefteeGifts(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $sourceGeeftee */
        $sourceGeeftee = $ev->getParam('source_geeftee');
        /** @var \Geeftlist\Model\Geeftee $targetGeeftee */
        $targetGeeftee = $ev->getTarget();

        // Select and delete geeftee/gift associations for gifts already assigned to $targetGeeftee
        // AND also to $sourceGeeftee (updating those rows would throw a unicity constraint error)
        $select = $this->connection->select(GiftResource::GEEFTEE_GIFT_TABLE)
            ->columns(['gift_id'])
            ->where(['geeftee_id' => $targetGeeftee->getId()]);
        $resultSet = $this->connection->query($select);
        $alreadyAssignedGifts = array_column($resultSet->toArray(), 'gift_id');

        if ($alreadyAssignedGifts !== []) {
            $delete = $this->connection->delete(GiftResource::GEEFTEE_GIFT_TABLE);
            $delete->where
                    ->in('gift_id', $alreadyAssignedGifts)
                    ->AND
                    ->equalTo('geeftee_id', $sourceGeeftee->getId());
            $this->connection->query($delete);
        }

        // Now update safely geeftee/gift associations from $sourceGeeftee to $targetGeeftee
        $update = $this->connection->update(GiftResource::GEEFTEE_GIFT_TABLE)
            ->set(['geeftee_id' => $targetGeeftee->getId()])
            ->where(['geeftee_id' => $sourceGeeftee->getId()]);
        $this->connection->query($update);
    }

    public function onDeleteGeefterMigrateGifts(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $ev->getTarget();
        /** @var \Geeftlist\Model\Geefter $recovererGeefter|false */
        $recovererGeefter = $ev->getParam('recoverer_geefter');

        if ($recovererGeefter) {
            $update = $this->connection->update(GiftResource::MAIN_TABLE)
                ->set(['creator_id' => $recovererGeefter->getId()])
                ->where(['creator_id' => $geefter->getId()]);
            $this->connection->query($update);
        }
        else {
            // Assign orphan gifts to the first geefter with a reservation on it or delete them if none available
            $gifts = $this->geeftlistResourceFactory->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE)
                ->addFieldToFilter('creator_id', $geefter->getId());

            /** @var \Geeftlist\Model\Gift $gift */
            foreach ($gifts as $gift) {
                $reassigned = false;
                /** @var \Geeftlist\Model\Reservation $reservation */
                foreach ($gift->getReservationCollection() as $reservation) {
                    if ($reservation->getGeefterId() != $geefter->getId()) {
                        try {
                            $gift->setCreatorId($reservation->getGeefterId())
                                ->save();
                            $reassigned = true;
                        }
                        catch (PermissionException) {
                        }

                        break;
                    }
                }

                if (! $reassigned) {
                    $this->securityService->callPrivileged(static function () use ($gift): void {
                        $gift->delete();
                    });
                }
            }
        }
    }

    public function validateModel(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Gift $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $constraints = [
            'creator_id' => [ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE],
            'created_at' => [ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE],
            'label'      => [ErrorInterface::TYPE_FIELD_EMPTY],
            'estimated_price' => [static fn($value): bool => empty($value) || ((float) $value)],
            'image_id' => [function ($value) {
                if ($value) {
                    return $this->giftImageService->validate($value, Image::IMAGE_TYPE_FULL)
                        || $this->giftImageService->validate($value, Image::IMAGE_TYPE_TMP);
                }
            }]
        ];

        $this->modelValidator->validate($model, $constraints, $errorAggregator);
    }
}
