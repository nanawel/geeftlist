<?php

namespace Geeftlist\Observer;


use Laminas\EventManager\Event;

class Setup extends AbstractObserver
{
    public function __construct(
        Context $context,
        /** @var \Geeftlist\Indexer\IndexerInterface[] */
        protected array $indexers
    ) {
        parent::__construct($context);
    }

    public function afterInstallSampleDataReindexAllEntities(Event $ev): void {
        $executed = $ev->getParam('executed');

        if ($executed) {
            $this->logger->info('Setup install executed, reindexing entities.');
            $this->reindexAllEntities();
        }
        else {
            $this->logger->info('No setup install executed, skipping entities reindex.');
        }
    }

    public function afterUpgradeSampleDataReindexAllEntities(Event $ev): void {
        $executed = $ev->getParam('executed');

        if ($executed) {
            $this->logger->info('Setup upgrade executed, reindexing entities.');
            $this->reindexAllEntities();
        }
        else {
            $this->logger->info('No setup upgrade executed, skipping entities reindex.');
        }
    }

    protected function reindexAllEntities() {
        foreach ($this->indexers as $indexer) {
            $indexer->reindexAll();
        }
    }
}
