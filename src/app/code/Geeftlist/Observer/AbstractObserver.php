<?php

namespace Geeftlist\Observer;


abstract class AbstractObserver
{
    protected \Base $fw;

    protected \OliveOil\Core\Model\App\ConfigInterface $appConfig;

    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \Geeftlist\Model\Session\GeefterInterface $geefterSession;

    protected \Geeftlist\Model\ModelFactory $geeftlistModelFactory;

    protected \Geeftlist\Model\ResourceFactory $geeftlistResourceFactory;

    protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory;

    protected \Geeftlist\Service\Security $securityService;

    protected \Geeftlist\Service\Permission $permissionService;

    protected \Psr\Log\LoggerInterface $logger;

    public function __construct(
        \Geeftlist\Observer\Context $context
    ) {
        $this->fw = $context->getFw();
        $this->appConfig = $context->getAppConfig();
        $this->connection = $context->getConnection();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->i18n = $context->getI18n();
        $this->geefterSession = $context->getGeefterSession();
        $this->geeftlistModelFactory = $context->getGeeftlistModelFactory();
        $this->geeftlistResourceFactory = $context->getGeeftlistResourceFactory();
        $this->modelRepositoryFactory = $context->getModelRepositoryFactory();
        $this->securityService = $context->getSecurityService();
        $this->permissionService = $context->getPermissionService();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }
}
