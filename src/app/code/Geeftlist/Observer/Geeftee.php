<?php

namespace Geeftlist\Observer;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use Laminas\EventManager\EventInterface;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Geeftee extends AbstractObserver
{
    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    /**
     * @param string $nameValidationPattern
     */
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        protected \Geeftlist\Model\Geeftee\RestrictionsAppenderInterface $geefteeRestrictionsAppender,
        protected $nameValidationPattern
    ) {
        $this->coreFactory = $context->getCoreFactory();
        parent::__construct($context);
    }

    /**
     * Set geeftee's creator if missing
     */
    public function setCreator(EventInterface $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\Geeftee $geeftee */
            $geeftee = $ev->getTarget();
            if (
                !$geeftee->getFlag('skip_autoset_creator') && (! $geeftee->getId()
                && ! $geeftee->getCreatorId()
                && $geeftee->getGeefterId() != $currentGeefter->getId())
            ) {
                $geeftee->setCreatorId($currentGeefter->getId());
            }
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanView(EventInterface $ev) {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        // Special check here since we can be in the middle of a login phase (and the target geeftee
        // might just be the one logging in!)
        if ($this->geefterSession->isLoggedIn()) {
            $currentGeefter = $this->geefterSession->getGeefter();

            // Avoid loop
            if ($geeftee->getGeefterId() == $currentGeefter->getId()) {
                return true;
            }
        }

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_VIEW)) {
            throw new PermissionException('Cannot view this geeftee.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanEdit(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_EDIT)) {
            throw new PermissionException('Cannot modify this geeftee.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanDelete(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_DELETE)) {
            throw new PermissionException('Cannot delete this geeftee.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanMergeTo(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_MERGE_TO)) {
            throw new PermissionException('Cannot merge this geeftee.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanAddGifts(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_ADD_GIFT)) {
            throw new PermissionException('Cannot add gift to this geeftee.');
        }
    }

    /**
     * @throws PermissionException
     */
    public function assertCanAddBadges(EventInterface $ev): void {
        $this->assertCanSetBadges($ev);
    }

    /**
     * @throws PermissionException
     */
    public function assertCanRemoveBadges(EventInterface $ev): void {
        $this->assertCanSetBadges($ev);
    }

    /**
     * @throws PermissionException
     */
    public function assertCanSetBadges(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $ev->getTarget();

        if (!$this->permissionService->isAllowed($geeftee, TypeInterface::ACTION_SET_BADGES)) {
            throw new PermissionException('Cannot set badges from this geeftee.');
        }
    }

    /**
     * Exclude geeftees according to current geeftee.
     */
    public function addGeefterAccessRestrictionsToCollection(EventInterface $ev): void {
        if ($currentGeefter = $this->geefterSession->getGeefter()) {
            /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
            $collection = $ev->getTarget();

            $this->geefteeRestrictionsAppender
                ->addGeefterAccessRestrictions(
                    $collection,
                    $currentGeefter,
                    $collection->getFlag('required_actions') ?: TypeInterface::ACTION_VIEW
                );
        }
    }

    public function validateModel(EventInterface $ev): void {
        /** @var \Geeftlist\Model\Geeftee $model */
        $model = $ev->getTarget();
        /** @var \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $ev->getParam('error_aggregator');

        $validationRules = [
            'name' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                ErrorInterface::TYPE_FIELD_EMPTY,
                function ($value) use ($model) {
                    if (! $model->assertNameUnique()) {
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This name is already taken!')
                            ->setMessage('This name is already taken!');
                    }
                },
                function ($value) use ($model) {
                    // #559 Skip validation only for existing geeftees (pre-#140) if name has not been changed
                    if (
                        $model->getOrigData('name') !== $value
                        && ! preg_match($this->nameValidationPattern, $value)
                    ) {
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This name is not valid.')
                            ->setMessage('This name is not valid.');
                    }
                }
            ],
            'email' => [
                ErrorInterface::TYPE_FIELD_NOT_SCALAR,
                $this->coreFactory->make(\OliveOil\Core\Service\Validation\Email::class),
                function ($value) use ($model) {
                    if (trim((string) $model->getEmail()) !== '' && ! $model->assertEmailUnique()) {
                        $error = $this->coreFactory->make(ErrorInterface::class);
                        return $error->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('This email already exists!')
                            ->setMessage('This email already exists!');
                    }
                }
            ],
            'avatar_path' => [
                ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE
            ]
        ];

        $this->modelValidator->validate($model, $validationRules, $errorAggregator);
    }
}
