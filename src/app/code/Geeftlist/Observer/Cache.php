<?php


namespace Geeftlist\Observer;


use Geeftlist\Model\AbstractModel;
use Laminas\EventManager\EventInterface;

class Cache extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Cache $cache
    ) {
        parent::__construct($context);
    }

    public function onModelSave(EventInterface $ev): void {
        /** @var AbstractModel $model */
        $model = $ev->getTarget();

        $this->cache->invalidateTags($model->getIdentities());
    }

    public function onModelDelete(EventInterface $ev): void {
        /** @var AbstractModel $model */
        $model = $ev->getTarget();

        $this->cache->invalidateTags($model->getIdentities());
    }
}
