<?php

namespace Geeftlist\Observer\MailNotification\Gift;

use Geeftlist\Model\MailNotification\Event;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Observer\MailNotification\Gift;
use OliveOil\Core\Model\MagicObject;

class AddToGeeftees extends Gift
{
    public function includeNotifiableGeefters(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $notifiableGeefters = $this->permissionService
            ->getAllowed($target, TypeInterface::ACTION_BE_NOTIFIED);
        $notifiableGeefterIds = MagicObject::getAllDataValues($notifiableGeefters, 'id');

        if ($notifiableGeefterIds === []) {
            // Empty include: Force returning empty collection and stop propagation
            $geefters->addFieldToFilter('main_table.geefter_id', 0);
            $ev->stopPropagation();
        } else {
            $geefters->addFieldToFilter('main_table.geefter_id', ['in' => $notifiableGeefterIds]);
        }

        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }

    public function excludeEmptyGiftCreator(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        if (! $target->getCreatorId()) {
            // Add a WHERE geefter_id = 0 which ensures zero results
            $geefters->addFieldToFilter('main_table.geefter_id', 0);

            $geefters->setFlag(
                'filters_applied',
                array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
            );
        }
    }

    /**
     * Explicitly exclude geeftee for open gifts *even if notifiable* because it's handled
     * via \Geeftlist\Observer\MailNotification\AddToMe::includeGiftGeeftees (avoids duplicate item)
     */
    public function excludeGeefteeOnOpenGift(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        if ($this->giftHelper->isOpenGift($target)) {
            $geefters->addFieldToFilter('geeftees', ['nin' => $target->getGeefteeIds()])
                ->setFlag(
                    'filters_applied',
                    array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
                )
            ;
        }
    }
}
