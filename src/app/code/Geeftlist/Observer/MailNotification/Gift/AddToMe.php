<?php

namespace Geeftlist\Observer\MailNotification\Gift;

use Geeftlist\Model\MailNotification\Event;
use Geeftlist\Observer\MailNotification\Gift;

class AddToMe extends Gift
{
    public function includeGiftGeeftees(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geeftees = $this->geefteeGiftHelper->getGeeftees($target);
        $notifiableGeefterIds = [];
        foreach ($geeftees as $geeftee) {
            $notifiableGeefterIds[] = $geeftee->getGeefterId();
        }

        if ($notifiableGeefterIds === []) {
            // Empty include: Force returning empty collection and stop propagation
            $geefters->addFieldToFilter('main_table.geefter_id', 0);
            $ev->stopPropagation();
        } else {
            $geefters->addFieldToFilter('geefter_id', ['in' => $notifiableGeefterIds]);
        }

        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }
}
