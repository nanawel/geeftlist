<?php

namespace Geeftlist\Observer\MailNotification;


use Geeftlist\Model\MailNotification\Constants;
use Geeftlist\Model\MailNotification\EventInterface;

class MailNotification extends AbstractObserver
{
    public function excludeGeeftersDisabledNotifications(EventInterface $ev): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geefters->addFieldToFilter('email_notification_freq', ['ne' => Constants::SENDING_FREQ_DISABLED]);

        // This observer is not considered like a normal filter so we don't add it to the "filters_applied" flag
        // to prevent invalid broadcast of notifications
//        $geefters->setFlag(
//            'filters_applied',
//            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
//        );
    }
}
