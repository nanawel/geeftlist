<?php

namespace Geeftlist\Observer\MailNotification;

use Geeftlist\Model\MailNotification\Event;

class Family extends AbstractObserver
{
    public function includeRelatedGeeftees(Event $ev): void {
        /** @var \Geeftlist\Model\Family $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geefters->addFieldToFilter('geeftees', ['in' => $target->getGeefteeCollection()->getAllIds()]);

        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }

    public function excludeGeeftees(Event $ev): void {
        $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geefters->addFieldToFilter('geeftees', ['nin' => $ev->getParam('geeftee_ids')]);

        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }
}
