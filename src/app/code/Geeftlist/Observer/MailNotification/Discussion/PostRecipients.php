<?php

namespace Geeftlist\Observer\MailNotification\Discussion;

use Geeftlist\Model\MailNotification\Event;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;
use Geeftlist\Observer\MailNotification\AbstractObserver;
use OliveOil\Core\Model\MagicObject;

class PostRecipients extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        \Geeftlist\Service\Geefter\Email $geefterEmailService
    ) {
        parent::__construct($context, $senderService, $geefterEmailService);
    }

    public function includeNotifiableGeefters(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $target */
        $target = $ev->getTarget();
        /** @var int[] $recipientIds */
        $recipientIds = $ev->getParam('recipient_ids') ?? [];
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $notifiableGeefters = $this->permissionService
            ->getAllowed($target->getTopic(), TypeInterface::ACTION_BE_NOTIFIED);
        $notifiableGeefterIds = array_intersect(
            $recipientIds,
            MagicObject::getAllDataValues($notifiableGeefters, 'id')
        );

        $geefters->addFieldToFilter(
            'main_table.geefter_id',
            $notifiableGeefterIds !== [] ? ['in' => $notifiableGeefterIds] : 0
        );
        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }
}
