<?php

namespace Geeftlist\Observer\MailNotification\Discussion;

use Geeftlist\Model\MailNotification\Event;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Observer\MailNotification\AbstractObserver;
use OliveOil\Core\Model\MagicObject;

class Post extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        \Geeftlist\Service\Geefter\Email $geefterEmailService
    ) {
        parent::__construct($context, $senderService, $geefterEmailService);
    }

    public function includeNotifiableGeefters(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $notifiableGeefters = $this->permissionService
            ->getAllowed($target->getTopic(), TypeInterface::ACTION_BE_NOTIFIED);
        $notifiableGeefterIds = MagicObject::getAllDataValues($notifiableGeefters, 'id');

        $geefters->addFieldToFilter(
            'main_table.geefter_id',
            $notifiableGeefterIds !== [] ? ['in' => $notifiableGeefterIds] : 0
        );
        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }

    public function excludePostAuthor(Event $ev): void {
        /** @var \Geeftlist\Model\Discussion\Post $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geefters->addFieldToFilter('main_table.geefter_id', ['ne' => $target->getAuthorId()]);
    }
}
