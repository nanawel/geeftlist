<?php

namespace Geeftlist\Observer\MailNotification;

use Geeftlist\Model\MailNotification\Event;

abstract class Gift extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        \Geeftlist\Service\Geefter\Email $geefterEmailService,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        protected \Geeftlist\Helper\Gift $giftHelper
    ) {
        parent::__construct($context, $senderService, $geefterEmailService);
    }

    public function excludeEmptyGiftCreator(Event $ev): void {
        /** @var \Geeftlist\Model\Gift $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        if (! $target->getCreatorId()) {
            // Add a WHERE geefter_id = 0 which ensures zero results
            $geefters->addFieldToFilter('main_table.geefter_id', 0);

            $geefters->setFlag(
                'filters_applied',
                array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
            );
        }
    }

    public function excludeActor(Event $ev): void {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');
        $actorId = $ev->getParam('actor_id');

        if ($actorId) {
            $geefters->addFieldToFilter('main_table.geefter_id', ['ne' => $actorId]);
            $geefters->setFlag(
                'filters_applied',
                array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
            );
        }
    }
}
