<?php

namespace Geeftlist\Observer\MailNotification;

use Geeftlist\Model\MailNotification\Event;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use OliveOil\Core\Model\MagicObject;

class Reservation extends AbstractObserver
{
    public function __construct(
        \Geeftlist\Observer\Context $context,
        \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        \Geeftlist\Service\Geefter\Email $geefterEmailService,
        protected \OliveOil\Core\Model\RepositoryInterface $giftRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository
    ) {
        parent::__construct($context, $senderService, $geefterEmailService);
    }

    /**
     * Special method: recreate the deleted object for the following methods
     */
    public function recreateObject(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $target */
        $target = $ev->getTarget();

        $giftId = $ev->getParam('gift_id');
        $geefterId = $ev->getParam('geefter_id');
        $target->setGiftId($giftId)
            ->setGift($this->giftRepository->get($giftId))
            ->setGeefterId($geefterId)
            ->setGeefter($this->geefterRepository->get($geefterId));
    }

    public function includeNotifiableGeefters(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($target->getGiftId());
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $notifiableGeefters = $this->permissionService
            ->getAllowed($gift, TypeInterface::ACTION_BE_NOTIFIED);
        $notifiableGeefterIds = MagicObject::getAllDataValues($notifiableGeefters, 'id');

        $geefters->addFieldToFilter(
            'main_table.geefter_id',
            $notifiableGeefterIds !== [] ? ['in' => $notifiableGeefterIds] : 0
        );
        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }

    public function excludeActor(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geefters->addFieldToFilter('main_table.geefter_id', ['ne' => $target->getGeefterId()]);

        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }

    /**
     * NOTE: This method should *NOT* be necessary since notifiable geefters should already
     *       be retrieved by self::includeNotifiableGeefters()
     *       BUT this needs a refactoring to index reservation permissions separately. (see #373)
     */
    public function excludeGiftGeeftees(Event $ev): void {
        /** @var \Geeftlist\Model\Reservation $target */
        $target = $ev->getTarget();
        /** @var \Geeftlist\Model\ResourceModel\Db\Geefter\Collection $geefters */
        $geefters = $ev->getParam('geefter_collection');

        $geefteeIds = $target->getGift()->getGeefteeCollection()->getAllIds();
        $geefters->addFieldToFilter('geeftees', ['nin' => $geefteeIds]);

        $geefters->setFlag(
            'filters_applied',
            array_merge($geefters->getFlag('filters_applied') ?: [], [__METHOD__])
        );
    }
}
