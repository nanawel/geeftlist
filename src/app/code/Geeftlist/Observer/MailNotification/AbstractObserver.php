<?php

namespace Geeftlist\Observer\MailNotification;


use Geeftlist\Model\Notification;

class AbstractObserver extends \Geeftlist\Observer\AbstractObserver
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $notificationRepository;

    public function __construct(
        \Geeftlist\Observer\Context $context,
        protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService
    ) {
        $this->notificationRepository = $context->getModelRepositoryFactory()->resolveGet(Notification::ENTITY_TYPE);
        parent::__construct($context);
    }
}
