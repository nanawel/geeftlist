<?php

namespace Geeftlist\Service\Avatar;


interface GeneratorInterface
{
    /**
     * @param string $name
     * @return \Intervention\Image\Image
     */
    public function generate($name);
}
