<?php

namespace Geeftlist\Service\Avatar;


class Generator implements GeneratorInterface
{
    public function __construct(protected \LasseRafn\InitialAvatarGenerator\InitialAvatar $initialAvatar)
    {
    }

    /**
     * @param string $name
     * @return \Intervention\Image\Image
     */
    public function generate($name) {
        $color = sprintf("#%6.6s", hash('md2', $name));

        return $this->getInitialAvatar()
            ->background($color)
            ->generate($name);
    }

    public function getInitialAvatar(): \LasseRafn\InitialAvatarGenerator\InitialAvatar {
        return $this->initialAvatar;
    }
}
