<?php

namespace Geeftlist\Service\Avatar;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Exception\UploadException;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;
use OliveOil\Core\Service\UploadInterface;

class Geeftee implements GeefteeInterface
{
    public const AVATAR_PATH_PATTERN = '/^([0-9a-f]{8})\/(generated|user)_\d+_\d+\.[a-z]{3}$/';

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /**
     * @param string $placeholderUrl
     * @param string $avatarBasePath
     * @param string $avatarUrlBasePath
     * @param int $imageDirPermissions
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\Crypt $cryptService,
        protected \Geeftlist\Service\Avatar\GeneratorInterface $generator,
        protected \Geeftlist\Service\Security $securityService,
        protected \OliveOil\Core\Service\UploadInterface $uploadService,
        protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository,
        \OliveOil\Core\Service\Log $logService,
        protected $placeholderUrl,
        protected $avatarBasePath,
        protected $avatarUrlBasePath,
        protected $imageDirPermissions = 0775
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    public function getUrl(AbstractModel $model, $params = []) {
        return $this->getUrlByGeeftee($model, $params);
    }

    /**
     * @param \Geeftlist\Model\Geeftee $geeftee
     * @param array $params
     * @return string
     */
    public function getUrlByGeeftee(AbstractModel $geeftee, $params = []) {
        try {
            if ($avatarPath = $this->getAvatarPath($geeftee)) {
                return $this->getUrlByAvatarPath($avatarPath, $params);
            }
        }
        catch (\Throwable $throwable) {
            $this->getLogger()->error("Cannot get avatar's URL: " . $throwable->getMessage(), ['exception' => $throwable]);
        }

        return $this->getPlaceholderUrl();
    }

    /**
     * @param string $avatarPath
     * @param array $params
     * @return string|null
     */
    public function getUrlByAvatarPath(?string $avatarPath, $params = []) {
        if ($avatarPath) {
            return $this->getUrlBuilder()->getUrl(
                $this->getAvatarUrlBasePath() . DIRECTORY_SEPARATOR . $avatarPath,
                $params
            );
        }

        return null;
    }

    /**
     * @param \Geeftlist\Model\Geeftee|\Geeftlist\Model\Geefter $geeftee
     * @param string $inputName
     * @@param ErrorAggregatorInterface $errorAggregator
     * @return \OliveOil\Core\Model\Upload\File|null
     * @throws UploadException
     */
    public function upload(AbstractModel $geeftee, $inputName, ErrorAggregatorInterface $errorAggregator) {
        if ($geeftee instanceof Geefter) {
            $geefter = $geeftee;
            $geeftee = $geeftee->getGeeftee();
        }

        [$relativePath, $fullPath] = $this->makeAvatarPath(self::TYPE_USER_DEFINED, $geeftee->getId());

        $file = $this->getUploadService()->upload(
            $inputName,
            dirname($fullPath),
            $errorAggregator,
            [UploadInterface::OPTION_FILENAME => basename($fullPath)]
        );

        if (empty($errorAggregator->getFieldErrors($inputName))) {
            $file->setData('avatar_path', $avatarPath = sprintf(
                '%s/%s',
                dirname($relativePath),
                basename((string) $file->getData('name'))    // Name might have been altered by the upload (extension, etc.)
            ));

            // No errors, set the path to $geefter or $geeftee
            ($geefter ?? $geeftee)->setAvatarPath($avatarPath);
        }

        return $file;
    }

    /**
     * @param string $avatarPath
     * @throws \InvalidArgumentException
     * @throws FilesystemException
     */
    public function apply(\Geeftlist\Model\Geeftee $geeftee, $avatarPath): void {
        if (! $this->validate($avatarPath)) {
            $this->getLogger()->warning(sprintf(
                'Tried to apply an invalid avatar path "%s" on geeftee #%d.',
                $avatarPath,
                $geeftee->getId() ?: '<none>'
            ));
            throw new \InvalidArgumentException('Invalid avatar path.');
        }

        // Nothing more to do here, field is already filled in with the right value
    }

    public function getAvatarPath(\Geeftlist\Model\Geeftee $geeftee, $generateIfMissing = true, $saveToModel = true) {
        $path = $geeftee->getAvatarPath();

        if ((! $path || ! $this->fileExists($path)) && $generateIfMissing) {
            $path = $this->generateAvatar($geeftee);
            if ($saveToModel) {
                $this->getSecurityService()->callPrivileged(function () use ($geeftee, $path): void {
                    $geeftee->setAvatarPath($path);
                    $this->getGeefteeRepository()->save($geeftee);
                });
            }
        }

        return $path;
    }

    /**
     * @return string
     * @throws FilesystemException
     */
    public function generateAvatar(\Geeftlist\Model\Geeftee $geeftee) {
        $image = $this->getGenerator()->generate($geeftee->getName());

        [$relativePath, $fullPath] = $this->makeAvatarPath(self::TYPE_GENERATED, $geeftee->getId());
        $this->prepareImageDir($fullPath);
        $image->save($fullPath);

        return $relativePath;
    }

    /**
     * @param string $type
     * @param int $geefteeId
     * @return string[] [relativePath, fullPath]
     */
    public function makeAvatarPath($type, $geefteeId): array {
        $token = $this->getCryptService()->generateToken(8);
        $relativePath = sprintf(
            '%s/%s_%d_%d.png',
            $token,
            $type,
            $this->getAppConfig()->getValue('AVATAR_VERSION'),
            $geefteeId
        );

        return [$relativePath, sprintf('%s/%s', $this->getAvatarBasePath(), $relativePath)];
    }

    /**
     * @return $this
     * @throws FilesystemException
     */
    protected function prepareImageDir(string $imagePath): static {
        $dirPerms = $this->getImageDirPermissions();
        if (! is_dir($dirPath = dirname($imagePath))) {
            if (! mkdir($dirPath, $dirPerms, true)) {
                throw new FilesystemException('Cannot create directory ' . $imagePath);
            }
        } elseif (! chmod($dirPath, $dirPerms)) {
            $this->getLogger()->warning(sprintf('Cannot change directory %s permissions to %d.', $dirPath, $dirPerms));
        }

        return $this;
    }

    /**
     * @param string $avatarPath
     */
    public function validate($avatarPath): bool {
        if (!$avatarPath) {
            return false;
        }

        return strlen($avatarPath)
            && $this->matchImageIdFormat($avatarPath)
            && $this->fileExists($avatarPath);
    }

    /**
     * @param string $avatarPath
     */
    public function matchImageIdFormat($avatarPath): bool {
        return (bool) preg_match(self::AVATAR_PATH_PATTERN, $avatarPath);
    }

    public function fileExists(string $avatarPath): bool {
        return is_file($this->getAvatarBasePath() . DIRECTORY_SEPARATOR . $avatarPath);
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }

    public function getGenerator(): \Geeftlist\Service\Avatar\GeneratorInterface {
        return $this->generator;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    public function getUploadService(): \OliveOil\Core\Service\UploadInterface {
        return $this->uploadService;
    }

    public function getGeefteeRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefteeRepository;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * @return string
     */
    public function getPlaceholderUrl() {
        return $this->placeholderUrl;
    }

    /**
     * @return string
     */
    public function getAvatarBasePath() {
        return $this->avatarBasePath;
    }

    /**
     * @return string
     */
    public function getAvatarUrlBasePath() {
        return $this->avatarUrlBasePath;
    }

    /**
     * @return int
     */
    public function getImageDirPermissions() {
        return $this->imageDirPermissions;
    }
}
