<?php

namespace Geeftlist\Service;


use Geeftlist\Model\AbstractModel;

interface PermissionInterface
{
    /**
     * @param AbstractModel|AbstractModel[] $entities
     * @param string|string[] $actions
     * @return bool|null
     */
    public function isAllowed($entities, $actions);

    /**
     * @param AbstractModel|AbstractModel[] $entities
     * @param string|string[] $actions
     * @return AbstractModel[]
     */
    public function getAllowed($entities, $actions);
}
