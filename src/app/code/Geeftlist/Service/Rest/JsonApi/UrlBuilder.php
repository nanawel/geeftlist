<?php


namespace Geeftlist\Service\Rest\JsonApi;

/**
 * Note: this class would eventually be moved to \Geeftlist\Service\Url\Builder when all URL builders
 *       are refactored to use \League\Uri classes.
 */
class UrlBuilder
{
    /**
     * @param string $baseUrl
     */
    public function __construct(protected $baseUrl)
    {
    }

    // TODO
}
