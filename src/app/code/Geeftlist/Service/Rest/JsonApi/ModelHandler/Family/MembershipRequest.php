<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler\Family;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\MissingValueException;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

class MembershipRequest extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_POST,
    ];

    public function __construct(
        \Geeftlist\Service\Rest\Context $context,
        protected \Geeftlist\Helper\Family\Membership $familyMembershipHelper,
        $entityType
    ) {
        parent::__construct($context, $entityType);
    }

    public function post(RequestInterface $request, ResponseInterface $response): void {
        $data = $request->getData();
        $this->assertIsResourceObject(
            $data,
            sprintf('%s/family/data', self::RESOURCE_OBJECT_FIELD_RELATIONSHIPS),
            Family::ENTITY_TYPE,
            [self::RESOURCE_OBJECT_FIELD_ID]
        );
        $family = $this->getModelRepository(Family::ENTITY_TYPE)
            ->get($data[self::RESOURCE_OBJECT_FIELD_RELATIONSHIPS]['family']['data']['id']);

        $geeftee = null;
        try {
            $this->assertIsResourceObject(
                $data,
                sprintf('%s/geeftee/data', self::RESOURCE_OBJECT_FIELD_RELATIONSHIPS),
                Geeftee::ENTITY_TYPE,
                [self::RESOURCE_OBJECT_FIELD_ID]
            );
            $geeftee = $this->getModelRepository(Geeftee::ENTITY_TYPE)
                ->get($data[self::RESOURCE_OBJECT_FIELD_RELATIONSHIPS]['geeftee']['data']['id']);
        }
        catch (MissingValueException) {
            // just ignore
        }

        $model = $this->getFamilyMembershipHelper()->request($family, $geeftee);

        // Reload model to get all attributes
        $model = $this->getModelRepository()
            ->get($model->getId());

        $response->setCode(HttpStatus::STATUS_CREATED)
            ->setData($this->getJsonApiEncoder()->encodeData($model));
    }

    protected function getSponsorRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Model\Family\MembershipRequest $membershipRequest */
        $membershipRequest = $this->getModelRepository(\Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $geefterRestService */
        $geefterRestService = $this->getRestService(Geefter::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(['id' => $membershipRequest->getSponsorId()]);

        $geefterRestService->get($forwardedRequest, $response);
    }

    public function acceptAction(RequestInterface $request, ResponseInterface $response): void {
        /** @var Family\MembershipRequest $membershipRequest */
        $membershipRequest = $this->getModelRepository(\Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        $this->getFamilyMembershipHelper()->acceptRequest($membershipRequest);

        $this->getRestService()->get($request, $response);
    }

    public function cancelAction(RequestInterface $request, ResponseInterface $response): void {
        /** @var Family\MembershipRequest $membershipRequest */
        $membershipRequest = $this->getModelRepository(\Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        $this->getFamilyMembershipHelper()->cancelRequest($membershipRequest);

        $this->getRestService()->get($request, $response);
    }

    public function rejectAction(RequestInterface $request, ResponseInterface $response): void {
        /** @var Family\MembershipRequest $membershipRequest */
        $membershipRequest = $this->getModelRepository(\Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        $this->getFamilyMembershipHelper()->rejectRequest($membershipRequest);

        $this->getRestService()->get($request, $response);
    }

    public function getFamilyMembershipHelper(): \Geeftlist\Helper\Family\Membership {
        return $this->familyMembershipHelper;
    }
}
