<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use OliveOil\Core\Constants\Http;

/**
 * Class Badge
 *
 * @method \Geeftlist\Model\Badge getModel($id, $field = null, $entityType = null)
 */
class Badge extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
    ];
}
