<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\GiftList;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

/**
 * Class Gift
 *
 * @method \Geeftlist\Model\Gift getModel($id, $field = null, $entityType = null)
 */
class Gift extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_POST,
        Http::VERB_DELETE,
    ];

    public function __construct(
        \Geeftlist\Service\Rest\Context $context,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftHelper,
        $entityType
    ) {
        parent::__construct($context, $entityType);
    }

    /**
     * Special behavior for gifts: we replace standard deletion with the "mark as deleted" action
     *
     * @throws NoSuchEntityException
     * @throws NotImplementedException
     * @throws \InvalidArgumentException
     */
    public function delete(RequestInterface $request, ResponseInterface $response): void {
        if (is_numeric($id = $request->getRouteParam('id'))) {
            $model = $this->getModel($id)
                ->markAsDeleted();
            $this->getModelRepository()->save($model);

            $response->setCode(HttpStatus::STATUS_OK)
                ->setData($this->getJsonApiEncoder()->encodeMeta(['deleted_items' => 1]));
        }
        elseif (is_array($params = $request->getQueryParams())) {
            // FIXME Add support for filters/limit/order
            throw new NotImplementedException(__METHOD__);
        }
        else {
            throw new \InvalidArgumentException();
        }
    }

    protected function getCreatorRelationships(RequestInterface $request, ResponseInterface $response) {
        $gift = $this->modelRepositoryFactory->resolveGet(\Geeftlist\Model\Gift::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geefter $geefterRestService */
        $geefterRestService = $this->getRestService(Geefter::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setRouteParams(['id' => $gift->getCreatorId()]);

        $geefterRestService->get($forwardedRequest, $response);
    }

    protected function getGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        $gift = $this->modelRepositoryFactory->resolveGet(\Geeftlist\Model\Gift::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $geefteeRestService */
        $geefteeRestService = $this->getRestService(Geeftee::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    RepositoryInterface::WILDCARD_ID_FIELD => ['in' => $gift->getGeefteeIds()]
                ]
            ]
        ));

        $geefteeRestService->get($forwardedRequest, $response);
    }

    protected function getDiscussionTopicsRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $discussionTopicRestService */
        $discussionTopicRestService = $this->getRestService(Topic::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    'linked_entity_type' => ['eq' => \Geeftlist\Model\Gift::ENTITY_TYPE],
                    'linked_entity_id' => ['eq' => $request->getRouteParam('id')]
                ]
            ]
        ));

        $discussionTopicRestService->get($forwardedRequest, $response);
    }

    protected function getGiftListsRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $giftlistRestService */
        $giftlistRestService = $this->getRestService(GiftList::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    'gifts' => ['in' => $request->getRouteParam('id')]
                ]
            ]
        ));

        $giftlistRestService->get($forwardedRequest, $response);
    }

    /**
     * Handler for "PATCH /gift/id/relationships/creator"
     */
    protected function patchCreatorRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $newCreatorId = $this->extractToOneRelationshipsId($request->getData(), \Geeftlist\Model\Geefter::ENTITY_TYPE);

        $subRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $id])
            ->setData([
                'type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'attributes' => [
                    'creator_id' => $newCreatorId
                ]
            ]);

        $this->patch($subRequest, $response);
    }

    /**
     * Handler for "PATCH /gift/id/relationships/geeftees"
     */
    protected function patchGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $geeftees = $this->getModelRepository(Geeftee::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Geeftee::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getModel($id);
        if (! $gift || ! $gift->getId()) {
            throw new NoSuchEntityException($id);
        }

        if (empty($geeftees)) {
            throw new \InvalidArgumentException('Empty or unavailable list of geeftees provided.');
        }

        $this->geefteeGiftHelper
            ->replaceLinks([$gift], $geeftees);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGeefteesRelationships($getRequest, $response);
    }

    /**
     * Handler for "PATCH /gift/id/relationships/giftLists"
     */
    protected function patchGiftListsRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        /** @var GiftList[] $giftLists */
        $giftLists = $this->getModelRepository(GiftList::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), GiftList::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getModel($id);
        if (! $gift || ! $gift->getId()) {
            throw new NoSuchEntityException($id);
        }

        if (empty($giftLists)) {
            // Nothing, this is not an abnormal situation, it only means "remove from all lists"
        }

        $this->giftListGiftHelper
            ->replaceLinks([$gift], $giftLists);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGiftListsRelationships($getRequest, $response);
    }

    /**
     * Handler for "POST /gift/id/relationships/geeftees"
     */
    protected function postGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $geeftees = $this->getModelRepository(Geeftee::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Geeftee::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getModel($id);
        if (! $gift || ! $gift->getId()) {
            throw new NoSuchEntityException($id);
        }

        $this->geefteeGiftHelper
            ->addLinks($geeftees, [$gift]);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGeefteesRelationships($getRequest, $response);
    }

    /**
     * Handler for "DELETE /gift/id/relationships/geeftees"
     */
    protected function deleteGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $geeftees = $this->getModelRepository(Geeftee::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Geeftee::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getModel($id);
        if (! $gift || ! $gift->getId()) {
            throw new NoSuchEntityException($id);
        }

        $this->geefteeGiftHelper
            ->removeLinks($geeftees, [$gift]);
    }
}
