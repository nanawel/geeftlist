<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use Geeftlist\Model\AbstractModel;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use Narrowspark\HttpStatus\Exception\NotFoundException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\MissingValueException;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Model\Rest\JsonApi\Params;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;
use OliveOil\Core\Service\Rest\JsonApi\ParamsBuilder;
use OliveOil\Core\Service\Rest\RestInterface;

abstract class AbstractHandler implements RestInterface
{
    public const RESOURCE_OBJECT_FIELD_ID             = 'id';

    public const RESOURCE_OBJECT_FIELD_ATTRIBUTES     = 'attributes';

    public const RESOURCE_OBJECT_FIELD_RELATIONSHIPS  = 'relationships';

    public const RESOURCE_OBJECT_FIELD_LINKS          = 'links';

    public const RESOURCE_OBJECT_FIELD_META           = 'meta';

    public const RESOURCE_OBJECT_FIELD_TYPES = [
        self::RESOURCE_OBJECT_FIELD_ID            => 'int',
        self::RESOURCE_OBJECT_FIELD_ATTRIBUTES    => 'array',
        self::RESOURCE_OBJECT_FIELD_RELATIONSHIPS => 'array',
        self::RESOURCE_OBJECT_FIELD_LINKS         => 'array',
        self::RESOURCE_OBJECT_FIELD_META          => 'array',
    ];

    protected \Base $fw;

    protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder;

    protected \OliveOil\Core\Service\GenericFactoryInterface $restServiceFactory;

    protected \OliveOil\Core\Service\Rest\JsonApi\Encoder\EncoderInterface $jsonApiEncoder;

    protected \Geeftlist\Model\RepositoryFactory $modelRepositoryFactory;

    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    protected \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager;

    protected \OliveOil\Core\Helper\Transaction $transactionHelper;

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    protected \Geeftlist\Model\Session\GeefterInterface $geefterSession;

    protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor;

    /** @var string[] */
    protected array $supportedVerbs = Http::VERBS;

    /**
     * @param string $entityType
     */
    public function __construct(
        \Geeftlist\Service\Rest\Context $context,
        protected $entityType
    ) {
        $this->fw = $context->getFw();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->restServiceFactory = $context->getRestServiceFactory();
        $this->jsonApiEncoder = $context->getJsonApiEncoder();
        $this->modelRepositoryFactory = $context->getGeeftlistRepositoryFactory();
        $this->coreFactory = $context->getCoreFactory();
        $this->requestManager = $context->getRequestManager();
        $this->transactionHelper = $context->getTransactionHelper();
        $this->eventManager = $context->getEventService()->getEventManager($this);
        $this->dataValidationErrorProcessor = $context->getDataValidationErrorProcessor();
    }

    /**
     * @inheritDoc
     */
    public function options(RequestInterface $request, ResponseInterface $response): void {
        $response->setCode(HttpStatus::STATUS_OK)
            ->addHeaders([
                'Allow' => $this->getSupportedVerbs()
            ]);
    }

    /**
     * @inheritDoc
     */
    public function get(RequestInterface $request, ResponseInterface $response): void {
        /** @var Params $params */
        $params = $this->newParamsBuilder()
            ->fromRequest($request)
            ->setPrimaryEntityType($this->getEntityType())
            ->build();

        $encoder = $this->getJsonApiEncoder()
            ->initFromParams($params);

        if ($id = $params->getPrimaryEntityId()) {
            $this->beforeGetItem($request, $params);

            $model = $this->getModel($id);

            $response->setCode(HttpStatus::STATUS_OK)
                ->setData($encoder->encodeData($model));
        }
        else {
            $this->beforeGetCollection($request, $params);

            $items = $this->getModelRepository($this->getEntityType())
                ->find($params->getPrimaryCollectionConstraints());

            $response->setCode(HttpStatus::STATUS_OK)
                ->setData($encoder->encodeData($items));
        }
    }

    protected function beforeGetItem(RequestInterface $request, Params $params) {
        $this->getEventManager()->trigger(
            [
                'getItem::before',
                sprintf('getItem::%s::before', $this->getEntityType()),
            ],
            $this,
            [
                'entity_type' => $this->getEntityType(),
                'request' => $request,
                'params' => $params
            ]
        );
    }

    protected function beforeGetCollection(RequestInterface $request, Params $params) {
        $this->getEventManager()->trigger(
            [
                'getCollection::before',
                sprintf('getCollection::%s::before', $this->getEntityType()),
            ],
            $this,
            [
                'entity_type' => $this->getEntityType(),
                'request' => $request,
                'params' => $params
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function head(RequestInterface $request, ResponseInterface $response): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * @inheritDoc
     */
    public function patch(RequestInterface $request, ResponseInterface $response): void {
        /** @var Params $params */
        $params = $this->newParamsBuilder()
            ->fromRequest($request)
            ->setPrimaryEntityType($this->getEntityType())
            ->build();

        if (!$id = $params->getPrimaryEntityId()) {
            throw new MethodNotAllowedException();
        }

        $model = $this->getModel($id);

        $data = $request->getData();
        $this->assertIsResourceObject($data, '', $this->getEntityType(), [self::RESOURCE_OBJECT_FIELD_ATTRIBUTES]);
        $model->addData($data['attributes']);

        $errorAggregator = $model->validate();
        if ($allFieldErrors = $errorAggregator->getFieldErrors()) {
            $response->setCode(HttpStatus::STATUS_UNPROCESSABLE_ENTITY)
                ->setData($this->getJsonApiEncoder()->encodeErrors(
                    $this->dataValidationErrorProcessor->process($errorAggregator, $data)
                ));
        }
        else {
            $this->transactionHelper->execute(function () use ($model, $request, $response): void {
                $this->getModelRepository()->save($model);

                $this->handleRelationshipsCompound($model, $request, $response);
            });

            $getRequest = $this->newRequest(false, $request)
                ->setRouteParams(['id' => $model->getId()]);
            $this->get($getRequest, $response);
        }
    }

    /**
     * @inheritDoc
     */
    public function post(RequestInterface $request, ResponseInterface $response): void {
        // No route param is allowed
        if ($this->filterArgs($request->getRouteParams()) !== []) {
            throw new BadRequestException();
        }

        $model = $this->newModel();

        $data = $request->getData();
        $this->assertIsResourceObject($data, null, $this->getEntityType(), [self::RESOURCE_OBJECT_FIELD_ATTRIBUTES]);
        $model->setData($data['attributes']);

        $errorAggregator = $model->validate();
        if ($allFieldErrors = $errorAggregator->getFieldErrors()) {
            $response->setCode(HttpStatus::STATUS_UNPROCESSABLE_ENTITY)
                ->setData($this->getJsonApiEncoder()->encodeErrors(
                    $this->dataValidationErrorProcessor->process($errorAggregator, $data)
                ));
        }
        else {
            $this->transactionHelper->execute(function () use ($model, $request, $response): void {
                $this->getModelRepository()->save($model);

                $this->handleRelationshipsCompound($model, $request, $response);
            });

            $getRequest = $this->newRequest(false, $request)
                ->setRouteParams(['id' => $model->getId()]);
            $this->get($getRequest, $response);

            // Override status code
            $response->setCode(HttpStatus::STATUS_CREATED);
        }
    }

    /**
     * @inheritDoc
     */
    public function put(RequestInterface $request, ResponseInterface $response): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * @inheritDoc
     */
    public function delete(RequestInterface $request, ResponseInterface $response): void {
        /** @var Params $params */
        $params = $this->newParamsBuilder()
            ->fromRequest($request)
            ->setPrimaryEntityType($this->getEntityType())
            ->build();

        if ($id = $params->getPrimaryEntityId()) {
            $this->getModelRepository()->delete($this->getModel($id));

            $response->setCode(HttpStatus::STATUS_OK)
                ->setData($this->getJsonApiEncoder()->encodeMeta(['deleted_items' => 1]));
        }
        elseif (is_array($params = $request->getQueryParams())) {
            // FIXME Add support for filters/limit/order
            throw new NotImplementedException(__METHOD__);
        }
        else {
            throw new \InvalidArgumentException();
        }
    }

    /*
     * RELATIONSHIPS
     */

    /**
     * Entrypoint for "* /entity/id/relationships/*" requests
     *
     * @inheritDoc
     */
    public function dispatchRelationshipsRequest(RequestInterface $request, ResponseInterface $response): void {
        if (empty($request->getRouteParam('id'))) {
            throw new BadRequestException();
        }

        $rel = $request->getRouteParam('rel');
        if ($handlerMethod = $this->getRelationshipMethod($request->getVerb(), $rel)) {
            $this->{$handlerMethod}($request, $response);
        }
        else {
            throw new NotFoundException();
        }
    }

    /**
     * Handler for "OPTIONS /entity/id/relationships" requests (list all the available relationships)
     * Also called from __call() as a fallback when no "optionsXxxRelationships()" is defined in the current class.
     */
    protected function optionsRelationships(RequestInterface $request, ResponseInterface $response) {
        $relationships = $this->collectRelationships();
        if ($rel = $request->getRouteParam('rel')) {
            if (isset($relationships[$rel])) {
                $response->addHeaders([
                    'Allow' => implode(',', $relationships[$rel])
                ]);
            }
            else {
                throw new NotFoundException();
            }
        }
        else {
            $response->setData($this->getJsonApiEncoder()->encodeMeta([
                'relationships' => array_keys($relationships)
            ]));
        }
    }

    /**
     * Handler for POST of relationships located in a compound document after a POST on an entity
     * @see https://jsonapi.org/format/#crud-updating-resource-relationships
     */
    protected function handleRelationshipsCompound(
        AbstractModel $model,
        RequestInterface $request,
        ResponseInterface $response
    ) {
        if (($relatedEntityId = $model->getId()) && is_array($relationships = $request->getData()['relationships'] ?? false)) {
            foreach ($relationships as $relationshipName => $relationshipData) {
                $subRequest = $this->newRequest(false, $request)
                    ->setVerb($request->getVerb())
                    ->setRouteParams([
                        'id' => $relatedEntityId,
                        'rel' => $relationshipName
                    ])
                    ->setData($relationshipData['data'] ?? []);
                $subResponse = $this->newResponse();

                $this->dispatchRelationshipsRequest($subRequest, $subResponse);
            }
        }
    }

    /**
     * TODO Use cache!
     */
    protected function collectRelationships(): array {
        $relationships = [];
        foreach (get_class_methods($this) as $method) {
            if ($rel = $this->getRelationshipFromMethod(Http::VERBS, $method)) {
                [$relationship, $verb] = $rel;
                if (!isset($relationships[$relationship])) {
                    $relationships[$relationship] = [
                        Http::VERB_OPTIONS
                    ];
                }

                $relationships[$relationship][] = $verb;
            }
        }

        return $relationships;
    }

    /**
     * @param string|string[] $verbs
     * @param string $method
     * @return string[]|false
     */
    protected function getRelationshipFromMethod($verbs, $method): array|false {
        if (!is_array($verbs)) {
            $verbs = [$verbs];
        }
        elseif ($verbs === []) {
            $verbs = Http::VERBS;
        }

        $verbs = array_map('strtolower', $verbs);

        if (preg_match(sprintf('/(%s)(\w+)Relationships/', implode('|', $verbs)), $method, $matches)) {
            return [\OliveOil\camelcase2snakecase($matches[2]), strtoupper($matches[1])];
        }

        return false;
    }

    /**
     * @param string $verb
     * @param string $rel
     * @return string
     */
    protected function getRelationshipMethod($verb, $rel) {
        $method = strtolower($verb) . ucfirst(\OliveOil\snakecase2camelcase($rel)) . 'Relationships';

        // Handle fallback to {verb}Relationships() if no specific handler method has been defined
        // (primarily used for OPTIONS)
        if ($rel && !method_exists($this, $method)) {
            $method = $this->getRelationshipMethod($verb, '');
        }

        return $method;
    }

    /*
     * UTILS
     */

    /**
     * @param int|string $id
     * @param string|null $field
     * @param string|null $entityType
     * @return \Geeftlist\Model\AbstractModel
     * @throws NoSuchEntityException
     */
    protected function getModel($id, $field = null, $entityType = null) {
        /** @var AbstractModel $model */
        $model = $this->getModelRepository($entityType ?: $this->getEntityType())
            ->get($id, $field);

        if (! $model || ! $model->getId()) {
            throw new NoSuchEntityException(sprintf("Unknown entity with ID '%s'", $id));
        }

        return $model;
    }

    /**
     * @param string|null $entityType
     * @return \Geeftlist\Model\AbstractModel
     */
    protected function newModel($entityType = null) {
        /** @var AbstractModel $model */
        $model = $this->getModelRepository($entityType ?: $this->getEntityType())
            ->newModel();

        return $model;
    }

    /**
     * @param bool $initFromGlobals
     * @param RequestInterface|null $related
     * @return \OliveOil\Core\Model\Rest\RequestInterface
     */
    protected function newRequest($initFromGlobals = false, RequestInterface $related = null) {
        /** @var RequestInterface $request */
        $request = $this->requestManager->newRequest($initFromGlobals)
            ->setRelatedRequest($related);

        return $request;
    }

    /**
     * @return \OliveOil\Core\Model\Rest\ResponseInterface
     */
    protected function newResponse() {
        /** @var ResponseInterface $response */
        $response = $this->requestManager->newResponse();

        return $response;
    }

    protected function filterArgs(array $args): array {
        return array_filter($args, static fn($k): bool => ! is_numeric($k), ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param string|array|null $path
     * @param string|null $expectedType
     * @throws \InvalidArgumentException
     * @throws MissingValueException
     */
    protected function assertIsResourceObject(
        array $data,
        $path,
        $expectedType = null,
        array $requiredFields = []
    ) {
        if ($path) {
            $parts = is_array($path) ? $path : explode('/', $path);

            while ($parts) {
                $part = array_shift($parts);
                // Special part identifier to handle multiple occurrences
                if ($part == '*') {
                    foreach ($data as $el) {
                        $this->assertIsResourceObject($el, $parts, $expectedType, $requiredFields);
                    }

                    return;
                }

                if (! isset($data[$part])) {
                    throw new MissingValueException('Invalid input data.');
                }

                $data =& $data[$part];
            }
        }

        if (empty($data['type'])) {
            throw new \InvalidArgumentException('Missing resource type.');
        }

        if ($expectedType && $data['type'] != $expectedType) {
            throw new \InvalidArgumentException(sprintf('Invalid resource type (expected "%s").', $expectedType));
        }

        $missingFields = [];
        $invalidFields = [];
        foreach ($requiredFields as $requiredField) {
            if (empty($data[$requiredField])) {
                $missingFields[] = $requiredField;
            }

            switch (self::RESOURCE_OBJECT_FIELD_TYPES[$requiredField]) {
                case 'int':
                    if (((int) $data[$requiredField]) != $data[$requiredField]) {
                        $invalidFields[] = $requiredField;
                    }

                    break;
                case 'array':
                    if (!is_array($data[$requiredField])) {
                        $invalidFields[] = $requiredField;
                    }

                    break;
                // no default
            }
        }

        if ($missingFields !== []) {
            throw new \InvalidArgumentException(sprintf(
                'Missing required top-level member(s): %s.',
                implode(', ', $missingFields)
            ));
        }

        if ($invalidFields !== []) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid top-level member(s): %s.',
                implode(', ', $invalidFields)
            ));
        }
    }

    /**
     * @param string|null $expectedType
     * @return int
     */
    protected function extractToOneRelationshipsId(array $data, $expectedType = null) {
        $this->assertIsResourceObject($data, null, $expectedType, [self::RESOURCE_OBJECT_FIELD_ID]);

        return $data['id'];
    }

    /**
     * @param string|null $expectedType
     * @return int[]
     */
    protected function extractToManyRelationshipsIds(array $data, $expectedType = null): array {
        $this->assertIsResourceObject($data, '*', $expectedType, [self::RESOURCE_OBJECT_FIELD_ID]);

        return array_column($data, 'id');
    }

    public function __call($name, $arguments) {
        HttpStatus::getReasonException(HttpStatus::STATUS_METHOD_NOT_ALLOWED);
    }

    /**
     * @return \OliveOil\Core\Service\Rest\JsonApi\ParamsBuilder
     */
    public function newParamsBuilder() {
        return $this->coreFactory->make(ParamsBuilder::class);
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getRestServiceFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->restServiceFactory;
    }

    /**
     * @param string|null $entityType
     * @return \OliveOil\Core\Service\Rest\RestInterface
     */
    public function getRestService($entityType = null) {
        if ($entityType === null) {
            $entityType = $this->getEntityType();
        }

        return $this->getRestServiceFactory()->getFromCode($entityType);
    }

    /**
     * @param bool $new Whether to return a new instance or not
     * @return \OliveOil\Core\Service\Rest\JsonApi\Encoder\EncoderInterface
     */
    public function getJsonApiEncoder($new = true) {
        $encoder = $this->jsonApiEncoder;
        if ($new) {
            $encoder = clone $encoder;
        }

        return $encoder;
    }

    /**
     * @param string|null $entityType
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getModelRepository($entityType = null) {
        if ($entityType === null) {
            $entityType = $this->getEntityType();
        }

        return $this->modelRepositoryFactory->getFromCode($entityType);
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }

    /**
     * @return string
     */
    public function getEntityType() {
        return $this->entityType;
    }

    /**
     * @inheritDoc
     */
    public function getSupportedVerbs(array $params = []) {
        return $this->supportedVerbs;
    }
}
