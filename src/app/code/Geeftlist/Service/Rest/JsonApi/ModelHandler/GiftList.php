<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use Geeftlist\Model\Gift;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

/**
 * Class GiftList
 *
 * @method \Geeftlist\Model\GiftList getModel($id, $field = null, $entityType = null)
 */
class GiftList extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_POST,
        Http::VERB_DELETE,
    ];

    public function __construct(
        \Geeftlist\Service\Rest\Context $context,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftHelper,
        $entityType
    ) {
        parent::__construct($context, $entityType);
    }

    /**
     * Handler for "GET /gift/id/relationships/owner"
     */
    protected function getOwnerRelationships(RequestInterface $request, ResponseInterface $response) {
        $giftList = $this->modelRepositoryFactory->resolveGet(\Geeftlist\Model\GiftList::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $geefterRestService */
        $geefterRestService = $this->getRestService(\Geeftlist\Model\Geefter::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setRouteParams(['id' => $giftList->getOwnerId()]);

        $geefterRestService->get($forwardedRequest, $response);
    }

    /**
     * Handler for "PATCH /gift/id/relationships/owner"
     */
    protected function patchOwnerRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $newCreatorId = $this->extractToOneRelationshipsId($request->getData(), \Geeftlist\Model\Geefter::ENTITY_TYPE);

        $subRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $id])
            ->setData([
                'type' => \Geeftlist\Model\GiftList::ENTITY_TYPE,
                'attributes' => [
                    'owner_id' => $newCreatorId
                ]
            ]);

        $this->patch($subRequest, $response);
    }

    /**
     * Handler for "GET /giftList/id/relationships/gifts"
     */
    protected function getGiftsRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $giftRestService */
        $giftRestService = $this->getRestService(\Geeftlist\Model\Gift::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    'giftlists' => ['in' => [$request->getRouteParam('id')]]
                ]
            ]
        ));

        $giftRestService->get($forwardedRequest, $response);
    }

    /**
     * Handler for "PATCH /giftList/id/relationships/gifts"
     */
    protected function patchGiftsRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        /** @var Gift[] $gifts */
        $gifts = $this->getModelRepository(Gift::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Gift::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $this->getModel($id);
        if (! $giftList || ! $giftList->getId()) {
            throw new NoSuchEntityException($id);
        }

        $this->giftListGiftHelper
            ->replaceLinks([$giftList], $gifts);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGiftsRelationships($getRequest, $response);
    }

    /**
     * Handler for "POST /giftList/id/relationships/gifts"
     */
    protected function postGiftsRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        /** @var Gift[] $gifts */
        $gifts = $this->getModelRepository(Gift::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Gift::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $this->getModel($id);
        if (! $giftList || ! $giftList->getId()) {
            throw new NoSuchEntityException($id);
        }

        if (empty($gifts)) {
            throw new \InvalidArgumentException('Empty or unavailable list of gifts provided.');
        }

        $this->giftListGiftHelper
            ->addLinks([$giftList], $gifts);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGiftsRelationships($getRequest, $response);
    }

    /**
     * Handler for "DELETE /giftList/id/relationships/gifts"
     */
    protected function deleteGiftsRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        /** @var Gift[] $gifts */
        $gifts = $this->getModelRepository(Gift::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Gift::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $this->getModel($id);
        if (! $giftList || ! $giftList->getId()) {
            throw new NoSuchEntityException($id);
        }

        if (empty($gifts)) {
            throw new \InvalidArgumentException('Empty or unavailable list of gifts provided.');
        }

        $this->giftListGiftHelper
            ->removeLinks([$giftList], $gifts);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGiftsRelationships($getRequest, $response);
    }
}
