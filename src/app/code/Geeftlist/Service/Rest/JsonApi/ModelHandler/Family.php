<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

class Family extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_POST,
        Http::VERB_DELETE,
    ];

    public function __construct(
        \Geeftlist\Service\Rest\Context $context,
        protected \Geeftlist\Model\Family\Geeftee $familyGeefteeHelper,
        $entityType
    ) {
        parent::__construct($context, $entityType);
    }

    protected function getOwnerRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Model\Family $family */
        $family = $this->getModelRepository(\Geeftlist\Model\Family::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $geefterRestService */
        $geefterRestService = $this->getRestService(Geefter::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(['id' => $family->getOwnerId()]);

        $geefterRestService->get($forwardedRequest, $response);
    }

    protected function getGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $geefteeRestService */
        $geefteeRestService = $this->getRestService(Geeftee::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    'families' => ['in' => [$request->getRouteParam('id')]]
                ]
            ]
        ));

        $geefteeRestService->get($forwardedRequest, $response);
    }

    protected function patchGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $geeftees = $this->getModelRepository(Geeftee::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Geeftee::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Family $family */
        $family = $this->getModel($id);

        $this->getFamilyGeefteeHelper()
            ->replaceLinks([$family], $geeftees);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGeefteesRelationships($getRequest, $response);
    }

    protected function postGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $geeftees = $this->getModelRepository(Geeftee::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Geeftee::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Family $family */
        $family = $this->getModel($id);

        $this->getFamilyGeefteeHelper()
            ->addLinks([$family], $geeftees);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGeefteesRelationships($getRequest, $response);
    }

    protected function deleteGeefteesRelationships(RequestInterface $request, ResponseInterface $response) {
        if (empty($id = $request->getRouteParam('id'))) {
            throw new \InvalidArgumentException('Invalid ID.');
        }

        $geeftees = $this->getModelRepository(Geeftee::ENTITY_TYPE)
            ->find(['filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [[
                    'in' => $this->extractToManyRelationshipsIds($request->getData(), Geeftee::ENTITY_TYPE)
                ]]
            ]]);

        /** @var \Geeftlist\Model\Family $family */
        $family = $this->getModel($id);

        $this->getFamilyGeefteeHelper()
            ->removeLinks([$family], $geeftees);

        // Return response from GET
        $getRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $request->getRouteParam('id')]);
        $this->getGeefteesRelationships($getRequest, $response);
    }

    public function getFamilyGeefteeHelper(): \Geeftlist\Model\Family\Geeftee {
        return $this->familyGeefteeHelper;
    }
}
