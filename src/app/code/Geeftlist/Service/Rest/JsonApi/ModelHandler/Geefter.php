<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

class Geefter extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
    ];

    protected function getGeefteeRelationships(RequestInterface $request, ResponseInterface $response) {
        $geefter = $this->getModelRepository(\Geeftlist\Model\Geefter::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geeftee $geefteeRestService */
        $geefteeRestService = $this->getRestService(\Geeftlist\Model\Geeftee::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $geefter->getGeeftee()->getId()]);

        $geefteeRestService->get($forwardedRequest, $response);
    }
}
