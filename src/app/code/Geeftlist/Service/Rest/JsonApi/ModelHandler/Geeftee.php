<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use Geeftlist\Model\Family;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

class Geeftee extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_POST,
        Http::VERB_DELETE,
    ];

    public function delete(RequestInterface $request, ResponseInterface $response): never {
        throw new MethodNotAllowedException('Cannot delete geeftee.');
    }

    public function getGeefterRelationships(RequestInterface $request, ResponseInterface $response): void {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $this->getModelRepository(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\Geefter $geefterRestService */
        $geefterRestService = $this->getRestService(\Geeftlist\Model\Geefter::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false, $request)
            ->setRouteParams(['id' => $geeftee->getGeefterId()]);

        $geefterRestService->get($forwardedRequest, $response);
    }

    public function getFamiliesRelationships(RequestInterface $request, ResponseInterface $response): void {
        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\Family $familyRestService */
        $familyRestService = $this->getRestService(Family::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false, $request)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    'geeftees' => ['in' => [$request->getRouteParam('id')]]
                ]
            ]
        ));

        $familyRestService->get($forwardedRequest, $response);
    }

    public function getGiftsRelationships(RequestInterface $request, ResponseInterface $response): void {
        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\Gift $giftRestService */
        $giftRestService = $this->getRestService(\Geeftlist\Model\Gift::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false, $request)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    'geeftees' => ['in' => [$request->getRouteParam('id')]]
                ]
            ]
        ));

        $giftRestService->get($forwardedRequest, $response);
    }
}
