<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler;


use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;

/**
 * Class Reservation
 *
 * @method \Geeftlist\Model\Reservation getModel($id, $field = null, $entityType = null)
 */
class Reservation extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_POST,
        Http::VERB_DELETE,
    ];

    protected function getGiftRelationships(RequestInterface $request, ResponseInterface $response) {
        /** @var \Geeftlist\Model\Reservation $reservation */
        $reservation = $this->getModelRepository()
            ->get($request->getRouteParam('id'));

        /** @var \Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler $giftRestService */
        $giftRestService = $this->getRestService(\Geeftlist\Model\Gift::ENTITY_TYPE);

        $forwardedRequest = $this->newRequest(false)->setQueryParams(array_merge_recursive(
            $request->getQueryParams(),
            [
                'filter' => [
                    RepositoryInterface::WILDCARD_ID_FIELD => ['eq' => $reservation->getGiftId()]
                ]
            ]
        ));

        $giftRestService->get($forwardedRequest, $response);
    }
}
