<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelHandler\Discussion;


use Geeftlist\Service\Rest\JsonApi\ModelHandler\AbstractHandler;
use OliveOil\Core\Constants\Http;

/**
 * Class Post
 *
 * @method \Geeftlist\Model\Discussion\Post getModel($id, $field = null, $entityType = null)
 */
class Post extends AbstractHandler
{
    /** @var string[] */
    protected array $supportedVerbs = [
        Http::VERB_OPTIONS,
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_POST,
        Http::VERB_DELETE,
    ];
}
