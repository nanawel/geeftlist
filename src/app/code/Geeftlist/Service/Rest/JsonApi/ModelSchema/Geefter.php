<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


use Geeftlist\Exception\Security\PermissionException;
use Neomerx\JsonApi\Contracts\Factories\FactoryInterface;
use Neomerx\JsonApi\Contracts\Schema\ContextInterface;
use OliveOil\Core\Exception\NoSuchEntityException;

class Geefter extends AbstractSchema
{
    /** @var array */
    protected $visibleAttributes = [
        'geefter_id',
        'email',
        'username'
    ];

    public function __construct(
        FactoryInterface $factory,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Service\Avatar\GeefteeInterface $geefteeAvatarService
    ) {
        parent::__construct($factory, $repositoryFactory);
    }

    public function getType(): string {
        return \Geeftlist\Model\Geefter::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Geefter $resource
     */
    public function getAttributes($resource, ContextInterface $context): iterable {
        $attributes = parent::getAttributes($resource, $context);

        try {
            $geeftee = $resource->getGeeftee();

            // Avatar URL
            $avatarUrl = $this->getGeefteeAvatarService()->getUrl($geeftee);
            // Profile URL
            $profileUrl = $resource->getProfileUrl();
        }
        catch (NoSuchEntityException | PermissionException) {
            $avatarUrl = null;
            $profileUrl = null;
        }

        $attributes['avatar_url'] = $avatarUrl;
        $attributes['profile_url'] = $profileUrl;

        return $attributes;
    }

    /**
     * @param \Geeftlist\Model\Geefter $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'geeftee' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geeftee::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'geefter_id' => [['eq' => $resource->getId()]]
                    ]
                ]
            ]
        ];
    }

    protected function getGeefteeAvatarService(): \Geeftlist\Service\Avatar\GeefteeInterface {
        return $this->geefteeAvatarService;
    }
}
