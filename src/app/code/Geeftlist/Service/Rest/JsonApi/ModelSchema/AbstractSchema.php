<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


use Geeftlist\Exception\Api\Rest\JsonApiException;
use Geeftlist\Model\AbstractModel;
use Neomerx\JsonApi\Contracts\Factories\FactoryInterface;
use Neomerx\JsonApi\Contracts\Schema\ContextInterface;
use Neomerx\JsonApi\Contracts\Schema\IdentifierInterface;
use Neomerx\JsonApi\Schema\BaseSchema;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Service\Rest\JsonApi\EditableContextInterface;
use OliveOil\Core\Service\Rest\JsonApi\Factories\Factory;

/**
 * Class AbstractSchema
 *
 * @method Factory getFactory()
 */
abstract class AbstractSchema extends BaseSchema
{
    public const RELATIONSHIP_CARDINALITY_SINGLE     = 'single';

    public const RELATIONSHIP_CARDINALITY_COLLECTION = 'collection';

    /** @var array|null */
    protected $visibleAttributes;

    public function __construct(
        FactoryInterface $factory,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory
    ) {
        parent::__construct($factory);
    }

    /**
     * @param \Geeftlist\Model\AbstractModel $resource
     */
    public function getId($resource): ?string {
        if (!$resource instanceof AbstractModel) {
            throw new \InvalidArgumentException('Not a valid resource.');
        }

        return $resource->getId();
    }

    /**
     * @param \Geeftlist\Model\AbstractModel $resource
     * @return array
     */
    public function getAttributes($resource, ContextInterface $context): iterable {
        if (!$resource instanceof AbstractModel) {
            throw new \InvalidArgumentException('Not a valid resource.');
        }

        // Return only non-object fields
        return array_filter(
            is_array($this->visibleAttributes)
                ? \OliveOil\array_mask($resource->getData(), $this->visibleAttributes)
                : $resource->getData(),
            static fn($item): bool => !is_object($item)
        );
    }

    /**
     * @param \Geeftlist\Model\AbstractModel $resource
     * @param EditableContextInterface $context
     * @return array
     * @throws JsonApiException
     */
    public function getRelationships($resource, ContextInterface $context): iterable {
        return $this->resolveRelationshipDefinitions(
            $this->getRelationshipDefinitions($resource),
            $resource,
            $context
        );
    }

    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [];
    }

    /**
     * @throws JsonApiException
     */
    protected function resolveRelationshipDefinitions(
        array $relationshipItems,
        AbstractModel $resource,
        EditableContextInterface $context
    ): array {
        $return = [];
        foreach ($relationshipItems as $relationshipName => $relationshipDefinition) {
            $return[$relationshipName] = [
                self::RELATIONSHIP_LINKS_SELF => true,
                self::RELATIONSHIP_LINKS_RELATED => true,
            ];
            try {
                $data = $this->resolveRelationshipDefinition(
                    $relationshipName,
                    $relationshipDefinition,
                    $resource,
                    $context
                );
                if ($data === false) {
                    $return[$relationshipName][self::RELATIONSHIP_META] = [
                        'notice:not_included' => 'Related item(s) have not been retrieved. Use "include" if needed.'
                    ];
                }
                else {
                    $return[$relationshipName][self::RELATIONSHIP_DATA] = $data;
                }
            }
            catch (\Throwable $e) {
                throw new JsonApiException(
                    sprintf(
                        'Error fetching relationship "%s". Available relationships: %s.',
                        $relationshipName,
                        implode(', ', array_keys($relationshipItems))
                    ),
                    null,
                    $e
                );
            }
        }

        return $return;
    }

    /**
     * @param string $relationshipName
     * @param array|callable $relationshipDefinition
     * @return AbstractModel|AbstractModel[]|IdentifierInterface|null|bool
     * @throws JsonApiException
     */
    protected function resolveRelationshipDefinition(
        $relationshipName,
        $relationshipDefinition,
        AbstractModel $resource,
        EditableContextInterface $context
    ) {
//        if (is_callable($relationshipDefinition)) {
//            // FIXME Should allow easier return of object identifiers like with array below
//            return $relationshipDefinition($relationshipName, $resource, $context);
//        }
        if (is_array($relationshipDefinition)) {
            // Relationship data is not requested, only send object identifier
            if (!$context->isIncludedRelationship($relationshipName)) {
                $return = $this->resolveRelationshipResourceIdentifier(
                    $relationshipName,
                    $relationshipDefinition,
                    $context
                );
            }
            // Send relationship object resource
            else {
                $return = $this->resolveRelationshipResource(
                    $relationshipName,
                    $relationshipDefinition,
                    $context
                );
            }

            return $return;
        }

        if ($relationshipDefinition === null) {
            return null;
        }

        throw new JsonApiException('Unsupported definition type: ' . gettype($relationshipDefinition));
    }

    /**
     * @param string $relationshipName
     * @return \Neomerx\JsonApi\Contracts\Schema\IdentifierInterface|bool|null
     * @throws JsonApiException
     */
    protected function resolveRelationshipResourceIdentifier(
        $relationshipName,
        array $definition,
        EditableContextInterface $context,
        array $meta = null
    ) {
        if ($definition['cardinality'] == self::RELATIONSHIP_CARDINALITY_COLLECTION) {
            // Don't return object identifiers for collection items atm
            return false;
        }

        $entityType = $definition['entity_type'];

        if (array_key_exists('constraints', $definition)) {
            $relatedEntity = $this->getModelRepository($entityType)
                ->findOne($definition['constraints']);

            $entityId = $relatedEntity ? $relatedEntity->getId() : null;
        }
        elseif (array_key_exists('entity_id', $definition)) {
            $entityId = $definition['entity_id'];
        }
        else {
            throw new JsonApiException('Invalid relationship definition: missing entity type/ID or constraints.');
        }

        if (!$entityId) {
            return null;
        }

        return $this->getFactory()->createResourceIdentifier(
            $entityType,
            $entityId,
            $meta ?? $definition['meta'] ?? null,
        );
    }

    /**
     * @param string $name
     * @return \OliveOil\Core\Model\AbstractModel|\OliveOil\Core\Model\AbstractModel[]|null
     * @throws JsonApiException
     */
    protected function resolveRelationshipResource(
        $name,
        array $definition,
        EditableContextInterface $context
    ) {
        $constraints = $context->getRelationshipsCollectionConstraints($name);

        // Entity ID is provided, use it to build a filter
        if (!empty($definition['entity_id'])) {
            $constraints = array_merge_recursive(
                $constraints,
                [
                    'filter' => [
                        RepositoryInterface::WILDCARD_ID_FIELD => [['eq' => $definition['entity_id']]]
                    ]
                ]
            );
        }

        if (!empty($definition['constraints'])) {
            $constraints = array_merge_recursive($constraints, $definition['constraints']);
        }

        /** @var RepositoryInterface $repository */
        $repository = $this->getModelRepository($definition['entity_type']);
        return match ($card = ($definition['cardinality'] ?? null)) {
            self::RELATIONSHIP_CARDINALITY_SINGLE => $repository->findOne($constraints),
            self::RELATIONSHIP_CARDINALITY_COLLECTION => $repository->find($constraints),
            default => throw new JsonApiException('Unsupported relationship cardinality: ' . $card),
        };
    }

    /**
     * @param string $entityType
     * @return RepositoryInterface
     */
    protected function getModelRepository($entityType) {
        return $this->getRepositoryFactory()->resolveGet($entityType);
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }
}
