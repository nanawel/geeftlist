<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema\Discussion;


use Geeftlist\Service\Rest\JsonApi\ModelSchema\AbstractSchema;

class Topic extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Discussion\Topic $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'linked_entity' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => $resource->getLinkedEntityType(),
                'entity_id' => $resource->getLinkedEntityId(),
            ],
            'posts' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Discussion\Post::ENTITY_TYPE,
                'entity_id' => $resource->getAuthorId(),
            ]
        ];
    }
}
