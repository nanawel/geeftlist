<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


class Family extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\Family::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Family $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'owner' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getOwnerId(),
            ],
            'geeftees' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Geeftee::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'family' => [['in' => $resource->getId()]]
                    ]
                ]
            ]
        ];
    }
}
