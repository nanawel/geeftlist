<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema\Family;


use Geeftlist\Service\Rest\JsonApi\ModelSchema\AbstractSchema;

class MembershipRequest extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Family\MembershipRequest $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'geeftee' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geeftee::ENTITY_TYPE,
                'entity_id' => $resource->getGeefteeId(),
            ],
            'family' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Family::ENTITY_TYPE,
                'entity_id' => $resource->getFamilyId(),
            ],
            'sponsor' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getSponsorId(),
            ]
        ];
    }
}
