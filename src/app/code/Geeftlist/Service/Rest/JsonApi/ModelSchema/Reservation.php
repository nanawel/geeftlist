<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


class Reservation extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\Reservation::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Reservation $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'geefter' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getGeefterId(),
            ],
            'gift' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'entity_id' => $resource->getGiftId(),
            ]
        ];
    }
}
