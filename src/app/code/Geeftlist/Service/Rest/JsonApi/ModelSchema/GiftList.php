<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


class GiftList extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\GiftList::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\GiftList $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'owner' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getOwnerId(),
            ],
            'gifts' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'gifts' => [['in' => $resource->getId()]]
                    ]
                ]
            ]
        ];
    }
}
