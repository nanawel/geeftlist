<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


use Neomerx\JsonApi\Contracts\Factories\FactoryInterface;
use Neomerx\JsonApi\Contracts\Schema\ContextInterface;

class Gift extends AbstractSchema
{
    public function __construct(
        FactoryInterface $factory,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Service\Gift\Image $giftImageService
    ) {
        parent::__construct($factory, $repositoryFactory);
    }

    public function getType(): string {
        return \Geeftlist\Model\Gift::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Gift $resource
     */
    public function getAttributes($resource, ContextInterface $context): iterable {
        $attributes = parent::getAttributes($resource, $context);

        // Image URL
        $imageUrl = $this->giftImageService->getUrl($resource);
        unset($attributes['image_id']);
        $attributes['image_url'] = $imageUrl;

        // View URL
        $attributes['view_url'] = $resource->getViewUrl();

        return $attributes;
    }

    /**
     * @param \Geeftlist\Model\Gift $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'creator' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getCreatorId(),
            ],
            'geeftees' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Geeftee::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'gift' => [['eq' => $resource->getId()]]
                    ]
                ]
            ],
            'reservations' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Reservation::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'gift_id' => [['eq' => $resource->getId()]]
                    ]
                ]
            ]
        ];
    }
}
