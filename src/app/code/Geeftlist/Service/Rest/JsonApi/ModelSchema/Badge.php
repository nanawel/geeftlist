<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


class Badge extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\Badge::ENTITY_TYPE;
    }
}
