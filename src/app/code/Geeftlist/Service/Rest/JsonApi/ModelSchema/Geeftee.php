<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema;


use Neomerx\JsonApi\Contracts\Factories\FactoryInterface;
use Neomerx\JsonApi\Contracts\Schema\ContextInterface;

class Geeftee extends AbstractSchema
{
    public function __construct(
        FactoryInterface $factory,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Service\Avatar\GeefteeInterface $geefteeAvatarService
    ) {
        parent::__construct($factory, $repositoryFactory);
    }

    public function getType(): string {
        return \Geeftlist\Model\Geeftee::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Geeftee $resource
     */
    public function getAttributes($resource, ContextInterface $context): iterable {
        $attributes = parent::getAttributes($resource, $context);

        // Avatar URL
        $avatarUrl = $this->getGeefteeAvatarService()->getUrl($resource);
        unset($attributes['avatar_path']);
        $attributes['avatar_url'] = $avatarUrl;

        // Profile URL
        $attributes['profile_url'] = $resource->getProfileUrl();

        return $attributes;
    }

    /**
     * @param \Geeftlist\Model\Geeftee $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'geefter' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getGeefterId(),
            ],
            'families' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Family::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'geeftees' => [['in' => [$resource->getId()]]]
                    ]
                ]
            ],
            'gifts' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_COLLECTION,
                'entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
                'constraints' => [
                    'filter' => [
                        'geeftees' => [['in' => [$resource->getId()]]]
                    ]
                ]
            ]
        ];
    }

    protected function getGeefteeAvatarService(): \Geeftlist\Service\Avatar\GeefteeInterface {
        return $this->geefteeAvatarService;
    }
}
