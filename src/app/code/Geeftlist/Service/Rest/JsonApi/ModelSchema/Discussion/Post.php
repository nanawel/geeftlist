<?php

namespace Geeftlist\Service\Rest\JsonApi\ModelSchema\Discussion;


use Geeftlist\Service\Rest\JsonApi\ModelSchema\AbstractSchema;

class Post extends AbstractSchema
{
    public function getType(): string {
        return \Geeftlist\Model\Discussion\Post::ENTITY_TYPE;
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post $resource
     * @return array
     */
    protected function getRelationshipDefinitions(\Geeftlist\Model\AbstractModel $resource): array {
        return [
            'topic' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE,
                'entity_id' => $resource->getTopicId(),
            ],
            'author' => [
                'cardinality' => self::RELATIONSHIP_CARDINALITY_SINGLE,
                'entity_type' => \Geeftlist\Model\Geefter::ENTITY_TYPE,
                'entity_id' => $resource->getAuthorId(),
            ]
        ];
    }
}
