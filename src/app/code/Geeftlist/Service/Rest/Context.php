<?php

namespace Geeftlist\Service\Rest;


class Context
{
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\GenericFactoryInterface $restServiceFactory,
        protected \OliveOil\Core\Service\Rest\JsonApi\Encoder\EncoderInterface $jsonApiEncoder,
        protected \Geeftlist\Model\RepositoryFactory $geeftlistRepositoryFactory,
        protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \OliveOil\Core\Service\Http\RequestManagerInterface $requestManager,
        protected \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor
    ) {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getRestServiceFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->restServiceFactory;
    }

    public function getJsonApiEncoder(): \OliveOil\Core\Service\Rest\JsonApi\Encoder\EncoderInterface {
        return $this->jsonApiEncoder;
    }

    public function getGeeftlistRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->geeftlistRepositoryFactory;
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getRequestManager(): \OliveOil\Core\Service\Http\RequestManagerInterface {
        return $this->requestManager;
    }

    public function getTransactionHelper(): \OliveOil\Core\Helper\Transaction {
        return $this->transactionHelper;
    }

    public function getEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->eventService;
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }

    public function getDataValidationErrorProcessor(): \OliveOil\Core\Helper\Data\Validation\ErrorProcessor {
        return $this->dataValidationErrorProcessor;
    }
}
