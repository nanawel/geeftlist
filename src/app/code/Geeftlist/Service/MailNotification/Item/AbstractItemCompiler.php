<?php

namespace Geeftlist\Service\MailNotification\Item;


use Geeftlist\Model\Notification;
use OliveOil\Core\Exception\NoSuchEntityException;

abstract class AbstractItemCompiler implements ItemCompilerInterface
{
    protected \Base $fw;

    protected \OliveOil\Core\Model\I18n $i18n;

    protected \OliveOil\Core\Service\GenericFactoryInterface $itemFactory;

    protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository;

    protected \OliveOil\Core\Model\RepositoryInterface $giftRepository;

    protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository;

    protected \OliveOil\Core\Model\RepositoryInterface $familyRepository;

    protected \OliveOil\Core\Model\RepositoryInterface $reservationRepository;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        Context $context
    ) {
        $this->fw = $context->getFw();
        $this->i18n = $context->getI18n();
        $this->itemFactory = $context->getItemFactory();
        $this->geefterRepository = $context->getGeefterRepository();
        $this->giftRepository = $context->getGiftRepository();
        $this->geefteeRepository = $context->getGeefteeRepository();
        $this->familyRepository = $context->getFamilyRepository();
        $this->reservationRepository = $context->getReservationRepository();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    /**
     * @param \Geeftlist\Model\Notification[] $notifications
     * @param \Geeftlist\Model\Geefter|null $recipientGeefter
     * @return \Geeftlist\Model\MailNotification\MailData\Item[]
     */
    public function toItems(array $notifications, \Geeftlist\Model\Geefter $recipientGeefter = null): array {
        $items = [];

        $i18n = $recipientGeefter && $recipientGeefter->getI18n() ? $recipientGeefter->getI18n() : $this->i18n;

        /** @var \Geeftlist\Model\MailNotification\MailData\Item $itemPrototype */
        $itemPrototype = $this->itemFactory->make(\Geeftlist\Model\MailNotification\MailData\Item::class);
        foreach ($notifications as $notification) {
            try {
                $getDataMethod = sprintf(
                    '%sToItemData',
                    lcfirst($this->fw->camelcase($notification->getEventName()))
                );
                if (! is_callable([$this, $getDataMethod])) {
                    throw new \Exception('No items provider method found for event ' . $notification->getEventName());
                }

                $item = clone $itemPrototype;
                $item->setData($this->$getDataMethod($notification, $i18n, $recipientGeefter))
                    ->setNotification($notification)
                    ->setGeefter($recipientGeefter)
                    ->setEventName($notification->getEventName());

                $items[] = $item;
            }
            catch (NoSuchEntityException $e) {
                $this->logger->notice(sprintf(
                    'NoSuchEntityException caught when processing notification #%d: %s',
                    $notification->getId(),
                    $e->getMessage()
                ));

                if ($recipientGeefter instanceof \Geeftlist\Model\Geefter) {
                    // Mark notification as failed so there will be no future attempt
                    $notification->updateRecipientStatus($recipientGeefter, Notification::STATUS_FAILED);
                }
            }
            catch (\Throwable $e) {
                $this->logger->error(sprintf(
                    'Exception caught when processing notification #%d: %s',
                    $notification->getId(),
                    $e->getMessage()
                ));
                $this->logger->critical($e);

                if ($recipientGeefter instanceof \Geeftlist\Model\Geefter) {
                    // Mark notification as failed so there will be no future attempt
                    $notification->updateRecipientStatus($recipientGeefter, Notification::STATUS_FAILED);
                }
            }
        }

        return $items;
    }
}
