<?php

namespace Geeftlist\Service\MailNotification\Item;


use OliveOil\Core\Exception\NoSuchEntityException;

class Family extends AbstractItemCompiler
{
    protected function joinToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Family::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a family notification');
        }

        /** @var \Geeftlist\Model\Family $family */
        $family = $this->familyRepository->get($notification->getTargetId());

        if (!$family) {
            throw new NoSuchEntityException(sprintf(
                'Family #%d is not available anymore.',
                $notification->getTargetId()
            ));
        }

        $eventParams = $notification->getEventParams();
        $geeftees = [];
        if (isset($eventParams['geeftee_ids'])) {
            foreach ($eventParams['geeftee_ids'] as $geefteeId) {
                $geeftees[] = $this->geefteeRepository->get($geefteeId);
            }
        }

        // #516 Handle case where geeftees have disappeared after merge
        $geeftees = array_filter($geeftees);
        if ($geeftees === []) {
            throw new NoSuchEntityException('Specified geeftee(s) does/do not exist anymore.');
        }

        return [
            'geeftees' => $geeftees,
            'family' => $family
        ];
    }

    protected function leaveToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Family::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a family notification');
        }

        /** @var \Geeftlist\Model\Family $family */
        $family = $this->familyRepository->get($notification->getTargetId());

        $eventParams = $notification->getEventParams();
        $geeftees = [];
        if (isset($eventParams['geeftee_ids'])) {
            foreach ($eventParams['geeftee_ids'] as $geefteeId) {
                $geeftees[] = $this->geefteeRepository->get($geefteeId);
            }
        }

        // #516 Handle case where geeftees have disappeared after merge
        $geeftees = array_filter($geeftees);
        if ($geeftees === []) {
            throw new NoSuchEntityException('Specified geeftee(s) does/do not exist anymore.');
        }

        return [
            'geeftees' => $geeftees,
            'family' => $family
        ];
    }
}
