<?php

namespace Geeftlist\Service\MailNotification\Item\Discussion;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Service\MailNotification\Item\AbstractItemCompiler;
use OliveOil\Core\Exception\NoSuchEntityException;

class Post extends AbstractItemCompiler
{
    public function __construct(
        \Geeftlist\Service\MailNotification\Item\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $discussionTopicRepository,
        protected \OliveOil\Core\Model\RepositoryInterface $discussionPostRepository,
        protected \Geeftlist\Helper\Url $urlHelper
    ) {
        parent::__construct($context);
    }

    protected function newToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Discussion\Post::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a post notification');
        }

        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $this->getDiscussionPostRepository()->get($notification->getTargetId());

        if (!$post || $post->getStatus() != \Geeftlist\Model\Discussion\Post::STATUS_NORMAL) {
            throw new NoSuchEntityException(sprintf(
                'Post #%d is not available anymore.',
                $notification->getTargetId()
            ));
        }

        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $this->getDiscussionTopicRepository()->get($post->getTopicId());
        if (!$topic) {
            throw new NoSuchEntityException(sprintf(
                'Topic #%d does not exist anymore.',
                $post->getTopicId()
            ));
        }

        /** @var \Geeftlist\Model\Geefter $author */
        $author = $this->geefterRepository->get($post->getAuthorId());
        if (!$author) {
            throw new NoSuchEntityException(sprintf(
                'Geefter #%d does not exist anymore.',
                $post->getAuthorId()
            ));
        }

        /**
         * FIXME Not great, should be in a dedicated class
         * @see \Geeftlist\Helper\Url::getDiscussionTopicUrl()
         */
        switch ($topic->getLinkedEntityType()) {
            case Gift::ENTITY_TYPE:
                /** @var \Geeftlist\Model\Gift $gift */
                $gift = $this->giftRepository->get($topic->getLinkedEntityId());
                if (!$gift || $gift->getStatus() != Status::AVAILABLE) {
                    throw new NoSuchEntityException(sprintf(
                        'Gift #%d is not available anymore.',
                        $topic->getLinkedEntityId()
                    ));
                }

                $topicTitle = $gift->getLabel();
                break;

            default:
                $topicTitle = $topic->getTitle();
        }

        return [
            'topic' => $topic,
            'post' => $post,
            'author' => $author,
            'author_name' => $author->getUsername(),
            'post_url' => $this->getUrlHelper()->getDiscussionPostUrl($post),
            'topic_url' => $this->getUrlHelper()->getDiscussionTopicUrl($topic),
            'topic_title' => $topicTitle
        ];
    }

    public function getDiscussionTopicRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->discussionTopicRepository;
    }

    public function getDiscussionPostRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->discussionPostRepository;
    }

    public function getUrlHelper(): \Geeftlist\Helper\Url {
        return $this->urlHelper;
    }
}
