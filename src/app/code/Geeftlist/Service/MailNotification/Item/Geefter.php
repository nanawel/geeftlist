<?php

namespace Geeftlist\Service\MailNotification\Item;


class Geefter extends AbstractItemCompiler
{
    protected function newToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Geefter::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a geefter notification');
        }

        /** @var \Geeftlist\Model\Geefter $newGeefter */
        $newGeefter = $this->geefterRepository->get($notification->getTargetId());

        /** @var array $eventParams */
        $eventParams = $notification->getEventParams();

        if (!$newGeefter) {
            $this->logger->warning('Invalid geefter ID: ' . $notification->getTargetId());

            return [
                'geefter_name'  => '[invalid]',
                'geefter_email' => '[invalid]',
                'remote_ip'     => $eventParams['remote_ip'] ?? '<unknown IP>',
                'forwarded_for' => $eventParams['forwarded_for'] ?? '<null>'
            ];
        }

        return [
            'geefter_name'  => $newGeefter->getUsername(),
            'geefter_email' => $newGeefter->getEmail(),
            'remote_ip'     => $eventParams['remote_ip'] ?? '<unknown IP>',
            'forwarded_for' => $eventParams['forwarded_for'] ?? '<null>'
        ];
    }

    protected function deleteToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Geefter::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a geefter notification');
        }

        /** @var array $eventParams */
        $eventParams = $notification->getEventParams();

        return [
            'geefter_name'  => $eventParams['username'],
            'geefter_email' => $eventParams['email'],
            'remote_ip'     => $eventParams['remote_ip'] ?? '<unknown IP>',
            'forwarded_for' => $eventParams['forwarded_for'] ?? '<null>'
        ];
    }
}
