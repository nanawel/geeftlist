<?php

namespace Geeftlist\Service\MailNotification\Item;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift\Field\Status;
use OliveOil\Core\Exception\NoSuchEntityException;

class Reservation extends AbstractItemCompiler
{
    protected function newToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ) {
        if ($notification->getTargetType() != \Geeftlist\Model\Reservation::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a reservation notification');
        }

        /** @var \Geeftlist\Model\Reservation $reservation */
        $reservation = $this->reservationRepository->get($notification->getTargetId());

        if (!$reservation) {
            // Can happen if the reservation is canceled between creation and notification time
            throw new NoSuchEntityException(sprintf(
                'Reservation #%d does not exist anymore.',
                $notification->getTargetId()
            ));
        }

        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $this->geefterRepository->get($reservation->getGeefterId());
        if (!$geefter) {
            throw new NoSuchEntityException(sprintf(
                'Geefter #%d does not exist anymore.',
                $reservation->getGeefterId()
            ));
        }

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($reservation->getGiftId());
        if (!$gift || $gift->getStatus() != Status::AVAILABLE) {
            throw new NoSuchEntityException(sprintf(
                'Gift #%d is not available anymore.',
                $reservation->getGiftId()
            ));
        }

        // Note: "geefter" here refers to the *actor* of the action, hence the comparison with
        // the gift's geeftees so that the sentence has the intended meaning.
        // See #578
        $geefteeNames = [];
        foreach ($gift->getGeefteeCollection() as $giftGeeftee) {
            $geefteeNames[] = $geefter->getGeeftee()->getId() == $giftGeeftee->getId()
                ? $i18n->tr('himself/herself')
                : $giftGeeftee->getName();
        }

        // #516 Handle case where geeftees have disappeared
        if ($geefteeNames === []) {
            throw new NoSuchEntityException('Specified geeftee(s) does/do not exist anymore.');
        }

        return [
            'geefter' => $geefter,
            'gift' => $gift,
            'geefter_name' => $geefter->getUsername(),
            'geeftee_names' => $geefteeNames,
            'reservation' => $reservation
        ] + $notification->getEventParams();
    }

    protected function updateToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ) {
        return $this->newToItemData($notification, $i18n);
    }

    protected function deleteToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Reservation::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a reservation notification');
        }

        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $this->geefterRepository->get($notification->getEventParams()['geefter_id']);
        if (!$geefter) {
            throw new NoSuchEntityException(sprintf(
                'Geefter #%d does not exist anymore.',
                $notification->getEventParams()['geefter_id']
            ));
        }

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($notification->getEventParams()['gift_id']);
        if (!$gift || $gift->getStatus() != Status::AVAILABLE) {
            throw new NoSuchEntityException(sprintf(
                'Gift #%d is not available anymore.',
                $notification->getEventParams()['gift_id']
            ));
        }

        // Note: "geefter" here refers to the *actor* of the action, hence the comparison with
        // the gift's geeftees so that the sentence has the intended meaning.
        // See #578
        $geefteeNames = array_map(
            static fn(Geeftee $g) => $g->getId() == $geefter->getGeeftee()->getId()
                ? $i18n->tr('himself/herself')
                : $g->getName(),
            $gift->getGeefteeCollection()->getItems()
        );

        // #516 Handle case where geeftees have disappeared
        if ($geefteeNames === []) {
            throw new NoSuchEntityException('Specified geeftee(s) does/do not exist anymore.');
        }

        return [
            'geefter' => $geefter,
            'gift' => $gift,
            'geefter_name' => $geefter->getUsername(),
            'geeftee_names' => $geefteeNames
        ];
    }
}
