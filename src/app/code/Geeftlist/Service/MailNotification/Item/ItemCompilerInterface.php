<?php

namespace Geeftlist\Service\MailNotification\Item;


interface ItemCompilerInterface
{
    /**
     * @param \Geeftlist\Model\Notification[] $notifications
     * @param \Geeftlist\Model\Geefter|null $recipientGeefter
     * @return \Geeftlist\Model\MailNotification\MailData\Item[]
     */
    public function toItems(array $notifications, \Geeftlist\Model\Geefter $recipientGeefter = null);
}
