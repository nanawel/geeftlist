<?php

namespace Geeftlist\Service\MailNotification\Item;


class Context
{
    public function __construct(protected \Base $fw, protected \OliveOil\Core\Model\I18n $i18n, protected \OliveOil\Core\Service\GenericFactoryInterface $itemFactory, protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository, protected \OliveOil\Core\Model\RepositoryInterface $giftRepository, protected \OliveOil\Core\Model\RepositoryInterface $geefteeRepository, protected \OliveOil\Core\Model\RepositoryInterface $familyRepository, protected \OliveOil\Core\Model\RepositoryInterface $reservationRepository, protected \OliveOil\Core\Service\Log $logService)
    {
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getItemFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->itemFactory;
    }

    public function getGeefterRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefterRepository;
    }

    public function getGiftRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->giftRepository;
    }

    public function getGeefteeRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefteeRepository;
    }

    public function getFamilyRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->familyRepository;
    }

    public function getReservationRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->reservationRepository;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }
}
