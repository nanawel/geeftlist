<?php

namespace Geeftlist\Service\MailNotification\Item;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift\Field\Status;
use OliveOil\Core\Exception\NoSuchEntityException;

class Gift extends AbstractItemCompiler
{
    protected function changeToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Gift::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a gift notification');
        }

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($notification->getTargetId());

        if (!$gift || $gift->getStatus() != Status::AVAILABLE) {
            throw new NoSuchEntityException(sprintf(
                'Gift #%d is not available anymore.',
                $notification->getTargetId()
            ));
        }

        /** @var \Geeftlist\Model\Geefter $creator */
        $creator = $this->geefterRepository->get($gift->getCreatorId());
        if (!$creator) {
            throw new NoSuchEntityException(sprintf('Geefter #%d does not exist anymore.', $gift->getCreatorId()));
        }

        $geefteeNames = array_map(
            static fn(Geeftee $g) => $g->getId() == $gift->getCreator()->getGeeftee()->getId()
                ? $i18n->tr('himself/herself')
                : $g->getName(),
            $gift->getGeefteeCollection()->getItems()
        );

        // #516 Handle case where geeftees have disappeared
        if ($geefteeNames === []) {
            throw new NoSuchEntityException('Specified geeftee(s) does/do not exist anymore.');
        }

        $changedFields = $notification->getEventParams()['changed_fields'];
        $translatableChangedFields = array_map(static fn($changedField): string => sprintf('gift.field||%s', $changedField), $changedFields);

        return [
            'creator_name'      => $creator->getUsername(),
            'geeftee_names'     => $geefteeNames,
            'changed_fields'    => $changedFields,
            'translatable_changed_fields' => $translatableChangedFields,
            'gift'              => $gift
        ];
    }

    protected function addToGeefteesToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        if ($notification->getTargetType() != \Geeftlist\Model\Gift::ENTITY_TYPE) {
            throw new \InvalidArgumentException('Not a gift notification');
        }

        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->giftRepository->get($notification->getTargetId());

        $geeftees = $gift->getGeefteeCollection()->getItems();

        // #516 Handle case where geeftees have disappeared
        if (empty($geeftees)) {
            throw new NoSuchEntityException('Specified geeftee(s) does/do not exist anymore.');
        }

        $geefteeNames = array_map(
            static fn(Geeftee $g) => $g->getId() == $gift->getCreator()->getGeeftee()->getId()
                ? $i18n->tr('himself/herself')
                : $g->getName(),
            $geeftees
        );

        return [
            'creator_name'  => $gift->getCreatorUsername(),
            'geeftees'      => $geeftees,
            'geeftee_names' => $geefteeNames,
            'gift'          => $gift
        ];
    }

    protected function addToMeToItemData(
        \Geeftlist\Model\Notification $notification,
        \OliveOil\Core\Model\I18n $i18n,
        \Geeftlist\Model\Geefter $recipientGeefter = null
    ): array {
        $data = $this->addToGeefteesToItemData($notification, $i18n, $recipientGeefter);

        $data['other_geeftees'] = $data['geeftees'];
        $data['other_geeftee_names'] = [];

        if ($recipientGeefter instanceof \Geeftlist\Model\Geefter) {
            $data['other_geeftees'] = array_filter(
                $data['geeftees'],
                static fn(Geeftee $geeftee): bool => $geeftee->getId() != $recipientGeefter->getGeeftee()->getId()
            );
            $data['other_geeftee_names'] = array_map(
                static fn(Geeftee $g) => $g->getName(),
                $data['other_geeftees']
            );
        }

        return $data;
    }
}
