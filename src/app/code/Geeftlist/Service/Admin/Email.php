<?php

namespace Geeftlist\Service\Admin;


use OliveOil\Core\Model\Email\EmailInterface;

class Email
{
    public function __construct(protected \OliveOil\Core\Model\App\ConfigInterface $appConfig, protected \OliveOil\Core\Service\GenericFactoryInterface $emailFactory, protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService, protected \OliveOil\Core\Model\I18n $i18n, protected \OliveOil\Core\Service\Url\Builder $urlBuilder)
    {
    }

    /**
     * @return EmailInterface
     * @throws \OliveOil\Core\Exception\Mail\InvalidMailTypeException
     */
    public function getPreparedEmail(
        $type = \OliveOil\Core\Model\Email\Template::class,
        $i18n = null
    ) {
        if ($i18n === null) {
            $i18n = clone $this->getI18n();
        }

        return $this->getEmailFactory()->make($type)
            ->setTo($this->getAppConfig()->getValue('EMAIL_ADMIN'))
            ->setI18n($i18n);
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getEmailFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->emailFactory;
    }

    public function getSenderService(): \OliveOil\Core\Service\Email\Sender\SenderInterface {
        return $this->senderService;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\Builder {
        return $this->urlBuilder;
    }
}
