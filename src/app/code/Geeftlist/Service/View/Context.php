<?php

namespace Geeftlist\Service\View;


class Context extends \OliveOil\Core\Service\View\Context
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Model\I18n $i18n,
        \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        \OliveOil\Core\Model\View\LayoutInterface $layout,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \Geeftlist\Helper\Url $urlHelper,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Service\Security $securityService
    ) {
        parent::__construct(
            $fw,
            $appConfig,
            $i18n,
            $urlBuilder,
            $layout,
            $eventService,
            $logService,
            $coreFactory
        );
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }

    public function getUrlHelper(): \Geeftlist\Helper\Url {
        return $this->urlHelper;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }
}
