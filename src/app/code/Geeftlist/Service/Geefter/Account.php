<?php

namespace Geeftlist\Service\Geefter;


use Geeftlist\Exception\Geefter\RegistrationDisabledException;
use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Exception\Security\InvalidTokenException;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Invitation;

class Account
{
    public const SIGNUP_SUCCESS_CONFIRMATION_REQUIRED = 'SIGNUP_SUCCESS_AFTER_CONFIRMATION';
    public const SIGNUP_SUCCESS_NO_CONFIRMATION       = 'SIGNUP_SUCCESS_NO_CONFIRMATION';

    protected \OliveOil\Core\Model\Event\Manager $eventManager;

    public function __construct(
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        protected \Geeftlist\Service\Invitation $invitationService,
        protected \Geeftlist\Service\Geefter\Email $geefterEmailService,
        protected \OliveOil\Core\Helper\Data\Validation\ErrorProcessor $dataValidationErrorProcessor
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    /**
     * @return array [Geefter, \OliveOil\Core\Model\Validation\ErrorAggregatorInterface]
     * @throws RegistrationDisabledException
     */
    public function signup(array $geefterData, ?Invitation $invitation = null, bool $dryRun = false): array {
        if (
            ! $this->appConfig->getValue('GEEFTER_REGISTRATION_ENABLE')
            && !$invitation
        ) {
            throw new RegistrationDisabledException('Geefter registration is not available at the moment.');
        }

        if ($invitation) {
            $this->invitationService->validate($invitation);
        }

        $geefterData = $this->filterSignupData($geefterData);

        /** @var Geefter $geefter */
        $geefter = $this->geefterRepository->newModel()
            ->addData($geefterData);

        $errors = $geefter->validate();
        if ($errors->isEmpty() && !$dryRun) {
            $geefter->generatePasswordToken()
                ->attachInstanceListener('save::after', function () use ($geefter): void {
                    if (!$this->appConfig->getValue('NO_CONFIRMATION_EMAIL')) {
                        $this->geefterEmailService->sendNewAccountEmail($geefter);
                    }
                });
            $this->geefterRepository->save($geefter);

            if ($noConfirmationNeeded = $this->appConfig->getValue('NO_CONFIRMATION_EMAIL')) {
                $geefter->setFlag(self::SIGNUP_SUCCESS_NO_CONFIRMATION);
            }
            else {
                $geefter->setFlag(self::SIGNUP_SUCCESS_CONFIRMATION_REQUIRED);
            }

            $this->eventManager->trigger('signup_success', $this, [
                'geefter'             => $geefter,
                'email'               => $geefterData['email'],
                'confirmation_needed' => !$noConfirmationNeeded,
                'invitation'          => $invitation
            ]);
        }

        return [$geefter, $errors];
    }

    /**
     * @param string $token
     * @return Geefter
     * @throws InvalidTokenException
     */
    public function loadGeefterByToken($token) {
        try {
            /** @var Geefter|null $geefter */
            $geefter = $this->geefterRepository->get($token, 'password_token');
            if (! $geefter) {
                throw new AuthenticationException('No geefter found with such token.');
            }

            return $geefter;
        }
        catch (\Exception $exception) {
            $this->eventManager->trigger('geefter_password_token_failure', $this, [
                'token' => $token
            ]);

            throw new InvalidTokenException(
                'Invalid or expired token. You may want to generate a new one to update your password.',
                0,
                $exception
            );
        }
    }

    protected function filterSignupData(array $geefterData): array {
        return \OliveOil\array_mask($geefterData, [
            'username',
            'email',
        ]);
    }
}
