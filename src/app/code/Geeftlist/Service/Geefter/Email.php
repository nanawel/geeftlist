<?php

namespace Geeftlist\Service\Geefter;


use Geeftlist\Model\Geefter;
use OliveOil\Core\Exception\MailException;
use OliveOil\Core\Model\Email\EmailInterface;

class Email
{
    public const EMAIL_TEMPLATE_NEW_ACCOUNT     = 'geefter/new_account.phtml';

    public const EMAIL_TEMPLATE_RESET_PASSWORD  = 'geefter/reset_password.phtml';

    public const EMAIL_TEMPLATE_CONFIRM_EMAIL   = 'geefter/confirm_email.phtml';

    public const EMAIL_TEMPLATE_UPDATED_EMAIL   = 'geefter/email_updated.phtml';

    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $emailFactory, protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService, protected \OliveOil\Core\Model\I18n $i18n, protected \OliveOil\Core\Service\Url\Builder $urlBuilder)
    {
    }

    /**
     * @return EmailInterface
     * @throws \OliveOil\Core\Exception\Mail\InvalidMailTypeException
     */
    public function getPreparedEmail(
        Geefter $geefter,
        $type = \OliveOil\Core\Model\Email\Template::class,
        $i18n = null
    ) {
        if ($i18n === null && ! ($i18n = $geefter->getI18n())) {
            $i18n = clone $this->i18n;
        }

        return $this->emailFactory->make($type)
            ->setTo($geefter->getEmail())
            ->setI18n($i18n)
            ->setData('recipient', $geefter);
    }

    public function sendNewAccountEmail(Geefter $geefter): static {
        /** @var EmailInterface $email */
        $email = $this->getPreparedEmail($geefter)
            ->setTemplate(self::EMAIL_TEMPLATE_NEW_ACCOUNT)
            ->setUsername($geefter->getUsername())
            ->setEmail($geefter->getEmail())
            ->setConfirmUrl($this->urlBuilder->getUrl(
                'signup/confirm',
                [
                    'token' => $geefter->getPasswordToken(),
                    '_force_scheme' => true
                ]
            ));
        $this->senderService->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        return $this;
    }

    public function sendLostPasswordEmail(Geefter $geefter): static {
        /** @var EmailInterface $email */
        $email = $this->getPreparedEmail($geefter)
            ->setTemplate(self::EMAIL_TEMPLATE_RESET_PASSWORD)
            ->setUsername($geefter->getUsername())
            ->setEmail($geefter->getEmail())
            ->setResetUrl($this->urlBuilder->getUrl(
                'lostpassword/reset',
                [
                    'token' => $geefter->getPasswordToken(),
                    '_force_scheme' => true
                ]
            ));
        $this->senderService->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        return $this;
    }

    public function sendUpdateEmailConfirmationEmail(Geefter $geefter): static {
        /** @var EmailInterface $email */
        $email = $this->getPreparedEmail($geefter)
            ->setTo($geefter->getPendingEmail())
            ->setTemplate(self::EMAIL_TEMPLATE_CONFIRM_EMAIL)
            ->setUsername($geefter->getUsername())
            ->setEmail($geefter->getPendingEmail())
            ->setConfirmUrl($this->urlBuilder->getUrl(
                'account/confirmEmailUpdate',
                [
                    'token' => $geefter->getPendingEmailToken(),
                    '_force_scheme' => true
                ]
            ));
        $this->senderService->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        return $this;
    }

    public function sendEmailUpdatedEmail(Geefter $geefter): static {
        /** @var EmailInterface $email */
        $email = $this->getPreparedEmail($geefter)
            ->setTemplate(self::EMAIL_TEMPLATE_UPDATED_EMAIL)
            ->setUsername($geefter->getUsername());
        $this->senderService->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        return $this;
    }
}
