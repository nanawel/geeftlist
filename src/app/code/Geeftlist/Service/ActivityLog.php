<?php

namespace Geeftlist\Service;


use OliveOil\Core\Helper\DateTime;

class ActivityLog implements ActivityLogInterface
{
    protected \Psr\Log\LoggerInterface $logger;

    protected \Geeftlist\Model\ActivityLog $modelPrototype;

    public function __construct(
        protected readonly \Base $fw,
        protected readonly \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected readonly \OliveOil\Core\Model\RepositoryInterface $activityLogRepository,
        protected readonly \Geeftlist\Model\Session\Http\Geefter $geefterSession,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);
        /** @phpstan-ignore-next-line */
        $this->modelPrototype = $activityLogRepository->newModel();
    }

    /**
     * @inheritDoc
     */
    public function log(array $activityData, $appendContext = true): static {
        try {
            if ($this->isEnabled()) {
                if ($appendContext) {
                    $activityData += $this->getContextData();
                }

                $newModel = clone $this->modelPrototype;
                $newModel->setPayload($activityData);
                $this->activityLogRepository->save($newModel);
            }
        }
        catch (\Throwable $throwable) {
            $this->logger->critical($throwable);
        }

        return $this;
    }

    protected function getContextData(): array {
        $contextData = [
            '_timestamp'      => time(),
            '_date_utc'       => DateTime::getDateSql(),
            '_path'           => $this->fw->get('SERVER.REQUEST_URI'),
            '_user_agent'     => $this->fw->get('SERVER.HTTP_USER_AGENT'),
            '_remote_ip'      => $this->fw->get('SERVER.REMOTE_ADDR'),
            '_forwarded_for'  => $this->fw->get('SERVER.HTTP_X_FORWARDED_FOR')
        ];
        if ($this->geefterSession->isLoggedIn()) {
            $contextData['_logged_in_geefter_id'] = $this->geefterSession->getGeefterId();
        }

        return $contextData;
    }

    public function isEnabled(): bool {
        return (bool) $this->appConfig->getValue('ACTIVITYLOG_ENABLED');
    }
}
