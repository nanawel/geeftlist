<?php

namespace Geeftlist\Service;

use Geeftlist\Model\AbstractModel;
use OliveOil\Core\Exception\UploadException;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

interface AvatarInterface
{
    public const TYPE_GENERATED    = 'generated';

    public const TYPE_USER_DEFINED = 'user';

    /**
     * @param array $params
     * @return string
     */
    public function getUrl(AbstractModel $model, $params = []);

    /**
     * @return string
     */
    public function getPlaceholderUrl();

    /**
     * @param string $inputName
     * @return \OliveOil\Core\Model\Upload\File|null
     * @throws UploadException
     */
    public function upload(AbstractModel $geeftee, $inputName, ErrorAggregatorInterface $errorAggregator);
}
