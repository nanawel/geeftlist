<?php

namespace Geeftlist\Service;


use Geeftlist\Model\MailNotification\EventInterface;
use Geeftlist\Model\ResourceModel\Db\Geefter\Collection as IGeefterCollection;

interface MailNotificationInterface
{
    /**
     * @param string $recipientType
     * @return void
     */
    public function prepareNotifications($recipientType);

    /**
     * @param string|EventInterface $eventName
     * @param object $eventTarget
     * @param \ArrayAccess|array|null $eventParams
     * @return IGeefterCollection|null
     */
    public function getNotifiableGeefters($eventName, $eventTarget, $eventParams = []);
}
