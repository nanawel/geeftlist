<?php

namespace Geeftlist\Service\App;


use OliveOil\Core\Exception\Session\ExpiredException;
use OliveOil\Core\Model\App\Error;

class ErrorHandler extends \OliveOil\Core\Service\App\ErrorHandler
{
    protected function handleErrorHttp(Error $error): bool {
        if ($error->getSourceError() instanceof ExpiredException) {
            $this->sessionManager->getSession()
                ->addErrorMessage('Your session has expired, please log in again.');

            // Only save current URL on GET
            if ($this->requestManager->getRequest()->getVerb() === \OliveOil\Core\Constants\Http::VERB_GET) {
                $this->sessionManager->getSession()
                    ->setCurrentUrlOnExpiredSession($this->urlBuilder->getUrl('', ['_current' => true]));
            }

            $this->responseRedirect->reroute('login');
        }

        return parent::handleErrorHttp($error);
    }

    protected function getReferer(): string {
        $referer = parent::getReferer();

        // Do not return actual referer if it's the redirect controller
        if (str_starts_with((string) $referer, $this->urlBuilder->getUrl('redirect'))) {
            return $this->getRefererFallbackUrl();
        }

        return $referer;
    }
}
