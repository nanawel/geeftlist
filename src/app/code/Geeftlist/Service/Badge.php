<?php

namespace Geeftlist\Service;


use Geeftlist\Exception\BadgeException;
use Geeftlist\Model\AbstractModel;
use OliveOil\Core\Model\RepositoryInterface;

class Badge implements BadgeInterface
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var int[][][] */
    protected $entityBadgeIdsCache = [];

    /** @var array<string, array<int, \Geeftlist\Model\Badge>> */
    protected $badgeCache = [];

    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\RepositoryInterface $badgeRepository,
        protected \Geeftlist\Model\Session\GeefterInterface $geefterSession,
        protected \Geeftlist\Model\Badge\Entity $badgeEntityHelper,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @inheritDoc
     */
    public function getBadges(AbstractModel $model, $language = null): array {
        return $this->getBadgesForEntityId($model->getEntityType(), $model->getId(), $language);
    }

    /**
     * @inheritDoc
     */
    public function getBadgesForEntityId($entityType, $entityId, $language = null): array {
        if (!$language) {
            $language = $this->geefterSession->getLanguage();
        }

        if (!isset($this->entityBadgeIdsCache[$entityType][$entityId])) {
            $badgeIds = $this->badgeRepository->findAllIds([
                'filter' => [
                    'entity' => [
                        ['eq' => [$entityType, $entityId]]
                    ],
                ]
            ]);

            $this->entityBadgeIdsCache[$entityType][$entityId] = $badgeIds;
        }

        foreach ($this->entityBadgeIdsCache[$entityType][$entityId] as $badgeId) {
            if (!isset($this->badgeCache[$language][$badgeId])) {
                /** @var \Geeftlist\Model\Badge|bool $badge */
                $badge = current($this->badgeRepository->find([
                    'join' => [
                        'language' => $language
                    ],
                    'filter' => [
                        RepositoryInterface::WILDCARD_ID_FIELD => [
                            ['eq' => $badgeId]
                        ]
                    ]
                ]));
                if (!$badge) {
                    throw new BadgeException(sprintf(
                        'Unknown badge with ID "%d" and language "%s".',
                        $badgeId,
                        $language
                    ));
                }

                $this->badgeCache[$language][$badgeId] = $badge;
            }
        }

        if (isset($this->badgeCache[$language])) {
            return array_filter($this->badgeCache[$language], fn($badge): bool =>
                /** @var \Geeftlist\Model\Badge $badge */
                in_array($badge->getId(), $this->entityBadgeIdsCache[$entityType][$entityId]));
        }

        return [];
    }

    /**
     * @inheritDoc
     */
    public function getBadgesForEntityType($entityType, $language = null) {
        if (!$language) {
            $language = $this->geefterSession->getLanguage();
        }

        /** @var \Geeftlist\Model\Badge[] $badges */
        $badges = $this->badgeRepository->find([
            'join' => [
                'language' => $language
            ],
            'filter' => [
                'entity_type' => [['eq' => $entityType]],
            ]
        ]);

        return $badges;
    }

    /**
     * @inheritDoc
     */
    public function setBadgeIds(AbstractModel $model, array $badgeIds): static {
        $badgeIds = array_unique($badgeIds);
        $badges = $this->badgeRepository->find([
            'filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [
                    ['in' => $badgeIds]
                ]
            ]
        ]);
        if (count($badgeIds) !== count($badges)) {
            throw new BadgeException('Invalid badges.');
        }

        $this->badgeEntityHelper->replaceLinks([$model], $badges);
        unset($this->entityBadgeIdsCache[$model->getEntityType()][$model->getId()]);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearCache(): static {
        $this->entityBadgeIdsCache = [];
        $this->badgeCache = [];

        return $this;
    }
}
