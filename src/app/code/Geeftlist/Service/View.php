<?php

namespace Geeftlist\Service;

class View extends \OliveOil\Core\Service\View
{
    protected \Geeftlist\Model\Session\GeefterInterface $geefterSession;

    protected \Geeftlist\Helper\Url $urlHelper;

    protected \Geeftlist\Service\Security $securityService;

    public function __construct(
        \Geeftlist\Service\View\Context $context
    ) {
        $this->geefterSession = $context->getGeefterSession();
        $this->urlHelper = $context->getUrlHelper();
        $this->securityService = $context->getSecurityService();
        parent::__construct($context);
    }

    /**
     * Use with caution!
     *
     * @param string $blockName
     * @param string $mime
     * @param array|null $hive
     * @param int $ttl
     * @return string
     */
    public function renderBlockPrivileged($blockName, $mime = 'text/html', array $hive = null, $ttl = 0) {
        return $this->getSecurityService()->callPrivileged(fn() => parent::renderBlock($blockName, $mime, $hive, $ttl));
    }

    /**
     * @return \Geeftlist\Model\Session\GeefterInterface
     */
    public function getGeefterSession() {
        return $this->geefterSession;
    }

    /**
     * @return \Geeftlist\Helper\Url
     */
    public function getUrlHelper() {
        return $this->urlHelper;
    }

    /**
     * @return \Geeftlist\Service\Security
     */
    public function getSecurityService() {
        return $this->securityService;
    }
}
