<?php

namespace Geeftlist\Service;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geefter;

class Permission implements PermissionInterface
{
    /**
     * @param \Geeftlist\Service\Permission\DelegatorInterface[] $delegators
     */
    public function __construct(protected array $delegators)
    {
    }

    /**
     * @param AbstractModel|AbstractModel[] $entities
     * @param string|string[] $actions
     */
    public function isAllowed($entities, $actions): bool {
        if (! is_array($entities)) {
            $entities = [$entities];
        }

        if (! is_array($actions)) {
            $actions = [$actions];
        }

        foreach ($this->getDelegators() as $delegator) {
            if ($delegator->isAllowed($entities, $actions) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param AbstractModel|AbstractModel[] $entities
     * @param string|string[] $actions
     * @param string|null $requesterType
     * @return AbstractModel[]
     */
    public function getAllowed($entities, $actions, $requesterType = Geefter::ENTITY_TYPE): array {
        if (! is_array($entities)) {
            $entities = [$entities];
        }

        $allowed = [];
        foreach ($this->getDelegators() as $delegator) {
            foreach ($delegator->getAllowed($entities, $actions) as $allowedRequester) {
                if ($requesterType === null || $requesterType === $allowedRequester->getEntityType()) {
                    $allowed[$allowedRequester->getEntityType() . $allowedRequester->getId()] = $allowedRequester;
                }
            }
        }

        return array_values($allowed);
    }

    /**
     * @return Permission\DelegatorInterface[]
     */
    public function getDelegators(): array {
        return $this->delegators;
    }
}
