<?php

namespace Geeftlist\Service\Permission;


use Geeftlist\Model\AbstractModel;
use OliveOil\Core\Exception\MissingImplementationException;
use OliveOil\Core\Exception\System\TypeException;
use OliveOil\Core\Exception\SystemException;

class Geefter
{
    public function __construct(
        /** @var \Geeftlist\Service\Permission\Geefter\DelegatorInterface[] */
        protected array $entityDelegators
    ) {
    }

    /**
     * @param AbstractModel[]|AbstractModel $entities
     * @param string|string[] $actions
     * @throws SystemException
     */
    public function isAllowed(\Geeftlist\Model\Geefter $geefter, $entities, $actions): bool {
        if (!is_array($entities)) {
            $entities = [$entities];
        }

        if (!is_array($actions)) {
            $actions = [$actions];
        }

        $actions = array_unique($actions);

        $entitiesByType = [];
        foreach ($entities as $entity) {
            $entitiesByType[$entity->getEntityType()][] = $entity;
        }

        foreach ($entitiesByType as $type => $entities) {
            if (!$this->getEntityDelegator($type)->isAllowed($geefter, $entities, $actions)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string|string[] $actions
     * @return \Geeftlist\Model\Geefter[]
     * @throws SystemException
     */
    public function getAllowed(AbstractModel $entity, $actions) {
        if (!is_array($actions)) {
            $actions = [$actions];
        }

        return $this->getEntityDelegator($entity->getEntityType())
            ->getAllowed([$entity], $actions);
    }

    /**
     * @param string $entityType
     * @return Geefter\DelegatorInterface
     * @throws SystemException
     */
    protected function getEntityDelegator($entityType) {
        if (!isset($this->entityDelegators[$entityType])) {
            throw new MissingImplementationException(sprintf("No delegator found for entity type '%s'.", $entityType));
        }

        if (!$this->entityDelegators[$entityType] instanceof \Geeftlist\Service\Permission\Geefter\DelegatorInterface) {
            throw new TypeException(sprintf(
                'Invalid delegator found for entity type %s: %s does not implement %s.',
                $entityType,
                $this->entityDelegators[$entityType]::class,
                \Geeftlist\Service\Permission\Geefter\DelegatorInterface::class
            ));
        }

        return $this->entityDelegators[$entityType];
    }
}
