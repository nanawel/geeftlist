<?php

namespace Geeftlist\Service\Permission\Geefter;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geefter;

interface DelegatorInterface
{
    /**
     * @param AbstractModel[] $entities
     * @param string[] $actions
     * @return bool
     */
    public function isAllowed(Geefter $geefter, array $entities, $actions);

    /**
     * @param AbstractModel[] $entities
     * @param string|string[] $actions
     * @return Geefter[]
     */
    public function getAllowed(array $entities, $actions);
}
