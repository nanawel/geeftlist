<?php

namespace Geeftlist\Service\Permission;


use Geeftlist\Model\AbstractModel;
use OliveOil\Core\Exception\SystemException;

class GeefterSession implements \Geeftlist\Service\Permission\DelegatorInterface
{
    public function __construct(protected \Geeftlist\Model\Session\GeefterInterface $geefterSession, protected \Geeftlist\Service\Permission\Geefter $geefterPermissionService)
    {
    }

    /**
     * @param AbstractModel[] $entities
     * @param string[] $actions
     * @throws SystemException
     */
    public function isAllowed(array $entities, $actions): bool {
        if ($geefter = $this->getGeefterSession()->getGeefter()) {
            foreach ($entities as $entity) {
                if ($this->getGeefterPermissionService()->isAllowed($geefter, $entity, $actions) === false) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param AbstractModel[] $entities
     * @param string|string[] $actions
     * @return AbstractModel[]
     * @throws SystemException
     */
    public function getAllowed(array $entities, $actions): array {
        $allowedGeeftersByEntity = [];
        foreach ($entities as $entity) {
            $allowedGeeftersByEntity[$entity->getId()]
                = $this->getGeefterPermissionService()->getAllowed($entity, $actions);
        }

        if (count($entities) == 1) {
            return current($allowedGeeftersByEntity);
        }

        // Eliminate duplicate entries & return only geefters allowed for *all* specified entities
        return array_intersect_key(...$allowedGeeftersByEntity);
    }

    public function getGeefterSession(): \Geeftlist\Model\Session\GeefterInterface {
        return $this->geefterSession;
    }

    public function getGeefterPermissionService(): \Geeftlist\Service\Permission\Geefter {
        return $this->geefterPermissionService;
    }
}
