<?php

namespace Geeftlist\Service\Permission;


use Geeftlist\Model\AbstractModel;

interface DelegatorInterface
{
    /**
     * @param AbstractModel[] $entities
     * @param string[] $actions
     * @return bool|null
     */
    public function isAllowed(array $entities, $actions);

    /**
     * @param AbstractModel[] $entities
     * @param string|string[] $actions
     * @return AbstractModel[]
     */
    public function getAllowed(array $entities, $actions);
}
