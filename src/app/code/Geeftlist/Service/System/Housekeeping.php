<?php
namespace Geeftlist\Service\System;


use Geeftlist\Model\Activity;
use Geeftlist\Model\Cms\Block;
use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\Share\Entity\Rule;
use OliveOil\Core\Helper\DateTime;
use function OliveOil\snakecase2camelcase;

class Housekeeping
{
    public function __construct(protected \Geeftlist\Model\RepositoryFactory $repositoryFactory)
    {
    }

    public function pruneAllBrokenEntityLinks(): void {
        $entityTypes = [
            Activity::ENTITY_TYPE,
            Topic::ENTITY_TYPE,
            Rule::ENTITY_TYPE
        ];
        foreach ($entityTypes as $entityType) {
            $this->pruneBrokenEntityLinks($entityType);
        }
    }

    public function pruneBrokenEntityLinks(string $entityType): void {
        $methodName = 'pruneBroken' . ucfirst(
            snakecase2camelcase(str_replace('/', '_', $entityType))
        );
        if (is_callable([$this, $methodName])) {
            $this->$methodName();
        }
        else {
            throw new \InvalidArgumentException('Unsupported entity type: ' . $entityType);
        }
    }

    protected function pruneBrokenActivity() {
        $startTime = new \DateTime();
        $existingEntityIds = $this->loadAllEntityIds();

        /** @var \OliveOil\Core\Model\RepositoryInterface $repository */
        $repository = $this->getRepositoryFactory()->resolveGet(Activity::ENTITY_TYPE);

        foreach ($existingEntityIds as $entityType => $entityIds) {
            $repository->deleteAll([
                'filter' => [
                    'main_table.object_type' => [['eq' => $entityType]],
                    'main_table.object_id' => [['nin' => $entityIds]],
                    'main_table.created_at' => [['lt' => DateTime::getDateSql($startTime)]]
                ]
            ]);
        }
    }

    protected function pruneBrokenShareEntityRule() {
        $startTime = new \DateTime();
        $existingEntityIds = $this->loadAllEntityIds();

        /** @var \OliveOil\Core\Model\RepositoryInterface $repository */
        $repository = $this->getRepositoryFactory()->resolveGet(Rule::ENTITY_TYPE);

        foreach ($existingEntityIds as $entityType => $entityIds) {
            $repository->deleteAll([
                'filter' => [
                    'main_table.target_entity_type' => [['eq' => $entityType]],
                    'main_table.target_entity_id' => [['nin' => $entityIds]],
                    'main_table.created_at' => [['lt' => DateTime::getDateSql($startTime)]]
                ]
            ]);
        }
    }

    protected function pruneBrokenDiscussionTopic() {
        $startTime = new \DateTime();
        $existingEntityIds = $this->loadAllEntityIds();

        /** @var \OliveOil\Core\Model\RepositoryInterface $repository */
        $repository = $this->getRepositoryFactory()->resolveGet(Topic::ENTITY_TYPE);

        foreach ($existingEntityIds as $entityType => $entityIds) {
            $repository->deleteAll([
                'filter' => [
                    'main_table.linked_entity_type' => [['eq' => $entityType]],
                    'main_table.linked_entity_id' => [['nin' => $entityIds]],
                    'main_table.created_at' => [['lt' => DateTime::getDateSql($startTime)]]
                ]
            ]);
        }
    }

    protected function loadAllEntityIds(): array {
        $entityTypes = [
            Activity::ENTITY_TYPE,
            Block::ENTITY_TYPE,
            Post::ENTITY_TYPE,
            Topic::ENTITY_TYPE,
            Family::ENTITY_TYPE,
            Geeftee::ENTITY_TYPE,
            Geefter::ENTITY_TYPE,
            Gift::ENTITY_TYPE,
            Reservation::ENTITY_TYPE
        ];

        $entityIds = [];
        foreach ($entityTypes as $entityType) {
            /** @var \OliveOil\Core\Model\RepositoryInterface $repository */
            $repository = $this->getRepositoryFactory()->resolveGet($entityType);

            $entityIds[$entityType] = $repository->findAllIds();
        }

        return $entityIds;
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }
}
