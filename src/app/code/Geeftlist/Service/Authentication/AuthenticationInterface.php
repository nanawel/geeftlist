<?php

namespace Geeftlist\Service\Authentication;


use Geeftlist\Exception\Security\AuthenticationException;

interface AuthenticationInterface
{
    /**
     * @return object Authenticated subject
     * @throws AuthenticationException
     */
    public function authenticate(mixed $args);

    /**
     * @param object $subject
     * @return bool
     * @throws AuthenticationException
     */
    public function authenticateSubject($subject, mixed $args);
}
