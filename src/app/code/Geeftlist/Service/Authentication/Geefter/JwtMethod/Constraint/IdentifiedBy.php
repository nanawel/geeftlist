<?php

namespace Geeftlist\Service\Authentication\Geefter\JwtMethod\Constraint;

use Geeftlist\Model\Geefter;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Validation\Constraint;
use Lcobucci\JWT\Validation\ConstraintViolation;

final class IdentifiedBy implements Constraint
{
    protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository;

    protected \OliveOil\Core\Service\Crypt $cryptService;

    public function __construct(
        \Geeftlist\Service\Authentication\Geefter\Context $context,
        protected \Lcobucci\JWT\Signer\Key $signingKey
    ) {
        $this->geefterRepository = $context->getGeefterRepository();
        $this->cryptService = $context->getCryptService();
    }

    public function assert(Token $token): void {
        if (!$token instanceof Token\Plain) {
            throw new ConstraintViolation('Not a valid token.');
        }

        if (!$jti = $token->claims()->get('jti')) {
            throw new ConstraintViolation('Missing jti.');
        }

        if (!str_contains((string) $jti, ':')) {
            throw new ConstraintViolation('Invalid jti.');
        }

        if (!$geefterId = $token->claims()->get('geefter_id')) {
            throw new ConstraintViolation('Missing geefter_id.');
        }

        [$hash, $time] = explode(':', (string) $jti);

        /** @var Geefter $geefter */
        $geefter = $this->geefterRepository->get($geefterId);

        if (
            !$this->cryptService->verify(
                $geefter->getApiKey() . $this->signingKey->contents(),
                (string) $hash
            )
        ) {
            throw new ConstraintViolation('Illegal jti.');
        }
    }
}
