<?php

namespace Geeftlist\Service\Authentication\Geefter;


use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;
use Lcobucci\JWT\Token\Plain;
use OliveOil\Core\Exception\NotImplementedException;

class JwtMethod extends AbstractMethod
{
    public function __construct(
        \Geeftlist\Service\Authentication\Geefter\Context $context,
        protected \Lcobucci\JWT\Configuration $configuration,
        protected \Lcobucci\JWT\Builder $builderPrototype
    ) {
        parent::__construct($context);
    }

    /**
     * @param mixed $args
     * @return Geefter Authenticated geefter
     * @throws AuthenticationException
     * @throws NotImplementedException
     */
    public function authenticate($args) {
        if (is_string($args)) {
            $geefter = $this->getGeefter($args);
        }
        elseif (is_array($args)) {
            throw new NotImplementedException('Array support is not available.');
        }
        else {
            throw new \InvalidArgumentException('Invalid authentication data format.');
        }

        if (! $geefter || ! $geefter->getId()) {
            throw new AuthenticationException('Invalid credentials.');
        }

        return $geefter;
    }

    /**
     * @param Geefter $subject
     * @param mixed $args
     * @throws AuthenticationException
     */
    public function authenticateSubject($subject, $args): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * @return string
     */
    public function generateToken(Geefter $geefter, array $claims = []) {
        $builder = $this->getNewBuilder();
        $builder->withClaim('geefter_id', $geefter->getId())
            ->identifiedBy($this->getTokenId($geefter));

        foreach ($claims as $name => $value) {
            $builder->withClaim($name, $value);
        }

        return $builder->getToken(
            $this->getConfiguration()->signer(),
            $this->getConfiguration()->signingKey()
        )->toString();
    }

    /**
     * @param string $token
     * @return \Geeftlist\Model\Geefter
     * @throws AuthenticationException
     */
    public function getGeefter($token) {
        $claims = $this->doAuthenticate($token);

        if (!isset($claims['geefter_id'])) {
            throw new AuthenticationException('No such claim "geefter_id".');
        }

        /** @var Geefter $geefter */
        $geefter = $this->getGeefterRepository()->get((string) $claims['geefter_id']);

        return $geefter;
    }

    /**
     * @param string $token
     * @throws AuthenticationException
     */
    protected function doAuthenticate($token): array {
        if (empty($token)) {
            throw new \InvalidArgumentException('Empty token.');
        }

        $config = $this->getConfiguration();
        $jwt = $config->parser()->parse($token);
        try {
            if (! $config->validator()->validate($jwt, ...$config->validationConstraints())) {
                throw new AuthenticationException('Data validation failed.');
            }
        }
        catch (AuthenticationException $authenticationException) {
            throw new AuthenticationException('Invalid credentials.', 0, $authenticationException);
        }

        if (!$jwt instanceof Plain) {
            throw new AuthenticationException('Invalid credentials.');
        }

        return $jwt->claims()->all();
    }

    protected function getTokenId(Geefter $geefter): string {
        if (! $geefter->getApiKey()) {
            $geefter->regenerateApiKey();
            $this->getGeefterRepository()->save($geefter);
        }

        return $this->getCryptService()->hash(
            $geefter->getApiKey() . $this->getConfiguration()->signingKey()->contents()
        ) . ':' . time();
    }

    protected function getNewBuilder() {
        return clone $this->getBuilderPrototype();
    }

    public function getConfiguration(): \Lcobucci\JWT\Configuration {
        return $this->configuration;
    }

    public function getBuilderPrototype(): \Lcobucci\JWT\Builder {
        return $this->builderPrototype;
    }
}
