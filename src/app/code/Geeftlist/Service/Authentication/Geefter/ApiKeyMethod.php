<?php

namespace Geeftlist\Service\Authentication\Geefter;


use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;

class ApiKeyMethod extends AbstractMethod
{
    /**
     * @param string|array $args
     * @return Geefter Authenticated geefter
     * @throws AuthenticationException
     */
    public function authenticate($args) {
        $apiKey = $this->extractAuthData($args);
        if ($apiKey === '' || $apiKey === '0') {
            throw new \InvalidArgumentException('Missing API key.');
        }

        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $this->getGeefterRepository()->get($apiKey, 'api_key');
        if (! $geefter || ! $geefter->getId()) {
            throw new AuthenticationException('Invalid credentials.');
        }

        return $geefter;
    }

    /**
     * @param Geefter $subject
     * @param string $args
     * @return bool
     * @throws AuthenticationException
     */
    public function authenticateSubject($subject, $args) {
        $apiKey = $this->extractAuthData($args);
        if ($apiKey === '' || $apiKey === '0') {
            throw new \InvalidArgumentException('Missing API key.');
        }

        // Force loading original geefter's data from DB
        $origSubject = $this->getGeefterRepository()->get($subject->getId());

        return $this->doAuthenticateSubject($origSubject, $apiKey);
    }

    /**
     * @param Geefter $subject
     * @param string $apiKey
     * @return bool
     * @throws AuthenticationException
     */
    protected function doAuthenticateSubject($subject, $apiKey) {
        if (hash_equals($subject->getApiKey(), $apiKey)) {
            return true;
        }

        throw new AuthenticationException('Invalid credentials.');
    }

    protected function extractAuthData($args): string {
        if (is_string($args)) {
            $apiKey = $args;
        }
        elseif (is_array($args)) {
            if (!is_scalar($args['api_key'] ?? '')) {
                throw new \InvalidArgumentException('Invalid authentication data format.');
            }

            $apiKey = $args['api_key'] ?? '';
        }
        else {
            throw new \InvalidArgumentException('Invalid authentication data format.');
        }

        return trim($apiKey);
    }
}
