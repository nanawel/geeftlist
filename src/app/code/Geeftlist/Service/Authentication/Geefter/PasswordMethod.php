<?php

namespace Geeftlist\Service\Authentication\Geefter;


use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;

class PasswordMethod extends AbstractMethod
{
    /**
     * @param mixed $args
     * @return Geefter Authenticated geefter
     * @throws AuthenticationException
     */
    public function authenticate($args) {
        $authData = $this->extractAuthData($args);

        if (empty($authData['email']) || empty($authData['password'])) {
            throw new \InvalidArgumentException('Missing email and/or password.');
        }

        /** @var \Geeftlist\Model\Geefter $geefter */
        $geefter = $this->getGeefterRepository()->get($authData['email'], 'email');

        if (! $geefter || ! $geefter->getId()) {
            throw new AuthenticationException('Invalid credentials.');
        }

        $this->doAuthenticateSubject($geefter, (string) $authData['password']);

        return $geefter;
    }

    /**
     * @param Geefter $subject
     * @param mixed $args
     * @return bool
     * @throws AuthenticationException
     */
    public function authenticateSubject($subject, $args) {
        $authData = $this->extractAuthData($args);
        if (empty($authData['password'])) {
            throw new \InvalidArgumentException('Missing password.');
        }

        // Force loading original geefter's data from DB
        /** @var Geefter $origSubject */
        $origSubject = $this->getGeefterRepository()->get($subject->getId());

        return $this->doAuthenticateSubject($origSubject, $authData['password']);
    }

    /**
     * @param Geefter $subject
     * @param string $password
     * @return bool
     * @throws AuthenticationException
     */
    protected function doAuthenticateSubject(Geefter $subject, string $password) {
        if ($this->getCryptService()->verify($password, (string) $subject->getPasswordHash())) {
            return true;
        }

        throw new AuthenticationException('Invalid credentials.');
    }

    protected function extractAuthData($args): array {
        if (is_string($args)) {
            $string = base64_decode($args);
            if (!is_string($string) || !str_contains($string, ':')) {
                throw new \InvalidArgumentException('Invalid authentication data format.');
            }

            [$email, $password] = explode(':', $string);
        }
        elseif (is_array($args)) {
            if (!is_scalar($args['email'] ?? '') || !is_scalar($args['password'] ?? '')) {
                throw new \InvalidArgumentException('Invalid authentication data format.');
            }

            $email = $args['email'] ?? '';
            $password = $args['password'] ?? '';
        }
        else {
            throw new \InvalidArgumentException('Invalid authentication data format.');
        }

        return [
            'email' => trim($email),
            'password' => trim($password)
        ];
    }
}
