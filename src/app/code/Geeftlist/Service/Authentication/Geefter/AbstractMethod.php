<?php

namespace Geeftlist\Service\Authentication\Geefter;


use Geeftlist\Service\Authentication\AuthenticationInterface;

abstract class AbstractMethod implements AuthenticationInterface
{
    protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository;

    protected \OliveOil\Core\Service\Crypt $cryptService;

    public function __construct(
        \Geeftlist\Service\Authentication\Geefter\Context $context
    ) {
        $this->geefterRepository = $context->getGeefterRepository();
        $this->cryptService = $context->getCryptService();
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefterRepository() {
        return $this->geefterRepository;
    }

    /**
     * @return \OliveOil\Core\Service\Crypt
     */
    public function getCryptService() {
        return $this->cryptService;
    }
}
