<?php

namespace Geeftlist\Service\Authentication\Geefter;


class Context
{
    /**
     * Context constructor.
     *
     * The $geefterRepository used here must not use cache because we need to force reloading models from DB
     * to compare value during authentication (password, API key, etc.)
     */
    public function __construct(
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        // This instance must *not* use cache!
        protected \OliveOil\Core\Service\Crypt $cryptService
    ) {
    }

    public function getGeefterRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefterRepository;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }
}
