<?php

namespace Geeftlist\Service\Gift;


use Geeftlist\Model\Gift;
use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Exception\UploadException;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;

class Image
{
    public const IMAGE_TYPE_FULL      = 'full';

    public const IMAGE_TYPE_THUMBNAIL = 'tn';

    public const IMAGE_TYPE_TMP       = 'tmp';

    public const IMAGE_TYPES = [
        self::IMAGE_TYPE_FULL,
        self::IMAGE_TYPE_THUMBNAIL,
        self::IMAGE_TYPE_TMP,
    ];

    public const IMAGE_ID_LENGTH = 32;

    public const IMAGE_ID_PATTERN = '/^([0-9a-f]{32})$/';

    public const IMAGE_ID_FILENAME_PATTERN = '/^([0-9a-f]{32})/';

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /**
     * @param string $imageBasePath
     * @param string $imageUrlBasePath
     * @param string $placeholderUrl
     * @param int $imageDirPermissions
     */
    public function __construct(
        protected \Base $fw,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Service\Crypt $cryptService,
        protected \Geeftlist\Service\Security $securityService,
        protected \OliveOil\Core\Service\UploadInterface $uploadService,
        \OliveOil\Core\Service\Log $logService,
        protected \OliveOil\Core\Helper\Image $imageHelper,
        protected $placeholderUrl,
        protected $imageBasePath,
        protected $imageUrlBasePath,
        protected array $imageTypesConfig,
        protected $imageDirPermissions = 0775
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @param string $type
     * @param array $params
     * @return string
     */
    public function getUrl(Gift $gift, $type = self::IMAGE_TYPE_FULL, $params = []) {
        if (!$type) {
            $type = self::IMAGE_TYPE_FULL;
        }

        try {
            if ($imageId = $gift->getImageId()) {
                if ($type !== self::IMAGE_TYPE_FULL) {
                    $this->generateImage($imageId, self::IMAGE_TYPE_FULL, $type);
                }

                if ($url = $this->getUrlByImageId($imageId, $type)) {
                    return $url;
                }
            }
        }
        catch (\Throwable $throwable) {
            $this->getLogger()->warning(sprintf(
                "Cannot get image URL for gift #%d: %s",
                $gift->getId(),
                $throwable->getMessage()
            ), ['exception' => $throwable]);
        }

        return $this->getPlaceholderUrl();
    }

    /**
     * @param string $imageId
     * @param string $type
     * @return string|null
     */
    public function getUrlByImageId($imageId, $type, $params = []) {
        if ($imageId && $type) {
            return $this->getUrlBuilder()->getUrl($this->getImageUrlPath($imageId, $type), $params);
        }

        return null;
    }

    /**
     * @param string $inputName
     * @return \OliveOil\Core\Model\Upload\File
     * @throws UploadException
     */
    public function upload($inputName, ErrorAggregatorInterface $errorAggregator) {
        $imageId = $this->makeImageId();

        $tmpImagePath = $this->getImagePath($imageId, self::IMAGE_TYPE_TMP);
        $file = $this->getUploadService()->upload(
            $inputName,
            dirname($tmpImagePath),
            $errorAggregator,
            ['filename' => basename($tmpImagePath)]
        );

        try {
            $tmpImagePath = $this->generateImageByPath(
                $file->getData('final_path'),
                $imageId,
                self::IMAGE_TYPE_TMP,
                true
            );
            // Delete original file (if the extension was not .jpg)
            if ($file->getData('final_path') != $tmpImagePath) {
                unlink($file->getData('final_path'));
            }

            $file->setData('image_id', $imageId)
                ->setData('image_path', $tmpImagePath);
        }
        catch (\Throwable $throwable) {
            throw new UploadException('Could not save image.', 0, $throwable);
        }

        return $file;
    }

    /**
     * @param string $tmpImageId
     * @throws \InvalidArgumentException
     * @throws FilesystemException
     */
    public function apply(Gift $gift, $tmpImageId): void {
        switch (true) {
            case $this->validate($tmpImageId, self::IMAGE_TYPE_TMP):
                $this->generateImage($tmpImageId, self::IMAGE_TYPE_TMP, self::IMAGE_TYPE_FULL);

                if (!$this->deleteImage($tmpImageId, self::IMAGE_TYPE_TMP)) {
                    $this->getLogger()->warning(sprintf('Could not delete file with ID %s', $tmpImageId));
                }
                // no break

            case $this->validate($tmpImageId, self::IMAGE_TYPE_FULL):   // In case of a duplicated gift
                $gift->setImageId($tmpImageId);
                break;

            default:
                $this->getLogger()->warning(sprintf(
                    'Tried to apply an invalid image "%s" on gift #%d.',
                    $tmpImageId,
                    $gift->getId() ?: '<none>'
                ));
                throw new \InvalidArgumentException('Invalid image ID.');
        }
    }

    /**
     * @param string $imageId
     * @param string $fromType
     * @param string $toType
     * @param bool $overwrite
     */
    public function generateImage($imageId, $fromType, $toType, $overwrite = false): string {
        return $this->generateImageByPath(
            $this->getImagePath($imageId, $fromType),
            $imageId,
            $toType,
            $overwrite
        );
    }

    /**
     * @param string $sourceImagePath
     * @param string $imageId
     * @param string $outputType
     * @param bool $overwrite
     */
    protected function generateImageByPath($sourceImagePath, $imageId, $outputType, $overwrite = false): string {
        $outputImagePath = $this->getImagePath($imageId, $outputType);
        if ($overwrite || ! $this->fileExists($imageId, $outputType)) {
            $this->getImageHelper()->resize(
                $sourceImagePath,
                $this->getImagePath($imageId, $outputType),
                $this->getImageTypesConfig($outputType, 'width'),
                $this->getImageTypesConfig($outputType, 'height'),
                $this->getImageTypesConfig($outputType, 'format'),
                $this->getImageTypesConfig($outputType, 'quality')
            );
        }

        return $outputImagePath;
    }

    /**
     * @see \Geeftlist\Service\Gift\Image::validate
     *
     * @param bool $mustNotExist
     */
    public function makeImageId($mustNotExist = true): string {
        $n = 0;
        do {
            $imageId = $this->getCryptService()->generateToken(self::IMAGE_ID_LENGTH);
            $path = $this->getImagePath($imageId, self::IMAGE_TYPE_FULL);
        }
        while (++$n < 100 && (!$mustNotExist || $this->fileExists($path, self::IMAGE_TYPE_FULL)));

        if (!$path && $n == 100) {
            throw new \RuntimeException('Could not find available image ID.');
        }

        return $imageId;
    }

    /**
     * @param string $imageId
     * @param string $expectedType
     */
    public function validate($imageId, $expectedType): bool {
        if (!$imageId) {
            return false;
        }

        return strlen($imageId)
            && $this->matchImageIdFormat($imageId)
            && $this->fileExists($imageId, $expectedType);
    }

    /**
     * @param string $imageId
     */
    public function matchImageIdFormat($imageId): bool {
        return (bool) preg_match(self::IMAGE_ID_PATTERN, $imageId);
    }

    /**
     * @param string $imageId
     * @param string $type
     */
    public function fileExists($imageId, $type): bool {
        return is_file($this->getImagePath($imageId, $type));
    }

    /**
     * @param string $imagePath
     * @param string $type
     */
    public function deleteImage($imagePath, $type): bool {
        return @unlink($this->getImagePath($imagePath, $type));
    }

    /**
     * @param string $imageId
     * @param string $type
     */
    public function getImagePath($imageId, $type): string {
        if (!$imageId) {
            throw new \InvalidArgumentException('Image ID must be specified.');
        }

        if (!$type) {
            throw new \InvalidArgumentException('Image type must be specified.');
        }

        $path = sprintf(
            '%s/%s/%s',
            substr($imageId, 0, 2),
            substr($imageId, 2, 2),
            $this->getImageTypesConfig($type, 'filename_callback')($imageId, $type)
        );

        return $this->getImageBasePath() . DIRECTORY_SEPARATOR . $path;
    }

    /**
     * @param string $imageId
     * @param string $type
     */
    public function getImageUrlPath($imageId, $type): string {
        if (!$imageId) {
            throw new \InvalidArgumentException('Image ID must be specified.');
        }

        if (!$type) {
            throw new \InvalidArgumentException('Image type must be specified.');
        }

        $path = sprintf(
            '%s/%s/%s',
            substr($imageId, 0, 2),
            substr($imageId, 2, 2),
            $this->getImageTypesConfig($type, 'filename_callback')($imageId, $type)
        );

        return $this->getImageUrlBasePath() . DIRECTORY_SEPARATOR . $path;
    }

    public function getImageIdFromFile($filepath) {
        if (preg_match(self::IMAGE_ID_FILENAME_PATTERN, (string) $filepath, $matches)) {
            return $matches[1];
        }

        return null;
    }

    public function getFw(): \Base {
        return $this->fw;
    }

    public function getAppConfig(): \OliveOil\Core\Model\App\ConfigInterface {
        return $this->appConfig;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\BuilderInterface {
        return $this->urlBuilder;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }

    public function getImageHelper(): \OliveOil\Core\Helper\Image {
        return $this->imageHelper;
    }

    public function getUploadService(): \OliveOil\Core\Service\UploadInterface {
        return $this->uploadService;
    }

    /**
     * @return string
     */
    public function getImageBasePath() {
        return $this->imageBasePath;
    }

    /**
     * @return string
     */
    public function getImageUrlBasePath() {
        return $this->imageUrlBasePath;
    }

    /**
     * @return string
     */
    public function getPlaceholderUrl() {
        return $this->placeholderUrl;
    }

    /**
     * @param string|null $type
     * @param string|null $key
     * @return array|mixed
     */
    public function getImageTypesConfig($type, $key = null) {
        if ($type === null) {
            return $this->imageTypesConfig;
        }

        if ($key === null) {
            if (!isset($this->imageTypesConfig[$type])) {
                throw new \InvalidArgumentException(sprintf('Invalid config requested: %s', $type));
            }

            return $this->imageTypesConfig[$type];
        }

        if (!isset($this->imageTypesConfig[$type][$key])) {
            throw new \InvalidArgumentException(sprintf('Invalid config requested: %s::%s', $type, $key));
        }

        return $this->imageTypesConfig[$type][$key];
    }

    /**
     * @return int
     */
    public function getImageDirPermissions() {
        return $this->imageDirPermissions;
    }
}
