<?php

namespace Geeftlist\Service\Gift;


use Geeftlist\Exception\GiftException;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use OliveOil\Core\Exception\MailException;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\Email\EmailInterface;

class Email
{
    public const EMAIL_TEMPLATE_ARCHIVING_REQUEST    = 'gift/archiving_request.phtml';

    public const EMAIL_TEMPLATE_REPORT_CREATOR       = 'gift/report/creator.phtml';

    public const EMAIL_TEMPLATE_REPORT_ADMINISTRATOR = 'admin/report/gift.phtml';

    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \OliveOil\Core\Service\GenericFactoryInterface $emailFactory,
        protected \OliveOil\Core\Service\Email\Sender\SenderInterface $senderService,
        protected \OliveOil\Core\Model\I18n $i18n,
        protected \OliveOil\Core\Service\Url\Builder $urlBuilder,
        protected \Geeftlist\Service\Admin\Email $adminEmailService,
        \OliveOil\Core\Service\EventInterface $eventService
    ) {
        $this->eventManager = $eventService->getEventManager($this);
    }

    /**
     * @return EmailInterface
     * @throws \OliveOil\Core\Exception\Mail\InvalidMailTypeException
     */
    public function getPreparedEmail(
        Geefter $geefter,
        $type = \OliveOil\Core\Model\Email\Template::class,
        $i18n = null
    ) {
        if ($i18n === null && ! ($i18n = $geefter->getI18n())) {
            $i18n = clone $this->getI18n();
        }

        return $this->getEmailFactory()->make($type)
            ->setTo($geefter->getEmail())
            ->setI18n($i18n)
            ->setData('recipient', $geefter);
    }

    /**
     * @return $this
     * @throws MailException
     * @throws \OliveOil\Core\Exception\Mail\InvalidMailTypeException
     */
    public function sendArchivingRequest(Gift $gift, Geefter $requester): static {
        if (!$creator = $gift->getCreator()) {
            throw new GiftException(sprintf(
                'Cannot send an archiving request for gift #%d: no creator set.',
                $gift->getId()
            ));
        }

        /** @var EmailInterface $email */
        $email = $this->getPreparedEmail($creator)
            ->setTemplate(self::EMAIL_TEMPLATE_ARCHIVING_REQUEST)
            ->setGift($gift)
            ->setRequester($requester)
            ->setData('geefteeNames', array_column($gift->getGeefteeSummary(), 'name'))
            ->setData(
                'archiveUrl',
                $this->getUrlBuilder()->getUrl('gift/confirmArchive', ['gift_id' => $gift->getId()])
            )
            ->setData(
                'deleteUrl',
                $this->getUrlBuilder()->getUrl('gift/confirmDelete', ['gift_id' => $gift->getId()])
            );
        $this->getSenderService()->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        $this->getEventManager()->trigger('sendGiftArchivingRequest::after', $this, [
            'gift' => $gift,
            'requester' => $requester
        ]);

        return $this;
    }

    /**
     * @param string $message
     * @return $this
     * @throws MailException
     * @throws \OliveOil\Core\Exception\Mail\InvalidMailTypeException
     */
    public function sendReportToCreator(Gift $gift, Geefter $requester, $message): static {
        if (!$creator = $gift->getCreator()) {
            throw new GiftException(sprintf(
                'Cannot send an archiving request for gift #%d: no creator set.',
                $gift->getId()
            ));
        }

        /** @var EmailInterface $email */
        $email = $this->getPreparedEmail($creator)
            ->setReplyTo($requester->getEmail())
            ->setTemplate(self::EMAIL_TEMPLATE_REPORT_CREATOR)
            ->setGift($gift)
            ->setRequester($requester)
            ->setData('geefteeNames', array_column($gift->getGeefteeSummary(), 'name'))
            ->setData('reportDate', DateTime::getDate())
            ->setMessage($message);
        $this->getSenderService()->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        $this->getEventManager()->trigger('sendGiftReportToCreator::after', $this, [
            'gift' => $gift,
            'requester' => $requester,
            'message' => $message
        ]);

        return $this;
    }

    /**
     * @param string $message
     * @return $this
     * @throws MailException
     * @throws \OliveOil\Core\Exception\Mail\InvalidMailTypeException
     */
    public function sendReportToAdministrator(Gift $gift, Geefter $requester, $message): static {
        /** @var EmailInterface $email */
        $email = $this->getAdminEmailService()->getPreparedEmail()
            ->setReplyTo($requester->getEmail())
            ->setTemplate(self::EMAIL_TEMPLATE_REPORT_ADMINISTRATOR)
            ->setGift($gift)
            ->setRequester($requester)
            ->setData('geefteeNames', array_column($gift->getGeefteeSummary(), 'name'))
            ->setData('reportDate', DateTime::getDate())
            ->setMessage($message);
        $this->getSenderService()->send($email);

        if ($err = $email->getError()) {
            throw new MailException('Could not send the email.');
        }

        $this->getEventManager()->trigger('sendGiftReportToAdministrator::after', $this, [
            'gift' => $gift,
            'requester' => $requester,
            'message' => $message
        ]);

        return $this;
    }

    public function getEmailFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->emailFactory;
    }

    public function getSenderService(): \OliveOil\Core\Service\Email\Sender\SenderInterface {
        return $this->senderService;
    }

    public function getI18n(): \OliveOil\Core\Model\I18n {
        return $this->i18n;
    }

    public function getUrlBuilder(): \OliveOil\Core\Service\Url\Builder {
        return $this->urlBuilder;
    }

    public function getAdminEmailService(): \Geeftlist\Service\Admin\Email {
        return $this->adminEmailService;
    }

    /**
     * @return \OliveOil\Core\Model\Event\Manager
     */
    public function getEventManager() {
        return $this->eventManager;
    }
}
