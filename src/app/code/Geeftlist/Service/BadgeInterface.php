<?php

namespace Geeftlist\Service;

use Geeftlist\Model\AbstractModel;

interface BadgeInterface
{
    /**
     * @param string|null $language
     * @return \Geeftlist\Model\Badge[]
     */
    public function getBadges(AbstractModel $model, $language = null);

    /**
     * @param string $entityType
     * @param string $entityId
     * @param string|null $language
     * @return \Geeftlist\Model\Badge[]
     */
    public function getBadgesForEntityId($entityType, $entityId, $language = null);

    /**
     * @param string $entityType
     * @param string|null $language
     * @return \Geeftlist\Model\Badge[]
     */
    public function getBadgesForEntityType($entityType, $language = null);

    /**
     * @param int[] $badgeIds
     * @return $this
     */
    public function setBadgeIds(AbstractModel $model, array $badgeIds);
}
