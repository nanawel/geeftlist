<?php

namespace Geeftlist\Service;


class Security
{
    public function __construct(
        protected \OliveOil\Core\Service\RegistryInterface $registry,
        protected \OliveOil\Core\Service\EventInterface $securityEventService,
        protected string $registryFlag
    ) {
    }

    /**
     * @param callable|array|string $callable
     * @param array $args
     * @return mixed
     */
    public function callPrivileged($callable, $args = []) {
        $this->enterDisabledRestrictionContext();

        try {
            return call_user_func_array($callable, $args);
        }
        finally {
            $this->exitDisabledRestrictionContext();
        }
    }

    public function disableRestrictions(): void {
        $this->enterDisabledRestrictionContext();
    }

    public function enableRestrictions(): void {
        $this->exitDisabledRestrictionContext();
    }

    public function isRestrictionEnabled(): bool {
        return (! $this->registry->exists($this->registryFlag))
            || (! $this->registry->get($this->registryFlag));
    }

    public function getDisabledRestrictionLevel() {
        $currentLevel = $this->getSecurityEventService()->areListenersDisabled()
            ? 1
            : 0;
        if ($this->registry->exists($this->registryFlag)) {
            $currentLevel = max(
                $currentLevel,
                (int) $this->registry->get($this->registryFlag)
            );
        }

        return $currentLevel;
    }

    public function resetRestrictionLevel(): static {
        $this->registry->set($this->registryFlag, $this->getSecurityEventService()->areListenersDisabled() ? 1 : 0);

        return $this;
    }

    protected function enterDisabledRestrictionContext(): static {
        $currentLevel = $this->getDisabledRestrictionLevel();
        $this->registry->set($this->registryFlag, ++$currentLevel);

        $this->getSecurityEventService()->disableListeners();

        return $this;
    }

    protected function exitDisabledRestrictionContext(): static {
        $currentLevel = $this->getDisabledRestrictionLevel();
        $this->registry->set($this->registryFlag, max(--$currentLevel, 0));
        if ($currentLevel == 0) {
            $this->getSecurityEventService()->enableListeners();
        }

        return $this;
    }

    /**
     * Caution: Used for UT
     *
     * @return \OliveOil\Core\Service\EventInterface
     */
    public function getSecurityEventService(): \OliveOil\Core\Service\EventInterface {
        return $this->securityEventService;
    }
}
