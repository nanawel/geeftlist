<?php

namespace Geeftlist\Service;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\MailNotification\EventInterface;
use Geeftlist\Model\Notification;
use Geeftlist\Model\ResourceModel\Db\Geefter\Collection as IGeefterCollection;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\TransportObject;

class MailNotification implements MailNotificationInterface
{
    public const EVENT_PREPARE_NOTIFICATIONS   = 'prepareEmailNotifications';

    public const EVENT_GET_NOTIFIABLE_GEEFTERS = 'getNotifiableGeefters';

    /** @var \OliveOil\Core\Model\Event\Manager */
    protected $eventManager;

    public function __construct(
        protected \OliveOil\Core\Model\RepositoryInterface $notificationRepository,
        protected \OliveOil\Core\Service\EventInterface $eventService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $eventFactory,
        protected \Geeftlist\Model\ResourceFactory $resourceModelFactory
    ) {
        $this->eventManager = $this->eventService->getEventManager($this);
    }

    /**
     * Retrieve all "created" notifications and trigger the EVENT_PREPARE_NOTIFICATIONS event
     * for each one so that specialized observers can prepare or mark notifications accordingly
     * (ex: merge linked events, or delete invalid ones) before collecting the recipients.
     *
     * Workflow: CREATED => READY
     *
     * @inheritDoc
     */
    public function prepareNotifications($recipientType): void {
        /** @var Notification[] $createdNotifications */
        $createdNotifications = $this->notificationRepository->find([
            'filter' => [
                'status' => [['eq' => Notification::STATUS_CREATED]],
                'type' => [['eq' => Notification::TYPE_EMAIL]],
                'recipient_type' => [['eq' => $recipientType]],
            ],
            'sort_order' => [RepositoryInterface::WILDCARD_ID_FIELD => SORT_ASC]
        ]);

        $transportObject = new TransportObject([
            'notifications'              => $createdNotifications,
            'notifications_for_deletion' => []
        ]);

        // Delegate preparation to specialized observers
        $this->eventManager->trigger(
            self::EVENT_PREPARE_NOTIFICATIONS,
            $this,
            ['transport_object' => $transportObject]
        );

        // Delete notifications
        $deletedNotificationIds = [];

        /** @var Notification[] $notificationsForDeletion */
        $notificationsForDeletion = $transportObject->getData('notifications_for_deletion');
        foreach ($notificationsForDeletion as $notificationForDeletion) {
            $this->notificationRepository->delete($notificationForDeletion);
            $deletedNotificationIds[] = $notificationForDeletion->getId();
        }

        $createdNotifications = $transportObject->getData('notifications');
        foreach ($createdNotifications as $createdNotification) {
            // Only save entity if it has not been deleted in the previous loop
            if (!in_array($createdNotification->getId(), $deletedNotificationIds)) {
                $createdNotification->setStatus(Notification::STATUS_READY);
                $this->notificationRepository->save($createdNotification);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getNotifiableGeefters($eventName, $eventTarget, $eventParams = []) {
        if (!is_string($eventName)) {
            throw new \InvalidArgumentException('Invalid $eventName parameter.');
        }

        $eventName = sprintf('%s::%s', $eventName, self::EVENT_GET_NOTIFIABLE_GEEFTERS);
        if (!is_object($eventTarget)) {
            throw new \InvalidArgumentException('$eventTarget must be an object');
        }

        /** @var EventInterface $event */
        $event = $this->eventFactory->make(
            \Geeftlist\Model\MailNotification\EventInterface::class,
            [
                'name' => $eventName,
                'target' => $eventTarget,
                'params' => $eventParams
            ]
        );

        if (! ($geefterCollection = $event->getParam('geefter_collection')) instanceof IGeefterCollection) {
            $geefterCollection = $this->resourceModelFactory->resolveMakeCollection(Geefter::ENTITY_TYPE);
            $event->setParam('geefter_collection', $geefterCollection);
        }

        $generalEvent = clone $event;
        $generalEvent->setName(self::EVENT_GET_NOTIFIABLE_GEEFTERS);
        $this->eventService
            ->getEventManager($eventTarget)->triggerEvent($generalEvent);

        $this->eventService
            ->getEventManager($eventTarget)->triggerEvent($event);

        // Prevent sending mail notification if no filters have been added to the collection
        if (
            !$geefterCollection->getFlag('filters_applied')
            && !($eventParams['_allow_broadcast'] ?? false)
        ) {
            return null;
        }

        return $event->getParam('geefter_collection');
    }
}
