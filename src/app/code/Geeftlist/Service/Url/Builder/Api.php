<?php

namespace Geeftlist\Service\Url\Builder;


use OliveOil\Core\Service\Url\Builder;

class Api extends Builder
{
    public function autocompletePath(string $path): string {
        // Do not alter $path here
        return $path;
    }

    protected function buildParamString(array $params = []): string {
        $unhandledParams = array_diff_key($params, ['id' => null]);
        if ($unhandledParams !== []) {
            $this->logger->warning('Unhandled params detected: ' . implode(', ', array_keys($params)));
        }

        return $params['id'] ?? '';
    }
}
