<?php

namespace Geeftlist\Service;

interface ActivityLogInterface
{
    /**
     * @return $this
     */
    public function log(array $activityData, $appendContext = true);

    /**
     * @return bool
     */
    public function isEnabled();
}
