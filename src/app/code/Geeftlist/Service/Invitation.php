<?php

namespace Geeftlist\Service;


use Geeftlist\Exception\Invitation\UnavailableException;
use Geeftlist\Exception\InvitationException;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\Http\RequestInterface;

class Invitation
{
    public const INVITATION_TOKEN_URL_PARAM = 'invitation_token';

    public function __construct(
        protected \OliveOil\Core\Service\Url\BuilderInterface $urlBuilder,
        protected \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        protected \OliveOil\Core\Model\RepositoryInterface $invitationRepository,
        protected \OliveOil\Core\Service\Crypt $cryptService
    ) {
    }

    public function generate(Geefter $asGeefter): \Geeftlist\Model\Invitation {
        $invitationToken = $this->cryptService->generateToken(
            $this->appConfig->getValue('INVITATION_TOKEN_LENGTH', 64)
        );

        $expirationDate = DateTime::getDate()
            ->add(new \DateInterval('PT' . $this->appConfig->getValue('INVITATION_TOKEN_TTL', 86400) . 'S'));

        /** @var \Geeftlist\Model\Invitation $invitation */
        $invitation = $this->invitationRepository->newModel();
        $invitation->setCreatorId($asGeefter->getId())
            ->setToken($invitationToken)
            ->setExpiresAt(DateTime::getDateSql($expirationDate));

        $this->invitationRepository->save($invitation);

        return $invitation;
    }

    public function getSignupLink(\Geeftlist\Model\Invitation $invitation): string {
        return $this->urlBuilder->getUrl(
            'signup',
            ['_query' => [self::INVITATION_TOKEN_URL_PARAM => $invitation->getToken()]]
        );
    }

    public function generateSignupLink(Geefter $asGeefter): string {
        return $this->getSignupLink($this->generate($asGeefter));
    }

    public function isEnabled(): bool {
        return (bool) $this->appConfig->getValue('INVITATION_ENABLED', false);
    }

    /**
     * @throws UnavailableException
     */
    public function assertEnabled(): void {
        if (!$this->isEnabled()) {
            throw new UnavailableException('Sorry, invitations are disabled on this server.');
        }
    }

    /**
     * @throws InvitationException
     */
    public function validate(\Geeftlist\Model\Invitation $invitation): \Geeftlist\Model\Invitation {
        return $this->validateToken($invitation->getToken());
    }

    /**
     * @throws InvitationException
     */
    public function validateTokenFromRequest(RequestInterface $request): ?\Geeftlist\Model\Invitation {
        if ($token = (string) $request->getQueryParam(self::INVITATION_TOKEN_URL_PARAM)) {
            return $this->validateToken($token);
        }

        return null;
    }

    /**
     * @throws InvitationException
     */
    public function validateToken(string $token): \Geeftlist\Model\Invitation {
        $this->assertEnabled();

        if (empty($token)) {
            throw new InvitationException('Invalid invitation token provided.');
        }

        /** @var \Geeftlist\Model\Invitation|null $invitation */
        $invitation = $this->invitationRepository->get($token, 'token');
        if (!$invitation) {
            throw new InvitationException('Invalid invitation token provided.');
        }

        $now = new \DateTimeImmutable();
        $expirationDate = DateTime::getDate($invitation->getExpiresAt());
        if ($now > $expirationDate) {
            throw new InvitationException('This invitation token is expired.');
        }

        if ($invitation->getUsedAt() || $invitation->getGeefterId()) {
            throw new InvitationException('This invitation has already been used.');
        }

        return $invitation;
    }
}
