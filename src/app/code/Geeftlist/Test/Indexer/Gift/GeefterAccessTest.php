<?php

namespace Geeftlist\Test\Indexer\Gift;


use Geeftlist\Indexer\Gift\GeefterAccess;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group indexers
 */
class GeefterAccessTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Indexer\Gift\GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        $stubIndexerResource = $this
            ->getMockBuilder(\Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $this->getContainer()->make(
            GeefterAccess::class, [
            'indexerResource' => $stubIndexerResource
        ]);
    }

//    public function testReindexAll() {
//        $this->markTestSkipped('Not implemented');
//    }

    public function testReindexEntities(): void {
        $cut = $this->getCut();

        $gifts = [];
        $giftCount = 5;
        for ($i = 0; $i < $giftCount; ++$i) {
            $gifts[] = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
            ]])->save();
        }

        /** @var MockObject $stubResource */
        $stubResource = $cut->getIndexerResource();
        $stubResource->method('reindexEntities')
            ->willReturnCallback(function($entities) use ($gifts): void {
                $this->assertEquals($gifts, $entities);
            });

        $this->assertNotEmpty($gifts);
        $cut->reindexEntities($gifts);
    }

    public function testReindexEntity(): void {
        $cut = $this->getCut();

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();

        /** @var MockObject $stubResource */
        $stubResource = $cut->getIndexerResource();
        $stubResource->method('reindexEntity')
            ->willReturnCallback(function($entity) use ($gift): void {
                $this->assertEquals($gift, $entity);
            });

        $cut->reindexEntity($gift);
    }
}
