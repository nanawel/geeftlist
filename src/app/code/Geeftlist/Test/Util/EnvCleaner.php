<?php

namespace Geeftlist\Test\Util;

class EnvCleaner extends \OliveOil\Core\Test\Util\EnvCleaner
{
    public function endTest(\PHPUnit\Framework\Test $test, $time): void {
        if (is_callable([$test, 'getEventService'])) {
            $test->getEventService()->disableListeners('*');
        }

        if (is_callable([$test, 'getGeefterSession'])) {
            $test->getGeefterSession()->invalidate();
        }

        if (is_callable([$test, 'getView'])) {
            $test->getView()->clearData();
            $test->getView()->getLayout()->clear();
        }

        try {
            parent::endTest($test, $time);
        }
        finally {
            if (is_callable([$test, 'getEventService'])) {
                $test->getEventService()->enableListeners('*');
            }
        }
    }
}
