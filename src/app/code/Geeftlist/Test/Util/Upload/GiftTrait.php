<?php

namespace Geeftlist\Test\Util\Upload;


use Geeftlist\Service\Gift\Image;
use Geeftlist\Test\Util\UploadTrait;
use Symfony\Component\Filesystem\Filesystem;

trait GiftTrait
{
    use UploadTrait;

    public static $TEST_IMAGE_ID         = 'deadbeefdeadbeefdeadbeefdeadbeef';

    public static $TEST_IMAGE_BASE_PATH  = 'de/ad/deadbeefdeadbeefdeadbeefdeadbeef';

    /**
     * @return Image
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getGiftImageService() {
        return $this->getContainer()->get(Image::class);
    }

    protected function prepareTestImage($imageId, string $imageFile = 'gift.jpg', $type = Image::IMAGE_TYPE_FULL) {
        $imagePath = $this->getGiftImageService()->getImagePath($imageId, $type);
        $imageDir = dirname((string) $imagePath);

        $fs = $this->getContainer()->get(Filesystem::class);
        $fs->remove($imageDir);
        $fs->mkdir($imageDir, 0777);
        $fs->copy(static::$TEST_IMAGE_SOURCE_DIR . $imageFile, $imagePath, true);
    }
}