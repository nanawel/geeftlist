<?php

namespace Geeftlist\Test\Util;


use Geeftlist\Test\Constants;
use OliveOil\Core\Exception\Filesystem\FileNotFoundException;

class SampleData implements Constants
{
    /** @var \Faker\Generator */
    protected $faker;

    /**
     * @param string $sampleDataFolder
     */
    public function __construct(protected \Geeftlist\Model\ModelFactory $modelFactory, protected \Geeftlist\Model\ResourceFactory $resourceModelFactory, protected \Geeftlist\Service\Security $securityService, protected \OliveOil\Core\Service\GenericFactoryInterface $indexerFactory, protected \OliveOil\Core\Service\Crypt $cryptService, protected $sampleDataFolder)
    {
    }

    public static function fakeEmailFromName($name): string {
        return trim(
                (string) preg_replace('/[^a-z0-9-]+/', '.', strtolower((string) $name)), '.'
            ) . '@' . static::SAMPLEDATA_EMAIL_DOMAIN;
    }

    public function injectCuts(string $file, $testCase, array $vars = []) {
        $path = $this->sampleDataFolder . DIRECTORY_SEPARATOR . $file . '.php';
        if (!file_exists($path)) {
            throw new FileNotFoundException($path);
        }

        extract($vars);
        unset($vars);
        /* $testCase can be used in included files */
        return include $path;
    }

    /**
     * @param string $indexer
     * @return \Geeftlist\Indexer\IndexerInterface
     */
    public function getIndexer($indexer) {
        return $this->getIndexerFactory()->resolveGet($indexer);
    }

    public function getModelFactory(): \Geeftlist\Model\ModelFactory {
        return $this->modelFactory;
    }

    public function getResourceFactory(): \Geeftlist\Model\ResourceFactory {
        return $this->resourceModelFactory;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    public function getIndexerFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->indexerFactory;
    }

    public function getCryptService(): \OliveOil\Core\Service\Crypt {
        return $this->cryptService;
    }

    /**
     * @return \Faker\Generator
     */
    public function getFaker() {
        if (! $this->faker) {
            $this->faker = \Faker\Factory::create();
        }

        return $this->faker;
    }
}
