<?php

namespace Geeftlist\Test\Util;


use OliveOil\Core\Test\Util\ControllerTestCaseTrait;

trait ApiControllerTestCaseTrait
{
    use ControllerTestCaseTrait {
        getRequest as ControllerTestCaseTrait_getRequest;
        getResponse as ControllerTestCaseTrait_getResponse;
    }

    /**
     * WORKAROUND: PHPStan does not resolve correctly the type of the returned objects when
     *             using @method tag in the class' docblock so we force an override here (ugly but meh).
     *
     * @return \OliveOil\Core\Model\Rest\RequestInterface
     */
    public function getRequest() {
        return $this->ControllerTestCaseTrait_getRequest();
    }

    /**
     * Same as above, plus we force the type to the special Response class for tests.
     *
     * @return \OliveOil\Core\Test\Model\Rest\TestResponse
     */
    public function getResponse() {
        return $this->ControllerTestCaseTrait_getResponse();
    }
}
