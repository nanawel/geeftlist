<?php

namespace Geeftlist\Test\Util;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Test\Constants;

/**
 * @method \Faker\Generator faker()
 */
trait TestDataTrait
{

    /**
     * @param Geeftee|Geeftee[] $geeftee
     * @param string|null $status
     * @param int|null $count
     * @return Gift[]
     */
    public function generateGifts(
        Geefter $creator,
        mixed $geeftee,
        ?string $status = Gift\Field\Status::AVAILABLE,
        ?int $count = 10
    ): array {
        if (!is_array($geeftee)) {
            $geeftee = [$geeftee];
        }

        $gifts = [];
        for ($i = 1; $i <= $count; ++$i) {
            $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'creator_id' => $creator->getId(),
                'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX) . ('_' . $i),
                'status'     => $status ?? Gift\Field\Status::AVAILABLE
            ]])->save();
            foreach ($geeftee as $g) {
                ($g instanceof Geefter ? $g->getGeeftee() : $g)->addGift($gift);
            }

            $gifts[$gift->getId()] = $gift;
        }

        return $gifts;
    }

    /**
     * @param int|null $geeftersCnt The number of geefters to generate
     * @param int|null $minFamilies The min number of families each geefter might create
     * @param int|null $maxFamilies The max number of families each geefter might create
     * @param int|null $minGifts The min number of gifts a geefter might create for each relative
     * @param int|null $maxGifts The max number of gifts a geefter might create for each relative
     * @return array
     */
    public function generateCutsLight(
        ?int $geeftersCnt = 3,
        ?int $minFamilies = 0,
        ?int $maxFamilies = 3,
        ?int $minGifts = 0,
        ?int $maxGifts = 3
    ) {
        return $this->generateBaseCuts(
            [
                'count' => $geeftersCnt,
            ],
            [
                'min' => $minFamilies,
                'max' => $maxFamilies
            ],
            [
                'min' => $minGifts,
                'max' => $maxGifts
            ]
        );
    }

    /**
     * @param array|null $familySpecs
     * @param array|null $giftsSpecs
     */
    protected function generateBaseCuts(
        array $geefterSpecs,
        ?array $familySpecs = [],
        ?array $giftsSpecs = []
    ): array {
        $this->disableListeners('index');

        $geefterSpecs = array_replace_recursive([
            'count' => 3,
        ], $geefterSpecs);
        $familySpecs = array_replace_recursive([
            'min' => 0,
            'max' => 3,
            'members' => [
                'min' => 0,
                'max' => '*'
            ],
            'visibility' => [
                Family\Field\Visibility::PUBLIC => 1,       // 33%
                Family\Field\Visibility::PROTECTED => 1,    // 33%
                Family\Field\Visibility::PRIVATE => 1       // 33%
            ]
        ], $familySpecs);
        $giftsSpecs = array_replace_recursive([
            'min' => 0,
            'max' => 3,
            /*'geeftees' => [
                'self' => 1,        // 33%
                'relatives' => 2,   // 66%
            ],*/
            'status' => [
                Gift\Field\Status::AVAILABLE => 6,          // 75%
                Gift\Field\Status::ARCHIVED => 1,           // 12.5%
                Gift\Field\Status::DELETED => 1,            // 12.5%
            ]
        ], $giftsSpecs);

        $distributionFunc = static function ($specs) {
            while (true) {
                foreach ($specs as $val => $cnt) {
                    for ($i = $cnt; $i > 0; --$i) {
                        yield $val;
                    }
                }
            }
        };
        $minMaxFunc = static function (array $specs, $countable = null) {
            $max = $countable !== null
                ? count($countable)
                : ($specs['max'] === '*' ? PHP_INT_MAX : (int) $specs['max']);
            while (true) {
                if (! $specs['min'] && ! $max) {
                    yield 0;
                }

                for ($i = $specs['min']; $i <= $max; ++$i) {
                    yield $i;
                }
            }
        };

        // Generate geefters
        $geefters = [];
        $geeftersCnt = $geefterSpecs['count'];
        for ($i = 0; $i < $geeftersCnt; ++$i) {
            $geefter = $this->getRichard(true, $i);
            $geefters[$geefter->getId()] = $geefter;
        }

        // Generate geeftees
        $geeftees = [];
        foreach ($geefters as $geefter) {
            $geeftee = $geefter->getGeeftee();
            $geeftees[$geeftee->getId()] = $geeftee;
        }

        // Generate families
        $families = [];
        $familiesOwner = [];
        $visibilityGenerator = $distributionFunc($familySpecs['visibility']);
        $familyCntGenerator = $minMaxFunc($familySpecs);
        foreach ($geefters as $geefter) {
            $familiesToCreate = $familyCntGenerator->current();
            for ($i = 0; $i < $familiesToCreate; ++$i) {
                $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                    'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                    'owner_id' => $geefter->getId(),
                    'visiblity' => $visibilityGenerator->current()
                ]])->save();
                $families[$family->getId()] = $family;
                $familiesOwner[$family->getId()] = $geefter;
                $visibilityGenerator->next();
            }

            $familyCntGenerator->next();
        }

        // Add new geeftees to random families
        $familiesMembers = [];
        $familiesByGeeftee = [];
        $familyMembersCntGenerator = $minMaxFunc($familySpecs['members'], $geeftees);
        $this->callPrivileged(
            function() use (
                $geefters, $families, $geeftees, $familySpecs, $familyMembersCntGenerator, &$familiesMembers, &$familiesByGeeftee
            ): void {
            foreach ($families as $family) {
                $newMembers = $this->faker()->randomElements(
                    $geeftees,
                    $familyMembersCntGenerator->current()
                );
                foreach ($newMembers as $newMember) {
                    $familiesMembers[$family->getId()][$newMember->getId()] = $newMember;
                    $familiesByGeeftee[$newMember->getId()][$family->getId()] = $family;
                    $family->addGeeftee($newMembers);
                }

                $familyMembersCntGenerator->next();
            }
        });

        // Generate gifts
        $giftsById = [];
        $giftsByGeefteeId = [];
        $giftsCntGenerator = $minMaxFunc($giftsSpecs);
        $statusGenerator = $distributionFunc($giftsSpecs['status']);
        $this->callPrivileged(
            function() use (
                $geefters, $geeftees, $giftsSpecs, $giftsCntGenerator, $statusGenerator, &$giftsById, &$giftsByGeefteeId
            ): void {
            foreach ($geefters as $geefter) {
                foreach ($geeftees as $geeftee) {
                    $giftsCnt = $giftsCntGenerator->current();

                    for ($i = 0; $i < $giftsCnt; ++$i) {
                        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                            'creator_id' => $geefter->getId(),
                            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX) . ('_' . $i),
                            'status'     => $statusGenerator->current()
                        ]])->save();

                        $giftsById[$gift->getId()] = $gift;
                        $giftsByGeefteeId[$geeftee->getId()][$gift->getId()] = $gift;

                        $geeftee->addGift($gift);
                        $statusGenerator->next();
                    }

                    $giftsCntGenerator->next();
                }
            }
        });

        $relativesByGeefter = [];
        $relativesByGeeftee = [];
        foreach ($geeftees as $geeftee) {
            $relatives = [];
            if (isset($familiesByGeeftee[$geeftee->getId()])) {
                foreach (array_keys($familiesByGeeftee[$geeftee->getId()]) as $familyId) {
                     $relatives += $familiesMembers[$familyId];
                }
            }

            unset($relatives[$geeftee->getId()]);   // Remove geeftee himself (not really a relative)
            if ($geeftee->getGeefterId()) {
                $relativesByGeefter[$geeftee->getGeefterId()] = $relatives;
            }

            $relativesByGeeftee[$geeftee->getId()] = $relatives;
        }

        // Batch reindex gifts
        $this->getIndexer('gift/geefterAccess')->reindexEntities($giftsById);

        $this->enableListeners('index');

        return [
            'geefters' => $geefters,
            'geeftersById' => $geefters,
            'geeftees' => $geeftees,
            'geefteesById' => $geeftees,
            'families' => $families,
            'familiesById' => $families,
            'familiesOwner' => $familiesOwner,
            'familiesMembers' => $familiesMembers,
            'familiesByGeeftee' => $familiesByGeeftee,
            'relativesByGeefter' => $relativesByGeefter,
            'relativesByGeeftee' => $relativesByGeeftee,
            'gifts' => $giftsById,
            'giftsById' => $giftsById,
            'giftsByGeefteeId' => $giftsByGeefteeId,
        ];
    }

    /**
     * @param Geeftee[]|Geefter[]|null $members
     * @param string $visibility
     * @param bool $skipSendEmails
     * @return Family
     */
    protected function generateFamily(
        Geefter $owner,
        ?array $members = [],
        ?string $visibility = Family\Field\Visibility::PROTECTED,
        ?bool $skipSendEmails = true
    ) {
        /** @var Geeftee[] $members */
        $members = array_map(static function ($m) {
            switch (true) {
                case $m instanceof Geefter:
                    $m = $m->getGeeftee();
                case $m instanceof Geeftee:
                    return $m;
                default:
                    throw new \InvalidArgumentException('$members must be composed of Geeftees or Geefters.');
            }
        }, $members);

        $emailEnabledValue = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', !$skipSendEmails);

        try {
            /** @var Family $family */
            $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                'owner_id' => $owner->getId(),
                'visiblity' => $visibility
            ]])->save();
            $this->callPrivileged(static function () use ($owner, $family, $members): void {
                $family->addGeeftee($owner->getGeeftee());
                $family->addGeeftee($members);
            });
        } finally {
            $this->getAppConfig()->setValue('EMAIL_ENABLED', $emailEnabledValue);
        }

        return $family;
    }

    /**
     * @param Geefter $asGeefter
     * @param int|Gift[] $withGifts
     * @return GiftList
     */
    protected function generateGiftList($asGeefter, $withGifts) {
        $giftList = $this->newEntity(GiftList::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GIFTLIST_LABEL_PREFIX),
            'owner_id' => $asGeefter->getId(),
        ]])->save();

        if (is_int($withGifts)) {
            $gifts = [];
            for ($i = 0; $i < $withGifts; ++$i) {
                $gifts[] = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                    'creator_id' => $asGeefter->getId(),
                ]])->save();
            }
        }
        elseif (is_array($withGifts)) {
            $gifts = $withGifts;
        }

        if ($gifts !== []) {
            /** @var \Geeftlist\Model\GiftList\Gift $giftListGiftHelper */
            $giftListGiftHelper = $this->getContainer()->get(GiftList\Gift::class);
            $giftListGiftHelper->addLinks([$giftList], $gifts);
        }

        return $giftList;
    }

    /**
     * @param Geefter $creator
     * @return Geeftee
     */
    protected function generateGeefterlessGeeftee($creator) {
        return $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $creator->getId(),
        ]])->save();
    }
}
