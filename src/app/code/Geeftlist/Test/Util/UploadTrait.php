<?php

namespace Geeftlist\Test\Util;


use Symfony\Component\Filesystem\Filesystem;

trait UploadTrait
{
    public static $TEST_IMAGE_SOURCE_DIR = __DIR__ . '/../cuts/images/';

    protected function getFakeUploadedImagePath(string $imageFile = 'gift.jpg'): string|false {
        $fs = $this->getContainer()->get(Filesystem::class);

        $tmpName = tempnam('/tmp', 'geeftlist_tests_');
        $fs->copy(static::$TEST_IMAGE_SOURCE_DIR . $imageFile, $tmpName, true);

        return $tmpName;
    }
}
