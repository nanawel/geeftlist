<?php

namespace Geeftlist\Test\Util;

use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Notification;
use Geeftlist\Test\Constants;
use Laminas\Db\Sql\Expression;
use OliveOil\Core\Model\AbstractModel;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\Security\Captcha;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Service\Session\Manager;

trait TestCaseTrait
{
    use \OliveOil\Core\Test\Util\TestCaseTrait {
        executeWithListeners as oliveOil_executeWithListeners;
        executeOnlyWithListeners as oliveOil_executeOnlyWithListeners;
    }

    /** @var \OliveOil\Core\Model\AbstractModel[] */
    protected $cutEntities = [];

    /** @var int */
    protected $janeId;

    /** @var int */
    protected $johnId;

    /**
     * @before
     */
    public function setUpContext(): void {
        $this->getSecurityService()->resetRestrictionLevel();
        $this->enableListeners('*');
        $this->unsetGeefterInSession();
    }

    public function initFixedGeefters(): void {
        if (! $this->johnId) {
            $this->johnId = $this->getJohn()->getId();
            $this->resetJohn();
        }

        if (! $this->janeId) {
            $this->janeId = $this->getJane()->getId();
            $this->resetJane();
        }
    }

    public function resetFixedGeefters(): void {
        $this->resetJohn();
        $this->resetJane();
    }

    protected function resetJohn() {
        $this->getSecurityService()->callPrivileged(function (): void {
            // Ensure we really persist this object in database
            $john = $this->getJohn();
            $john->addData([
                'username' => Constants::GEEFTER_JOHN_USERNAME,
                'email'    => Constants::GEEFTER_JOHN_EMAIL,
                'password' => Constants::GEEFTER_JOHN_PASSWORD,
                'pending_email'       => null,
                'pending_email_token' => null,
            ])
                ->save();
            $geeftee = $john->getGeeftee();
            $geeftee->addData([
                    'name'  => Constants::GEEFTER_JOHN_USERNAME,
                    'email' => Constants::GEEFTER_JOHN_EMAIL,
                    'dob'   => Constants::GEEFTEE_JOHN_DOB
                ])->setOrigData([
                    'geeftee_id' => $geeftee->getOrigData('geeftee_id'),
                    'geefter_id' => $john->getId()
                ])
                ->save();
        });
    }

    protected function resetJane() {
        $this->getSecurityService()->callPrivileged(function (): void {
            // Ensure we really persist this object in database
            $jane = $this->getJane();
            $jane->addData([
                'username' => Constants::GEEFTER_JANE_USERNAME,
                'email'    => Constants::GEEFTER_JANE_EMAIL,
                'password' => Constants::GEEFTER_JANE_PASSWORD,
                'pending_email'       => null,
                'pending_email_token' => null,
            ])
                ->save();
            $geeftee = $jane->getGeeftee();
            $geeftee->addData([
                    'name'  => Constants::GEEFTER_JANE_USERNAME,
                    'email' => Constants::GEEFTER_JANE_EMAIL,
                    'dob'   => Constants::GEEFTEE_JANE_DOB
                ])->setOrigData([
                    'geeftee_id' => $geeftee->getOrigData('geeftee_id'),
                'geefter_id' => $jane->getId()
                ])
                ->save();
        });
    }

    /**
     * @param string $entityType
     * @param array|int $conditions
     * @return AbstractModel|null
     */
    public function getEntity($entityType, $conditions = null, $flags = []) {
        if (is_numeric($conditions)) {
            return $this->getRepository($entityType)
                ->get((int) $conditions);
        }

        /* @var \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection */
        $collection = $this->getEntityCollection($entityType, $conditions);
        $collection->getSelect()
            ->order(new Expression('RAND()'))
            ->limit(1);

        foreach ($flags as $flag) {
            $collection->setFlag($flag);
        }

        return $collection->getFirstItem();
    }

    /**
     * @param string $entityType
     * @param array|null $conditions
     * @return AbstractCollection
     */
    public function getEntityCollection($entityType, $conditions = null) {
        /* @var \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection */
        $collection = $this->getGeeftlistResourceFactory()->resolveMakeCollection($entityType);
        if ($conditions) {
            foreach ($conditions as $field => $cond) {
                $collection->addFieldToFilter($field, $cond);
            }
        }

        return $collection;
    }

    /**
     * @param string $entityType
     * @param array|null $criterias
     * @return AbstractModel[]
     */
    public function getEntityItems($entityType, $criteria = null) {
        return $this->getRepository($entityType)
            ->find($criteria);
    }

    /**
     * @param string $entityType
     * @param array|null $constructorArguments
     * @return object
     */
    public function newEntity($entityType, $constructorArguments = []) {
        return $this->cutEntities[] = $this->getRepository($entityType)
            ->newModel($constructorArguments);
    }

    /**
     * @param int|string|Geefter|null $geefter
     * @return Geefter|null
     */
    public function setGeefterInSession($geefter) {
        if (empty($geefter)) {
            throw new \InvalidArgumentException('$geefter cannot be null. Use unsetGeefterInSession() to clear.');
        }

        if (! $geefter instanceof Geefter) {
            $geefter = $this->newEntity(Geefter::ENTITY_TYPE);
            if (is_int($geefter)) {
                $geefter->load($geefter);
            }
            else {
                $geefter->loadByEmail($geefter);
            }
        }

        $this->getGeefterSession()
            ->invalidate()
            ->setGeefter($geefter);

        return $geefter;
    }

    public function unsetGeefterInSession(): void {
        $this->getGeefterSession()
            ->invalidate()
            ->setGeefter(null);
    }

    public function getGeefterSession() {
        return $this->getSession('geefter');
    }

    /**
     * @return Geefter
     */
    public function getJohn() {
        return $this->getSecurityService()->callPrivileged(function () {
            $john = $this->getEntity(
                Geefter::ENTITY_TYPE,
                $this->johnId ?: ['email' => Constants::GEEFTER_JOHN_EMAIL]
            );
            if (!$john) {
                throw new \UnexpectedValueException('Uh oh... John seems to be missing from the database.');
            }

            return $john;
        });
    }

    /**
     * @return Geefter
     */
    public function getGeefterButJohn($conditions = []) {
        $conditions = array_merge(
            [
                'email' => ['nlike' => Constants::GEEFTER_JOHN_EMAIL],
                'geeftee' => ['null' => false]
            ],
            $conditions
        );
        return $this->getEntity(Geefter::ENTITY_TYPE, $conditions);
    }

    /**
     * @return Geefter
     */
    public function getGeefterBut($geefter, $conditions = []) {
        $conditions = array_merge(
            [
                'geefter_id' => ['nlike' => $geefter->getId()],
                'geeftee' => ['null' => false]
            ],
            $conditions
        );
        return $this->getEntity(Geefter::ENTITY_TYPE, $conditions);
    }

    /**
     * @return Geefter
     */
    public function getJane() {
        return $this->getSecurityService()->callPrivileged(function () {
            $jane = $this->getEntity(
                Geefter::ENTITY_TYPE,
                $this->janeId ?: ['email' => Constants::GEEFTER_JANE_EMAIL]
            );
            if (! $jane) {
                throw new \UnexpectedValueException('Uh oh... Jane seems to be missing from the database.');
            }

            return $jane;
        });
    }

    /**
     * Special geefter that is recreated at every call by default
     *
     * @param bool $recreate
     * @param string|null $suffix
     * @return Geefter
     */
    public function getRichard($recreate = true, $suffix = '') {
        return $this->getSecurityService()->callPrivileged(function () use ($recreate, $suffix) {
            /** @var \Geeftlist\Model\Geefter $richard */
            $richard = $this->getEntity(
                Geefter::ENTITY_TYPE,
                ['email' => $this->getRichardEmail($suffix)]
            );

            if ($richard && $richard->getId()) {
                if (! $recreate) {
                    return $richard;
                }

                $richard->deleteWithGeeftee();
            }

            return $this->createRichard($suffix);
        });
    }

    /**
     * @param string $suffix
     * @return Geefter
     * @throws \DI\NotFoundException
     */
    public function createRichard($suffix = '') {
        return $this->getSecurityService()->callPrivileged(function () use ($suffix) {
            $richard = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
                'username' => Constants::GEEFTER_RICHARD_USERNAME . ($suffix !== '' ? ' ' . $suffix : ''),
                'email' => $this->getRichardEmail($suffix),
                'password_hash' => $this->getCryptService()->hash(Constants::GEEFTER_RICHARD_PASSWORD)
            ]])->save();
            $richard->getGeeftee()->addData([
                'dob' => '1975-06-11'
            ])->save();

            return $richard;
        });
    }

    private function getRichardEmail($suffix): string {
        return sprintf(
            'richard.roe%s%s@%s',
            $suffix !== '' ? '+' : '',
            preg_replace('/[^a-z0-9]+/i', '.', trim((string) $suffix)),
            Constants::SAMPLEDATA_EMAIL_DOMAIN
        );
    }

    /**
     * @return Geefter
     */
    public function getGeefterButJane($conditions = []) {
        $conditions = array_merge(
            $conditions,
            ['email' => ['nlike' => Constants::GEEFTER_JANE_EMAIL]]
        );
        return $this->getEntity(Geefter::ENTITY_TYPE, $conditions);
    }

    /**
     * @return Geeftee
     */
    public function getAnonymousGeeftee($conditions = []) {
        $conditions = array_merge(
            [
                'email' => ['nin' => [
                    Constants::GEEFTER_JOHN_EMAIL,
                    Constants::GEEFTER_JANE_EMAIL,
                    Constants::GEEFTER_RICHARD_EMAIL
                ]]
            ],
            $conditions
        );
        return $this->getEntity(Geeftee::ENTITY_TYPE, $conditions);
    }

    /**
     * @return Geefter
     */
    public function getAnonymousGeefter($conditions = []) {
        $conditions = array_merge(
            [
                'email' => ['nin' => [
                    Constants::GEEFTER_JOHN_EMAIL,
                    Constants::GEEFTER_JANE_EMAIL,
                    Constants::GEEFTER_RICHARD_EMAIL
                ]],
                'geeftee' => ['null' => false]
            ],
            $conditions
        );

        return $this->getEntity(Geefter::ENTITY_TYPE, $conditions);
    }

    public function getCsrfToken() {
        return $this->getGeefterSession()->getCsrfToken();
    }

    public function clearNotifications() {
        $this->getEntityCollection(Notification::ENTITY_TYPE)
            ->delete();

        return $this;
    }

    public function clearCacheArrayObjects() {
        \OliveOil\Core\Model\Cache\VolatileArrayObject::clearAllObjects();

        return $this;
    }

    /**
     * Delete all entities created through self::newEntity()
     *
     * @param bool $clear Clear entities array
     * @return $this
     */
    public function deleteCutEntities($clear = true) {
        $this->getSecurityService()->callPrivileged(function() use ($clear): void {
            foreach ($this->cutEntities as $e) {
                if ($e instanceof AbstractModel && $e->getId()) {
                    if ($e instanceof Geefter) {
                        // Ensure we don't leave a geefterless geeftee in the database
                        $e->deleteWithGeeftee();
                    }
                    else {
                        $e->delete();
                    }
                }
            }

            if ($clear) {
                $this->cutEntities = [];
            }
        });

        return $this;
    }

    public function disableGeefterAccessListeners(): void {
        $this->getSecurityService()->disableRestrictions();
    }

    public function enableGeefterAccessListeners(): void {
        $this->getSecurityService()->resetRestrictionLevel()
            ->enableRestrictions();
    }

    public function areGeefterAccessListenersEnabled(): bool {
        return $this->getSecurityService()->getDisabledRestrictionLevel() > 0;
    }

    /**
     * @return mixed
     */
    public function callPrivileged(callable $callable) {
        return $this->getSecurityService()->callPrivileged($callable);
    }

    /**
     * @param string $eventSpace
     * @param bool $enableListeners
     * @return mixed
     */
    public function executeWithListeners(callable $callable, $eventSpace, $enableListeners = true) {
        if ($eventSpace === 'geefteraccess') {
            throw new \InvalidArgumentException(
                'Cannot handle geefteraccess event space. Use \Geeftlist\Helper\Security::callPrivileged() instead.'
            );
        }

        return $this->oliveOil_executeWithListeners($callable, $eventSpace, $enableListeners);
    }

    /**
     * @param string[] $enabledEventSpaces
     * @return mixed
     */
    public function executeOnlyWithListeners(array $enabledEventSpaces, callable $callable) {
        if (in_array('geefteraccess', $enabledEventSpaces)) {
            throw new \InvalidArgumentException(
                'Cannot handle geefteraccess event space. Use \Geeftlist\Helper\Security::callPrivileged() instead.'
            );
        }

        return $this->oliveOil_executeOnlyWithListeners($enabledEventSpaces, $callable);
    }

    /**
     * @param string $dataKey
     * @param SessionInterface|string $session
     * @return Captcha
     */
    public function generateCaptchaInSession(
        string $dataKey,
        SessionInterface|string $session = Manager::SESSION_DEFAULT
    ) {
        if (is_string($session)) {
            $session = $this->getSession($session);
        }

        $captcha = $this->getCaptchaService()->generate();
        $session->setPersistentData($dataKey, $captcha);

        return $captcha;
    }

    /**
     * @return \OliveOil\Core\Model\ResourceModel\Db\Connection
     */
    public function getConnection() {
        return $this->getContainer()->get(\OliveOil\Core\Model\ResourceModel\Db\Connection::class);
    }

    /**
     * @return \Geeftlist\Observer\Cron\MailNotification\Geefter
     */
    public function getMailNotificationCronObserver() {
        return $this->getContainer()->get(\Geeftlist\Observer\Cron\MailNotification\Geefter::class);
    }

    /**
     * @return \Geeftlist\Observer\Cron\MailNotification\Geefter\LatestActivity
     */
    public function getLatestActivityMailNotificationCronObserver() {
        return $this->getContainer()->get(\Geeftlist\Observer\Cron\MailNotification\Geefter\LatestActivity::class);
    }

    /**
     * @return \Geeftlist\Model\ModelFactory
     */
    public function getGeeftlistModelFactory() {
        return $this->getContainer()->get(\Geeftlist\Model\ModelFactory::class);
    }

    /**
     * @return \Geeftlist\Model\ResourceFactory
     */
    public function getGeeftlistResourceFactory() {
        return $this->getContainer()->get(\Geeftlist\Model\ResourceFactory::class);
    }

    /**
     * @return \Geeftlist\Service\Security
     */
    public function getSecurityService() {
        return $this->getContainer()->get(\Geeftlist\Service\Security::class);
    }

    /**
     * @return \Geeftlist\Helper\Share\Entity
     */
    public function getShareEntityRuleHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Share\Entity::class);
    }

    /**
     * @param string $indexer
     * @return \Geeftlist\Indexer\IndexerInterface
     */
    public function getIndexer($indexer) {
        return $this->getContainer()->get('Geeftlist\Indexer\Factory')
            ->resolveGet($indexer);
    }

    /**
     * @return \Geeftlist\Service\PermissionInterface
     */
    public function getPermissionService() {
        return $this->getContainer()->get(\Geeftlist\Service\PermissionInterface::class);
    }

    /**
     * @return \Geeftlist\Service\Permission\Geefter
     */
    public function getGeefterPermissionService() {
        return $this->getContainer()->get(\Geeftlist\Service\Permission\Geefter::class);
    }

    /**
     * @return \Geeftlist\Model\RepositoryFactory
     */
    public function getRepositoryFactory() {
        return $this->getContainer()->get(\Geeftlist\Model\RepositoryFactory::class);
    }

    /**
     * @param string $entityType
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getRepository($entityType) {
        return $this->getRepositoryFactory()->resolveGet($entityType);
    }

    /**
     * @return \Geeftlist\Service\Badge
     */
    public function getBadgeService() {
        return $this->getContainer()->get(\Geeftlist\Service\Badge::class);
    }

    /**
     * @return \OliveOil\Core\Service\Security\Captcha
     */
    public function getCaptchaService() {
        return $this->getContainer()->get(\OliveOil\Core\Service\Security\Captcha::class);
    }
}
