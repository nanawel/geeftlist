<?php

namespace Geeftlist\Test\Observer\Geeftee\ClaimRequest;

use Geeftlist\Helper\Geeftee\ClaimRequest;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Geeftee\ClaimRequest\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->getSecurityService()->callPrivileged(function(): void {
            // Force language on used geefters
            $this->getJohn()->setLanguage('en-US')->save();
            $this->getJane()->setLanguage('en-US')->save();
        });
        $this->disableGeefterAccessListeners();
    }

    /**
     * @return ClaimRequest
     */
    protected function getClaimRequestHelper() {
        return $this->getContainer()->get(ClaimRequest::class);
    }

    public function testOnRequestNotifyCreator(): void {
        $fakeEmailSender = $this->getEmailSender();
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Geeftee $newGeeftee */
        $newGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();

        $this->setGeefterInSession($john);

        $request = $this->getClaimRequestHelper()->request($newGeeftee, $john);

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(1, count($sentEmails));

        foreach ($sentEmails as $emailInfo) {
            $this->assertEquals($jane->getEmail(), $emailInfo['to']);
            $this->assertMatchesRegularExpression(
                '/(New pending claim request)|(Nouvelle demande de rattachement en attente)/',
                $emailInfo['subject']
            );
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        // Cleanup for other tests
        $request->delete();
    }
}
