<?php

namespace Geeftlist\Test\Observer\Geeftee;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Helper\Geeftee\ClaimRequest;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Geeftee\ClaimRequest newCut()
 *
 * @group base
 * @group controllers
 */
class ClaimRequestTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    /**
     * @return ClaimRequest
     */
    protected function getClaimRequestHelper() {
        return $this->getContainer()->get(ClaimRequest::class);
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanCreate_notGeefterless(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $this->setGeefterInSession($john);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $john->getId(),
            'geeftee_id' => $jane->getGeeftee()->getId()
        ]]);

        $this->expectException(PermissionException::class);

        $this->enableGeefterAccessListeners();
        $cr->save();
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanCreate_notCurrentGeefter(): void {
        $john = $this->getJohn();
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $john->getGeeftee(),
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        $this->setGeefterInSession($john);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]]);

        $this->expectException(PermissionException::class);

        $this->enableGeefterAccessListeners();
        $cr->save();
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanCreate_notRelated(): void {
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();

        $this->setGeefterInSession($richard);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]]);

        $this->expectException(PermissionException::class);

        $this->enableGeefterAccessListeners();
        $cr->save();
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanCreate_valid(): void {
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        $this->setGeefterInSession($richard);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]]);

        $this->enableGeefterAccessListeners();
        $cr->save();

        $this->assertNotNull($cr->getId());
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanAccept_requester(): void {
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]])->save();

        $this->setGeefterInSession($richard);
        $this->enableGeefterAccessListeners();

        $this->expectException(PermissionException::class);

        $this->getClaimRequestHelper()->acceptRequest($cr);
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanAccept_unrelated(): void {
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]])->save();

        $this->setGeefterInSession($this->getJohn());
        $this->enableGeefterAccessListeners();

        $this->expectException(PermissionException::class);

        $this->getClaimRequestHelper()->acceptRequest($cr);
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanAccept_valid(): void {
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]])->save();

        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        $this->getClaimRequestHelper()->acceptRequest($cr);

        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_ACCEPTED, $cr->getDecisionCode());
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanReject_requester(): void {
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]])->save();

        $this->setGeefterInSession($richard);
        $this->enableGeefterAccessListeners();

        $this->expectException(PermissionException::class);

        $this->getClaimRequestHelper()->rejectRequest($cr);
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanReject_unrelated(): void {
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJane()->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]])->save();

        $this->setGeefterInSession($this->getJohn());
        $this->enableGeefterAccessListeners();

        $this->expectException(PermissionException::class);

        $this->getClaimRequestHelper()->rejectRequest($cr);
    }

    /**
     * @group ticket-408
     */
    public function testAssertCanReject_valid(): void {
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee
        ]);

        /** @var Geeftee\ClaimRequest $cr */
        $cr = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $richard->getId(),
            'geeftee_id' => $geefterlessGeeftee->getId()
        ]])->save();

        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        $this->getClaimRequestHelper()->rejectRequest($cr);

        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_REJECTED, $cr->getDecisionCode());
    }

    public function testAddCurrentGeefterFilter(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $geefterlessGeeftee1 = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $geefterlessGeeftee2 = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $family->addGeeftee([
            $richard->getGeeftee(),
            $geefterlessGeeftee1,
            $geefterlessGeeftee2
        ]);

        /** @var Geeftee\ClaimRequest $cr1 */
        $cr1 = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $john->getId(),
            'geeftee_id' => $geefterlessGeeftee1->getId()
        ]])->save();
        /** @var Geeftee\ClaimRequest $cr2 */
        $cr2 = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
            'geefter_id' => $jane->getId(),
            'geeftee_id' => $geefterlessGeeftee2->getId()
        ]])->save();

        // As John
        $this->setGeefterInSession($john);
        $this->enableGeefterAccessListeners();

        $this->assertEqualsCanonicalizing(
            [$cr1->getId()],
            array_intersect(
                [$cr1->getId(), $cr2->getId()], // Keep only cuts from current test
                $this->getEntityCollection(Geeftee\ClaimRequest::ENTITY_TYPE)->getAllIds()
            )
        );

        // As Jane
        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        $this->assertEqualsCanonicalizing(
            [$cr2->getId()],
            array_intersect(
                [$cr1->getId(), $cr2->getId()], // Keep only cuts from current test
                $this->getEntityCollection(Geeftee\ClaimRequest::ENTITY_TYPE)->getAllIds()
            )
        );

        // As Richard
        $this->setGeefterInSession($richard);
        $this->enableGeefterAccessListeners();

        $this->assertEqualsCanonicalizing(
            [$cr1->getId(), $cr2->getId()], // Keep only cuts from current test
            $this->getEntityCollection(Geeftee\ClaimRequest::ENTITY_TYPE)->getAllIds()
        );

        // As Richard2
        $this->setGeefterInSession($richard2);
        $this->enableGeefterAccessListeners();

        $this->assertEmpty($this->getEntityCollection(Geeftee\ClaimRequest::ENTITY_TYPE)->getAllIds());
    }
}
