<?php

namespace Geeftlist\Test\Observer\Geeftee;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Observer\Geeftee\MailNotification;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Geeftee\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->clearNotifications()
            ->clearCacheArrayObjects();
        $this->getGeefterSession()->invalidate();

        $this->getSecurityService()->callPrivileged(function(): void {
            // Force language on used geefters
            $this->getJohn()->setLanguage('en-US')->save();
            $this->getJane()->setLanguage('en-US')->save();
        });

        $this->disableListeners('index');   // Not needed here
    }

    public function getCut() {
        return $this->getContainer()->get(MailNotification::class);
    }

    public function testOnAddGift_notifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $giftGeeftee = $this->getRichard(true, '_geeftee');
        $giftGeefter = $this->getRichard(true, '_geefter');
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        // Create 2 families with Geefter/Geeftee/Richard
        /** @var Family $commonFamily1 */
        $commonFamily1 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Geefter-Geeftee-Richard family',
            'owner_id' => $giftGeeftee->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily1, $giftGeeftee, $giftGeefter, $richard): void {
            $commonFamily1->addGeeftee([
                $giftGeeftee->getGeeftee(),
                $giftGeefter->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });
        $commonFamily2 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Geefter-Geeftee-Richard family',
            'owner_id' => $giftGeeftee->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily2, $giftGeeftee, $giftGeefter, $richard, $richard2): void {
            $commonFamily2->addGeeftee([
                $giftGeeftee->getGeeftee(),
                $richard->getGeeftee(),
                $richard2->getGeeftee(),
            ]);
        });

        $this->setGeefterInSession($giftGeefter);
        $this->enableListeners('geefternotification')
            ->enableListeners('index');

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftGeefter->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();

        // Gift creation should not send email (only assignation)
        $this->assertEmpty($fakeEmailSender->getSentEmails());

        $giftGeeftee->getGeeftee()->addGift($gift);

        $this->disableListeners('geefternotification')
            ->disableGeefterAccessListeners();

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Gift::ENTITY_TYPE,
            'target_id'    => $gift->getId(),
            'event_name'   => Notification\Gift\Constants::EVENT_ADD_TO_GEEFTEES
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        // Check emails content
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertNotEquals($giftGeeftee->getEmail(), $emailInfo['to']);
            $this->assertNotEquals($giftGeefter->getEmail(), $emailInfo['to']);
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        sort($seenEmails);

        // Check recipients
        $expectedRecipientEmails = [];
        $notifiableGeefters = $this->getPermissionService()->getAllowed($gift, [TypeInterface::ACTION_BE_NOTIFIED]);
        foreach ($notifiableGeefters as $geefter) {
            $expectedRecipientEmails[] = $geefter->getEmail();
        }

        $expectedRecipientEmails = array_unique(
            // Manually excluding geefter here because he's the actor of the action (see mail notification observer)
            array_diff($expectedRecipientEmails, [$giftGeefter->getEmail()])
        );
        sort($expectedRecipientEmails);

        $this->assertNotContainsEquals($giftGeeftee->getEmail(), $expectedRecipientEmails);
        $this->assertNotContainsEquals($giftGeefter->getEmail(), $expectedRecipientEmails);
        $this->assertContainsEquals($richard->getEmail(), $expectedRecipientEmails);
        $this->assertContainsEquals($richard2->getEmail(), $expectedRecipientEmails);
        $this->assertEquals($expectedRecipientEmails, $seenEmails);
    }

    public function testOnAddOpenGift_notifyFamilyAndGeeftees(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $giftGeeftee = $this->getRichard(true, ' geeftee');
        $giftGeefter = $this->getRichard(true, ' geefter');
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        // Create 2 families with Geefter/Geeftee/Richard
        /** @var Family $commonFamily1 */
        $commonFamily1 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Geeftee-Geefter-Richard family',
            'owner_id' => $richard->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily1, $giftGeeftee, $giftGeefter, $richard): void {
            $commonFamily1->addGeeftee([
                $giftGeeftee->getGeeftee(),
                $giftGeefter->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });
        $commonFamily2 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Geeftee-Richard-Richard2 family',
            'owner_id' => $giftGeefter->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily2, $giftGeeftee, $giftGeefter, $richard, $richard2): void {
            $commonFamily2->addGeeftee([
                $giftGeeftee->getGeeftee(),
                $richard->getGeeftee(),
                $richard2->getGeeftee(),
            ]);
        });

        $this->setGeefterInSession($giftGeefter);
        $this->enableListeners('geefternotification')
            ->enableListeners('index');

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftGeefter->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                ['rule_type' => \Geeftlist\Model\Share\Gift\RuleType\RegularType::CODE],
                ['rule_type' => \Geeftlist\Model\Share\Gift\RuleType\OpenGiftType::CODE]
            ]
        ]])->save();

        // Gift creation should not send email (only assignation)
        $this->assertEmpty($fakeEmailSender->getSentEmails());

        $giftGeeftee->getGeeftee()->addGift($gift);

        $this->disableListeners('geefternotification')
            ->disableGeefterAccessListeners();

        // Check notifications creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Gift::ENTITY_TYPE,
            'target_id'    => $gift->getId(),
            'event_name'   => Notification\Gift\Constants::EVENT_ADD_TO_GEEFTEES
        ]);
        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Gift::ENTITY_TYPE,
            'target_id'    => $gift->getId(),
            'event_name'   => Notification\Gift\Constants::EVENT_ADD_TO_ME
        ]);
        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        // Check emails content
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        sort($seenEmails);

        // Check recipients
        $expectedRecipientEmails = [];
        $notifiableGeefters = $this->getPermissionService()->getAllowed($gift, [TypeInterface::ACTION_BE_NOTIFIED]);
        foreach ($notifiableGeefters as $geefter) {
            $expectedRecipientEmails[] = $geefter->getEmail();
        }

        $expectedRecipientEmails = array_unique(
            // Manually excluding geefter here because he's the actor of the action (see mail notification observer)
            array_diff($expectedRecipientEmails, [$giftGeefter->getEmail()])
        );
        sort($expectedRecipientEmails);

        $this->assertContainsEquals($giftGeeftee->getEmail(), $expectedRecipientEmails);
        $this->assertNotContainsEquals($giftGeefter->getEmail(), $expectedRecipientEmails);
        $this->assertContainsEquals($richard->getEmail(), $expectedRecipientEmails);
        $this->assertContainsEquals($richard2->getEmail(), $expectedRecipientEmails);
        $this->assertEquals($expectedRecipientEmails, $seenEmails);
    }

    public function testOnAddGift_noFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $richard = $this->getRichard();
        $this->setGeefterInSession($richard);

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $richard->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $richard->getGeeftee()->addGift($gift);

        $this->assertEmpty($fakeEmailSender->getSentEmails());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $this->assertEmpty($fakeEmailSender->getSentEmails());
    }

    public function testOnMergeWithNewGeefter_noCreator(): void {
        $fakeEmailSender = $this->getEmailSender();

        $john = $this->getJohn();
        $this->getJane();

        /** @var Geeftee $geefterlessGeeftee */
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'email'      => uniqid('geeftee') . '@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'creator_id' => null    // That's what we're testing here (even if it's not a valid situation)
        ]])->save();

        $this->assertEmpty($fakeEmailSender->getSentEmails());

        $this->getGeefteeHelper()->merge($john->getGeeftee(), $geefterlessGeeftee);

        $this->assertEmpty($fakeEmailSender->getSentEmails());
    }

    /**
     * @group ticket-179
     */
    public function testOnExistingGeefteeSignup(): void {
        $fakeEmailSender = $this->getEmailSender();

        $jane = $this->getJane();

        $commonEmail = uniqid('geeftee') . '@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN;

        $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'email'      => $commonEmail,
            'creator_id' => $jane->getId()
        ]])->save();
        /** @var Geefter $newGeefter */
        $newGeefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username'   => $this->faker()->name,
            'email'      => $commonEmail,
            'password' => \Geeftlist\Test\Constants::GEEFTER_OTHERS_PASSWORD,
            'creator_id' => $jane->getId()
        ]]);

        $this->assertEmpty($fakeEmailSender->getSentEmails());

        $newGeefter->save();

        /** @var array $sentEmails Force type for PHPStan */
        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertCount(1, $sentEmails);
        $this->assertEquals($jane->getEmail(), $sentEmails[0]['to']);
        $this->assertValidHtmlWithoutErrors($sentEmails[0]['body']);
    }

    /**
     * @return \Geeftlist\Helper\Geeftee
     * @throws \DI\NotFoundException
     */
    protected function getGeefteeHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Geeftee::class);
    }
}
