<?php

namespace Geeftlist\Test\Observer;

use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

abstract class AbstractGiftTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;

    protected function prepareCuts() {
        return $this->getSecurityService()->callPrivileged(function(): array {
            $richardJaneFamily = $this->getEntity(
                Family::ENTITY_TYPE,
                ['name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . 'Richard-Jane Gift Restrictions Test']
            );
            if ($richardJaneFamily) {
                $richardJaneFamily->delete();
            }

            $richard = $this->getRichard();
            $jane = $this->getJane();   // A member of one of Richard's families
            $john = $this->getJohn();   // A complete stranger to Richard

            /** @var Family $richardJaneFamily */
            $richardJaneFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                'name'     => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . 'Richard-Jane Gift Restrictions Test',
                'owner_id' => $jane->getId()
            ]])->save();
            $this->callPrivileged(static function () use ($richardJaneFamily, $jane, $richard): void {
                $richardJaneFamily->addGeeftee([
                    $jane->getGeeftee(),
                    $richard->getGeeftee()
                ]);
            });

            /** @var Gift $gift1 */
            $gift1 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Richard for himself',
                'creator_id' => $richard->getId()
            ]])->save();
            $richard->getGeeftee()->addGift($gift1);

            /** @var Gift $gift2 */
            $gift2 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Richard with no geeftee',
                'creator_id' => $richard->getId()
            ]])->save();

            /** @var Gift $gift3 */
            $gift3 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Richard for Jane',
                'creator_id' => $richard->getId()
            ]])->save();
            $jane->getGeeftee()->addGift($gift3);

            /** @var Gift $gift4 */
            $gift4 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Jane for herself',
                'creator_id' => $jane->getId()
            ]])->save();
            $jane->getGeeftee()->addGift($gift4);

            /** @var Gift $gift5 */
            $gift5 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Jane with no geeftee',
                'creator_id' => $jane->getId()
            ]])->save();

            /** @var Gift $gift6 */
            $gift6 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Jane for Richard',
                'creator_id' => $jane->getId()
            ]])->save();
            $richard->getGeeftee()->addGift($gift6);

            /** @var Gift $gift7 */
            $gift7 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by Jane for John',
                'creator_id' => $jane->getId()
            ]])->save();
            $john->getGeeftee()->addGift($gift7);

            /** @var Gift $gift8 */
            $gift8 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by John for himself',
                'creator_id' => $john->getId()
            ]])->save();
            $john->getGeeftee()->addGift($gift8);

            /** @var Gift $gift9 */
            $gift9 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by John with no geeftee',
                'creator_id' => $john->getId()
            ]])->save();

            /** @var Gift $gift10 */
            $gift10 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label'      => 'A gift created by John for Jane',
                'creator_id' => $john->getId()
            ]])->save();
            $jane->getGeeftee()->addGift($gift10);

            $refAllGifts = [
                $gift1->getId()  => $gift1,
                $gift2->getId()  => $gift2,
                $gift3->getId()  => $gift3,
                $gift4->getId()  => $gift4,
                $gift5->getId()  => $gift5,
                $gift6->getId()  => $gift6,
                $gift7->getId()  => $gift7,
                $gift8->getId()  => $gift8,
                $gift9->getId()  => $gift9,
                $gift10->getId() => $gift10,
            ];
            $refAllGiftIds = array_keys($refAllGifts);

            $refAllGiftsVisibleToRichard = [
                $gift1->getId(),
                $gift2->getId(),
                $gift3->getId(),
                $gift4->getId(),
                $gift10->getId(),
            ];

            $refAllGiftsNotVisibleToRichard = array_diff(
                $refAllGiftIds,
                $refAllGiftsVisibleToRichard
            );
            $refAllGiftsEditableByRichard = [
                $gift1->getId(),
                $gift2->getId(),
                $gift3->getId()
            ];
            $refAllGiftsMarkableAsDeletedByRichard = [
                $gift1->getId(),
                $gift2->getId(),
                $gift3->getId()
            ];
            $refAllGiftsNotEditableByRichard = array_diff(
                $refAllGiftsVisibleToRichard,
                $refAllGiftsEditableByRichard
            );
            $refAllGiftsNotMarkableAsDeletedByRichard = $refAllGiftsNotEditableByRichard;

            return [
                'richard'           => $richard,
                'john'              => $john,
                'jane'              => $jane,
                'richardJaneFamily' => $richardJaneFamily,
                'gift1'             => $gift1,
                'gift2'             => $gift2,
                'gift3'             => $gift3,
                'gift4'             => $gift4,
                'gift5'             => $gift5,
                'gift6'             => $gift6,
                'gift7'             => $gift7,
                'gift8'             => $gift8,
                'gift9'             => $gift9,
                'gift10'            => $gift10,
                'refAllGifts'       => $refAllGifts,
                'refAllGiftIds'     => $refAllGiftIds,
                'refAllGiftsVisibleToRichard' => $refAllGiftsVisibleToRichard,
                'refAllGiftsNotVisibleToRichard' => $refAllGiftsNotVisibleToRichard,
                'refAllGiftsEditableByRichard' => $refAllGiftsEditableByRichard,
                'refAllGiftsNotEditableByRichard' => $refAllGiftsNotEditableByRichard,
                'refAllGiftsMarkableAsDeletedByRichard' => $refAllGiftsMarkableAsDeletedByRichard,
                'refAllGiftsNotMarkableAsDeletedByRichard' => $refAllGiftsNotMarkableAsDeletedByRichard,
            ];
        });
    }
}
