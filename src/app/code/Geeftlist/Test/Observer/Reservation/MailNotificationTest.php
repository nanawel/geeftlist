<?php

namespace Geeftlist\Test\Observer\Reservation;

use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Reservation\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->disableGeefterAccessListeners();
        $this->getGeefterSession()->invalidate();
        $this->clearNotifications()
            ->clearCacheArrayObjects();

        $this->getSecurityService()->callPrivileged(function(): void {
            // Force language on used geefters
            $this->getJohn()->setLanguage('en-US')->save();
            $this->getJane()->setLanguage('en-US')->save();
        });
    }

    public function testOnNewReservationNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender()->clearSentEmails();

        $giftGeefter = $this->getRichard(true, 'geefter');
        $giftGeeftee = $this->getRichard(true, 'geeftee');
        $richard = $this->getRichard();

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($family, $richard, $giftGeefter, $giftGeeftee): void {
            $family->addGeeftee([
                $giftGeefter->getGeeftee(),
                $giftGeeftee->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftGeefter->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $giftGeeftee->getGeeftee()->addGift($gift);

        $this->setGeefterInSession($giftGeefter);
        $this->clearNotifications()
            ->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();

        $newReservation = $this->newEntity(Reservation::ENTITY_TYPE)
            ->setGeefterId($giftGeefter->getId())
            ->setGiftId($gift->getId())
            ->save();

        $this->disableGeefterAccessListeners();

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Reservation::ENTITY_TYPE,
            'target_id'    => $newReservation->getId(),
            'event_name'   => Notification\Reservation\Constants::EVENT_NEW
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        // Check emails content
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        // Check recipients
        $this->assertNotContainsEquals($giftGeeftee->getEmail(), $seenEmails);
        $this->assertNotContainsEquals($giftGeefter->getEmail(), $seenEmails);
        $this->assertEquals([$richard->getEmail()], $seenEmails);
    }

    public function testOnOpenGiftNewReservationNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender()->clearSentEmails();

        $giftGeefter = $this->getRichard(true, 'geefter');
        $giftGeeftee = $this->getRichard(true, 'geeftee');
        $richard = $this->getRichard();

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($family, $richard, $giftGeefter, $giftGeeftee): void {
            $family->addGeeftee([
                $giftGeefter->getGeeftee(),
                $giftGeeftee->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftGeefter->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                ['rule_type' => \Geeftlist\Model\Share\Gift\RuleType\RegularType::CODE],
                ['rule_type' => \Geeftlist\Model\Share\Gift\RuleType\OpenGiftType::CODE]
            ]
        ]])->save();
        $giftGeeftee->getGeeftee()->addGift($gift);

        $this->setGeefterInSession($giftGeefter);
        $this->clearNotifications()
            ->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();

        $newReservation = $this->newEntity(Reservation::ENTITY_TYPE)
            ->setGeefterId($giftGeefter->getId())
            ->setGiftId($gift->getId())
            ->save();

        $this->disableGeefterAccessListeners();

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Reservation::ENTITY_TYPE,
            'target_id'    => $newReservation->getId(),
            'event_name'   => Notification\Reservation\Constants::EVENT_NEW
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        // Check emails content
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        // Check recipients
        $this->assertNotContainsEquals($giftGeeftee->getEmail(), $seenEmails);
        $this->assertNotContainsEquals($giftGeefter->getEmail(), $seenEmails);
        $this->assertEquals([$richard->getEmail()], $seenEmails);
    }

    public function testOnReservationUpdateNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $giftGeefter = $this->getRichard(true, 'geefter');
        $giftGeeftee = $this->getRichard(true, 'geeftee');
        $richard = $this->getRichard();
        $this->setGeefterInSession($giftGeefter);

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($family, $richard, $giftGeefter, $giftGeeftee): void {
            $family->addGeeftee([
                $giftGeefter->getGeeftee(),
                $giftGeeftee->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftGeefter->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $giftGeeftee->getGeeftee()->addGift($gift);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE)
            ->setGeefterId($giftGeefter->getId())
            ->setGiftId($gift->getId())
            ->setType(Reservation::TYPE_RESERVATION)
            ->setAmount(12.34)
            ->save();

        $this->clearNotifications()
            ->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();

        $reservation->setType(Reservation::TYPE_PURCHASE)
            ->setAmount(23.45)
            ->save();

        $this->disableGeefterAccessListeners();

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Reservation::ENTITY_TYPE,
            'target_id'    => $reservation->getId(),
            'event_name'   => Notification\Reservation\Constants::EVENT_UPDATE
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        // Check emails content
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);

            // Check content only with John (en_US)
            if ($emailInfo['to'] == $richard->getEmail()) {
                $this->assertMatchesRegularExpression('/(achat)|(purchase)/i', $emailInfo['body']);
                $this->assertMatchesRegularExpression('/(Old amount)|(Ancien montant)/i', $emailInfo['body']);
                $this->assertMatchesRegularExpression('/(New amount)|(Nouveau montant)/i', $emailInfo['body']);
            }
        }

        // Check recipients
        $this->assertNotContainsEquals($giftGeeftee->getEmail(), $seenEmails);
        $this->assertNotContainsEquals($giftGeefter->getEmail(), $seenEmails);
        $this->assertEquals([$richard->getEmail()], $seenEmails);
    }

    /**
     * @group fix20230416
     */
    public function testOnReservationDeleteNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $giftGeefter = $this->getRichard(true, 'geefter');
        $giftGeeftee = $this->getRichard(true, 'geeftee');
        $richard = $this->getRichard();
        $this->setGeefterInSession($giftGeefter);

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($family, $richard, $giftGeefter, $giftGeeftee): void {
            $family->addGeeftee([
                $giftGeefter->getGeeftee(),
                $giftGeeftee->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftGeefter->getId(),
            'label'      => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $giftGeeftee->getGeeftee()->addGift($gift);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE)
            ->setGeefterId($giftGeefter->getId())
            ->setGiftId($gift->getId())
            ->setType(Reservation::TYPE_RESERVATION)
            ->setAmount(12.34)
            ->save();

        $this->clearNotifications()
            ->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();

        $reservation->delete();

        $this->disableGeefterAccessListeners();

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Reservation::ENTITY_TYPE,
            'target_id'    => $reservation->getId(),
            'event_name'   => Notification\Reservation\Constants::EVENT_DELETE
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        // Check emails content
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);

            // Check content only with John (en_US)
            if ($emailInfo['to'] == $richard->getEmail()) {
                $this->assertMatchesRegularExpression(
                    '/' . preg_quote($giftGeefter->getUsername()) . ' (has cancelled)|(a annulé)/i',
                    $emailInfo['body']
                );
            }
        }

        // Check recipients
        $this->assertNotContainsEquals($giftGeeftee->getEmail(), $seenEmails);
        $this->assertNotContainsEquals($giftGeefter->getEmail(), $seenEmails);
        $this->assertEquals([$richard->getEmail()], $seenEmails);
    }
}
