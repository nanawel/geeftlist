<?php

namespace Geeftlist\Test\Observer;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Family newCut()
 *
 * @group base
 * @group observers
 */
class FamilyTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;

    public function testFilterByVisibility(): void {
        $geefters = [
            $this->getJohn(),
            $this->getJane(),
            $richard = $this->getRichard(),
            $this->getAnonymousGeefter()
        ];

        /** @var Geefter $geefter */
        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);
            $this->disableGeefterAccessListeners();

            $geefterFamilyIds = $geefter->getFamilyCollection()->getAllIds();
            $visibleFamilyIds = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Family::ENTITY_TYPE)
                ->addFieldToFilter(
                    'visibility',
                    ['in' => [Family\Field\Visibility::PUBLIC, Family\Field\Visibility::PROTECTED]]
                )
                ->getAllIds();
            $expectedFamilyIds = array_unique(array_merge($geefterFamilyIds, $visibleFamilyIds));
            sort($expectedFamilyIds);
            $this->assertNotEmpty($expectedFamilyIds);

            $this->enableGeefterAccessListeners();

            $familyIds = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Family::ENTITY_TYPE)
                ->getAllIds();
            sort($familyIds);

            $this->assertEquals(
                $expectedFamilyIds,
                $familyIds,
                sprintf('Geefter: %s%s', $geefter->getUsername(), PHP_EOL) .
                'Families that *should* be visible but are not: '
                    . (implode(', ', array_diff($geefterFamilyIds, $familyIds)) ?: 'none (good)') . "\n" .
                'Families that *should not* be visible but are: '
                    . (implode(', ', array_diff($familyIds, $geefterFamilyIds)) ?: 'none (good)')
            );
        }
    }
}
