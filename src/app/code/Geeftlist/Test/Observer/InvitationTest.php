<?php

namespace Geeftlist\Test\Observer;

use Geeftlist\Exception\Invitation\MaxReachedException;
use Geeftlist\Model\Invitation;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Invitation newCut()
 *
 * @group base
 * @group observers
 */
class InvitationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;

    protected function setUp(): void {
        parent::setUp();
        /** @var RepositoryInterface $invitationRepository */
        $invitationRepository = $this->getRepository(Invitation::ENTITY_TYPE);
        $invitationRepository->deleteAll();

        $this->getAppConfig()->setValue('INVITATION_ENABLED', true);
    }

    /**
     * @group ticket-668
     */
    public function testOnBeforeSaveCheckMaxInvitationReachedForGeefter(): void {
        $this->getAppConfig()->setValue('INVITATION_MAX_PER_GEEFTER', 2);

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var \Geeftlist\Service\Invitation $invitationService */
        $invitationService = $this->getContainer()->get(\Geeftlist\Service\Invitation::class);

        $invitationService->generate($jane);
        $invitationService->generate($jane);

        try {
            $invitationService->generate($jane);
            $this->fail('Failed throwing exception on max invitation limit reached');
        } catch (MaxReachedException $e) {
            $this->assertEquals(
                'Sorry, the maximum invitations limit is reached.',
                $e->getMessage()
            );
        }

        // Disable limitation
        $this->getAppConfig()->setValue('INVITATION_MAX_PER_GEEFTER', 0);

        // Should be okay again
        $invitationService->generate($jane);
    }
}
