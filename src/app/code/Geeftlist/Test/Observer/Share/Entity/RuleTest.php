<?php

namespace Geeftlist\Test\Observer\Share\Entity;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Entity\Rule;
use Geeftlist\Model\Share\Gift\RuleType\FamilyType;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Model\Share\Gift\RuleType\PrivateType;
use Geeftlist\Model\Share\Gift\RuleType\RegularType;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group observers
 */
class RuleTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->enableGeefterAccessListeners();
        $this->unsetGeefterInSession();
    }

    /**
     * @return Gift
     */
    protected function getEntityCut($creator = null) {
        if (! $creator) {
            $creator = $this->getJohn();
        }

        return $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $creator->getId()
        ]]);
    }

    public function testValidateEntityShareRules_invalid(): void {
        $invalidShareEntityRuleConfigurations = [
            "Invalid rule type" => [
                [
                    'rule_type' => 'invalid'
                ]
            ],
            "Invalid family rule params" => [
                [
                    'rule_type' => FamilyType::CODE,
                    'params' => ['family_ids' => null]
                ]
            ]
        ];

        foreach ($invalidShareEntityRuleConfigurations as $testLabel => $testData) {
            $entityCut = $this->getEntityCut()->addData([
                \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY
                    => $testData
            ]);

            $errorAggregator = $entityCut->validate();

            $this->assertNotEmpty($errorAggregator->getFieldErrors(), 'Test case: ' . $testLabel);
        }
    }

    public function testValidateEntityShareRules_valid(): void {
        $john = $this->getJohn();
        $validShareEntityRuleConfigurations = [
            "Valid rule type (regular)" => [
                [
                    'rule_type' => RegularType::CODE
                ]
            ],
            "Valid rule type (private) with useless params" => [
                [
                    'rule_type' => PrivateType::CODE,
                    'params' => [
                        'some' => [
                            'useless params'
                        ]
                    ]
                ]
            ],
            "Valid family rule params" => [
                [
                    'rule_type' => FamilyType::CODE,
                    'params' => [
                        'family_ids' => [$this->faker()->randomElement($john->getFamilyCollection()->getAllIds())]
                    ]
                ]
            ]
        ];

        foreach ($validShareEntityRuleConfigurations as $testLabel => $testData) {
            $entityCut = $this->getEntityCut()->addData([
                \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY
                    => $testData
            ]);

            $errorAggregator = $entityCut->validate();

            $this->assertEmpty($errorAggregator->getFieldErrors(), 'Test case: ' . $testLabel);
        }
    }

    public function testValidateModel_invalid(): void {
        $entityCut = $this->getEntityCut()->save();
        $invalidData = [
            "Empty data" => [],
            "Empty target entity type" => [
                'target_entity_type' => null,
                'target_entity_id' => $entityCut->getId(),
                'rule_type' => RegularType::CODE
            ],
            "Empty rule type" => [
                'target_entity_type' => $entityCut->getEntityType(),
                'target_entity_id' => $entityCut->getId(),
                'rule_type' => null
            ]
        ];

        foreach ($invalidData as $testLabel => $testData) {
            $cut = $this->newEntity(Rule::ENTITY_TYPE, ['data' => $testData]);
            $errorAggregator = $cut->validate();

            $this->assertNotEmpty($errorAggregator->getFieldErrors(), 'Test case: ' . $testLabel);
        }
    }

    public function testValidateModel_valid(): void {
        $entityCut = $this->getEntityCut()->save();
        $invalidData = [
            "Default" => [
                'target_entity_type' => $entityCut->getEntityType(),
                'target_entity_id' => $entityCut->getId(),
                'rule_type' => RegularType::CODE
            ]
        ];

        foreach ($invalidData as $testLabel => $testData) {
            $cut = $this->newEntity(Rule::ENTITY_TYPE, ['data' => $testData]);
            $errorAggregator = $cut->validate();

            $this->assertEmpty($errorAggregator->getFieldErrors(), 'Test case: ' . $testLabel);
        }
    }

    public function testSaveEntityShareRule_create(): void {
        $this->disableListeners('index');

        $entityCut = $this->getEntityCut()
            ->save();

        $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
            'target_entity_type' => $entityCut->getEntityType(),
            'target_entity_id' => $entityCut->getId(),
        ]);
        $this->assertCount(0, $shareRules);

        $this->enableListeners('index');

        $entityCut->addData([
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => PrivateType::CODE
                ]
            ],
        ])->save();

        $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
            'target_entity_type' => $entityCut->getEntityType(),
            'target_entity_id' => $entityCut->getId(),
        ]);
        $this->assertCount(1, $shareRules);
        $this->assertEquals(PrivateType::CODE, $shareRules->getFirstItem()->getRuleType());
    }

    public function testSaveEntityShareRule_update(): void {
        $entityCut = $this->getEntityCut()
            ->save();

        $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
            'target_entity_type' => $entityCut->getEntityType(),
            'target_entity_id' => $entityCut->getId(),
        ]);
        $this->assertCount(1, $shareRules);
        $defaultShareRule = $shareRules->getFirstItem();
        $this->assertNotEquals(PrivateType::CODE, $defaultShareRule->getRuleType());

        $entityCut->addData([
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => PrivateType::CODE
                ]
            ],
        ])->save();

        $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
            'target_entity_type' => $entityCut->getEntityType(),
            'target_entity_id' => $entityCut->getId(),
        ]);
        $this->assertCount(1, $shareRules);
        $this->assertEquals(PrivateType::CODE, $shareRules->getFirstItem()->getRuleType());
    }

    public function testSaveEntityShareRule_delete(): void {
        $this->enableGeefterAccessListeners();
        $john = $this->getJohn();

        $familyRuleData = [
            'rule_type' => FamilyType::CODE,
            'params' => [
                'family_ids' => $this->faker()->randomElements($john->getFamilyCollection()->getAllIds(), 3)
            ],
            'priority' => 100
        ];
        $geefterRuleData = [
            'rule_type' => GeefterType::CODE,
            'params' => [
                'geefter_ids' => [$this->getJane()->getId()]
            ],
            'priority' => 200
        ];

        $entityCut = $this->getEntityCut()
            ->addData([
                \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                    $familyRuleData,
                    $geefterRuleData
                ]
            ])
            ->save();

        $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
            'target_entity_type' => $entityCut->getEntityType(),
            'target_entity_id' => $entityCut->getId(),
        ]);
        $this->assertCount(2, $shareRules);

        $entityCut->addData([
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                $familyRuleData
            ]
        ])->save();

        $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
            'target_entity_type' => $entityCut->getEntityType(),
            'target_entity_id' => $entityCut->getId(),
        ]);
        $this->assertCount(1, $shareRules);
        $this->assertEquals(
            $familyRuleData,
            array_intersect_key($shareRules->getFirstItem()->getData(), $familyRuleData)
        );
    }
}
