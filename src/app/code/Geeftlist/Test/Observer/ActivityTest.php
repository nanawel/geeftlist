<?php

namespace Geeftlist\Test\Observer;

use Geeftlist\Model\Activity;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;


/**
 * @method \Geeftlist\Observer\Activity newCut()
 *
 * @group base
 * @group observers
 */
class ActivityTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;

    protected bool $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);

        $this->disableListeners('geefternotification')
            ->disableListeners('activity')
            ->disableListeners('geefteraccess')
            ->disableListeners('index');

        static $firstRun = true;
        if ($firstRun) {
            // Make sure no invalid data remains in DB when this test class is run
            // => IDs may be reused by MySQL when deleting/reinserting rows
            $this->getContainer()->get('Geeftlist\Model\Activity\Repository')->deleteAll();
        }
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);

        $this->enableListeners('activity')
            ->enableListeners('geefteraccess')
            ->enableListeners('index');

        $this->unsetGeefterInSession();
    }

    public function testOnGiftSave_create(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->enableListeners('activity');

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();

        /** @var Activity $activity */
        $activity = $this->getEntity(Activity::ENTITY_TYPE, [
            'actor_type'  => Geefter::ENTITY_TYPE,
            'actor_id'    => $jane->getId(),
            'object_type' => Gift::ENTITY_TYPE,
            'object_id'   => $gift->getId()
        ], ['skip_exclude_geefteeless_gifts']);

        $this->assertNotNull($activity);
        $this->assertEquals(Activity\Field\Action::CREATE, $activity->getAction());
    }

    public function testOnGiftSave_update(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();

        $this->enableListeners('activity');

        $gift->setLabel(uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX))
            ->save();

        /** @var Activity $activity */
        $activity = $this->getEntity(Activity::ENTITY_TYPE, [
            'actor_type'  => Geefter::ENTITY_TYPE,
            'actor_id'    => $jane->getId(),
            'object_type' => Gift::ENTITY_TYPE,
            'object_id'   => $gift->getId()
        ], ['skip_exclude_geefteeless_gifts']);

        $this->assertNotNull($activity);
        $this->assertEquals(Activity\Field\Action::UPDATE, $activity->getAction());
    }

    public function testOnGiftMarkAsDeleted(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();

        $this->enableListeners('activity');

        $gift->markAsDeleted()
            ->save();

        /** @var Activity $activity */
        $activity = $this->getEntity(Activity::ENTITY_TYPE, [
            'actor_type'  => Geefter::ENTITY_TYPE,
            'actor_id'    => $jane->getId(),
            'object_type' => Gift::ENTITY_TYPE,
            'object_id'   => $gift->getId()
        ], ['skip_exclude_geefteeless_gifts']);

        $this->assertNotNull($activity);
        $this->assertEquals(Activity\Field\Action::DELETE, $activity->getAction());
    }

    public function testOnFamilyAddGeeftee(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
        ]])->save();

        $this->enableListeners('activity');

        $this->callPrivileged(static function () use ($family, $john): void {
            $family->addGeeftee($john->getGeeftee());
        });

        /** @var Activity $activity */
        $activity = $this->getEntity(Activity::ENTITY_TYPE, [
            'actor_type'  => Geeftee::ENTITY_TYPE,
            'actor_id'    => $john->getGeeftee()->getId(),
            'object_type' => Family::ENTITY_TYPE,
            'object_id'   => $family->getId()
        ]);

        $this->assertNotNull($activity);
        $this->assertEquals(Activity\Field\Action::JOIN, $activity->getAction());
    }

    public function testOnFamilyRemoveGeeftee(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
        ]])->save();

        $this->callPrivileged(static function () use ($family, $john): void {
            $family->addGeeftee($john->getGeeftee());
        });

        $this->enableListeners('activity');

        $this->callPrivileged(static function () use ($family, $john): void {
            $family->removeGeeftee($john->getGeeftee());
        });

        /** @var Activity $activity */
        $activity = $this->getEntity(Activity::ENTITY_TYPE, [
            'actor_type'  => Geeftee::ENTITY_TYPE,
            'actor_id'    => $john->getGeeftee()->getId(),
            'object_type' => Family::ENTITY_TYPE,
            'object_id'   => $family->getId()
        ]);

        $this->assertNotNull($activity);
        $this->assertEquals(Activity\Field\Action::LEAVE, $activity->getAction());
    }

    public function testAddCurrentGeefteeGiftsFilter_ownGifts(): void {
        $geefters = [
            $this->getJohn(),
            $this->getJane(),
            $richard = $this->getRichard(),
            $richard2 = $this->getRichard(true, 2)
        ];
        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        foreach ($geefters as $geefter) {
            $this->callPrivileged(static function () use ($commonFamily, $geefter): void {
                $commonFamily->addGeeftee($geefter->getGeeftee());
            });
        }

        $this->enableListeners('activity')
            ->enableListeners('index');

        foreach ($geefters as $geefter) {
            if ($geefter !== $richard2) {
                foreach ($geefters as $creator) {
                    $this->generateGifts($creator, $geefter->getGeeftee(), null, 2);
                }
            }
        }

        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);
            $geeftee = $geefter->getGeeftee();
            $this->enableGeefterAccessListeners();

            $activities = $this->getEntityCollection(Activity::ENTITY_TYPE)
                ->getItems();
            $this->assertNotEmpty(
                $activities,
                sprintf('%s (%s) has no visible activities.', $geefter->getUsername(), $geefter->getId())
            );

            $this->disableGeefterAccessListeners();

            /** @var Activity $a */
            foreach ($activities as $a) {
                $isGift = $a->getObjectType() == Gift::ENTITY_TYPE;
                $this->assertTrue(! $isGift
                    || ($isGift
                        && ($a->getObject()->getGeefteeId() != $geeftee->getId()
                            || $a->getObject()->getCreatorId() == $geefter->getId())),
                    sprintf('%s (%s) can see a gift for himself/herself ', $geefter->getUsername(), $geefter->getId())
                    . sprintf('(%s (%s).', $a->getObject()->getLabel(), $a->getObject()->getId())
                );
            }
        }
    }

    public function testAddCurrentGeefteeGiftsFilter_relatedGeefteeGifts(): void {
        $jane = $this->getJane();
        $john = $this->getJohn();
        $richard = $this->getRichard();

        /** @var Family $johnRichardFamily */
        $johnRichardFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($johnRichardFamily, $john, $richard): void {
            $johnRichardFamily->addGeeftee([$john->getGeeftee(), $richard->getGeeftee()]);
        });

        $this->enableListeners('activity')
            ->enableListeners('index');
        $this->unsetGeefterInSession();

        // Generate new CREATE GIFT activities
        $giftForJohnByHimself = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $john->getGeeftee()->addGift($giftForJohnByHimself);

        $giftForJohnByRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $john->getGeeftee()->addGift($giftForJohnByRichard);

        $giftForRichardByHimself = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard->getGeeftee()->addGift($giftForRichardByHimself);

        $giftForRichardByJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $richard->getGeeftee()->addGift($giftForRichardByJohn);

        $allActivityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->addFieldToFilter('object_id', ['in' => [
                $giftForJohnByHimself->getId(),
                $giftForJohnByRichard->getId(),
                $giftForRichardByHimself->getId(),
                $giftForRichardByJohn->getId(),
            ]])
            ->addFieldToFilter('object_type', Gift::ENTITY_TYPE)
            ->getAllIds();
        $janeVisibleActivityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->addFieldToFilter('object_id', ['in' => [
                $giftForJohnByHimself->getId(),
                $giftForJohnByRichard->getId()
            ]])
            ->addFieldToFilter('object_type', Gift::ENTITY_TYPE)
            ->getAllIds();
        $johnVisibleActivityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->addFieldToFilter('object_id', ['in' => [
                $giftForJohnByHimself->getId(),
                $giftForRichardByHimself->getId(),
                $giftForRichardByJohn->getId()
            ]])
            ->addFieldToFilter('object_type', Gift::ENTITY_TYPE)
            ->getAllIds();
        $richardVisibleActivityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->addFieldToFilter('object_id', ['in' => [
                $giftForRichardByHimself->getId(),
                $giftForJohnByHimself->getId(),
                $giftForJohnByRichard->getId()
            ]])
            ->addFieldToFilter('object_type', Gift::ENTITY_TYPE)
            ->getAllIds();

        $this->assertNotEmpty($janeVisibleActivityIds);
        $this->assertNotEmpty($johnVisibleActivityIds);
        $this->assertNotEmpty($richardVisibleActivityIds);

        $this->enableListeners('geefteraccess');
        $this->setGeefterInSession($jane);

        // Jane should see activity related to John's gifts only
        $activityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->getAllIds();
        $this->assertNotEmpty($activityIds);
        foreach ($janeVisibleActivityIds as $id) {
            $this->assertContainsEquals(
                $id,
                $activityIds
            );
        }

        foreach (array_diff($allActivityIds, $janeVisibleActivityIds) as $id) {
            $this->assertNotContainsEquals(
                $id,
                $activityIds
            );
        }

        $this->setGeefterInSession($john);

        // John should see activity related to Richard's gifts only and its own creations if he's not the geeftee
        $activityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->getAllIds();
        $this->assertNotEmpty($activityIds);
        foreach ($johnVisibleActivityIds as $id) {
            $this->assertContainsEquals(
                $id,
                $activityIds
            );
        }

        foreach (array_diff($allActivityIds, $johnVisibleActivityIds) as $id) {
            $this->assertNotContainsEquals(
                $id,
                $activityIds
            );
        }

        $this->setGeefterInSession($richard);

        // Richard should see activity related to John's gifts only and its own creations if he's not the geeftee
        $activityIds = $this->getGeeftlistResourceFactory()
            ->resolveMakeCollection(Activity::ENTITY_TYPE)
            ->getAllIds();
        $this->assertNotEmpty($activityIds);
        foreach ($richardVisibleActivityIds as $id) {
            $this->assertContainsEquals(
                $id,
                $activityIds
            );
        }

        foreach (array_diff($allActivityIds, $richardVisibleActivityIds) as $id) {
            $this->assertNotContainsEquals(
                $id,
                $activityIds
            );
        }
    }

    public function testAddFamilyFilter(): void {
        $geefters = [
            $this->getJohn(),
            $this->getJane(),
            $richard = $this->getRichard(),
            $richard2 = $this->getRichard(true, 2)
        ];

        $this->enableListeners('activity')
            ->enableListeners('index');

        $allFamilyCuts = [];

        foreach ($geefters as $geefter) {
            if ($geefter !== $richard2) {
                /** @var Family $family */
                $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                    'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                    'owner_id' => $geefter->getId()
                ]])->save();
                $allFamilyCuts[$family->getId()] = $family;
            }
        }

        $i = 0;
        foreach ($allFamilyCuts as $familyCut) {
            foreach ($geefters as $member) {
                if (++$i % 3 !== 0) {
                    $this->callPrivileged(static function () use ($familyCut, $member): void {
                        $familyCut->addGeeftee($member->getGeeftee());
                    });
                }
            }
        }

        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);

            $geefterFamilyIds = $geefter->getFamilyCollection()
                ->addFieldToFilter('geeftee', $geefter->getGeeftee()->getId())
                ->getAllIds();

            // Make sure geefter is not member of ALL test families
            $this->assertNotEmpty(array_diff($allFamilyCuts, $geefterFamilyIds));

            $this->enableGeefterAccessListeners();

            $activities = $this->getEntityCollection(Activity::ENTITY_TYPE)
                ->load();
            if ($geefter !== $richard) {
                $this->assertNotEmpty(
                    $activities,
                    sprintf('%s (%s) has no visible activities.', $geefter->getUsername(), $geefter->getId())
                );
            }

            $this->disableGeefterAccessListeners();

            /** @var Activity $a */
            foreach ($activities as $a) {
                $this->assertTrue(
                    $a->getObjectType() != Family::ENTITY_TYPE
                    || ($a->getObjectType() === Family::ENTITY_TYPE && in_array($a->getObjectId(), $geefterFamilyIds)),
                    sprintf('%s (%s) can see an activity of the family ', $geefter->getUsername(), $geefter->getId())
                    // Skip object loading (*much* faster)
                    //. "{$a->getObject()->getName()} ({$a->getObject()->getId()}) without being a member."
                    . sprintf('#%d without being a member.', $a->getObjectId())
                );
            }
        }
    }
}
