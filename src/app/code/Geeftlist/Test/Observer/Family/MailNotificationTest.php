<?php

namespace Geeftlist\Test\Observer\Family;

use Geeftlist\Model\Family;
use Geeftlist\Model\Notification;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Family\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->clearNotifications()
            ->clearCacheArrayObjects();;

        $this->getSecurityService()->callPrivileged(function(): void {
            // Force language on used geefters
            $this->getJohn()->setLanguage('en-US')->save();
            $this->getJane()->setLanguage('en-US')->save();
        });

        $this->disableListeners('index');   // Not needed here
    }

    public function testOnAddGeefteeNotifyGeeftee(): void {
        $fakeEmailSender = $this->getEmailSender();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($john->getGeeftee()));

        $this->callPrivileged(static function () use ($commonFamily, $john): void {
            $commonFamily->addGeeftee($john->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($john->getGeeftee()));

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);
        $seenJohn = false;
        foreach ($sentEmails as $emailInfo) {
            if ($emailInfo['to'] == $john->getEmail()) {
                $seenJohn = true;
                $this->assertMatchesRegularExpression(
                    '/(You are now a member)|(Vous êtes à présent membre de la famille)/',
                    $emailInfo['subject']
                );
                $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
            }
        }

        $this->assertTrue($seenJohn);
    }

    public function testOnAddGeefteeNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $richard = $this->getRichard();
        $this->callPrivileged(static function () use ($commonFamily, $richard): void {
            $commonFamily->addGeeftee($richard->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($john->getGeeftee()));

        $this->enableListeners('geefternotification');

        $this->callPrivileged(static function () use ($commonFamily, $john): void {
            $commonFamily->addGeeftee($john->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($john->getGeeftee()));

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Family::ENTITY_TYPE,
            'target_id'    => $commonFamily->getId(),
            'event_name'   => Notification\Family\Constants::EVENT_JOIN,
            'event_params' => json_encode(['geeftee_ids' => [$john->getGeeftee()->getId()]])
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Force sending notifications now
        $this->getEmailSender()->clearSentEmails(); // Flush "new member" and "owner" direct notification emails
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(2, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);

            $this->assertMatchesRegularExpression(
                '/(Recent activity in your families)|(Activité récente dans vos familles)/',
                $emailInfo['subject']
            );
            $this->assertMatchesRegularExpression(
                '/(has joined family)|(a rejoint la famille)/',
                $emailInfo['body']
            );
        }

        $this->assertContainsEquals($jane->getEmail(), $seenEmails);
        $this->assertContainsEquals($richard->getEmail(), $seenEmails);
    }

    public function testOnAddGeefteesNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $this->callPrivileged(static function () use ($commonFamily, $richard): void {
            $commonFamily->addGeeftee($richard->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($john->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($richard2->getGeeftee()));

        $this->enableListeners('geefternotification');

        $this->callPrivileged(static function () use ($commonFamily, $john, $richard2): void {
            $commonFamily->addGeeftee([$john->getGeeftee(), $richard2->getGeeftee()]);
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($john->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard2->getGeeftee()));

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Family::ENTITY_TYPE,
            'target_id'    => $commonFamily->getId(),
            'event_name'   => Notification\Family\Constants::EVENT_JOIN,
            'event_params' => json_encode(
                ['geeftee_ids' => [$john->getGeeftee()->getId(), $richard2->getGeeftee()->getId()]]
            )
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Force sending notifications now
        $this->getEmailSender()->clearSentEmails(); // Flush "new member" and "owner" direct notification emails
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(2, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);

            $this->assertMatchesRegularExpression(
                '/(Recent activity in your families)|(Activité récente dans vos familles)/',
                $emailInfo['subject']
            );
            $this->assertMatchesRegularExpression(
                '/(The following geeftees have joined family)|(Les geeftees suivants ont rejoint la famille)/',
                $emailInfo['body']
            );
        }

        $this->assertContainsEquals($jane->getEmail(), $seenEmails);
        $this->assertContainsEquals($richard->getEmail(), $seenEmails);
    }

    /**
     * @group ticket-411
     */
    public function testOnAddGeefteeNotifyOwner(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $richard = $this->getRichard();
        $this->callPrivileged(static function () use ($commonFamily, $richard): void {
            $commonFamily->addGeeftee($richard->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($john->getGeeftee()));

        $this->enableListeners('geefternotification');

        $this->callPrivileged(static function () use ($commonFamily, $john): void {
            $commonFamily->addGeeftee($john->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($john->getGeeftee()));

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(2, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);

            if ($emailInfo['to'] != $john->getEmail()) {
                $this->assertMatchesRegularExpression(
                    '/(is now a new member of)|(est à présent membre de)/',
                    $emailInfo['subject']
                );
                $this->assertMatchesRegularExpression(
                    '/(is now a new member of)|(est à présent membre de)/',
                    $emailInfo['body']
                );
            }
        }

        $this->assertContainsEquals($jane->getEmail(), $seenEmails);
    }

    public function testOnRemoveGeefteeNotifyGeeftee(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $john): void {
            $commonFamily->addGeeftee($john->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($john->getGeeftee()));

        $this->enableListeners('geefternotification');

        $this->callPrivileged(static function () use ($commonFamily, $john): void {
            $commonFamily->removeGeeftee($john->getGeeftee());
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($john->getGeeftee()));

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);
        $seenJohn = false;
        foreach ($sentEmails as $emailInfo) {
            if ($emailInfo['to'] == $john->getEmail()) {
                $seenJohn = true;
                $this->assertMatchesRegularExpression(
                    '/(You are no longer a member)|(Vous n\'êtes à présent plus membre de la famille)/',
                    $emailInfo['subject']
                );
                $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
            }
        }

        $this->assertTrue($seenJohn);
    }

    public function testOnRemoveGeefteeNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $this->setGeefterInSession($jane);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $john, $richard, $richard2): void {
            $commonFamily->addGeeftee([
                $john->getGeeftee(),
                $richard->getGeeftee(),
                $richard2->getGeeftee()
            ]);
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($john->getGeeftee()));
        $this->assertTrue($commonFamily->isMember($richard->getGeeftee()));

        $this->enableListeners('geefternotification');

        $this->callPrivileged(static function () use ($commonFamily, $john, $richard): void {
            $commonFamily->removeGeeftee([
                $john->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $this->assertTrue($commonFamily->isMember($jane->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($john->getGeeftee()));
        $this->assertFalse($commonFamily->isMember($richard->getGeeftee()));

        // Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Family::ENTITY_TYPE,
            'target_id'    => $commonFamily->getId(),
            'event_name'   => Notification\Family\Constants::EVENT_LEAVE,
            'event_params' => json_encode([
                'geeftee_ids' => [$john->getGeeftee()->getId(), $richard->getGeeftee()->getId()]
            ])
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // Force sending notifications now
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails() ;

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(4, count($sentEmails));     // 2 individual emails + 2 family notification emails

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            if (! in_array($emailInfo['to'], [$john->getEmail(), $richard->getEmail()])) {
                $seenEmails[] = $emailInfo['to'];
                $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
                $this->assertMatchesRegularExpression(
                    '/(Recent activity in your families)|(Activité récente dans vos familles)/',
                    $emailInfo['subject']
                );
                $this->assertMatchesRegularExpression(
                    '/(have left family)|(ont quitté la famille)/',
                    $emailInfo['body']
                );
            }
        }

        $this->assertContainsEquals($jane->getEmail(), $seenEmails);
        $this->assertContainsEquals($richard2->getEmail(), $seenEmails);
    }
}
