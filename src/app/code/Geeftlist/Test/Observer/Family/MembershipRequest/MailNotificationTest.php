<?php

namespace Geeftlist\Test\Observer\Family\MembershipRequest;

use Geeftlist\Helper\Family\Membership;
use Geeftlist\Model\Family;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Family\MembershipRequest\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->getSecurityService()->callPrivileged(function(): void {
            // Force language on used geefters
            $this->getJohn()->setLanguage('en-US')->save();
            $this->getJane()->setLanguage('en-US')->save();
        });

        $this->disableListeners('index');   // Not needed here
    }

    public function testOnRequestNotifyOwner(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Family $newFamily */
        $newFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'visibility' => Family\Field\Visibility::PROTECTED,
            'owner_id'   => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($newFamily, $jane): void {
            $newFamily->addGeeftee($jane->getGeeftee());
        });

        $this->setGeefterInSession($john);
        $this->enableListeners('geefternotification');

        $request = $this->getContainer()->get(Membership::class)->request($newFamily, $john->getGeeftee());

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(1, count($sentEmails));

        foreach ($sentEmails as $emailInfo) {
            $this->assertEquals($jane->getEmail(), $emailInfo['to']);
            $this->assertMatchesRegularExpression(
                '/(New pending membership request)|(Nouvelle candidature en attente)/',
                $emailInfo['subject']
            );
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        // Cleanup for other tests
        $request->delete();
    }

    public function testOnRequestNotifyGeeftee(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        /** @var Family $newFamily */
        $newFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'visibility' => Family\Field\Visibility::PUBLIC,
            'owner_id'   => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($newFamily, $jane): void {
            $newFamily->addGeeftee($jane->getGeeftee());
        });

        $this->setGeefterInSession($jane);
        $this->enableListeners('geefternotification');

        $requests = $this->getContainer()->get(Membership::class)->request(
            $newFamily,
            [$john->getGeeftee(), $richard->getGeeftee()]
        );

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(2, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertMatchesRegularExpression(
                '/(New invitation to join family)|(Nouvelle invitation à rejoindre la famille)/',
                $emailInfo['subject']
            );
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        $this->assertContainsEquals($john->getEmail(), $seenEmails);
        $this->assertContainsEquals($richard->getEmail(), $seenEmails);

        // Cleanup for other tests
        foreach ($requests as $request) {
            $request->delete();
        }
    }
}
