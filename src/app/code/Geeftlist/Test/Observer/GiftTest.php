<?php

namespace Geeftlist\Test\Observer;

use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Test\Util\FakerTrait;

/**
 * @method \Geeftlist\Observer\Gift newCut()
 *
 * @group base
 * @group observers
 */
class GiftTest extends AbstractGiftTest
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;

    /**
     * @return RepositoryInterface
     * @throws \DI\NotFoundException
     * @throws \OliveOil\Core\Exception\Di\Exception
     */
    public function getGiftRepository() {
        return $this->getRepository(Gift::ENTITY_TYPE);
    }

    public function testAssignCreator(): void {
        $this->getGeefterSession()->invalidate();

        $this->disableGeefterAccessListeners();

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(3, true),
            'estimated_price' => $this->faker()->randomFloat(2, 1, 2000)
        ]])->save();
        $this->assertNull($gift->getCreatorId());

        $this->enableGeefterAccessListeners();

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(3, true),
            'estimated_price' => $this->faker()->randomFloat(2, 1, 2000)
        ]])->save();
        $this->assertNull($gift->getCreatorId());

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $this->getGiftRepository()->save($gift);

        $this->assertEquals($john->getId(), $gift->getCreatorId());
    }

    public function testGeefteeGiftCollectionRestrictions(): void {
        $this->disableGeefterAccessListeners();

        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         */
        extract($this->prepareCuts());

        $this->assertNotEmpty($refAllGifts);
        $this->assertNotEmpty($refAllGiftsVisibleToRichard);
        $this->assertNotEquals($refAllGiftIds, $refAllGiftsVisibleToRichard);
        foreach ($refAllGiftsVisibleToRichard as $ragvtr) {
            $this->assertContainsEquals($ragvtr, $refAllGiftIds);
        }

        $this->setGeefterInSession($richard);

        $allIds = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->orderBy('gift_id')
            ->getAllIds();

        $this->assertEmpty(array_diff($refAllGiftIds, $allIds));
        // Same result, different implementation (for debug)
//        foreach ($refAllGiftIds as $ragi) {
//            $this->assertContainsEquals($ragi, $allIds);
//        }

        $this->enableGeefterAccessListeners();
        $this->clearCacheArrayObjects();

        $this->callPrivileged(static function () use ($richardJaneFamily, $richard): void {
            $richardJaneFamily->removeGeeftee($richard->getGeeftee());
        });
        $this->assertTrue($richardJaneFamily->isMember($jane->getGeeftee()));
        $this->assertFalse($richardJaneFamily->isMember($richard->getGeeftee()));

        $refAllGiftsVisibleToRichardWhenNoFamily = [
            $gift1->getId(),
            $gift2->getId(),
            $gift3->getId(),
        ];
        $refAllGiftsNotVisibleToRichardWhenNoFamily = array_diff(
            $refAllGiftIds,
            $refAllGiftsVisibleToRichardWhenNoFamily
        );

        $allVisibleIds = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->orderBy('gift_id')
            ->getAllIds();

        foreach ($refAllGiftsVisibleToRichardWhenNoFamily as $agvtrwnf) {
            $this->assertContainsEquals($agvtrwnf, $allVisibleIds, $refAllGifts[$agvtrwnf]->getLabel() . ' should be visible to Richard but is not.');
        }

        foreach ($refAllGiftsNotVisibleToRichardWhenNoFamily as $agnvtrwnf) {
            $this->assertNotContainsEquals($agnvtrwnf, $allVisibleIds, $refAllGifts[$agnvtrwnf]->getLabel() . ' should NOT be visible to Richard but is.');
        }

        $this->clearCacheArrayObjects();

        $this->callPrivileged(static function () use ($richardJaneFamily, $richard): void {
            $richardJaneFamily->addGeeftee($richard->getGeeftee());
        });

        $this->assertTrue($richardJaneFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($richardJaneFamily->isMember($richard->getGeeftee()));

        $allVisibleIds = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->orderBy('gift_id')
            ->getAllIds();

        $this->assertNotEmpty($allVisibleIds);

        foreach ($refAllGiftsVisibleToRichard as $agvtr) {
            $this->assertContainsEquals($agvtr, $allVisibleIds, $refAllGifts[$agvtr]->getLabel() . ' should be visible to Richard but is not.');
        }

        foreach ($refAllGiftsNotVisibleToRichard as $agnvtr) {
            $this->assertNotContainsEquals($agnvtr, $allVisibleIds, $refAllGifts[$agnvtr]->getLabel() . ' should NOT be visible to Richard but is.');
        }
    }

    public function testGeefteeGiftLoadRestrictions(): void {
        $this->disableGeefterAccessListeners();

        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         */
        extract($this->prepareCuts());

        $this->setGeefterInSession($richard);

        foreach ($refAllGiftIds as $giftId) {
            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $this->assertNotNull($gift);
            $this->assertEquals($refAllGifts[$giftId]->getLabel(), $gift->getLabel());
        }

        $this->enableGeefterAccessListeners();

        foreach ($refAllGiftsVisibleToRichard as $giftId) {
            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $this->assertNotNull($gift);
            $this->assertEquals($refAllGifts[$giftId]->getLabel(), $gift->getLabel());
        }

        foreach ($refAllGiftsNotVisibleToRichard as $giftId) {
            try {
                $this->getEntity(Gift::ENTITY_TYPE, $giftId);
                $this->fail('Failed preventing loading: ' . $refAllGifts[$giftId]->getLabel());
            }
            catch (PermissionException) {}
        }
    }

    public function testGiftSaveRestrictions(): void {
        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         * @var int[] $refAllGiftsEditableByRichard
         * @var int[] $refAllGiftsNotEditableByRichard
         * @var int[] $refAllGiftsMarkableAsDeletedByRichard
         * @var int[] $refAllGiftsNotMarkableAsDeletedByRichard
         */
        extract($this->prepareCuts());

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        foreach ($refAllGiftsEditableByRichard as $giftId) {
            /** @var Gift $gift */
            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $gift->setLabel($gift->getLabel() . ' (modified)');
            $this->getGiftRepository()->save($gift);
        }

        foreach ($refAllGiftsNotEditableByRichard as $giftId) {
            /** @var Gift $gift */
            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $originalLabel = $gift->getLabel();
            try {
                $gift->setLabel($gift->getLabel() . ' (modified)');
                $this->getGiftRepository()->save($gift);
                $this->fail('Failed preventing saving: ' . $gift->getLabel());
            }
            catch (PermissionException) {}

            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $this->assertEquals($originalLabel, $gift->getLabel());
        }
    }

    public function testGiftMarkAsDeletedRestrictions(): void {
        $giftRepository = $this->getRepository(Gift::ENTITY_TYPE);
        $this->disableGeefterAccessListeners();

        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         * @var int[] $refAllGiftsEditableByRichard
         * @var int[] $refAllGiftsNotEditableByRichard
         * @var int[] $refAllGiftsMarkableAsDeletedByRichard
         * @var int[] $refAllGiftsNotMarkableAsDeletedByRichard
         */
        extract($this->prepareCuts());

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        foreach ($refAllGiftsMarkableAsDeletedByRichard as $giftId) {
            /** @var Gift $gift */
            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $this->assertNotEquals(Gift\Field\Status::DELETED, $gift->getStatus());

            $gift->markAsDeleted();
            $giftRepository->save($gift);

            $this->getSecurityService()->callPrivileged(function() use ($giftId): void {
                $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
                $this->assertEquals(Gift\Field\Status::DELETED, $gift->getStatus());
            });
        }

        foreach ($refAllGiftsNotMarkableAsDeletedByRichard as $giftId) {
            /** @var Gift $gift */
            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $this->assertNotEquals(Gift\Field\Status::DELETED, $gift->getStatus());

            try {
                $gift->markAsDeleted();
                $giftRepository->save($gift);
                $this->fail('Failed preventing saving: ' . $gift->getLabel());
            }
            catch (PermissionException) {}

            $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
            $this->assertNotEquals(Gift\Field\Status::DELETED, $gift->getStatus());
        }
    }

    public function testGiftDeleteRestrictions(): void {
        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         */
        extract($this->prepareCuts());

        $this->enableGeefterAccessListeners();

        foreach ([$john, $jane, $richard] as $geefter) {
            $this->setGeefterInSession($geefter);

            foreach ($refAllGifts as $gift) {
                $this->enableGeefterAccessListeners();
                try {
                    $this->getGiftRepository()->delete($gift);
                    $this->fail('Failed preventing deleting: ' . $gift->getLabel());
                }
                catch (PermissionException) {}

                // Disable listeners to be able to load the entity for assertions
                $this->disableGeefterAccessListeners();

                $gift = $this->getEntity(Gift::ENTITY_TYPE, $gift->getId());
                $this->assertNotNull($gift);
                $this->assertNotNull($gift->getId());
            }
        }
    }

    /**
     * @group ticket-612
     */
    public function testOnGeefteeSaveUpdateIndexes(): void {
        $cuts = $this->generateCutsLight(1, 1, 1, 1, 1);
        /** @var Geefter $geefter */
        $geefter = current($cuts['geefters']);
        /** @var Family $family */
        $family = current($cuts['families']);

        // Ensure $geefter is a member of $family
        $family->addGeeftee($geefter->getGeeftee());

        $email = uniqid('someone') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;

        /** @var Geeftee $geefterlessGeeftee */
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'email' => $email
        ]])->save();
        $family->addGeeftee($geefterlessGeeftee);

        $this->enableGeefterAccessListeners();

        // Because of \Geeftlist\Model\Share\Gift\RuleType\RegularType::$familyGeefterIdsCache
        $this->clearCacheArrayObjects();

        /** @var Geefter $newGeefter */
        $newGeefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => uniqid('geefter_'),
            'email' => $email,
            'password' => '123456'
        ]])->save();

        /** @var Geeftee $geeftee */
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geefterlessGeeftee->getId());

        $this->assertEquals($newGeefter->getId(), $geeftee->getGeefterId());

        $this->setGeefterInSession($newGeefter);

        $geefterGiftIds = array_map(static fn($g) => $g->getId(), $cuts['giftsByGeefteeId'][$geefter->getGeeftee()->getId()]);

        self::assertEqualsCanonicalizing(
            $geefterGiftIds,
            $geefter->getGeeftee()->getGiftCollection()->getAllIds()
        );
    }
}
