<?php

namespace Geeftlist\Test\Observer;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Geeftee newCut()
 *
 * @group base
 * @group controllers
 */
class GeefteeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;

    protected bool $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    protected function generateCuts() {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);

        $janeFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($janeFamily, $jane, $richard, $richard2): void {
            $janeFamily->addGeeftee([
                $jane->getGeeftee(),
                $richard->getGeeftee(),
                $richard2->getGeeftee()
            ]);
        });

        $johnFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($johnFamily, $john, $richard3): void {
            $johnFamily->addGeeftee([
                $john->getGeeftee(),
                $richard3->getGeeftee(),
            ]);
        });

        $geefters = [
            'john' => $john,
            'jane' => $jane,
            'richard' => $richard,
            'richard2' => $richard2,
            'richard3' => $richard3
        ];

        $gifts = [];
        foreach ($geefters as $geefter) {
            foreach ($geefters as $geefterGeeftee) {
                $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                    'creator_id' => $geefter->getId()
                ]])->save();
                $geefterGeeftee->getGeeftee()->addGift($gift);

                $gifts[$geefterGeeftee->getId()][] = $gift;
            }
        }

        return [
            'janeFamily' => $janeFamily,
            'johnFamily' => $johnFamily,
            'gifts' => $gifts,
            'geefters' => $geefters,
        ] + $geefters;
    }

    public function testAddGiftPermissionCheck(): void {
        $cuts = $this->generateCutsLight(3, 0, 3, 0, 0);
        /** @var Geefter[] $geefters */
        $geefters = $cuts['geefters'];
        $geeftees = $cuts['geeftees'];
        $relativesByGeefter = $cuts['relativesByGeefter'];

        $this->enableGeefterAccessListeners();

        $addedGifts = [];
        $seenNonRelatives = [];
        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);

            // Allowed (relatives)
            foreach ($relativesByGeefter[$geefter->getId()] as $geeftee) {
                $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                    'creator_id' => $geefter->getId()
                ]])->save();

                $geeftee->addGift($gift);
                $addedGifts[] = $gift;
            }

            // Forbidden (non-relatives)
            $addGiftForbiddenGeeftees = array_diff_key($geeftees, $relativesByGeefter[$geefter->getId()]);
            unset($addGiftForbiddenGeeftees[$geefter->getGeeftee()->getId()]);
            foreach ($addGiftForbiddenGeeftees as $geeftee) {
                try {
                    $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                        'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                        'creator_id' => $geefter->getId()
                    ]])->save();

                    $geeftee->addGift($gift);
                    $this->fail(sprintf('Failed preventing %s ', $geefter->getUsername())
                        . ('from adding a gift to ' . $geeftee->getName()));
                }
                catch (\Geeftlist\Exception\Security\PermissionException) {}
            }

            $seenNonRelatives = array_merge($addGiftForbiddenGeeftees, $seenNonRelatives);
        }

        $this->assertNotEmpty($addedGifts);
        $this->assertNotEmpty($seenNonRelatives);
    }

    public function testSetCreator(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        $newGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
        ]])->setFlag('skip_autoset_creator')
            ->save();

        $this->assertNotNull($newGeeftee->getId());
        $this->assertNull($newGeeftee->getCreatorId());

        $newGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
        ]])->save();

        $this->assertNotNull($newGeeftee->getId());
        $this->assertEquals($jane->getId(), $newGeeftee->getCreatorId());
    }

    /**
     * @group ticket-478
     * @group critical
     */
    public function testRelatedGeefteeCollectionRestrictions(): void {
        $cuts = $this->generateCutsLight(3, 0, 3, 0, 0);
        /** @var Geefter[] $geefters */
        $geefters = $cuts['geefters'];
        $relativesByGeeftee = $cuts['relativesByGeeftee'];

        $this->enableGeefterAccessListeners();

        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);

            // Add geeftee himself (not exactly a relative, but should not be filtered either)
            $relatedGeefteeIds = array_merge(
                array_keys($relativesByGeeftee[$geefter->getGeeftee()->getId()]),
                [$geefter->getGeeftee()->getId()]
            );

            $actualRelatedGeefteeIds = $this->getGeeftlistResourceFactory()
                ->resolveMakeCollection(Geeftee::ENTITY_TYPE)
                ->getAllIds();

            $this->assertEqualsCanonicalizing(
                $relatedGeefteeIds,
                $actualRelatedGeefteeIds,
                sprintf(
                    'Invalid related geeftees found for %s (geeftee #%d)',
                    $geefter->getGeeftee()->getName(),
                    $geefter->getGeeftee()->getId()
                )
            );
        }
    }
}
