<?php

namespace Geeftlist\Test\Observer\Cron\Reservation;

use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group observers
 */
class StaleNotification extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    public const STALE_RESERVATION_DELAY = 24*365*10;

          // 10 years
    public const STALE_PURCHASE_DELAY = 24*365*10;         // 10 years too

    /**
     * @group ticket-519
     * @return \Geeftlist\Observer\Cron\Reservation\StaleNotification
     */
    public function newCut() {
        return $this->getContainer()->make(\Geeftlist\Observer\Cron\Reservation\StaleNotification::class, [
            'staleReservationDelay' => self::STALE_RESERVATION_DELAY,
            'stalePurchaseDelay' => self::STALE_PURCHASE_DELAY,
        ]);
    }

    public function testSendStaleReservationNotifications(): void {
        $cut = $this->newCut();

        $fakeEmailSender = $this->getEmailSender();
        $richard = $this->getRichard();             // Gift creator
        $richard2 = $this->getRichard(true, 2);     // Geeftee
        $richard3 = $this->getRichard(true, 3);     // Reserver

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'           => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(3, true),
            'estimated_price' => $this->faker()->randomFloat(2, 1, 2000),
            'creator_id'      => $richard->getId()
        ]])->save();
        /** @var Reservation|null $reservation */
        $reservation = null;
        $this->callPrivileged(function() use ($richard2, $richard3, $gift, &$reservation): void {
            $richard2->getGeeftee()->addGift($gift);

            /** @var Reservation $reservation */
            $reservation = $this->newEntity(Reservation::ENTITY_TYPE)
                ->setGeefterId($richard3->getId())
                ->setGiftId($gift->getId())
                ->setType(Reservation::TYPE_RESERVATION)
                ->setAmount(12.34)
                ->setReservedAt(DateTime::getDateSql(
                    time() - (60*60*self::STALE_RESERVATION_DELAY + $this->faker()->numberBetween(24*5, 24*180))
                ))
                ->save();
        });

        $this->assertNotNull($reservation->getId());
        $this->assertNull($reservation->getStaleNotificationSentAt());

        $this->getEmailSender()->clearSentEmails();

        $cut->sendStaleReservationNotifications();

        /** @var RepositoryInterface $reservationRepository */
        $reservationRepository = $this->getRepository(Reservation::ENTITY_TYPE);
        $updatedReservation = $reservationRepository->get($reservation->getId());

        $this->assertNotNull($updatedReservation->getStaleNotificationSentAt());

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(1, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
            $this->assertMatchesRegularExpression(
                '/(About your reservation)|(À propos de votre réservation)/',
                $emailInfo['subject']
            );
            $this->assertMatchesRegularExpression(
                '/(You made a reservation on)|(Vous avez réservé)/',
                $emailInfo['body']
            );
        }

        $this->assertContainsEquals($richard3->getEmail(), $seenEmails);
    }

    public function testSendStalePurchaseNotifications(): void {
        $cut = $this->newCut();

        $fakeEmailSender = $this->getEmailSender();
        $richard = $this->getRichard();             // Gift creator
        $richard2 = $this->getRichard(true, 2);     // Geeftee
        $richard3 = $this->getRichard(true, 3);     // Reserver

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'           => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(3, true),
            'estimated_price' => $this->faker()->randomFloat(2, 1, 2000),
            'creator_id'      => $richard->getId()
        ]])->save();
        /** @var Reservation|null $reservation */
        $reservation = null;
        $this->callPrivileged(function() use ($richard2, $richard3, $gift, &$reservation): void {
            $richard2->getGeeftee()->addGift($gift);

            /** @var Reservation $reservation */
            $reservation = $this->newEntity(Reservation::ENTITY_TYPE)
                ->setGeefterId($richard3->getId())
                ->setGiftId($gift->getId())
                ->setType(Reservation::TYPE_PURCHASE)
                ->setAmount(12.34)
                ->setReservedAt(DateTime::getDateSql(
                    time() - (60*60*self::STALE_PURCHASE_DELAY + $this->faker()->numberBetween(24*5, 24*180))
                ))
                ->save();
        });

        $this->assertNotNull($reservation->getId());
        $this->assertNull($reservation->getStaleNotificationSentAt());

        $this->getEmailSender()->clearSentEmails();

        $cut->sendStalePurchaseNotifications();

        /** @var RepositoryInterface $reservationRepository */
        $reservationRepository = $this->getRepository(Reservation::ENTITY_TYPE);
        $updatedReservation = $reservationRepository->get($reservation->getId());

        $this->assertNotNull($updatedReservation->getStaleNotificationSentAt());

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(1, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
            $this->assertMatchesRegularExpression(
                '/(About your reservation)|(À propos de votre réservation)/',
                $emailInfo['subject']
            );
            $this->assertMatchesRegularExpression(
                '/(You made a purchase on)|(Vous avez acheté)/',
                $emailInfo['body']
            );
        }

        $this->assertContainsEquals($richard3->getEmail(), $seenEmails);
    }
}
