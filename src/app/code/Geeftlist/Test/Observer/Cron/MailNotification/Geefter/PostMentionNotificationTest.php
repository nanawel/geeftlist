<?php

namespace Geeftlist\Test\Observer\Cron\MailNotification\Geefter;

use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group observers
 * @doesNotPerformAssertions
 */
class PostMentionNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    /** @see \Geeftlist\Test\Observer\Discussion\Post\MailNotificationTest */
}
