<?php

namespace Geeftlist\Test\Observer\Cron\MailNotification\Geefter;

use Geeftlist\Model\Gift;
use Geeftlist\Model\MailNotification\Constants;
use Geeftlist\Model\Notification;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group observers
 */
class LatestActivityTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    /**
     * @group ticket-442
     * @return \Geeftlist\Observer\Cron\MailNotification\Geefter\LatestActivity
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function newCut() {
        return $this->getContainer()->make(\Geeftlist\Observer\Cron\MailNotification\Geefter\LatestActivity::class);
    }

    public function testCanNotify(): void {
        $cut = $this->newCut();

        // Default frequency: realtime
        $richard = $this->getRichard();

        $this->assertFalse($cut->canNotify($richard));

        $targetId = 9999;

        /** @var \Geeftlist\Model\Notification $notification */
        $notification = $this->newEntity(Notification::ENTITY_TYPE, ['data' => [
            'type' => 'test',
            'category' => Notification::CATEGORY_FAMILIES_ACTIVITY,
            'recipient_type' => Notification::RECIPIENT_TYPE_GEEFTER,
            'target_type' => Gift::ENTITY_TYPE,
            'target_id' => $targetId++,
            'event_name' => 'dummy_event',
        ]])->save();

        /** @var \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient $notificationResource */
        $notificationResource = $this->getGeeftlistResourceFactory()->resolveGet($notification->getEntityType());
        $notificationResource->addRecipient($notification, $richard, Notification::STATUS_CREATED);

        // Not yet notifiable (defer delay applies)
        $this->assertFalse($cut->canNotify($richard));

        $this->clearNotifications()
            ->clearCacheArrayObjects();

        /** @var \Geeftlist\Model\Notification $notification */
        $notification = $this->newEntity(Notification::ENTITY_TYPE, ['data' => [
            'type' => 'test',
            'category' => Notification::CATEGORY_FAMILIES_ACTIVITY,
            'recipient_type' => Notification::RECIPIENT_TYPE_GEEFTER,
            'target_type' => Gift::ENTITY_TYPE,
            'target_id' => $targetId++,
            'event_name' => 'dummy_event',
        ]])->save();

        /** @var \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient $notificationResource */
        $notificationResource = $this->getGeeftlistResourceFactory()->resolveGet($notification->getEntityType());
        $notificationResource->addRecipient($notification, $richard, Notification::STATUS_PENDING, [
            'created_at' => DateTime::getDateSql(new \DateTime('1 hour ago'))
        ]);

        // Notifiable now
        $this->assertTrue($cut->canNotify($richard));

        $this->clearNotifications()
            ->clearCacheArrayObjects();

        // Now set frequency to daily
        $richard->setEmailNotificationFreq(Constants::SENDING_FREQ_DAILY);

        // Not notifiable anymore
        $this->assertFalse($cut->canNotify($richard));

        $this->clearNotifications()
            ->clearCacheArrayObjects();

        /** @var \Geeftlist\Model\Notification $notification */
        $notification = $this->newEntity(Notification::ENTITY_TYPE, ['data' => [
            'type' => 'test',
            'category' => Notification::CATEGORY_FAMILIES_ACTIVITY,
            'recipient_type' => Notification::RECIPIENT_TYPE_GEEFTER,
            'target_type' => Gift::ENTITY_TYPE,
            'target_id' => $targetId++,
            'event_name' => 'dummy_event',
        ]])->save();

        $richard->setEmailNotificationLast(DateTime::getDateSql('2 hours ago'))
            ->save();

        /** @var \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient $notificationResource */
        $notificationResource = $this->getGeeftlistResourceFactory()->resolveGet($notification->getEntityType());
        $notificationResource->addRecipient($notification, $richard, Notification::STATUS_PENDING, [
            'created_at' => DateTime::getDateSql(new \DateTime('23 hours ago'))
        ]);

        // Not yet notifiable (forbidden by frequency)
        $this->assertFalse($cut->canNotify($richard));

        // Now reach the notification defer max count
        for ($i = 0; $i <= $this->getAppConfig()->getValue('NOTIFICATION_DEFER_MAX_COUNT'); ++$i) {
            $notification = $this->newEntity(Notification::ENTITY_TYPE, ['data' => [
                'type' => 'test',
                'category' => Notification::CATEGORY_FAMILIES_ACTIVITY,
                'recipient_type' => Notification::RECIPIENT_TYPE_GEEFTER,
                'target_type' => Gift::ENTITY_TYPE,
                'target_id' => $targetId++,
                'event_name' => 'dummy_event_' . $i,
            ]])->save();

            /** @var \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient $notificationResource */
            $notificationResource = $this->getGeeftlistResourceFactory()->resolveGet($notification->getEntityType());
            $notificationResource->addRecipient($notification, $richard, Notification::STATUS_PENDING, [
                'created_at' => DateTime::getDateSql(new \DateTime('23 hours ago'))
            ]);
        }

        // Still not yet notifiable (forbidden by frequency)
        $this->assertFalse($cut->canNotify($richard));

        $richard->setEmailNotificationLast(DateTime::getDateSql('25 hours ago'))
            ->save();

        // Notifiable now (last sending is > delay/frequency)
        $this->assertTrue($cut->canNotify($richard));
    }
}
