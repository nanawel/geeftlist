<?php

namespace Geeftlist\Test\Observer;


use Geeftlist\Exception\ReservationException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Reservation newCut()
 *
 * @group base
 * @group observers
 */
class ReservationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;

    protected function prepareCuts(): array {
        $this->disableGeefterAccessListeners();

        $commonFamily = $this->getEntity(
            Family::ENTITY_TYPE,
            ['name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . 'John-Jane Reservation Test']
        );
        if ($commonFamily) {
            $commonFamily->delete();
        }

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . 'John-Jane Reservation Test'
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $jane, $john, $richard): void {
            $commonFamily->addGeeftee([
                $jane->getGeeftee(),
                $john->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        /** @var Gift $gift1 */
        $gift1 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => 'A gift created by John for himself (available)',
            'creator_id' => $john->getId(),
            'status'     => Gift\Field\Status::AVAILABLE
        ]])->save();
        $john->getGeeftee()->addGift($gift1);

        /** @var Gift $gift2 */
        $gift2 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => 'A gift created by John for himself (archived)',
            'creator_id' => $john->getId(),
            'status'     => Gift\Field\Status::ARCHIVED
        ]])->save();
        $john->getGeeftee()->addGift($gift2);

        /** @var Gift $gift3 */
        $gift3 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => 'A gift created by John for Jane (available)',
            'creator_id' => $john->getId(),
            'status'     => Gift\Field\Status::AVAILABLE
        ]])->save();
        $jane->getGeeftee()->addGift($gift3);

        /** @var Gift $gift4 */
        $gift4 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => 'A gift created by Jane for John (deleted)',
            'creator_id' => $jane->getId(),
            'status'     => Gift\Field\Status::DELETED
        ]])->save();
        $john->getGeeftee()->addGift($gift4);

        /** @var Gift $gift5 */
        $gift5 = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => 'A gift created by Jane for John and reserved by Richard2 (available)',
            'creator_id' => $jane->getId(),
            'status'     => Gift\Field\Status::AVAILABLE
        ]])->save();
        $john->getGeeftee()->addGift($gift5);
        $this->callPrivileged(function() use ($richard2, $gift5): void {
            $this->newEntity(Reservation::ENTITY_TYPE)
                ->setGeefterId($richard2->getId())
                ->setGiftId($gift5->getId())
                ->save();
        });

        $refAllGifts = [
            $gift1->getId()  => $gift1,
            $gift2->getId()  => $gift2,
            $gift3->getId()  => $gift3,
            $gift4->getId()  => $gift4,
            $gift5->getId()  => $gift5,
        ];
        $refAllGiftIds = array_keys($refAllGifts);

        $refAllGiftsReservableByJohn = [
            $gift3->getId(),
        ];
        $refAllGiftsReservableByJane = [
            $gift1->getId(),
            $gift5->getId(),
        ];
        $refAllGiftsReservableByRichard = [
            $gift1->getId(),
            $gift3->getId(),
            $gift5->getId(),
        ];
        $refAllGiftsReservableByRichard2 = [];

        return [
            'john'              => $john,
            'richard'           => $richard,
            'richard2'          => $richard2,
            'jane'              => $jane,
            'commonFamily'      => $commonFamily,
            'gift1'             => $gift1,
            'gift2'             => $gift2,
            'gift3'             => $gift3,
            'gift4'             => $gift4,
            'gift5'             => $gift5,
            'refAllGifts'       => $refAllGifts,
            'refAllGiftIds'     => $refAllGiftIds,
            'refAllGiftsReservableByJohn'     => $refAllGiftsReservableByJohn,
            'refAllGiftsReservableByJane'     => $refAllGiftsReservableByJane,
            'refAllGiftsReservableByRichard'  => $refAllGiftsReservableByRichard,
            'refAllGiftsReservableByRichard2' => $refAllGiftsReservableByRichard2,
        ];
    }

    public function testReservationRestrictions(): void {
        /**
         * @var Geefter $john
         * @var Geefter $jane
         * @var Geefter $richard
         * @var Geefter $richard2
         * @var Family $commonFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsReservableByJohn
         * @var int[] $refAllGiftsReservableByJane
         * @var int[] $refAllGiftsReservableByRichard
         * @var int[] $refAllGiftsReservableByRichard2
         */
        extract($this->prepareCuts());

        $availableGiftsByGeefter = [
            [
                'geefter'  => $john,
                'gift_ids' => $refAllGiftsReservableByJohn
            ],
            [
                'geefter'  => $jane,
                'gift_ids' => $refAllGiftsReservableByJane
            ],
            [
                'geefter'  => $richard,
                'gift_ids' => $refAllGiftsReservableByRichard
            ],
            [
                'geefter'  => $richard2,
                'gift_ids' => $refAllGiftsReservableByRichard2
            ]
        ];

        $this->enableGeefterAccessListeners();

        foreach ($availableGiftsByGeefter as $tmp) {
            $this->setGeefterInSession($tmp['geefter']);

            $availableGiftIds = $tmp['gift_ids'];
            $unavailableGiftIds = array_diff($refAllGiftIds, $availableGiftIds);
            foreach ($availableGiftIds as $availableGiftId) {
                $cut = $this->newEntity(Reservation::ENTITY_TYPE)
                    ->setGeefterId($tmp['geefter']->getId())
                    ->setGiftId($availableGiftId);

                try {
                    $cut->save();
                }
                catch (\Throwable $e) {
                    $this->fail('Failed saving reservation for "'
                        . $refAllGifts[$availableGiftId]->getLabel()
                        . '" as ' . $tmp['geefter']->getUsername()
                        . ' (' . $e->getMessage() . ')');
                }

                $this->assertNotNull($cut->getId());

                // Delete tested reservation
                $this->disableGeefterAccessListeners();
                $cut->delete();
                $this->enableGeefterAccessListeners();
            }

            foreach ($unavailableGiftIds as $unavailableGiftId) {
                $cut = $this->newEntity(Reservation::ENTITY_TYPE)
                    ->setGeefterId($tmp['geefter']->getId())
                    ->setGiftId($unavailableGiftId);

                try {
                    $cut->save();
                    $this->fail('Failed preventing the creation of a reservation of "'
                        . $refAllGifts[$unavailableGiftId]->getLabel()
                        . '" by ' . $tmp['geefter']->getUsername()
                    );
                }
                catch (ReservationException|PermissionException) {}

                $this->assertNull($cut->getId());
            }
        }

        // Richard2
        $this->setGeefterInSession($richard2);

        foreach (array_diff($refAllGiftIds, [$gift5->getId()]) as $unavailableGiftId) {
            try {
                $this->getEntity(Gift::ENTITY_TYPE, $unavailableGiftId);
                $this->fail('Failed preventing loading of "' . $refAllGifts[$unavailableGiftId]->getLabel()
                    . '" (' . $unavailableGiftId . ') by Richard2 (not relative)');
            }
            catch (PermissionException) {}
        }

        // Gift5 should be visible to Richard2 because (and only because) of the his existing reservation
        $this->assertEquals($gift5->getId(), $this->getEntity(Gift::ENTITY_TYPE, $gift5->getId())->getId());
    }
}
