<?php

namespace Geeftlist\Test\Observer\Geefter;

use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use Laminas\EventManager\Event;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group observers
 */
class PasswordBasedSessionValidationTest extends TestCase
{
    use TestCaseTrait;

    public function testProcessSessionHash_noHashInSession(): void {
        $cut = $this->newCut();

        $geefterSession = $this->getGeefterSession();
        $this->setGeefterInSession($this->getJohn());

        self::assertNull($geefterSession->getPasswordValidationHash());

        $ev = $this->getContainer()->make(Event::class, [
            'name' => 'init::after::geefter',
            'target' => $geefterSession
        ]);
        $cut->processSessionHash($ev);

        self::assertNotNull($geefterSession->getPasswordValidationHash());
    }

    public function testProcessSessionHash_noPasswordSet(): void {
        $cut = $this->newCut();

        $geefterSession = $this->getGeefterSession();
        $geefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => uniqid('Passwordless Geefter '),
            'email'    => uniqid('geefter') . '@'. Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]])->save();
        $this->setGeefterInSession($geefter);

        $geefterSession->setPasswordValidationHash('azerty');

        $ev = $this->getContainer()->make(Event::class, [
            'name' => 'init::after::geefter',
            'target' => $geefterSession
        ]);
        $cut->processSessionHash($ev);

        // Unchanged, but no exception either
        self::assertEquals('azerty', $geefterSession->getPasswordValidationHash());
    }

    public function testProcessSessionHash_invalidHash(): void {
        $cut = $this->newCut();

        $geefterSession = $this->getGeefterSession();
        $this->setGeefterInSession($this->getJohn());

        $geefterSession->setPasswordValidationHash('azerty');

        $ev = $this->getContainer()->make(Event::class, [
            'name' => 'init::after::geefter',
            'target' => $geefterSession
        ]);

        $this->expectException(\OliveOil\Core\Exception\Session\ExpiredException::class);
        $cut->processSessionHash($ev);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testProcessSessionHash_validHash(): void {
        $cut = $this->newCut();

        $geefterSession = $this->getGeefterSession();
        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $geefterSession->setPasswordValidationHash($cut->getPasswordValidationHash($john));

        $ev = $this->getContainer()->make(Event::class, [
            'name' => 'init::after::geefter',
            'target' => $geefterSession
        ]);

        $cut->processSessionHash($ev);
    }

    public function testUpdateSessionHash(): void {
        $cut = $this->newCut();

        $geefterSession = $this->getGeefterSession();
        $geefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => uniqid('Passwordless Geefter '),
            'email'    => uniqid('geefter') . '@'. Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'password' => '123456'
        ]])->save();
        $this->setGeefterInSession($geefter);

        $initialHash = $cut->getPasswordValidationHash($geefter);
        $geefterSession->setPasswordValidationHash($initialHash);

        $geefter->setPasswordHash($this->getCryptService()->hash('abcdef'));

        $ev = $this->getContainer()->make(Event::class, [
            'name' => 'save::after',
            'target' => $geefter
        ]);

        $cut->updateSessionHash($ev);

        $newHash = $geefterSession->getPasswordValidationHash();
        self::assertNotEmpty($newHash);
        self::assertNotEquals($initialHash, $newHash);
    }

    /**
     * @return \Geeftlist\Observer\Geefter\PasswordBasedSessionValidation
     */
    protected function newCut() {
        return $this->getContainer()->make(\Geeftlist\Observer\Geefter\PasswordBasedSessionValidation::class);
    }
}
