<?php

namespace Geeftlist\Test\Observer\Gift;

use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Observer\AbstractGiftTest;

/**
 * @method \Geeftlist\Observer\Gift\Topic newCut()
 *
 * @group base
 * @group observers
 */
class TopicTest extends AbstractGiftTest
{
    public function testTopicCollectionRestrictions(): void {
        $this->disableListeners('geefternotification');

        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         */
        extract($this->prepareCuts());

        $this->disableGeefterAccessListeners();

        $allGiftTopicsByGift = $this->getGiftTopics($refAllGifts);
        $allGiftTopics = [];
        foreach ($allGiftTopicsByGift as $giftId => $giftTopics) {
            foreach ($giftTopics as $topicVisibility => $giftTopic) {
                $this->assertNotEmpty(
                    $giftTopic,
                    sprintf('Missing %s topic on gift %d', $topicVisibility, $giftId)
                );
                $allGiftTopics[$giftTopic->getId()] = $giftTopic;
            }
        }

        //
        // Richard
        //
        $this->setGeefterInSession($richard);
        $this->enableGeefterAccessListeners();

        $expectedTopics = [
            // Gift1
            $allGiftTopicsByGift[$gift1->getId()]['public'],
            // Gift2
            $allGiftTopicsByGift[$gift2->getId()]['protected'],
            $allGiftTopicsByGift[$gift2->getId()]['public'],
            // Gift3
            $allGiftTopicsByGift[$gift3->getId()]['protected'],
            $allGiftTopicsByGift[$gift3->getId()]['public'],
            // Gift4
            $allGiftTopicsByGift[$gift4->getId()]['protected'],
            $allGiftTopicsByGift[$gift4->getId()]['public'],
            // Gift10
            $allGiftTopicsByGift[$gift10->getId()]['protected'],
            $allGiftTopicsByGift[$gift10->getId()]['public']
        ];

        $topics = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Topic::ENTITY_TYPE)
            ->addFieldToFilter('main_table.topic_id', ['in' => array_keys($allGiftTopics)])
            ->getItems();
        $this->checkVisibleTopics($allGiftTopics, $expectedTopics, $topics);

        //
        // Jane
        //
        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        $expectedTopics = [
            // Gift1
            $allGiftTopicsByGift[$gift1->getId()]['public'],
            $allGiftTopicsByGift[$gift1->getId()]['protected'],
            // Gift4
            $allGiftTopicsByGift[$gift4->getId()]['public'],
            // Gift5
            $allGiftTopicsByGift[$gift5->getId()]['protected'],
            $allGiftTopicsByGift[$gift5->getId()]['public'],
            // Gift6
            $allGiftTopicsByGift[$gift6->getId()]['protected'],
            $allGiftTopicsByGift[$gift6->getId()]['public'],
            // Gift7
            $allGiftTopicsByGift[$gift7->getId()]['protected'],
            $allGiftTopicsByGift[$gift7->getId()]['public'],
            // Gift8
            $allGiftTopicsByGift[$gift8->getId()]['public'],
            $allGiftTopicsByGift[$gift8->getId()]['protected']
        ];

        $topics = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Topic::ENTITY_TYPE)
            ->addFieldToFilter('main_table.topic_id', ['in' => array_keys($allGiftTopics)])
            ->getItems();
        $this->checkVisibleTopics($allGiftTopics, $expectedTopics, $topics);

        //
        // John
        //
        $this->setGeefterInSession($john);
        $this->enableGeefterAccessListeners();

        $expectedTopics = [
            // Gift3
            $allGiftTopicsByGift[$gift3->getId()]['protected'],
            $allGiftTopicsByGift[$gift3->getId()]['public'],
            // Gift4
            $allGiftTopicsByGift[$gift4->getId()]['public'],
            $allGiftTopicsByGift[$gift4->getId()]['protected'],
            // Gift8
            $allGiftTopicsByGift[$gift8->getId()]['public'],
            // Gift9
            $allGiftTopicsByGift[$gift9->getId()]['protected'],
            $allGiftTopicsByGift[$gift9->getId()]['public'],
            // Gift10
            $allGiftTopicsByGift[$gift10->getId()]['protected'],
            $allGiftTopicsByGift[$gift10->getId()]['public']
        ];

        $topics = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Topic::ENTITY_TYPE)
            ->addFieldToFilter('main_table.topic_id', ['in' => array_keys($allGiftTopics)])
            ->getItems();
        $this->checkVisibleTopics($allGiftTopics, $expectedTopics, $topics);
    }

    protected function checkVisibleTopics($allGiftTopics, $expectedTopics, $actualTopics) {
        $this->getSecurityService()->callPrivileged(
            function() use ($expectedTopics, $actualTopics, $allGiftTopics): void {
                $expectedTopicIds = [];
                /** @var Topic $expectedTopic */
                foreach ($expectedTopics as $i => $expectedTopic) {
                    $this->assertNotNull($expectedTopic, sprintf('Topic at offset #%s is null and should not be.', $i));
                    $expectedTopicIds[] = $expectedTopic->getId();
                    $this->assertArrayHasKey(
                        $expectedTopic->getId(),
                        $actualTopics,
                        sprintf(
                            '%s topic for %s (%d) should be visible by %s but is not.',
                            ucfirst((string) $expectedTopic->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)),
                            $expectedTopic->getEntity()->getLabel(),
                            $expectedTopic->getEntity()->getId(),
                            $this->getGeefterSession()->getGeefter()->getUsername()
                        )
                    );
                }

                foreach (array_diff(array_keys($allGiftTopics), $expectedTopicIds) as $unexpectedTopicId) {
                    $this->assertArrayNotHasKey(
                        $unexpectedTopicId,
                        $actualTopics,
                        sprintf(
                            '%s topic for %s (#%d) should NOT be visible by %s but is.',
                            ucfirst((string) $allGiftTopics[$unexpectedTopicId]->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)),
                            $allGiftTopics[$unexpectedTopicId]->getEntity()->getLabel(),
                            $allGiftTopics[$unexpectedTopicId]->getEntity()->getId(),
                            $this->getGeefterSession()->getGeefter()->getUsername()
                        )
                    );
                }
            }
        );
        $this->assertCount(count($expectedTopics), $actualTopics);
    }

    /**
     * @param Gift[] $gifts
     * @return Topic[][]
     */
    protected function getGiftTopics($gifts): array {
        $topics = [];

        /** @var \Geeftlist\Helper\Gift\Discussion\Topic $topicHelper */
        $topicHelper = $this->getGeeftlistModelFactory()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
        foreach ($gifts as $gift) {
            $topics[$gift->getId()] = [];
            if ($protectedTopic = $topicHelper->getProtectedTopic($gift, true)) {
                $topics[$gift->getId()]['protected'] = $protectedTopic;
            }

            if ($publicTopic = $topicHelper->getPublicTopic($gift, true)) {
                $topics[$gift->getId()]['public'] = $publicTopic;
            }
        }

        return $topics;
    }
}
