<?php

namespace Geeftlist\Test\Observer\Gift;

use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Observer\AbstractGiftTest;

/**
 * @method \Geeftlist\Observer\Gift newCut()
 *
 * @group base
 * @group observers
 */
class PostTest extends AbstractGiftTest
{
    public function testPostCollectionRestrictions(): void {
        $this->disableListeners('geefternotification');

        /**
         * @var Geefter $richard
         * @var Geefter $jane
         * @var Geefter $john
         * @var Family $richardJaneFamily
         * @var Gift $gift1
         * @var Gift $gift2
         * @var Gift $gift3
         * @var Gift $gift4
         * @var Gift $gift5
         * @var Gift $gift6
         * @var Gift $gift7
         * @var Gift $gift8
         * @var Gift $gift9
         * @var Gift $gift10
         * @var Gift[] $refAllGifts
         * @var int[] $refAllGiftIds
         * @var int[] $refAllGiftsVisibleToRichard
         * @var int[] $refAllGiftsNotVisibleToRichard
         */
        extract($this->prepareCuts());

        $this->disableGeefterAccessListeners();

        $allGiftPostsByGift = $this->getGiftPosts($refAllGifts);
        $allGiftPosts = [];
        foreach ($allGiftPostsByGift as $giftId => $giftPostsByVisibility) {
            foreach ($giftPostsByVisibility as $topicVisibility => $giftPosts) {
                $this->assertNotEmpty(
                    $giftPosts,
                    sprintf('Missing posts for %s topic on gift %d', $topicVisibility, $giftId)
                );
                foreach ($giftPosts as $giftPost) {
                    if ($giftPost) {
                        $allGiftPosts[$giftPost->getId()] = $giftPost;
                    }
                }
            }
        }

        //
        // Richard
        //
        $this->setGeefterInSession($richard);
        $this->enableGeefterAccessListeners();

        $expectedPosts = array_merge(
            // Gift1
            $allGiftPostsByGift[$gift1->getId()]['public'],
            // Gift2
            $allGiftPostsByGift[$gift2->getId()]['protected'],
            $allGiftPostsByGift[$gift2->getId()]['public'],
            // Gift3
            $allGiftPostsByGift[$gift3->getId()]['protected'],
            $allGiftPostsByGift[$gift3->getId()]['public'],
            // Gift4
            $allGiftPostsByGift[$gift4->getId()]['protected'],
            $allGiftPostsByGift[$gift4->getId()]['public'],
            // Gift10
            $allGiftPostsByGift[$gift10->getId()]['protected'],
            $allGiftPostsByGift[$gift10->getId()]['public']
        );

        $posts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Post::ENTITY_TYPE)
            ->addFieldToFilter('post_id', ['in' => array_keys($allGiftPosts)])
            ->getItems();
        $this->checkVisiblePosts($allGiftPosts, $expectedPosts, $posts);

        //
        // Jane
        //
        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        $expectedPosts = array_merge(
            // Gift1
            $allGiftPostsByGift[$gift1->getId()]['public'],
            $allGiftPostsByGift[$gift1->getId()]['protected'],
            // Gift4
            $allGiftPostsByGift[$gift4->getId()]['public'],
            // Gift5
            $allGiftPostsByGift[$gift5->getId()]['protected'],
            $allGiftPostsByGift[$gift5->getId()]['public'],
            // Gift6
            $allGiftPostsByGift[$gift6->getId()]['protected'],
            $allGiftPostsByGift[$gift6->getId()]['public'],
            // Gift7
            $allGiftPostsByGift[$gift7->getId()]['protected'],
            $allGiftPostsByGift[$gift7->getId()]['public'],
            // Gift8
            $allGiftPostsByGift[$gift8->getId()]['public'],
            $allGiftPostsByGift[$gift8->getId()]['protected']
        );

        $posts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Post::ENTITY_TYPE)
            ->addFieldToFilter('post_id', ['in' => array_keys($allGiftPosts)])
            ->getItems();
        $this->checkVisiblePosts($allGiftPosts, $expectedPosts, $posts);

        //
        // John
        //
        $this->setGeefterInSession($john);
        $this->enableGeefterAccessListeners();

        $expectedPosts = array_merge(
            // Gift3
            $allGiftPostsByGift[$gift3->getId()]['protected'],
            $allGiftPostsByGift[$gift3->getId()]['public'],
            // Gift4
            $allGiftPostsByGift[$gift4->getId()]['public'],
            $allGiftPostsByGift[$gift4->getId()]['protected'],
            // Gift8
            $allGiftPostsByGift[$gift8->getId()]['public'],
            // Gift9
            $allGiftPostsByGift[$gift9->getId()]['protected'],
            $allGiftPostsByGift[$gift9->getId()]['public'],
            // Gift10
            $allGiftPostsByGift[$gift10->getId()]['protected'],
            $allGiftPostsByGift[$gift10->getId()]['public']
        );

        $posts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Post::ENTITY_TYPE)
            ->addFieldToFilter('post_id', ['in' => array_keys($allGiftPosts)])
            ->getItems();
        $this->checkVisiblePosts($allGiftPosts, $expectedPosts, $posts);
    }

    protected function checkVisiblePosts($allGiftPosts, $expectedPosts, $actualPosts) {
        $this->getSecurityService()->callPrivileged(
            function() use ($expectedPosts, $actualPosts, $allGiftPosts): void {
                $expectedPostIds = [];
                /** @var Post $expectedPost */
                foreach ($expectedPosts as $i => $expectedPost) {
                    $this->assertNotNull($expectedPost, sprintf('Post at offset #%s is null and should not be.', $i));
                    if ($expectedPost !== null) {
                        $expectedPostIds[] = $expectedPost->getId();
                        $this->assertArrayHasKey(
                            $expectedPost->getId(),
                            $actualPosts,
                            sprintf(
                                'Post #%d on %s topic for %s should be visible by %s but is not.',
                                $expectedPost->getId(),
                                $expectedPost->getTopic()->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY),
                                $expectedPost->getTopic()->getEntity()->getLabel(),
                                $this->getGeefterSession()->getGeefter()->getUsername()
                            )
                        );
                    }
                }

                foreach (array_diff(array_keys($allGiftPosts), $expectedPostIds) as $unexpectedTopicId) {
                    $this->assertArrayNotHasKey(
                        $unexpectedTopicId,
                        $actualPosts,
                        sprintf(
                            'Post #%d on %s topic for %s should NOT be visible by %s but is.',
                            $unexpectedTopicId,
                            $allGiftPosts[$unexpectedTopicId]->getTopic()
                                ->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY),
                            $allGiftPosts[$unexpectedTopicId]->getTopic()->getEntity()->getLabel(),
                            $this->getGeefterSession()->getGeefter()->getUsername()
                        )
                    );
                }
            }
        );
        $this->assertCount(count($expectedPosts), $actualPosts);
    }

    /**
     * @param Gift[] $gifts
     * @return array<int, array<string, Post[]>>
     */
    protected function getGiftPosts($gifts): array {
        $posts = [];

        /** @var \Geeftlist\Helper\Gift\Discussion\Topic $topicHelper */
        $topicHelper = $this->getGeeftlistModelFactory()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
        foreach ($gifts as $gift) {
            $posts[$gift->getId()] = [];
            if ($protectedTopic = $topicHelper->getProtectedTopic($gift, true)) {
                $this->generateDummyPosts($protectedTopic);
                $posts[$gift->getId()]['protected'] = $protectedTopic->getPostCollection()->getItems();
            }

            if ($publicTopic = $topicHelper->getPublicTopic($gift, true)) {
                $this->generateDummyPosts($publicTopic);
                $posts[$gift->getId()]['public'] = $publicTopic->getPostCollection()->getItems();
            }
        }

        return $posts;
    }

    protected function generateDummyPosts(Topic $topic) {
        for ($i = $this->faker()->numberBetween(1, 10); $i >= 0; --$i) {
            $this->getGeeftlistModelFactory()->resolveMake(Post::ENTITY_TYPE)
                ->setData([
                    'title'   => $this->faker()->sentence(),
                    'message' => $this->faker()->paragraphs(2, true),
                    'topic_id' => $topic->getId()
                ])->save();
        }
    }
}
