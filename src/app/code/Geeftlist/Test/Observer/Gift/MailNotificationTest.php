<?php

namespace Geeftlist\Test\Observer\Gift;

use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Geeftlist\Model\Reservation;
use Geeftlist\Observer\Geeftee\MailNotification;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Gift\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->clearNotifications()
            ->clearCacheArrayObjects();
        $this->getGeefterSession()->invalidate();

        $this->getI18n()->setLanguage('en-US')
            ->setGlobalLanguage('en-US');
    }

    public function getCut() {
        return $this->getContainer()->get(MailNotification::class);
    }

    /**
     * @group ticket-525
     * @throws \DI\NotFoundException
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function testOnArchiveGiftNotifyGeeftersWithReservation(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        // Geeftees
        $giftGeeftee = $this->getRichard(true, '_giftGeeftee');

        // Geefters (creator, reservationHolder1, reservationHolder2, + 1 other geefter)
        $giftCreator = $this->getRichard(true, '_creator');
        $reservationHolder1 = $this->getRichard(true, '_reservationHolder1');
        $reservationHolder2 = $this->getRichard(true, '_reservationHolder2');

        /** @var Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . __FUNCTION__,
            'owner_id' => $this->getJohn()->getId() // owner does not matter
        ]])->save();
        $this->callPrivileged(
            static function () use ($family, $giftCreator, $reservationHolder1, $reservationHolder2, $giftGeeftee): void {
                $family->addGeeftee([
                    $giftCreator->getGeeftee(),
                    $reservationHolder1->getGeeftee(),
                    $reservationHolder2->getGeeftee(),
                    $giftGeeftee->getGeeftee(),
                ]);
            }
        );

        $this->disableListeners('geefternotification')
            ->enableListeners('index');

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftCreator->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $giftGeeftee->getGeeftee()->addGift($gift);

        // Create various reservations
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $giftCreator->getId()
        ]])->save();
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $reservationHolder1->getId()
        ]])->save();
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $reservationHolder2->getId()
        ]])->save();

        $this->setGeefterInSession($giftCreator);
        $this->enableListeners('geefternotification');

        // Archive gift
        $gift->setStatus(Gift\Field\Status::ARCHIVED)
            ->save();

        $this->disableGeefterAccessListeners();

        $sentMails = $fakeEmailSender->getSentEmails(true);
        $recipientEmails = [];
        foreach ($sentMails as $sentMail) {
            $recipientEmails[] = $sentMail['to'];
            $this->assertValidHtmlWithoutErrors($sentMail['body']);
        }

        // Check recipients
        $this->assertEqualsCanonicalizing(
            [$reservationHolder1->getEmail(), $reservationHolder2->getEmail()],
            $recipientEmails
        );
    }

    /**
     * @group ticket-442
     * @group ticket-267
     * @throws \DI\NotFoundException
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function testOnChangeGiftNotifyGeeftersWithReservation(): void {
        $fakeEmailSender = $this->getEmailSender();

        $this->disableListeners('geefternotification');

        // Geeftees
        $currentGiftGeeftee = $this->getRichard(true, '_currentGeeftee');
        $newGiftGeeftee = $this->getRichard(true, '_newGeeftee');

        // Geefters (creator, reservationHolder1, reservationHolder2, + 1 other geefter)
        $giftCreator = $this->getRichard(true, '_creator');
        $reservationHolder1 = $this->getRichard(true, '_reservationHolder1');
        $reservationHolder2 = $this->getRichard(true, '_reservationHolder2');
        $richard = $this->getRichard(true);

        // 1st family: {$currentGiftGeeftee, $giftCreator, $reservationHolder1, $reservationHolder2}
        /** @var Family $commonFamily1 */
        $commonFamily1 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . __FUNCTION__ . ' 1',
            'owner_id' => $this->getJohn()->getId() // owner does not matter
        ]])->save();
        $this->callPrivileged(
            static function () use ($commonFamily1, $currentGiftGeeftee, $giftCreator, $reservationHolder1, $reservationHolder2): void {
                $commonFamily1->addGeeftee([
                    $currentGiftGeeftee->getGeeftee(),
                    $giftCreator->getGeeftee(),
                    $reservationHolder1->getGeeftee(),
                    $reservationHolder2->getGeeftee(),
                ]);
            }
        );
        // 1st family: {$newGiftGeeftee, $giftCreator, $reservationHolder2, $richard}
        $commonFamily2 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . __FUNCTION__ . ' 2',
            'owner_id' => $this->getJane()->getId() // owner does not matter
        ]])->save();
        $this->callPrivileged(
            static function () use ($commonFamily2, $newGiftGeeftee, $giftCreator, $reservationHolder2, $richard): void {
                $commonFamily2->addGeeftee([
                    $newGiftGeeftee->getGeeftee(),
                    $giftCreator->getGeeftee(),
                    $reservationHolder2->getGeeftee(),
                    $richard->getGeeftee(),
                ]);
            }
        );

        $this->setGeefterInSession($giftCreator);
        $this->disableListeners('geefternotification')
            ->enableListeners('index');

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $giftCreator->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $currentGiftGeeftee->getGeeftee()->addGift($gift);

        // Create various reservations
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $giftCreator->getId()
        ]])->save();
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $reservationHolder1->getId()
        ]])->save();
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $reservationHolder2->getId()
        ]])->save();

        $this->enableListeners('geefternotification');

        // Simulate gift modification + reassignment
        $gift->addData([
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX) . '_new',
            'description' => 'foo',
            'estimated_price' => 12.34
        ])->save();
        $newGiftGeeftee->getGeeftee()->addGift($gift);

        $this->disableGeefterAccessListeners();

        // Prepare notifications (will merge previous events in a single notification)
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();

        // Check notification creation
        /** @var Notification $notification */
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Gift::ENTITY_TYPE,
            'target_id'    => $gift->getId(),
            'event_name'   => Notification\Gift\Constants::EVENT_CHANGE
        ]);

        $expectedChangedFields = [
            'label',
            'description',
            'estimated_price',
            'geeftees'
        ];

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());
        $this->assertEqualsCanonicalizing($expectedChangedFields, $notification->getEventParams()['changed_fields']);

        // Send emails now
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertNotEmpty($sentEmails);

        $expectedRecipientEmails = [
            //$giftCreator->getEmail(), // Not expected here, since his is the actor
            $reservationHolder1->getEmail(),
            $reservationHolder2->getEmail(),
        ];

        // Check emails content
        // Note: 2 notification items are supposed to be created:
        // add_to_geeftees (for family members) + change (for reservation holders)
        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertNotEquals($currentGiftGeeftee->getEmail(), $emailInfo['to']);
            $this->assertNotEquals($giftCreator->getEmail(), $emailInfo['to']);

            // Reservation holders should only receive the "change" notification
            if (in_array($emailInfo['to'], $expectedRecipientEmails)) {
                $this->assertStringContainsString(
                    'has modified the following fields on',
                    $emailInfo['body']
                );
                foreach ($expectedChangedFields as $expectedChangedField) {
                    $this->assertStringContainsString(
                        sprintf(
                            '<li>%s</li>',
                            $this->getI18n()->tr(sprintf('gift.field||%s', $expectedChangedField))
                        ),
                        $emailInfo['body']
                    );
                }
            }
            // Other members of $commonFamily2 should only receive the "add_to_geeftees" notification
            else {
                $this->assertStringNotContainsString(
                    'has modified the following fields on',
                    $emailInfo['body']
                );
            }

            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
        }

        // Check recipients
        $this->assertEqualsCanonicalizing(
            $expectedRecipientEmails,
            array_intersect($expectedRecipientEmails, $seenEmails)
        );
    }

    /**
     * @return \Geeftlist\Helper\Geeftee
     * @throws \DI\NotFoundException
     */
    protected function getGeefteeHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Geeftee::class);
    }
}
