<?php

namespace Geeftlist\Test\Observer\Discussion\Post;

use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Notification;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Observer\Family\MailNotification newCut()
 *
 * @group base
 * @group observers
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        $this->disableGeefterAccessListeners();
        $this->getGeefterSession()->invalidate();
        $this->clearNotifications()
            ->clearCacheArrayObjects();
    }

    public function testOnAddGiftPostNotifyFamily(): void {
        $fakeEmailSender = $this->getEmailSender();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $jane, $john, $richard): void {
            $commonFamily->addGeeftee([
                $jane->getGeeftee(),
                $john->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard->getGeeftee()->addGift($gift);
        $topic = $this->getTopicHelper()->getProtectedTopic($gift);

        $this->clearNotifications();
        $this->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'title'     => $this->faker()->words(3, true),
            'message'   => $this->faker()->paragraphs(2, true),
            'topic_id'  => $topic->getId(),
            'author_id' => $jane->getId()
        ]])->save();

        // 1. Check notification creation
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Post::ENTITY_TYPE,
            'target_id'    => $post->getId(),
            'event_name'   => Notification\Discussion\Post\Constants::EVENT_NEW
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());

        // 2. Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getLatestActivityMailNotificationCronObserver()->sendMails();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(1, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
            $this->assertMatchesRegularExpression(
                '/(Recent activity in your families)|(Activité récente dans vos familles)/',
                $emailInfo['subject']
            );
            $this->assertMatchesRegularExpression(
                '/' . $jane->getUsername() . ' (has posted a.*?new comment)|(a ajouté un.*?nouveau commentaire)/',
                $emailInfo['body']
            );
        }

        $this->assertContainsEquals($john->getEmail(), $seenEmails);
    }

    /**
     * @group ticket-632
     */
    public function testOnAddGiftWithRecipientsPostNotifyRecipients_invalidRecipients(): void {
        $fakeEmailSender = $this->getEmailSender();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richardGeeftee = $this->getRichard(true, 3);
        $this->generateFamily($richard, [$richard2, $richardGeeftee]);
        $gift = current($this->generateGifts($richard, $richardGeeftee));

        $topic = $this->getTopicHelper()->getProtectedTopic($gift);

        $this->clearNotifications();
        $this->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard2);

        $title = $this->faker()->words(3, true);
        $message = $this->faker()->paragraphs(2, true);
        $recipientIds = [$richardGeeftee->getId(), $this->getJohn()->getId()];
        /** @var Post $post */
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'title'         => $title,
            'message'       => $message,
            'topic_id'      => $topic->getId(),
            'author_id'     => $richard2->getId(),
            'recipient_ids' => $recipientIds
        ]]);
        $this->getRepository(Post::ENTITY_TYPE)->save($post);

        // 1. Check notification creation
        // => *Not* forbidden although the recipient ID is invalid for this topic.
        /** @var Notification $notification */
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Post::ENTITY_TYPE,
            'target_id'    => $post->getId(),
            'event_name'   => Notification\Discussion\Post\Constants::EVENT_NEW_RECIPIENTS
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());
        $eventParams = $notification->getEventParams();
        $this->assertEquals($title, $eventParams['title']);
        $this->assertEquals($message, $eventParams['message']);
        $this->assertEqualsCanonicalizing($recipientIds, $eventParams['recipient_ids']);
        $this->assertEquals($post->getCreatedAt(), $eventParams['post_date']);

        // 2. Check notifications sending
        // => Here permissions check should prevent sending the notification to any invalid recipients
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getPostMentionMailNotificationCronObserver()->sendMessageToRecipients();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(0, count($sentEmails));
    }

    /**
     * @group ticket-632
     */
    public function testOnAddGiftWithRecipientsPostNotifyRecipients_validRecipients(): void {
        $fakeEmailSender = $this->getEmailSender();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richardGeeftee = $this->getRichard(true, 3);
        $this->generateFamily($richard, [$richard2, $richardGeeftee]);
        $gift = current($this->generateGifts($richard, $richardGeeftee));

        $topic = $this->getTopicHelper()->getProtectedTopic($gift);

        $this->clearNotifications();
        $this->getEmailSender()->clearSentEmails();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard2);

        $title = $this->faker()->words(3, true);
        $message = $this->faker()->paragraphs(2, true);
        $recipientIds = [$richard->getId()];
        /** @var Post $post */
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'title'         => $title,
            'message'       => $message,
            'topic_id'      => $topic->getId(),
            'author_id'     => $richard2->getId(),
            'recipient_ids' => $recipientIds
        ]]);
        $this->getRepository(Post::ENTITY_TYPE)->save($post);

        // 1. Check notification creation
        /** @var Notification $notification */
        $notification = $this->getEntity(Notification::ENTITY_TYPE, [
            'target_type'  => Post::ENTITY_TYPE,
            'target_id'    => $post->getId(),
            'event_name'   => Notification\Discussion\Post\Constants::EVENT_NEW_RECIPIENTS
        ]);

        $this->assertNotNull($notification);
        $this->assertNotNull($notification->getId());
        $eventParams = $notification->getEventParams();
        $this->assertEquals($title, $eventParams['title']);
        $this->assertEquals($message, $eventParams['message']);
        $this->assertEqualsCanonicalizing($recipientIds, $eventParams['recipient_ids']);
        $this->assertEquals($post->getCreatedAt(), $eventParams['post_date']);

        // 2. Check notifications sending
        $this->getMailNotificationCronObserver()->prepareNotifications()
            ->prepareMailRecipients();
        $this->getPostMentionMailNotificationCronObserver()->sendMessageToRecipients();

        $sentEmails = $fakeEmailSender->getSentEmails();
        $this->assertEquals(1, count($sentEmails));

        $seenEmails = [];
        foreach ($sentEmails as $emailInfo) {
            $seenEmails[] = $emailInfo['to'];
            $this->assertValidHtmlWithoutErrors($emailInfo['body'], $emailInfo['body']);
            $this->assertMatchesRegularExpression(
                '/(You have been mentioned in a discussion on)|(Vous avez été mentionné\(e\) dans la discussion)/',
                $emailInfo['subject']
            );
            $this->assertMatchesRegularExpression(
                '/' . $richard2->getUsername() . ' (has mentioned you on)|(vous a mentionné\(e\) dans la discussion)/',
                $emailInfo['body']
            );
        }

        $this->assertEqualsCanonicalizing([$richard->getEmail()], $seenEmails);
    }

    /**
     * @return \Geeftlist\Helper\Gift\Discussion\Topic
     */
    public function getTopicHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
    }

    /**
     * @return \Geeftlist\Observer\Cron\MailNotification\Geefter\Discussion\PostMentionNotification
     */
    public function getPostMentionMailNotificationCronObserver() {
        return $this->getContainer()->get(
            \Geeftlist\Observer\Cron\MailNotification\Geefter\Discussion\PostMentionNotification::class
        );
    }
}
