<?php

namespace Geeftlist\Test\Helper\Gift\Discussion;


use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group helpers
 */
class TopicTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    /**
     * @return \Geeftlist\Helper\Gift\Discussion\Topic
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
    }

    public function testGetGiftPublicTopic_CreatorIsNotGeeftee(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $stranger = $this->getRichard(true, '_stranger');

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('John-Jane-Richard Common Family '),
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($john, $jane, $richard, $commonFamily): void {
            $commonFamily->addGeeftee([
                $john->getGeeftee(),
                $jane->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $cut = $this->getCut();

        $newGiftForRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . uniqid('Gift from John to Richard '),
            'creator_id' => $john->getId(),
        ]])->save();

        $this->enableGeefterAccessListeners();

        $richard->getGeeftee()->addGift($newGiftForRichard);

        // Topic access with John (creator, related)
        $this->setGeefterInSession($john);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNotNull($publicTopic);

        // Topic access with Jane (only related)
        $this->setGeefterInSession($jane);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNotNull($publicTopic);

        // Topic access with Richard (geeftee)
        $this->setGeefterInSession($richard);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNull($publicTopic);

        // Topic access with stranger (unrelated)
        $this->setGeefterInSession($stranger);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNull($publicTopic);
    }

    public function testGetGiftProtectedTopic_CreatorIsNotGeeftee(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $stranger = $this->getRichard(true, '_stranger');

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('John-Jane-Richard Common Family '),
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($john, $jane, $richard, $commonFamily): void {
            $commonFamily->addGeeftee([
                $john->getGeeftee(),
                $jane->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $cut = $this->getCut();

        $newGiftForRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . uniqid('Gift from John to Richard '),
            'creator_id' => $john->getId(),
        ]])->save();

        $this->enableGeefterAccessListeners();

        $richard->getGeeftee()->addGift($newGiftForRichard);

        // Topic access with John (creator, related)
        $this->setGeefterInSession($john);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNotNull($protectedTopic);
        $this->assertEquals(
            Gift\Constants::TOPIC_VISIBILITY_PROTECTED,
            $protectedTopic->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)
        );
        $this->assertEquals(Gift::ENTITY_TYPE, $protectedTopic->getLinkedEntityType());
        $this->assertEquals($newGiftForRichard->getId(), $protectedTopic->getLinkedEntityId());
        $this->assertNotNull($protectedTopic->getPostCollection()->load());
        $protectedTopicId = $protectedTopic->getId();    // Saved for tests with direct loading

        $protectedTopic = $this->getEntity(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE, $protectedTopicId);
        $this->assertNotNull($protectedTopic->getId());

        // Topic access with Jane (only related)
        $this->setGeefterInSession($jane);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNotNull($protectedTopic);
        $this->assertEquals(
            Gift\Constants::TOPIC_VISIBILITY_PROTECTED,
            $protectedTopic->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)
        );
        $this->assertEquals(Gift::ENTITY_TYPE, $protectedTopic->getLinkedEntityType());
        $this->assertEquals($newGiftForRichard->getId(), $protectedTopic->getLinkedEntityId());
        $this->assertNotNull($protectedTopic->getPostCollection()->load());

        $protectedTopic = $this->getEntity(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE, $protectedTopicId);
        $this->assertNotNull($protectedTopic->getId());

        // Topic access with Richard (geeftee)
        $this->setGeefterInSession($richard);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNull($protectedTopic);

        try {
            $this->getEntity(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE, $protectedTopicId);
            $this->fail('Failed preventing loading gift protected topic by Richard');
        }
        catch (PermissionException $permissionException) {}

        // Topic access with stranger (unrelated)
        $this->setGeefterInSession($stranger);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNull($protectedTopic);
        try {
            $this->getEntity(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE, $protectedTopicId);
            $this->fail('Failed preventing loading gift protected topic by stranger');
        }
        catch (PermissionException) {}
    }

    public function testGetGiftPublicTopic_CreatorIsGeeftee(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $richard = $this->getRichard();
        $stranger = $this->getRichard(true, '_stranger');

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Jane-Richard Common Family '),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($jane, $richard, $commonFamily): void {
            $commonFamily->addGeeftee([
                $jane->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $cut = $this->getCut();

        $newGiftForRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . uniqid('Gift from Richard to himself '),
            'creator_id' => $richard->getId(),
        ]])->save();

        $this->enableGeefterAccessListeners();

        $richard->getGeeftee()->addGift($newGiftForRichard);

        // Topic access with Jane (only related)
        $this->setGeefterInSession($jane);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNotNull($publicTopic);
        $this->assertEquals(
            Gift\Constants::TOPIC_VISIBILITY_PUBLIC,
            $publicTopic->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)
        );
        $this->assertEquals(Gift::ENTITY_TYPE, $publicTopic->getLinkedEntityType());
        $this->assertEquals($newGiftForRichard->getId(), $publicTopic->getLinkedEntityId());
        $this->assertNotNull($publicTopic->getPostCollection()->load());

        // Topic access with Richard (creator, geeftee)
        $this->setGeefterInSession($richard);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNotNull($publicTopic);
        $this->assertEquals(
            Gift\Constants::TOPIC_VISIBILITY_PUBLIC,
            $publicTopic->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)
        );
        $this->assertEquals(Gift::ENTITY_TYPE, $publicTopic->getLinkedEntityType());
        $this->assertEquals($newGiftForRichard->getId(), $publicTopic->getLinkedEntityId());
        $this->assertNotNull($publicTopic->getPostCollection()->load());

        // Topic access with stranger (unrelated)
        $this->setGeefterInSession($stranger);

        $publicTopic = $cut->getPublicTopic($newGiftForRichard);
        $this->assertNull($publicTopic);
    }

    public function testGetGiftProtectedTopic_CreatorIsGeeftee(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $richard = $this->getRichard();
        $stranger = $this->getRichard(true, '_stranger');

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Jane-Richard Common Family '),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($jane, $richard, $commonFamily): void {
            $commonFamily->addGeeftee([
                $jane->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $cut = $this->getCut();

        $newGiftForRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . uniqid('Gift from Richard to himself '),
            'creator_id' => $richard->getId(),
        ]])->save();

        $this->enableGeefterAccessListeners();

        $richard->getGeeftee()->addGift($newGiftForRichard);

        // Topic access with Jane (only related)
        $this->setGeefterInSession($jane);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNotNull($protectedTopic);
        $this->assertEquals(
            Gift\Constants::TOPIC_VISIBILITY_PROTECTED,
            $protectedTopic->getMetadataByKey(Gift\Constants::TOPIC_VISIBILITY_KEY)
        );
        $this->assertEquals(Gift::ENTITY_TYPE, $protectedTopic->getLinkedEntityType());
        $this->assertEquals($newGiftForRichard->getId(), $protectedTopic->getLinkedEntityId());
        $this->assertNotNull($protectedTopic->getPostCollection()->load());

        // Topic access with Richard (creator, geeftee)
        $this->setGeefterInSession($richard);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNull($protectedTopic);

        // Topic access with stranger (unrelated)
        $this->setGeefterInSession($stranger);

        $protectedTopic = $cut->getProtectedTopic($newGiftForRichard);
        $this->assertNull($protectedTopic);
    }
}
