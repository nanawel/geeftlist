<?php

namespace Geeftlist\Test\Helper\Geefter;


use Geeftlist\Exception\Geefter\EmailUpdateException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Helper\Geefter\EmailUpdate newCut()
 *
 * @group base
 * @group helpers
 */
class EmailUpdateTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->initFixedGeefters();
        $this->getGeefterSession()->invalidate();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
        $this->resetFixedGeefters();
    }

    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Helper\Geefter\EmailUpdate::class);
    }

    public function testRequestEmailUpdateEmptyEmail(): void {
        $john = $this->getJohn();

        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());

        $this->getCut()->requestEmailUpdate($john, ' ');

        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());
    }

    public function testRequestEmailUpdateSameAsCurrentEmail(): void {
        $john = $this->getJohn();

        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());

        $this->getCut()->requestEmailUpdate($john, $john->getEmail());

        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());
    }

    public function testRequestEmailUpdate(): void {
        $john = $this->getJohn();

        $newEmail = 'johndoe@gmail.com';

        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());

        $this->getCut()->requestEmailUpdate($john, $newEmail);

        $this->assertEquals($newEmail, $john->getPendingEmail());
        $this->assertNotEmpty($john->getPendingEmailToken());
    }

    public function testCancelEmailUpdate(): void {
        $john = $this->getJohn()
            ->setPendingEmail('johndoe@gmail.com')
            ->setPendingEmailToken('test-token');

        $this->assertNotEmpty($john->getPendingEmail());
        $this->assertNotEmpty($john->getPendingEmailToken());

        $this->getCut()->cancelEmailUpdate($john);

        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());
    }

    public function testGetGeefterFromToken(): void {
        $john = $this->getJohn()
            ->setPendingEmailToken('john-token')
            ->save();
        $jane = $this->getJane()
            ->setPendingEmailToken('jane-token')
            ->save();

        $jane2 = $this->getCut()->getGeefterFromToken('jane-token');
        $this->assertEquals($jane->getId(), $jane2->getId());
        $john2 = $this->getCut()->getGeefterFromToken('john-token');
        $this->assertEquals($john->getId(), $john2->getId());

        try {
            $this->getCut()->getGeefterFromToken('invalid-token');
            $this->fail('Failed throwing exception on non-existent token');
        }
        catch (\Throwable) {}
    }

    public function testApplyEmailUpdateGeefterExists(): void {
        $johnNewEmail = 'johndoe+newmail@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $john = $this->getJohn()
            ->setPendingEmail($johnNewEmail)
            ->setPendingEmailToken('john-token')
            ->save();
        $johnEmail = $john->getEmail();

        $johnUsurper = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => 'John Doe Usurper',
            'email'    => $johnNewEmail,
            'password' => \Geeftlist\Test\Constants::GEEFTER_OTHERS_PASSWORD
        ]])->save();

        $this->assertEquals($johnNewEmail, $johnUsurper->getEmail());

        try {
            $this->getCut()->applyEmailUpdate('john-token');
            $this->fail('Failed preventing updating email with existing geefter email.');
        }
        catch (EmailUpdateException) {}

        $john = $this->getJohn();
        $johnUsurper = $this->getEntity(Geefter::ENTITY_TYPE, $johnUsurper->getId());

        $this->assertEquals($johnEmail, $john->getEmail());
        $this->assertEquals($johnNewEmail, $johnUsurper->getEmail());
    }

    public function testApplyEmailUpdateGeefterlessGeefteeExists(): void {
        $jane = $this->getJane();
        $johnNewEmail = 'johndoe+newmail@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $john = $this->getJohn()
            ->setPendingEmail($johnNewEmail)
            ->setPendingEmailToken('john-token')
            ->save();
        $john->getEmail();

        $this->assertNotEquals($johnNewEmail, $john->getEmail());

        /** @var Geeftee $geefterlessGeeftee */
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'  => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'email' => $johnNewEmail
        ]])->save();
        /** @var Family $geefteeFamily */
        $geefteeFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'       => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($jane, $geefterlessGeeftee, $geefteeFamily): void {
            $geefteeFamily->addGeeftee([$jane->getGeeftee(), $geefterlessGeeftee]);
        });
        /** @var Gift $geefteeGift */
        $geefteeGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => $this->faker()->words(3, true),
            'creator_id' => $jane->getId()
        ]])->save();
        $geefterlessGeeftee->addGift($geefteeGift);

        $this->enableGeefterAccessListeners();

        $this->assertTrue($geefteeFamily->isMember($geefterlessGeeftee));
        $this->assertTrue($geefteeFamily->isMember($jane->getGeeftee()));
        $this->assertFalse($geefteeFamily->isMember($john->getGeeftee()));
        $this->assertEquals($geefteeGift->getId(), $geefterlessGeeftee->getGiftCollection()->getFirstItem()->getId());
        $this->assertNotContainsEquals($geefteeGift->getId(), $john->getGeeftee()->getGiftCollection()->getAllIds());

        $this->getCut()->applyEmailUpdate('john-token');

        $this->assertFalse($geefteeFamily->isMember($geefterlessGeeftee));
        $this->assertTrue($geefteeFamily->isMember($jane->getGeeftee()));
        $this->assertTrue($geefteeFamily->isMember($john->getGeeftee()));
        $this->assertNull($this->getEntity(Geeftee::ENTITY_TYPE, $geefterlessGeeftee->getId()));
        $this->assertContainsEquals($geefteeGift->getId(), $john->getGeeftee()->getGiftCollection()->getAllIds());
    }

    public function testApplyEmailUpdate(): void {
        $johnNewEmail = 'johndoe+newmail@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $john = $this->getJohn()
            ->setPendingEmail($johnNewEmail)
            ->setPendingEmailToken('john-token')
            ->save();
        $janeNewEmail = 'janedoe+newmail@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $jane = $this->getJane()
            ->setPendingEmail($janeNewEmail)
            ->setPendingEmailToken('jane-token')
            ->save();
        $janeEmail = $jane->getEmail();

        $this->assertNotEquals($johnNewEmail, $john->getEmail());
        $this->assertNotEquals($janeNewEmail, $janeEmail);

        $this->getCut()->applyEmailUpdate('john-token');

        $john = $this->getJohn();
        $jane = $this->getJane();

        $this->assertEquals($johnNewEmail, $john->getEmail());
        $this->assertEmpty($john->getPendingEmail());
        $this->assertEmpty($john->getPendingEmailToken());
        $this->assertEquals($janeEmail, $jane->getEmail());
    }
}
