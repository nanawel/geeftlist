<?php

namespace Geeftlist\Test\Helper;


use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Service\Event;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group helpers
 */
class SecurityTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /** @var \OliveOil\Core\Service\EventInterface|null */
    protected static $defaultEventService;

    /** @var \Laminas\EventManager\SharedEventManager|null */
    protected static $sharedEventManager;

    /** @var bool */
    protected $defaultObserverCalled = false;

    protected function setUp(): void {
        self::$sharedEventManager = $this->getContainer()->make(\Laminas\EventManager\SharedEventManager::class);
        self::$defaultEventService = $this->getContainer()->make(Event::class, [
            // Cannot use cache with Closures
            'cache' => $this->getContainer()->get('OliveOil\Core\Service\Cache\Void'),
            'sharedEventManager' => self::$sharedEventManager,
                'eventSpace' => 'default',
                'observerProvider' => $this->getObserverProviderMock([
                    'uniqueId1' => [
                        'identifier' => 'stdClass',
                        'priority' => '90',
                        'eventName' => 'my_default_event',
                        'callback' => function (): void {
                            $this->defaultObserverCalled = true;
                        },
                    ]
                ])
            ])->setupListeners();
    }

    protected function tearDown(): void {
        self::$sharedEventManager = null;
        self::$defaultEventService = null;
        $this->defaultObserverCalled = false;
    }

    /**
     * @return \Geeftlist\Service\Security
     */
    public function getCut() {
        return $this->getContainer()->make(
            \Geeftlist\Service\Security::class, [
            'securityEventService' => $this->getContainer()->make(Event::class, [
                // Cannot use cache with Closures
                'cache' => $this->getContainer()->get('OliveOil\Core\Service\Cache\Void'),
                'eventSpace' => 'security',
                'sharedEventManager' => self::$sharedEventManager,
                'observerProvider' => $this->getObserverProviderMock([
                    'uniqueId2' => [
                        'identifier' => 'stdClass',
                        'priority' => '80',
                        'eventName' => 'my_security_event',
                        'callback' => static function (): void {
                            throw new PermissionException('my_security_event');
                        },
                    ]
                ])
            ])->setupListeners()
        ]);
    }

    public function testCallPrivileged(): void {
        $cut = $this->getCut();
        $target = new class() extends \stdClass {}; // "extends" only to have a valid identifier when triggering events

        $evManagerDefault = $cut->getSecurityEventService()->getEventManager($target);

        $this->assertFalse($this->defaultObserverCalled);

        $evManagerDefault->trigger('my_default_event', $target);

        $this->assertTrue($this->defaultObserverCalled);

        try {
            $evManagerDefault->trigger('my_security_event', $target);
            $this->fail('Failed triggering expected observer.');
        }
        catch (PermissionException $permissionException) {
            $this->assertEquals('my_security_event', $permissionException->getMessage());
        }

        $this->defaultObserverCalled = false;

        $cut->callPrivileged(function() use ($evManagerDefault, $target, $cut): void {
            $evManagerDefault->trigger('my_security_event', $target);   // should not throw exception now

            $this->assertFalse($this->defaultObserverCalled);

            $evManagerDefault->trigger('my_default_event', $target);

            $this->assertTrue($this->defaultObserverCalled);

            $this->defaultObserverCalled = false;

            // Test nested call
            $cut->callPrivileged(function() use ($evManagerDefault, $target, $cut): void {
                $evManagerDefault->trigger('my_security_event', $target);   // should not throw exception either

                $this->assertFalse($this->defaultObserverCalled);

                $evManagerDefault->trigger('my_default_event', $target);

                $this->assertTrue($this->defaultObserverCalled);

                $this->defaultObserverCalled = false;
            });

            $evManagerDefault->trigger('my_security_event', $target);   // still not

            $this->assertFalse($this->defaultObserverCalled);

            $evManagerDefault->trigger('my_default_event', $target);

            $this->assertTrue($this->defaultObserverCalled);

            $this->defaultObserverCalled = false;
        });

        // Back outside privileged context, should call security observer again
        try {
            $evManagerDefault->trigger('my_security_event', $target);
            $this->fail('Failed triggering expected observer.');
        }
        catch (PermissionException $permissionException) {
            $this->assertEquals('my_security_event', $permissionException->getMessage());
        }
    }

    protected function getObserverProviderMock($definitions) {
        $mock = $this->createMock(\OliveOil\Core\Service\Event\ProviderInterface::class);
        $mock->method('getDefinitions')
            ->willReturn($definitions);

        return $mock;
    }
}
