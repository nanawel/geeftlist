<?php

namespace Geeftlist\Test\Helper\Family;


use Geeftlist\Exception\Family\AlreadyMemberException;
use Geeftlist\Exception\Family\MembershipException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Helper\Family\Membership newCut()
 *
 * @group base
 * @group helpers
 */
class MembershipTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Helper\Family\Membership::class);
    }

    /**
     * @param string $visibility
     * @return Family
     */
    protected function newFamily(Geefter $owner, $visibility) {
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'       => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Membership Test Family '),
            'owner_id'   => $owner->getId(),
            'visibility' => $visibility
        ]])->save();

        return $this->callPrivileged(static fn() => $family->addGeeftee($owner->getGeeftee()));
    }

    /**
     * @param string $familyVisibility
     */
    protected function prepareRequestTest($familyVisibility): array {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Geefter $richard */
        $richard = $this->getEntity(
            Geefter::ENTITY_TYPE,
            ['email' => 'richard.roe@' . Constants::SAMPLEDATA_EMAIL_DOMAIN]
        );
        if ($richard) {
            $richard->deleteWithGeeftee();
        }

        /** @var Geefter $richard */
        $richard = $this->getRichard();

        /** @var Family $family */
        $family = $this->newFamily($john, $familyVisibility);

        return [
            'john'    => $john,
            'jane'    => $jane,
            'richard' => $richard,
            'family'  => $family
        ];
    }

    public function testRequestPublicFamily(): void {
        $vars = $this->prepareRequestTest(Family\Field\Visibility::PUBLIC);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PUBLIC, $family->getVisibility());

        $this->disableGeefterAccessListeners();

        $this->assertTrue($family->isMember($john->getGeeftee()));
        $this->assertTrue($family->isOwner($john));
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->enableGeefterAccessListeners();
        $cut = $this->getCut();

        $this->setGeefterInSession($jane);
        $mr = $cut->request($family, $jane->getGeeftee());
        $membershipRequestId = $mr->getId();

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mr->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));

        try {
            $cut->request($family, $jane->getGeeftee());
            $this->fail('Failed preventing adding an existing member.');
        }
        catch (AlreadyMemberException) {}

        $this->disableGeefterAccessListeners();

        $this->assertEquals(
            Family\MembershipRequest::DECISION_CODE_ACCEPTED,
            $this->getEntity(Family\MembershipRequest::ENTITY_TYPE, $membershipRequestId)
                ->getDecisionCode()
        );

        $this->callPrivileged(static function () use ($family, $jane): void {
            $family->removeGeeftee($jane->getGeeftee());
        });
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->enableGeefterAccessListeners();

        $this->setGeefterInSession($richard);

        try {
            $cut->request($family, $jane->getGeeftee(), $richard);
            $this->fail('Failed preventing a non-member from inviting a geeftee into a family.');
        }
        catch (PermissionException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($john);

        $mrCreatedByJohn = $cut->request($family, $jane->getGeeftee(), $john);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mrCreatedByJohn->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        //
        // PENDING MEMBERSHIP REQUEST ACCEPT/REJECT BY GEEFTEE
        //

        try {
            $cut->acceptRequest($mrCreatedByJohn);
            $this->fail('Failed preventing accepting an invitation to a public family for someone else.');
        }
        catch (MembershipException $membershipException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);
        try {
            $cut->acceptRequest($mrCreatedByJohn);
            $this->fail('Failed preventing accepting an invitation to a public family for someone else.');
        }
        catch (MembershipException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($jane);

        $cut->acceptRequest($mrCreatedByJohn);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mrCreatedByJohn->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));
    }

    public function testRequestProtectedFamily(): void {
        $vars = $this->prepareRequestTest(Family\Field\Visibility::PROTECTED);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PROTECTED, $family->getVisibility());

        $this->disableGeefterAccessListeners();

        $this->assertTrue($family->isMember($john->getGeeftee()));
        $this->assertTrue($family->isOwner($john));
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->enableGeefterAccessListeners();
        $cut = $this->getCut();

        $this->setGeefterInSession($jane);
        $mrCreatedByJane = $cut->request($family, $jane->getGeeftee());

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mrCreatedByJane->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $mrCreatedByJane->delete();

        $richard2 = $this->getRichard(true, 2);
        $this->setGeefterInSession($richard2);

        try {
            $cut->request($family, $jane->getGeeftee());
            $this->fail(sprintf(
                'Failed preventing a non-member (%s <%s>) from inviting a geeftee into family %s.',
                $richard2->getUsername(),
                $richard2->getEmail(),
                $family
            ));
        }
        catch (PermissionException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->disableGeefterAccessListeners();

        $mrCreatedByJane->delete();
        $this->callPrivileged(static function () use ($richard2, $family): void {
            $family->addGeeftee($richard2->getGeeftee());
        });

        $this->enableGeefterAccessListeners();

        $mrCreatedByAMember = $cut->request($family, $jane->getGeeftee());

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mrCreatedByAMember->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->disableGeefterAccessListeners();

        $this->callPrivileged(static function () use ($family, $jane): void {
            $family->removeGeeftee($jane->getGeeftee());
        });
        $mrCreatedByAMember->delete();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $mrCreatedByJohn = $cut->request($family, $jane->getGeeftee());

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mrCreatedByJohn->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        //
        // PENDING MEMBERSHIP REQUEST ACCEPT/REJECT BY GEEFTEE
        //

        try {
            $cut->acceptRequest($mrCreatedByJohn);
            $this->fail('Failed preventing accepting an invitation to a protected family for someone else.');
        }
        catch (MembershipException $membershipException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);
        try {
            $cut->acceptRequest($mrCreatedByJohn);
            $this->fail('Failed preventing accepting an invitation to a protected family for someone else.');
        }
        catch (MembershipException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($jane);

        $cut->acceptRequest($mrCreatedByJohn);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mrCreatedByJohn->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));
    }

    public function testRequestPrivateFamily(): void {
        $vars = $this->prepareRequestTest(Family\Field\Visibility::PRIVATE);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PRIVATE, $family->getVisibility());

        $this->disableGeefterAccessListeners();

        $this->assertTrue($family->isMember($john->getGeeftee()));
        $this->assertTrue($family->isOwner($john));
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->enableGeefterAccessListeners();
        $cut = $this->getCut();

        $this->setGeefterInSession($jane);
        $mr = $cut->request($family, $jane->getGeeftee());

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_REJECTED, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);

        try {
            $cut->request($family, $jane->getGeeftee());
            $this->fail('Failed preventing a non-member from inviting a geeftee into a family.');
        }
        catch (PermissionException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($john);

        $mrCreatedByJohn = $cut->request($family, $jane->getGeeftee());

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mrCreatedByJohn->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        //
        // PENDING MEMBERSHIP REQUEST ACCEPT/REJECT BY GEEFTEE
        //

        try {
            $cut->acceptRequest($mrCreatedByJohn);
            $this->fail('Failed preventing accepting an invitation to a private family for someone else.');
        }
        catch (MembershipException $membershipException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);
        try {
            $cut->acceptRequest($mrCreatedByJohn);
            $this->fail('Failed preventing accepting an invitation to a private family for someone else.');
        }
        catch (MembershipException) {}

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($jane);

        $cut->acceptRequest($mrCreatedByJohn);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mrCreatedByJohn->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));
    }

    public function testAcceptNotPendingRequest(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PUBLIC);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $cut = $this->getCut();

        $this->setGeefterInSession($john);

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->setGeefterInSession($jane);
        $cut->acceptRequest($mr);
        $this->assertTrue($family->isMember($jane->getGeeftee()));

        $this->enableGeefterAccessListeners();

        try {
            $cut->acceptRequest($mr);
            $this->fail('Failed detecting an already closed request.');
        }
        catch (MembershipException) {}

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mr->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));
    }

    public function testAcceptAlreadyMember(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PROTECTED);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $cut = $this->getCut();

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($john);

        $this->callPrivileged(static function () use ($jane, $family): void {
            $family->addGeeftee($jane->getGeeftee());
        });

        $this->assertTrue($family->isMember($jane->getGeeftee()));

        try {
            $cut->acceptRequest($mr);
            $this->fail('Failed detecting a request for a geeftee  who is already a member of the family.');
        }
        catch (AlreadyMemberException) {}

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_CANCELED, $mr->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));
    }

    public function testAcceptRequestForProtectedFamily(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PROTECTED);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PROTECTED, $family->getVisibility());

        $cut = $this->getCut();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);

        try {
            $cut->acceptRequest($mr);
            $this->fail('Failed preventing a non-member from accepting a membership request.');
        }
        catch (PermissionException $permissionException) {}

        $this->disableGeefterAccessListeners();

        $this->callPrivileged(static function () use ($richard, $family): void {
            $family->addGeeftee($richard->getGeeftee());
        });
        $this->assertTrue($family->isMember($richard->getGeeftee()));

        $this->enableGeefterAccessListeners();

        try {
            $cut->acceptRequest($mr);
            $this->fail('Failed preventing a non-owner from accepting a membership request.');
        }
        catch (PermissionException) {}

        $this->setGeefterInSession($john);

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $cut->acceptRequest($mr);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mr->getDecisionCode());
        $this->assertTrue($family->isMember($jane->getGeeftee()));
    }

//    public function testAcceptRequestForPrivateFamily() {
//        $this->markTestSkipped('TODO (Nothing different from protected for now)');
//    }

    public function testCancelRequestForProtectedFamily(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PROTECTED);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $cut = $this->getCut();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);

        try {
            $cut->cancelRequest($mr);
            $this->fail('Failed preventing a stranger from cancelling a membership request.');
        }
        catch (PermissionException) {}

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($jane);

        $cut->cancelRequest($mr);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_CANCELED, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));
    }

    public function testCancelInvitation(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PUBLIC);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $cut = $this->getCut();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);

        try {
            $cut->cancelRequest($mr);
            $this->fail('Failed preventing a stranger from cancelling an invitation.');
        }
        catch (PermissionException) {}

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($john);

        $cut->cancelRequest($mr);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_CANCELED, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));
    }

    public function testRejectRequest(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PROTECTED);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PROTECTED, $family->getVisibility());

        $cut = $this->getCut();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);

        try {
            $cut->rejectRequest($mr);
            $this->fail('Failed preventing a non-member from rejecting a membership request.');
        }
        catch (PermissionException $permissionException) {}

        $this->disableGeefterAccessListeners();

        $this->callPrivileged(static function () use ($richard, $family): void {
            $family->addGeeftee($richard->getGeeftee());
        });
        $this->assertTrue($family->isMember($richard->getGeeftee()));

        $this->enableGeefterAccessListeners();

        try {
            $cut->rejectRequest($mr);
            $this->fail('Failed preventing a non-owner from rejecting a membership request.');
        }
        catch (PermissionException) {}

        $this->setGeefterInSession($john);

        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $cut->rejectRequest($mr);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_REJECTED, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));
    }

    public function testRejectInvitation(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PUBLIC);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PUBLIC, $family->getVisibility());

        $cut = $this->getCut();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $mr = $cut->request($family, $jane->getGeeftee());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($richard);

        try {
            $cut->rejectRequest($mr);
            $this->fail('Failed preventing a stranger from cancelling an invitation.');
        }
        catch (MembershipException) {}

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_PENDING, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));

        $this->setGeefterInSession($jane);

        $cut->rejectRequest($mr);

        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_REJECTED, $mr->getDecisionCode());
        $this->assertFalse($family->isMember($jane->getGeeftee()));
    }

    public function testInviteGeefterlessGeeftee(): void {
        $this->disableGeefterAccessListeners();

        $vars = $this->prepareRequestTest(Family\Field\Visibility::PUBLIC);
        /** @var Family $family */
        /** @var Geefter $john */
        /** @var Geefter $jane */
        /** @var Geefter $richard */
        extract($vars);

        $this->assertEquals(Family\Field\Visibility::PUBLIC, $family->getVisibility());

        $cut = $this->getCut();
        /** @var Geeftee $geefterlessGeeftee */
        $geefterlessGeeftee = $this->getEntity(Geeftee::ENTITY_TYPE, ['geefter_id' => ['null' => true]]);
        $this->assertNotNull($geefterlessGeeftee);
        $this->assertFalse($family->isMember($geefterlessGeeftee));

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        // Public family
        $mr = $cut->request($family, $geefterlessGeeftee);
        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mr->getDecisionCode());
        $this->assertTrue($family->isMember($geefterlessGeeftee));

        // Protected family
        $protectedFamily = $this->newFamily($john, Family\Field\Visibility::PROTECTED);
        $this->assertFalse($protectedFamily->isMember($geefterlessGeeftee));

        $mr = $cut->request($protectedFamily, $geefterlessGeeftee);
        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mr->getDecisionCode());
        $this->assertTrue($protectedFamily->isMember($geefterlessGeeftee));

        // Private family
        $privateFamily = $this->newFamily($john, Family\Field\Visibility::PRIVATE);
        $this->assertFalse($privateFamily->isMember($geefterlessGeeftee));

        $mr = $cut->request($privateFamily, $geefterlessGeeftee);
        $this->assertEquals(Family\MembershipRequest::DECISION_CODE_ACCEPTED, $mr->getDecisionCode());
        $this->assertTrue($privateFamily->isMember($geefterlessGeeftee));
    }
}
