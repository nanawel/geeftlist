<?php

namespace Geeftlist\Test\Helper;


use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group helpers
 */
class GeefteeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Helper\Geeftee
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Helper\Geeftee::class);
    }

    /**
     * @return \Geeftlist\Model\Geeftee
     */
    public function newGeeftee() {
        return $this->newEntity(Geeftee::ENTITY_TYPE);
    }

    /**
     * @return \Geeftlist\Model\Geeftee
     */
    public function getGeeftee(): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newGeeftee()->setData([
            'name'      => $this->faker()->name,
            'email'     => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]);
        return $cut->save();
    }

    public function testMerge(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $srcGeeftee = $this->newGeeftee()->setData([
            'name'       => $this->faker()->name,
            'email'      => uniqid('source_geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'creator_id' => $john->getId()
        ])->save();
        $jane = $this->getJane();

        $this->assertNotNull($srcGeeftee->getId());
        $this->assertNotNull($jane->getGeeftee()->getId());
        $this->assertNotEquals($srcGeeftee->getId(), $jane->getGeeftee()->getId());

        $allFamilies = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Family::ENTITY_TYPE);
        $allFamilyIds = $allFamilies->orderBy('main_table.family_id')
            ->getAllIds();
        $this->assertNotEmpty($allFamilyIds);

        $allGifts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Gift::ENTITY_TYPE);
        $allGiftIds = $allGifts->orderBy('main_table.gift_id')
            ->getAllIds();
        $this->assertNotEmpty($allGiftIds);

        // Prepare specific test data
        $janeOnlyFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Jane-only family '),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($jane, $janeOnlyFamily): void {
            $janeOnlyFamily->addGeeftee($jane->getGeeftee());
        });
        $geefteeOnlyFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Geeftee-only family ')
        ]])->save();
        $this->callPrivileged(static function () use ($srcGeeftee, $geefteeOnlyFamily): void {
            $geefteeOnlyFamily->addGeeftee($srcGeeftee);
        });
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Common family ')
        ]])->save();
        $this->callPrivileged(static function () use ($jane, $srcGeeftee, $commonFamily): void {
            $commonFamily->addGeeftee([$srcGeeftee, $jane->getGeeftee()]);
        });
        $janeGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . uniqid("Jane's gift ")
        ]])->save();
        $jane->getGeeftee()->addGift($janeGift);
        $srcGeefteeGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . uniqid('Merge source geeftee gift ')
        ]])->save();
        $srcGeeftee->addGift($srcGeefteeGift);

        // Source geeftee's families & gifts
        $srcGeefteeFamilyIds = $srcGeeftee->getFamilyCollection()
            ->clearOrders()
            ->orderBy('main_table.family_id')
            ->getAllIds();
        $this->assertNotEmpty($srcGeefteeFamilyIds);
        $this->assertNotEquals($allFamilyIds, $srcGeefteeFamilyIds);

        $srcGeefteeGiftIds = $srcGeeftee->getGiftCollection()
            ->clearOrders()
            ->orderBy('main_table.gift_id')
            ->getAllIds();
        $this->assertNotEmpty($srcGeefteeGiftIds);
        $this->assertNotEquals($allGiftIds, $srcGeefteeGiftIds);

        // Target geeftee's families & gifts
        $janeFamilyIds = $jane->getFamilyCollection()
            ->clearOrders()
            ->orderBy('main_table.family_id')
            ->getAllIds();
        $this->assertNotEmpty($janeFamilyIds);
        $this->assertNotEquals($allFamilyIds, $janeFamilyIds);

        $janeGiftIds = $jane->getGeeftee()->getGiftCollection()
            ->clearOrders()
            ->orderBy('main_table.gift_id')
            ->getAllIds();
        $this->assertNotEmpty($janeGiftIds);
        $this->assertNotEquals($allGiftIds, $janeGiftIds);

        // Preconditions
        $this->assertNotEquals($janeFamilyIds, $srcGeefteeFamilyIds);
        $this->assertNotEquals($janeGiftIds, $srcGeefteeGiftIds);
        $this->assertNotEmpty(array_diff($janeFamilyIds, $srcGeefteeFamilyIds));
        $this->assertNotEmpty(array_diff($srcGeefteeFamilyIds, $janeFamilyIds));
        $this->assertNotEmpty(array_diff($janeGiftIds, $srcGeefteeGiftIds));
        $this->assertNotEmpty(array_diff($srcGeefteeGiftIds, $janeGiftIds));
        $this->assertNotEmpty(array_intersect($janeFamilyIds, $srcGeefteeFamilyIds));
        $this->assertEmpty(array_intersect($janeGiftIds, $srcGeefteeGiftIds));

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($this->getGeefterButJohn());

        // Proceed
        try {
            $this->getCut()->merge($jane->getGeeftee(), $srcGeeftee);
            $this->fail('Failed preventing geeftee from being merged by anyone but his creator.');
        }
        catch (PermissionException) {}

        $this->disableGeefterAccessListeners();

        // Reload from DB and check => should NOT have changed
        $jane = $this->getJane();
        $srcGeeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $srcGeeftee->getId());

        // Source geeftee's families & gifts
        $srcGeefteeFamilyIds2 = $srcGeeftee->getFamilyCollection()
            ->clearOrders()
            ->orderBy('main_table.family_id')
            ->getAllIds();
        $this->assertEquals($srcGeefteeFamilyIds, $srcGeefteeFamilyIds2);

        $srcGeefteeGiftIds2 = $srcGeeftee->getGiftCollection()
            ->clearOrders()
            ->orderBy('main_table.gift_id')
            ->getAllIds();
        $this->assertEquals($srcGeefteeGiftIds, $srcGeefteeGiftIds2);

        // Target geeftee's families & gifts
        $janeFamilyIds2 = $jane->getFamilyCollection()
            ->clearOrders()
            ->orderBy('main_table.family_id')
            ->getAllIds();
        $this->assertEquals($janeFamilyIds, $janeFamilyIds2);

        $janeGiftIds2 = $jane->getGeeftee()->getGiftCollection()
            ->clearOrders()
            ->orderBy('main_table.gift_id')
            ->getAllIds();
        $this->assertEquals($janeGiftIds, $janeGiftIds2);


        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $this->getCut()->merge($jane->getGeeftee(), $srcGeeftee);

        $this->disableGeefterAccessListeners();

        // Reload from DB and check => should have changed
        $jane = $this->getJane();
        $srcGeeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $srcGeeftee->getId());

        $this->assertNotNull($jane->getGeeftee());
        $this->assertNull($srcGeeftee);

        $expectedFamilyIds = array_unique(array_merge($janeFamilyIds, $srcGeefteeFamilyIds));
        sort($expectedFamilyIds);

        $janeFamilyIds2 = $jane->getFamilyCollection()
            ->clearOrders()
            ->orderBy('main_table.family_id')
            ->getAllIds();
        $this->assertEquals($expectedFamilyIds, $janeFamilyIds2);

        $expectedGiftIds = array_unique(array_merge($janeGiftIds, $srcGeefteeGiftIds));
        sort($expectedGiftIds);

        $janeGiftIds2 = $jane->getGeeftee()->getGiftCollection()
            ->clearOrders()
            ->orderBy('main_table.gift_id')
            ->getAllIds();
        $this->assertEquals($expectedGiftIds, $janeGiftIds2);
    }
}
