<?php

namespace Geeftlist\Test\Helper\Geeftee;


use Geeftlist\Exception\Geeftee\ClaimRequest\AlreadyExistsException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Helper\Geeftee\ClaimRequest newCut()
 *
 * @group base
 * @group helpers
 */
class ClaimRequestTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Helper\Geeftee\ClaimRequest::class);
    }

    public function testRequest(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX . uniqid('ClaimRequest geeftee '),
            'geefter_id' => null,
            'creator_id' => $john->getId()
        ]])->save();
        $jane = $this->getJane();

        $this->assertNotNull($john->getId());
        $this->assertNotNull($jane->getId());
        $this->assertNotNull($geeftee->getId());

        $cut = $this->getCut();
        $claimRequest = $cut->request($geeftee, $jane);

        $claimRequest = $this->getEntity(Geeftee\ClaimRequest::ENTITY_TYPE, $claimRequest->getId());

        $this->assertEquals($geeftee->getId(), $claimRequest->getGeefteeId());
        $this->assertEquals($jane->getId(), $claimRequest->getGeefterId());
        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_PENDING, $claimRequest->getDecisionCode());

        try {
            $cut->request($geeftee, $jane);
            $this->fail('Failed preventing two equal claim requests to be created.');
        }
        catch (AlreadyExistsException) {}
    }

    public function testAcceptRequest(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => $this->faker()->name,
            'email'      => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'geefter_id' => null,
            'creator_id' => $john->getId(),
        ]])->save();
        $geefteeId = $geeftee->getId();
        $jane = $this->getJane();

        $this->assertNotNull($john->getId());
        $this->assertNotNull($jane->getId());
        $this->assertNotNull($geefteeId);

        $cut = $this->getCut();
        $claimRequest = $cut->request($geeftee, $jane);
        $claimRequestId = $claimRequest->getId();

        /** @var Geeftee\ClaimRequest $claimRequest */
        $claimRequest = $this->getEntity(Geeftee\ClaimRequest::ENTITY_TYPE, $claimRequestId);

        $this->enableGeefterAccessListeners();

        $anyGeefterButJohn = $this->getGeefterButJohn();
        $this->assertNotNull($anyGeefterButJohn->getId());
        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_PENDING, $claimRequest->getDecisionCode());
        $this->setGeefterInSession($anyGeefterButJohn);

        try {
            $cut->acceptRequest($claimRequest);
            $this->fail("Failed preventing claim request to be accepted by anyone but the geeftee's creator.");
        }
        catch (PermissionException) {}

        // Check source geeftee still exists
        $this->unsetGeefterInSession();   // Prevent observers from filtering collection
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geefteeId);
        $this->assertNotNull($geeftee);

        $this->setGeefterInSession($john);

        $cut->acceptRequest($claimRequest);

        // John cannot see the CR anymore due to filters on collection => return null under his session
        $claimRequest = $this->getEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['claim_request_id' => $claimRequestId]);
        $this->assertNull($claimRequest);

        $this->disableGeefterAccessListeners();

        $claimRequest = $this->getEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['claim_request_id' => $claimRequestId]);
        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_ACCEPTED, $claimRequest->getDecisionCode());
        $this->assertNotNull($claimRequest->getDecisionDate());

        // Source geeftee should have been deleted
        $this->unsetGeefterInSession();   // Prevent observers from filtering collection
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geefteeId);
        $this->assertNull($geeftee);
    }

    public function testRejectRequest(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => $this->faker()->name,
            'email'      => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'geefter_id' => null,
            'creator_id' => $john->getId(),
        ]])->save();
        $geefteeId = $geeftee->getId();
        $jane = $this->getJane();

        $this->assertNotNull($john->getId());
        $this->assertNotNull($jane->getId());
        $this->assertNotNull($geefteeId);

        $cut = $this->getCut();
        $claimRequest = $cut->request($geeftee, $jane);
        $claimRequestId = $claimRequest->getId();

        /** @var Geeftee\ClaimRequest $claimRequest */
        $claimRequest = $this->getEntity(Geeftee\ClaimRequest::ENTITY_TYPE, $claimRequestId);

        $this->enableGeefterAccessListeners();

        $anyGeefterButJohn = $this->getGeefterButJohn();
        $this->assertNotNull($anyGeefterButJohn->getId());
        $this->setGeefterInSession($anyGeefterButJohn);

        try {
            $cut->rejectRequest($claimRequest);
            $this->fail("Failed preventing claim request to be rejected by anyone but the geeftee's creator.");
        }
        catch (PermissionException) {}

        // Check source geeftee still exists
        $this->unsetGeefterInSession();   // Prevent observers from filtering collection
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geefteeId);
        $this->assertNotNull($geeftee);

        $this->setGeefterInSession($john);

        $cut->rejectRequest($claimRequest);

        // John can still see the CR
        $claimRequest = $this->getEntity(Geeftee\ClaimRequest::ENTITY_TYPE, $claimRequestId);
        $this->assertNotNull($claimRequest);
        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_REJECTED, $claimRequest->getDecisionCode());
        $this->assertNotNull($claimRequest->getDecisionDate());

        $this->unsetGeefterInSession();   // Prevent observers from filtering collection
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geefteeId);
        $this->assertNotNull($geeftee);
    }
}
