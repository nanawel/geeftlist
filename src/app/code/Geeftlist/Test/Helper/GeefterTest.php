<?php

namespace Geeftlist\Test\Helper;


use Geeftlist\Exception\Security\UnrelatedGeefteeException;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group helpers
 */
class GeefterTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Helper\Geefter
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Helper\Geefter::class);
    }

    /**
     * @return \Geeftlist\Model\Geefter
     */
    public function newGeefter() {
        return $this->newEntity(Geefter::ENTITY_TYPE);
    }

    /**
     * @return \Geeftlist\Model\Geefter
     */
    public function getGeefter(): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newGeefter()->setData([
            'name'      => $this->faker()->name,
            'email'     => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]);
        return $cut->save();
    }

    public function testDeleteAccountWithoutRecoverer(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        /** @var Geefter $tempGeefter */
        $tempGeefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => uniqid('Geefter to be deleted '),
            'email'    => uniqid('geefter') . '@'. Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'password' => '123456'
        ]])->save();
        $geeftee = $tempGeefter->getGeeftee();
        $geefterId = $tempGeefter->getId();

        /** @var Gift $giftFromJohn */
        $giftFromJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $geeftee->addGift($giftFromJohn);

        /** @var Gift $giftFromHimself */
        $giftFromHimself = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $geeftee->addGift($giftFromHimself);

        /** @var Gift $giftForJane */
        $giftForJane = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $jane->getGeeftee()->addGift($giftForJane);

        /** @var Gift $giftForJaneWithReservation */
        $giftForJaneWithReservation = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $jane->getGeeftee()->addGift($giftForJaneWithReservation);
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id' => $giftForJaneWithReservation->getId(),
            'geefter_id' => $john->getId()
        ]])->save();

        /** @var Gift $giftForJohnWithOwnReservation */
        $giftForJohnWithOwnReservation = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $john->getGeeftee()->addGift($giftForJohnWithOwnReservation);
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id' => $giftForJohnWithOwnReservation->getId(),
            'geefter_id' => $tempGeefter->getId()
        ]])->save();

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNotNull($tempGeefter->getId());
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geeftee->getId());
        $this->assertNotNull($geeftee->getId());
        $this->assertContainsEquals($geeftee->getId(), $giftFromJohn->getGeefteeCollection()->getAllIds());

        $this->enableGeefterAccessListeners();

        $this->getCut()->deleteAccount($tempGeefter);

        $this->disableGeefterAccessListeners();

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNull($tempGeefter);

        // Gift from John => should be left intact
        $giftFromJohn = $this->getEntity(Gift::ENTITY_TYPE, $giftFromJohn->getId());
        $this->assertNotNull($giftFromJohn);
        $this->assertContainsEquals($geeftee->getId(), $giftFromJohn->getGeefteeCollection()->getAllIds());

        // Gift for himself w/o reservation => should be deleted
        $giftFromHimself = $this->getEntity(Gift::ENTITY_TYPE, $giftFromHimself->getId());
        $this->assertNull($giftFromHimself);

        // Gift for Jane w/o reservation => should be deleted
        $giftForJane = $this->getEntity(Gift::ENTITY_TYPE, $giftForJane->getId());
        $this->assertNull($giftForJane);

        // Gift for Jane with reservation => should be reassigned
        $giftForJaneWithReservation = $this->getEntity(Gift::ENTITY_TYPE, $giftForJaneWithReservation->getId());
        $this->assertNotNull($giftForJaneWithReservation);
        $this->assertEquals($john->getId(), $giftForJaneWithReservation->getCreatorId());

        // Gift for John with own reservation => should be deleted
        $giftForJohnWithOwnReservation = $this->getEntity(Gift::ENTITY_TYPE, $giftForJohnWithOwnReservation->getId());
        $this->assertNull($giftForJohnWithOwnReservation);
    }

    public function testDeleteAccountWithRecoverer(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        /** @var Geefter $tempGeefter */
        $tempGeefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => uniqid('Geefter to be deleted '),
            'email'    => uniqid('geefter') . '@'. Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'password' => '123456'
        ]])->save();
        $geeftee = $tempGeefter->getGeeftee();
        $geefterId = $tempGeefter->getId();

        /** @var Gift $giftFromJohn */
        $giftFromJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $geeftee->addGift($giftFromJohn);

        /** @var Gift $giftFromHimself */
        $giftFromHimself = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $geeftee->addGift($giftFromHimself);

        /** @var Gift $giftForJane */
        $giftForJane = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $jane->getGeeftee()->addGift($giftForJane);

        /** @var Gift $giftForJaneWithReservation */
        $giftForJaneWithReservation = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $jane->getGeeftee()->addGift($giftForJaneWithReservation);
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id' => $giftForJaneWithReservation->getId(),
            'geefter_id' => $john->getId()
        ]])->save();

        /** @var Gift $giftForJohnWithOwnReservation */
        $giftForJohnWithOwnReservation = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $tempGeefter->getId()
        ]])->save();
        $john->getGeeftee()->addGift($giftForJohnWithOwnReservation);
        $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id' => $giftForJohnWithOwnReservation->getId(),
            'geefter_id' => $tempGeefter->getId()
        ]])->save();

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNotNull($tempGeefter->getId());
        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geeftee->getId());
        $this->assertNotNull($geeftee->getId());
        $this->assertContainsEquals($geeftee->getId(), $giftFromJohn->getGeefteeCollection()->getAllIds());

        $this->enableGeefterAccessListeners();

        // Try with an unrelated geefter => failure
        try {
            $this->getCut()->deleteAccount($tempGeefter, [
                'recoverer_geefter_id' => $richard->getId()
            ]);
            $this->fail('Failed preventing using an unrelated geefter as recoverer.');
        }
        catch (UnrelatedGeefteeException) {}

        $this->disableGeefterAccessListeners();

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($tempGeefter, $richard, $commonFamily): void {
            $commonFamily->addGeeftee([$tempGeefter->getGeeftee(), $richard->getGeeftee()]);
        });

        $this->enableGeefterAccessListeners();

        // Try again now that Richard and TempGeefter share a family
        $this->getCut()->deleteAccount($tempGeefter, [
            'recoverer_geefter_id' => $richard->getId()
        ]);

        $this->disableGeefterAccessListeners();

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNull($tempGeefter);

        // Gift from John => should be left intact
        $giftFromJohn = $this->getEntity(Gift::ENTITY_TYPE, $giftFromJohn->getId());
        $this->assertNotNull($giftFromJohn->getId());
        $this->assertContainsEquals($geeftee->getId(), $giftFromJohn->getGeefteeCollection()->getAllIds());

        // Gift for himself w/o reservation => should be assigned to Richard
        $giftFromHimself = $this->getEntity(Gift::ENTITY_TYPE, $giftFromHimself->getId());
        $this->assertNotNull($giftFromHimself->getId());
        $this->assertEquals($richard->getId(), $giftFromHimself->getCreatorId());

        // Gift for Jane w/o reservation => should be assigned to Richard
        $giftForJane = $this->getEntity(Gift::ENTITY_TYPE, $giftForJane->getId());
        $this->assertNotNull($giftForJane->getId());
        $this->assertEquals($richard->getId(), $giftForJane->getCreatorId());

        // Gift for Jane with reservation => should be assigned to Richard
        $giftForJaneWithReservation = $this->getEntity(Gift::ENTITY_TYPE, $giftForJaneWithReservation->getId());
        $this->assertNotNull($giftForJaneWithReservation->getId());
        $this->assertEquals($richard->getId(), $giftForJaneWithReservation->getCreatorId());

        // Gift for John with own reservation => should be assigned to Richard
        $giftForJohnWithOwnReservation = $this->getEntity(Gift::ENTITY_TYPE, $giftForJohnWithOwnReservation->getId());
        $this->assertNotNull($giftForJohnWithOwnReservation->getId());
        $this->assertEquals($richard->getId(), $giftForJohnWithOwnReservation->getCreatorId());
    }
}
