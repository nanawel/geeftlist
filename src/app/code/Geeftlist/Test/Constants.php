<?php

namespace Geeftlist\Test;


interface Constants
{
    public const SAMPLEDATA_EMAIL_DOMAIN                    = 'geeftlist.example.org';

    public const SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX = '@';

    public const SAMPLEDATA_FAMILY_NAME_PREFIX              = "[SAMPLEDATA] 👪 ";

    public const SAMPLEDATA_GIFT_LABEL_PREFIX               = "[SAMPLEDATA] 🎁 ";

    public const SAMPLEDATA_GIFTLIST_LABEL_PREFIX           = "[SAMPLEDATA] 📑 ";

    public const SAMPLEDATA_DISCUSSION_TOPIC_TITLE_PREFIX   = "[SAMPLEDATA] 📢 ";

    public const SAMPLEDATA_DISCUSSION_POST_TITLE_PREFIX    = "[SAMPLEDATA] 💬 ";

    public const GEEFTER_JOHN_USERNAME    = 'John Doe';

    public const GEEFTER_JANE_USERNAME    = 'Jane Doe';

    public const GEEFTER_RICHARD_USERNAME = 'Richard Roe';

    public const GEEFTER_JOHN_EMAIL       = 'johndoe@' . self::SAMPLEDATA_EMAIL_DOMAIN;

    public const GEEFTER_JANE_EMAIL       = 'janedoe@' . self::SAMPLEDATA_EMAIL_DOMAIN;

    public const GEEFTER_RICHARD_EMAIL    = 'richard.roe@' . self::SAMPLEDATA_EMAIL_DOMAIN;

    public const GEEFTER_JOHN_PASSWORD    = 'foo';

    public const GEEFTER_JANE_PASSWORD    = 'bar';

    public const GEEFTER_RICHARD_PASSWORD = 'foo';

    public const GEEFTER_OTHERS_PASSWORD  = '12345678';

    public const GEEFTER_PASSWORDS = [
        self::GEEFTER_JOHN_EMAIL    => self::GEEFTER_JOHN_PASSWORD,
        self::GEEFTER_JANE_EMAIL    => self::GEEFTER_JANE_PASSWORD,
        self::GEEFTER_RICHARD_EMAIL => self::GEEFTER_RICHARD_PASSWORD
    ];

    public const GEEFTEE_JOHN_DOB = '1970-09-08';

    public const GEEFTEE_JANE_DOB = '1970-04-03';
}
