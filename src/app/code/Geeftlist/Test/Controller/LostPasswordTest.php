<?php

namespace Geeftlist\Test\Controller;




use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class LostPasswordTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $fw = $this->getFw();

        $this->mock('GET /lostpassword');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?lostpassword-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndexLoggedIn(): void {
        $this->setGeefterInSession($this->getJane());

        try {
            $this->mock('GET /lostpassword');
            $this->fail('Failed rerouting logged in user to dashboard');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('dashboard'), $reroute->getUrl());
        }
    }

    public function testPostExistingEmail(): void {
        $this->getJane()
            ->setPasswordToken(null)
            ->setPasswordTokenDate(null)
            ->save();
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_INFO));

        try {
            $this->mock('POST /lostpassword/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => $this->getJane()->getEmail()
            ]);
            $this->fail('Failed rerouting after post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('lostpassword/success'), $reroute->getUrl());
            $this->assertNotNull($this->getJane()->getPasswordToken());
        }
    }

    public function testPostNotExistingEmail(): void {
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_INFO));

        try {
            $this->mock('POST /lostpassword/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => 'no-one@geeftlist.example.org'
            ]);
            $this->fail('Failed rerouting after post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('lostpassword/success'), $reroute->getUrl());
        }
    }
}
