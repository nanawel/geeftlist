<?php

namespace Geeftlist\Test\Controller\Discussion\Traits;

use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;

trait EntityGeneratorTrait
{
    /**
     * @param Geefter $geefter
     * @param Geeftee $geeftee
     * @return Topic
     */
    protected function generateGiftWithTopic($geefter, $geeftee) {
        $gift = current($this->generateGifts($geefter, $geeftee, null, 1));
        $this->getRepository(Gift::ENTITY_TYPE)->save($gift);

        /** @var Topic $topic */
        $topic = $this->newEntity(Topic::ENTITY_TYPE, ['data' => [
            'title' => 'foo',
            'linked_entity_type' => $gift->getEntityType(),
            'linked_entity_id' => $gift->getId(),
            'author_id' => $geefter->getId()
        ]]);
        $this->getRepository(Topic::ENTITY_TYPE)->save($topic);

        return $topic;
    }
}
