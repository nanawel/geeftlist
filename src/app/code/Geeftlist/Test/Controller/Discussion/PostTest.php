<?php

namespace Geeftlist\Test\Controller\Discussion;

use Geeftlist\Model\Discussion\Post;
use Geeftlist\Test\Controller\Discussion\Traits\EntityGeneratorTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class PostTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use EntityGeneratorTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        $this->enableGeefterAccessListeners();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testSave_notLoggedIn(): void {
        try {
            $this->mock('POST /discussion_post/save');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testSave_invalidPost(): void {
        $this->setGeefterInSession($this->getJane());

        try {
            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid form post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        try {
            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'title'                   => $this->faker()->words(4, true)
            ]);
            $this->fail('Failed rerouting on invalid form post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }
    }

    public function testSave_invalidPost_missingTopicId(): void {
        $this->setGeefterInSession($this->getJane());

        try {
            $uniqueTitle = uniqid('title_');
            $message = $this->faker()->paragraphs(2, true);

            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'title'                   => $uniqueTitle,
                'message'                 => $message
            ]);
            $this->fail('Failed rerouting invalid form post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }
    }

    /**
     * @group ticket-620
     */
    public function testPin_notTopicOwner(): void {
        $jane = $this->getJane();
        $richard = $this->getRichard();

        $topic = $this->generateGiftWithTopic($jane, $jane->getGeeftee());

        [$janePost, $richardPost] = $this->callPrivileged(
            function () use ($topic, $jane, $richard): array {
                $janePost = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
                    'topic_id' => $topic->getId(),
                    'author_id' => $jane->getId(),
                    'message' => uniqid('message_')
                ]])->save();
                $this->getRepository(Post::ENTITY_TYPE)->save($janePost);

                $richardPost = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
                    'topic_id' => $topic->getId(),
                    'author_id' => $richard->getId(),
                    'message' => uniqid('message_')
                ]])->save();
                $this->getRepository(Post::ENTITY_TYPE)->save($richardPost);


                return [$janePost, $richardPost];
            }
        );

        $this->setGeefterInSession($richard);
        // Jane's post
        try {
            $this->mock('POST /discussion_post/pin', [
                'post_id' => $janePost->getId(),
                $this->getCsrfTokenName() => $this->getCsrfToken(),
            ]);
            $this->fail('Failed rerouting on illegal request');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertFalse($this->getRepository(Post::ENTITY_TYPE)->get($janePost->getId())->getIsPinned());

        // Richard's own post
        try {
            $this->mock('POST /discussion_post/pin', [
                'post_id' => $richardPost->getId(),
                $this->getCsrfTokenName() => $this->getCsrfToken(),
            ]);
            $this->fail('Failed rerouting on illegal request');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertFalse($this->getRepository(Post::ENTITY_TYPE)->get($richardPost->getId())->getIsPinned());

        $this->setGeefterInSession($this->getJohn());
        try {
            $this->mock('POST /discussion_post/pin', [
                'post_id' => $janePost->getId(),
                $this->getCsrfTokenName() => $this->getCsrfToken(),
            ]);
            $this->fail('Failed rerouting on illegal request');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertFalse($this->getRepository(Post::ENTITY_TYPE)->get($janePost->getId())->getIsPinned());
    }

    /**
     * @group ticket-620
     */
    public function testPin_valid(): void {
        $jane = $this->getJane();
        $richard = $this->getRichard();

        $topic = $this->generateGiftWithTopic($jane, $jane->getGeeftee());

        [$janePost, $richardPost] = $this->callPrivileged(function () use ($topic, $jane, $richard): array {
            $janePost = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
                'topic_id' => $topic->getId(),
                'author_id' => $jane->getId(),
                'message' => uniqid('message_')
            ]])->save();
            $this->getRepository(Post::ENTITY_TYPE)->save($janePost);

            $richardPost = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
                'topic_id' => $topic->getId(),
                'author_id' => $richard->getId(),
                'message' => uniqid('message_')
            ]])->save();
            $this->getRepository(Post::ENTITY_TYPE)->save($richardPost);


            return [$janePost, $richardPost];
        });

        $this->setGeefterInSession($jane);

        // Jane's own post => OK
        try {
            $this->mock('POST /discussion_post/pin', [
                'post_id' => $janePost->getId(),
                $this->getCsrfTokenName() => $this->getCsrfToken(),
            ]);
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertTrue($this->getRepository(Post::ENTITY_TYPE)->get($janePost->getId())->getIsPinned());

        // Richard's post => OK
        try {
            $this->mock('POST /discussion_post/pin', [
                'post_id' => $richardPost->getId(),
                $this->getCsrfTokenName() => $this->getCsrfToken(),
            ]);
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertTrue((bool) $this->getRepository(Post::ENTITY_TYPE)->get($richardPost->getId())->getIsPinned());
    }

    /**
     * @see \Geeftlist\Test\Controller\Gift\DiscussionTest
     */
}
