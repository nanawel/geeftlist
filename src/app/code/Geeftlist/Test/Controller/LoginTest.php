<?php

namespace Geeftlist\Test\Controller;





use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class LoginTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $fw = $this->getFw();

        $this->mock('GET /login');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?login-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndexLoggedIn(): void {
        $this->setGeefterInSession($this->getJane());

        try {
            $this->mock('GET /login');
            $this->fail('Failed rerouting logged in user to dashboard');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('dashboard'), $reroute->getUrl());
        }
    }

    public function testPostMissingCsrfToken(): void {
        $john = $this->getJohn();

        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('POST /login/post', [
                'email' => $john->getEmail(),
                'password' => Constants::GEEFTER_JOHN_PASSWORD
            ]);
            $this->fail('Failed rerouting on missing CSRF token');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }
    }

    public function testPostInvalidCsrfToken(): void {
        $john = $this->getJohn();

        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('POST /login/post', [
                $this->getCsrfTokenName() => crc32((string) $this->getCsrfToken()),
                'email' => $john->getEmail(),
                'password' => Constants::GEEFTER_JOHN_PASSWORD
            ]);
            $this->fail('Failed rerouting on invalid CSRF token');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }
    }

    public function testPostMissingEmail(): void {
        try {
            $this->mock('POST /login/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'password' => Constants::GEEFTER_JOHN_PASSWORD
            ]);
            $this->fail('Failed rerouting on missing email');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testPostMissingPassword(): void {
        $john = $this->getJohn();

        try {
            $this->mock('POST /login/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => $john->getEmail()
            ]);
            $this->fail('Failed rerouting on missing password');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testPostInvalidPassword(): void {
        $john = $this->getJohn();

        try {
            $this->mock('POST /login/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => $john->getEmail(),
                'password' => crc32(Constants::GEEFTER_JOHN_PASSWORD)
            ]);
            $this->fail('Failed rerouting on invalid password');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * @group ticket-679
     */
    public function testPostValid(): void {
        $john = $this->getJohn();

        $john->setLastLoginAt(DateTime::getDateSql(0));
        $this->getRepository(Geefter::ENTITY_TYPE)->save($john);
        $origLastLoginAt = $john->getLastLoginAt();

        try {
            $this->mock('POST /login/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => $john->getEmail(),
                'password' => Constants::GEEFTER_JOHN_PASSWORD
            ]);
            $this->fail('Failed rerouting on valid login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('dashboard'), $reroute->getUrl());

            // Make sure "last_login_at" has been updated (#679)
            $john = $this->getJohn();
            $this->assertNotNull($john->getLastLoginAt());
            $this->assertNotEquals($origLastLoginAt, $john->getLastLoginAt());
        }

        $this->getGeefterSession()->invalidate();
        $richard = $this->getRichard();

        try {
            $this->mock('POST /login/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => $richard->getEmail(),
                'password' => Constants::GEEFTER_RICHARD_PASSWORD
            ]);
            $this->fail('Failed rerouting on valid login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('dashboard'), $reroute->getUrl());
        }
    }
}
