<?php

namespace Geeftlist\Test\Controller\Family;


use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ExploreTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $family = $this->getEntity(Family::ENTITY_TYPE);
        $this->assertNotNull($family);
        $this->assertNotNull($family->getId());

        try {
            $this->mock('GET /family_explore/index/family_id/' . $family->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * Deleted 2019-03-16 - Useless since it does even less than testIndex_displayedGeefteesAndGifts() next
     *
     * @group ticket-379
     */
    //public function testIndexLoggedIn() {}

    /**
     * @group ticket-379
     */
    public function testIndex_displayedGeefteesAndGifts(): void {
        $fw = $this->getFw();

        $cuts = $this->generateCutsLight(3, 1);
        $geefters = $cuts['geefters'];

        $seenGifts = 0;
        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);
            $this->enableGeefterAccessListeners();

            /** @var \Geeftlist\Model\Family $family */
            $family = $geefter->getFamilyCollection()->getFirstItem();

            if (!$family) {
                continue;
            }

            $this->mock('GET /family_explore/index/family_id/' . $family->getId());
            $response = $fw->get('RESPONSE');

            $familyGeeftees = $family->getGeefteeCollection();
            $refGifts = [];
            /** @var \Geeftlist\Model\Geeftee $geeftee */
            foreach ($familyGeeftees as $geeftee) {
                $refGifts[$geeftee->getId()] = $geeftee->getGiftCollection()
                    ->addFieldToFilter('status', Gift\Field\Status::AVAILABLE);
            }

            foreach ($familyGeeftees as $geeftee) {
                // 1. Check that all related geeftees are present in the page
                $this->assertMatchesRegularExpression(
                    sprintf('|<a .*?href="#geeftee-%d-gifts"|', $geeftee->getId()),
                    $response,
                    sprintf("Tab for geeftee #%d '%s' not found in page as %s.",
                        $geeftee->getId(),
                        $geeftee->getName(),
                        $geefter->getUsername()
                    )
                );

                // 2. Check that all visible gifts to the geefter are present in the page for each geeftee
                foreach ($refGifts[$geeftee->getId()] as $refGift) {
                    $this->assertMatchesRegularExpression(
                        sprintf('|<a .*?/gift/view/gift_id/(%d)|', $refGift->getId()),
                        $response,
                        sprintf("Link to gift #%d '%s' (creator: %s) not found in page as %s.",
                            $refGift->getId(),
                            $refGift->getLabel(),
                            $refGift->getCreatorUsername(),
                            $geefter->getUsername()
                        )
                    );
                }
            }

            // 3. Check that all gifts in the page are visible to the geefter
            $seenGifts += preg_match_all('|<a .*?/gift/view/gift_id/(\d+)|', (string) $response, $matches);
            $actualGiftIds = count($matches[1]) > 0 ? $matches[1] : [];

            $this->disableGeefterAccessListeners();

            $giftCollection = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'gift_id' => ['in' => $actualGiftIds]
            ]);

            foreach ($giftCollection as $gift) {
                $this->assertEquals(Gift\Field\Status::AVAILABLE, $gift->getStatus());
                $this->assertTrue(
                    $this->getGeefterPermissionService()->isAllowed($geefter, $gift, TypeInterface::ACTION_VIEW),
                    sprintf('Failed asserting that %s should be able to view gift #%s ', $geefter->getUsername(), $gift->getId())
                    . sprintf('(%s / %s) on family/explore page.', $gift->getLabel(), $gift->getDescription())
                );

                // Add new validation tests here
            }
        }

        $this->assertGreaterThan(0, $seenGifts);
    }
}
