<?php

namespace Geeftlist\Test\Controller\Family;


use Geeftlist\Model\Family;
use Geeftlist\Model\ResourceModel\Db\Family\Collection;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ManageTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndex_notLoggedIn(): void {
        try {
            $this->mock('GET /family_manage/index');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndex_loggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /family_manage/index');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $maxRecords = 100;
        /** @var Collection $expectedFamilies */
        $expectedFamilies = $this->getEntityCollection(Family::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', $john->getGeeftee()->getId())
            ->setLimit($maxRecords);
        $expectedFamilyIds = $expectedFamilies->getAllIds();

        $this->mock('GET /family_manage/grid [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals($response['recordsTotal'], $response['recordsFiltered']);
        $this->assertEquals(count($expectedFamilies), $response['recordsTotal']);
        $familyIds = array_column($response['items'], 'family_id');
        $this->assertNotEmpty($familyIds);
        $this->assertEqualsCanonicalizing($expectedFamilyIds, $familyIds);
    }

    public function testNew(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock("GET /family_manage/new");
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-new.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testEdit_notOwner(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $jane->getFamilyCollection()
            ->addFieldToFilter('owner_id', ['neq' => $jane->getId()])
            ->getFirstItem();

        try {
            $this->mock('GET /family_manage/edit/family_id/' . $family->getId());
            $this->fail('Failed rerouting while attempting to edit a family as a non-owner.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
        }
    }

    public function testEdit_owner(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $jane->getFamilyCollection()
            ->addFieldToFilter('owner_id', $jane->getId())
            ->getFirstItem();

        $this->mock('GET /family_manage/edit/family_id/' . $family->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-edit.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-625
     */
    public function testDelete_notOwner(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();

        $this->setGeefterInSession($this->getJohn());

        try {
            $this->mock(sprintf(
                'GET /family_manage/delete/family_id/%d?%s=%s',
                $family->getId(),
                $this->getCsrfTokenName(),
                $this->getCsrfToken()
            ));
            $this->fail('Failed rerouting while attempting to delete a family as a non-owner.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
        }
    }

    /**
     * @group ticket-625
     */
    public function testDelete_owner_invalidCsrf(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();

        $this->setGeefterInSession($jane);

        try {
            $this->mock(sprintf(
                'GET /family_manage/delete/family_id/%d?%s=%s',
                $family->getId(),
                $this->getCsrfTokenName(),
                'foo'
            ));
            $this->fail('Failed preventing deletion with missing CSRF token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
        }
    }

    /**
     * @group ticket-625
     */
    public function testDelete_owner(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();

        $this->setGeefterInSession($jane);

        try {
            $this->mock(sprintf(
                'GET /family_manage/delete/family_id/%d?%s=%s',
                $family->getId(),
                $this->getCsrfTokenName(),
                $this->getCsrfToken()
            ));
            $this->fail('Failed preventing deletion with missing CSRF token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
        }
    }
}
