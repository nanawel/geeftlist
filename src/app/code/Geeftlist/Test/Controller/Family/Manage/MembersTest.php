<?php

namespace Geeftlist\Test\Controller\Family\Manage;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 * @group ticket-399
 */
class MembersTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->getEmailSender()->clearSentEmails();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $family = $this->getEntity(Family::ENTITY_TYPE);
        $this->assertNotNull($family);
        $this->assertNotNull($family->getId());

        try {
            $this->mock('GET /family_manage_members/index/family_id/' . $family->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $this->newFamilyCut($jane);

        $this->assertNotNull($family);

        $this->mock('GET /family_manage_members/index/family_id/' . $family->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-members-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        /** @var Family $family */
        $family = $this->getEntityCollection(Family::ENTITY_TYPE, [
            'owner_id' => $john->getId()
        ])->getFirstItem();

        // Ensure family contains AT LEAST ONE geefterlss geeftee to check that returned list is valid even with
        // this type of geeftee (issue with geefter_id <> N that does not include geefter_id IS NULL)
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
        ]])->save();
        $this->callPrivileged(static function () use ($geefterlessGeeftee, $family): void {
            $family->addGeeftee($geefterlessGeeftee);
        });

        $maxRecords = 100;
        $expectedGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
            // Note: Do *not* use filter on geefter_id here, because the column is nullable (and "!=" skip those values)
            ->addFieldToFilter('geeftee_id', ['ne' => $john->getGeeftee()->getId()])
            ->addFieldToFilter('family', $family->getId())
            ->setLimit($maxRecords);
        $expectedGeefteeIds = $expectedGeeftees->getAllIds();

        $this->assertNotEmpty($expectedGeefteeIds);

        $this->mock('GET /family_manage_members/grid/family_id/' . $family->getId() . ' [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals($response['recordsTotal'], $response['recordsFiltered']);
        $this->assertEquals(count($expectedGeeftees), $response['recordsTotal']);
        $geefteeIds = array_column($response['items'], 'geeftee_id');
        $this->assertNotEmpty($geefteeIds);
        $this->assertEqualsCanonicalizing($expectedGeefteeIds, $geefteeIds);
    }

    public function testPostBanAction_notMember(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newFamilyCut($jane);

        /** @var \Geeftlist\Model\Geeftee $aGeeftee */
        $aGeeftee = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
            ->addFieldToFilter('family', ['neq' => $family->getId()])
            ->addFieldToFilter('main_table.geeftee_id', ['neq' => $jane->getGeeftee()->getId()])
            ->getFirstItem();
        $membersCount = count($family->getGeefteeIds());

        $this->assertNotNull($family);
        $this->assertNotNull($aGeeftee);
        $this->assertGreaterThan(2, $membersCount);
        $this->assertFalse($family->isMember($aGeeftee));
        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->enableGeefterAccessListeners();

        try {
            $this->mock('GET /family_manage_members/postBan/family_id/' . $family->getId()
                . '/geeftee_id/' . $aGeeftee->getId()
                . '?' . $this->getCsrfTokenName() . '=' . $this->getCsrfToken());
            $this->fail('Failed rerouting after attempting to ban a non-member from a family.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
        }

        /** @var Family $family */
        $family = $this->getEntity(Family::ENTITY_TYPE, $family->getId());
        $this->assertNotNull($family);

        $this->assertFalse($family->isMember($aGeeftee));
        $this->assertCount($membersCount, $family->getGeefteeIds());
        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testPostBanAction_linkSingle(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newFamilyCut($jane);

        /** @var \Geeftlist\Model\Geeftee $aGeeftee */
        $aGeeftee = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
            ->addFieldToFilter('family', $family->getId())
            ->addFieldToFilter('main_table.geeftee_id', ['neq' => $jane->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.geefter_id', ['null' => false])  // Best coverage of code
            ->getFirstItem();
        $membersCount = count($family->getGeefteeIds());

        $this->assertNotNull($family);
        $this->assertNotNull($aGeeftee);
        $this->assertGreaterThan(2, $membersCount);
        $this->assertTrue($family->isMember($aGeeftee));
        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->enableGeefterAccessListeners();

        try {
            $this->mock('GET /family_manage_members/postBan/family_id/' . $family->getId()
                . '/geeftee_id/' . $aGeeftee->getId()
                . '?' . $this->getCsrfTokenName() . '=' . $this->getCsrfToken());
            $this->fail('Failed rerouting after attempting to ban a member from a family.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
        }

        /** @var Family $family */
        $family = $this->getEntity(Family::ENTITY_TYPE, $family->getId());
        $this->assertNotNull($family);

        $this->assertFalse($family->isMember($aGeeftee));
        $this->assertCount($membersCount - 1, $family->getGeefteeIds());
        $this->assertNotEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testPostBanAction_batchSelect(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newFamilyCut($jane);

        /** @var \Geeftlist\Model\Geeftee[] $geeftees */
        $geeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
            ->addFieldToFilter('family', $family->getId())
            ->addFieldToFilter('main_table.geeftee_id', ['neq' => $jane->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.geefter_id', ['null' => false])  // Best coverage of code
            ->setLimit(2)
            ->orderBy(Collection::ORDER_BY_RANDOM)
            ->getItems();
        $membersCount = count($family->getGeefteeIds());

        $this->assertNotNull($family);
        $this->assertCount(2, $geeftees);
        $this->assertGreaterThan(2, $membersCount);
        foreach ($geeftees as $geeftee) {
            $this->assertTrue($family->isMember($geeftee));
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->enableGeefterAccessListeners();

        try {
            $this->mock('POST /family_manage_members/postBan/family_id/' . $family->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'selected_grid_rows' => array_keys($geeftees)
            ]);
            $this->fail('Failed rerouting after attempting to ban 2 members from a family.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
        }

        /** @var Family $family */
        $family = $this->getEntity(Family::ENTITY_TYPE, $family->getId());
        $this->assertNotNull($family);

        foreach ($geeftees as $geeftee) {
            $this->assertFalse($family->isMember($geeftee));
        }

        $this->assertCount($membersCount - 2, $family->getGeefteeIds());
        $this->assertNotEmpty($this->getEmailSender()->getSentEmails());
    }

    protected function newFamilyCut(Geefter $owner) {
        return $this->executeOnlyWithListeners(['default'], function() use ($owner) {
            /** @var Family $family */
            $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                'owner_id' => $owner->getId()
            ]])->save();

            $randomGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
                ->addFieldToFilter('geeftee_id', ['neq' => $owner->getGeeftee()->getId()])
                ->orderBy(Collection::ORDER_BY_RANDOM)
                ->setLimit(12);

            $this->callPrivileged(static function () use ($family, $owner, $randomGeeftees): void {
                $family->addGeeftee(array_merge(
                    [$owner->getGeeftee()],
                    $randomGeeftees->getItems()
                ));
            });

            return $family;
        });
    }
}
