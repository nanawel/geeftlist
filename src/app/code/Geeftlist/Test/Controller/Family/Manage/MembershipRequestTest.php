<?php

namespace Geeftlist\Test\Controller\Family\Manage;


use Geeftlist\Model\Family;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class MembershipRequestTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        try {
            $this->mock('GET /family_manage_membership_request/index');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /family_manage_membership_request/index');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-membership-request-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-387
     */
    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $maxRecords = 100;
        $expectedRequests = $this->getEntityCollection(Family\MembershipRequest::ENTITY_TYPE)
            ->addFieldToFilter('decision_code', Family\MembershipRequest::DECISION_CODE_PENDING)
            ->setLimit($maxRecords);
        $expectedRequestIds = $expectedRequests->getAllIds();

        $this->mock('GET /family_manage_membership_request/grid [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $requestIds = array_column($response['items'], 'membership_request_id');
        $this->assertNotEmpty($requestIds);
        $this->assertEqualsCanonicalizing($expectedRequestIds, $requestIds);
        $this->assertEquals($response['recordsTotal'], $response['recordsFiltered']);
        $this->assertEquals(count($expectedRequests), $response['recordsTotal']);
    }
}
