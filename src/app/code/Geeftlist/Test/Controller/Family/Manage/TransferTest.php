<?php

namespace Geeftlist\Test\Controller\Family\Manage;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class TransferTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndex_notLoggedIn(): void {
        $family = $this->getEntity(Family::ENTITY_TYPE);
        $this->assertNotNull($family);
        $this->assertNotNull($family->getId());

        try {
            $this->mock('GET /family_manage_transfer/index/family_id/' . $family->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndex_loggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $this->newFamilyCut($jane);

        $this->assertNotNull($family);

        $this->mock('GET /family_manage_transfer/index/family_id/' . $family->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-transfer-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-387
     */
    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $family = $this->getEntityCollection(Family::ENTITY_TYPE, [
            'owner_id' => $john->getId()
        ])->getFirstItem();

        $maxRecords = 100;
        $exptectedGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE, [
            'geefter_id' => ['null' => false]
        ])->addFieldToFilter('geefter_id', ['ne' => $john->getId()])
            ->addFieldToFilter('family', $family->getId())
            ->setLimit($maxRecords);
        $expectedGeefteeIds = $exptectedGeeftees->getAllIds();

        $this->mock('GET /family_manage_transfer/grid/family_id/' . $family->getId() . ' [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals($response['recordsTotal'], $response['recordsFiltered']);
        $this->assertEquals(count($exptectedGeeftees), $response['recordsTotal']);
        $geefteeIds = array_column($response['items'], 'geeftee_id');
        $this->assertNotEmpty($geefteeIds);
        $this->assertEqualsCanonicalizing($expectedGeefteeIds, $geefteeIds);
    }

    public function testConfirm_invalidGeeftee(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newFamilyCut($jane);

        /** @var \Geeftlist\Model\Geeftee $aGeeftee */
        $aGeeftee = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
            ->addFieldToFilter('family', ['neq' => $family->getId()])
            ->addFieldToFilter('main_table.geeftee_id', ['neq' => $jane->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.geefter_id', ['null' => false])
            ->getFirstItem();
        $aGeefter = $aGeeftee->getGeefter();

        $this->assertNotNull($family);
        $this->assertNotNull($aGeeftee);
        $this->assertNotNull($aGeefter->getId());
        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->enableGeefterAccessListeners();

        try {
            $this->mock('GET /family_manage_transfer/confirm/family_id/' . $family->getId()
                . '/geeftee_id/' . $aGeeftee->getId()
                . '?' . $this->getCsrfTokenName() . '=' . $this->getCsrfToken());
            $this->fail('Failed rerouting after attempting to transfer a family to a non-member.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $family = $this->getEntity(Family::ENTITY_TYPE, $family->getId());
        $this->assertNotNull($family);

        $this->assertEquals($jane->getId(), $family->getOwnerId());
        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testConfirm_valid(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Family $family */
        $family = $this->newFamilyCut($jane);

        /** @var \Geeftlist\Model\Geeftee $aGeeftee */
        $aGeeftee = $family->getGeefteeCollection()
            ->addFieldToFilter('main_table.geeftee_id', ['neq' => $jane->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.geefter_id', ['null' => false])
            ->getFirstItem();
        $aGeefter = $aGeeftee->getGeefter();

        $this->assertNotNull($family->getId());
        $this->assertNotNull($aGeeftee->getId());
        $this->assertNotNull($aGeefter->getId());
        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->enableGeefterAccessListeners();

        try {
            $this->mock('GET /family_manage_transfer/confirm/family_id/' . $family->getId()
                . '/geeftee_id/' . $aGeeftee->getId()
                . '?' . $this->getCsrfTokenName() . '=' . $this->getCsrfToken());
            $this->fail('Failed rerouting after transfering a family.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('family_manage'), $reroute->getUrl());
        }

        $family = $this->getEntity(Family::ENTITY_TYPE, $family->getId());
        $this->assertNotNull($family);

        $this->assertEquals($aGeefter->getId(), $family->getOwnerId());
        $sentMails = $this->getEmailSender()->getSentEmails();
        $this->assertCount(1, $sentMails);
        $this->assertEquals($aGeefter->getEmail(), $sentMails[0]['to']);
    }

    protected function newFamilyCut(Geefter $owner) {
        return $this->executeOnlyWithListeners(['default'], function() use ($owner) {
            /** @var Family $family */
            $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                'owner_id' => $owner->getId()
            ]])->save();

            $randomGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
                ->addFieldToFilter('geeftee_id', ['neq' => $owner->getGeeftee()->getId()])
                ->orderBy(Collection::ORDER_BY_RANDOM)
                ->setLimit(12);

            $this->callPrivileged(static function () use ($family, $owner, $randomGeeftees): void {
                $family->addGeeftee(array_merge(
                    [$owner->getGeeftee()],
                    $randomGeeftees->getItems()
                ));
            });

            return $family;
        });
    }
}
