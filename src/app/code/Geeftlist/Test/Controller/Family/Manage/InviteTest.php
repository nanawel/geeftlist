<?php

namespace Geeftlist\Test\Controller\Family\Manage;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class InviteTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $family = $this->getEntity(Family::ENTITY_TYPE);
        $this->assertNotNull($family);
        $this->assertNotNull($family->getId());

        try {
            $this->mock('GET /family_manage_invite/index/family_id/' . $family->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $family = $this->newFamilyCut($jane);

        $this->assertNotNull($family);

        $this->mock('GET /family_manage_invite/index/family_id/' . $family->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?family-manage-invite-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-387
     */
    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();
        $johnOriginalFamilyIds = $john->getFamilyCollection()->getAllIds();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $family = $this->newFamilyCut($john);

        $maxRecords = 100;
        $expectedGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE, [
            'geeftee_id' => ['ne' => $john->getGeeftee()->getId()]
        ])->addFieldToFilter('families', ['in' => $johnOriginalFamilyIds])
            ->addFieldToFilter('family', ['neq' => $family->getId()])
            ->setLimit($maxRecords);
        $expectedGeefteeIds = $expectedGeeftees->getAllIds();

        $this->mock('GET /family_manage_invite/grid/family_id/' . $family->getId() . ' [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals($response['recordsTotal'], $response['recordsFiltered']);
        $geefteeIds = array_column($response['items'], 'geeftee_id');
        $this->assertNotEmpty($geefteeIds);
        $this->assertEqualsCanonicalizing($expectedGeefteeIds, $geefteeIds);
    }

    protected function newFamilyCut(Geefter $owner) {
        return $this->executeOnlyWithListeners(['default'], function() use ($owner) {
            /** @var Family $family */
            $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
                'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                'owner_id' => $owner->getId()
            ]])->save();

            $randomGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE)
                ->addFieldToFilter('geeftee_id', ['neq' => $owner->getGeeftee()->getId()])
                ->orderBy(Collection::ORDER_BY_RANDOM)
                ->setLimit(12);

            $this->callPrivileged(static function () use ($family, $owner, $randomGeeftees): void {
                $family->addGeeftee(array_merge(
                    [$owner->getGeeftee()],
                    $randomGeeftees->getItems()
                ));
            });

            return $family;
        });
    }
}
