<?php

namespace Geeftlist\Test\Controller;

use OliveOil\Core\Service\Url\Redirect;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class RedirectTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $redirectPath = $this->getUrlRedirectService()->getPathForRedirect(
            'mycontroller/myaction',
            [
                'myparam' => 'myvalue',
                '_fragment' => 'myfragment'
            ]
        );
        try {
            $this->mock('GET /' . $redirectPath);
            $this->fail('Failed rerouting redirect URL when not logged in');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
            $this->assertEquals(
                $this->getUrl($redirectPath),
                $this->getGeefterSession()->getAfterLoginUrl()
            );
        }
    }

    public function testIndexLoggedIn(): void {
        $this->setGeefterInSession($this->getJane());

        $redirectTargetPath = $this->getUrlBuilder()->getPath(
            'mycontroller/myaction',
            [
                'myparam' => 'myvalue',
                '_fragment' => 'myfragment'
            ]
        );
        $redirectPath = $this->getUrlRedirectService()->parameterizedPathToRedirectPath($redirectTargetPath);
        try {
            $this->mock('GET /' . $redirectPath);
            $this->fail('Failed rerouting valid redirect URL when logged in');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($redirectTargetPath, $reroute->getUrl());
        }
    }

    public function testIndexAfterLoggingIn(): void {
        $this->setGeefterInSession($this->getJane());

        $redirectTargetPath = $this->getUrlBuilder()->getPath(
            'mycontroller/myaction',
            [
                'myparam' => 'myvalue',
                '_fragment' => 'myfragment'
            ]
        );
        $redirectPath = $this->getUrlRedirectService()->parameterizedPathToRedirectPath($redirectTargetPath);

        // Inject redirect URL as "after login URL" in session
        $this->getGeefterSession()->setPersistentData('after_login_url', $this->getUrl($redirectPath));
        try {
            $this->mock('GET /login/index');
            $this->fail('Failed rerouting to after-login-URL when logged in');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($redirectPath, $reroute->getUrl());
            try {
                $this->mock('GET /' . substr($reroute->getUrl(), strlen($this->getUrlBuilder()->getFullBaseUrl())));
                $this->fail('Failed rerouting to valid redirect after logging in');
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertStringEndsWith($redirectTargetPath, $reroute->getUrl());
            }
        }
        finally {
            $this->getGeefterSession()->unsetPersistentData('after_login_url');
        }
    }

    /**
     * @return Redirect
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getUrlRedirectService() {
        return $this->getContainer()->get(Redirect::class);
    }
}
