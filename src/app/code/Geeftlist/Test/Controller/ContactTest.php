<?php

namespace Geeftlist\Test\Controller;

use Geeftlist\Form\Element\ContactSubjectSelect;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ContactTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndex_notLoggedIn_captchaDisabled(): void {
        $fw = $this->getFw();

        $this->unsetGeefterInSession();

        $this->getAppConfig()->setValue('CAPTCHA_CONTACT_VISITOR_ENABLE', 0);

        $this->mock('GET /contact');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?contact-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertDoesNotMatchRegularExpression('/name="captcha\[id\]"/', $response, 'Unexpected captcha found in response');
    }

    public function testIndex_notLoggedIn_captchaEnabled(): void {
        $fw = $this->getFw();

        $this->unsetGeefterInSession();

        $this->getAppConfig()->setValue('CAPTCHA_CONTACT_VISITOR_ENABLE', 1);

        $this->mock('GET /contact');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?contact-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertMatchesRegularExpression('/name="captcha\[id\]"/', $response, 'Cannot find captcha element in response');
    }

    public function testIndex_loggedIn(): void {
        $fw = $this->getFw();

        $this->setGeefterInSession($this->getJane());

        $this->mock('GET /contact');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?contact-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testPost_invalidSubject(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->getEmailSender()->clearSentEmails();

        try {
            $this->mock('POST /contact/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'subject' => 'invalid_subject',
                'message' => $this->faker()->paragraphs(1, true)
            ]);
            $this->fail('Failed rerouting on invalid subject');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertStringEndsWith($this->getUrl('contact'), $reroute->getUrl());
        }
    }

    public function testPost_missingMessage(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var \Geeftlist\Form\Element\ContactSubjectSelect $subjectSelect */
        $subjectSelect = $this->getGeeftlistModelFactory()->make(ContactSubjectSelect::class);

        $subject = array_rand($subjectSelect->getOptions(), 1);

        try {
            $this->mock('POST /contact/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'subject' => $subject
            ]);
            $this->fail('Failed rerouting on missing message');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertStringEndsWith($this->getUrl('contact'), $reroute->getUrl());
        }
    }

    public function testPost_invalidCaptcha(): void {
        $this->unsetGeefterInSession();

        $this->getAppConfig()->getValue('CAPTCHA_CONTACT_VISITOR_ENABLE', 1);

        /** @var \Geeftlist\Form\Element\ContactSubjectSelect $subjectSelect */
        $subjectSelect = $this->getGeeftlistModelFactory()->make(ContactSubjectSelect::class);

        $subject = array_rand($subjectSelect->getOptions(), 1);
        $message = $this->faker()->paragraphs(1, true);

        $captcha = $this->generateCaptchaInSession('contact_form_captcha');

        try {
            $this->mock('POST /contact/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'subject' => $subject,
                'message' => $message,
                'captcha' => [
                    'id'     => $captcha->getId(),
                    'answer' => 'foo'
                ]
            ]);
            $this->fail('Failed rerouting on invalid captcha');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertStringEndsWith($this->getUrl('contact'), $reroute->getUrl());
        }
    }

    public function testPost_notLoggedIn_invalidEmail(): void {
        $this->unsetGeefterInSession();

        $this->getAppConfig()->getValue('CAPTCHA_CONTACT_VISITOR_ENABLE', 1);

        /** @var \Geeftlist\Form\Element\ContactSubjectSelect $subjectSelect */
        $subjectSelect = $this->getGeeftlistModelFactory()->make(ContactSubjectSelect::class);

        $subject = array_rand($subjectSelect->getOptions(), 1);
        $message = $this->faker()->paragraphs(1, true);

        $captcha = $this->generateCaptchaInSession('contact_form_captcha');

        try {
            $this->mock('POST /contact/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'subject'         => $subject,
                'message'         => $message,
                'requester_email' => 'bar',
                'captcha'         => [
                    'id'     => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting on invalid requester email');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertStringEndsWith($this->getUrl('contact'), $reroute->getUrl());
        }
    }

    public function testPost_notLoggedIn_valid(): void {
        $this->unsetGeefterInSession();

        $this->getAppConfig()->getValue('CAPTCHA_CONTACT_VISITOR_ENABLE', 1);

        /** @var \Geeftlist\Form\Element\ContactSubjectSelect $subjectSelect */
        $subjectSelect = $this->getGeeftlistModelFactory()->make(ContactSubjectSelect::class);

        $subject = array_rand($subjectSelect->getOptions(), 1);
        $message = $this->faker()->paragraphs(1, true);
        $requesterEmail = 'foo@example.org';

        $captcha = $this->generateCaptchaInSession('contact_form_captcha');

        $this->getEmailSender()->clearSentEmails();

        try {
            $this->mock('POST /contact/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'subject'         => $subject,
                'message'         => $message,
                'requester_email' => $requesterEmail,
                'captcha' => [
                    'id'     => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $sentEmails = $this->getEmailSender()->getSentEmails(true);

        $this->assertEquals(1, count($sentEmails));
        $this->assertEquals($this->getAppConfig()->getValue('EMAIL_CONTACT'), $sentEmails[0]['to']);
        $this->assertEquals($requesterEmail, $sentEmails[0]['reply_to']);
        $this->assertMatchesRegularExpression(
            sprintf('/%s/', preg_quote($subject)),
            $sentEmails[0]['body'],
            "Cannot find subject in email's body"
        );
        $this->assertMatchesRegularExpression(
            sprintf('/%s/', preg_quote($message, '/')),
            $sentEmails[0]['body'],
            "Cannot find message in email's body"
        );
        $this->assertValidHtmlWithoutErrors($sentEmails[0]['body']);
    }

    public function testPost_loggedIn_valid(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->getAppConfig()->getValue('CAPTCHA_CONTACT_VISITOR_ENABLE', 1);

        /** @var \Geeftlist\Form\Element\ContactSubjectSelect $subjectSelect */
        $subjectSelect = $this->getGeeftlistModelFactory()->make(ContactSubjectSelect::class);

        $subject = array_rand($subjectSelect->getOptions(), 1);
        $message = $this->faker()->paragraphs(1, true);

        $captcha = $this->generateCaptchaInSession('contact_form_captcha');

        $this->getEmailSender()->clearSentEmails();

        try {
            $this->mock('POST /contact/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'subject' => $subject,
                'message' => $message,
                'captcha' => [
                    'id'     => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }

        $sentEmails = $this->getEmailSender()->getSentEmails(true);

        $this->assertEquals(1, count($sentEmails));
        $this->assertEquals($this->getAppConfig()->getValue('EMAIL_CONTACT'), $sentEmails[0]['to']);
        $this->assertEquals($jane->getEmail(), $sentEmails[0]['reply_to']);
        $this->assertMatchesRegularExpression(
            sprintf('/%s/', preg_quote($subject)),
            $sentEmails[0]['body'],
            "Cannot find subject in email's body"
        );
        $this->assertMatchesRegularExpression(
            sprintf('/%s/', preg_quote($message, '/')),
            $sentEmails[0]['body'],
            "Cannot find message in email's body"
        );
        $this->assertValidHtmlWithoutErrors($sentEmails[0]['body']);
    }
}
