<?php

namespace Geeftlist\Test\Controller\Reservation;


use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class EditTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    /** @var bool */
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->enableGeefterAccessListeners();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    public function testIndex_notLoggedIn(): void {
        try {
            $this->mock('GET /gift_reservation/index');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndex_newReservation(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        $this->mock('GET /gift_reservation/index/gift_id/' . $newGiftForJohn->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-reservation-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
        $this->assertDoesNotMatchRegularExpression('/="checked"/i', $response);
    }

    public function testIndex_newReservation_allowed(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $this->setGeefterInSession($john);

        /** @var \Geeftlist\Model\Family $johnRichardFamily */
        $johnRichardFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($john, $richard, $johnRichardFamily): void {
            $johnRichardFamily->addGeeftee([
                $john->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $newGiftForRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $richard->getGeeftee()->addGift($newGiftForRichard);

        // As John
        $this->mock('GET /gift_reservation/index/gift_id/' . $newGiftForRichard->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-reservation-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);

        // As Jane, once member of a common family
        $this->callPrivileged(static function () use ($jane, $johnRichardFamily): void {
            $johnRichardFamily->addGeeftee($jane->getGeeftee());
        });

        $this->mock('GET /gift_reservation/index/gift_id/' . $newGiftForRichard->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-reservation-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-394
     */
    public function testIndex_newReservation_notAllowed(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $this->setGeefterInSession($john);

        /** @var \Geeftlist\Model\Family $johnRichardFamily */
        $johnRichardFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($john, $richard, $johnRichardFamily): void {
            $johnRichardFamily->addGeeftee([
                $john->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $newGiftForRichard = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId()
        ]])->save();
        $richard->getGeeftee()->addGift($newGiftForRichard);

        foreach ([$richard, $richard2, $jane] as $geefter) {
            $this->setGeefterInSession($geefter);
            $this->clearCacheArrayObjects();

            try {
                $this->mock('GET /gift_reservation/index/gift_id/' . $newGiftForRichard->getId());
                $this->fail('Failed rerouting on missing permissions.');
            }
            catch (Forward $e) {
                $this->assertEquals('GET /gift/notfound', $e->getRoute());
                $this->assertEmpty($this->getResponse()->getBody());
            }
        }
    }

    public function testIndex_existingReservation(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id' => $newGiftForJohn->getId(),
            'geefter_id' => $jane->getId(),
            'type' => Reservation::TYPE_PURCHASE,
            'amount' => 12.34
        ]])->save();
        $this->assertNotNull($reservation->getId());

        $this->mock('GET /gift_reservation/index/gift_id/' . $reservation->getGiftId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-reservation-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);

        // Use inlined HTML not to depend on HTML formatting and new lines
        $inlineResponseHtml = str_replace("\n", ' ', (string) $response);
        $this->assertMatchesRegularExpression(
            sprintf('/%s.*?value="%s".*?checked/i', preg_quote('<input'), $reservation->getType()),
            $inlineResponseHtml
        );
        $this->assertMatchesRegularExpression(
            sprintf('/%s.*?value="%s".*?/i', preg_quote('<input'), preg_quote((string) $reservation->getAmount())),
            $inlineResponseHtml
        );
    }

    public function testPost_invalidForm(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
            ]);
            $this->fail('Failed rerouting on missing action');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type' => 'foo'
            ]);
            $this->fail('Failed rerouting on invalid action');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type'   => Reservation::TYPE_RESERVATION,
                'amount' => 'foo'
            ]);
            $this->fail('Failed rerouting on invalid amount');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type'   => 'cancel'
            ]);
            $this->fail('Failed rerouting on cancelling nonexisting reservation');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }
    }

    /**
     * @group ticket-400
     */
    public function testPost_validNew(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Gift $newGiftForJohn */
        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type'   => Reservation::TYPE_RESERVATION,
                'amount' => ''
            ]);
            $this->fail('Failed rerouting on valid form');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $reservation = $this->getEntity(Reservation::ENTITY_TYPE, ['gift_id' => $newGiftForJohn->getId()]);
        $this->assertNotNull($reservation->getId());
        $this->assertEquals(Reservation::TYPE_RESERVATION, $reservation->getType());
        $this->assertNull($reservation->getAmount());
    }

    /**
     * @group ticket-250
     */
    public function testPost_validNew_withLists(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Gift $newGiftForJohn */
        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        /** @var GiftList $randomList */
        $randomList = $this->faker()->randomElement($this->getEntityItems(GiftList::ENTITY_TYPE, [
            'filter' => [
                'status' => [['eq' => GiftList\Field\Status::ACTIVE]],
            ]
        ]));
        $this->assertNotNull($randomList);

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type'   => Reservation::TYPE_RESERVATION,
                'amount' => '',
                'giftlist_ids' => $randomList->getId()
            ]);
            $this->fail('Failed rerouting on valid form');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $reservation = $this->getEntity(Reservation::ENTITY_TYPE, ['gift_id' => $newGiftForJohn->getId()]);
        $this->assertNotNull($reservation->getId());
        $this->assertEquals(Reservation::TYPE_RESERVATION, $reservation->getType());
        $this->assertNull($reservation->getAmount());

        $this->assertContainsEquals($newGiftForJohn->getId(), $randomList->getGiftIds());
    }

    /**
     * @group ticket-400
     */
    public function testPost_validUpdate(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'geefter_id' => $jane->getId(),
            'gift_id' => $newGiftForJohn->getId(),
            'type' => Reservation::TYPE_RESERVATION
        ]])->save();

        $this->assertNotNull($reservation->getId());
        $this->assertEquals(Reservation::TYPE_RESERVATION, $reservation->getType());
        $this->assertNull($reservation->getAmount());

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type'   => Reservation::TYPE_PURCHASE,
                'amount' => '12.34'
            ]);
            $this->fail('Failed rerouting on valid form');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $reservation = $this->getEntity(Reservation::ENTITY_TYPE, ['gift_id' => $newGiftForJohn->getId()]);
        $this->assertNotNull($reservation->getId());
        $this->assertEquals(Reservation::TYPE_PURCHASE, $reservation->getType());
        $this->assertEquals('12.34', $reservation->getAmount());
    }

    /**
     * @group ticket-250
     */
    public function testPost_validUpdate_withLists(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $newGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGiftForJohn);

        /** @var GiftList $randomList */
        $randomList = $this->faker()->randomElement($this->getEntityItems(GiftList::ENTITY_TYPE, [
            'filter' => [
                'status' => [['eq' => GiftList\Field\Status::ACTIVE]],
            ]
        ]));
        $this->assertNotNull($randomList);
        $this->getContainer()->get(GiftList\Gift::class)
            ->addLinks([$randomList], [$newGiftForJohn]);


        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'geefter_id' => $jane->getId(),
            'gift_id' => $newGiftForJohn->getId(),
            'type' => Reservation::TYPE_RESERVATION
        ]])->save();

        $this->assertNotNull($reservation->getId());
        $this->assertEquals(Reservation::TYPE_RESERVATION, $reservation->getType());
        $this->assertNull($reservation->getAmount());
        $this->assertContainsEquals($newGiftForJohn->getId(), $randomList->getGiftIds());

        try {
            $this->mock('POST /gift_reservation/post/gift_id/' . $newGiftForJohn->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'type'   => Reservation::TYPE_PURCHASE,
                'amount' => '12.34',
                'giftlist_ids' => ''
            ]);
            $this->fail('Failed rerouting on valid form');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $reservation = $this->getEntity(Reservation::ENTITY_TYPE, ['gift_id' => $newGiftForJohn->getId()]);
        $this->assertNotNull($reservation->getId());
        $this->assertEquals(Reservation::TYPE_PURCHASE, $reservation->getType());
        $this->assertEquals('12.34', $reservation->getAmount());

        $this->assertNotContainsEquals($newGiftForJohn->getId(), $randomList->getGiftIds());
    }
}
