<?php

namespace Geeftlist\Test\Controller\Reservation;


use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ListControllerTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->enableGeefterAccessListeners();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testList_notLoggedIn(): void {
        try {
            $this->mock('GET /gift_reservation/list');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testList_loggedIn(): void {
        $fw = $this->getFw();

        $this->setGeefterInSession($this->getJane());

        $this->mock('GET /gift_reservation/list');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-reservation-list.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testList_loggedIn_noFamily(): void {
        $fw = $this->getFw();

        $this->setGeefterInSession($this->getRichard());

        $this->mock('GET /gift_reservation/list');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-reservation-list.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndex_notLoggedIn(): void {
        try {
            $this->mock('GET /gift_reservation/index');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }
}
