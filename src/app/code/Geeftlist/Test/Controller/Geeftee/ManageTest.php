<?php

namespace Geeftlist\Test\Controller\Geeftee;


use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ManageTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        $this->disableGeefterAccessListeners();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testNew_notLoggedIn(): void {
        try {
            $this->mock('GET /geeftee_manage/new');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testNew_loggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /geeftee_manage/new');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?geeftee-manage-new.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testSearch_loggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /geeftee_manage/search');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?geeftee-manage-search.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testSearchPost_nonExistingEmail(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /geeftee_manage/searchPost', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
            ]);
            $this->fail('Failed rerouting on non-existing email');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringStartsWith($this->getUrl('geeftee_manage/new'), $reroute->getUrl());
        }
    }

    public function testSearchPost_existingEmail(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        try {
            $this->mock(
                'POST /geeftee_manage/searchPost',
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'email' => $this->getJohn()->getEmail()
                ],
                ['Referer' => $this->getUrl('geeftee_manage/search')]
            );
            $this->fail('Failed rerouting on existing email');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/search'), $reroute->getUrl());
        }
    }

    /**
     * @group optim20230415
     */
    public function testEdit_notLoggedIn(): void {
        $richard = $this->getRichard();
        $aGeeftee = $this->generateGeefterlessGeeftee($richard);

        try {
            $this->mock('GET /geeftee_manage/edit/geeftee_id/' . $aGeeftee->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * @group optim20230415
     */
    public function testEdit_asNotCreator(): void {
        $richard = $this->getRichard();
        $aGeeftee = $this->generateGeefterlessGeeftee($this->getJohn());

        $this->setGeefterInSession($richard);

        try {
            $this->mock(
                'GET /geeftee_manage/edit/geeftee_id/' . $aGeeftee->getId(),
                [],
                ['Referer' => $this->getUrl('dashboard/index')]
            );
            $this->fail('Failed rerouting unauthorized page to referer');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('dashboard/index'), $reroute->getUrl());
        }
    }

    /**
     * @group optim20230415
     */
    public function testEdit_asCreator(): void {
        $fw = $this->getFw();

        $richard = $this->getRichard();
        $aGeeftee = $this->generateGeefterlessGeeftee($richard);

        $this->setGeefterInSession($richard);

        $this->mock('GET /geeftee_manage/edit/geeftee_id/' . $aGeeftee->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?geeftee-manage-edit.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group optim20230415
     */
    public function testSave_new_notLoggedIn(): void {
        try {
            $this->mock(
                'POST /geeftee_manage/save',
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
                    'email' => uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
                ]
            );
            $this->fail('Failed rerouting unauthorized page to login');
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * @group optim20230415
     */
    public function testSave_new_invalid_emptyName(): void {
        $richard = $this->getRichard();
        $family = $this->generateFamily($richard);

        $this->setGeefterInSession($richard);

        $this->enableGeefterAccessListeners();
        $email = uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        try {
            $this->mock(
                'POST /geeftee_manage/save',
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'email'     => $email,
                    'family_id' => $family->getId()
                ],
                ['Referer' => $this->getUrl('geeftee_manage/edit')]
            );
            $this->fail('Failed rerouting on missing name');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/edit'), $reroute->getUrl());

            $this->assertNull($this->getEntity(Geeftee::ENTITY_TYPE, ['email' => $email]));
        }
    }

    /**
     * @group optim20230415
     */
    public function testSave_new_invalid_emptyFamily(): void {
        $richard = $this->getRichard();

        $this->setGeefterInSession($richard);

        $this->enableGeefterAccessListeners();
        $name = uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX);
        $email = uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        try {
            $this->mock(
                'POST /geeftee_manage/save',
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name' => $name,
                    'email' => $email
                ],
                ['Referer' => $this->getUrl('geeftee_manage/edit')]
            );
            $this->fail('Failed rerouting on missing name');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/edit'), $reroute->getUrl());

            $this->assertNull($this->getEntity(Geeftee::ENTITY_TYPE, ['email' => $email]));
        }
    }

    /**
     * @group optim20230415
     */
    public function testSave_new_valid_withEmail(): void {
        $richard = $this->getRichard();
        $family = $this->generateFamily($richard);

        $this->setGeefterInSession($richard);

        $this->enableGeefterAccessListeners();
        $name = uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX);
        $email = uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        try {
            $this->mock(
                'POST /geeftee_manage/save',
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name'  => $name,
                    'email' => $email,
                    'family_id' => $family->getId()
                ],
                ['Referer' => $this->getUrl('geeftee_manage/edit')]
            );
            $this->fail('Failed rerouting on valid form');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/edit'), $reroute->getUrl());

            $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, ['email' => $email]);
            $this->assertNotNull($geeftee);
            $this->assertEquals($name, $geeftee->getName());
        }
    }

    /**
     * @group optim20230415
     */
    public function testSave_new_valid_withoutEmail(): void {
        $richard = $this->getRichard();
        $family = $this->generateFamily($richard);

        $this->setGeefterInSession($richard);

        $this->enableGeefterAccessListeners();
        $name = uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX);
        try {
            $this->mock(
                'POST /geeftee_manage/save',
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name'  => $name,
                    'no_email' => 1,
                    'family_id' => $family->getId()
                ],
                ['Referer' => $this->getUrl('geeftee_manage/edit')]
            );
            $this->fail('Failed rerouting on valid form');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/edit'), $reroute->getUrl());

            $this->assertNotNull($this->getEntity(Geeftee::ENTITY_TYPE, ['name' => $name]));
        }
    }

    /**
     * @group optim20230415
     */
    public function testSave_existing_notLoggedIn(): void {
        $richard = $this->getRichard();
        $aGeeftee = $this->generateGeefterlessGeeftee($richard);

        try {
            $this->mock(
                'POST /geeftee_manage/save/geeftee_id/' . $aGeeftee->getId(),
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name'  => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
                    'email' => uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
                ]
            );
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * @group optim20230415
     */
    public function testSave_existing_asNotCreator(): void {
        $richard = $this->getRichard();
        $aGeeftee = $this->generateGeefterlessGeeftee($richard);

        $this->setGeefterInSession($this->getJohn());

        $this->enableGeefterAccessListeners();
        $email = uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        try {
            $this->mock(
                'POST /geeftee_manage/save/geeftee_id/' . $aGeeftee->getId(),
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name'  => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
                    'email' => $email
                ],
                ['Referer' => $this->getUrl('geeftee_manage/edit')]
            );
            $this->fail('Failed rerouting on save as not creator');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/edit'), $reroute->getUrl());

            $this->assertNull($this->getEntity(Geeftee::ENTITY_TYPE, ['email' => $email]));
        }

        $this->getGeefterSession()->clearMessages();
    }

    /**
     * @group optim20230415
     */
    public function testSave_existing_asCreator(): void {
        $richard = $this->getRichard();
        $aGeeftee = $this->generateGeefterlessGeeftee($richard);
        $geefteeId = $aGeeftee->getId();

        $this->setGeefterInSession($richard);

        $this->enableGeefterAccessListeners();
        $newName = uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX);
        $newEmail = uniqid() . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        try {
            $this->mock(
                'POST /geeftee_manage/save/geeftee_id/' . $geefteeId,
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'name'  => $newName,
                    'email' => $newEmail
                ],
                ['Referer' => $this->getUrl('geeftee_manage/edit')]
            );
            $this->fail('Failed rerouting on save as creator');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_WARN));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('geeftee_manage/edit'), $reroute->getUrl());

            $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, $geefteeId);
            $this->assertNotNull($geeftee);
            $this->assertEquals($newName, $geeftee->getName());
            $this->assertEquals($newEmail, $geeftee->getEmail());
        }

        $this->getGeefterSession()->clearMessages();
    }
}
