<?php

namespace Geeftlist\Test\Controller\Geeftee\Manage;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ClaimRequestTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        $this->disableGeefterAccessListeners();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        try {
            $this->mock('GET /geeftee_manage_claim_request/index');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /geeftee_manage_claim_request/index');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?geeftee-manage-claim-request-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-387
     */
    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        // Ensure at least 1 CR of both types exist
        $fixedClaimRequests = $this->generateClaimRequestCuts($john);

        $maxRecords = 100;
        $expectedClaimRequests = $this->getEntityCollection(Geeftee\ClaimRequest::ENTITY_TYPE, [
            'geefter_id' => $john->getId()
        ]);
        $expectedClaimRequestIds = $expectedClaimRequests->getAllIds();
        $expectedClaimRequests = $this->getEntityCollection(Geeftee\ClaimRequest::ENTITY_TYPE)
            ->addJoinRelation('geeftee')
            ->addFieldToFilter('geeftee.creator_id', $john->getId());
        $expectedClaimRequestIds = array_unique(array_merge(
            $expectedClaimRequestIds,
            $expectedClaimRequests->getAllIds()
        ));

        $this->mock('GET /geeftee_manage_claim_request/grid [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals($response['recordsTotal'], $response['recordsFiltered']);
        $claimRequestIds = array_column($response['items'], 'claim_request_id');
        $this->assertNotEmpty($claimRequestIds);
        foreach ($fixedClaimRequests as $fixedClaimRequest) {
            $this->assertContainsEquals($fixedClaimRequest->getId(), $claimRequestIds);
        }

        $this->assertEqualsCanonicalizing($expectedClaimRequestIds, $claimRequestIds);
    }

    protected function generateClaimRequestCuts(Geefter $geefter) {
        return $this->executeOnlyWithListeners(['default'], function() use ($geefter): array {
            $geefterlessGeeftees = $this->getEntityCollection(Geeftee::ENTITY_TYPE, [
                'geefter_id' => ['null' => true],
                'families' => ['in' => $geefter->getFamilyCollection()->getAllIds()]
            ]);

            $cr1 = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
                'geeftee_id' => $geefterlessGeeftees->getFirstItem()->getId(),
                'geefter_id' => $geefter->getId()
            ]])->save();

            $anotherGeefter = $this->getGeefterBut($geefter);
            $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
                'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
                'creator_id' => $geefter->getId()
            ]])->save();

            $cr2 = $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE, ['data' => [
                'geeftee_id' => $geefterlessGeeftee->getId(),
                'geefter_id' => $anotherGeefter->getId()
            ]])->save();

            return [$cr1, $cr2];
        });
    }
}
