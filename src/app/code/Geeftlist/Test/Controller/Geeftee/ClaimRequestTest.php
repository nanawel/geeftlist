<?php

namespace Geeftlist\Test\Controller\Geeftee;


use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ClaimRequestTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        $this->disableGeefterAccessListeners();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-458
     */
    public function testCreate(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
        ]])->save();

        $this->assertNotNull($geefterlessGeeftee);

        $this->mock('GET /geeftee_claim_request/create/geeftee_id/'. $geefterlessGeeftee->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?geeftee-claim-request-create.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-551
     */
    public function testCreate_creatorIsCurrentGeefter(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
        ]])->save();

        $this->assertEquals($jane->getId(), $geeftee->getCreatorId());

        $this->mock('GET /geeftee_claim_request/create/geeftee_id/'. $geeftee->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?geeftee-claim-request-create.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-551
     */
    public function testCreatePost(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $this->getJohn()->getId()
        ]])->save();

        try {
            $this->mock('POST /geeftee_claim_request/createPost/geeftee_id/'. $geefterlessGeeftee->getId(), [
                'confirmation' => 'ok',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
        } catch (Reroute $reroute) {
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));

            $this->assertNotNull(
                $this->getRepository(Geeftee::ENTITY_TYPE)->get($geefterlessGeeftee->getId())
            );
            $this->assertCount(
                1,
                $this->getRepository(Geeftee\ClaimRequest::ENTITY_TYPE)->find([
                    'filter' => [
                        'geeftee_id' => [['eq' => $geefterlessGeeftee->getId()]],
                        'geefter_id' => [['eq' => $jane->getId()]],
                        'decision_code' => [['eq' => Geeftee\ClaimRequest::DECISION_CODE_PENDING]],
                    ]
                ])
            );
        }
    }

    /**
     * @group ticket-551
     */
    public function testCreatePost_creatorIsCurrentGeefter(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
        ]])->save();

        $this->assertEquals($jane->getId(), $geeftee->getCreatorId());

        try {
            $this->mock('POST /geeftee_claim_request/createPost/geeftee_id/'. $geeftee->getId(), [
                'confirmation' => 'ok',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
        } catch (Reroute $reroute) {
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));

            $this->assertNull(
                $this->getRepository(Geeftee::ENTITY_TYPE)->get($geeftee->getId())
            );
            $this->assertCount(
                1,
                $this->getRepository(Geeftee\ClaimRequest::ENTITY_TYPE)->find([
                    'filter' => [
                        'geeftee_id' => [['eq' => $geeftee->getId()]],
                        'geefter_id' => [['eq' => $jane->getId()]],
                        'decision_code' => [['eq' => Geeftee\ClaimRequest::DECISION_CODE_ACCEPTED]],
                    ]
                ])
            );
        }
    }
}
