<?php

namespace Geeftlist\Test\Controller\Geeftee;


use Geeftlist\Model\Geeftee;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ProfileTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        // We need permission restrictions here to draw related geeftees in test cases
        $this->enableGeefterAccessListeners();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-458
     */
    public function testIndex(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, [
            'geefter_id' => ['null' => false]
        ]);

        $this->mock('GET /geeftee_profile/index/geeftee_id/' . $geeftee->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?geeftee-profile-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertDoesNotMatchRegularExpression(
            '/<div\s.*?class=".*?claim-profile-link.*?"/',
            $response,
            'Invalid tag found in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-458
     */
    public function testIndexGeefterlessGeeftee(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $geeftee = $this->getEntity(Geeftee::ENTITY_TYPE, [
            'geefter_id' => ['null' => true],
            'creator_id' => ['ne' => $jane->getId()]
        ]);

        $this->assertNotNull($geeftee);

        $this->mock('GET /geeftee_profile/index/geeftee_id/' . $geeftee->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?geeftee-profile-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }
}
