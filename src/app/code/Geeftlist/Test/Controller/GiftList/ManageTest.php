<?php

namespace Geeftlist\Test\Controller\GiftList;


use Geeftlist\Model\GiftList;
use Geeftlist\Model\ResourceModel\Db\GiftList\Collection;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ManageTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        try {
            $this->mock('GET /giftlist_manage/index');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /giftlist_manage/index');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?giftlist-manage-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testGrid(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $maxRecords = 100;
        /** @var Collection $expectedGiftLists */
        $expectedGiftLists = $this->getEntityCollection(GiftList::ENTITY_TYPE)
            ->addFieldToFilter('owner_id', $john->getId())
            ->addFieldToFilter('status', GiftList\Field\Status::ACTIVE)
            ->setLimit($maxRecords);
        $expectedTotalGiftLists = $this->getEntityCollection(GiftList::ENTITY_TYPE)
            ->addFieldToFilter('owner_id', $john->getId())
            ->setLimit($maxRecords);
        $expectedGiftListIds = $expectedGiftLists->getAllIds();

        $this->mock('GET /giftlist_manage/grid [ajax]', [
            'length' => $maxRecords,
            $this->getCsrfTokenName() => $this->getCsrfToken()
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $giftListIds = array_column($response['items'], 'giftlist_id');
        $this->assertNotEmpty($giftListIds, 'No list found. Are sample data installed?');
        $this->assertEqualsCanonicalizing($expectedGiftListIds, $giftListIds);
        $this->assertEquals(count($expectedTotalGiftLists), $response['recordsTotal']);
    }

    public function testNew(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock("GET /giftlist_manage/new");
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?giftlist-manage-new.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testEditNotOwner(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var GiftList $giftList */
        $giftList = $this->callPrivileged(fn() => $this->getEntityCollection(GiftList::ENTITY_TYPE)
            ->addFieldToFilter('owner_id', ['neq' => $jane->getId()])
            ->getFirstItem());
        $this->assertNotNull($giftList, 'No list found. Are sample data installed?');

        try {
            $this->mock('GET /giftlist_manage/edit/giftlist_id/' . $giftList->getId());
            $this->fail('Failed rerouting while attempting to edit a giftlist as a non-owner.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
        }
    }

    public function testEditOwner(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var GiftList $giftList */
        $giftList = $this->getEntityCollection(GiftList::ENTITY_TYPE)
            ->addFieldToFilter('owner_id', $jane->getId())
            ->getFirstItem();
        $this->assertNotNull($giftList, 'No list found. Are sample data installed?');

        $this->mock('GET /giftlist_manage/edit/giftlist_id/' . $giftList->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?giftlist-manage-edit.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertHtmlAttributeEquals($giftList->getName(), ['//input[@name="name"]', 'value'], $response);
    }
}
