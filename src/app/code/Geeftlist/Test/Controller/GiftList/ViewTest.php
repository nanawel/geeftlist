<?php

namespace Geeftlist\Test\Controller\GiftList;


use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ViewTest extends TestCase
{
    use FakerTrait;
    use TestDataTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndex_notLoggedIn(): void {
        $giftList = $this->getEntity(GiftList::ENTITY_TYPE);
        $this->assertNotNull($giftList);
        $this->assertNotNull($giftList->getId());

        try {
            $this->mock('GET /giftlist/view/giftlist_id/' . $giftList->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexL_loggedIn_activeList(): void {
        $fw = $this->getFw();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $this->setGeefterInSession($richard);

        $this->generateFamily($richard, [$richard2, $richard3]);

        /** @var Gift[] $activeGifts */
        $activeGifts = $this->generateGifts($richard, [$richard2], Gift\Field\Status::AVAILABLE, 3);
        $this->assertCount(3, $activeGifts);

        /** @var Gift[] $archivedGifts */
        $archivedGifts = $this->generateGifts($richard, [$richard3], Gift\Field\Status::ARCHIVED, 1);
        $this->assertCount(1, $archivedGifts);

        /** @var GiftList $giftList */
        $giftList = $this->generateGiftList($richard, array_merge($activeGifts, $archivedGifts));

        $this->mock('GET /giftlist/view/giftlist_id/' . $giftList->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?giftlist-view.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $matchCount = preg_match_all('/class="gift /', (string) $response);
        $this->assertEquals(3, $matchCount);    // Only 3 are AVAILABLE, and so present in the list

        $this->assertHtmlAttributesEqual(
            array_keys($activeGifts),
            '//*[@data-entity-type="gift"]/@data-id',
            $response
        );
    }

    public function testIndex_loggedIn_archivedList(): void {
        $fw = $this->getFw();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $this->setGeefterInSession($richard);

        $this->generateFamily($richard, [$richard2, $richard3]);

        /** @var Gift[] $activeGifts */
        $activeGifts = $this->generateGifts($richard, [$richard2], Gift\Field\Status::AVAILABLE, 3);
        $this->assertCount(3, $activeGifts);

        /** @var Gift[] $archivedGifts */
        $archivedGifts = $this->generateGifts($richard, [$richard3], Gift\Field\Status::ARCHIVED, 1);
        $this->assertCount(1, $archivedGifts);

        $allGifts = $activeGifts + $archivedGifts;
        $this->assertCount(4, $allGifts);

        /** @var GiftList $giftList */
        $giftList = $this->generateGiftList($richard, $allGifts);
        $giftList->setStatus(GiftList\Field\Status::ARCHIVED);
        $this->getRepository(GiftList::ENTITY_TYPE)
            ->save($giftList);

        $this->mock('GET /giftlist/view/giftlist_id/' . $giftList->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?giftlist-view.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $matchCount = preg_match_all('/class="gift /', (string) $response);
        $this->assertEquals(4, $matchCount);    // 3+1 because the list is archived so ARCHIVED gifts are also visible

        $this->assertHtmlAttributesEqual(
            array_keys($allGifts),
            '//*[@data-entity-type="gift"]/@data-id',
            $response
        );
    }
}
