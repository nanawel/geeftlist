<?php

namespace Geeftlist\Test\Controller;


use Geeftlist\Model\Invitation;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class InvitationTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;

    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        $this->getAppConfig()->setValue('CAPTCHA_INVITATION_CREATION_ENABLE', true);
        $this->getAppConfig()->setValue('INVITATION_ENABLED', true);
        $this->getAppConfig()->setValue('INVITATION_MAX_PER_GEEFTER', 0);

        $this->getRepository(\Geeftlist\Model\Invitation::ENTITY_TYPE)
            ->deleteAll();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-668
     */
    public function testCreateNotLoggedIn(): void {
        try {
            $this->mock('GET /invitation/create');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * @group ticket-668
     */
    public function testCreateLoggedIn(): void {
        $fw = $this->getFw();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /invitation/create');

        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<form\s.*?name="invitation-create"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testPost_invalidCaptcha(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        // Simulate request coming from the "create" form
        $this->getSession()->setCreateInvitationFormNonce(time());
        $captcha = $this->generateCaptchaInSession('create_invitation_form_captcha');

        try {
            $this->mock('POST /invitation/generate', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => 'foo'
                ]
            ]);
            $this->fail('Failed rerouting after invalid captcha submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getI18n()->tr('Invalid captcha answer. Please retry.'),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))->getMessage()
            );
            $this->assertStringEndsWith($this->getUrl('/invitation/create'), $reroute->getUrl());
        }
    }

    /**
     * @group ticket-668
     */
    public function testPost_valid_withCaptcha(): void {
        $fw = $this->getFw();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        // Simulate request coming from the "create" form
        $this->getSession()->setCreateInvitationFormNonce(time());
        $captcha = $this->generateCaptchaInSession('create_invitation_form_captcha');

        $this->mock('POST /invitation/generate', [
            $this->getCsrfTokenName() => $this->getCsrfToken(),
            'captcha'  => [
                'id' => $captcha->getId(),
                'answer' => $captcha->getPhrase()
            ]
        ]);
        $response = $fw->get('RESPONSE');

        /** @var Invitation $latestInvitation */
        $latestInvitation = $this->getEntity(Invitation::ENTITY_TYPE);

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class="invitation-generate/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertHtmlAttributeEquals(
            $this->getContainer()->get(\Geeftlist\Service\Invitation::class)->getSignupLink($latestInvitation),
            ['//input[@id="invitation-link-input"]', 'value'],
            $response,
            'Invalid generated signup invitation URL'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }
}
