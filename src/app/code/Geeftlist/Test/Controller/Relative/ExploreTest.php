<?php

namespace Geeftlist\Test\Controller\Relative;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ExploreTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $family = $this->getEntity(Family::ENTITY_TYPE);
        $this->assertNotNull($family);
        $this->assertNotNull($family->getId());

        try {
            $this->mock('GET /relative_explore/index/family_id/' . $family->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $this->setGeefterInSession($richard);

        // No family, no relative
        $this->mock('GET /relative_explore/index');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?relative-explore-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);

        $matchCount = preg_match_all('/<li class="geeftee-title/', (string) $response, $matches);
        $this->assertEquals(0, $matchCount);

        $this->disableGeefterAccessListeners();

        // Adding to 2 families
        $janeFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($jane, $richard, $janeFamily): void {
            $janeFamily->addGeeftee([
                $jane->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $johnFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($john, $richard, $johnFamily): void {
            $johnFamily->addGeeftee([
                $john->getGeeftee(),
                $richard->getGeeftee()
            ]);
        });

        $this->enableGeefterAccessListeners();

        $this->mock('GET /relative_explore/index');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?relative-explore-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $matchCount = preg_match_all('/class="geeftee-tab"/', (string) $response, $matches);
        $this->assertEquals(3, $matchCount);
    }

    public function testIndex_displayedGifts(): void {
        $fw = $this->getFw();

        $cuts = $this->generateCutsLight();
        /** @var Geefter[] $geefters */
        $geefters = $cuts['geefters'];
        $relativesByGeefter = $cuts['relativesByGeefter'];

        $seenGifts = 0;
        foreach ($geefters as $geefter) {
            $this->setGeefterInSession($geefter);
            $this->enableGeefterAccessListeners();

            /** @var Gift[] $refGifts */
            $refGifts = $this->getEntityCollection(
                Gift::ENTITY_TYPE,
                [
                    'status' => Gift\Field\Status::AVAILABLE
                ])
                ->addFieldToFilter('geeftees', ['in' => array_keys($relativesByGeefter[$geefter->getId()])])
                ->getItems()
            ;

            $this->mock('GET /relative_explore/index');
            $response = $fw->get('RESPONSE');

            // 1. Check that all visible gifts to the geefter are present in the page
            if ($refGifts) {
                foreach ($refGifts as $refGift) {
                    $this->assertMatchesRegularExpression(
                        sprintf('|<a .*?/gift/view/gift_id/(%d)|', $refGift->getId()),
                        $response,
                        sprintf("Link to gift #%d '%s' (creator: %s) not found in page as %s.",
                            $refGift->getId(),
                            $refGift->getLabel(),
                            $refGift->getCreatorUsername(),
                            $geefter->getUsername()
                        )
                    );
                    ++$seenGifts;
                }
            }
            else {
                $this->assertEquals(0, preg_match_all('|<a .*?/gift/view/gift_id/(\d+)|', (string) $response));
                continue;
            }

            // 2. Check that all gifts in the page are visible to the geefter
            $this->assertGreaterThan(0, preg_match_all('|<a .*?/gift/view/gift_id/(\d+)|', (string) $response, $matches));
            $actualGiftIds = $matches[1];

            $this->disableGeefterAccessListeners();

            $giftCollection = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'gift_id' => ['in' => $actualGiftIds]
            ]);

            foreach ($giftCollection as $gift) {
                $this->assertEquals(Gift\Field\Status::AVAILABLE, $gift->getStatus());
                $this->assertTrue(
                    $this->getGeefterPermissionService()->isAllowed($geefter, $gift, TypeInterface::ACTION_VIEW),
                    sprintf('Failed asserting that %s should be able to view gift #%s ', $geefter->getUsername(), $gift->getId())
                    . sprintf('(%s / %s) on family/explore page.', $gift->getLabel(), $gift->getDescription())
                );

                // Add new validation tests here
            }

            $this->assertGreaterThan(0, $seenGifts);
        }
    }
}
