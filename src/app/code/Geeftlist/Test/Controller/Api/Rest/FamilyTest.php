<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\Family;
use Geeftlist\Test\Constants;
use Narrowspark\HttpStatus\Exception\UnprocessableEntityException;
use Narrowspark\HttpStatus\HttpStatus;
use Neomerx\JsonApi\Schema\Error;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Validation\ErrorInterface;

/**
 * @group api
 * @group controllers
 */
class FamilyTest extends AbstractModelControllerTest
{
    protected $entityCode = Family::ENTITY_TYPE;

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    public function testPreventUpdateOwnerId(): void {
        $this->enableGeefterAccessListeners();
        $john = $this->getJohn();
        $entity = $this->getNewEntity($john)
            ->save();

        $this->assertNotNull($entity->getId());

        $updatedData = $this->getUpdateEntityData($john);
        $updatedData['attributes']['owner_id'] = $this->getJane()->getId();

        try {
            $this->mockApiEntityRequest(
                Http::VERB_PATCH,
                '',
                ['id' => $entity->getId()],
                [],
                $this->getAuthenticationHeaders($john),
                json_encode(['data' => $updatedData])
            );
            $this->fail('Failed preventing update of owner ID.');
        }
        catch (UnprocessableEntityException) {}

        /** @var Error[] $responseErrors */
        $responseErrors = $this->getResponse()->getErrors();

        $this->assertEquals([
            'status' => HttpStatus::STATUS_BAD_REQUEST,
            'code' => ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE,
            'title' => 'Cannot update this field.',
            'meta' => [
                'field' => 'owner_id'
            ]
        ], $responseErrors[0]);
    }

    protected function getGetEntityId($asGeefter) {
        return $this->getEntity($this->entityCode, [
            'owner_id' => $asGeefter->getId()
        ])->getId();
    }

    protected function getCreateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'name'     => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                'owner_id' => $asGeefter->getId()
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        return $this->getEntity($this->entityCode, [
            'owner_id' => $asGeefter->getId()
        ])->getId();
    }

    protected function prepareCollection($asGeefter): static {
        // nothing to do here (families should already exist in sample data)

        return $this;
    }
}
