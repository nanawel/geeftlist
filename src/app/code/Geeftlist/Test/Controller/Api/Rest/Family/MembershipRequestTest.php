<?php

namespace Geeftlist\Test\Controller\Api\Rest\Family;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Family;
use Geeftlist\Model\Family\MembershipRequest;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractModelControllerTest;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class MembershipRequestTest extends AbstractModelControllerTest
{
    protected $entityCode = MembershipRequest::ENTITY_TYPE;

    protected $expectedAllowedMethods = [
        Http::VERB_GET,
        Http::VERB_POST,
        Http::VERB_OPTIONS
    ];

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    protected function getGetEntityId($asGeefter) {
        $family = $this->getEntity(Family::ENTITY_TYPE, [
            'family_id' => ['nin' => $asGeefter->getFamilyCollection()->getAllIds()],
            'visibility' => ['ne' => Family\Field\Visibility::PRIVATE]
        ]);

        return $this->newEntity($this->entityCode, ['data' => [
            'family_id' => $family->getId(),
            'geeftee_id' => $asGeefter->getGeeftee()->getId()
        ]])->save()
            ->getId();
    }

    /**
     * @param Geefter $asGeefter
     * @return AbstractModel
     */
    protected function getNewEntity($asGeefter) {
        $createEntityData = $this->getCreateEntityData($asGeefter);

        return $this->newEntity(
            $this->getEntityCode(),
            ['data' => [
                'family_id' => $createEntityData['relationships']['family']['data']['id'],
                'geeftee_id' => $createEntityData['relationships']['geeftee']['data']['id']
            ]]
        );
    }

    protected function getCreateEntityData($asGeefter): array {
        $owner = $this->getEntity(Geefter::ENTITY_TYPE, [
            'geefter_id' => ['ne' => $asGeefter->getId()]
        ]);
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $owner->getId()
        ]])->save();

        return [
            'type' => $this->getEntityCode(),
            'relationships' => [
                'family' => [
                    'data' => [
                        'type' => $family->getEntityType(),
                        'id' => $family->getId(),
                    ]
                ],
                'geeftee' => [
                    'data' => [
                        'type' => $asGeefter->getGeeftee()->getEntityType(),
                        'id' => $asGeefter->getGeeftee()->getId(),
                    ]
                ]
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'decision_code' => MembershipRequest::DECISION_CODE_CANCELED
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        $family = $this->getEntity(Family::ENTITY_TYPE, [
            'family_id' => ['nin' => $asGeefter->getFamilyCollection()->getAllIds()],
            'visibility' => ['ne' => Family\Field\Visibility::PRIVATE]
        ]);

        return $this->newEntity($this->entityCode, ['data' => [
            'family_id' => $family->getId(),
            'geeftee_id' => $asGeefter->getGeeftee()->getId()
        ]])->save()
            ->getId();
    }

    /**
     * Special case for deletion: not supported
     */
    public function testDeleteItem(): void {
        $john = $this->getJohn();
        $entityId = $this->getDeleteEntityId($john);

        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
            $this->fail('Failed preventing membership request deletion.');
        }
        catch (MethodNotAllowedException) {}

        $response = $this->getResponse();

        $this->assertEmpty($response->getData());

        $this->disableGeefterAccessListeners();

        $entity = $this->getEntity($this->getEntityCode(), $entityId);
        $this->assertNotNull($entity->getId());
    }

    protected function prepareCollection($asGeefter): static {
        // nothing to do here (membership requests should already exist in sample data)

        return $this;
    }
}
