<?php

namespace Geeftlist\Test\Controller\Api\Rest\Service\Token;


use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractServiceControllerTest;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;
use Geeftlist\Test\Util\ApiControllerTestCaseTrait;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use OliveOil\Core\Test\Util\FakerTrait;

/**
 * @group api
 * @group controllers
 */
class JwtTest extends AbstractServiceControllerTest
{
    use FakerTrait;
    use ApiControllerTestCaseTrait;
    use ValidationTrait;
    public function testGet(): void {
        try {
            $this->mockApiRequest('GET /api/token', [], [], json_encode([]));
            $this->fail('Failed preventing call of unsupported method GET.');
        }
        catch (\Throwable) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_emptyMethod(): void {
        try {
            $this->mockApiRequest('POST /api/token');
            $this->fail('Failed preventing call with empty authentication method.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([]));
            $this->fail('Failed preventing call with empty authentication method.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_invalidMethod(): void {
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'foo'
                ]
            ]));
            $this->fail('Failed preventing call with invalid authentication method.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => ['bar']
                ]
            ]));
            $this->fail('Failed preventing call with invalid authentication method.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_invalidPassword(): void {
        // Missing fields
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'password'
                ]
            ]));
            $this->fail('Failed preventing call with invalid password data.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        // Missing fields 2
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'password',
                    'email' => 'someone@example.org'
                ]
            ]));
            $this->fail('Failed preventing call with invalid password data.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        // Unknown user
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'password',
                    'email' => 'someone@example.org',
                    'password' => 'foo'
                ]
            ]));
            $this->fail('Failed preventing call with invalid password data.');
        }
        catch (UnauthorizedException $unauthorizedException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        // Valid user, invalid password
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'password',
                    'email' => Constants::GEEFTER_JANE_EMAIL,
                    'password' => 'baz'
                ]
            ]));
            $this->fail('Failed preventing call with invalid password data.');
        }
        catch (UnauthorizedException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_validPassword(): void {
        $this->assertEmpty($this->getResponse()->getData());
        $this->assertEmpty($this->getResponse()->getHeaders());

        $this->mockApiRequest('POST /api/token', [], [], json_encode([
            'data' => [
                'method' => 'password',
                'email' => Constants::GEEFTER_JANE_EMAIL,
                'password' => Constants::GEEFTER_JANE_PASSWORD
            ]
        ]));
        $response = $this->getResponse();

        $this->assertIsArray($response->getMeta());
        $this->assertArrayHasKey('api_key', $response->getMeta());
        $this->assertEquals($this->getJane()->getApiKey(), $response->getMeta()['api_key']);

        $this->assertIsArray($response->getData());
        $this->assertIsValidResourceObject(
            $response->getData(),
            Geefter::ENTITY_TYPE,
            $this->getJane()->getId()
        );

        $responseHeaders = $this->getResponse()->getHeaders();
        $this->assertNotEmpty($responseHeaders);
        $this->assertArrayHasKey('Authentication', $responseHeaders);
    }

    public function testPost_invalidApiKey(): void {
        // Missing fields
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'apikey'
                ]
            ]));
            $this->fail('Failed preventing call with invalid API key data.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        // Invalid API key
        try {
            $this->mockApiRequest('POST /api/token', [], [], json_encode([
                'data' => [
                    'method' => 'apikey',
                    'api_key' => 'foo'
                ]
            ]));
            $this->fail('Failed preventing call with invalid API key data.');
        }
        catch (UnauthorizedException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_validApiKey(): void {
        $john = $this->getJohn();

        $this->assertEmpty($this->getResponse()->getData());
        $this->assertEmpty($this->getResponse()->getHeaders());

        $this->mockApiRequest('POST /api/token', [], [], json_encode([
            'data' => [
                'method' => 'apikey',
                'api_key' => $john->getApiKey()
            ]
        ]));
        $response = $this->getResponse();

        $this->assertIsArray($response->getMeta());
        $this->assertArrayHasKey('api_key', $response->getMeta());
        $this->assertEquals($john->getApiKey(), $response->getMeta()['api_key']);

        $this->assertIsArray($response->getData());
        $this->assertIsValidResourceObject(
            $response->getData(),
            Geefter::ENTITY_TYPE,
            $john->getId()
        );

        $responseHeaders = $this->getResponse()->getHeaders();
        $this->assertNotEmpty($responseHeaders);
        $this->assertArrayHasKey('Authentication', $responseHeaders);
    }
}
