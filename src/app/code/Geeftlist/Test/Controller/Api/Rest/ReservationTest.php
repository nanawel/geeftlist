<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Constants;

/**
 * @group api
 * @group controllers
 */
class ReservationTest extends AbstractModelControllerTest
{
    protected $entityCode = Reservation::ENTITY_TYPE;

    protected function getGetEntityId($asGeefter) {
        $gift = $this->newGift($asGeefter);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $asGeefter->getId()
        ]])->save();

        return $reservation->getId();
    }

    protected function getCreateEntityData($asGeefter): array {
        $this->setGeefterInSession($asGeefter);
        $gift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'creator_id' => ['ne' => $asGeefter->getId()],
                'status'     => Gift\Field\Status::AVAILABLE,
                'geeftee'    => ['null' => false]
            ]
        );
        $this->assertNotNull($gift->getId());
        // Find an existing reservation to avoid having to find a valid geeftee
        $existingReservation = $this->getEntity(
            Reservation::ENTITY_TYPE,
            [
                'main_table.geefter_id' => $asGeefter->getId(),
                'main_table.gift_id'    => $gift->getId()
            ]
        );
        if ($existingReservation) {
            $existingReservation->delete();
        }

        $this->unsetGeefterInSession();

        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'geefter_id' => $asGeefter->getId(),
                'gift_id'    => $gift->getId(),
                'type'       => Reservation::TYPE_RESERVATION,
                'amount'     => 12.34
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'type'   => Reservation::TYPE_PURCHASE,
                'amount' => 23.45
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        return $this->getGetEntityId($asGeefter);
    }

    /**
     * @inheritDoc
     */
    protected function prepareCollection($asGeefter): static {
        // Generate 5 reservations
        for ($i = 0; $i < 5; ++$i) {
            $this->getGetEntityId($asGeefter);
        }

        return $this;
    }

    /**
     * @param Geefter $asGeefter
     * @return Gift
     */
    protected function newGift($asGeefter) {
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $asGeefter->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getRelatedGeeftee($asGeefter)->addGift($gift);

        return $gift;
    }

    /**
     * @param Geefter $asGeefter
     * @return Geeftee
     */
    protected function getRelatedGeeftee($asGeefter) {
        $geeftee = null;

        $families = $asGeefter->getFamilyCollection()->getItems();
        $this->assertNotEmpty($families, sprintf('Geefter %s has no family.', $asGeefter->getUsername()));

        while (! $geeftee && ($family = next($families))) {
            $geeftee = $family->getGeefteeCollection()
                ->addFieldToFilter('main_table.geeftee_id', ['ne' => $asGeefter->getGeeftee()->getId()])
                ->getFirstItem();

            if (! $geeftee || ! $geeftee->getId()) {
                $geeftee = null;
            }
        }

        $this->assertIsObject($geeftee, sprintf('Geefter %s has no related geeftee usable.', $asGeefter->getUsername()));

        return $geeftee;
    }
}
