<?php

namespace Geeftlist\Test\Controller\Api\Rest\Family\MembershipRequest;


use Geeftlist\Helper\Family\Membership;
use Geeftlist\Model\Family;
use Geeftlist\Model\Family\MembershipRequest;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractModelCustomActionsTest;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class CustomActionsTest extends AbstractModelCustomActionsTest
{
    protected $entityCode = MembershipRequest::ENTITY_TYPE;

    protected $allowedActions = ['accept', 'cancel', 'reject'];

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    public function testOptions(): void {
        $originHeader = [
            'Origin' => 'http://www.example.org'
        ];
        $this->mockApiCustomActionsRequest(
            '',
            Http::VERB_OPTIONS,
            '',
            ['id' => 1],    // ID does not matter here
            [],
            $originHeader
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $this->getResponse()->getCode());

        $this->assertNull($response->getData());

        $responseHeaders = array_merge(headers_list(), $this->getResponse()->getHeaders());
        $this->assertIsArray($responseHeaders);
        // Not supported, see https://php.net/manual/fr/function.headers-list.php#120539
        //$this->assertArrayHasKey('Access-Control-Allow-Origin', $responseHeaders);
        //$this->assertEquals('*', $responseHeaders['Access-Control-Allow-Origin']);
        $this->assertArrayHasKey('Access-Control-Allow-Methods', $responseHeaders);
        $this->assertEquals(
            [Http::VERB_POST, Http::VERB_OPTIONS],
            explode(',', $responseHeaders['Access-Control-Allow-Methods'])
        );
        $this->assertArrayHasKey('Allow-Actions', $responseHeaders);
        $this->assertEquals(
            $this->allowedActions,
            explode(',', $responseHeaders['Allow-Actions'])
        );
    }

    public function testAccept_invitation_ownerNotAllowed(): void {
        $this->testAction(
            'accept',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getJohn(),
            true,
            'Failed preventing accepting invitation by family owner.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testAccept_invitation_unrelatedNotAllowed(): void {
        $this->testAction(
            'accept',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getRichard(),
            true,
            'Failed preventing accepting invitation by unrelated geefter.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testAccept_invitation(): void {
        $this->testAction(
            'accept',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getJane(),
            false,
            '',
            MembershipRequest::DECISION_CODE_ACCEPTED,
            true
        );
    }

    public function testAccept_request_candidateNotAllowed(): void {
        $this->testAction(
            'accept',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getJane(),
            true,
            'Failed preventing accepting request by candidate.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testAccept_request_unrelatedNotAllowed(): void {
        $this->testAction(
            'accept',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getRichard(),
            true,
            'Failed preventing accepting request by candidate.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testAccept_request(): void {
        $this->testAction(
            'accept',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getJohn(),
            false,
            '',
            MembershipRequest::DECISION_CODE_ACCEPTED,
            true
        );
    }

    public function testCancel_invitation_candidateNotAllowed(): void {
        $richard = $this->getRichard();
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [$richard->getGeeftee()],
            $this->getJane()->getGeeftee(),
            $richard,
            $this->getJane(),
            true,
            'Failed preventing cancelling invitation by candidate.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testCancel_invitation_unrelatedNotAllowed(): void {
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getRichard(),
            true,
            'Failed preventing cancelling invitation by unrelated geefter.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testCancel_invitation_sponsor(): void {
        $richard = $this->getRichard();
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [$richard->getGeeftee()],
            $this->getJane()->getGeeftee(),
            $richard,
            $richard,
            false,
            '',
            MembershipRequest::DECISION_CODE_CANCELED,
            true
        );
    }

    public function testCancel_invitation_familyOwner(): void {
        $richard = $this->getRichard();
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [$richard->getGeeftee()],
            $this->getJane()->getGeeftee(),
            $richard,
            $this->getJohn(),
            false,
            '',
            MembershipRequest::DECISION_CODE_CANCELED,
            true
        );
    }

    public function testCancel_request_unrelatedNotAllowed(): void {
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getRichard(),
            true,
            'Failed preventing cancelling request by unrelated geefter.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testCancel_request_familyOwnerNotAllowed(): void {
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getJohn(),
            true,
            'Failed preventing cancelling request by family owner.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testCancel_request(): void {
        $this->testAction(
            'cancel',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getJane(),
            false,
            '',
            MembershipRequest::DECISION_CODE_CANCELED,
            true
        );
    }

    public function testReject_invitation_ownerNotAllowed(): void {
        $this->testAction(
            'reject',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getJohn(),
            true,
            'Failed preventing rejecting invitation by family owner.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testReject_invitation_unrelatedNotAllowed(): void {
        $this->testAction(
            'reject',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getRichard(),
            true,
            'Failed preventing rejecting invitation by unrelated geefter.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testReject_invitation(): void {
        $this->testAction(
            'reject',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJohn(),
            $this->getJane(),
            false,
            '',
            MembershipRequest::DECISION_CODE_REJECTED,
            true
        );
    }

    public function testReject_request_candidateNotAllowed(): void {
        $this->testAction(
            'reject',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getJane(),
            true,
            'Failed preventing rejecting request by candidate.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testReject_request_unrelatedNotAllowed(): void {
        $this->testAction(
            'reject',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getRichard(),
            true,
            'Failed preventing rejecting request by candidate.',
            MembershipRequest::DECISION_CODE_PENDING,
            false
        );
    }

    public function testReject_request(): void {
        $this->testAction(
            'reject',
            $this->getJohn(),
            Family\Field\Visibility::PROTECTED,
            [],
            $this->getJane()->getGeeftee(),
            $this->getJane(),
            $this->getJohn(),
            false,
            '',
            MembershipRequest::DECISION_CODE_REJECTED,
            true
        );
    }

    /**
     * @param Geeftee[] $members
     * @return Family
     */
    protected function newFamily(
        Geefter $owner,
        string $visibility = Family\Field\Visibility::PROTECTED,
        array $members = []
    ): Family {
        /** @var \Geeftlist\Model\Family $family */
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'visibility' => $visibility,
            'owner_id' => $owner->getId()
        ]])->save();
        if (!in_array($owner->getGeeftee(), $members)) {
            $members[] = $owner->getGeeftee();
        }

        $this->callPrivileged(static function () use ($family, $members): void {
            $family->addGeeftee($members);
        });

        return $family;
    }

    /**
     * @return MembershipRequest
     */
    protected function newMembershipRequest(
        Geefter $familyOwner,
        Geeftee $withGeeftee,
        string $familyVisibility = Family\Field\Visibility::PROTECTED,
        Geefter $sponsor = null,
        array $familyMembers = []
    ) {
        /** @var \Geeftlist\Model\Family $family */
        $family = $this->newFamily($familyOwner, $familyVisibility, $familyMembers);

        return $this->callPrivileged(fn() => $this->getFamilyMembershipHelper()->request($family, $withGeeftee, $sponsor));
    }

    protected function testAction(
        string $action,
        \Geeftlist\Model\Geefter $familyOwner,
        string $familyVisibility,
        array $familyMembers,
        \Geeftlist\Model\Geeftee $geeftee,
        ?\Geeftlist\Model\Geefter $sponsor,
        Geefter|int $asGeefter,
        bool $expectedForbidden,
        string $failureMessage,
        string $expectedDecisionCode,
        bool $expectedReturnObject
    ): void {
        $mr = $this->newMembershipRequest($familyOwner, $geeftee, $familyVisibility, $sponsor, $familyMembers);
        $this->assertEquals(MembershipRequest::DECISION_CODE_PENDING, $mr->getDecisionCode());

        try {
            $this->mockApiCustomActionsRequest(
                '',
                Http::VERB_POST,
                $action,
                ['id' => $mr->getId()],
                [],
                $this->getAuthenticationHeaders($asGeefter)
            );
            if ($expectedForbidden) {
                $this->fail($failureMessage);
            }
        }
        catch (ForbiddenException) {}

        if (!$expectedForbidden) {
            $this->assertEquals(HttpStatus::STATUS_OK, $this->getResponse()->getCode());
        }

        $response = $this->getResponse();

        if ($expectedReturnObject) {
            $this->assertIsValidResourceObject($response->getData());
        }
        else {
            $this->assertNull($response->getData());
        }

        $mr = $this->getEntity(MembershipRequest::ENTITY_TYPE, $mr->getId());
        $this->assertEquals($expectedDecisionCode, $mr->getDecisionCode());
    }

    /**
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getFamilyMembershipHelper(): Membership {
        return $this->getContainer()->get(Membership::class);
    }
}
