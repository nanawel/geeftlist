<?php

namespace Geeftlist\Test\Controller\Api\Rest\Service\Account;


use Geeftlist\Controller\Api\Rest\Service\Account\Register;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractServiceControllerTest;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;
use Geeftlist\Test\Util\ApiControllerTestCaseTrait;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use OliveOil\Core\Test\Util\FakerTrait;

/**
 * @group api
 * @group controllers
 */
class RegisterTest extends AbstractServiceControllerTest
{
    use FakerTrait;
    use ApiControllerTestCaseTrait;
    use ValidationTrait;
    public function testPost_emptyArguments(): void {
        try {
            $this->mockApiRequest('POST /api/account/register');
            $this->fail('Failed preventing call with empty arguments.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([]));
            $this->fail('Failed preventing call with empty arguments.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_invalidUsername(): void {
        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => ' ',
                    'email' => uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
                ]
            ]));
            $this->fail('Failed preventing call with invalid username.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => [uniqid('geefter-api')],
                    'email' => uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
                ]
            ]));
            $this->fail('Failed preventing call with invalid username.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => Constants::GEEFTER_JANE_USERNAME,
                    'email' => uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
                ]
            ]));
            $this->fail('Failed preventing call with invalid username.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_invalidEmail(): void {
        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => uniqid('geefter-api'),
                    'email' => ' '
                ]
            ]));
            $this->fail('Failed preventing call with invalid email.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => uniqid('geefter-api'),
                    'email' => [' ']
                ]
            ]));
            $this->fail('Failed preventing call with invalid email.');
        }
        catch (BadRequestException $badRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }

        try {
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => uniqid('geefter-api'),
                    'email' => Constants::GEEFTER_JANE_EMAIL
                ]
            ]));
            $this->fail('Failed preventing call with invalid email.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_serviceUnavailable(): void {
        $flagValue = $this->getAppConfig()->getValue('GEEFTER_REGISTRATION_ENABLE');
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', 0);

        try {
            $username = uniqid('geefter-api');
            $email = uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
            $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
                'data' => [
                    'username' => $username,
                    'email' => $email
                ]
            ]));
            $this->fail('Failed preventing POST when service is disabled.');
        }
        catch (ServiceUnavailableException) {
            $this->assertTrue(true); // Just to have an assertion
        }
        finally {
            $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', $flagValue);
        }
    }

    public function testPost_valid(): void {
        $this->assertEmpty($this->getResponse()->getData());
        $this->assertEmpty($this->getResponse()->getHeaders());

        $username = uniqid('geefter-api');
        $email = uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->mockApiRequest('POST /api/account/register', [], [], json_encode([
            'data' => [
                'username' => $username,
                'email' => $email
            ]
        ]));
        $response = $this->getResponse();

        $this->assertIsValidResourceObject(
            $response->getData(),
            Geefter::ENTITY_TYPE
        );

        $this->assertIsArray($response->getMeta());
        $this->assertArrayHasKey('messages', $response->getMeta());
        $this->assertNotEmpty($response->getMeta()['messages']);
        $this->assertArrayHasKey('registration-status', $response->getMeta());
        $this->assertEquals(
            $response->getMeta()['registration-status'],
            Register::META_REGISTRATION_STATUS_CONFIRMATION_REQUIRED
        );

        $createdGeefter = $this->getEntity(Geefter::ENTITY_TYPE, ['email' => $email]);
        $this->assertEquals($username, $createdGeefter->getUsername());
    }
}
