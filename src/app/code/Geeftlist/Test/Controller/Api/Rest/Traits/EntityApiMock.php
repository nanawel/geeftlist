<?php

namespace Geeftlist\Test\Controller\Api\Rest\Traits;


trait EntityApiMock
{
    /**
     * @param string $verb
     * @param array $args
     * @param array|null $headers
     * @param string|null $body
     * @return mixed
     */
    protected function mockApiEntityRequest(
        $verb,
        string $pathSuffix = '',
        array $params = [],
        $args = [],
        $headers = null,
        $body = null
    ) {
        try {
            $apiEntityCode = str_replace('/', '_', $this->getEntityCode());
            $pattern = strtoupper($verb) . (' /api/' . $apiEntityCode);
            if (isset($params['id'])) {
                $pattern .= '/' . $params['id'];
            }

            $pattern .= $pathSuffix;

            if (!is_string($body)
                && (! isset($headers['Content-Type']) || str_contains((string) $headers['Content-Type'], 'json'))
            ) {
                $body = json_encode($body);
            }

            return $this->getFw()->mock($pattern, $args, $headers, $body);
        } finally {
            // Clear internal caches before returning (models for Repositories among others)
            $this->clearCacheArrayObjects();
        }
    }
}
