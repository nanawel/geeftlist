<?php

namespace Geeftlist\Test\Controller\Api\Rest\Family;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Controller\Api\Rest\AbstractModelRelationshipsTest;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class RelationshipsTest extends AbstractModelRelationshipsTest
{
    protected $entityCode = Family::ENTITY_TYPE;

    protected $expectedRelationships = [
        'geeftees' => [
            Http::VERB_OPTIONS, Http::VERB_GET, Http::VERB_PATCH, HTTP::VERB_POST, Http::VERB_DELETE
        ],
        'owner' => [
            Http::VERB_OPTIONS, Http::VERB_GET
        ],
    ];

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    public function testGetGeeftees_notConnected(): void {
        $john = $this->getJohn();

        $entity = $this->getGetGeefteesEntity($john);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_GET,
                '',
                ['id' => $entityId]
            );
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {
            // OK
        }
    }

    public function testGetGeeftees_notMember(): void {
        // Not member of the family
        $richard = $this->getRichard();

        $entity = $this->getGetGeefteesEntity($this->getJane(), [$this->getJohn()->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);
        $this->assertNotEmpty($entity->getGeefteeIds());

        $this->mockApiRelationshipsRequest(
            'geeftees',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($richard)
        );
        $response = $this->getResponse();

        // Not supported yet, see #228
        //$this->assertEquals(HttpStatus::STATUS_OK, http_response_code());
        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Geeftee::ENTITY_TYPE
        );
    }

    public function testGetGeeftees(): void {
        $john = $this->getJohn();

        $entity = $this->getGetGeefteesEntity($john, [$this->getJane()->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);
        $referenceGeefteeIds = $entity->getGeefteeIds();
        sort($referenceGeefteeIds);

        $this->mockApiRelationshipsRequest(
            'geeftees',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john)
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Geeftee::ENTITY_TYPE,
            $referenceGeefteeIds
        );
    }

    public function testPatchGeeftees_asOwner(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        // Create another family so that John and Richard be already related
        // otherwise "replace" won't work as it removes related geeftees first and remove all is forbidden
        $this->getPatchGeefteesEntity($john, [$richard->getGeeftee()]);

        $entity = $this->getPatchGeefteesEntity($john, [$jane->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $originalMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(2, count($originalMemberIds));
        $this->assertNotContainsEquals($richard->getGeeftee()->getId(), $originalMemberIds);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_PATCH,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($john),
                [
                    'data' => [[
                        'type' => $richard->getGeeftee()->getEntityType(),
                        'id' => $richard->getGeeftee()->getId()
                    ]]
                ]
            );
            $this->fail('Failed preventing patching family geeftees as owner.');
        }
        catch (BadRequestException) {}

        $this->assertEquals($originalMemberIds, $entity->getGeefteeIds());
    }

    public function testPatchGeeftees_asMember(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        $entity = $this->getPatchGeefteesEntity($john, [$jane->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $originalMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(2, count($originalMemberIds));
        $this->assertNotContainsEquals($richard->getGeeftee()->getId(), $originalMemberIds);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_PATCH,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($jane),
                [
                    'data' => [[
                        'type' => $richard->getGeeftee()->getEntityType(),
                        'id' => $richard->getGeeftee()->getId()
                    ]]
                ]
            );
            $this->fail('Failed preventing patching family geeftees as member.');
        }
        catch (ForbiddenException) {}

        $this->assertEquals($originalMemberIds, $entity->getGeefteeIds());
    }

    public function testPostGeeftees_asOwner(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        // All 3 geefters must already be related so that John (here) can retrieve Richard on POST
        $this->generateFamily($john, [$jane->getGeeftee(), $richard->getGeeftee()]);

        $entity = $this->getPatchGeefteesEntity($john, [$jane->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $originalMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(2, count($originalMemberIds));
        $this->assertNotContainsEquals($richard->getGeeftee()->getId(), $originalMemberIds);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_POST,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($john),
                [
                    'data' => [[
                        'type' => $richard->getGeeftee()->getEntityType(),
                        'id' => $richard->getGeeftee()->getId()
                    ]]
                ]
            );
            $this->fail('Failed preventing posting family geeftees as owner.');
        }
        catch (ForbiddenException) {}

        $this->assertEquals($originalMemberIds, $entity->getGeefteeIds());
    }

    public function testPostGeeftees_asMember(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        // All 3 geefters must already be related so that Jane (here) can retrieve Richard on POST
        $this->generateFamily($john, [$jane->getGeeftee(), $richard->getGeeftee()]);

        $entity = $this->getPatchGeefteesEntity($john, [$jane->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $originalMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(2, count($originalMemberIds));
        $this->assertNotContainsEquals($richard->getGeeftee()->getId(), $originalMemberIds);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_POST,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($jane),
                [
                    'data' => [[
                        'type' => $richard->getGeeftee()->getEntityType(),
                        'id' => $richard->getGeeftee()->getId()
                    ]]
                ]
            );
            $this->fail('Failed preventing posting family geeftees as member.');
        }
        catch (ForbiddenException) {}

        $this->assertEquals($originalMemberIds, $entity->getGeefteeIds());
    }

    public function testDeleteGeeftees_asMember(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        $entity = $this->getDeleteGeefteesEntity($john, [$jane->getGeeftee(), $richard->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $originalMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(3, count($originalMemberIds));
        $this->assertContainsEquals($richard->getGeeftee()->getId(), $originalMemberIds);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_DELETE,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($jane),
                [
                    'data' => [[
                        'type' => $richard->getGeeftee()->getEntityType(),
                        'id' => $richard->getGeeftee()->getId()
                    ]]
                ]
            );
            $this->fail('Failed preventing deleting family geeftees as member.');
        }
        catch (ForbiddenException) {}

        $actualMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(3, count($actualMemberIds));
        $this->assertContainsEquals($richard->getGeeftee()->getId(), $actualMemberIds);
    }

    public function testDeleteGeeftees_asOwner(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        $entity = $this->getDeleteGeefteesEntity($john, [$jane->getGeeftee(), $richard->getGeeftee()]);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $originalMemberIds = $entity->getGeefteeIds();
        $this->assertEquals(3, count($originalMemberIds));
        $this->assertContainsEquals($richard->getGeeftee()->getId(), $originalMemberIds);

        $this->mockApiRelationshipsRequest(
            'geeftees',
            Http::VERB_DELETE,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => [[
                    'type' => $richard->getGeeftee()->getEntityType(),
                    'id' => $richard->getGeeftee()->getId()
                ]]
            ]
        );

        $expectedMemberIds = array_values(array_diff($originalMemberIds, [$richard->getGeeftee()->getId()]));
        $actualMemberIds = $entity->getGeefteeIds();
        $this->assertEqualsCanonicalizing($expectedMemberIds, $actualMemberIds);
    }

    /**
     * @param Geeftee[] $withGeeftees
     * @return Family
     */
    protected function getGetGeefteesEntity(\Geeftlist\Model\Geefter $asGeefter, array $withGeeftees = []) {
        return $this->generateFamily($asGeefter, $withGeeftees);
    }

    /**
     * @param Geeftee[] $withGeeftees
     * @return Family
     */
    protected function getPatchGeefteesEntity(\Geeftlist\Model\Geefter $asGeefter, array $withGeeftees = []) {
        return $this->generateFamily($asGeefter, $withGeeftees);
    }

    /**
     * @param Geeftee[] $withGeeftees
     * @return Family
     */
    protected function getDeleteGeefteesEntity(\Geeftlist\Model\Geefter $asGeefter, array $withGeeftees = []) {
        return $this->generateFamily($asGeefter, $withGeeftees);
    }
}
