<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class GeefteeTest extends AbstractModelControllerTest
{
    protected $entityCode = Geeftee::ENTITY_TYPE;

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    protected function getGetEntityId($asGeefter) {
        return $this->getEntity($this->entityCode, [
            'geefter_id' => $asGeefter->getId()
        ])->getId();
    }

    protected function getCreateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'name'       => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
                'creator_id' => $asGeefter->getId()
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        return $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $asGeefter->getId()
        ]])->save();
    }

    protected function prepareCollection($asGeefter): static {
        // nothing to do here (geeftees should already exist in sample data)

        return $this;
    }

    /**
     * Special case for deletion: not supported yet
     */
    public function testDeleteItem(): void {
        $john = $this->getJohn();
        $entityId = $this->getDeleteEntityId($john);

        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
            $this->fail('Failed preventing geeftee deletion.');
        }
        catch (MethodNotAllowedException) {}

        $response = $this->getResponse();

        $this->assertNull($response->getData());

        $this->disableGeefterAccessListeners();

        $entity = $this->getEntity($this->getEntityCode(), $entityId);
        $this->assertNotNull($entity->getId());
    }
}
