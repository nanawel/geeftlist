<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\GiftList;
use Geeftlist\Test\Constants;

/**
 * @group api
 * @group controllers
 */
class GiftListTest extends AbstractModelControllerTest
{
    protected $entityCode = GiftList::ENTITY_TYPE;

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    protected function getGetEntityId($asGeefter) {
        return $this->getEntity($this->entityCode, [
            'owner_id' => $asGeefter->getId()
        ])->getId();
    }

    protected function getCreateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'name'     => uniqid(Constants::SAMPLEDATA_GIFTLIST_LABEL_PREFIX),
                'owner_id' => $asGeefter->getId()
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'name' => uniqid(Constants::SAMPLEDATA_GIFTLIST_LABEL_PREFIX)
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        return $this->getNewEntity($asGeefter)
            ->save()
            ->getId();
    }

    protected function prepareCollection($asGeefter): static {
        // nothing to do here (gift lists should already exist in sample data)

        return $this;
    }
}
