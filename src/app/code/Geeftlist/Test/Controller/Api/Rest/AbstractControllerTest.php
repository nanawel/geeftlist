<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\ApiControllerTestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group api
 * @group controllers
 */
abstract class AbstractControllerTest extends TestCase
{
    use FakerTrait;
    use ApiControllerTestCaseTrait;
    use TestDataTrait;
    /** @var string */
    protected $entityCode;

    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();

        $this->getFw()->set('SERVER.CONTENT_TYPE', 'application/vnd.api+json');
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @return string
     */
    public function getEntityCode() {
        return $this->entityCode;
    }

    /**
     * @param int|string|Geefter $geefter
     */
    public function getAuthenticationHeaders($geefter): array {
        if (is_object($geefter)) {
            $model = $geefter;
        }
        else {
            $model = $this->getGeeftlistModelFactory()->resolveMake(Geefter::ENTITY_TYPE);
            if (is_numeric($geefter)) {
                $model->load($geefter);
            }
            else {
                $model->loadByEmail($geefter);
            }
        }

        if (! $model->getId()) {
            throw new \InvalidArgumentException();
        }

        $password = array_key_exists($model->getEmail(), Constants::GEEFTER_PASSWORDS)
            ? Constants::GEEFTER_PASSWORDS[$model->getEmail()]
            : Constants::GEEFTER_OTHERS_PASSWORD;

        return [
            'Authorization' => 'Basic ' . base64_encode(implode(':', [$model->getEmail(), $password]))
        ];
    }

    public function assertEmptyResponseData(array $responseData): void {
        $this->assertIsArray($responseData);
        $this->assertArrayHasKey('data', $responseData);
        $this->assertEmpty($responseData['data']);
    }

    protected function mockApiRequest($pattern, array $args = [], array $headers = null, $body = null) {
        if (!isset($headers['Content-Type'])) {
            $headers['Content-Type'] = 'application/vnd.api+json';
        }

        return $this->getFw()->mock($pattern, $args, $headers, $body);
    }
}
