<?php

namespace Geeftlist\Test\Controller\Api\Rest\Service\Account;


use Geeftlist\Controller\Api\Rest\Service\Account\CheckAvailability;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractServiceControllerTest;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;
use Geeftlist\Test\Util\ApiControllerTestCaseTrait;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use OliveOil\Core\Test\Util\FakerTrait;

/**
 * @group api
 * @group controllers
 */
class CheckAvailabilityTest extends AbstractServiceControllerTest
{
    use FakerTrait;
    use ApiControllerTestCaseTrait;
    use ValidationTrait;
    public function testPost_emptyArguments(): void {
        try {
            $this->mockApiRequest('POST /api/account/register');
            $this->fail('Failed preventing call with empty arguments.');
        }
        catch (BadRequestException) {
            $this->assertEmpty($this->getResponse()->getData());
        }
    }

    public function testPost_emptyArguments2(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    public function testPost_invalidUsername(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => ' ',
                'email' => uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
            ]
        ]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    public function testPost_invalidUsername2(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => [uniqid('geefter-api')],
                'email' => uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
            ]
        ]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    public function testPost_invalidUsername3(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => Constants::GEEFTER_JANE_USERNAME,
                'email' => uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
            ]
        ]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    public function testPost_invalidEmail(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => uniqid('geefter-api'),
                'email' => ' '
            ]
        ]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    public function testPost_invalidEmail2(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => uniqid('geefter-api'),
                'email' => [' ']
            ]
        ]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    public function testPost_invalidEmail3(): void {
        $responseErrors = $this->getResponse()->getErrors();
        $this->assertEmpty($responseErrors);

        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => uniqid('geefter-api'),
                'email' => Constants::GEEFTER_JANE_EMAIL
            ]
        ]));

        $responseErrors = $this->getResponse()->getErrors();
        $this->assertNotEmpty($responseErrors);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testPost_serviceUnavailable(): void {
        $flagValue = $this->getAppConfig()->getValue('GEEFTER_REGISTRATION_ENABLE');
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', 0);

        try {
            $username = uniqid('geefter-api');
            $email = uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
            $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
                'data' => [
                    'username' => $username,
                    'email' => $email
                ]
            ]));
            $this->fail('Failed preventing POST when service is disabled.');
        }
        catch (ServiceUnavailableException) {}
        finally {
            $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', $flagValue);
        }
    }

    public function testPost_valid(): void {
        $this->assertEmpty($this->getResponse()->getData());
        $this->assertEmpty($this->getResponse()->getHeaders());

        $username = uniqid('geefter-api');
        $email = uniqid('geefter-api') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->mockApiRequest('POST /api/account/checkAvailability', [], [], json_encode([
            'data' => [
                'username' => $username,
                'email' => $email
            ]
        ]));
        $response = $this->getResponse();
        $responseData = $response->getData();
        $this->assertNull($responseData);

        $this->assertIsArray($response->getMeta());
        $this->assertArrayHasKey('messages', $response->getMeta());
        $this->assertNotEmpty($response->getMeta()['messages']);
        $this->assertArrayHasKey('registration-status', $response->getMeta());
        $this->assertEquals(
            $response->getMeta()['registration-status'],
            CheckAvailability::META_REGISTRATION_STATUS_OK_AVAILABLE
        );

        $createdGeefter = $this->getEntity(Geefter::ENTITY_TYPE, ['email' => $email]);
        $this->assertNull($createdGeefter);
    }
}
