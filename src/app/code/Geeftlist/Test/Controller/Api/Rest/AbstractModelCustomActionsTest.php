<?php

namespace Geeftlist\Test\Controller\Api\Rest;

use Geeftlist\Test\Controller\Api\Rest\Traits\EntityApiMock;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;


/**
 * @group api
 * @group controllers
 */
abstract class AbstractModelCustomActionsTest extends AbstractControllerTest
{
    use ValidationTrait;
    use EntityApiMock;
    protected function mockApiCustomActionsRequest(
        $action,
        $verb,
        $pathSuffix = '',
        $params = [],
        $args = [],
        $headers = null,
        $body = null
    ) {
        return $this->mockApiEntityRequest(
            $verb,
            sprintf('/action/%s%s', $action, $pathSuffix),
            $params,
            $args,
            $headers,
            $body
        );
    }
}
