<?php

namespace Geeftlist\Test\Controller\Api\Rest\Service\System;


use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractServiceControllerTest;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;

/**
 * @group api
 * @group controllers
 */
class PingTest extends AbstractServiceControllerTest
{
    public function testPing(): void {
        $fw = $this->getFw();

        $this->mock('GET /api/system/ping', null, ['Content-Type' => 'application/json']);
        $response = $fw->get('RESPONSE');
        $this->assertStringContainsString('pong', $response);
    }

    public function testPing_authBasic(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();
        $this->mock('GET /api/system/ping', null, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode(
                    implode(':', [$john->getEmail(), Constants::GEEFTER_JOHN_PASSWORD])
                )
        ]);
        $response = $fw->get('RESPONSE');
        $this->assertStringContainsString(sprintf('pong %s!', $john->getUsername()), $response);
    }

    public function testPing_authApiKey(): void {
        $fw = $this->getFw();

        try {
            $this->mock('GET /api/system/ping', null, [
                'Content-Type' => 'application/json',
                'Authorization' => 'Apikey invalid-api-key'
            ]);
            $this->fail('Access allowed to invalid API key');
        }
        catch (UnauthorizedException) {}

        $john = $this->getJohn();
        $this->mock('GET /api/system/ping', null, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Apikey ' . $john->getApiKey()
        ]);
        $response = $fw->get('RESPONSE');
        $this->assertStringContainsString(sprintf('pong %s!', $john->getUsername()), $response);
    }
}
