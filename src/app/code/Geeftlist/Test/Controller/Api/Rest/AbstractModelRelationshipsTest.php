<?php

namespace Geeftlist\Test\Controller\Api\Rest;

use Geeftlist\Test\Controller\Api\Rest\Traits\EntityApiMock;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;


/**
 * @group api
 * @group controllers
 */
abstract class AbstractModelRelationshipsTest extends AbstractControllerTest
{
    use ValidationTrait;
    use EntityApiMock;
    /** @var string[][] */
    protected $expectedRelationships;

    protected function mockApiRelationshipsRequest(
        ?string $relationshipType,
        $verb,
        $pathSuffix = '',
        $params = [],
        $args = [],
        $headers = null,
        $body = null
    ) {
        return $this->mockApiEntityRequest(
            $verb,
            sprintf(
                '/relationships%s%s',
                $relationshipType ? '/' . $relationshipType : '',
                $pathSuffix ?: ''
            ),
            $params,
            $args,
            $headers,
            $body
        );
    }

    public function testOptionsCreator_corsDisabled(): void {
        foreach (array_keys($this->expectedRelationships) as $expectedRelationship) {
            $this->mockApiRelationshipsRequest(
                $expectedRelationship,
                Http::VERB_OPTIONS,
                '',
                ['id' => 1],    // ID does not matter here
            );
            $response = $this->getResponse();

            $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

            $this->assertNull($response->getData());

            $responseHeaders = array_merge(headers_list(), $response->getHeaders());
            $this->assertIsArray($responseHeaders);
            // Not supported, see https://php.net/manual/fr/function.headers-list.php#120539
            //$this->assertArrayNotHasKey('Access-Control-Allow-Origin', $responseHeaders);
            $this->assertArrayNotHasKey('Access-Control-Allow-Methods', $responseHeaders);
        }
    }

    public function testOptions_corsEnabled(): void {
        foreach ($this->expectedRelationships as $expectedRelationship => $allowedMethods) {
            $originHeader = [
                'Origin' => 'http://www.example.org'
            ];
            $this->mockApiRelationshipsRequest(
                $expectedRelationship,
                Http::VERB_OPTIONS,
                '',
                ['id' => 1],    // ID does not matter here
                [],
                $originHeader
            );
            $response = $this->getResponse();

            $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

            $this->assertNull($response->getData());

            $responseHeaders = array_merge(headers_list(), $response->getHeaders());
            $this->assertIsArray($responseHeaders);
            // Not supported, see https://php.net/manual/fr/function.headers-list.php#120539
            //$this->assertArrayHasKey('Access-Control-Allow-Origin', $responseHeaders);
            //$this->assertEquals('*', $responseHeaders['Access-Control-Allow-Origin']);
            $this->assertArrayHasKey('Access-Control-Allow-Methods', $responseHeaders);
            $this->assertEqualsCanonicalizing(
                $allowedMethods,
                explode(',', $responseHeaders['Access-Control-Allow-Methods']),
                'Relationship: ' . $expectedRelationship
            );
        }
    }

    protected function validateRelationships(array $expectedRelationships, array $relationshipsData) {
        $this->assertArrayHasKey('relationships', $relationshipsData);
        $this->assertIsArray($relationshipsData['relationships']);
        $this->assertEqualsCanonicalizing($expectedRelationships, $relationshipsData['relationships']);
    }
}
