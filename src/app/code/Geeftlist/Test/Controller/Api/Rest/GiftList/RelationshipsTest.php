<?php

namespace Geeftlist\Test\Controller\Api\Rest\GiftList;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractModelRelationshipsTest;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use Narrowspark\HttpStatus\Exception\UnprocessableEntityException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Validation\ErrorInterface;

/**
 * @group api
 * @group controllers
 */
class RelationshipsTest extends AbstractModelRelationshipsTest
{
    protected $entityCode = GiftList::ENTITY_TYPE;

    protected $expectedRelationships = [
        'owner' => [
            Http::VERB_OPTIONS, Http::VERB_GET, Http::VERB_PATCH
        ],
        'gifts' => [
            Http::VERB_OPTIONS, Http::VERB_GET, Http::VERB_PATCH, HTTP::VERB_POST, Http::VERB_DELETE
        ],
    ];

    public function testGetOwner_notAllowed(): void {
        $john = $this->getJohn();

        $entity = $this->getGetOwnerEntity($john);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'owner',
                Http::VERB_GET,
                '',
                ['id' => $entityId]
            );
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {
        }
    }

    public function testGetOwner_asSelf(): void {
        $john = $this->getJohn();

        $entity = $this->getGetOwnerEntity($john);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->mockApiRelationshipsRequest(
            'owner',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john)
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject(
            $response->getData(),
            Geefter::ENTITY_TYPE,
            $entity->getCreatorId()
        );
    }

    public function testGetOwner_asOther_forbidden(): void {
        $john = $this->getJohn();

        $entity = $this->getGetOwnerEntity($john);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'owner',
                Http::VERB_GET,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($this->getJane())
            );
            $this->fail("Access allowed to another geefter's list owner");
        }
        catch (ForbiddenException) {
        }
    }

    public function testGetGifts(): void {
        $john = $this->getJohn();

        $entity = $this->getGetGiftsEntity($john);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->mockApiRelationshipsRequest(
            'gifts',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john)
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Gift::ENTITY_TYPE,
            $entity->getGeefteeIds(),
        );
    }

    public function testPatchOwner_neverAllowed(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchOwnerEntity($john);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'owner',
                Http::VERB_PATCH,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($john),
                [
                    'data' => [
                        'type' => Geefter::ENTITY_TYPE,
                        'id' => $this->getJane()->getId()
                    ]
                ]
            );
            $this->fail("Failed preventing update of giftlist's owner.");
        }
        catch (UnprocessableEntityException) {}

        $responseErrors = $this->getResponse()->getErrors();

        $this->assertIsArray($responseErrors);
        $this->assertEquals(ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE, $responseErrors[0]['code']);

        $this->assertEquals(HttpStatus::STATUS_UNPROCESSABLE_ENTITY, $this->getResponse()->getCode());

        $this->assertEquals(
            $entity->getOwnerId(),
            $this->getEntity($this->getEntityCode(), $entityId)->getOwnerId()
        );
    }

    public function testPatchGifts_empty(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchGiftsEntity($john, 2);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertNotNull($entity);
        $this->assertCount(2, $entity->getGiftIds());

        $this->mockApiRelationshipsRequest(
            'gifts',
            Http::VERB_PATCH,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => []
            ]
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Gift::ENTITY_TYPE,
            []
        );

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertEmpty($entity->getGiftIds());
    }

    public function testPatchGifts_notOwnList_forbidden(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $entity = $this->getPatchGiftsEntity($john, 2);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();

        $newGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId(),
        ]])->save();
        $this->assertNotNull($entityId);

        $origGiftIds = $entity->getGiftIds();
        $this->assertCount(2, $origGiftIds);
        $this->assertNotContainsEquals($newGift->getId(), $origGiftIds);

        try {
            $this->mockApiRelationshipsRequest(
                'gifts',
                Http::VERB_PATCH,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($jane),
                [
                    'data' => [[
                        'type' => Gift::ENTITY_TYPE,
                        'id' => $newGift->getId()
                    ]]
                ]
            );
            $this->fail("Access allowed to another geefter's list gifts");
        }
        catch (ForbiddenException) {
        }

        $this->unsetGeefterInSession(); // Prevent permission exception in the next steps

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertCount(2, $entity->getGiftIds());
        $this->assertEqualsCanonicalizing($origGiftIds, $entity->getGiftIds());
    }

    public function testPatchGifts(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchGiftsEntity($john, 2);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();

        $newGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
        ]])->save();
        $this->assertNotNull($entityId);

        $origGiftIds = $entity->getGiftIds();
        $this->assertCount(2, $origGiftIds);
        $this->assertNotContainsEquals($newGift->getId(), $origGiftIds);

        $this->mockApiRelationshipsRequest(
            'gifts',
            Http::VERB_PATCH,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => [[
                    'type' => Gift::ENTITY_TYPE,
                    'id' => $newGift->getId()
                ]]
            ]
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Gift::ENTITY_TYPE,
            [$newGift->getId()]
        );

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertCount(1, $entity->getGiftIds());
        $this->assertEqualsCanonicalizing([$newGift->getId()], $entity->getGiftIds());
    }

    public function testPostGifts(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchGiftsEntity($john, 2);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();

        $newGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
        ]])->save();

        $this->assertNotNull($entityId);

        $origGiftIds = $entity->getGiftIds();
        $this->assertCount(2, $origGiftIds);
        $this->assertNotContainsEquals($newGift->getId(), $origGiftIds);

        $this->mockApiRelationshipsRequest(
            'gifts',
            Http::VERB_POST,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => [[
                    'type' => Gift::ENTITY_TYPE,
                    'id' => $newGift->getId()
                ]]
            ]
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $expectedGiftIds = array_merge($origGiftIds, [$newGift->getId()]);
        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Gift::ENTITY_TYPE,
            $expectedGiftIds
        );

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertCount(3, $entity->getGiftIds());
        $this->assertEqualsCanonicalizing(
            $expectedGiftIds,
            $entity->getGiftIds()
        );
    }

    public function testDeleteGifts(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchGiftsEntity($john, 2);
        $this->assertNotNull($entity);
        $entityId = $entity->getId();

        $origGiftIds = $entity->getGiftIds();
        $this->assertCount(2, $origGiftIds);
        $giftIdToBeDeleted = current($origGiftIds);
        $this->assertContainsEquals($giftIdToBeDeleted, $origGiftIds);

        $this->mockApiRelationshipsRequest(
            'gifts',
            Http::VERB_DELETE,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => [[
                    'type' => Gift::ENTITY_TYPE,
                    'id' => $giftIdToBeDeleted
                ]]
            ]
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $expectedGiftIds = array_diff($origGiftIds, [$giftIdToBeDeleted]);
        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Gift::ENTITY_TYPE,
            $expectedGiftIds
        );

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertCount(1, $entity->getGiftIds());
        $this->assertEqualsCanonicalizing(
            $expectedGiftIds,
            $entity->getGiftIds()
        );
    }

    /**
     * @param Geefter $asGeefter
     * @return GiftList
     */
    protected function getGetOwnerEntity($asGeefter) {
        return $this->generateGiftList($asGeefter, 0);
    }

    /**
     * @param Geefter $asGeefter
     * @return GiftList
     */
    protected function getGetGiftsEntity($asGeefter) {
        return $this->generateGiftList($asGeefter, 3);
    }

    /**
     * @param Geefter $asGeefter
     * @return GiftList
     */
    protected function getPatchOwnerEntity($asGeefter) {
        return $this->generateGiftList($asGeefter, 0);
    }

    /**
     * @param Geefter $asGeefter
     * @param int $withGifts
     * @return GiftList
     */
    protected function getPatchGiftsEntity($asGeefter, $withGifts) {
        return $this->generateGiftList($asGeefter, $withGifts);
    }
}
