<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Controller\Api\Rest\Traits\EntityApiMock;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use Narrowspark\HttpStatus\Exception\NotFoundException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
abstract class AbstractModelControllerTest extends AbstractControllerTest
{
    use ValidationTrait;
    use EntityApiMock;
    /** @var string[] */
    protected $expectedAllowedMethods = [
        Http::VERB_GET,
        Http::VERB_POST,
        //Http::VERB_PUT,
        Http::VERB_PATCH,
        Http::VERB_DELETE,
        Http::VERB_OPTIONS
    ];

    public function testOptions_unsupported(): void {
        if (! in_array($method = Http::VERB_OPTIONS, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($this->getJohn()));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }
        }

        $this->expectNotToPerformAssertions();
    }

    public function testOptions_unauthenticated(): void {
        if (!in_array($method = Http::VERB_OPTIONS, $this->expectedAllowedMethods)) {
            // See testOptions_unsupported() above
            return;
        }

        // OPTIONS request are allowed unauthenticated (mainly for CORS preflight requests)
        $this->mockApiEntityRequest(Http::VERB_OPTIONS);
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertNull($response->getData());

        $responseHeaders = $response->getHeaders();
        $this->assertIsArray($responseHeaders);
        $this->assertArrayHasKey('Allow', $responseHeaders);
        $this->assertEqualsCanonicalizing(
            $this->expectedAllowedMethods,
            explode(',', $responseHeaders['Allow'])
        );
    }

    public function testOptions_corsDisabled(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_OPTIONS, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }

            return;
        }

        $this->mockApiEntityRequest(Http::VERB_OPTIONS, '', [], [], $this->getAuthenticationHeaders($john));
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertNull($response->getData());

        $responseHeaders = array_merge(headers_list(), $response->getHeaders());
        $this->assertIsArray($responseHeaders);
        // Not supported, see https://php.net/manual/fr/function.headers-list.php#120539
        //$this->assertArrayNotHasKey('Access-Control-Allow-Origin', $responseHeaders);
        $this->assertArrayNotHasKey('Access-Control-Allow-Methods', $responseHeaders);
    }

    public function testOptions_corsEnabled(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_OPTIONS, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }

            return;
        }

        $originHeader = [
            'Origin' => 'http://www.example.org'
        ];
        $this->mockApiEntityRequest(Http::VERB_OPTIONS, '', [], [], $originHeader + $this->getAuthenticationHeaders($john));
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertNull($response->getData());

        $responseHeaders = array_merge(headers_list(), $response->getHeaders());
        $this->assertIsArray($responseHeaders);
        // Not supported, see https://php.net/manual/fr/function.headers-list.php#120539
        //$this->assertArrayHasKey('Access-Control-Allow-Origin', $responseHeaders);
        //$this->assertEquals('*', $responseHeaders['Access-Control-Allow-Origin']);
        $this->assertArrayHasKey('Access-Control-Allow-Methods', $responseHeaders);
        $this->assertEqualsCanonicalizing(
            $this->expectedAllowedMethods,
            explode(',', $responseHeaders['Access-Control-Allow-Methods'])
        );
    }

    public function testGetItem(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_GET, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }
            catch (\Throwable) {
                // OK too (might be missing required arguments)
            }

            return;
        }

        $entityId = $this->getGetEntityId($john);
        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_GET, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        try {
            $this->mockApiEntityRequest(Http::VERB_GET, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
        } catch (ForbiddenException) {
            $this->fail(sprintf(
                "Got a ForbiddenException while attempting to access entity #%d as %s",
                $entityId,
                $john
            ));
        }

        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject($response->getData());
    }

    public function testGetItem_invalid(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_GET, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }
            catch (\Throwable) {
                // OK too (might be missing required arguments)
            }
        }

        $this->expectNotToPerformAssertions();
    }

    public function testGetItem_nonExisting(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_GET, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }
            catch (\Throwable) {
                // OK too (might be missing required arguments)
            }

            $this->expectNotToPerformAssertions();

            return;
        }

        try {
            $this->mockApiEntityRequest(Http::VERB_GET, '', ['id' => -1], [], $this->getAuthenticationHeaders($john));
            $this->fail('Failed throwing error on non-existing entity');
        }
        catch (NotFoundException) {}

        $response = $this->getResponse();

        $this->assertNull($response->getData());
    }

    public function testGetCollection(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_GET, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }
            catch (\Throwable) {
                // OK too (might be missing required arguments)
            }

            $this->expectNotToPerformAssertions();

            return;
        }

        $this->getGeefterSession()->invalidate();
        $this->prepareCollection($john);

        try {
            $this->mockApiEntityRequest(Http::VERB_GET);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        $this->mockApiEntityRequest(Http::VERB_GET, '', [], [], $this->getAuthenticationHeaders($john));
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection($response->getData());
    }

    public function testPost(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_POST, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }

            $this->expectNotToPerformAssertions();

            return;
        }

        $newEntityData = $this->getCreateEntityData($john);
        $this->assertNotNull($newEntityData);

        try {
            $this->mockApiEntityRequest(Http::VERB_POST, '', [], $newEntityData, ['Content-Type' => 'multipart/form-data']);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        $this->mockApiEntityRequest(
            Http::VERB_POST,
            '',
            [],
            ['data' => $newEntityData],
            $this->getAuthenticationHeaders($john) + ['Content-Type' => 'multipart/form-data']
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_CREATED, $response->getCode());

        $this->assertIsValidResourceObject($response->getData());
    }

    public function testPatch(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_PATCH, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }

            $this->expectNotToPerformAssertions();

            return;
        }

        $entity = $this->getNewEntity($john)
            ->save();
        $this->assertNotNull($entity->getId());

        $updatedData = $this->getUpdateEntityData($john);
        try {
            $this->mockApiEntityRequest(Http::VERB_PATCH, '', ['id' => $entity->getId()], [], [], json_encode(['data' => $updatedData]));
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        $this->mockApiEntityRequest(
            Http::VERB_PATCH,
            '',
            ['id' => $entity->getId()],
            [],
            $this->getAuthenticationHeaders($john),
            json_encode(['data' => $updatedData])
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject($response->getData());

        $updatedEntity = $this->getEntity($this->getEntityCode(), $entity->getId());
        foreach ($updatedData['attributes'] as $field => $value) {
            $this->assertNotEquals($entity->getData($field), $updatedEntity->getData($field));
            $this->assertEquals($value, $updatedEntity->getData($field));
        }
    }

    public function testDeleteItem(): void {
        $john = $this->getJohn();

        if (!in_array($method = Http::VERB_DELETE, $this->expectedAllowedMethods)) {
            try {
                $this->mockApiEntityRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
            }
            catch (MethodNotAllowedException) {
                // OK
            }

            $this->expectNotToPerformAssertions();

            return;
        }

        $entityId = $this->getDeleteEntityId($john);
        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('deleted_items', $response->getMeta());
        $this->assertEquals(1, $response->getMeta()['deleted_items']);

        $entity = $this->getEntity($this->getEntityCode(), $entityId);
        $this->assertNull($entity);
    }

//    public function testDeleteCollection() {
//        $this->markTestSkipped('Not implemented');
//
//        $john = $this->getJohn();
//
//        if (!in_array($method = Http::VERB_DELETE, $this->expectedAllowedMethods)) {
//            try {
//                $this->mockApiRequest($method, '', [], [], $this->getAuthenticationHeaders($john));
//                $this->fail(sprintf('Failed preventing call to an unsupported method: %s.', $method));
//            }
//            catch (MethodNotAllowedException $e) {
//                // OK
//            }
//            return;
//        }
//
//        $this->enableListeners();
//        $entityId = $this->getDeleteEntityId($john);
//
//        $this->assertNotNull($entityId);
//
//        try {
//            $this->mockApiRequest(Http::VERB_DELETE, ['id' => $entityId]);
//            $this->fail('Access allowed unauthenticated');
//        }
//        catch (UnauthorizedException $e) {
//            // Not supported yet, see #228
//            //$this->assertEquals(HttpStatus::STATUS_UNAUTHORIZED, http_response_code());
//        }
//
//        $this->mockApiRequest(Http::VERB_DELETE, ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
//        $response = $this->getResponse();
//
//
//    }

    /**
     * @param Geefter $asGeefter
     * @return AbstractModel
     */
    protected function getNewEntity($asGeefter) {
        return $this->newEntity(
            $this->getEntityCode(),
            ['data' => $this->getCreateEntityData($asGeefter)['attributes'] ?? []]
        );
    }

    /**
     * @param Geefter $asGeefter
     * @return int
     */
    abstract protected function getGetEntityId($asGeefter);

    /**
     * @param Geefter $asGeefter
     * @return array
     */
    abstract protected function getCreateEntityData($asGeefter);

    /**
     * @param Geefter $asGeefter
     * @return array
     */
    abstract protected function getUpdateEntityData($asGeefter);

    /**
     * @param Geefter $asGeefter
     * @return int
     */
    abstract protected function getDeleteEntityId($asGeefter);

    /**
     * @param Geefter $asGeefter
     * @return $this
     */
    abstract protected function prepareCollection($asGeefter);
}
