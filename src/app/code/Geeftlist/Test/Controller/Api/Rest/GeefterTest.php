<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Narrowspark\HttpStatus\Exception\ForbiddenException;
use Narrowspark\HttpStatus\Exception\MethodNotAllowedException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class GeefterTest extends AbstractModelControllerTest
{
    protected $entityCode = Geefter::ENTITY_TYPE;

    protected $expectedAllowedMethods = [
        Http::VERB_GET,
        Http::VERB_PATCH,
        Http::VERB_OPTIONS
    ];

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }

    public function testGetItem(): void {
        $john = $this->getJohn();
        $entityId = $this->getGetEntityId($john);

        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_GET, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        // Retrieve another geefter => Fail
        try {
            $this->mockApiEntityRequest(
                Http::VERB_GET,
                '',
                ['id' => $this->getJane()->getId()],
                [],
                $this->getAuthenticationHeaders($john)
            );
            $this->fail('Failed preventing call to GET for another geefter.');
        }
        catch (ForbiddenException) {}

        $response = $this->getResponse();

        $this->assertNull($response->getData());

        // Retrieve currently authenticated geefter => OK
        $this->mockApiEntityRequest(Http::VERB_GET, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));

        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject($response->getData());
    }

    /**
     * This test ensures we return a STATUS_FORBIDDEN even for an invalid ID (and not a 404 like other entities).
     */
    public function testGetItem_nonExisting(): void {
        $john = $this->getJohn();

        try {
            $this->mockApiEntityRequest(Http::VERB_GET, '', ['id' => '9999999999'], [], $this->getAuthenticationHeaders($john));
            $this->fail('Failed throwing error on non-existing entity');
        }
        catch (ForbiddenException) {}

        $response = $this->getResponse();
        $this->assertNull($response->getData());
    }

    public function testGetCollection(): void {
        $this->getGeefterSession()->invalidate();

        $john = $this->getJohn();
        $this->prepareCollection($john);

        try {
            $this->mockApiEntityRequest(Http::VERB_GET);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        $this->mockApiEntityRequest(Http::VERB_GET, '', [], [], $this->getAuthenticationHeaders($john));
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Geefter::ENTITY_TYPE,
            [$john->getId()]
        );
    }

    public function testPatch(): void {
        $john = $this->getJohn();
        $entity = $this->getNewEntity($john)
            ->save();

        $this->assertNotNull($entity->getId());

        $updatedData = $this->getUpdateEntityData($john);
        try {
            $this->mockApiEntityRequest(
                Http::VERB_PATCH,
                '',
                ['id' => $entity->getId()],
                [],
                [],
                json_encode(['data' => $updatedData])
            );
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        $this->mockApiEntityRequest(
            Http::VERB_PATCH,
            '',
            ['id' => $entity->getId()],
            [],
            $this->getAuthenticationHeaders($entity),
            json_encode(['data' => $updatedData])
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject($response->getData(), Geefter::ENTITY_TYPE);

        $updatedEntity = $this->getEntity($this->getEntityCode(), $entity->getId());
        foreach ($updatedData['attributes'] as $field => $value) {
            $this->assertNotEquals($entity->getData($field), $updatedEntity->getData($field));
            $this->assertEquals($value, $updatedEntity->getData($field));
        }
    }

    /**
     * Special case for deletion: not supported
     */
    public function testDeleteItem(): void {
        $john = $this->getJohn();
        $entityId = $this->getDeleteEntityId($john);

        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {}

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
            $this->fail('Failed preventing geeftee deletion.');
        }
        catch (MethodNotAllowedException) {}

        $response = $this->getResponse();

        $this->assertNull($response->getData());

        $this->disableGeefterAccessListeners();

        $entity = $this->getEntity($this->getEntityCode(), $entityId);
        $this->assertNotNull($entity->getId());
    }

    protected function getGetEntityId($asGeefter) {
        return $this->getEntity($this->entityCode, [
            'geefter_id' => $asGeefter->getId()
        ])->getId();
    }

    protected function getCreateEntityData($asGeefter): array {
        $uniqId = uniqid('API_Geefter_');
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'username' => $uniqId,
                'email'    => strtolower($uniqId) . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN,
                'password' => Constants::GEEFTER_OTHERS_PASSWORD
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        $uniqId = uniqid('API_Geefter_');
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'username' => $uniqId,
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        $uniqId = uniqid('API_Geefter_');
        return $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => $uniqId,
            'email'    => strtolower($uniqId) . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]])->save();
    }

    protected function prepareCollection($asGeefter): static {
        // nothing to do here (geefters should already exist in sample data)

        return $this;
    }
}
