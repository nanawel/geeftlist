<?php

namespace Geeftlist\Test\Controller\Api\Rest\Gift;


use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Controller\Api\Rest\AbstractModelRelationshipsTest;
use Narrowspark\HttpStatus\Exception\BadRequestException;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use Narrowspark\HttpStatus\Exception\UnprocessableEntityException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;
use OliveOil\Core\Model\Validation\ErrorInterface;

/**
 * @group api
 * @group controllers
 */
class RelationshipsTest extends AbstractModelRelationshipsTest
{
    protected $entityCode = Gift::ENTITY_TYPE;

    protected $expectedRelationships = [
        'creator' => [
            Http::VERB_OPTIONS, Http::VERB_GET, Http::VERB_PATCH
        ],
        'geeftees' => [
            Http::VERB_OPTIONS, Http::VERB_GET, Http::VERB_PATCH, HTTP::VERB_POST, Http::VERB_DELETE
        ],
        'discussion_topics' => [
            Http::VERB_OPTIONS, Http::VERB_GET
        ],
    ];

    public function testGetCreator_notAllowed(): void {
        $john = $this->getJohn();

        $entity = $this->getGetCreatorEntity($john, $this->getJane()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'creator',
                Http::VERB_GET,
                '',
                ['id' => $entityId]
            );
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {
        }
    }

    public function testGetCreator(): void {
        $john = $this->getJohn();

        $entity = $this->getGetCreatorEntity($john, $this->getJane()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->mockApiRelationshipsRequest(
            'creator',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john)
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject(
            $response->getData(),
            Geefter::ENTITY_TYPE,
            $entity->getCreatorId()
        );
    }

    public function testGetGeeftees(): void {
        $john = $this->getJohn();

        $entity = $this->getGetGeefteesEntity($john, $this->getJane()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->mockApiRelationshipsRequest(
            'geeftees',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john)
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Geeftee::ENTITY_TYPE,
            $entity->getGeefteeIds(),
        );
    }

    public function testGetDiscussionTopics(): void {
        $john = $this->getJohn();

        $entity = $this->getGetDiscussionTopicsEntity($john, $this->getJane()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->mockApiRelationshipsRequest(
            'discussion_topics',
            Http::VERB_GET,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john)
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $topicIds = [
            $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class)
                ->getProtectedTopic($entity)->getId(),
            $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class)
                ->getPublicTopic($entity)->getId()
        ];

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Topic::ENTITY_TYPE,
            $topicIds
        );
    }

    public function testPatchCreator_neverAllowed(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchCreatorEntity($john, $this->getJane()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'creator',
                Http::VERB_PATCH,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($john),
                [
                    'data' => [
                        'type' => Geefter::ENTITY_TYPE,
                        'id' => $this->getJane()->getId()
                    ]
                ]
            );
            $this->fail("Failed preventing update of gift's creator.");
        }
        catch (UnprocessableEntityException) {}

        $responseErrors = $this->getResponse()->getErrors();

        $this->assertIsArray($responseErrors);
        $this->assertEquals(ErrorInterface::TYPE_FIELD_NOT_OVERWRITABLE, $responseErrors[0]['code']);

        $this->assertEquals(HttpStatus::STATUS_UNPROCESSABLE_ENTITY, $this->getResponse()->getCode());

        $this->assertEquals(
            $entity->getCreatorId(),
            $this->getEntity($this->getEntityCode(), $entityId)->getCreatorId()
        );
    }

    public function testPatchGeeftees_empty(): void {
        $john = $this->getJohn();

        $entity = $this->getPatchGeefteesEntity($john, $this->getRichard()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        try {
            $this->mockApiRelationshipsRequest(
                'geeftees',
                Http::VERB_PATCH,
                '',
                ['id' => $entityId],
                [],
                $this->getAuthenticationHeaders($john),
                [
                    'data' => []
                ]
            );
            $this->fail('Failed preventing assigning a gift to an empty geeftees list.');
        }
        catch (BadRequestException) {
        }
    }

    public function testPatchGeeftees(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $entity = $this->getPatchGeefteesEntity($john, $this->getRichard()->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->assertCount(1, $entity->getGeefteeIds());
        $this->assertNotEquals($jane->getGeeftee()->getId(), $entity->getGeefteeIds()[0]);

        $this->mockApiRelationshipsRequest(
            'geeftees',
            Http::VERB_PATCH,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => [[
                    'type' => $jane->getGeeftee()->getEntityType(),
                    'id' => $jane->getGeeftee()->getId()
                ]]
            ]
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Geeftee::ENTITY_TYPE,
            [$jane->getGeeftee()->getId()]
        );

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertCount(1, $entity->getGeefteeIds());
        $this->assertEquals($jane->getGeeftee()->getId(), $entity->getGeefteeIds()[0]);
    }

    public function testPostGeeftees(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();

        // John and Richard need to be related to allow adding geeftee to gift
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $commonFamily->addGeeftee([
            $john->getGeeftee(),
            $jane->getGeeftee(),
            $richard->getGeeftee()
        ]);

        $entity = $this->getPatchGeefteesEntity($john, $jane->getGeeftee());
        $this->assertNotNull($entity);
        $entityId = $entity->getId();
        $this->assertNotNull($entityId);

        $this->assertCount(1, $entity->getGeefteeIds());
        $this->assertNotEquals($richard->getGeeftee()->getId(), $entity->getGeefteeIds()[0]);

        $this->mockApiRelationshipsRequest(
            'geeftees',
            Http::VERB_POST,
            '',
            ['id' => $entityId],
            [],
            $this->getAuthenticationHeaders($john),
            [
                'data' => [[
                    'type' => $richard->getGeeftee()->getEntityType(),
                    'id' => $richard->getGeeftee()->getId()
                ]]
            ]
        );

        $this->enableListeners('default');

        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidJsonApiCollection(
            $response->getData(),
            Geeftee::ENTITY_TYPE,
            [$jane->getGeeftee()->getId(), $richard->getGeeftee()->getId()]
        );

        $entity = $this->getEntity($entity->getEntityType(), $entityId);
        $this->assertCount(2, $entity->getGeefteeIds());
        $this->assertEqualsCanonicalizing(
            [$jane->getGeeftee()->getId(), $richard->getGeeftee()->getId()],
            $entity->getGeefteeIds()
        );
    }

    /**
     * @param Geefter $asGeefter
     * @param Geeftee[]|Geeftee $forGeeftees
     * @return Gift
     */
    protected function getGetCreatorEntity($asGeefter, $forGeeftees) {
        return $this->generateGift($asGeefter, $forGeeftees);
    }

    /**
     * @param Geefter $asGeefter
     * @param Geeftee[]|Geeftee $forGeeftees
     * @return Gift
     */
    protected function getGetGeefteesEntity($asGeefter, $forGeeftees) {
        return $this->generateGift($asGeefter, $forGeeftees);
    }

    /**
     * @param Geefter $asGeefter
     * @param Geeftee[]|Geeftee $forGeeftees
     * @return Gift
     */
    protected function getGetDiscussionTopicsEntity($asGeefter, $forGeeftees) {
        $gift = $this->generateGift($asGeefter, $forGeeftees);

        // Create topics if they do not exist
        $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class)->getProtectedTopic($gift);
        $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class)->getPublicTopic($gift);

        return $gift;
    }

    /**
     * @param Geefter $asGeefter
     * @param Geeftee[]|Geeftee $forGeeftees
     * @return Gift
     */
    protected function getPatchCreatorEntity($asGeefter, $forGeeftees) {
        return $this->generateGift($asGeefter, $forGeeftees);
    }

    /**
     * @param Geefter $asGeefter
     * @param Geeftee[]|Geeftee $forGeeftees
     * @return Gift
     */
    protected function getPatchGeefteesEntity($asGeefter, $forGeeftees) {
        return $this->generateGift($asGeefter, $forGeeftees);
    }

    /**
     * @param Geefter $asGeefter
     * @param Geeftee[]|Geeftee $forGeeftees
     * @return Gift
     */
    protected function generateGift($asGeefter, $forGeeftees) {
        if (!is_array($forGeeftees)) {
            $forGeeftees = [$forGeeftees];
        }

        $gift = $this->newEntity($this->entityCode, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $asGeefter->getId(),
        ]])->save();
        foreach ($forGeeftees as $forGeeftee) {
            $forGeeftee->addGift($gift);
        }

        return $gift;
    }
}
