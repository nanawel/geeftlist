<?php

namespace Geeftlist\Test\Controller\Api\Rest\Geeftee;


use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Controller\Api\Rest\AbstractModelRelationshipsTest;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class RelationshipsTest extends AbstractModelRelationshipsTest
{
    protected $entityCode = Geeftee::ENTITY_TYPE;

    protected $expectedRelationships = [
        'families' => [
            Http::VERB_OPTIONS, Http::VERB_GET
        ],
        'geefter' => [
            Http::VERB_OPTIONS, Http::VERB_GET
        ]
    ];

    protected function setUp(): void {
        parent::setUp();

        // Speed up execution
        $this->disableListeners('index');
    }
}
