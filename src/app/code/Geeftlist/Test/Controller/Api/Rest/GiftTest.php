<?php

namespace Geeftlist\Test\Controller\Api\Rest;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Narrowspark\HttpStatus\Exception\UnauthorizedException;
use Narrowspark\HttpStatus\HttpStatus;
use OliveOil\Core\Constants\Http;

/**
 * @group api
 * @group controllers
 */
class GiftTest extends AbstractModelControllerTest
{
    protected $entityCode = Gift::ENTITY_TYPE;

    protected function getGetEntityId($asGeefter) {
        return $this->getEntity($this->entityCode, [
            'creator_id' => $asGeefter->getId(),
            'status' => ['ne' => Gift\Field\Status::DELETED]
        ])->getId();
    }

    protected function getCreateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'description'     => $this->faker()->paragraphs(3, true),
                'estimated_price' => $this->faker()->randomFloat(2, 1, 2000),
                'creator_id'      => $asGeefter->getId()
            ]
        ];
    }

    protected function getUpdateEntityData($asGeefter): array {
        return [
            'type' => $this->getEntityCode(),
            'attributes' => [
                'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'description'     => $this->faker()->paragraphs(3, true),
                'estimated_price' => $this->faker()->randomFloat(2, 1, 2000)
            ]
        ];
    }

    protected function getDeleteEntityId($asGeefter) {
        return current($this->generateGifts($asGeefter, $this->getJane()->getGeeftee(), null, 1))->getId();
    }

    protected function prepareCollection($asGeefter): static {
        // Delete gifts with no creator (remainders of previous tests, not buggy or invalid but would
        // break our checks here)
        $creatorLessGifts = $this->getEntityCollection($this->getEntityCode(), ['creator_id' => ['null' => true]]);
        $creatorLessGifts->delete();

        return $this;
    }

    public function testPatch_withCompoundDocuments_geeftees(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Gift $entity */
        $entity = $this->getNewEntity($john)
            ->save();
        $this->assertNotNull($entity->getId());
        $this->assertEmpty($entity->getGeefteeIds());

        $updatedData = $this->getUpdateEntityData($john);
        $updatedData['relationships'] = [
            'geeftees' => [
                'data' => [[
                    'type' => Geeftee::ENTITY_TYPE,
                    'id' => $jane->getGeeftee()->getId()
                ]]
            ]
        ];

        $this->mockApiEntityRequest(
            Http::VERB_PATCH,
            '',
            ['id' => $entity->getId()],
            [],
            $this->getAuthenticationHeaders($john),
            json_encode(['data' => $updatedData])
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsValidResourceObject($response->getData());

        /** @var Gift $updatedEntity */
        $updatedEntity = $this->getEntity($this->getEntityCode(), $entity->getId());
        foreach ($updatedData['attributes'] as $field => $value) {
            $this->assertNotEquals($entity->getData($field), $updatedEntity->getData($field));
            $this->assertEquals($value, $updatedEntity->getData($field));
        }

        $this->assertEquals([$jane->getGeeftee()->getId()], $updatedEntity->getGeefteeIds());
    }

    public function testPost_withCompoundDocuments(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $entityData = $this->getCreateEntityData($john);
        $entityData['relationships'] = [
            'geeftees' => [
                'data' => [[
                    'type' => Geeftee::ENTITY_TYPE,
                    'id' => $jane->getGeeftee()->getId()
                ]]
            ]
        ];

        $this->mockApiEntityRequest(
            Http::VERB_POST,
            '',
            [],
            [],
            $this->getAuthenticationHeaders($john),
            json_encode(['data' => $entityData])
        );
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_CREATED, $response->getCode());

        $this->assertIsValidResourceObject($response->getData());

        /** @var Gift $createdEntity */
        $createdEntity = $this->getEntity($this->getEntityCode(), $response->getData()['id']);
        foreach ($entityData['attributes'] as $field => $value) {
            $this->assertEquals($value, $createdEntity->getData($field));
        }

        $this->assertEquals([$jane->getGeeftee()->getId()], $createdEntity->getGeefteeIds());
    }

    /**
     * Special case for deletion (actually "mark as deleted")
     */
    public function testDeleteItem(): void {
        $john = $this->getJohn();
        $entityId = $this->getDeleteEntityId($john);

        $this->assertNotNull($entityId);

        try {
            $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId]);
            $this->fail('Access allowed unauthenticated');
        }
        catch (UnauthorizedException) {
            // Not supported yet, see #228
            //$this->assertEquals(HttpStatus::STATUS_UNAUTHORIZED, http_response_code());
        }

        $this->mockApiEntityRequest(Http::VERB_DELETE, '', ['id' => $entityId], [], $this->getAuthenticationHeaders($john));
        $response = $this->getResponse();

        $this->assertEquals(HttpStatus::STATUS_OK, $response->getCode());

        $this->assertIsArray($response->getMeta());
        $this->assertArrayHasKey('deleted_items', $response->getMeta());
        $this->assertEquals(1, $response->getMeta()['deleted_items']);

        $this->disableGeefterAccessListeners();

        $entity = $this->getEntity($this->getEntityCode(), $entityId);
        $this->assertNotNull($entity->getId());
        $this->assertEquals(Gift\Field\Status::DELETED, $entity->getStatus());
    }
}
