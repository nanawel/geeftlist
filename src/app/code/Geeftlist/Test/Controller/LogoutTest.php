<?php

namespace Geeftlist\Test\Controller;





use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class LogoutTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        try {
            $this->mock('GET /logout');
            $this->fail('Failed rerouting not logged in user to home');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $this->setGeefterInSession($this->getJane());
        $this->assertNotEmpty($this->getGeefterSession()->getGeefter());

        try {
            $this->mock('GET /logout');
            $this->fail('Failed rerouting logged in user to home');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
            $this->assertEmpty($this->getGeefterSession()->getGeefter());
        }
    }
}
