<?php

namespace Geeftlist\Test\Controller\System\Healthcheck;

use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class PrometheusTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->tearDownControllerTest();
    }

    public function testIndex(): void {
        $fw = $this->getFw();

        $this->mock('GET /system/healthcheck');
        $response = $fw->get('RESPONSE');

        $helpLinePattern = '^# HELP\s+\w+\s*$';
        $typeLinePattern = '^# TYPE\s+\w+\s+gauge\s*$';  // Only gauge at this time
        $metricLinePattern = '^\w+{.*}\s+\d+(.\d+)?\s*$';

        $this->assertMatchesRegularExpression(
            "/($helpLinePattern\n$typeLinePattern\n($metricLinePattern)+\n)+/m",
            $response,
            'Invalid content detected'
        );
    }
}
