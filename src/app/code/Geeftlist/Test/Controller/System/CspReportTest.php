<?php

namespace Geeftlist\Test\Controller\System;

use Narrowspark\HttpStatus\Exception\UnsupportedMediaTypeException;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class CspReportTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-671
     */
    public function testPost_invalidContentType(): void {
        $this->expectException(UnsupportedMediaTypeException::class);
        $this->mock(
            'POST /system/cspReport/post',
            [],
            ['Content-Type' => 'application/json'],
            <<<EOJSON
            {
              "csp-report": {
                "blocked-uri": "http://example.com/css/style.css",
                "disposition": "report",
                "document-uri": "http://example.com/signup.html",
                "effective-directive": "style-src-elem",
                "original-policy": "default-src 'none'; style-src cdn.example.com; report-to /_/csp-reports",
                "referrer": "",
                "status-code": 200,
                "violated-directive": "style-src-elem"
              }
            }
            EOJSON,
        );
    }

    /**
     * @group ticket-671
     */
    public function testPost_invalidJson(): void {
        // Error is silently logged, nothing to expect on the client side
        $this->expectNotToPerformAssertions();

        $this->mock(
            'POST /system/cspReport/post',
            [],
            ['Content-Type' => 'application/csp-report'],
            <<<EOJSON
            {
              "csp-report": {
                "blocked-uri": "http://example.com/css/style.css",
                "disposition": "report",
                "document-uri": "http://example.com/signup.html",
                "effective-directive": "style-src-elem",
                "original-policy": "default-src 'none'; style-src cdn.example.com; report-to /_/csp-reports",
                "referrer": "",
                "status-code": 200,
                "violated-directive": "style-src-elem",
                "INVALID":
              }
            }
            EOJSON,
        );
    }

    /**
     * @group ticket-671
     */
    public function testPost_valid(): void {
        $this->expectNotToPerformAssertions();
        $this->mock(
            'POST /system/cspReport/post',
            [],
            ['Content-Type' => 'application/csp-report'],
            <<<EOJSON
            {
              "csp-report": {
                "blocked-uri": "http://example.com/css/style.css",
                "disposition": "report",
                "document-uri": "http://example.com/signup.html",
                "effective-directive": "style-src-elem",
                "original-policy": "default-src 'none'; style-src cdn.example.com; report-to /_/csp-reports",
                "referrer": "",
                "status-code": 200,
                "violated-directive": "style-src-elem"
              }
            }
            EOJSON,
        );
    }
}
