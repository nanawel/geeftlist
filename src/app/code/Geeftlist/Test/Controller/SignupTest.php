<?php

namespace Geeftlist\Test\Controller;


use Geeftlist\Model\Geefter;
use Geeftlist\Service\Invitation;
use Geeftlist\Test\Constants;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class SignupTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;

    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->enableGeefterAccessListeners();

        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', true);
        $this->getAppConfig()->setValue('INVITATION_ENABLED', true);
        $this->getAppConfig()->setValue('INVITATION_MAX_PER_GEEFTER', 0);
        $this->getAppConfig()->setValue('SKIP_EMAIL_CHECK_MX', true);

        $this->getRepository(\Geeftlist\Model\Invitation::ENTITY_TYPE)
            ->deleteAll();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getSession()->unsetPersistentData('create_account_form_captcha');
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndex_notLoggedIn_registrationEnabled(): void {
        $fw = $this->getFw();

        $this->mock('GET /signup');
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertMatchesRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationEnabled_withInvalidInvitationToken(): void {
        $fw = $this->getFw();

        $this->mock(sprintf('GET /signup?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, '123456'));
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertMatchesRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationDisabled_withoutInvitation(): void {
        $fw = $this->getFw();
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $this->mock('GET /signup');
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertDoesNotMatchRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Unexpected form in response'
        );
        $this->assertHtmlInnerHtmlEquals(
            $this->getI18n()->tr('Sorry, registration is disabled.'),
            '//p[@class="dialog-message"]',
            $response,
            'Cannot find error message in response.'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationDisabled_withInvalidInvitationToken(): void {
        $fw = $this->getFw();
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $this->mock(sprintf('GET /signup?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, '123456'));
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertDoesNotMatchRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Unexpected form in response'
        );
        $this->assertHtmlInnerHtmlEquals(
            $this->getI18n()->tr('Invalid invitation token provided.'),
            '//p[@class="dialog-message"]',
            $response,
            'Cannot find error message in response.'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationDisabled_withUsedInvitation(): void {
        $fw = $this->getFw();
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $invitation = $this->getContainer()->get(Invitation::class)
            ->generate($john);
        $invitation->setUsedAt(DateTime::getDateSql('-1 day'));
        $this->getRepository(\Geeftlist\Model\Invitation::ENTITY_TYPE)->save($invitation);

        $this->unsetGeefterInSession();

        $this->mock(sprintf('GET /signup?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, $invitation->getToken()));
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertDoesNotMatchRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Unexpected form in response'
        );
        $this->assertHtmlInnerHtmlEquals(
            $this->getI18n()->tr('This invitation has already been used.'),
            '//p[@class="dialog-message"]',
            $response,
            'Cannot find error message in response.'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationDisabled_withExpiredInvitation(): void {
        $fw = $this->getFw();
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $invitation = $this->getContainer()->get(Invitation::class)
            ->generate($john);
        $invitation->setExpiresAt(DateTime::getDateSql('-1 day'));
        $this->getRepository(\Geeftlist\Model\Invitation::ENTITY_TYPE)->save($invitation);

        $this->unsetGeefterInSession();

        $this->mock(sprintf('GET /signup?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, $invitation->getToken()));
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertDoesNotMatchRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Unexpected form in response'
        );
        $this->assertHtmlInnerHtmlEquals(
            $this->getI18n()->tr('This invitation token is expired.'),
            '//p[@class="dialog-message"]',
            $response,
            'Cannot find error message in response.'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationDisabled_withValidInvitation(): void {
        $fw = $this->getFw();
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $invitation = $this->getContainer()->get(Invitation::class)
            ->generate($john);

        $this->unsetGeefterInSession();

        $this->mock(sprintf('GET /signup?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, $invitation->getToken()));
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertHtmlAttributeEquals(
            $this->getUrl(sprintf(
                'signup/post?%s=%s',
                Invitation::INVITATION_TOKEN_URL_PARAM,
                $invitation->getToken()
            )),
            ['//form[@id="account-create"]', 'action'],
            $response,
            'Invalid form "action" value: missing invitation token?'
        );
        $this->assertMatchesRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-668
     */
    public function testIndex_notLoggedIn_registrationAndInvitationDisabled_withValidInvitation(): void {
        $fw = $this->getFw();
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);
        $this->getAppConfig()->setValue('INVITATION_ENABLED', false);

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $invitation = $this->getContainer()->get(Invitation::class)
            ->generate($john);

        $this->unsetGeefterInSession();

        $this->mock(sprintf('GET /signup?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, $invitation->getToken()));
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertDoesNotMatchRegularExpression(
            '/<form\s.*?id=".*?account-create.*?"/',
            $response,
            'Unexpected form in response'
        );
        $this->assertHtmlInnerHtmlEquals(
            $this->getI18n()->tr('Sorry, registration is disabled.'),
            '//p[@class="dialog-message"]',
            $response,
            'Cannot find error message in response.'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndex_loggedIn(): void {
        $this->setGeefterInSession($this->getJane());

        try {
            $this->mock('GET /signup');
            $this->fail('Failed rerouting to dashboard');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('dashboard'), $reroute->getUrl());
        }
    }

    public function testPost_validForm_noEmailConfirmation(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))
            );
            $this->assertMatchesRegularExpression(
                '#' . $this->getUrl('/signup/confirm') . '/token/.*#',
                $reroute->getUrl()
            );
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNotNull($newGeefter);
        $this->assertNotNull($newGeefter->getId());
        $this->assertEquals($newUsername, $newGeefter->getUsername());
        $this->assertEquals($newEmail, $newGeefter->getEmail());
    }

    public function testPost_validForm_invalidCaptcha(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => 'foo'
                ]
            ]);
            $this->fail('Failed rerouting after invalid captcha submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getI18n()->tr('Invalid captcha answer. Please retry.'),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))->getMessage()
            );
            $this->assertStringEndsWith($this->getUrl('/signup'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    public function testPost_emptyUsername_noEmailConfirmation(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => '',
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after invalid form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/signup'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    public function testPost_emptyEmail_noEmailConfirmation(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);
        $this->getAppConfig()->setValue('SKIP_EMAIL_CHECK_MX', false);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after invalid form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/signup'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    public function testPost_invalidEmail_noEmailConfirmation(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => '',
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after invalid form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('/signup'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    /**
     * @group ticket-668
     */
    public function testPost_registrationDisabled_withoutInvitation(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit without invitation');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getI18n()->tr('Sorry, registration is disabled.'),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))->getMessage()
            );
            $this->assertStringEndsWith($this->getUrl('/signup'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    /**
     * @group ticket-668
     */
    public function testPost_registrationDisabled_withInvalidInvitationToken(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        $queryString = sprintf('?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, '123456');

        try {
            $this->mock('POST /signup/post' . $queryString, [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit with invalid invitation token');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getI18n()->tr('Invalid invitation token provided.'),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))->getMessage()
            );
            $this->assertStringEndsWith($this->getUrl('/signup/index' . $queryString), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    /**
     * @group ticket-668
     */
    public function testPost_registrationDisabled_withExpiredInvitationToken(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $invitation = $this->getContainer()->get(Invitation::class)
            ->generate($john);
        $invitation->setExpiresAt(DateTime::getDateSql('-1 day'));
        $this->getRepository(\Geeftlist\Model\Invitation::ENTITY_TYPE)->save($invitation);

        $this->unsetGeefterInSession();

        $queryString = sprintf('?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, $invitation->getToken());

        try {
            $this->mock('POST /signup/post' . $queryString, [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit with expired invitation token');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEquals(
                $this->getI18n()->tr('This invitation token is expired.'),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))->getMessage()
            );
            $this->assertStringEndsWith($this->getUrl('/signup/index' . $queryString), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNull($newGeefter);
    }

    /**
     * @group ticket-668
     */
    public function testPost_registrationDisabled_withValidInvitationToken(): void {
        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', true);
        $this->getAppConfig()->setValue('GEEFTER_REGISTRATION_ENABLE', false);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $invitation = $this->getContainer()->get(Invitation::class)
            ->generate($john);

        $this->unsetGeefterInSession();

        $queryString = sprintf('?%s=%s', Invitation::INVITATION_TOKEN_URL_PARAM, $invitation->getToken());

        try {
            $this->mock('POST /signup/post' . $queryString, [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit with valid invitation token');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))
            );
            $this->assertStringContainsString($this->getUrl('/signup/confirm/token/'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNotNull($newGeefter);
    }

    public function testPostAndConfirm_validForm(): void {
        $fw = $this->getFw();

        $this->getAppConfig()->setValue('NO_CONFIRMATION_EMAIL', false);

        $newUsername = uniqid('new_user_');
        $newEmail = $newUsername . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN;
        $this->assertNull($this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]));
        $captcha = $this->generateCaptchaInSession('create_account_form_captcha');

        // POST FORM
        try {
            $this->mock('POST /signup/post', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'username' => $newUsername,
                'email'    => $newEmail,
                'captcha'  => [
                    'id' => $captcha->getId(),
                    'answer' => $captcha->getPhrase()
                ]
            ]);
            $this->fail('Failed rerouting after form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))
            );
            $this->assertStringEndsWith($this->getUrl('signup/success'), $reroute->getUrl());
        }

        $newGeefter = $this->getEntity(Geefter::ENTITY_TYPE, [
            'email' => $newEmail
        ]);
        $this->assertNotNull($newGeefter);
        $this->assertNotNull($newGeefter->getId());
        $this->assertEquals($newUsername, $newGeefter->getUsername());
        $this->assertEquals($newEmail, $newGeefter->getEmail());
        $this->assertNotNull($newGeefter->getPasswordToken());
        $this->assertNull($newGeefter->getPasswordHash());
        $this->assertNull($this->getGeefterSession()->getGeefter());

        // GET CONFIRM PAGE
        $this->mock('GET /signup/confirm/token/' . $newGeefter->getPasswordToken());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?signup-confirm.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        // POST CONFIRM PAGE
        $password = 'g33ftl1s7';
        try {
            $this->mock('POST /signup/confirmPost', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'token'            => $newGeefter->getPasswordToken(),
                'password'         => $password,
                'password_confirm' => $password
            ]);
            $this->fail('Failed rerouting after form submit');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR))
            );
            $this->assertStringEndsWith($this->getUrl('dashboard'), $reroute->getUrl());
        }

        $newGeefter = $this->getGeefterSession()->getGeefter();
        $this->assertNotNull($newGeefter);
        $this->assertEquals($newGeefter->getEmail(), $newGeefter->getEmail());
        $this->assertNotNull($newGeefter->getPasswordHash());
        $this->assertTrue($this->getCryptService()->verify($password, $newGeefter->getPasswordHash()));
    }
}
