<?php

namespace Geeftlist\Test\Controller\Gift\Grid;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ForMeTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use GiftTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();

        $this->getContainer()->get(\Geeftlist\View\Grid\Gift::class)->reset();
    }

    public function testIndex_notLoggedIn(): void {
        try {
            $this->mock('GET /gift/index/for/me');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndex_loggedIn(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $jane->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $jane->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/index/for/me');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndex_loggedIn_noFamily(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $richard->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $richard->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        $this->mock('GET /gift/index/for/me');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-379
     */
    public function testGrid_hideArchived(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $richardGifts = $this->generateGifts($richard, $richard->getGeeftee(), Gift\Field\Status::AVAILABLE, 12);
        foreach ($richardGifts as $richardGift) {
            $richardGift->setStatus(
                $richardGift->getId() % 3 === 0
                    ? Gift\Field\Status::ARCHIVED
                    : Gift\Field\Status::AVAILABLE
            )->save();
        }

        $expectedRichardGifts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', $richard->getGeeftee()->getId())
            ->addFieldToFilter('main_table.creator_id', $richard->getId())
            ->addFieldToFilter('main_table.status', Gift\Field\Status::AVAILABLE)
            ->load();
        $expectedRichardGiftIds = $expectedRichardGifts->getAllIds();
        sort($expectedRichardGiftIds);
        $expectedAllRichardGifts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', $richard->getGeeftee()->getId())
            ->addFieldToFilter('main_table.creator_id', $richard->getId())
            ->addFieldToFilter('main_table.status', ['ne' => Gift\Field\Status::DELETED])
            ->load();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);
        $maxRecords = 100;

        $this->mock('GET /gift/grid/for/me [ajax]', [
            'length' => $maxRecords
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertNotEquals($response['recordsTotal'], $response['recordsFiltered']);
        $this->assertEquals(count($expectedAllRichardGifts), $response['recordsTotal']);
        $this->assertEquals(count($expectedRichardGifts), $response['recordsFiltered']);
        $this->assertEquals(min($maxRecords, count($expectedRichardGifts)), count($response['items']));
        $giftIds = array_column($response['items'], 'gift_id');
        sort($giftIds);
        $this->assertEquals($expectedRichardGiftIds, $giftIds);
    }

    /**
     * @group ticket-379
     */
    public function testGrid_showArchived(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $richardGifts = $this->generateGifts($richard, $richard->getGeeftee(), Gift\Field\Status::AVAILABLE, 12);
        foreach ($richardGifts as $richardGift) {
            $richardGift->setStatus(
                $richardGift->getId() % 3 === 0
                    ? Gift\Field\Status::ARCHIVED
                    : Gift\Field\Status::AVAILABLE
            )->save();
        }

        $expectedRichardGifts = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', $richard->getGeeftee()->getId())
            ->addFieldToFilter('main_table.creator_id', $richard->getId())
            ->addFieldToFilter('main_table.status', ['ne' => Gift\Field\Status::DELETED])
            ->load();
        $expectedRichardGiftIds = $expectedRichardGifts->getAllIds();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);
        $maxRecords = 100;

        $this->mock('GET /gift/grid/for/me [ajax]', [
            'custom_filters' => ['show_archived' => 'true'],
            'length' => $maxRecords
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals(count($expectedRichardGifts), $response['recordsTotal']);
        $this->assertEquals(count($expectedRichardGifts), $response['recordsFiltered']);
        $this->assertEqualsCanonicalizing($expectedRichardGifts->getAllIds(), array_column($response['items'], 'gift_id'));
        $this->assertEquals(min($maxRecords, count($expectedRichardGifts)), count($response['items']));
        $giftIds = array_column($response['items'], 'gift_id');
        $this->assertEqualsCanonicalizing($expectedRichardGiftIds, $giftIds);
    }

    /**
     * @group ticket-617
     */
    public function testGrid_searchAndOrder(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $this->mock('GET /gift/grid/for/me [ajax]', [
            'custom_filters' => ['show_archived' => 'true'],
            'order' => [
                ['column' => 4, 'dir' => 'asc']
            ],
            'search' => ['value' => 'a'],
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        // Just check we don't get an error as response
        $this->assertIsArray($response);
        $this->assertArrayHasKey('recordsTotal', $response);
        $this->assertArrayHasKey('recordsFiltered', $response);
        $this->assertArrayHasKey('items', $response);
    }

    public function testGrid_noFamily(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $gift = current($this->generateGifts($richard, $richard->getGeeftee(), Gift\Field\Status::AVAILABLE, 1));

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        $this->mock('GET /gift/grid/for/me [ajax]');
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals(1, $response['recordsTotal']);
        $this->assertEquals(1, $response['recordsFiltered']);
        $this->assertEquals(1, count($response['items']));

        $item = $response['items'][0];
        $this->assertEquals($gift->getLabel(), $item['label']);
    }
}
