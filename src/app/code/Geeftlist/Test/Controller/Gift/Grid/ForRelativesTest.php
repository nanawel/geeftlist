<?php

namespace Geeftlist\Test\Controller\Gift\Grid;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ForRelativesTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();

        $this->getContainer()->get(\Geeftlist\View\Grid\Gift::class)->reset();
    }

    public function testIndex_notLoggedIn(): void {
        try {
            $this->mock('GET /gift/index/for/relatives');
            $this->fail('Failed rerouting unauthorized page to login');
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndex_loggedIn(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $gift = $this->newEntity(
            Gift::ENTITY_TYPE,
            [
                'data' => [
                    'creator_id' => $jane->getId(),
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
                ]
            ]
        )->save();
        $john->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/index/for/relatives');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndex_loggedIn_noFamily(): void {
        $fw = $this->getFw();

        $this->enableGeefterAccessListeners();

        $this->setGeefterInSession($this->getRichard());

        $this->mock('GET /gift/index/for/relatives');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<div\s.*?class="dialog.*?error.*?"/', $response, 'Cannot find error dialog in response');
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-379
     * @group ticket-400
     */
    public function testGrid_hideArchived(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $geefters = $this->generateCutsLight(3, 1, 2, 3, 5)['geefters'];

        // Only test with first geefter
        $richard = current($geefters);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        $expectedVisibleGifts = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', ['neq' => $richard->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.creator_id', $richard->getId())
            ->addFieldToFilter('main_table.status', Gift\Field\Status::AVAILABLE)
            ->getItems();
        $expectedAllGifts = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', ['neq' => $richard->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.creator_id', $richard->getId())
            ->getItems();

        $this->assertNotEmpty($expectedVisibleGifts);
        $this->assertNotEmpty($expectedAllGifts);

        $maxRecords = 100;

        $this->mock(
            'GET /gift/grid/for/relatives [ajax]',
            [
                'length' => $maxRecords
            ]
        );
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertNotEquals($response['recordsTotal'], $response['recordsFiltered']);
        $this->assertEquals(count($expectedAllGifts), $response['recordsTotal']);
        $this->assertEquals(count($expectedVisibleGifts), $response['recordsFiltered']);
        $this->assertEquals(count($expectedVisibleGifts), count($response['items']));
        $giftIds = array_column($response['items'], 'gift_id');
        sort($giftIds);
        $this->assertEquals(array_keys($expectedVisibleGifts), $giftIds);
    }

    /**
     * @group ticket-379
     */
    public function testGrid_showArchived(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $geefters = $this->generateCutsLight(3, 1, 2, 3, 5)['geefters'];

        // Only test with first geefter
        $richard = current($geefters);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        $expectedVisibleGifts = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('geeftee', ['neq' => $richard->getGeeftee()->getId()])
            ->addFieldToFilter('main_table.creator_id', $richard->getId())
            ->getItems();

        $this->assertNotEmpty($expectedVisibleGifts);

        $maxRecords = 100;

        $this->mock(
            'GET /gift/grid/for/relatives [ajax]',
            [
                'custom_filters' => ['show_archived' => 'true'],
                'length' => $maxRecords
            ]
        );
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        $this->assertIsArray($response);
        $this->assertEquals(count($expectedVisibleGifts), $response['recordsTotal']);
        $this->assertEquals(count($expectedVisibleGifts), $response['recordsFiltered']);
        $this->assertEquals(min($maxRecords, count($expectedVisibleGifts)), count($response['items']));
        $giftIds = array_column($response['items'], 'gift_id');
        sort($giftIds);
        $this->assertEquals(array_keys($expectedVisibleGifts), $giftIds);
    }

    /**
     * @group ticket-617
     */
    public function testGrid_searchAndOrder(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($john);

        $this->mock('GET /gift/grid/for/me [ajax]', [
            'custom_filters' => ['show_archived' => 'true'],
            'order' => [
                ['column' => 4, 'dir' => 'asc']
            ],
            'search' => ['value' => 'a'],
        ]);
        $json = $fw->get('RESPONSE');
        $response = json_decode((string) $json, true);

        // Just check we don't get an error as response
        $this->assertIsArray($response);
        $this->assertArrayHasKey('recordsTotal', $response);
        $this->assertArrayHasKey('recordsFiltered', $response);
        $this->assertArrayHasKey('items', $response);
    }

    /**
     * @group ticket-400
     */
    public function testGrid_noFamily(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $gift = current(
            $this->generateGifts(
                $richard,
                $this->getJohn()->getGeeftee(),
                Gift\Field\Status::AVAILABLE,
                1
            )
        );
        $this->assertNotNull($gift->getId());

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        $this->mock('GET /gift/grid/for/relatives [ajax]');
        $json = $fw->get('RESPONSE');

        $response = json_decode((string) $json, true);
        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }
}
