<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class RequestArchivingTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use GiftTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testRequestArchiving_invalidGift(): void {
        $richard = $this->getRichard();

        $gift = $this->getEntity(Gift::ENTITY_TYPE, [
            'creator_id' => $this->getJane()->getId()
        ]);

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->setGeefterInSession($richard);

        try {
            $this->mock(sprintf(
                'GET /gift/requestArchiving/gift_id/%d?%s=%s',
                $gift->getId(),
                $this->getCsrfTokenName(),
                $this->getCsrfToken()
            ));
            $this->fail('Failed preventing access to the requestArchiving form for an invalid gift.');
        }
        catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testRequestArchiving_invalidCsrf(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        $this->setGeefterInSession($john);

        try {
            $this->mock(sprintf(
                'GET /gift/requestArchiving/gift_id/%d?%s=%s',
                $gift->getId(),
                $this->getCsrfTokenName(),
                'foo'
            ));
            $this->fail('Failed rerouting on invalid CSRF.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testRequestArchiving_validGift(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->setGeefterInSession($john);

        try {
            $this->mock(sprintf(
                'GET /gift/requestArchiving/gift_id/%d?%s=%s',
                $gift->getId(),
                $this->getCsrfTokenName(),
                $this->getCsrfToken()
            ));
            $this->fail('Failed rerouting on success.');
        }
        catch (Reroute $reroute) {
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertCount(1, $this->getEmailSender()->getSentEmails());
    }
}
