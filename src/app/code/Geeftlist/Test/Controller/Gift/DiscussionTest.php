<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Notification;
use Geeftlist\Test\Controller\Discussion\Traits\EntityGeneratorTrait;
use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class DiscussionTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use GiftTrait;
    use EntityGeneratorTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-632
     * @group ticket-620
     */
    public function testEditPost_invalidPost_notAuthor(): void {
        $post = $this->getEntity(Post::ENTITY_TYPE, [
            'author_id' => ['neq' => $this->getJohn()->getId()]
        ]);

        $this->setGeefterInSession($this->getJohn());

        try {
            $this->mock('GET /discussion_post/edit/post_id/' . $post->getId());
            $this->fail('Failed preventing access to the edit form with an invalid post.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }
    }

    /**
     * @group ticket-632
     */
    public function testEditPost_validPost(): void {
        $fw = $this->getFw();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $topic = $this->generateGiftWithTopic($richard, $richard2->getGeeftee());
        $postMessage = uniqid('message_');
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'topic_id' => $topic->getId(),
            'author_id' => $richard->getId(),
            'message' => $postMessage
        ]])->save();

        $this->setGeefterInSession($richard);

        $this->mock('GET /discussion_post/edit/post_id/' . $post->getId());
        $response = $fw->get('RESPONSE');
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testSave_invalidGift(): void {
        $this->markTestIncomplete('Not implemented');
    }

    public function testSave_validGift(): void {
        $this->markTestIncomplete('Not implemented');
    }

    public function testSave_validPost_existingEntity(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $topic = $this->generateGiftWithTopic($jane, $this->getJohn()->getGeeftee());
        $postMessage = uniqid('message_');
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'topic_id' => $topic->getId(),
            'author_id' => $jane->getId(),
            'message' => $postMessage
        ]])->save();
        $newMessage = $this->faker()->paragraphs(2, true);

        $this->assertNotEquals($newMessage, $post->getMessage());
        $this->assertNotNull($this->getEntity(Post::ENTITY_TYPE, ['message' => $postMessage]));

        try {
            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'post_id'                 => $post->getId(),
                'message'                 => $newMessage
            ]);
            $this->fail('Failed rerouting after post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $post = $this->getEntity(Post::ENTITY_TYPE, $post->getId());
            $this->assertEquals($newMessage, $post->getMessage());

        }
    }

    public function testSave_validPost_newEntity(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $topic = $this->generateGiftWithTopic($jane, $this->getJohn()->getGeeftee());
        $uniqueTitle = uniqid('title_');
        $message = $this->faker()->paragraphs(2, true);

        try {
            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'topic_id'                => $topic->getId(),
                'title'                   => $uniqueTitle,
                'message'                 => $message
            ]);
            $this->fail('Failed rerouting after post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();

            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $post = $this->getEntity(Post::ENTITY_TYPE, ['title' => $uniqueTitle]);
            $this->assertNotNull($post);
            $this->assertEquals($message, $post->getMessage());
        }
    }

    /**
     * @group ticket-632
     */
    public function testSave_validPost_newEntity_withRecipients_enabledByConfig(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $this->generateFamily($richard, [$richard2, $richard3]);
        $topic = $this->generateGiftWithTopic($richard, $richard2->getGeeftee());

        $this->getAppConfig()->setValue('GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION', true);

        $this->setGeefterInSession($richard);
        $this->enableListeners('geefternotification');

        $uniqueTitle = uniqid('title_');
        $message = $this->faker()->paragraphs(2, true);
        try {
            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'topic_id'                => $topic->getId(),
                'title'                   => $uniqueTitle,
                'message'                 => $message,
                'recipient_ids'           => [$richard3->getId()],
            ]);
            $this->fail('Failed rerouting after post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();

            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $post = $this->getEntity(Post::ENTITY_TYPE, ['title' => $uniqueTitle]);
            $this->assertNotNull($post);
            $this->assertEquals($message, $post->getMessage());

            $notification = $this->getEntity(Notification::ENTITY_TYPE, [
                'target_type'  => Post::ENTITY_TYPE,
                'target_id'    => $post->getId(),
                'event_name'   => Notification\Discussion\Post\Constants::EVENT_NEW_RECIPIENTS
            ]);
            $this->assertNotNull($notification);
        }
    }

    /**
     * @group ticket-632
     */
    public function testSave_validPost_newEntity_withRecipients_disabledByConfig(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $this->generateFamily($richard, [$richard2, $richard3]);
        $topic = $this->generateGiftWithTopic($richard, $richard2->getGeeftee());

        $this->getAppConfig()->setValue('GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION', false);

        $this->setGeefterInSession($richard);
        $this->enableListeners('geefternotification');

        $uniqueTitle = uniqid('title_');
        $message = $this->faker()->paragraphs(2, true);
        try {
            $this->mock('POST /discussion_post/save', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'topic_id'                => $topic->getId(),
                'title'                   => $uniqueTitle,
                'message'                 => $message,
                'recipient_ids'           => [$richard3->getId()],
            ]);
            $this->fail('Failed rerouting after post');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->disableGeefterAccessListeners();

            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $post = $this->getEntity(Post::ENTITY_TYPE, ['title' => $uniqueTitle]);
            $this->assertNotNull($post);
            $this->assertEquals($message, $post->getMessage());

            $notification = $this->getEntity(Notification::ENTITY_TYPE, [
                'target_type'  => Post::ENTITY_TYPE,
                'target_id'    => $post->getId(),
                'event_name'   => Notification\Discussion\Post\Constants::EVENT_NEW_RECIPIENTS
            ]);
            $this->assertNull($notification);
        }
    }

    /**
     * @group ticket-632
     */
    public function testDelete_validPost_missingCsrf(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $topic = $this->generateGiftWithTopic($richard, $richard2->getGeeftee());
        $postMessage = uniqid('message_');
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'topic_id' => $topic->getId(),
            'author_id' => $richard->getId(),
            'message' => $postMessage
        ]])->save();

        $this->setGeefterInSession($richard);

        try {
            $this->mock('GET /discussion_post/delete/post_id/' . $post->getId());
            $this->fail('Failed preventing deletion with missing CSRF token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $this->assertNotNull($this->getEntity(Post::ENTITY_TYPE, $post->getId()));
        }
    }

    /**
     * @group ticket-632
     */
    public function testDelete_validPost(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $topic = $this->generateGiftWithTopic($richard, $richard2->getGeeftee());
        $postMessage = uniqid('message_');
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'topic_id' => $topic->getId(),
            'author_id' => $richard->getId(),
            'message' => $postMessage
        ]])->save();

        $this->setGeefterInSession($richard);

        try {
            $this->mock('GET /discussion_post/delete/post_id/' . $post->getId()
                . sprintf('?%s=%s', $this->getCsrfTokenName(), $this->getCsrfToken()),
            );
            $this->fail('Failed rerouting after request');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $this->assertNull($this->getEntity(Post::ENTITY_TYPE, $post->getId()));
        }
    }
}
