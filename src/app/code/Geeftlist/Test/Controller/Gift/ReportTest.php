<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ReportTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use GiftTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testReport_invalidGift(): void {
        $richard = $this->getRichard();

        $gift = $this->getEntity(Gift::ENTITY_TYPE, [
            'creator_id' => $this->getJane()->getId()
        ]);

        $this->setGeefterInSession($richard);

        try {
            $this->mock('GET /gift/report/gift_id/' . $gift->getId());
            $this->fail('Failed preventing access to the report form for an invalid gift.');
        }
        catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }
    }

    public function testReport_validGift(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->setGeefterInSession($john);

        $this->mock('GET /gift/report/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');

        $this->assertValidHtmlWithoutErrors($response);
        $this->assertHtmlAttributeEquals(
            $gift->getViewUrl(),
            ['//a[@class="gift-page-link"]', 'href'],
            $response,
            'Wrong gift URL (or cannot find gift box in the page?)'
        );
        $this->assertHtmlElementExists(
            sprintf('//form[starts-with(@action, "%s")]', $this->getUrl('gift/reportSubmit/gift_id/' . $gift->getId())),
            $response,
            'Missing form in the page.'
        );
    }

    public function testReportSubmit_invalidGift(): void {
        $richard = $this->getRichard();

        $gift = $this->getEntity(Gift::ENTITY_TYPE, [
            'creator_id' => $this->getJane()->getId()
        ]);

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->setGeefterInSession($richard);

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed preventing form POST for an invalid gift.');
        }
        catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testReportSubmit_invalidForm_missingCsrf(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        $this->setGeefterInSession($john);

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                'recipient' => Gift\Constants::REPORT_CREATOR,
                'message' => 'lorem ipsum'
            ]);
            $this->fail('Failed preventing form POST with missing CSRF token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }


    public function testReportSubmit_invalidForm(): void {
        $john = $this->getJohn();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->setGeefterInSession($john);

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed preventing form POST with missing mandatory values.');
        }
        catch (Reroute $reroute) {
            $this->assertCount(1, $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->getGeefterSession()->clearMessages();
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'message' => 'lotem ipsum'
            ]);
            $this->fail('Failed preventing form POST with a missing recipient.');
        }
        catch (Reroute $reroute) {
            $this->assertCount(1, $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->getGeefterSession()->clearMessages();
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'recipient' => 'foo'
            ]);
            $this->fail('Failed preventing form POST with an invalid recipient.');
        }
        catch (Reroute $reroute) {
            $this->assertCount(1, $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->getGeefterSession()->clearMessages();
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'recipient' => 'creator'
            ]);
            $this->fail('Failed preventing form POST with an empty message.');
        }
        catch (Reroute) {
            $this->assertCount(1, $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->getGeefterSession()->clearMessages();
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testReportSubmit_validGift(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->setGeefterInSession($john);

        try {
            $this->mock('POST /gift/reportSubmit/gift_id/' . $gift->getId(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'recipient' => Gift\Constants::REPORT_CREATOR,
                'message' => $this->faker()->paragraph(2)
            ]);
        }
        catch (Reroute $reroute) {
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());
        }

        $this->assertCount(1, $this->getEmailSender()->getSentEmails());
    }
}
