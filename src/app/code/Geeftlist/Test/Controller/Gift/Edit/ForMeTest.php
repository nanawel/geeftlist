<?php

namespace Geeftlist\Test\Controller\Gift\Edit;


use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Service\Gift\Image;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ForMeTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use GiftTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();

        $this->getContainer()->get(\Geeftlist\View\Grid\Gift::class)->reset();
    }

    /**
     * @group ticket-267
     */
    public function testNew(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->enableGeefterAccessListeners();

        $this->mock('GET /gift/new/for/me');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-new.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        // Geeftee input must be prefilled with current geeftee
        $this->assertHtmlAttributeEquals(
            $jane->getGeeftee()->getId(),
            ['//input[@name="geeftee_ids"]', 'value'],
            $response
        );
    }

    /**
     * @group ticket-403
     */
    public function testNew_fromDuplicate(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var Gift $ownGift */
        $ownGift = $jane->getGeeftee()->getGiftCollection()->getFirstItem();

        $this->enableGeefterAccessListeners();

        $this->mock('GET /gift/new/for/me/copyOf/' . $ownGift->getId());
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-new.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertHtmlAttributeEquals(
            $jane->getI18n()->tr('Copy of {0}', $ownGift->getLabel()),
            ['//input[@name="label"]', 'value'],
            $response,
            'Label must be set to "Copy of {gift label}" when duplicating a gift'
        );
    }

    public function testEdit(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        /** @var Gift $gift */
        $gift = $this->newEntity(
            Gift::ENTITY_TYPE,
            [
                'data' => [
                    'creator_id' => $jane->getId(),
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
                ]
            ]
        )->save();
        $jane->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/edit/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-edit.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertMatchesRegularExpression(
            '/(<div\s.*?class=".*?gift-edit-form[^"]*")+/',
            $response,
            'Cannot find gift form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertHtmlAttributeEquals(
            $gift->getLabel(),
            ['//input[@name="label"]', 'value'],
            $response
        );
    }

    /**
     * @group ticket-394
     */
    public function testEdit_notAllowed(): void {
        $this->disableGeefterAccessListeners();

        $johnGift = $this->faker()->randomElement(
            $this->getJohn()->getGeeftee()->getGiftCollection()->setLimit(5)->getItems()
        );

        $this->enableGeefterAccessListeners();
        $richard = $this->getRichard();
        $this->setGeefterInSession($richard);

        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('GET /gift/edit/gift_id/' . $johnGift->getId());
            $this->fail('Failed preventing unrelated geefter from accessing the edit page of a gift.');
        }
        catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }
    }

    public function testSave_valid_new_singleGeeftee(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->enableGeefterAccessListeners();

        $badgeIds = $this->faker()->randomElements(
            array_keys($this->getBadgeService()->getBadgesForEntityType(Gift::ENTITY_TYPE)),
            2
        );

        $giftData = [
            'geeftee_ids'     => [$jane->getGeeftee()->getId()],
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::ARCHIVED,
            'badge_ids'       => $badgeIds
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                implode("\n", $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR) ?: [''])
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        /** @var Gift $createdGift */
        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());
        foreach ($giftData as $k => $gd) {
            if (! in_array($k, ['geeftee_ids', 'badge_ids'])) {
                $this->assertEquals($gd, $createdGift->getData($k), $k);
            }
        }

        $this->assertEqualsCanonicalizing($giftData['geeftee_ids'], $createdGift->getGeefteeIds());
        $this->assertEqualsCanonicalizing(
            $badgeIds,
            array_keys($this->getBadgeService()->getBadges($createdGift))
        );
    }

    /**
     * @group ticket-267
     */
    public function testSave_valid_new_multipleGeeftees(): void {
        $jane = $this->getJane();
        $john = $this->getJohn();
        $this->setGeefterInSession($jane);

        $this->enableGeefterAccessListeners();

        $badgeIds = $this->faker()->randomElements(
            array_keys($this->getBadgeService()->getBadgesForEntityType(Gift::ENTITY_TYPE)),
            2
        );

        $giftData = [
            'geeftee_ids'     => [$jane->getGeeftee()->getId(), $john->getGeeftee()->getId()],
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::ARCHIVED,
            'badge_ids'       => $badgeIds
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                implode("\n", $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR) ?: [''])
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        /** @var Gift $createdGift */
        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());
        foreach ($giftData as $k => $gd) {
            if (! in_array($k, ['geeftee_ids', 'badge_ids'])) {
                $this->assertEquals($gd, $createdGift->getData($k), $k);
            }
        }

        $this->assertEqualsCanonicalizing($giftData['geeftee_ids'], $createdGift->getGeefteeIds());
        $this->assertEqualsCanonicalizing(
            $badgeIds,
            array_keys($this->getBadgeService()->getBadges($createdGift))
        );
    }

    public function testSave_valid_new_withImage(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->enableGeefterAccessListeners();

        $this->prepareTestImage(
            $imageId = $this->getGiftImageService()->makeImageId(true),
            'gift.jpg',
            Image::IMAGE_TYPE_TMP
        );
        $this->assertFileDoesNotExist($this->getGiftImageService()->getImagePath($imageId, Image::IMAGE_TYPE_FULL));

        $giftData = [
            'geeftee_ids' => $jane->getGeeftee()->getId(),
            'label'       => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'image_id'    => $imageId
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());
        foreach ($giftData as $k => $gd) {
            if ($k !== 'geeftee_ids') {
                $this->assertEquals($gd, $createdGift->getData($k), $k);
            }
        }

        $this->assertFileExists($this->getGiftImageService()->getImagePath($imageId, Image::IMAGE_TYPE_FULL));
    }

    /**
     * @group ticket-250
     */
    public function testSave_valid_new_withLists(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->enableGeefterAccessListeners();

        $giftList = $this->getEntity(GiftList::ENTITY_TYPE);

        $giftData = [
            'geeftee_ids'  => $jane->getGeeftee()->getId(),
            'label'        => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'giftlist_ids' => [$giftList->getId()]
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        /** @var Gift $createdGift */
        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());

        $actualGiftLists = $this->getContainer()->get(GiftList\Gift::class)
            ->getGiftListIds($createdGift);
        $this->assertEqualsCanonicalizing([$giftList->getId()], $actualGiftLists);
    }

    public function testSave_emptyLabel(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $giftData = [
            'label' => '',
            'description' => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority' => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status' => Gift\Field\Status::ARCHIVED
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on invalid form.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('gift/new', ['_query' => ['errors' => 1]]), $reroute->getUrl());
        }

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore, $janeGiftsCountAfter);
    }
}
