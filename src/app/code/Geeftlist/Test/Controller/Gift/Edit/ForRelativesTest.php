<?php

namespace Geeftlist\Test\Controller\Gift\Edit;


use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ForRelativesTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();

        $this->getContainer()->get(\Geeftlist\View\Grid\Gift::class)->reset();
    }

    public function testNew(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->enableGeefterAccessListeners();

        $this->mock('GET /gift/new/for/relatives');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-new.*?"/', $response, 'Cannot find body tag in response');
        $this->assertMatchesRegularExpression(
            '/(<div\s.*?class=".*?gift-edit-form[^"]*")+/',
            $response,
            'Cannot find gift form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        // Geeftee input must be empty
        $this->assertHtmlAttributeEquals(
            '',
            ['//input[@name="geeftee_ids"]', 'value'],
            $response
        );
    }

    public function testEdit(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        /** @var Gift $gift */
        $gift = $this->newEntity(
            Gift::ENTITY_TYPE,
            [
                'data' => [
                    'creator_id' => $jane->getId(),
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
                ]
            ]
        )->save();
        $john->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/edit/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-edit.*?"/', $response, 'Cannot find body tag in response');
        $this->assertMatchesRegularExpression(
            '/(<div\s.*?class=".*?gift-edit-form[^"]*")+/',
            $response,
            'Cannot find gift form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertHtmlAttributeEquals(
            $gift->getLabel(),
            ['//input[@name="label"]', 'value'],
            $response
        );
    }

    /**
     * @group ticket-267
     */
    public function testEdit_valid_multipleGeeftees(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        /** @var Gift $gift */
        $gift = $this->newEntity(
            Gift::ENTITY_TYPE,
            [
                'data' => [
                    'creator_id' => $jane->getId(),
                    'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
                ]
            ]
        )->save();
        $john->getGeeftee()->addGift($gift);
        $jane->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/edit/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?gift-edit.*?"/', $response, 'Cannot find body tag in response');
        $this->assertMatchesRegularExpression(
            '/(<div\s.*?class=".*?gift-edit-form[^"]*")+/',
            $response,
            'Cannot find gift form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertHtmlAttributeEquals(
            implode(',', [$john->getGeeftee()->getId(), $jane->getGeeftee()->getId()]),
            ['//input[@name="geeftee_ids"]', 'value'],
            $response
        );
        $this->assertHtmlAttributeEquals(
            $gift->getLabel(),
            ['//input[@name="label"]', 'value'],
            $response
        );
    }

    /**
     * @group ticket-394
     */
    public function testEdit_invalid_notAllowed(): void {
        $this->disableGeefterAccessListeners();

        $johnGift = $this->faker()->randomElement(
            $this->getJohn()->getGeeftee()->getGiftCollection()->setLimit(5)->getItems()
        );

        $this->enableGeefterAccessListeners();
        $richard = $this->getRichard();
        // The geefter must be in a family to avoir triggering the "you must join a family" error message
        $richardFamily = $this->newEntity(
            Family::ENTITY_TYPE,
            [
                'data' => [
                    'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                    'owner_id' => $richard->getId()
                ]
            ]
        )->save();
        $this->callPrivileged(
            static function () use ($richard, $richardFamily): void {
                $richardFamily->addGeeftee($richard->getGeeftee());
            }
        );

        $this->setGeefterInSession($richard);

        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('GET /gift/edit/gift_id/' . $johnGift->getId());
            $this->fail('Failed preventing unrelated geefter from accessing the edit page of a gift.');
        } catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }
    }

    public function testSave_valid_singleGeeftee_available(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $this->setGeefterInSession($jane);

        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Jane-Richard family',
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $john, $jane, $richard): void {
            $commonFamily->addGeeftee([
                $john->getGeeftee(),
                $jane->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $this->enableGeefterAccessListeners();

        $badgeIds = $this->faker()->randomElements(
            array_keys($this->getBadgeService()->getBadgesForEntityType(Gift::ENTITY_TYPE)),
            2
        );

        $giftData = [
            'geeftee_ids'     => $john->getGeeftee()->getId(),
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::AVAILABLE,
            'badge_ids'       => $badgeIds
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                implode("\n", $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR) ?: [''])
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        /** @var Gift $createdGift */
        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());
        foreach ($giftData as $k => $gd) {
            if (! in_array($k, ['geeftee_ids', 'badge_ids'])) {
                $this->assertEquals($gd, $createdGift->getData($k), $k);
            }
        }

        $this->assertEqualsCanonicalizing(
            $badgeIds,
            array_keys($this->getBadgeService()->getBadges($createdGift))
        );

        // Check that indexers have been run
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $jane,
            $createdGift,
            array_merge(TypeInterface::RELATIVE_ACTIONS, TypeInterface::MANAGE_ACTIONS),
        ));
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $richard,
            $createdGift,
            TypeInterface::RELATIVE_ACTIONS,
        ));
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $john,
            $createdGift,
            [],
        ));
    }

    public function testSave_valid_singleGeeftee_archived(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $this->setGeefterInSession($jane);

        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Jane-Richard family',
            'owner_id' => $john->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $john, $jane, $richard): void {
            $commonFamily->addGeeftee([
                $john->getGeeftee(),
                $jane->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $this->enableGeefterAccessListeners();

        $badgeIds = $this->faker()->randomElements(
            array_keys($this->getBadgeService()->getBadgesForEntityType(Gift::ENTITY_TYPE)),
            2
        );

        $giftData = [
            'geeftee_ids'     => $john->getGeeftee()->getId(),
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::ARCHIVED,
            'badge_ids'       => $badgeIds
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                implode("\n", $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR) ?: [''])
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        /** @var Gift $createdGift */
        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());
        foreach ($giftData as $k => $gd) {
            if (! in_array($k, ['geeftee_ids', 'badge_ids'])) {
                $this->assertEquals($gd, $createdGift->getData($k), $k);
            }
        }

        $this->assertEqualsCanonicalizing(
            $badgeIds,
            array_keys($this->getBadgeService()->getBadges($createdGift))
        );

        // Check that indexers have been run
        $unavailableAchivedActions = [
            TypeInterface::ACTION_RESERVE_PURCHASE,
            TypeInterface::ACTION_EDIT_RESERVATION,
            TypeInterface::ACTION_CANCEL_RESERVATION
        ];
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $jane,
            $createdGift,
            array_diff(
                array_merge(TypeInterface::RELATIVE_ACTIONS, TypeInterface::MANAGE_ACTIONS),
                $unavailableAchivedActions
            ),
        ));
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $richard,
            $createdGift,
            [], // No actions allowed on ARCHIVED gifts for relatives
        ));
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $john,
            $createdGift,
            [],
        ));
    }

    public function testSave_valid_multipleGeeftees(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $this->setGeefterInSession($jane);

        $janeRichardFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Jane-Richard family',
            'owner_id' => $jane->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($janeRichardFamily, $jane, $richard): void {
            $janeRichardFamily->addGeeftee([
                $jane->getGeeftee(),
                $richard->getGeeftee(),
            ]);
        });

        $this->enableGeefterAccessListeners();

        $badgeIds = $this->faker()->randomElements(
            array_keys($this->getBadgeService()->getBadgesForEntityType(Gift::ENTITY_TYPE)),
            2
        );

        $giftData = [
            'geeftee_ids'     => [$john->getGeeftee()->getId(), $richard->getGeeftee()->getId()],
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::AVAILABLE,
            'badge_ids'       => $badgeIds
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on valid form.');
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR),
                implode("\n", $this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR) ?: [''])
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
        }

        /** @var Gift $createdGift */
        $createdGift = $this->getEntity(
            Gift::ENTITY_TYPE,
            [
                'label' => $giftData['label']
            ]
        );

        $this->assertEquals($createdGift->getViewUrl(), $reroute->getUrl());

        $this->assertNotNull($createdGift);
        $this->assertNotNull($createdGift->getId());
        foreach ($giftData as $k => $gd) {
            if (! in_array($k, ['geeftee_ids', 'badge_ids'])) {
                $this->assertEquals($gd, $createdGift->getData($k), $k);
            }
        }

        $this->assertEqualsCanonicalizing($giftData['geeftee_ids'], $createdGift->getGeefteeIds());
        $this->assertEqualsCanonicalizing(
            $badgeIds,
            array_keys($this->getBadgeService()->getBadges($createdGift))
        );

        // Check that indexers have been run
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $jane,
            $createdGift,
            array_merge(TypeInterface::RELATIVE_ACTIONS, TypeInterface::MANAGE_ACTIONS),
        ));
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $richard,
            $createdGift,
            [],
        ));
        $this->assertTrue($this->getGeefterPermissionService()->isAllowed(
            $john,
            $createdGift,
            [],
        ));
    }

    public function testSave_invalid_emptyLabel(): void {
        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $johnGifts */
        $johnGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $johnGiftsCountBefore = $johnGifts
            ->addFieldToFilter('geeftee', $john->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $johnGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $giftData = [
            'geeftee_ids'     => $john->getGeeftee()->getId(),
            'label'           => '',
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::ARCHIVED
        ];

        try {
            $this->mock(
                'POST /gift/save',
                array_merge(
                    $giftData,
                    [
                        $this->getCsrfTokenName() => $this->getCsrfToken()
                    ]
                )
            );
            $this->fail('Failed rerouting on invalid form.');
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('gift/new', ['_query' => ['errors' => 1]]), $reroute->getUrl());
        }

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $johnGifts */
        $johnGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $johnGiftsCountAfter = $johnGifts
            ->addFieldToFilter('geeftee', $john->getGeeftee()->getId())
            ->count();
        $this->assertEquals($johnGiftsCountBefore, $johnGiftsCountAfter);
    }

    /**
     * @group ticket-394
     * @group ticket-403
     */
    public function testNew_valid_fromDuplicate(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);

        /** @var \Geeftlist\Model\Family $richardRichard2Family */
        $richardRichard2Family = $this->newEntity(
            Family::ENTITY_TYPE,
            [
                'data' => [
                    'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                    'owner_id' => $john->getId()
                ]
            ]
        )->save();
        $this->callPrivileged(
            static function () use ($richard, $richard2, $richardRichard2Family): void {
                $richardRichard2Family->addGeeftee(
                    [
                        $richard->getGeeftee(),
                        $richard2->getGeeftee()
                    ]
                );
            }
        );
        /** @var \Geeftlist\Model\Family $richard2Richard3Family */
        $richard2Richard3Family = $this->newEntity(
            Family::ENTITY_TYPE,
            [
                'data' => [
                    'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
                    'owner_id' => $john->getId()
                ]
            ]
        )->save();
        $this->callPrivileged(
            static function () use ($richard2, $richard3, $richard2Richard3Family): void {
                $richard2Richard3Family->addGeeftee(
                    [
                        $richard2->getGeeftee(),
                        $richard3->getGeeftee()
                    ]
                );
            }
        );
        $richard3GiftForRichard2 = current($this->generateGifts($richard3, $richard2->getGeeftee(), null, 1));
        $richard2GiftForRichard3 = current($this->generateGifts($richard2, $richard3->getGeeftee(), null, 1));

        $this->setGeefterInSession($richard);

        // Richard3's gift for Richard2 as Richard => OK (because Richard & Richard2 are related)
        $this->mock('GET /gift/new/copyOf/' . $richard3GiftForRichard2->getId());
        $response = $fw->get('RESPONSE');
        $this->assertValidHtmlWithoutErrors($response);

        $this->assertHtmlAttributeEquals(
            $richard->getI18n()->tr('Copy of {0}', $richard3GiftForRichard2->getLabel()),
            ['//input[@name="label"]', 'value'],
            $response
        );

        $this->clearCacheArrayObjects();

        // Richard2's gift for Richard3 as Richard => KO (because Richard & Richard3 are NOT related)
        try {
            $this->mock(
                'GET /gift/new/copyOf/' . $richard2GiftForRichard3->getId(),
                [
                    $this->getCsrfTokenName() => $this->getCsrfToken()
                ]
            );
            $this->fail('Failed rerouting on forbidden gift.');
        } catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }

        $this->assertEmpty($this->getGeefterSession()->getGiftForRelativesEditFormValues());
    }
}
