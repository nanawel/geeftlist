<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\UploadTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class UploadTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use UploadTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testPrepare_notLoggedIn(): void {
        try {
            $this->mock('POST /gift_upload/prepare');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testPrepare_loggedIn_noCsrf(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        $this->getFw()->set('FILE.image', [
            'name' => 'gift.jpg',
            'tmp_name' => $this->getFakeUploadedImagePath(),
            'size' => '42'
        ]);

        try {
            $this->mock('POST /gift_upload/prepare', [], [
                'Content-Type' => 'multipart/form-data'
            ]);
            $this->fail('Failed preventing image upload on missing CSRF token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }
    }

    public function testPrepare_httpPost(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->getFw()->set('FILES.image_id', [
            'name' => 'gift.jpg',
            'tmp_name' => $tmpName = $this->getFakeUploadedImagePath(),
            'size' => '42'
        ]);

        $this->assertFileExists($tmpName);

        $this->mock(
            'POST /gift_upload/prepare',
            [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ],
            [
                'Content-Type' => 'multipart/form-data'
            ]
        );

        $response = $fw->get('RESPONSE');
        $this->assertNotEmpty($response);
        $response = json_decode((string) $response, true);
        $this->assertIsArray($response);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('messages', $response);

        if ($response['status'] !== 'success') {
            $this->fail('Upload failed with errors: ' . implode(' | ', $response['messages'] ?? []));
        }

        $this->assertArrayHasKey('imageId', $response);
        $this->assertArrayHasKey('imageUrl', $response);
        $this->assertStringStartsWith($this->getAppConfig()->getValue('BASE_URL'), $response['imageUrl']);
    }

    public function testPrepare_url(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock(
            'POST /gift_upload/prepare',
            [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'image_id' => $this->getImagePlaceholderUrl(
                    $this->getAppConfig()->getValue('GIFT_IMAGE_MAX_WIDTH'),
                    $this->getAppConfig()->getValue('GIFT_IMAGE_MAX_HEIGHT')
                )
            ],
            [
                'Content-Type' => 'multipart/form-data'
            ]
        );

        $response = $fw->get('RESPONSE');
        $this->assertNotEmpty($response);
        $response = json_decode((string) $response, true);
        $this->assertIsArray($response);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('messages', $response);

        if ($response['status'] !== 'success') {
            $this->fail('Upload failed with errors: ' . implode(' | ', $response['messages'] ?? []));
        }

        $this->assertArrayHasKey('imageId', $response);
        $this->assertArrayHasKey('imageUrl', $response);
        $this->assertStringStartsWith($this->getAppConfig()->getValue('BASE_URL'), $response['imageUrl']);
    }

    public function testPrepare_url_invalidType(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock(
            'POST /gift_upload/prepare',
            [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'image_id' => $this->getInvalidImagePlaceholderUrl()
            ],
            [
                'Content-Type' => 'multipart/form-data'
            ]
        );

        $response = $fw->get('RESPONSE');
        $this->assertNotEmpty($response);
        $response = json_decode((string) $response, true);
        $this->assertIsArray($response);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('messages', $response);

        $this->assertEquals('failure', $response['status']);
        $this->assertArrayNotHasKey('imageId', $response);
        $this->assertArrayNotHasKey('imageUrl', $response);
    }

    public function testPrepare_url_unsupportedType(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock(
            'POST /gift_upload/prepare',
            [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'image_id' => $this->getImagePlaceholderUrl(
                    $this->getAppConfig()->getValue('GIFT_IMAGE_MAX_WIDTH') / 2,
                    $this->getAppConfig()->getValue('GIFT_IMAGE_MAX_HEIGHT') / 2,
                    'svg'
                )
            ],
            [
                'Content-Type' => 'multipart/form-data'
            ]
        );

        $response = $fw->get('RESPONSE');
        $this->assertNotEmpty($response);
        $response = json_decode((string) $response, true);
        $this->assertIsArray($response);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('messages', $response);

        $this->assertEquals('failure', $response['status']);
        $this->assertArrayNotHasKey('imageId', $response);
        $this->assertArrayNotHasKey('imageUrl', $response);
    }
}
