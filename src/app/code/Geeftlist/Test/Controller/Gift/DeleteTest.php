<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class DeleteTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    use GiftTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testConfirmDelete_invalidGift(): void {
        $richard = $this->getRichard();
        $gift = current($this->generateGifts($richard, $richard->getGeeftee(), null, 1));

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        try {
            $this->mock('GET /gift/confirmDelete/gift_id/' . $gift->getId());
            $this->fail('Failed preventing access to the delete confirm dialog for an invalid gift.');
        } catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }
    }

    public function testConfirmDelete_validGift(): void {
        $fw = $this->getFw();

        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
            'creator_id' => $john->getId()
        ])->getFirstItem();

        $this->setGeefterInSession($john);

        $this->mock('GET /gift/confirmDelete/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');

        $this->assertValidHtmlWithoutErrors($response);
        $this->assertHtmlAttributeEquals(
            $gift->getViewUrl(),
            ['//a[@class="gift-page-link"]', 'href'],
            $response,
            'Wrong gift URL (or cannot find gift box in the page?)'
        );
        $this->assertHtmlElementExists(
            sprintf('//a[starts-with(@href, "%s")]', $this->getUrl('gift/delete/gift_id/' . $gift->getId())),
            $response,
            'Missing deletion link in the page.'
        );
    }

    public function testDelete_invalidGift(): void {
        $richard = $this->getRichard();
        $gift = current($this->generateGifts($richard, $richard->getGeeftee(), null, 1));

        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        try {
            $this->mock(sprintf(
                'GET /gift/delete/gift_id/%d?%s=%s',
                $gift->getId(),
                $this->getCsrfTokenName(),
                $this->getCsrfToken()
            ));
            $this->fail('Failed preventing deletion with an invalid gift.');
        } catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testDelete_validGift_invalidCsrf(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, ['creator_id' => $john->getId()])
            ->getFirstItem();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        $this->setGeefterInSession($john);

        try {
            $this->mock(sprintf(
                'GET /gift/delete/gift_id/%d?%s=%s',
                $gift->getId(),
                $this->getCsrfTokenName(),
                'foo'
            ));
            $this->fail('Failed preventing deletion with missing CSRF token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $this->assertEmpty($this->getEmailSender()->getSentEmails());
    }

    public function testDelete_validGift(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $john->getId(),
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $this->getJane()->getGeeftee()->addGift($gift);

        $giftId = $gift->getId();

        $this->assertNotNull($giftId);
        $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
        $this->assertEquals(Gift\Field\Status::AVAILABLE, $gift->getStatus());

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->setGeefterInSession($john);

        try {
            $this->mock(sprintf(
                'GET /gift/delete/gift_id/%d?%s=%s',
                $gift->getId(),
                $this->getCsrfTokenName(),
                $this->getCsrfToken()
            ));
        }
        catch (Reroute $reroute) {
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());
        }

        $this->disableGeefterAccessListeners();

        $gift = $this->getEntity(Gift::ENTITY_TYPE, $giftId);
        $this->assertEquals(Gift\Field\Status::DELETED, $gift->getStatus());
    }
}
