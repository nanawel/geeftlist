<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class MassAddTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-638
     */
    public function testMassAdd_notLoggedIn(): void {
        $gift = $this->getEntity(Gift::ENTITY_TYPE);
        $this->assertNotNull($gift);
        $this->assertNotNull($gift->getId());

        try {
            $this->mock('GET /gift/massAdd');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    /**
     * @group ticket-638
     */
    public function testMassAdd_loggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/massAdd');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-massadd.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        // Just check there's no errors when rendering the page
        $this->assertValidHtmlWithoutErrors($response);
    }
}
