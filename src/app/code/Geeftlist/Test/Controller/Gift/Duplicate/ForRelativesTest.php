<?php

namespace Geeftlist\Test\Controller\Gift\Duplicate;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ForRelativesTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
        $this->clearCacheArrayObjects();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();

        $this->getContainer()->get(\Geeftlist\View\Grid\Gift::class)->reset();
    }

    public function testDuplicate_missingCsrf(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $aGiftForJohn = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('creator_id', ['ne' => $jane->getId()])
            ->addFieldToFilter('geeftee', $john->getGeeftee())
            ->getFirstItem();

        $this->assertNotNull($aGiftForJohn);

        $this->setGeefterInSession($jane);

        try {
            $this->mock(sprintf('GET /gift/duplicate/gift_id/%d', $aGiftForJohn->getId()));
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());
        }
    }

    public function testDuplicate_valid(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $aGiftForJohn = $this->getEntityCollection(Gift::ENTITY_TYPE)
            ->addFieldToFilter('creator_id', ['ne' => $jane->getId()])
            ->addFieldToFilter('geeftee', $john->getGeeftee())
            ->getFirstItem();

        $this->assertNotNull($aGiftForJohn);

        $this->setGeefterInSession($jane);

        try {
            $this->mock(
                sprintf('GET /gift/duplicate/gift_id/%d', $aGiftForJohn->getId()),
                [$this->getCsrfTokenName() => $this->getCsrfToken()]
        );
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertStringEndsWith(
                $this->getUrl('gift/new', ['copyOf' => $aGiftForJohn->getId()]),
                $reroute->getUrl()
            );
        }
    }

    public function testDuplicateArchive_missingCsrf(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $aGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($aGiftForJohn);

        // Reload from database
        /** @var Gift $aGiftForJohn */
        $aGiftForJohn = $this->getRepository(Gift::ENTITY_TYPE)
            ->get($aGiftForJohn->getId());

        $this->assertEquals(Gift\Field\Status::AVAILABLE, $aGiftForJohn->getStatus());

        $this->setGeefterInSession($jane);

        try {
            // Valid gift, but missing CSRF token in URL
            $this->mock(
                sprintf('GET /gift/duplicateArchive/gift_id/%d', $aGiftForJohn->getId())
            );
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertEquals($this->getUrl('/'), $reroute->getUrl());

            /** @var Gift $aGiftForJohn */
            $aGiftForJohn = $this->getRepository(Gift::ENTITY_TYPE)
                ->get($aGiftForJohn->getId());
            $this->assertNotEquals(Gift\Field\Status::ARCHIVED, $aGiftForJohn->getStatus());
        }
    }

    /**
     * @group ticket-523
     */
    public function testDuplicateArchive_valid(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        $aGiftForJohn = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($aGiftForJohn);

        // Reload from database
        /** @var Gift $aGiftForJohn */
        $aGiftForJohn = $this->getRepository(Gift::ENTITY_TYPE)
            ->get($aGiftForJohn->getId());

        $this->assertEquals(Gift\Field\Status::AVAILABLE, $aGiftForJohn->getStatus());

        $this->setGeefterInSession($jane);

        try {
            $this->mock(
                sprintf('GET /gift/duplicateArchive/gift_id/%d', $aGiftForJohn->getId()),
                [$this->getCsrfTokenName() => $this->getCsrfToken()]
            );
        } catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_SUCCESS));
            $this->assertStringEndsWith(
                $this->getUrl('gift/new', ['copyOf' => $aGiftForJohn->getId()]),
                $reroute->getUrl()
            );

            /** @var Gift $aGiftForJohn */
            $aGiftForJohn = $this->getRepository(Gift::ENTITY_TYPE)
                ->get($aGiftForJohn->getId());
            $this->assertEquals(Gift\Field\Status::ARCHIVED, $aGiftForJohn->getStatus());
        }
    }
}
