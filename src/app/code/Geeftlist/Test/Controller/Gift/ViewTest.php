<?php

namespace Geeftlist\Test\Controller\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Forward;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class ViewTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testView_notLoggedIn(): void {
        $gift = $this->getEntity(Gift::ENTITY_TYPE);
        $this->assertNotNull($gift);
        $this->assertNotNull($gift->getId());

        try {
            $this->mock('GET /gift/view/gift_id/' . $gift->getId());
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testView_loggedIn(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $john = $this->getJohn();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $john->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $john->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->mock('GET /gift/view/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-view.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testView_loggedIn_noFamily(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'creator_id' => $richard->getId(),
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $richard->getGeeftee()->addGift($gift);

        $this->enableGeefterAccessListeners();
        $this->setGeefterInSession($richard);

        $this->mock('GET /gift/view/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?gift-view.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testView_loggedIn_notAllowed(): void {
        $this->disableGeefterAccessListeners();

        $johnGift = $this->faker()->randomElement(
            $this->getJohn()->getGeeftee()->getGiftCollection()->setLimit(5)->getItems()
        );

        $this->enableGeefterAccessListeners();
        $richard = $this->getRichard();
        $this->setGeefterInSession($richard);

        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('GET /gift/view/gift_id/' . $johnGift->getId());
            $this->fail('Failed preventing unrelated geefter from seeing a gift.');
        }
        catch (Forward $forward) {
            $this->assertEquals('GET /gift/notfound', $forward->getRoute());
            $this->assertEmpty($this->getResponse()->getBody());
        }
    }

    /**
     * @group ticket-632
     */
    public function testView_postMentionRecipients_enabledByConfig(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gift = current($this->generateGifts($richard, $richard2, null, 1));
        $this->generateFamily($richard, [$richard2]);

        $this->getAppConfig()->setValue('GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION', true);

        $this->setGeefterInSession($richard);

        $this->mock('GET /gift/view/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<input[^>]+name="recipient_ids"/m',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    /**
     * @group ticket-632
     */
    public function testView_postMentionRecipients_disabledByConfig(): void {
        $fw = $this->getFw();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gift = current($this->generateGifts($richard, $richard2, null, 1));
        $this->generateFamily($richard, [$richard2]);

        $this->getAppConfig()->setValue('GIFT_ENABLE_DISCUSSION_POST_NOTIFICATION', false);

        $this->setGeefterInSession($richard);

        $this->mock('GET /gift/view/gift_id/' . $gift->getId());
        $response = $fw->get('RESPONSE');
        $this->assertDoesNotMatchRegularExpression(
            '/<input[^>]+name="recipient_ids"/m',
            $response,
            'Unexpected tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }
}
