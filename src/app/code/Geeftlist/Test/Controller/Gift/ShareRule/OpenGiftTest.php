<?php

namespace Geeftlist\Test\Controller\Gift\ShareRule;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use Geeftlist\Model\Share\Gift\RuleType\OpenGiftType;
use Geeftlist\Model\Share\Gift\RuleType\RegularType;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class OpenGiftTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-403
     */
    public function testEdit_enabled(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $newGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'           => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::AVAILABLE,
            Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                ['rule_type' => RegularType::CODE],
                ['rule_type' => OpenGiftType::CODE]
            ]
        ]])->save();
        $this->getJohn()->getGeeftee()->addGift($newGift);

        $newGift = $this->getEntity(Gift::ENTITY_TYPE, $newGift->getId());

        $this->mock('GET /gift/edit/gift_id/' . $newGift->getId());
        $response = $fw->get('RESPONSE');

        $this->assertHtmlAttributeEquals(
            'checked',
            [
                sprintf(
                    '//input[starts-with(@name, "%s") and @id="open_gift"]',
                    Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY
                ),
                'checked'
            ],
            $response,
            'Cannot find checked input for "Open gift" setting in the form.'
        );
    }

    public function testSave_validForm_enabled(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $giftData = [
            'geeftee_ids'     => $jane->getGeeftee()->getId(),
            'label'           => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::AVAILABLE,
            Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                ['rule_type' => OpenGiftType::CODE]
            ]
        ];

        try {
            $this->mock('POST /gift/save', array_merge($giftData, [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]));
            $this->fail('Failed rerouting after save.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
        }

        /** @var Gift $newGift */
        $newGift = $this->getEntity(Gift::ENTITY_TYPE, [
            'label' => $giftData['label']
        ]);

        $this->assertEquals($newGift->getViewUrl(), $reroute->getUrl());

        $this->assertTrue($this->getContainer()->get(\Geeftlist\Helper\Gift::class)->isOpenGift($newGift));

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore + 1, $janeGiftsCountAfter);
    }

    public function testSave_validForm_disabled(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $giftData = [
            'geeftee_ids'     => $jane->getGeeftee()->getId(),
            'label'           => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(2, true),
            'estimated_price' => '12.34',
            'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
            'status'          => Gift\Field\Status::AVAILABLE
        ];

        try {
            $this->mock('POST /gift/save', array_merge($giftData, [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]));
            $this->fail('Failed rerouting after save.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
        }

        /** @var Gift $newGift */
        $newGift = $this->getEntity(Gift::ENTITY_TYPE, [
            'label' => $giftData['label']
        ]);

        $this->assertEquals($newGift->getViewUrl(), $reroute->getUrl());

        $this->assertFalse($this->getContainer()->get(\Geeftlist\Helper\Gift::class)->isOpenGift($newGift));

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore + 1, $janeGiftsCountAfter);
    }
}
