<?php

namespace Geeftlist\Test\Controller\Gift\ShareRule;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Entity\Rule;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class GeefterTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-403
     */
    public function testSave_invalidForm(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $allGeefterIds = $this->getEntityCollection(Geefter::ENTITY_TYPE)->getAllIds();
        $allJaneRelatedGeefterIds = $this->getEntityCollection(Geefter::ENTITY_TYPE)
            ->addFieldToFilter('related_geefter', $jane->getId())
            ->getAllIds();
        $allOtherGeefterIds = array_diff($allGeefterIds, $allJaneRelatedGeefterIds);

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $invalidFamilyShareRuleTestData = [
            'Missing params' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
            ]],
            'Missing geefter_ids param key' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
                'params' => [
                    $this->faker()->randomElement($allOtherGeefterIds)
                ]
            ]],
            'Empty geefter_ids param' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
                'params' => [
                    'geefter_ids' => []
                ]
            ]],
            'Unrelated geefter_ids IDs' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
                'params' => [
                    'geefter_ids' => [
                        $this->faker()->randomElement($allOtherGeefterIds)
                    ]
                ]
            ]],
        ];

        foreach ($invalidFamilyShareRuleTestData as $testLabel => $testData) {
            $giftData = [
                'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'description'     => $this->faker()->paragraphs(2, true),
                'estimated_price' => '12.34',
                'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
                'status'          => Gift\Field\Status::AVAILABLE,
                Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => $testData
            ];

            try {
                $this->mock('POST /gift/save', array_merge($giftData, [
                    $this->getCsrfTokenName() => $this->getCsrfToken()
                ]));
                $this->fail(sprintf('Failed rerouting on invalid geefter share rule configuration: %s.', $testLabel));
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertNotEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    'Test case: ' . $testLabel
                );
                $this->assertEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS),
                    'Test case: ' . $testLabel
                );
                $this->assertStringEndsWith(
                    $this->getUrl('gift/new', ['_query' => ['errors' => 1]]),
                    $reroute->getUrl(),
                    'Test case: ' . $testLabel
                );
            }
        }

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore, $janeGiftsCountAfter);
    }

    public function testSave_validForm(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $allJaneRelatedGeefterIds = $this->getEntityCollection(Geefter::ENTITY_TYPE)
            ->addFieldToFilter('related_geefter', $jane->getId())
            ->getAllIds();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $validFamilyShareRuleTestData = [
            'One geefter ID' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
                'params' => [
                    'geefter_ids' => [
                        $this->faker()->randomElement($allJaneRelatedGeefterIds)
                    ]
                ]
            ]],
            'Several geefter IDs' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\GeefterType::CODE,
                'params' => [
                    'geefter_ids' => $this->faker()->randomElements($allJaneRelatedGeefterIds, 3)
                ]
            ]],
        ];

        foreach ($validFamilyShareRuleTestData as $testLabel => $testData) {
            $giftData = [
                'geeftee_ids'     => $jane->getGeeftee()->getId(),
                'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'description'     => $this->faker()->paragraphs(2, true),
                'estimated_price' => '12.34',
                'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
                'status'          => Gift\Field\Status::AVAILABLE,
                Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => $testData
            ];

            try {
                $this->mock('POST /gift/save', array_merge($giftData, [
                    $this->getCsrfTokenName() => $this->getCsrfToken()
                ]));
                $this->fail('Failed rerouting after save.');
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    'Test case: ' . $testLabel
                );
                $this->assertNotEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS),
                    'Test case: ' . $testLabel
                );
            }

            /** @var Gift $newGift */
            $newGift = $this->getEntity(Gift::ENTITY_TYPE, [
                'label' => $giftData['label']
            ]);

            $this->assertEquals($newGift->getViewUrl(), $reroute->getUrl(), 'Test case: ' . $testLabel);

            $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
                'target_entity_type' => $newGift->getEntityType(),
                'target_entity_id' => $newGift->getId(),
            ]);
            $this->assertCount(1, $shareRules);
            $this->assertEquals(
                $testData[0],
                array_intersect_key($shareRules->getFirstItem()->getData(), $testData[0])
            );
        }

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore + count($validFamilyShareRuleTestData), $janeGiftsCountAfter);
    }
}
