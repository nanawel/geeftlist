<?php

namespace Geeftlist\Test\Controller\Gift\ShareRule;


use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Entity\Rule;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class FamilyTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    /**
     * @group ticket-403
     */
    public function testSave_invalidForm(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $allFamilyIds = $this->getEntityCollection(Family::ENTITY_TYPE)->getAllIds();
        $allJaneFamilyIds = $jane->getFamilyCollection()->getAllIds();
        $allOtherFamilyIds = array_diff($allFamilyIds, $allJaneFamilyIds);

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $invalidFamilyShareRuleTestData = [
            'Missing params' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
            ]],
            'Missing family_ids param key' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
                'params' => [
                    $this->faker()->randomElement($allOtherFamilyIds)
                ]
            ]],
            'Empty family_ids param' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
                'params' => [
                    'family_ids' => []
                ]
            ]],
            'Unrelated family IDs' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
                'params' => [
                    'family_ids' => [
                        $this->faker()->randomElement($allOtherFamilyIds)
                    ]
                ]
            ]],
        ];

        foreach ($invalidFamilyShareRuleTestData as $testLabel => $testData) {
            $giftData = [
                'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'description'     => $this->faker()->paragraphs(2, true),
                'estimated_price' => '12.34',
                'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
                'status'          => Gift\Field\Status::AVAILABLE,
                Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => $testData
            ];

            try {
                $this->mock('POST /gift/save', array_merge($giftData, [
                    $this->getCsrfTokenName() => $this->getCsrfToken()
                ]));
                $this->fail(sprintf('Failed rerouting on invalid family share rule configuration: %s.', $testLabel));
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertNotEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    'Test case: ' . $testLabel
                );
                $this->assertEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS),
                    'Test case: ' . $testLabel
                );
                $this->assertStringEndsWith(
                    $this->getUrl('gift/new', ['_query' => ['errors' => 1]]),
                    $reroute->getUrl(),
                    'Test case: ' . $testLabel
                );
            }
        }

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore, $janeGiftsCountAfter);
    }

    public function testSave_validForm(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $allJaneFamilyIds = $jane->getFamilyCollection()->getAllIds();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountBefore = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertGreaterThan(0, $janeGiftsCountBefore);

        $this->enableGeefterAccessListeners();

        $validFamilyShareRuleTestData = [
            'One family ID' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
                'params' => [
                    'family_ids' => [
                        $this->faker()->randomElement($allJaneFamilyIds)
                    ]
                ]
            ]],
            'Several family IDs' => [[
                'rule_type' => \Geeftlist\Model\Share\Gift\RuleType\FamilyType::CODE,
                'params' => [
                    'family_ids' => $this->faker()->randomElements($allJaneFamilyIds, 3)
                ]
            ]],
        ];

        foreach ($validFamilyShareRuleTestData as $testLabel => $testData) {
            $giftData = [
                'geeftee_ids'     => [$jane->getGeeftee()->getId()],
                'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'description'     => $this->faker()->paragraphs(2, true),
                'estimated_price' => '12.34',
                'priority'        => Gift\Field\Priority::PRIORITY_MEDIUM,
                'status'          => Gift\Field\Status::AVAILABLE,
                Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => $testData
            ];

            try {
                $this->mock('POST /gift/save', array_merge($giftData, [
                    $this->getCsrfTokenName() => $this->getCsrfToken()
                ]));
                $this->fail('Failed rerouting after save.');
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    'Test case: ' . $testLabel
                );
                $this->assertNotEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS),
                    'Test case: ' . $testLabel
                );
            }

            /** @var Gift $newGift */
            $newGift = $this->getEntity(Gift::ENTITY_TYPE, [
                'label' => $giftData['label']
            ]);

            $this->assertEquals($newGift->getViewUrl(), $reroute->getUrl(), 'Test case: ' . $testLabel);

            $shareRules = $this->getEntityCollection(Rule::ENTITY_TYPE, [
                'target_entity_type' => $newGift->getEntityType(),
                'target_entity_id' => $newGift->getId(),
            ]);
            $this->assertCount(1, $shareRules);
            $this->assertEquals(
                $testData[0],
                array_intersect_key($shareRules->getFirstItem()->getData(), $testData[0])
            );
        }

        $this->disableGeefterAccessListeners();

        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $janeGifts */
        $janeGifts = $this->getEntityCollection(Gift::ENTITY_TYPE);
        $janeGiftsCountAfter = $janeGifts
            ->addFieldToFilter('geeftee', $jane->getGeeftee()->getId())
            ->count();
        $this->assertEquals($janeGiftsCountBefore + count($validFamilyShareRuleTestData), $janeGiftsCountAfter);
    }
}
