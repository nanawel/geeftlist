<?php

namespace Geeftlist\Test\Controller;

use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class AboutTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        $fw = $this->getFw();

        $this->mock('GET /about');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?about-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $this->setGeefterInSession($this->getJane());

        $this->mock('GET /about');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<body\s.*?class=".*?about-index.*?"/',
            $response,
            'Cannot find body tag in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }
}
