<?php

namespace Geeftlist\Test\Controller;





use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class DashboardTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
        $this->setUpControllerTest();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->tearDownControllerTest();
    }

    public function testIndexNotLoggedIn(): void {
        try {
            $this->mock('GET /dashboard');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndexLoggedIn(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $jane->setLanguage('en')
            ->save();

        $this->setGeefterInSession($jane);

        $this->mock('GET /dashboard');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?dashboard-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
        $this->assertMatchesRegularExpression(
            sprintf('#<h2>.*? %s</h2>#', preg_quote($this->getJane()->getUsername())),
            $response,
            'Cannot find username in header'
        );
        $this->assertMatchesRegularExpression('#<h2>Hello .*?</h2>#', $response, 'Cannot find localized header in response');

        $jane->setLanguage('fr')
            ->save();
        $this->setGeefterInSession($jane);

        $this->mock('GET /dashboard');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?dashboard-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
        $this->assertMatchesRegularExpression(
            sprintf('#<h2>.*? %s</h2>#', preg_quote($this->getJane()->getUsername())),
            $response,
            'Cannot find username in header'
        );
        $this->assertMatchesRegularExpression('#<h2>Bonjour .*?</h2>#', $response, 'Cannot find localized header in response');
    }

    public function testIndexLoggedIn_noFamily(): void {
        $fw = $this->getFw();

        $this->setGeefterInSession($this->getRichard());

        $this->mock('GET /dashboard');
        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression('/<body\s.*?class=".*?dashboard-index.*?"/', $response, 'Cannot find body tag in response');
        $this->assertValidHtmlWithoutErrors($response);
    }
}
