<?php

namespace Geeftlist\Test\Controller;


use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use OliveOil\Core\Model\Session;
use OliveOil\Core\Model\SessionInterface;
use OliveOil\Core\Test\Util\ControllerTestCaseTrait;
use OliveOil\Core\Test\Util\Exception\Reroute;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group controllers
 */
class AccountTest extends TestCase
{
    use FakerTrait;
    use ControllerTestCaseTrait;
    /** @var bool */
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->initFixedGeefters();

        $this->getGeefterSession()
            ->invalidate()
            ->clearMessages();
        $this->getSession()->invalidate();
        $this->setUpControllerTest();
        $this->enableGeefterAccessListeners();


        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', true);
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
        $this->getSession()->invalidate();
        $this->tearDownControllerTest();

        $this->resetJane();
        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    public function testIndex_notLoggedIn(): void {
        try {
            $this->mock('GET /account');
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testIndex_loggedIn(): void {
        $fw = $this->getFw();

        $this->setGeefterInSession($this->getJane());

        $this->mock('GET /account');

        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<form\s.*?name="account-form"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);

        $this->setGeefterInSession($this->getRichard());

        $this->mock('GET /account');

        $response = $fw->get('RESPONSE');
        $this->assertMatchesRegularExpression(
            '/<form\s.*?name="account-form"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testUpdate_missingToken(): void {
        $richard = $this->getRichard();
        $username = $richard->getUsername();
        $this->setGeefterInSession($richard);

        $newUsername = uniqid('Richard_');

        try {
            $this->mock('POST /account/update', [
                'username' => $newUsername
            ]);
            $this->fail('Failed rerouting on missing token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $this->assertEquals(
            $username,
            $this->getRichard(false)->getUsername()
        );
    }

    public function testUpdate_invalidToken(): void {
        $richard = $this->getRichard();
        $username = $richard->getUsername();
        $this->setGeefterInSession($richard);

        $newUsername = uniqid('Richard_');

        try {
            $this->mock('POST /account/update', [
                'username' => $newUsername,
                $this->getCsrfTokenName() => 'azerty'
            ]);
            $this->fail('Failed rerouting on invalid token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $this->assertEquals(
            $username,
            $this->getRichard(false)->getUsername()
        );
    }

    public function testUpdate_missingFields(): void {
        $jane = $this->getJane();
        $janeUsername = $jane->getUsername();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on empty form.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertEquals($janeUsername, $this->getJane()->getUsername());
        }
    }

    public function testUpdate_invalidUsername(): void {
        $jane = $this->getJane();
        $janeUsername = $jane->getUsername();
        $this->setGeefterInSession($jane);

        $invalidUsernames = [
            'azerty?',              // "?" is forbidden
            'azer?ty',              // "?" is forbidden
            str_repeat('a',  65),   // Max length is 64
            'ab',                   // Min length is 3
            'azerty$',              // "$" is forbidden
            '.azerty',              // Must start with a letter
            '12345',                // Must start with a letter
            '123ab'                 // Must start with a letter
        ];

        foreach ($invalidUsernames as $invalidUsername) {
            try {
                $this->mock('POST /account/update', [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'username' => $invalidUsername
                ]);
                $this->fail('Failed rerouting on invalid username');
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertNotEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    sprintf('Failed asserting that the username "%s" is invalid', $invalidUsername)
                );
                $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
                $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());
                $this->assertEquals($janeUsername, $this->getJane()->getUsername());
            }

            $this->getGeefterSession()->clearMessages();
        }
    }

    public function testUpdate_username_valid(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $validUsernames = [
            uniqid('Jane_'),
            'azerty',
            'azerty.com',
            'Mr Robot',
            'john@home',
            str_repeat('a', 64),
            'Lînus',
            'Törvalds',
            'Àzèrty'
        ];

        foreach ($validUsernames as $validUsername) {
            try {
                $this->mock('POST /account/update', [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'username' => $validUsername
                ]);
                $this->fail('Failed rerouting on valid username.');
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
                );
                $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
                $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

                $jane = $this->getJane();
                $this->assertEquals($validUsername, $jane->getUsername());
                $this->assertEquals($validUsername, $jane->getGeeftee()->getName());
            }

            $this->getGeefterSession()->clearMessages();
        }
    }

    /**
     * @group fix-20231104
     */
    public function testUpdate_email_invalid(): void {
        $fakeEmailSender = $this->getEmailSender();

        $jane = $this->getJane();
        $janeEmail = $jane->getEmail();
        $this->setGeefterInSession($jane);

        $invalidEmails = [
            //'jane+@gmail.com',   // Not invalid anymore since PHP 8.2+
            'jane.@gmail.com',
            'j@ne@gmail.com',
            //'j*ne@gmail.com',    // Not invalid anymore since PHP 8.2+
            //'j$ne@gmail.com',    // Not invalid anymore since PHP 8.2+
            'test@localhost',
            'jane.doe@' . Constants::SAMPLEDATA_EMAIL_DOMAIN,   // Invalid MX
        ];

        $appConfig_skipEmailCheckMx = $this->getAppConfig()->getValue('SKIP_EMAIL_CHECK_MX');
        $this->getAppConfig()->setValue('SKIP_EMAIL_CHECK_MX', 0);
        try {
            foreach ($invalidEmails as $invalidEmail) {
                $this->assertNotEquals($invalidEmail, $this->getJane()->getEmail());

                try {
                    $this->mock('POST /account/update', [
                        $this->getCsrfTokenName() => $this->getCsrfToken(),
                        'email' => $invalidEmail,
                        'current_password' => Constants::GEEFTER_JANE_PASSWORD
                    ]);
                    $this->fail('Failed rerouting on invalid email: ' . $invalidEmail);
                }
                catch (Reroute $reroute) {
                    $this->handleReroute($reroute);
                    $this->assertNotEmpty(
                        $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                        sprintf('Failed asserting that the email "%s" is invalid', $invalidEmail)
                    );
                    $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
                    $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

                    $jane = $this->getJane();
                    $this->assertEquals($janeEmail, $jane->getEmail());
                    $this->assertEquals($janeEmail, $jane->getGeeftee()->getEmail());
                    $this->assertEmpty($fakeEmailSender->getSentEmails());
                }

                $this->getGeefterSession()->clearMessages();
            }
        } finally {
            $this->getAppConfig()->setValue('SKIP_EMAIL_CHECK_MX', $appConfig_skipEmailCheckMx);
        }
    }

    public function testUpdate_email_missingPassword(): void {
        $fakeEmailSender = $this->getEmailSender();

        $jane = $this->getJane();
        $janeEmail = $jane->getEmail();
        $newEmail = 'janedoe@gmail.com';
        $this->setGeefterInSession($jane);

        $this->assertNotEquals($janeEmail, $newEmail);

        try {
            $this->mock('POST /account/update', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'email' => $newEmail
            ]);
            $this->fail('Failed rerouting on valid email without password.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertEquals($janeEmail, $jane->getEmail());
            $this->assertEquals($janeEmail, $jane->getGeeftee()->getEmail());
            $this->assertEmpty($fakeEmailSender->getSentEmails());
        }
    }

    public function testUpdate_email_valid(): void {
        $fakeEmailSender = $this->getEmailSender();

        /** @var Geefter $jane */
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $validEmails = [
            'jane+doe@gmail.com',
            'a@gmail.com',
            'azerty123@gmail.com',
            'jane-doe@gmail.com',
            'jane.doe@gmail.com'
        ];

        foreach ($validEmails as $validEmail) {
            $jane = $this->getJane();
            $this->assertNull($jane->getPendingEmail());
            $this->assertEquals(Constants::GEEFTER_JANE_EMAIL, $jane->getEmail());
            $this->assertEquals(Constants::GEEFTER_JANE_EMAIL, $jane->getGeeftee()->getEmail());

            try {
                $this->mock('POST /account/update', [
                    $this->getCsrfTokenName() => $this->getCsrfToken(),
                    'email' => $validEmail,
                    'current_password' => Constants::GEEFTER_JANE_PASSWORD
                ]);
                $this->fail('Failed rerouting on valid email.');
            }
            catch (Reroute $reroute) {
                $this->handleReroute($reroute);
                $this->assertEmpty(
                    $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                    current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
                );
                $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
                $this->assertStringEndsWith($this->getUrl('account/updateSuccess'), $reroute->getUrl());

                $jane = $this->getJane();
                $this->assertEquals($validEmail, $jane->getPendingEmail());

                $this->assertNotEmpty($fakeEmailSender->getSentEmails());
                $email = current($fakeEmailSender->getSentEmails(true));
                $this->assertEquals($validEmail, $email['to']);
            }

            $this->getGeefterSession()->clearMessages();
            $this->resetJane();
        }
    }

    public function testCancelEmailUpdate(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        // No pending email update
        $this->setGeefterInSession($john);
        try {
            $this->mock('GET /account/cancelEmailUpdate');
            $this->fail('Failed rerouting on cancel email update.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());
        }

        $this->disableGeefterAccessListeners();

        // Valid pending email
        $jane->setPendingEmail('jandoe@gmail.com')
            ->setPendingEmailToken('test-token')
            ->save();

        $this->setGeefterInSession($jane);
        $this->enableGeefterAccessListeners();

        try {
            $this->mock('GET /account/cancelEmailUpdate');
            $this->fail('Failed rerouting on cancel email update.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertNull($jane->getPendingEmail());
            $this->assertNull($jane->getPendingEmailToken());
        }
    }

    public function testConfirmEmailUpdateNotLoggedIn(): void {
        $fw = $this->getFw();

        $this->disableGeefterAccessListeners();

        $jane = $this->getJane()
            ->setPendingEmail('janedoe@gmail.com')
            ->generateUpdateEmailToken()
            ->save();

        $this->enableGeefterAccessListeners();

        $this->mock('GET /account/confirmEmailUpdate/token/' . $jane->getPendingEmailToken());
        $response = $fw->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<form\s.*?name="account-updateemail-confirm-form"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testConfirmEmailUpdate_loggedIn(): void {
        $this->disableGeefterAccessListeners();

        $jane = $this->getJane()
            ->setPendingEmail('janedoe@gmail.com')
            ->generateUpdateEmailToken()
            ->save();

        $this->setGeefterInSession($this->getJohn());

        $this->enableGeefterAccessListeners();

        $this->mock('GET /account/confirmEmailUpdate/token/' . $jane->getPendingEmailToken());
        $response = $this->getFw()->get('RESPONSE');

        $this->assertMatchesRegularExpression(
            '/<form\s.*?name="account-updateemail-confirm-form"/',
            $response,
            'Cannot find form in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testConfirmEmailUpdate_invalidToken(): void {
        $jane = $this->getJane()
            ->setPendingEmail('janedoe@gmail.com')
            ->generateUpdateEmailToken()
            ->save();

        try {
            $this->mock('GET /account/confirmEmailUpdate/token/' . substr($jane->getPendingEmailToken(), 0, -1));
            $this->fail('Failed rerouting on invalid email update token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());
        }
    }

    public function testConfirmEmailUpdatePost_invalidToken(): void {
        $fakeEmailSender = $this->getEmailSender();

        $newEmail = 'janedoe@gmail.com';
        $jane = $this->getJane()
            ->setPendingEmail($newEmail)
            ->generateUpdateEmailToken()
            ->save();

        $this->assertNotEquals($newEmail, $this->getJane()->getEmail());

        try {
            $this->mock('POST /account/confirmEmailUpdatePost/token/' . substr($jane->getPendingEmailToken(), 0, -1), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'password' => Constants::GEEFTER_JANE_PASSWORD
            ]);
            $this->fail('Failed rerouting on invalid email update token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $this->assertEquals(Constants::GEEFTER_JANE_EMAIL, $this->getJane()->getEmail());
            $this->assertEmpty($fakeEmailSender->getSentEmails());
        }
    }

    public function testConfirmEmailUpdatePost_invalidPassword(): void {
        $fakeEmailSender = $this->getEmailSender();

        $newEmail = 'janedoe@gmail.com';
        $jane = $this->getJane()
            ->setPendingEmail($newEmail)
            ->generateUpdateEmailToken()
            ->save();

        $this->assertNotEquals($newEmail, $this->getJane()->getEmail());

        try {
            $this->mock('POST /account/confirmEmailUpdatePost/token/' . $jane->getPendingEmailToken(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'password' => 'azerty'
            ]);
            $this->fail('Failed rerouting on invalid password.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/'), $reroute->getUrl());

            $this->assertEquals(Constants::GEEFTER_JANE_EMAIL, $this->getJane()->getEmail());
            $this->assertEmpty($fakeEmailSender->getSentEmails());
        }
    }

    public function testConfirmEmailUpdatePost_valid_notLoggedIn(): void {
        $fakeEmailSender = $this->getEmailSender();

        $newEmail = 'janedoe@gmail.com';
        $jane = $this->getJane()
            ->setPendingEmail($newEmail)
            ->generateUpdateEmailToken()
            ->save();

        $this->assertNotEquals($newEmail, $this->getJane()->getEmail());

        try {
            $this->mock('POST /account/confirmEmailUpdatePost/token/' . $jane->getPendingEmailToken(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'password' => Constants::GEEFTER_JANE_PASSWORD
            ]);
            $this->fail('Failed rerouting on valid email update token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/account/confirmEmailUpdateSuccess'), $reroute->getUrl());

            $this->assertEquals($newEmail, $this->getJane()->getEmail());
            $this->assertNotEmpty($fakeEmailSender->getSentEmails());
            $email = current($fakeEmailSender->getSentEmails());
            $this->assertEquals($newEmail, $email['to']);
        }
    }

    /**
     * @group ticket-681
     */
    public function testConfirmEmailUpdatePost_valid_loggedInAsAnotherGeefter(): void {
        $fakeEmailSender = $this->getEmailSender();

        $newEmail = 'janedoe@gmail.com';
        $jane = $this->getJane()
            ->setPendingEmail($newEmail)
            ->generateUpdateEmailToken()
            ->save();

        $this->assertNotEquals($newEmail, $this->getJane()->getEmail());

        $this->setGeefterInSession($this->getJohn());

        try {
            $this->mock('POST /account/confirmEmailUpdatePost/token/' . $jane->getPendingEmailToken(), [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'password' => Constants::GEEFTER_JANE_PASSWORD
            ]);
            $this->fail('Failed rerouting on valid email update token, when logged in as another geefter.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('/account/confirmEmailUpdateSuccess'), $reroute->getUrl());

            $this->assertEquals($newEmail, $this->getJane()->getEmail());
            $this->assertNotEmpty($fakeEmailSender->getSentEmails());
            $email = current($fakeEmailSender->getSentEmails());
            $this->assertEquals($newEmail, $email['to']);
        }
    }

    public function testConfirmEmailUpdateSuccess(): void {
        $fakeEmail = 'test@example.org';
        $this->getSession()->setUpdateEmailNewAddress($fakeEmail);

        $this->mock('GET /account/confirmEmailUpdateSuccess');
        $response = $this->getFw()->get('RESPONSE');

        $this->assertMatchesRegularExpression('/<div\s.*?class="dialog/', $response, 'Cannot find dialog in response');
        $this->assertMatchesRegularExpression(
            sprintf('/%s/', preg_quote($fakeEmail)),
            $response,
            'Cannot find email in response'
        );
        $this->assertValidHtmlWithoutErrors($response);
    }

    public function testUpdate_password_invalidCurrent(): void {
        $jane = $this->getJane()
            ->setPassword(Constants::GEEFTER_JANE_PASSWORD)
            ->save();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                'username' => $jane->getUsername(),
                'password' => 'g33ftl1s7',
                'password_confirm' => 'g33ftl1s7',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid password (missing current password).');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertTrue($this->getCryptService()->verify(Constants::GEEFTER_JANE_PASSWORD, $jane->getPasswordHash()));
        }

        $this->getGeefterSession()->clearMessages();

        try {
            $this->mock('POST /account/update', [
                'username'         => $jane->getUsername(),
                'current_password' => 'azerty',
                'password'         => 'g33ftl1s7',
                'password_confirm' => 'g33ftl1s7',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid password (invalid current password).');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertTrue($this->getCryptService()->verify(Constants::GEEFTER_JANE_PASSWORD, $jane->getPasswordHash()));
        }
    }

    public function testUpdate_password_missingConfirmation(): void {
        $jane = $this->getJane()
            ->setPassword(Constants::GEEFTER_JANE_PASSWORD)
            ->save();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                'username' => $jane->getUsername(),
                'current_password' => Constants::GEEFTER_JANE_PASSWORD,
                'password' => 'g33ftl1s7',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid password (missing confirmation).');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertTrue($this->getCryptService()->verify(Constants::GEEFTER_JANE_PASSWORD, $jane->getPasswordHash()));
        }
    }

    public function testUpdate_password_invalidTooShort(): void {
        $jane = $this->getJane()
            ->setPassword(Constants::GEEFTER_JANE_PASSWORD)
            ->save();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                'username'         => $jane->getUsername(),
                'current_password' => Constants::GEEFTER_JANE_PASSWORD,
                'password'         => 'azerty',
                'password_confirm' => 'azerty',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid password (password too short).');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertTrue($this->getCryptService()->verify(Constants::GEEFTER_JANE_PASSWORD, $jane->getPasswordHash()));
        }
    }

    public function testUpdate_password_invalidNotMatching(): void {
        $jane = $this->getJane()
            ->setPassword(Constants::GEEFTER_JANE_PASSWORD)
            ->save();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                'username'         => $jane->getUsername(),
                'current_password' => Constants::GEEFTER_JANE_PASSWORD,
                'password'         => 'g33ftl1s7',
                'password_confirm' => 'geeftlist',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid password (passwords do not match).');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertTrue($this->getCryptService()->verify(Constants::GEEFTER_JANE_PASSWORD, $jane->getPasswordHash()));
        }
    }

    public function testUpdate_password_valid(): void {
        $jane = $this->getJane()
            ->setPassword(Constants::GEEFTER_JANE_PASSWORD)
            ->save();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                'username'         => $jane->getUsername(),
                'current_password' => Constants::GEEFTER_JANE_PASSWORD,
                'password'         => 'g33ftl1s7',
                'password_confirm' => 'g33ftl1s7',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on success.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertTrue($this->getCryptService()->verify('g33ftl1s7', $jane->getPasswordHash()));
        }
    }

    public function testUpdate_dob_invalid(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->assertEquals('1970-04-03', $jane->getGeeftee()->getDob());

        try {
            $this->mock('POST /account/update', [
                'username' => $jane->getUsername(),
                'dob'      => Constants::GEEFTER_JOHN_PASSWORD,
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on invalid date of birth.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertEquals('1970-04-03', $jane->getGeeftee()->getDob());
        }
    }

    public function testUpdates_dob_valid(): void {
        $jane = $this->getJane()
            ->setLanguage('fr-FR')
            ->save();
        $this->setGeefterInSession($jane);

        $this->assertEquals('1970-04-03', $jane->getGeeftee()->getDob());

        try {
            $this->mock('POST /account/update', [
                'username' => $jane->getUsername(),
                'dob'      => '1985-06-05',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on valid FR date of birth.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertEquals('1985-06-05', $jane->getGeeftee()->getDob());
        }

        $jane->setLanguage('en-US')
            ->save();
        $this->setGeefterInSession($jane);

        try {
            $this->mock('POST /account/update', [
                'username' => $jane->getUsername(),
                'dob'      => '1985-08-07',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on valid EN date of birth.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR));
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertEquals('1985-08-07', $jane->getGeeftee()->getDob());
        }
    }

    public function testUpdate_dob_clear(): void {
        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        $this->assertEquals('1970-04-03', $jane->getGeeftee()->getDob());

        try {
            $this->mock('POST /account/update', [
                'username' => $jane->getUsername(),
                'dob'      => '',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on empty date of birth.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertNull($jane->getGeeftee()->getDob());
        }
    }

    public function testUpdate_language(): void {
        $fw = $this->getFw();

        $jane = $this->getJane();
        $this->setGeefterInSession($jane);

        // Switch to fr-FR
        try {
            $this->mock('POST /account/update', [
                'language' => 'fr-FR',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on valid language.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertEquals('fr-FR', $jane->getLanguage());

            $this->mock('GET /account');
            $body = $fw->get('RESPONSE');
            $this->assertStringContainsString('<h1>Mon compte</h1>', $body);
        }

        // Switch to en-US
        try {
            $this->mock('POST /account/update', [
                'language' => 'en-US',
                $this->getCsrfTokenName() => $this->getCsrfToken()
            ]);
            $this->fail('Failed rerouting on valid language.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertEmpty(
                $this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR),
                current($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_ERROR))
            );
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(Session::MESSAGE_SUCCESS));
            $this->assertStringEndsWith($this->getUrl('account'), $reroute->getUrl());

            $jane = $this->getJane();
            $this->assertEquals('en-US', $jane->getLanguage());

            $this->mock('GET /account');
            $body = $fw->get('RESPONSE');
            $this->assertStringContainsString('<h1>My Account</h1>', $body);
        }
    }

    public function testDeleteAccount_notLoggedIn(): void {
        try {
            $this->mock('POST /account/deleteAccountPost', [
                $this->getCsrfTokenName() => $this->getCsrfToken(),
                'current_password' => '123456'
            ]);
            $this->fail('Failed rerouting unauthorized page to login');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertStringEndsWith($this->getUrl('login'), $reroute->getUrl());
        }
    }

    public function testDeleteAccount_invalidToken(): void {
        /** @var Geefter $tempGeefter */
        $tempGeefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => 'Geefter to be deleted',
            'email'    => uniqid('geefter') . '@'. Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'password' => '123456'
        ]])->save();
        $geefterId = $tempGeefter->getId();

        $this->assertNotNull($geefterId);

        $this->setGeefterInSession($tempGeefter);
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('POST /account/deleteAccountPost', [
                'current_password' => '123456'
            ]);
            $this->fail('Failed rerouting on missing token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $this->getGeefterSession()->clearMessages();

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNotNull($tempGeefter->getId());
        $this->assertEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));

        try {
            $this->mock('POST /account/deleteAccountPost', [
                $this->getCsrfTokenName() => 'azerty',
                'current_password'        => '123456',
            ]);
            $this->fail('Failed rerouting on invalid token.');
        }
        catch (Reroute $reroute) {
            $this->handleReroute($reroute);
            $this->assertNotEmpty($this->getGeefterSession()->getMessagesByType(SessionInterface::MESSAGE_ERROR));
        }

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNotNull($tempGeefter->getId());
    }

    public function testDeleteAccount(): void {
        $fw = $this->getFw();

        /** @var Geefter $tempGeefter */
        $tempGeefter = $this->newEntity(Geefter::ENTITY_TYPE, ['data' => [
            'username' => uniqid('Geefter to be deleted'),
            'email'    => uniqid('geefter') . '@'. Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'password' => '123456'
        ]])->save();
        $geefterId = $tempGeefter->getId();

        $this->assertNotNull($geefterId);

        $this->setGeefterInSession($tempGeefter);

        $this->mock('POST /account/deleteAccountPost', [
            $this->getCsrfTokenName() => $this->getCsrfToken(),
            'current_password' => '123456'
        ]);
        $response = json_decode((string) $fw->get('RESPONSE'), true);
        $this->assertIsArray($response);

        $this->assertEquals($response['status'], 'success');
        $this->assertArrayHasKey('redirect', $response);

        $tempGeefter = $this->getEntity(Geefter::ENTITY_TYPE, $geefterId);
        $this->assertNull($tempGeefter);
    }
}
