<?php

namespace Geeftlist\Test\Model\Share\Gift\RuleType;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Model\Share\RuleTypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterTypeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->enableGeefterAccessListeners();
    }

    /**
     * @return GeefterType
     */
    public function getCut() {
        return $this->getContainer()->get(GeefterType::class);
    }

    public function testGetActionAuthorizationsByGeefter_noGeeftee(): void {
        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE);

        $jane = $this->getJane();
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();

        // Gift with no geeftee => empty
        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());
        $this->assertEmpty($actionAuthorizations);
    }

    public function testGetActionAuthorizationsByGeefter_emptyGeefterIds(): void {
        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE, ['data' => [
            'params' => null
        ]]);

        $john = $this->getJohn();
        $jane = $this->getJane();
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($johnGift);

        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());
        $this->assertEmpty($actionAuthorizations);
    }

    /**
     * @group ticket-489
     */
    public function testGetActionAuthorizationsByGeefter_invalidGeefterIds(): void {
        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE, ['data' => [
            'params' => ['geefter_ids' => [999999, 999998]]
        ]]);

        $john = $this->getJohn();
        $jane = $this->getJane();
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($johnGift);

        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());

        // Only return authorizations for Jane (creator)
        $this->assertEqualsCanonicalizing([$jane->getId()], array_keys($actionAuthorizations));
    }

    /**
     * @group ticket-489
     */
    public function testGetActionAuthorizationsByGeefter(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE, ['data' => [
            'params' => ['geefter_ids' => [$richard->getId(), $richard2->getId()]]
        ]]);
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($johnGift);

        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());
        $this->assertNotEmpty($actionAuthorizations);

        $referenceActionAuthorizations = [];
        /** @var \Geeftlist\Model\Geefter $geefter */
        foreach ([$richard, $richard2, $jane] as $geefter) {
            foreach (TypeInterface::RELATIVE_ACTIONS as $action) {
                $referenceActionAuthorizations[$geefter->getId()][$action] = RuleTypeInterface::ALLOWED;
            }
        }

        $this->assertEquals($referenceActionAuthorizations, $actionAuthorizations);
    }
}
