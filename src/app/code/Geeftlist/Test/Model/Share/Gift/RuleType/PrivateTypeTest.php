<?php

namespace Geeftlist\Test\Model\Share\Gift\RuleType;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\PrivateType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Model\Share\RuleTypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class PrivateTypeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->enableGeefterAccessListeners();
    }

    /**
     * @return PrivateType
     */
    public function getCut() {
        return $this->getContainer()->get(PrivateType::class);
    }

    public function testGetActionAuthorizationsByGeefter(): void {
        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE);

        $john = $this->getJohn();
        $jane = $this->getJane();
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($johnGift);

        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());
        $this->assertNotEmpty($actionAuthorizations);

        $referenceActionAuthorizations = [];
        foreach (TypeInterface::RELATIVE_ACTIONS as $action) {
            $referenceActionAuthorizations[$jane->getId()][$action] = RuleTypeInterface::ALLOWED;
        }

        foreach (TypeInterface::ALL_ACTIONS as $action) {
            $referenceActionAuthorizations[TypeInterface::WILDCARD_GEEFTER_ID][$action] = RuleTypeInterface::FORBIDDEN;
        }

        $this->assertEquals($referenceActionAuthorizations, $actionAuthorizations);
    }
}
