<?php

namespace Geeftlist\Test\Model\Share\Gift\RuleType;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\RegularType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Model\Share\RuleTypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class RegularTypeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->enableGeefterAccessListeners();
    }

    /**
     * @return RegularType
     */
    public function getCut() {
        return $this->getContainer()->get(RegularType::class);
    }

    public function testGetActionAuthorizationsByGeefter_noGeeftee(): void {
        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE);

        $jane = $this->getJane();
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();

        // Gift with no geeftee => empty
        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());
        $this->assertEmpty($actionAuthorizations);
    }

    public function testGetActionAuthorizationsByGeefter(): void {
        $cut = $this->newEntity(\Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE);

        $john = $this->getJohn();
        $jane = $this->getJane();
        $johnGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $jane->getId()
        ]])->save();
        $john->getGeeftee()->addGift($johnGift);

        $actionAuthorizations = $this->getCut()
            ->getActionAuthorizationsByGeefter($cut, $johnGift->getId());
        $this->assertNotEmpty($actionAuthorizations);

        $referenceActionAuthorizations = [];
        /** @var \Geeftlist\Model\Family $family */
        foreach ($john->getFamilyCollection() as $family) {
            /** @var \Geeftlist\Model\Geeftee $geeftee */
            foreach ($family->getGeefteeCollection() as $geeftee) {
                if ($geefterId = $geeftee->getGeefterId()) {
                    foreach (TypeInterface::RELATIVE_ACTIONS as $action) {
                        $referenceActionAuthorizations[$geefterId][$action] = RuleTypeInterface::ALLOWED;
                    }
                }
            }
        }

        $this->assertEquals($referenceActionAuthorizations, $actionAuthorizations);
    }
}
