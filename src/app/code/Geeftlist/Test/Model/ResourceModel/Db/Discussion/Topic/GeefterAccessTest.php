<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Discussion\Topic;


use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Discussion\Topic\GeefterAccess;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterAccessTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
    }

    /**
     * @return GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(GeefterAccess::class);
    }

    public function testIsAllowed(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);

        $publicTopic = $this->getGiftTopicHelper()->getPublicTopic($gift);
        $protectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($gift);

        // John has all permissions
        $this->assertTrue($cut->isAllowed($john, [$publicTopic, $protectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$publicTopic, $protectedTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$publicTopic, $protectedTopic], TypeInterface::AUTHOR_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$publicTopic, $protectedTopic], TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$publicTopic, $protectedTopic], TypeInterface::GIFT_GEEFTEE_PUBLIC_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$publicTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$publicTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$publicTopic], TypeInterface::AUTHOR_ACTIONS));
        $this->assertTrue($cut->isAllowed($jane, [$publicTopic], TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$publicTopic], TypeInterface::GIFT_GEEFTEE_PUBLIC_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$protectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$protectedTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$protectedTopic], TypeInterface::AUTHOR_ACTIONS));
        $this->assertTrue($cut->isAllowed($jane, [$protectedTopic], TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$protectedTopic], TypeInterface::GIFT_GEEFTEE_PUBLIC_ACTIONS));

        // Richard has read-only permissions (geefter share rule)
        $this->assertTrue($cut->isAllowed($richard, [$publicTopic, $protectedTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($richard, [$publicTopic, $protectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard, [$publicTopic, $protectedTopic], TypeInterface::AUTHOR_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$publicTopic, $protectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$publicTopic, $protectedTopic], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$publicTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$publicTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$publicTopic], TypeInterface::AUTHOR_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$publicTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$publicTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$protectedTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$protectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$protectedTopic], TypeInterface::AUTHOR_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$protectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$protectedTopic], TypeInterface::ALL_ACTIONS));
    }

    public function testGetAllowed(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);

        $publicTopic = $this->getGiftTopicHelper()->getPublicTopic($gift);
        $protectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($gift);

        // Only John has all & manage permissions
        $this->assertEquals(
            [$john->getId()],
            $cut->getAllowed([$publicTopic, $protectedTopic], TypeInterface::ALL_ACTIONS)
        );
        $this->assertEquals(
            [$john->getId()],
            $cut->getAllowed([$publicTopic, $protectedTopic], TypeInterface::MANAGE_ACTIONS)
        );

        // John and Richard have read-only permissions
        $this->assertEquals(
            [$john->getId(), $richard->getId()],
            $cut->getAllowed([$publicTopic, $protectedTopic], TypeInterface::RELATIVE_ACTIONS)
        );
        $this->assertEquals(
            [$john->getId(), $richard->getId()],
            $cut->getAllowed([$publicTopic, $protectedTopic], TypeInterface::RELATIVE_ACTIONS)
        );

        try {
            /** @psalm-suppress InvalidArgument */
            $this->assertEmpty($cut->getAllowed([null], TypeInterface::ALL_ACTIONS));
            $this->fail('Failed preventing calling getAllowed() with NULL.');
        }
        catch (\InvalidArgumentException) {}
    }

    /**
     * @return \Geeftlist\Helper\Gift\Discussion\Topic|mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getGiftTopicHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
    }
}
