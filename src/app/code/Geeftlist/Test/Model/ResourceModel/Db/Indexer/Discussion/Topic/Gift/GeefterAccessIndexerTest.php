<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Indexer\Discussion\Topic\Gift;


use Geeftlist\Indexer\Discussion\Topic\GeefterAccess as DiscussionTopicGeefterAccess;
use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Discussion\Topic\GeefterAccess;
use Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterAccessIndexerTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
    }

    /**
     * @return GeefterAccessIndexer
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(GeefterAccessIndexer::class);
    }

    /**
     * @return GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getGeefterAccess() {
        return $this->getContainer()->make(GeefterAccess::class);
    }

//    public function testReindexAll() {
//        $this->markTestSkipped('Not implemented');
//    }

    public function testReindexEntities(): void {
        $cut = $this->getCut();
        $geefterAccess = $this->getGeefterAccess();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        /** @var \Geeftlist\Model\Gift $giftFromJohnToJane */
        $giftFromJohnToJane = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
        ]
        ]])->save();
        $jane->getGeeftee()->addGift($giftFromJohnToJane);

        /** @var \Geeftlist\Model\Gift $giftFromJohnToHimself */
        $giftFromJohnToHimself = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
        ]
        ]])->save();
        $john->getGeeftee()->addGift($giftFromJohnToHimself);

        // Make sure reindexing topics is triggered manually hereafter
        $this->disableListeners('index');

        $giftFromJohnToJaneProtectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($giftFromJohnToJane);
        $giftFromJohnToJanePublicTopic = $this->getGiftTopicHelper()->getPublicTopic($giftFromJohnToJane);
        $giftFromJohnToHimselfProtectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($giftFromJohnToHimself);
        $giftFromJohnToHimselfPublicTopic = $this->getGiftTopicHelper()->getPublicTopic($giftFromJohnToHimself);

        $this->assertNotNull($giftFromJohnToJaneProtectedTopic);
        $this->assertNotNull($giftFromJohnToJanePublicTopic);
        $this->assertNotNull($giftFromJohnToHimselfProtectedTopic);
        $this->assertNotNull($giftFromJohnToHimselfPublicTopic);

        $giftFromJohnToJaneTopics = [
            $giftFromJohnToJaneProtectedTopic
        ];
        $giftFromJohnToHimselfTopics = [
            $giftFromJohnToHimselfProtectedTopic,
            $giftFromJohnToHimselfPublicTopic
        ];

        // John has all permissions except on $giftFromJohnToHimselfProtectedTopic
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Jane has no permissions at all on $giftFromJohnToJaneTopics (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::GIFT_GEEFTEE_PUBLIC_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));

        // Jane has relative permissions on $giftFromJohnToHimselfTopics (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));

        // Richard has relative permissions on both gifts' topics (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));

        $cut->reindexEntities(array_merge($giftFromJohnToJaneTopics, $giftFromJohnToHimselfTopics));

        // John has all permissions except on $giftFromJohnToHimselfProtectedTopic
        $this->assertTrue($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($john, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($john, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfPublicTopic], TypeInterface::ALL_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfPublicTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfPublicTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftFromJohnToHimselfPublicTopic], [TypeInterface::ACTION_POST]));

        // Jane has no permissions at all on $giftFromJohnToJaneTopics (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::GIFT_GEEFTEE_PUBLIC_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($jane, $giftFromJohnToJaneTopics, TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));

        // Jane has no permissions on $giftFromJohnToHimselfTopics (excluded by geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));

        // Richard has relative permissions on both gifts' topics (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToJaneTopics, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, [TypeInterface::ACTION_BE_NOTIFIED]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $giftFromJohnToHimselfTopics, TypeInterface::RELATIVE_ACTIONS));
    }

    public function testReindexEntity(): void {
        $cut = $this->getCut();
        $geefterAccess = $this->getGeefterAccess();
        $giftRepository = $this->getRepository(Gift::ENTITY_TYPE);
        $conn = $this->getConnection();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);

        // Make sure reindexing topics is triggered manually hereafter
        $this->disableListeners('index');

        $giftProtectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($gift);

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $giftProtectedTopic->getEntityType())
            ->and->equalTo('entity_id', $giftProtectedTopic->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        // All next assertions should be FALSE

        // John has all permissions
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Richard has read-only permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Reindexing...
        $cut->reindexEntity($giftProtectedTopic);

        // There should be index data now
        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $giftProtectedTopic->getEntityType())
            ->and->equalTo('entity_id', $giftProtectedTopic->getId());
        $this->assertGreaterThan(0, $conn->query($select)->count());

        // John has all permissions
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));

        // Richard has read-only permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Make sure reindexing topics is triggered manually hereafter
        $this->enableListeners('index');
        $this->getRegistry()->set('indexer::' . DiscussionTopicGeefterAccess::CODE . '::disabled', true);

        $gift->markAsDeleted();
        $giftRepository->save($gift);

        $this->getRegistry()->clear('indexer::' . DiscussionTopicGeefterAccess::CODE . '::disabled');

        // Index data should still be present
        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $giftProtectedTopic->getEntityType())
            ->and->equalTo('entity_id', $giftProtectedTopic->getId());
        $this->assertGreaterThan(0, $conn->query($select)->count());

        // All next assertions should have the same result as in the block before

        // John has all permissions
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::GIFT_GEEFTEE_PROTECTED_ACTIONS));

        // Richard has read-only permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Reindexing...
        $cut->reindexEntity($giftProtectedTopic);

        // There should not be any more index data
        // (see \Geeftlist\Model\Share\Discussion\Topic\RuleType\System\DeletedGiftType)
        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $giftProtectedTopic->getEntityType())
            ->and->equalTo('entity_id', $giftProtectedTopic->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        // John has all permissions
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Richard has read-only permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::ALL_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], [TypeInterface::ACTION_POST]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$giftProtectedTopic], TypeInterface::RELATIVE_ACTIONS));

        $this->enableListeners('index');
        $this->getRegistry()->set('indexer::' . DiscussionTopicGeefterAccess::CODE . '::disabled', true);

        // Really delete entity
        $giftRepository->delete($gift);

        $this->getRegistry()->clear('indexer::' . DiscussionTopicGeefterAccess::CODE . '::disabled');

        // Reindexing...
        $cut->reindexEntity($giftProtectedTopic);

        // There still should not be any index data
        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $giftProtectedTopic->getEntityType())
            ->and->equalTo('entity_id', $giftProtectedTopic->getId());
        $this->assertEquals(0, $conn->query($select)->count());
    }

    /**
     * @return \Geeftlist\Helper\Gift\Discussion\Topic|mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getGiftTopicHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
    }
}
