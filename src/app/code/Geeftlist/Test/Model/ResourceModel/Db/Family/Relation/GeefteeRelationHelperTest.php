<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Family\Relation;

use Geeftlist\Model\Family;
use Geeftlist\Model\ResourceModel\Db\Family\Collection;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefteeRelationHelperTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    protected function setUp(): void {
        $this->unsetGeefterInSession();
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Family\Collection
     */
    public function newCut() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Family\Collection $collection */
        $collection = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Family::ENTITY_TYPE);

        return $collection;
    }

    /**
     * @group ticket-250
     * @throws \DI\NotFoundException
     */
    public function testAddGeefteeIdFilter(): void {
        $cuts = $this->generateCutsLight(4, 0, 2, 0, 0);

        $allFamilyIds = $this->getEntityCollection(Family::ENTITY_TYPE)->getAllIds();
        foreach ($cuts['familiesByGeeftee'] as $geefteeId => $geefteeFamilies) {
            /** @var Collection $collection */
            $collection = $this->getEntityCollection(Family::ENTITY_TYPE);

            $expectedFamilyIds = array_keys($geefteeFamilies);
            $actualFamilyIds = $collection->addFieldToFilter('geeftee', $geefteeId)
                ->getAllIds();

            $this->assertEqualsCanonicalizing($expectedFamilyIds, $actualFamilyIds);

            /** @var Collection $collection */
            $collection = $this->getEntityCollection(Family::ENTITY_TYPE);

            $expectedFamilyIds = array_diff($allFamilyIds, array_keys($geefteeFamilies));
            $actualFamilyIds = $collection->addFieldToFilter('geeftee', ['neq' => $geefteeId])
                ->getAllIds();

            $this->assertEqualsCanonicalizing($expectedFamilyIds, $actualFamilyIds);
        }
    }
}
