<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Gift;


use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\ResourceModel\Db\Gift\GeefterAccess;
use Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Model\Share\Gift\RuleType\OpenGiftType;
use Geeftlist\Model\Share\Gift\RuleType\RegularType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterAccessTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
    }

    /**
     * @return GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(GeefterAccess::class);
    }

    /**
     * @return GeefterAccessIndexer
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getIndexer() {
        return $this->getContainer()->make(GeefterAccessIndexer::class);
    }

    /**
     * @group ticket-489
     */
    public function testIsAllowed_geefterShareRule(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);

        // John has all permissions
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($cut->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));
    }

    /**
     * @group ticket-489
     */
    public function testIsAllowed_regularShareRule_asReserver(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        /** @var Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
        ]])->save();
        $jane->getGeeftee()->addGift($gift);
        $this->callPrivileged(function() use ($gift, $richard): void {
            $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
                'gift_id' => $gift->getId(),
                'geefter_id' => $richard->getId()
            ]])->save();
        });

        // John has all permissions but reserver's
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative + reserver permissions (not related BUT with active reservation)
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT_RESERVATION]));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_CANCEL_RESERVATION]));
        $this->assertTrue($cut->isAllowed($richard, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));
    }

    /**
     * @group ticket-489
     */
    public function testGetAllowed_geefterShareRule(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);
        $this->callPrivileged(function() use ($gift, $richard2): void {
            $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
                'gift_id' => $gift->getId(),
                'geefter_id' => $richard2->getId()
            ]])->save();
        });

        // Only John has manage permissions
        $this->assertEquals([$john->getId()], $cut->getAllowed([$gift], TypeInterface::MANAGE_ACTIONS));

        // John and Richard have relative and reservation permissions
        $this->assertEquals(
            [$john->getId(), $richard->getId()],
            $cut->getAllowed([$gift], TypeInterface::RELATIVE_ACTIONS)
        );

        // Only Richard2 has reserver permissions
        $this->assertEquals([$richard2->getId()], $cut->getAllowed([$gift], TypeInterface::RESERVER_ACTIONS));
    }

    public function testGetAllowed_regularShareRule_openGift(): void {
        $cut = $this->getCut();

        $giftGeefter = $this->getRichard(true, 'geefter');
        $giftGeeftee = $this->getRichard(true, 'geeftee');
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);

        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' =>uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Geefter-Geeftee-Richard-Richard2 family',
            'owner_id' => $giftGeeftee->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($commonFamily, $giftGeeftee, $giftGeefter, $richard, $richard2): void {
            $commonFamily->addGeeftee([
                $giftGeeftee->getGeeftee(),
                $giftGeefter->getGeeftee(),
                $richard->getGeeftee(),
                $richard2->getGeeftee(),
            ]);
        });
        $unrelatedFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' =>uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX) . ' Unrelated family',
            'owner_id' => $richard->getId()
        ]])->save();
        $this->callPrivileged(static function () use ($unrelatedFamily, $richard, $richard3): void {
            $unrelatedFamily->addGeeftee([
                $richard->getGeeftee(),
                $richard3->getGeeftee(),
            ]);
        });

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $giftGeefter->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                ['rule_type' => RegularType::CODE],
                ['rule_type' => OpenGiftType::CODE],
            ]
        ]])->save();
        $giftGeeftee->getGeeftee()->addGift($gift);

        // Only $giftGeefter has manage permissions
        $this->assertEquals(
            [$giftGeefter->getId()],
            $cut->getAllowed([$gift], TypeInterface::MANAGE_ACTIONS)
        );

        // $giftGeeftee has view & be_notified permissions
        $this->assertContainsEquals(
            $giftGeeftee->getId(),
            $cut->getAllowed([$gift], TypeInterface::OPEN_GIFT_GEEFTEE_ACTIONS)
        );

        // $giftGeefter, $richard and $richard2 have relative permissions
        $this->assertEquals(
            [$giftGeefter->getId(), $richard->getId(), $richard2->getId()],
            $cut->getAllowed([$gift], TypeInterface::RELATIVE_ACTIONS)
        );
    }

//    public function testReindexAll() {
//        $this->markTestSkipped('Not implemented');
//    }

    /**
     * @group ticket-489
     */
    public function testReindexEntities(): void {
        $cut = $this->getCut();

        // Make sure reindexing is triggered manually
        $this->disableListeners('index');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gifts = [];
        $giftCount = 5;
        for ($i = 0; $i < $giftCount; ++$i) {
            /** @var \Geeftlist\Model\Gift $gift */
            $gifts[] = $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'creator_id' => $john->getId(),
                \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                    [
                        'rule_type' => GeefterType::CODE,
                        'params' => ['geefter_ids' => [$richard->getId()]]
                    ]
            ]
            ]]);
            $gift->save();
            $jane->getGeeftee()->addGift($gift);
        }

        // John has all permissions
        $this->assertFalse($cut->isAllowed($john, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($john, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($john, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, $gifts, TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, $gifts, TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertFalse($cut->isAllowed($richard, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, TypeInterface::ALL_ACTIONS));

        $this->getIndexer()->reindexEntities($gifts);

        // John has all permissions
        $this->assertTrue($cut->isAllowed($john, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($john, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertTrue($cut->isAllowed($john, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, $gifts, TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, $gifts, TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($cut->isAllowed($richard, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($richard, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, $gifts, TypeInterface::ALL_ACTIONS));
    }

    /**
     * @group ticket-489
     */
    public function testReindexEntity(): void {
        $cut = $this->getCut();
        $giftRepository = $this->getRepository(Gift::ENTITY_TYPE);
        $conn = $this->getConnection();

        // Make sure reindexing is triggered manually
        $this->disableListeners('index');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);
        $this->callPrivileged(function() use ($gift, $richard3): void {
            $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
                'gift_id' => $gift->getId(),
                'geefter_id' => $richard3->getId()
            ]])->save();
        });

        $select = $conn->select(GeefterAccess::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertFalse($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $this->getIndexer()->reindexEntity($gift);

        $select = $conn->select(GeefterAccess::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertGreaterThan(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($cut->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertTrue($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $gift->markAsDeleted();
        $giftRepository->save($gift);

        $select = $conn->select(GeefterAccess::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertGreaterThan(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($cut->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertTrue($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $this->getIndexer()->reindexEntity($gift);

        $select = $conn->select(GeefterAccess::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertFalse($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $giftRepository->delete($gift);

        $select = $conn->select(GeefterAccess::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        $this->getIndexer()->reindexEntity($gift);

        $select = $conn->select(GeefterAccess::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());
    }
}
