<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Geeftee\Relation;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class FamilyRelationHelperTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        $this->unsetGeefterInSession();
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    public function newCut() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
        $collection = $this->getGeeftlistResourceFactory()->resolveMakeCollection(Geeftee::ENTITY_TYPE);

        return $collection;
    }

    /**
     * @group ticket-250
     */
    public function testAddFamilyFilterIn(): void {
        $this->disableGeefterAccessListeners();

        /** @var Family $family */
        foreach ($this->getEntityCollection(Family::ENTITY_TYPE) as $family) {
            $geefteeIds = $family->getGeefteeCollection()->getAllIds();
            sort($geefteeIds);

            $this->assertEquals(
                $geefteeIds,
                $this->newCut()->addFieldToFilter('family', $family->getId())->getAllIds()
            );
        }
    }

    /**
     * @group ticket-250
     */
    public function testAddFamilyFilterNotIn(): void {
        $this->disableGeefterAccessListeners();

        $allGeefteeIds = $this->newCut()->getAllIds();

        /** @var Family $family */
        foreach ($this->getEntityCollection(Family::ENTITY_TYPE) as $family) {
            $referenceIds = array_unique(array_diff($allGeefteeIds, $family->getGeefteeCollection()->getAllIds()));
            sort($referenceIds);

            $this->assertEquals(
                $referenceIds,
                $this->newCut()->addFieldToFilter('family', ['neq' => $family->getId()])->getAllIds()
            );
        }
    }
}
