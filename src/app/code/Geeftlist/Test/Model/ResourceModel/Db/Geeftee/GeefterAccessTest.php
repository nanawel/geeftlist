<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Geeftee;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\ResourceModel\Db\Geeftee\GeefterAccess;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterAccessTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(GeefterAccess::class);
    }

    public function getCuts(): array {
        // GEEFTERS / GEEFTEES
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $richard4 = $this->getRichard(true, '_notRelative');
        /** @var Geeftee $geefterlessGeeftee */
        $geefterlessGeeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        /** @var Family $family1 */
        $family1 = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $this->callPrivileged(static function () use ($family1, $richard, $richard2, $richard3): void {
            $family1->addGeeftee([
                $richard->getGeeftee(),
                $richard2->getGeeftee(),
                $richard3->getGeeftee()
            ]);
        });

        // GIFTS
        // $gift1 from $richard2 to $richard (standard relative)
        $gift1 = $this->callPrivileged(function() use ($richard, $richard2) {
            $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'creator_id' => $richard2->getId()
            ]])->save();
            $richard->getGeeftee()->addGift($gift);

            return $gift;
        });
        // $gift2 from $richard4 to $richard (not relative "anymore", no reservation)
        $gift2 = $this->callPrivileged(function() use ($richard, $richard4) {
            $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'creator_id' => $richard4->getId()
            ]])->save();
            $richard->getGeeftee()->addGift($gift);

            return $gift;
        });
        // $gift3 from $richard3 to $richard4 (not relative "anymore" but with active reservation)
        $gift3 = $this->callPrivileged(function() use ($richard3, $richard4) {
            $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'creator_id' => $richard3->getId()
            ]])->save();
            $richard4->getGeeftee()->addGift($gift);
            $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
                'gift_id' => $gift->getId(),
                'geefter_id' => $richard3->getId()
            ]])->save();

            return $gift;
        });
        // $gift3 from $richard2 to $richard4 (not relative "anymore" and with inactive reservation)
        $gift4 = $this->callPrivileged(function() use ($richard2, $richard4) {
            $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'status' => Gift\Field\Status::ARCHIVED,
                'creator_id' => $richard2->getId()
            ]])->save();
            $richard4->getGeeftee()->addGift($gift);
            $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
                'gift_id' => $gift->getId(),
                'geefter_id' => $richard2->getId()
            ]])->save();

            return $gift;
        });

        return [
            'richard' => $richard,
            'richard2' => $richard2,
            'richard3' => $richard3,
            'richard4' => $richard4,
            'geefterlessGeeftee' => $geefterlessGeeftee,
            'gift1' => $gift1,
            'gift2' => $gift2,
            'gift3' => $gift3,
            'gift4' => $gift4,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function testIsAllowed_invalid(): void {
        $richard = $this->getRichard();

        $this->expectException(\InvalidArgumentException::class);

        // Should fail => geefter specified instead of geeftee
        $this->getCut()->isAllowed($richard, [$richard], [TypeInterface::ACTION_VIEW]);
    }

    /**
     * @throws \Throwable
     */
    public function testIsAllowed_singleEntity(): void {
        $cut = $this->getCut();

        /**
         * @var Geefter $richard
         * @var Geefter $richard2
         * @var Geefter $richard3
         * @var Geefter $richard4
         * @var Geeftee $geefterlessGeeftee
         */
        extract($this->getCuts());

        // Check access to $geefterlessGeeftee (creator = $richard)
        $this->assertTrue($cut->isAllowed($richard, [$geefterlessGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$geefterlessGeeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard, [$geefterlessGeeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard, [$geefterlessGeeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard, [$geefterlessGeeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertTrue($cut->isAllowed($richard, [$geefterlessGeeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertTrue($cut->isAllowed($richard, [$geefterlessGeeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));

        $this->assertFalse($cut->isAllowed($richard3, [$geefterlessGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$geefterlessGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$geefterlessGeeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertFalse($cut->isAllowed($richard2, [$geefterlessGeeftee], [TypeInterface::ACTION_RESERVE_GIFT]));

        // Check access to $richard
        $richardGeeftee = $richard->getGeeftee();
        $this->assertTrue($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]));
        $this->assertFalse($cut->isAllowed($richard, [$richardGeeftee], [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]));

        $this->assertTrue($cut->isAllowed($richard3, [$richardGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard2, [$richardGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard2, [$richardGeeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard2, [$richardGeeftee], [TypeInterface::ACTION_RESERVE_GIFT]));

        // Special: $richard4 has no permissions (even with gift with no reservation)
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]));
        $this->assertFalse($cut->isAllowed($richard4, [$richardGeeftee], [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]));

        // Test multiple $actions
        $this->assertTrue($cut->isAllowed($richard, [$richardGeeftee], [
            TypeInterface::ACTION_VIEW,
            TypeInterface::ACTION_EDIT,
            TypeInterface::ACTION_ADD_GIFT
        ]));
        $this->assertFalse($cut->isAllowed($richard, [$richardGeeftee], [
            TypeInterface::ACTION_VIEW,
            TypeInterface::ACTION_RESERVE_GIFT
        ]));
        $this->assertFalse($cut->isAllowed($richard, [$richardGeeftee], [
            TypeInterface::ACTION_RESERVE_GIFT,
            TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST
        ]));

        // Check access to $richard2
        $richard2Geeftee = $richard2->getGeeftee();

        $this->assertTrue($cut->isAllowed($richard2, [$richard2Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard2, [$richard2Geeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($richard2, [$richard2Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard2, [$richard2Geeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard2Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard2Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard2Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]));

        $this->assertTrue($cut->isAllowed($richard, [$richard2Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$richard2Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard, [$richard2Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee], [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]));
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee], [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]));
        $this->assertTrue($cut->isAllowed($richard3, [$richard2Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard3, [$richard2Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard3, [$richard2Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard4, [$richard2Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard4, [$richard2Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richard2Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richard2Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard4, [$richard2Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));

        // Check access to $richard3
        $richard3Geeftee = $richard3->getGeeftee();

        $this->assertTrue($cut->isAllowed($richard3, [$richard3Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard3, [$richard3Geeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($cut->isAllowed($richard3, [$richard3Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard3, [$richard3Geeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard3Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard3Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard3Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));

        $this->assertTrue($cut->isAllowed($richard, [$richard3Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [$richard3Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard, [$richard3Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard, [$richard3Geeftee], [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]));
        $this->assertFalse($cut->isAllowed($richard, [$richard3Geeftee], [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]));
        $this->assertTrue($cut->isAllowed($richard2, [$richard3Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard2, [$richard3Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertTrue($cut->isAllowed($richard2, [$richard3Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard3Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard2, [$richard3Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard4, [$richard3Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard4, [$richard3Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richard3Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard4, [$richard3Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard4, [$richard3Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));

        // Check access to $richard4
        $richard4Geeftee = $richard4->getGeeftee();

        // Special: $richard2 has no permissions (INACTIVE reservation)
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard2, [$richard4Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));

        // Special: $richard3 has view-only permissions (ACTIVE reservation)
        $this->assertTrue($cut->isAllowed($richard3, [$richard4Geeftee], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard4Geeftee], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard4Geeftee], [TypeInterface::ACTION_ADD_GIFT]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard4Geeftee], [TypeInterface::ACTION_SET_BADGES]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard4Geeftee], [TypeInterface::ACTION_RESERVE_GIFT]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard4Geeftee], [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]));
        $this->assertFalse($cut->isAllowed($richard3, [$richard4Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));
    }

    /**
     * @throws \Throwable
     */
    public function testIsAllowed_multipleEntities(): void {
        $cut = $this->getCut();

        /**
         * @var Geefter $richard
         * @var Geefter $richard2
         * @var Geefter $richard3
         * @var Geefter $richard4
         * @var Geeftee $geefterlessGeeftee
         */
        extract($this->getCuts());

        $richard2Geeftee = $richard2->getGeeftee();
        $richard3Geeftee = $richard3->getGeeftee();
        $richard4Geeftee = $richard4->getGeeftee();

        // Check access to $richard2 AND $richard3
        $this->assertTrue($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_VIEW]
        ));
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_EDIT]
        ));
        $this->assertTrue($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_ADD_GIFT]
        ));
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_SET_BADGES]
        ));
        $this->assertTrue($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_RESERVE_GIFT]
        ));
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee, $richard3Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));

        // Check access to $richard2 and $richard4
        $this->assertFalse($cut->isAllowed($richard, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_VIEW]
        ));

        $this->assertTrue($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_VIEW]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_EDIT]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_ADD_GIFT]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_SET_BADGES]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_RESERVE_GIFT]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST]
        ));
        $this->assertFalse($cut->isAllowed($richard3, [$richard2Geeftee, $richard4Geeftee],
            [TypeInterface::ACTION_REJECT_CLAIM_REQUEST]
        ));
    }

    /**
     * @throws \Throwable
     */
    public function testGetAllowed(): void {
        $cut = $this->getCut();

        $this->expectException(NotImplementedException::class);

        $cut->getAllowed([], []);
    }

//    /**
//     * @throws \Throwable
//     */
//    public function testAddGeefterAccessRestrictionsToCollection() {
//        // Not necessary, already tested by self::testIsAllowed_singleEntity()
//        // and testIsAllowed_multipleEntities()
//    }
}
