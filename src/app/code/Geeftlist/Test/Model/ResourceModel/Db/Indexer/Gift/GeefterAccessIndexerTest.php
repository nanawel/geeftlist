<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Indexer\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Model\ResourceModel\Db\Gift\GeefterAccess;
use Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterAccessIndexerTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
    }

    /**
     * @return GeefterAccessIndexer
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(GeefterAccessIndexer::class);
    }

    /**
     * @return GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getGeefterAccess() {
        return $this->getContainer()->make(GeefterAccess::class);
    }

//    public function testReindexAll() {
//        $this->markTestSkipped('Not implemented');
//    }

    /**
     * @group ticket-489
     */
    public function testReindexEntities(): void {
        $cut = $this->getCut();
        $geefterAccess = $this->getGeefterAccess();

        // Make sure reindexing is triggered manually
        $this->disableListeners('index');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $gifts = [];
        $giftCount = 5;
        for ($i = 0; $i < $giftCount; ++$i) {
            /** @var \Geeftlist\Model\Gift $gift */
            $gifts[] = $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
                'creator_id' => $john->getId(),
                \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                    [
                        'rule_type' => GeefterType::CODE,
                        'params' => ['geefter_ids' => [$richard->getId()]]
                    ]
            ]
            ]]);
            $gift->save();
            $jane->getGeeftee()->addGift($gift);
        }

        // John has all permissions
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, TypeInterface::ALL_ACTIONS));

        $cut->reindexEntities($gifts);

        // John has all permissions
        $this->assertTrue($geefterAccess->isAllowed($john, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($geefterAccess->isAllowed($john, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertTrue($geefterAccess->isAllowed($john, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, $gifts, TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($jane, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($geefterAccess->isAllowed($richard, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, $gifts, [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, $gifts, TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, $gifts, TypeInterface::ALL_ACTIONS));
    }

    /**
     * @group ticket-489
     */
    public function testReindexEntity(): void {
        $cut = $this->getCut();
        $geefterAccess = $this->getGeefterAccess();
        $giftRepository = $this->getRepository(Gift::ENTITY_TYPE);
        $conn = $this->getConnection();

        // Make sure reindexing is triggered manually
        $this->disableListeners('index');

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId()]]
                ]
            ]
        ]])->save();
        $jane->getGeeftee()->addGift($gift);
        $this->callPrivileged(function() use ($gift, $richard3): void {
            $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
                'gift_id' => $gift->getId(),
                'geefter_id' => $richard3->getId()
            ]])->save();
        });

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $cut->reindexEntity($gift);

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertGreaterThan(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($geefterAccess->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertTrue($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $gift->markAsDeleted();
        $giftRepository->save($gift);

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertGreaterThan(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertTrue($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertTrue($geefterAccess->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertTrue($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $cut->reindexEntity($gift);

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        // John has all permissions but reserver's
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($john, [$gift], TypeInterface::ALL_ACTIONS));

        // Jane has no permissions at all (geeftee)
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($jane, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard has relative permissions (geefter share rule)
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_RESERVE_PURCHASE]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard2 has no permissions at all (not related)
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard2, [$gift], TypeInterface::ALL_ACTIONS));

        // Richard3 has reserver permissions at all (not related BUT active reservation)
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_VIEW]));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RESERVER_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::RELATIVE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::MANAGE_ACTIONS));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], [TypeInterface::ACTION_EDIT]));
        $this->assertFalse($geefterAccess->isAllowed($richard3, [$gift], TypeInterface::ALL_ACTIONS));

        $giftRepository->delete($gift);

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());

        $cut->reindexEntity($gift);

        $select = $conn->select(GeefterAccessIndexer::MAIN_TABLE);
        $select->where->equalTo('entity_type', $gift->getEntityType())
            ->and->equalTo('entity_id', $gift->getId());
        $this->assertEquals(0, $conn->query($select)->count());
    }
}
