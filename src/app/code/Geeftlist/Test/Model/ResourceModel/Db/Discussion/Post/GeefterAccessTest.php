<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Discussion\Post;


use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Discussion\Post\GeefterAccess;
use Geeftlist\Model\Share\Discussion\Post\RuleType\TypeInterface;
use Geeftlist\Model\Share\Gift\RuleType\GeefterType;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Exception\NotImplementedException;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterAccessTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
    }

    /**
     * @return GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make(GeefterAccess::class);
    }

    /**
     * @group ticket-620
     */
    public function testIsAllowed(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $unrelated = $this->getRichard(true, 2);

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId(), $jane->getId()]]
                ]
            ]
        ]])->save();
        $john->getGeeftee()->addGift($gift);

        $publicTopic = $this->getGiftTopicHelper()->getPublicTopic($gift);
        $protectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($gift);

        $postFromJohnOnPublicTopic = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'message' => $this->faker()->sentence,
            'topic_id' => $publicTopic->getId(),
            'author_id' => $john->getId(),
        ]])->save();
        $postFromRichardOnPublicTopic = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'message' => $this->faker()->sentence,
            'topic_id' => $publicTopic->getId(),
            'author_id' => $richard->getId(),
        ]])->save();
        $postFromRichardOnProtectedTopic = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'message' => $this->faker()->sentence,
            'topic_id' => $protectedTopic->getId(),
            'author_id' => $richard->getId(),
        ]])->save();

        $allPosts = [
            $postFromJohnOnPublicTopic,
            $postFromRichardOnProtectedTopic,
            $postFromRichardOnPublicTopic
        ];

        //
        // VIEW

        // John, Jane & Richard can view the posts on the public topic
        $this->assertTrue($cut->isAllowed($john, [
            $postFromJohnOnPublicTopic,
            $postFromRichardOnPublicTopic
        ], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($jane, [
            $postFromJohnOnPublicTopic,
            $postFromRichardOnPublicTopic
        ], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($richard, [
            $postFromJohnOnPublicTopic,
            $postFromRichardOnPublicTopic
        ], [TypeInterface::ACTION_VIEW]));

        // John cannot view the posts on the protected topic
        $this->assertFalse($cut->isAllowed($john, [
            $postFromRichardOnProtectedTopic
        ], [TypeInterface::ACTION_VIEW]));

        // Richard & Jane can view the posts on the protected topic
        $this->assertTrue($cut->isAllowed($richard, [
            $postFromRichardOnProtectedTopic
        ], [TypeInterface::ACTION_VIEW]));
        $this->assertTrue($cut->isAllowed($jane, [
            $postFromRichardOnProtectedTopic
        ], [TypeInterface::ACTION_VIEW]));

        // An unreleated geefter can neither view the posts on the public nor protected topics
        foreach ($allPosts as $post) {
            $this->assertFalse($cut->isAllowed($unrelated, [$post], [TypeInterface::ACTION_VIEW]));
        }

        //
        // EDIT + DELETE

        // John can edit & delete its post
        $this->assertTrue($cut->isAllowed($john, [
            $postFromJohnOnPublicTopic
        ], [TypeInterface::ACTION_EDIT, TypeInterface::ACTION_DELETE]));

        // Richard can edit & delete its posts
        $this->assertTrue($cut->isAllowed($richard, [
            $postFromRichardOnProtectedTopic,
            $postFromRichardOnPublicTopic
        ], [TypeInterface::ACTION_EDIT, TypeInterface::ACTION_DELETE]));

        // Jane and the unrelated geefter cannot edit nor delete any of the posts
        foreach ($allPosts as $post) {
            $this->assertFalse($cut->isAllowed($jane, [$post], [TypeInterface::ACTION_EDIT]));
            $this->assertFalse($cut->isAllowed($jane, [$post], [TypeInterface::ACTION_DELETE]));
            $this->assertFalse($cut->isAllowed($unrelated, [$post], [TypeInterface::ACTION_EDIT]));
            $this->assertFalse($cut->isAllowed($unrelated, [$post], [TypeInterface::ACTION_DELETE]));
        }

        //
        // PIN

        // John can pin any post as the owner of both topics
        $this->assertTrue($cut->isAllowed($john, [
            $postFromJohnOnPublicTopic,
            $postFromRichardOnPublicTopic,
            $postFromRichardOnProtectedTopic,
        ], [TypeInterface::ACTION_PIN]));

        // All other geefters cannot
        foreach ([$jane, $richard, $unrelated] as $geefter) {
            foreach ($allPosts as $post) {
                $this->assertFalse($cut->isAllowed($geefter, [$post,], [TypeInterface::ACTION_PIN]));
            }
        }
    }

    /**
     * @group ticket-620
     * @
     */
    public function testGetAllowed(): void {
        $cut = $this->getCut();

        $this->expectException(NotImplementedException::class);

        $cut->getAllowed([], []);

        /*
        $john = $this->getJohn();
        $jane = $this->getJane();
        $richard = $this->getRichard();
        $unrelated = $this->getRichard(true, 2);

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $john->getId(),
            \Geeftlist\Model\Share\Entity\Rule\Constants::SHARE_ENTITY_RULE_MODEL_DATA_KEY => [
                [
                    'rule_type' => GeefterType::CODE,
                    'params' => ['geefter_ids' => [$richard->getId(), $jane->getId()]]
                ]
            ]
        ]])->save();
        $john->getGeeftee()->addGift($gift);

        $publicTopic = $this->getGiftTopicHelper()->getPublicTopic($gift);
        $protectedTopic = $this->getGiftTopicHelper()->getProtectedTopic($gift);

        $postFromJohnOnPublicTopic = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'message' => $this->faker()->sentence,
            'topic_id' => $publicTopic->getId(),
            'author_id' => $john->getId(),
        ]])->save();
        $postFromRichardOnPublicTopic = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'message' => $this->faker()->sentence,
            'topic_id' => $publicTopic->getId(),
            'author_id' => $richard->getId(),
        ]])->save();
        $postFromRichardOnProtectedTopic = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'message' => $this->faker()->sentence,
            'topic_id' => $protectedTopic->getId(),
            'author_id' => $richard->getId(),
        ]])->save();

        $allPosts = [
            $postFromJohnOnPublicTopic,
            $postFromRichardOnProtectedTopic,
            $postFromRichardOnPublicTopic
        ];

        // John has all permissions on its public post
        $this->assertEquals(
            [$john->getId()],
            $cut->getAllowed([$postFromJohnOnPublicTopic], TypeInterface::ALL_ACTIONS)
        );

        // Jane & Richard can view all posts
        $this->assertEquals(
            [$jane->getId(), $richard->getId()],
            $cut->getAllowed($allPosts, TypeInterface::ACTION_VIEW)
        );

        // ...

        try {
            // @psalm-suppress InvalidArgument //
            $this->assertEmpty($cut->getAllowed([null], TypeInterface::ALL_ACTIONS));
            $this->fail('Failed preventing calling getAllowed() with NULL.');
        }
        catch (\InvalidArgumentException $e) {}
        */
    }

    /**
     * @return \Geeftlist\Helper\Gift\Discussion\Topic|mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getGiftTopicHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
    }
}
