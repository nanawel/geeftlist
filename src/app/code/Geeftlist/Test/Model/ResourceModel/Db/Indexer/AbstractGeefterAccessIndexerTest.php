<?php

namespace Geeftlist\Test\Model\ResourceModel\Db\Indexer;


use Geeftlist\Model\ResourceModel\Db\Indexer\AbstractGeefterAccessIndexer;
use Geeftlist\Model\Share\Constants;
use Geeftlist\Model\Share\RuleTypeInterface;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

class AbstractGeefterAccessIndexerTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
    }

    public function getCut(): \Geeftlist\Model\ResourceModel\Db\Indexer\AbstractGeefterAccessIndexer {
        return new class extends AbstractGeefterAccessIndexer {
            public function __construct() {}    // Empty constructor, not needed here
        };
    }

    public function testMergeIndexData(): void {
        $testData = [
            [ // Test case #0
                [ // Existing index data ($indexData)
                    // empty
                ],
                [ // Index data to merge ($indexDataToMerge)
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Expected result
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED
                    ]
                ],
            ],
            [ // Test case #1
                [ // Existing index data ($indexData)
                    'john' => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Index data to merge ($indexDataToMerge)
                    'john' => [
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Expected result
                    'john' => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ]
                ],
            ],
            [ // Test case #2
                [ // Existing index data ($indexData)
                    'john' => [
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Index data to merge ($indexDataToMerge)
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Expected result
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED
                    ]
                ],
            ],
            [ // Test case #3
                [ // Existing index data ($indexData)
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Index data to merge ($indexDataToMerge)
                    'john' => [
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ],
                ],
                [ // Expected result
                    'john' => [
                    ]
                ],
            ],
            [ // Test case #4
                [ // Existing index data ($indexData)
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                        'b' => RuleTypeInterface::FORBIDDEN
                    ],
                ],
                [ // Index data to merge ($indexDataToMerge)
                    'john' => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                        'b' => RuleTypeInterface::ALLOWED
                    ],
                ],
                [ // Expected result
                    'john' => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ]
                ],
            ],
            [ // Test case #5
                [ // Existing index data ($indexData)
                    'john' => [
                        'a' => RuleTypeInterface::ALLOWED,
                        'b' => RuleTypeInterface::ALLOWED
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                    ],
                ],
                [ // Index data to merge ($indexDataToMerge)
                    'john' => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ],
                    Constants::WILDCARD_GEEFTER_ID => [
                        'b' => RuleTypeInterface::FORBIDDEN
                    ],
                ],
                [ // Expected result
                    'john' => [
                        'a' => RuleTypeInterface::FORBIDDEN
                    ]
                ],
            ]
        ];

        $cut = $this->getCut();

        foreach ($testData as $testCase => $testDatum) {
            [$indexData, $indexDataToMerge, $expectedResults] = $testDatum;
            $this->assertEquals(
                $expectedResults,
                $cut->mergeIndexData($indexData, $indexDataToMerge),
                sprintf('Invalid result on test case #%s for data: ', $testCase) . json_encode([$indexData, $indexDataToMerge])
            );
        }
    }
}
