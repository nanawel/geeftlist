<?php

namespace Geeftlist\Test\Model;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class ReservationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @param array $data
     * @return \Geeftlist\Model\Reservation
     */
    public function newCut($data = []) {
        return $this->newEntity(Reservation::ENTITY_TYPE, ['data' => $data]);
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Gift $aGift */
        $aGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . 'A gift proposed by John for Jane',
            'creator_id' => $john->getId()
        ]])->save();
        $jane->getGeeftee()->addGift($aGift);

        $cut = $this->newCut();

        $reservationData = [
            'geefter_id' => $john->getId(),
            'gift_id'    => $aGift->getId(),
            'type'       => Reservation::TYPE_RESERVATION
        ];
        $cut->setData($reservationData);
        $this->assertEquals($reservationData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($reservationData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($reservationData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Gift $aGift */
        $aGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'      => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . 'A gift proposed by John for Jane',
            'creator_id' => $john->getId()
        ]])->save();
        $jane->getGeeftee()->addGift($aGift);

        $cut = $this->newCut([
            'geefter_id' => $john->getId(),
            'gift_id'    => $aGift->getId(),
            'type'       => Reservation::TYPE_RESERVATION
        ])->save();
        $cutId = $cut->getId();

        $this->assertNotNull($cutId);

        $cut->delete();

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNull($cut2->getId());
        $this->assertEmpty($cut2->getData());
    }
}
