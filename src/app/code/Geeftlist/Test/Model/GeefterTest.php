<?php

namespace Geeftlist\Test\Model;


use Geeftlist\Exception\GeefterException;
use Geeftlist\Model\Geefter;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefterTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Model\Geefter
     */
    public function newCut() {
        return $this->newEntity(Geefter::ENTITY_TYPE);
    }

    /**
     * @return \Geeftlist\Model\Geefter
     */
    public function getCut(): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newCut()->setData([
            'username'  => $this->faker()->name,
            'email'     => uniqid('geefter') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'password' => \Geeftlist\Test\Constants::GEEFTER_OTHERS_PASSWORD
        ]);
        return $cut->save();
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $cutData = [
            'username'  => $this->faker()->name,
            'email'     => uniqid('geefter') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ];
        $cut->setData($cutData);
        $this->assertEquals($cutData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($cutData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($cutData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testSaveEmptyUsername(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $cutData = [
            'email'     => uniqid('geefter') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ];
        $cut->addData($cutData);

        try {
            $cut->save();
            $this->fail('Failed preventing saving a geefter with an empty username.');
        }
        catch (GeefterException $geefterException) {}

        $this->assertNull($cut->getId());   // Just to have an assertion

        $cut->setUsername('');

        try {
            $cut->save();
            $this->fail('Failed preventing saving a geefter with an empty username.');
        }
        catch (GeefterException) {}

        $this->assertNull($cut->getId());   // Just to have an assertion
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->getCut();
        $cutId = $cut->getId();

        $this->assertNotNull($cutId);

        $cut->delete();

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNull($cut2->getId());
        $this->assertEmpty($cut2->getData());
    }
}
