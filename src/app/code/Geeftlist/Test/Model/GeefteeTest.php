<?php

namespace Geeftlist\Test\Model;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GeefteeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Model\Geeftee
     */
    public function newCut() {
        return $this->newEntity(Geeftee::ENTITY_TYPE);
    }

    /**
     * @return \Geeftlist\Model\Geeftee
     */
    public function getCut(): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newCut()->setData([
            'name'      => $this->faker()->name,
            'email'     => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]);
        return $cut->save();
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $cutData = [
            'name'      => $this->faker()->name,
            'email'     => uniqid('geeftee') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ];
        $cut->setData($cutData);
        $this->assertEquals($cutData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($cutData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($cutData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->getCut();
        $cutId = $cut->getId();

        $this->assertNotNull($cutId);

        $cut->delete();

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNull($cut2->getId());
        $this->assertEmpty($cut2->getData());
    }

    public function testAddRemoveGift(): void {
        $cut = $this->getCut();

        $this->assertEmpty($cut->getGiftCollection()->getItems());

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'  => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . $this->faker()->words(3, true)
        ]])->save();
        $cut->addGift($gift);

        /** @var array $gifts Force type for PHPStan */
        $gifts = $cut->getGiftCollection()->getItems();
        $this->assertNotEmpty($gifts);
        $this->assertEquals($gift->getId(), current($gifts)->getId());

        $cut->removeGift($gift);

        $gifts = $cut->getGiftCollection()->getItems();
        $this->assertEmpty($gifts);
    }

    public function testGeefterSync(): void {
        $cut = $this->getCut();
        $cutEmail = $cut->getEmail();
        $uniqueUsername = uniqid('SOMEONE');

        $this->assertNotNull($cutEmail);
        $this->assertNotEquals($uniqueUsername, $cut->getName());

        $geefter = $this->getGeeftlistModelFactory()->resolveMake(Geefter::ENTITY_TYPE);
        $geefter->loadByEmail($cutEmail);

        $this->assertNull($geefter->getId());

        $geefter->setUsername($uniqueUsername)
            ->setEmail($cutEmail)
            ->setPassword(Constants::GEEFTER_OTHERS_PASSWORD)
            ->save();

        $cut2 = $this->newCut()
            ->loadByEmail($cutEmail);

        $this->assertNotNull($cut2->getId());
        $this->assertEquals($uniqueUsername, $cut2->getName());
    }
}
