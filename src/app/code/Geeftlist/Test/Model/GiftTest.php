<?php

namespace Geeftlist\Test\Model;

use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GiftTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Model\Gift
     */
    public function newCut() {
        return $this->newEntity(Gift::ENTITY_TYPE);
    }

    /**
     * @return \Geeftlist\Model\Gift
     */
    public function getCut(): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newCut()->setData([
            'label'           => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'description'     => $this->faker()->paragraphs(3, true) . "\n🎁",
            'estimated_price' => $this->faker()->randomFloat(2, 1, 2000)
        ]);
        return $cut->save();
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $giftData = [
            'label'           => $this->faker()->words(3, true),
            'description'     => $this->faker()->paragraphs(3, true),
            'estimated_price' => $this->faker()->randomFloat(2, 1, 2000)
        ];
        $cut->setData($giftData);
        $this->assertEquals($giftData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($giftData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($giftData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->getCut();
        $cutId = $cut->getId();

        $this->assertNotNull($cutId);

        $cut->delete();

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNull($cut2->getId());
        $this->assertEmpty($cut2->getData());
    }
}
