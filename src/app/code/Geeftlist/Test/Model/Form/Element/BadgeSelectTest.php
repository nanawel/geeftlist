<?php

namespace Geeftlist\Test\Model\Form\Element;


use Geeftlist\Form\Element\BadgeSelect;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class BadgeSelectTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Form\Element\BadgeSelect
     */
    public function newCut() {
        return $this->getContainer()->make(BadgeSelect::class);
    }

    public function testGetConfigJson(): void {
        $this->disableGeefterAccessListeners();

        /** @var Gift $entity */
        $entity = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
        ]])->save();
        $this->getRichard()->getGeeftee()->addGift($entity);

        $allEntityTypeBadges = $this->getBadgeService()->getBadgesForEntityType($entity->getEntityType());

        $cut = $this->newCut()
            ->setEntityType($entity->getEntityType())
            ->setEntityId($entity->getId());

        $this->assertEqualsCanonicalizing(
            [
                'badges' => $cut->badgeToArray($allEntityTypeBadges),
                'selectedBadgeIds' => []
            ],
            json_decode($cut->getConfigJson(), true)
        );

        $newBadgeIds = $this->faker()->randomElements(array_keys($allEntityTypeBadges), 3);
        $this->getBadgeService()->setBadgeIds($entity, $newBadgeIds);

        $this->assertEqualsCanonicalizing(
            [
                'badges' => $cut->badgeToArray($allEntityTypeBadges),
                'selectedBadgeIds' => $newBadgeIds
            ],
            json_decode($cut->getConfigJson(), true)
        );
    }
}
