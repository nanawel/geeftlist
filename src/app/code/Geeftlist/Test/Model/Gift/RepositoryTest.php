<?php

namespace Geeftlist\Test\Model\Gift;

use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class RepositoryTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
    }

    /**
     * @return RepositoryInterface
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->make('Geeftlist\Model\Gift\Repository');
    }

    public function testFind_pageCriteria(): void {
        $cut = $this->getCut();

        $referenceGifts = $cut->find([
            'limit' => 20
        ]);
        $this->assertCount(20, $referenceGifts);

        $actualGifts = $cut->find([
            'page' => 2
        ]);
        $this->assertEqualsCanonicalizing(
            array_slice(
                $referenceGifts,
                (2 - 1) * RepositoryInterface::COLLECTION_DEFAULT_PAGE_SIZE,
                RepositoryInterface::COLLECTION_DEFAULT_PAGE_SIZE
            ),
            $actualGifts
        );

        $actualGifts = $cut->find([
            'page' => [
                'number' => 1,
                'size' => 4
            ]
        ]);
        $this->assertEqualsCanonicalizing(array_slice($referenceGifts, 0, 4), $actualGifts);

        $actualGifts = $cut->find([
            'page' => [
                'number' => 3,
                'size' => 2
            ]
        ]);
        $this->assertEqualsCanonicalizing(array_slice($referenceGifts, 4, 2), $actualGifts);
    }
}
