<?php

namespace Geeftlist\Test\Model\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group indexers
 */
class GeefterAccessTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Model\Gift\GeefterAccess
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        $stubRestrictionsAppender = $this->getMockBuilder(\Geeftlist\Model\ResourceModel\Iface\Gift\RestrictionsAppenderInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stubGeefterAccessResource = $this->getMockBuilder(\Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stubGeefterAccessResource->method('isAllowed')
            ->willReturn('resource::isAllowed');
        $stubGeefterAccessResource->method('getAllowed')
            ->willReturn([$this->getJohn()->getId()]);

        return $this->getContainer()->make(
            Gift\GeefterAccess::class, [
            'restrictionsAppender' => $stubRestrictionsAppender,
            'geefterAccessResource' => $stubGeefterAccessResource
        ]);
    }

    public function testIsAllowed_oneGift(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $jane = $this->getJane();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'allowed_actions_geefter' . $john->getId() => [
                'dummy_allowed_action'
            ]
        ]]);

        $this->assertTrue($cut->isAllowed($john, [$gift], ['dummy_allowed_action']));
        $this->assertFalse($cut->isAllowed($john, [$gift], ['dummy_forbidden_action']));

        $this->assertEquals('resource::isAllowed', $cut->isAllowed($jane, [$gift], ['dummy_allowed_action']));
        $this->assertEquals('resource::isAllowed', $cut->isAllowed($jane, [$gift], ['dummy_forbidden_action']));
    }

    public function testIsAllowed_multipleGifts(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $gifts = [];
        $giftCount = 5;
        for ($i = 0; $i < $giftCount; ++$i) {
            $gifts[] = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
                'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
            ]]);
        }

        $this->assertEquals('resource::isAllowed', $cut->isAllowed($john, $gifts, ['dummy_allowed_action']));
        $this->assertEquals('resource::isAllowed', $cut->isAllowed($john, $gifts, ['dummy_forbidden_action']));
    }

    public function testGetAllowed(): void {
        $cut = $this->getCut();

        $john = $this->getJohn();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]]);

        $allowedGeefters = $cut->getAllowed([$gift], 'dummy_allowed_action');
        $this->assertCount(1, $allowedGeefters);
        $this->assertEquals($john->getId(), current($allowedGeefters)->getId());
    }
}
