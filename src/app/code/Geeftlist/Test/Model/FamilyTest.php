<?php

namespace Geeftlist\Test\Model;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class FamilyTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;

    public function newCut(): \Geeftlist\Model\Family {
        return $this->newEntity(Family::ENTITY_TYPE);
    }

    public function getCut(): \Geeftlist\Model\Family {
        $cut = $this->newCut()->setData([
            'name'      => Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . uniqid('Family_')
        ]);

        return $cut->save();
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $familyData = [
            'name'      => $this->faker()->name
        ];
        $cut->setData($familyData);
        $this->assertEquals($familyData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($familyData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($familyData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->getCut();
        $cutId = $cut->getId();

        $this->assertNotNull($cutId);

        $cut->delete();

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNull($cut2->getId());
        $this->assertEmpty($cut2->getData());
    }

    public function testAddRemoveGeeftee(): void {
        $cut = $this->getCut();

        $this->assertEmpty($cut->getGeefteeCollection()->getItems());

        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name'  => $this->faker()->name,
            'email' => uniqid('cut') . '@' . Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]])->save();
        $this->callPrivileged(static function () use ($cut, $geeftee): void {
            $cut->addGeeftee($geeftee);
        });

        /** @var array $geeftees Force type for PHPStan */
        $geeftees = $cut->getGeefteeCollection()->getItems();
        $this->assertCount(1, $geeftees);
        $this->assertEquals($geeftee->getId(), current($geeftees)->getId());

        // Add an already-member: no change
        $this->callPrivileged(static function () use ($cut, $geeftee): void {
            $cut->addGeeftee($geeftee);
        });

        /** @var array $geeftees Force type for PHPStan */
        $geeftees = $cut->getGeefteeCollection()->getItems();
        $this->assertCount(1, $geeftees);
        $this->assertEquals($geeftee->getId(), current($geeftees)->getId());

        $this->callPrivileged(static function () use ($cut, $geeftee): void {
            $cut->removeGeeftee($geeftee);
        });

        $geeftees = $cut->getGeefteeCollection()->getItems();
        $this->assertCount(0, $geeftees);

        // Remove a non-member: no change
        $this->callPrivileged(static function () use ($cut, $geeftee): void {
            $cut->removeGeeftee($geeftee);
        });
    }
}
