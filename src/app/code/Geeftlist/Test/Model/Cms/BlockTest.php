<?php

namespace Geeftlist\Test\Model\Cms;


use Geeftlist\Model\Cms\Block;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class BlockTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    public function newCut($data = []) {
        return $this->newEntity(Block::ENTITY_TYPE, ['data' => $data]);
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $cutData = [
            'code'      => uniqid('cms-block-code-'),
        ];
        $cut->setData($cutData);
        $this->assertEquals($cutData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($cutData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($cutData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut([
            'code' => uniqid('cms-block-code-'),
        ])->save();

        $cutId = $cut->getId();
        $this->assertNotNull($cutId);

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNotNull($cut2->getId());

        $cut->delete();

        $cut3 = $this->newCut();
        $cut3->load($cutId);

        $this->assertNull($cut3->getId());
        $this->assertEmpty($cut3->getData());
    }
}
