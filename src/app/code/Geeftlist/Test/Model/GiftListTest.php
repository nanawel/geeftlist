<?php

namespace Geeftlist\Test\Model;


use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class GiftListTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Model\GiftList
     */
    public function newCut() {
        return $this->newEntity(GiftList::ENTITY_TYPE);
    }

    /**
     * @return \Geeftlist\Model\GiftList
     */
    public function getCut(): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newCut()->setData([
            'owner_id' => $this->getJohn()->getId(),
            'name' => $this->faker()->words(2, true),
        ]);

        return $cut->save();
    }

    public function testLoadNonexistentEntity(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();
        $cut->load(9999999999);

        $this->assertNull($cut->getId());
        $this->assertEmpty($cut->getData());
    }

    public function testDataSave(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->newCut();

        $cutData = [
            'owner_id' => $this->getJane()->getId(),
            'name' => $this->faker()->words(2, true),
        ];
        $this->assertEmpty($cut->getData());

        $cut->setData($cutData);
        $this->assertEquals($cutData, $cut->getData());

        $cut->save();

        $cut2 = $this->newCut();
        foreach (array_keys($cutData) as $key) {
            $this->assertNull($cut2->getData($key));
        }

        $cut2->load($cut->getId());
        foreach ($cutData as $key => $gd) {
            $this->assertEquals($gd, $cut2->getData($key));
        }
    }

    public function testDelete(): void {
        $this->disableGeefterAccessListeners();    // Do not test observers in this test

        $cut = $this->getCut();
        $cutId = $cut->getId();

        $this->assertNotNull($cutId);

        $cut->delete();

        $cut2 = $this->newCut();
        $cut2->load($cutId);

        $this->assertNull($cut2->getId());
        $this->assertEmpty($cut2->getData());
    }

    public function testGetGiftIds(): void {
        $cut = $this->getCut();

        $this->assertEmpty($cut->getGiftIds());

        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label'  => Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . $this->faker()->words(3, true)
        ]])->save();
        $this->getContainer()->get(GiftList\Gift::class)
            ->addLinks([$cut], [$gift]);

        $giftIds = $cut->getGiftIds();
        $this->assertNotEmpty($giftIds);
        $this->assertEquals([$gift->getId()], $giftIds);

        $this->getContainer()->get(GiftList\Gift::class)
            ->removeLinks([$cut], [$gift]);

        $giftIds = $cut->getGiftIds();
        $this->assertEmpty($giftIds);
    }
}
