<?php

namespace Geeftlist\Test\Model\Geefter;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Session\GeefterTrait;

/**
 * @method int|null getGeefterId()
 *
 * @group base
 * @group models
 */
class FakeSession extends \OliveOil\Core\Test\Model\FakeSession
    implements \Geeftlist\Model\Session\GeefterInterface
{
    use GeefterTrait {
        resetGeefterData as geefterTrait_reset;
    }

    public function __construct(
        \OliveOil\Core\Model\Session\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        $name
    ) {
        parent::__construct(
            $context,
            $name
        );
    }

    /**
     * Special behavior: we don't reset the current geefter if present.
     * (it may have been set by a test method before the first call to a controller)
     *
     * @param bool $force
     * @return $this
     */
    public function init($force = false): static {
        $geefter = $this->geefter;

        parent::init($force);

        if ($geefter) {
            $this->setGeefter($geefter);
        }

        return $this;
    }

    public function getFamilyCollection() {
        if ($currentGeefter = $this->getGeefter()) {
            return $currentGeefter->getFamilyCollection();
        }

        return null;
    }

    public function getFamilyIds() {
        // Notice: cache is not desirable here as family memberships may vary rapidly during UT execution
        return $this->getFamilyCollection()
                ->getAllIds();
    }

    public function setGeefter(Geefter $geefter = null): static {
        $this->geefter = $geefter;
        $this->setGeefterId($geefter instanceof \Geeftlist\Model\Geefter ? $geefter->getId() : null);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function invalidate(): static {
        parent::invalidate()
            ->resetGeefterData();   // Also clear GeefterTrait's properties

        return $this;
    }

    public function getGeefterRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefterRepository;
    }
}
