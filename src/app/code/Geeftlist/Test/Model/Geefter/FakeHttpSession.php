<?php

namespace Geeftlist\Test\Model\Geefter;


/**
 * @method int|null getGeefterId()
 *
 * @group base
 * @group models
 */
class FakeHttpSession extends FakeSession
    implements \Geeftlist\Model\Session\Http\GeefterInterface
{
    /**
     * @inheritDoc
     */
    public function setType($type) {
        return parent::setType($type);
    }

    /**
     * @inheritDoc
     */
    public function getType() {
        return parent::getType();
    }
}
