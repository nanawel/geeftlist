<?php

namespace Geeftlist\Test\Model\Geeftee;


use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class ClaimRequestTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return \Geeftlist\Model\Geeftee\ClaimRequest
     */
    public function newCut() {
        return $this->newEntity(Geeftee\ClaimRequest::ENTITY_TYPE);
    }

    /**
     * @param int $geefterId
     * @param int $geefteeId
     * @return \Geeftlist\Model\Geeftee\ClaimRequest
     */
    public function getCut($geefterId, $geefteeId): \OliveOil\Core\Model\AbstractModel {
        $cut = $this->newCut();
        $cutData = [
            'geeftee_id' => $geefteeId,
            'geefter_id' => $geefterId
        ];
        return $cut->setData($cutData)
            ->save();
    }

    public function testSave(): void {
        $this->disableGeefterAccessListeners();

        /** @var Geeftee $geeftee */
        $geeftee = $this->getGeeftlistModelFactory()->resolveMake(Geeftee::ENTITY_TYPE, ['data' => [
            'name'       => Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX . 'John Smith',
            'creator_id' => $this->getJohn()->getId()
        ]]);
        $geeftee->save();

        $geefter = $this->getGeefterButJohn();

        $cut = $geeftee->newClaimRequest($geefter);

        $this->assertEquals($geeftee->getId(), $cut->getGeeftee()->getId());
        $this->assertEquals($geefter->getId(), $cut->getGeefter()->getId());
        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_PENDING, $cut->getDecisionCode());

        $cut->save();

        $cut2 = $this->newCut()->load($cut->getId());
        $this->assertEquals($geeftee->getId(), $cut2->getGeeftee()->getId());
        $this->assertEquals($geefter->getId(), $cut2->getGeefter()->getId());
        $this->assertNotNull($cut2->getCreatedAt());
        $this->assertEquals(Geeftee\ClaimRequest::DECISION_CODE_PENDING, $cut2->getDecisionCode());
    }
}
