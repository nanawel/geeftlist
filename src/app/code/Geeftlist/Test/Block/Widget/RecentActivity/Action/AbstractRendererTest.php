<?php

namespace Geeftlist\Test\Block\Widget\RecentActivity\Action;

use Geeftlist\Model\Activity;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;


/**
 * @group base
 * @group blocks
 */
abstract class AbstractRendererTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @param Activity $activity
     * @return \Geeftlist\Block\Widget\RecentActivity\Action\ActionInterface
     * @throws \DI\NotFoundException
     */
    public function getCut($activity) {
        return $this->getContainer()->get(\Geeftlist\Block\Widget\RecentActivity\Action\ActionRendererFactory::class)
            ->getByModel($activity);
    }

    /**
     * @return Activity
     */
    protected function newActivity(array $data) {
        /** @var Activity $activity */
        $activity = $this->newEntity(Activity::ENTITY_TYPE, ['data' => $data]);

        return $activity;
    }
}
