<?php

namespace Geeftlist\Test\Block\Widget\RecentActivity\Action\Gift;

use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Block\Widget\RecentActivity\Action\AbstractRendererTest;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;


/**
 * @group base
 * @group view
 */
class DeleteTest extends AbstractRendererTest
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->getI18n()->setLanguage('en-US');
    }

    public function testRenderHtml(): void {
        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX)
        ]])->save();
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $geeftee->addGift($gift);
        $activity = $this->newActivity([
            'actor_type' => Geefter::ENTITY_TYPE,
            'actor_id' => $this->getJane()->getId(),
            'object_type' => Gift::ENTITY_TYPE,
            'object_id' => $gift->getId(),
            'action' => \Geeftlist\Model\Activity\Field\Action::DELETE
        ]);
        $cut = $this->getCut($activity);

        $this->assertInstanceOf(\Geeftlist\Block\Widget\RecentActivity\Action\Gift::class, $cut);

        $html = $cut->render(['activity' => $activity]);
        $this->assertNotEmpty($html);
        $this->assertIsString($html);
        // Only check HTML syntax, that should cover most potential regressions
        $this->assertValidHtmlWithoutErrors("<!DOCTYPE html><html><body>"
            . $html
            . "</body></html>"
        );
    }
}
