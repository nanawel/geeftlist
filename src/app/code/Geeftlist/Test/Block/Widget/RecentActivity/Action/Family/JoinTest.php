<?php

namespace Geeftlist\Test\Block\Widget\RecentActivity\Action\Family;

use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Block\Widget\RecentActivity\Action\AbstractRendererTest;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;


/**
 * @group base
 * @group view
 */
class JoinTest extends AbstractRendererTest
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->getI18n()->setLanguage('en-US');
    }

    public function testRenderHtml(): void {
        $family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name' => uniqid(Constants::SAMPLEDATA_FAMILY_NAME_PREFIX)
        ]])->save();
        $activity = $this->newActivity([
            'actor_type' => Geeftee::ENTITY_TYPE,
            'actor_id' => $this->getJane()->getGeeftee()->getId(),
            'object_type' => Family::ENTITY_TYPE,
            'object_id' => $family->getId(),
            'action' => \Geeftlist\Model\Activity\Field\Action::JOIN
        ]);
        $cut = $this->getCut($activity);

        $this->assertInstanceOf(\Geeftlist\Block\Widget\RecentActivity\Action\Family::class, $cut);

        $html = $cut->render(['activity' => $activity]);
        $this->assertNotEmpty($html);
        $this->assertIsString($html);
        // Only check HTML syntax, that should cover most potential regressions
        $this->assertValidHtmlWithoutErrors("<!DOCTYPE html><html><body>"
            . $html
            . "</body></html>"
        );
    }
}
