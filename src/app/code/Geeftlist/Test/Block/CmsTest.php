<?php

namespace Geeftlist\Test\Block;


use Geeftlist\Block\Cms;
use Geeftlist\Model\Cms\Block as BlockModel;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group base
 * @group models
 */
class CmsTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @param array $data
     * @return \Geeftlist\Block\Cms
     */
    public function newCut($data = []) {
        $data = array_merge([uniqid('cms-block-')], $data);
        return $this->getContainer()->make(Cms::class, ['data' => $data]);
    }

    public function testRenderNonExistingBlock(): void {
        $cut = $this->newCut();

        $this->assertEmpty($cut->render());

        $cut->setConfig([
            'code' => 'my-non-existing-block'
        ]);

        $this->assertEmpty($cut->render());
    }

    public function testRenderExistingBlock(): void {
        $blockCode = uniqid('existing-block_');
        $model = $this->newEntity(BlockModel::ENTITY_TYPE, ['data' => [
            'code'          => $blockCode,
            'language'      => 'en',
            'content_type'  => 'text/html',
            'content'       => $this->faker()->paragraphs(4, true),
            'enabled'       => 1
        ]])->save();

        $cut = $this->newCut();
        $cut->setConfig('code', $blockCode);

        $this->assertStringContainsString($model->getContent(), $cut->render());
    }

    public function testRenderDisabledBlock(): void {
        $blockCode = uniqid('disabled-block_');
        $this->newEntity(BlockModel::ENTITY_TYPE, ['data' => [
            'code'          => $blockCode,
            'language'      => 'en',
            'content_type'  => 'text/html',
            'content'       => $this->faker()->paragraphs(4, true),
            'enabled'       => 0
        ]])->save();

        $cut = $this->newCut();
        $cut->setConfig('code', $blockCode);

        $this->assertEmpty($cut->render());
    }

    public function testRenderWithLanguagePriority(): void {
        $blockCode = uniqid('en-fr-block_');
        $enBlock = $this->newEntity(BlockModel::ENTITY_TYPE, ['data' => [
            'code'          => $blockCode,
            'language'      => 'en',
            'content_type'  => 'text/html',
            'content'       => $this->faker()->paragraphs(4, true),
            'enabled'       => 1
        ]])->save();
        $frBlock = $this->newEntity(BlockModel::ENTITY_TYPE, ['data' => [
            'code'          => $blockCode,
            'language'      => 'fr',
            'content_type'  => 'text/html',
            'content'       => $this->faker()->paragraphs(4, true),
            'enabled'       => 1
        ]])->save();

        // Priority FR-EN with both blocks enabled => render FR
        $this->getI18n()->clearCache()
            ->setLanguage('fr-FR', 'en');

        $cut = $this->newCut();
        $cut->setConfig('code', $blockCode);

        $this->assertNotEquals($enBlock->getContent(), $frBlock->getContent());
        $this->assertStringContainsString($frBlock->getContent(), $cut->render());

        // Priority FR-EN with block FR disabled => render EN
        $frBlock->setEnabled(false)
            ->save();

        $cut = $this->newCut();
        $cut->setConfig('code', $blockCode);

        $this->assertStringContainsString($enBlock->getContent(), $cut->render());

        // Priority EN-FR with both blocks enabled => render EN
        $this->getI18n()->clearCache()
            ->setLanguage('en', 'fr');
        $frBlock->setEnabled(true)
            ->save();

        $cut = $this->newCut();
        $cut->setConfig('code', $blockCode);

        $this->assertStringContainsString($enBlock->getContent(), $cut->render());

        // Priority EN-FR with block EN disabled => render FR
        $enBlock->setEnabled(false)
            ->save();

        $cut = $this->newCut();
        $cut->setConfig('code', $blockCode);

        $this->assertStringContainsString($frBlock->getContent(), $cut->render());
    }
}
