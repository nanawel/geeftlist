<?php

namespace Geeftlist\Test;


use Geeftlist\Indexer\Gift\GeefterAccess;
use OliveOil\Core\Exception\FilesystemException;
use OliveOil\Core\Test\Exception\BootstrapException;

class Bootstrap extends \OliveOil\Core\Test\Bootstrap
{
    public function __construct(
        \Base $fw,
        \OliveOil\Core\Model\AppInterface $app,
        \OliveOil\Core\Model\App\Config $appConfig,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Design $design,
        \OliveOil\Core\Model\SetupInterface $setup,
        \OliveOil\Core\Service\RouteInterface $routeService,
        \OliveOil\Core\Service\Cache $cache,
        \OliveOil\Core\Service\Log $logService,
        protected \Geeftlist\Service\System\Housekeeping $housekeepingService,
        protected \OliveOil\Core\Service\GenericFactoryInterface $indexerFactory,
        array $preTasks = null
    ) {
        parent::__construct(
            $fw,
            $app,
            $appConfig,
            $eventService,
            $design,
            $setup,
            $routeService,
            $cache,
            $logService,
            $preTasks
        );
        $this->preTasks[] = 'assertUserDataPubDirWritable';
        $this->preTasks[] = 'pruneBrokenEntityLinks';
        $this->preTasks[] = 'reindexGiftGeefterAccess';
    }

    protected function assertUserDataPubDirWritable() {
        $pubDir = realpath('pub/userdata');
        if (!is_dir($pubDir)) {
            throw new BootstrapException('pub/userdata/ directory does not exists.');
        }

        try {
            $this->assertDirWritableRecursive($pubDir);
        } catch (FilesystemException $filesystemException) {
            throw new BootstrapException($filesystemException->getMessage(), previous: $filesystemException);
        }
    }

    /**
     * @throws FilesystemException
     */
    protected function assertDirWritableRecursive(string $path) {
        if (!is_writable($path)) {
            throw new FilesystemException($path . ' directory is not writable by current user.');
        }

        $subDirs = glob($path . '/*', GLOB_ONLYDIR);
        foreach ($subDirs as $subDir) {
            if (!is_writable($subDir)) {
                throw new FilesystemException($subDir . ' directory is not writable by current user.');
            }

            $this->assertDirWritableRecursive($subDir);
        }
    }

    protected function pruneBrokenEntityLinks() {
        try {
            if ($this->readParam('PRUNE_BROKEN_ENTITY_LINKS')) {
                $this->log("Pruning broken entity links...", null, LOG_INFO);
                $this->housekeepingService->pruneAllBrokenEntityLinks();
                $this->log("Pruning complete.", null, LOG_INFO);
            }
        }
        catch (\Exception $exception) {
            $this->handleUnknownDatabase($exception);
            throw $exception;
        }
    }

    protected function reindexGiftGeefterAccess() {
        try {
            // TODO Make indexes parameterized
            if ($this->readParam('REINDEX_GEEFTERACCESS')
                || $this->readParam('RECREATE_DATABASE')    // Also run a full reindexing when this param is set
                || $this->readParam('INJECT_SAMPLE_DATA')   // Also run a full reindexing when this param is set
                || $this->readParam('UPGRADE_SAMPLE_DATA')  // Also run a full reindexing when this param is set
            ) {
                $this->log("Reindexing */geefterAccess...", null, LOG_INFO);
                $this->indexerFactory->resolveGet(GeefterAccess::CODE)
                    ->reindexAll();

                // Reindexing discussion/topics is not necessary here as it is already a dependent
                // index of gift/geefterAccess

                $this->log("Reindex complete.", null, LOG_INFO);
            }
        }
        catch (\Exception $exception) {
            $this->handleUnknownDatabase($exception);
            throw $exception;
        }
    }
}
