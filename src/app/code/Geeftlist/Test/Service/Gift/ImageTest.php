<?php

namespace Geeftlist\Test\Service\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Service\Gift\Image;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Exception\UploadException;
use OliveOil\Core\Model\Upload\File;
use OliveOil\Core\Model\Validation\ErrorAggregatorInterface;
use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    use TestCaseTrait;
    use GiftTrait;
    /**
     * @return Image
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(Image::class);
    }

    /**
     * @return string
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getBaseImagePath() {
        return $this->getContainer()->get(\Geeftlist\Service\Gift\Image::class . '.imageUrlBasePath');
    }

    public function testGetUrl(): void {
        $gift = $this->newEntity(Gift::ENTITY_TYPE);
        $testData = [
            [
                'image_id' => self::$TEST_IMAGE_ID,
                'type' => null,
                'expectedResult' => $this->getUrl(
                    $this->getBaseImagePath() . '/' . self::$TEST_IMAGE_BASE_PATH . '.jpg'
                )
            ],
            [
                'image_id' => self::$TEST_IMAGE_ID,
                'type' => Image::IMAGE_TYPE_FULL,
                'expectedResult' => $this->getUrl(
                    $this->getBaseImagePath() . '/' . self::$TEST_IMAGE_BASE_PATH . '.jpg'
                )
            ],
            [
                'image_id' => self::$TEST_IMAGE_ID,
                'type' => Image::IMAGE_TYPE_TMP,
                'expectedResult' => $this->getUrl(
                    $this->getBaseImagePath() . '/' . self::$TEST_IMAGE_BASE_PATH . '_tmp.jpg'
                )
            ],
            [
                'image_id' => self::$TEST_IMAGE_ID,
                'type' => Image::IMAGE_TYPE_THUMBNAIL,
                'expectedResult' => $this->getUrl(
                    $this->getBaseImagePath() . '/' . self::$TEST_IMAGE_BASE_PATH . '_tn.jpg'
                )
            ],
        ];

        foreach ($testData as $testDatum) {
            $this->prepareTestImage(self::$TEST_IMAGE_ID);
            $gift->setImageId($testDatum['image_id']);
            $this->assertEquals(
                $testDatum['expectedResult'],
                $this->getCut()->getUrl($gift, $testDatum['type'])
            );
        }
    }

    public function testUpload_invalidImage(): void {
        $cut = $this->getCut();

        $inputName = 'fakeInput';
        $tmpName = tempnam('/tmp', 'geeftlist_ut_');
        copy(self::$TEST_IMAGE_SOURCE_DIR . 'gift_invalid.jpg', $tmpName);
        $this->getFw()->set('FILES.' . $inputName, [
            'name' => 'gift_invalid.jpg',
            'tmp_name' => $tmpName,
            'size' => 42
        ]);

        /** @var ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->getContainer()->make(ErrorAggregatorInterface::class);
        try {
            $cut->upload($inputName, $errorAggregator);
            $this->fail('Failed throwing exception on invalid image.');
        }
        catch (UploadException) {}

        $this->assertNotEmpty($errorAggregator->getFieldErrors($inputName));
    }

    public function testUpload_invalidType(): void {
        $cut = $this->getCut();

        $inputName = 'fakeInput';
        $tmpName = tempnam('/tmp', 'geeftlist_ut_');
        file_put_contents($tmpName, '<?php phpinfo();');
        $this->getFw()->set('FILES.' . $inputName, [
            'name' => 'hidden_php.jpg',
            'tmp_name' => $tmpName,
            'size' => 42
        ]);

        /** @var ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->getContainer()->make(ErrorAggregatorInterface::class);
        try {
            $cut->upload($inputName, $errorAggregator);
            $this->fail('Failed throwing exception on invalid image.');
        }
        catch (UploadException) {}

        $this->assertNotEmpty($errorAggregator->getFieldErrors($inputName));
    }

    public function testUpload_valid(): void {
        $cut = $this->getCut();

        $inputName = 'fakeInput';
        $tmpName = tempnam('/tmp', 'geeftlist_ut_');
        copy(self::$TEST_IMAGE_SOURCE_DIR . 'gift.jpg', $tmpName);
        $this->getFw()->set('FILES.' . $inputName, [
            'name' => 'gift.jpg',
            'tmp_name' => $tmpName,
            'size' => 42
        ]);

        /** @var ErrorAggregatorInterface $errorAggregator */
        $errorAggregator = $this->getContainer()->make(ErrorAggregatorInterface::class);
        $file = $cut->upload($inputName, $errorAggregator);

        $this->assertEmpty($errorAggregator->getFieldErrors($inputName));

        $this->assertInstanceOf(File::class, $file);
        $this->assertNotEmpty($file->getData('image_id'));
        $this->assertNotEmpty($file->getData('image_path'));

        $this->assertEquals(
            $cut->getImagePath($file->getData('image_id'), Image::IMAGE_TYPE_TMP),
            $file->getData('image_path')
        );

        $this->assertFileExists($file->getData('image_path'));
    }

    public function testApply_invalid(): void {
        $cut = $this->getCut();

        $gift = $this->newEntity(Gift::ENTITY_TYPE);
        $invalidImageId = $cut->makeImageId(true);
        $this->assertNotEmpty($invalidImageId);
        try {
            $cut->apply($gift, $invalidImageId);

            $this->fail('Failed preventing applying an invalid image ID to a gift.');
        }
        catch (\InvalidArgumentException) {}
    }

    public function testApply_valid(): void {
        $cut = $this->getCut();
        $this->prepareTestImage(self::$TEST_IMAGE_ID, 'gift.jpg', Image::IMAGE_TYPE_TMP);

        $gift = $this->newEntity(Gift::ENTITY_TYPE);
        $this->assertFileDoesNotExist($cut->getImagePath(self::$TEST_IMAGE_ID, Image::IMAGE_TYPE_FULL));

        $cut->apply($gift, self::$TEST_IMAGE_ID);

        $this->assertEquals(self::$TEST_IMAGE_ID, $gift->getImageId());
        $this->assertFileExists($cut->getImagePath(self::$TEST_IMAGE_ID, Image::IMAGE_TYPE_FULL));
    }

    public function testGenerateImage(): void {
        $this->prepareTestImage(self::$TEST_IMAGE_ID, 'gift.jpg');

        $cut = $this->getCut();

        $this->assertFileDoesNotExist($cut->getImagePath(self::$TEST_IMAGE_ID, Image::IMAGE_TYPE_THUMBNAIL));
        $cut->generateImage(self::$TEST_IMAGE_ID, Image::IMAGE_TYPE_FULL, Image::IMAGE_TYPE_THUMBNAIL);
        $this->assertFileExists($cut->getImagePath(self::$TEST_IMAGE_ID, Image::IMAGE_TYPE_THUMBNAIL));
    }

    public function testMakeImageId(): void {
        $imageIds = [];
        for ($i = 3; $i; --$i) {
            $imageIds[] = $this->getCut()->makeImageId();
        }

        $this->assertCount(3, $imageIds);
        $this->assertCount(3, array_unique($imageIds));
    }

    public function testMatchImageIdFormat(): void {
        $testData = [
            '' => false,
            'azerty' => false,
            '123456' => false,
            'a1b2c3d4' => false,
            'e7c714f84f25c28eb3f9e4f6ef82d52' => false,
            'e7c714f84f25c28eb3f9e4f6ef82d52d' => true,
        ];

        foreach ($testData as $value => $expectedResult) {
            $this->assertEquals($expectedResult, $this->getCut()->matchImageIdFormat($value));
        }
    }
}
