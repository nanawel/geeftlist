<?php

namespace Geeftlist\Test\Service\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Service\Gift\Email;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\Upload\GiftTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    use TestCaseTrait;
    use FakerTrait;
    use GiftTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getEmailSender()->clearSentEmails();
        $this->getI18n()->setLanguage('en-US');
    }

    /**
     * @return Email
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(Email::class);
    }

    public function testSendArchivingRequest(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
            'creator_id' => $this->getJane()->getId()
        ])->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->getCut()->sendArchivingRequest($gift, $john);

        /** @var array $sentEmails Force type for PHPStan */
        $sentEmails = $this->getEmailSender()->getSentEmails();
        $this->assertCount(1, $sentEmails);

        $sentEmail = current($sentEmails);
        $this->assertEquals('Geeftlist: Archiving request for ' . $gift->getLabel(), $sentEmail['subject']);
        $this->assertEquals($jane->getEmail(), $sentEmail['to']);
        $this->assertValidHtmlWithoutErrors($sentEmail['body']);
    }

    public function testSendReportToCreator(): void {
        $john = $this->getJohn();
        $jane = $this->getJane();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();
        $message = $this->faker()->paragraph(2);

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->getCut()->sendReportToCreator($gift, $john, $message);

        /** @var array $sentEmails Force type for PHPStan */
        $sentEmails = $this->getEmailSender()->getSentEmails();
        $this->assertCount(1, $sentEmails);

        $sentEmail = current($sentEmails);
        $this->assertEquals(
            sprintf(
                'Geeftlist: Report for %s by %s',
                $gift->getLabel(),
                $john->getUsername()
            ),
            $sentEmail['subject']
        );
        $this->assertEquals($jane->getEmail(), $sentEmail['to']);
        $this->assertStringContainsString(nl2br($message), $sentEmail['body']);
        $this->assertValidHtmlWithoutErrors($sentEmail['body']);
    }

    public function testSendReportToAdministrator(): void {
        $john = $this->getJohn();

        /** @var Gift $gift */
        $gift = $this->getEntityCollection(Gift::ENTITY_TYPE, [
                'creator_id' => $this->getJane()->getId()
            ])
            ->addFieldToFilter('geeftee', ['neq' => $john->getGeeftee()->getId()])
            ->getFirstItem();
        $message = $this->faker()->paragraph(2);

        $this->assertEmpty($this->getEmailSender()->getSentEmails());

        $this->getCut()->sendReportToAdministrator($gift, $john, $message);

        /** @var array $sentEmails Force type for PHPStan */
        $sentEmails = $this->getEmailSender()->getSentEmails();
        $this->assertCount(1, $sentEmails);

        $sentEmail = current($sentEmails);
        $this->assertEquals(
            sprintf(
                '[Geeftlist Admin] Report for %s by %s',
                $gift->getLabel(),
                $john->getUsername()
            ),
            $sentEmail['subject']
        );
        $this->assertEquals($this->getAppConfig()->getValue('EMAIL_ADMIN'), $sentEmail['to']);
        $this->assertStringContainsString(nl2br($message), $sentEmail['body']);
        $this->assertValidHtmlWithoutErrors($sentEmail['body']);
    }
}
