<?php

namespace Geeftlist\Test\Service;

use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;
use Geeftlist\Model\MailNotification\Constants;
use Geeftlist\Model\Notification\Discussion\Post\Constants as PostConstants;
use Geeftlist\Model\Notification\Family\Constants as FamilyConstants;
use Geeftlist\Model\Notification\Gift\Constants as GiftConstants;
use Geeftlist\Model\Notification\Reservation\Constants as ReservationConstants;
use Geeftlist\Model\Reservation;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;


/**
 * @group base
 * @group services
 */
class MailNotificationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $emailEnabledBackup;

    protected function setUp(): void {
        parent::setUp();
        $this->disableGeefterAccessListeners();
        $this->unsetGeefterInSession();

        // Speed up tests by skipping email sending
        $this->emailEnabledBackup = (bool) $this->getAppConfig()->getValue('EMAIL_ENABLED');
        $this->getAppConfig()->setValue('EMAIL_ENABLED', false);
    }

    protected function tearDown(): void {
        parent::tearDown();

        $this->getAppConfig()->setValue('EMAIL_ENABLED', $this->emailEnabledBackup);
    }

    public function testGetNotifiableGeefters_Gift(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $richard4 = $this->getRichard(true, 4);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $richard->getId()
        ]])->save();
        $commonFamily->addGeeftee([
            $richard->getGeeftee(),
            $richard2->getGeeftee(),
            $richard3->getGeeftee(),
            $richard4->getGeeftee()
        ]);

        // Invalid gift (empty creator)
        $invalidGift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX)
        ]])->save();
        $richard2->getGeeftee()->addGift($invalidGift);

        $geefters = $cut->getNotifiableGeefters(GiftConstants::EVENT_ADD_TO_GEEFTEES, $invalidGift);
        $this->assertEmpty($geefters->getItems());

        // Gift for 1 geeftee
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);

        // We're not passing actor_id as third argument unlike in
        // \Geeftlist\Observer\Cron\MailNotification\Geefter::prepareMailRecipients so $richard ID gets included
        // in the results (while it shouldn't when run from the previously mentioned method)
        $geefters = $cut->getNotifiableGeefters(GiftConstants::EVENT_ADD_TO_GEEFTEES, $gift);
        $this->assertEquals(
            [$richard->getId(), $richard3->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );

        // Gift for 2 geeftees
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);
        $richard3->getGeeftee()->addGift($gift);

        // Same remark as above
        $geefters = $cut->getNotifiableGeefters(GiftConstants::EVENT_ADD_TO_GEEFTEES, $gift);
        $this->assertEquals(
            [$richard->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );
    }

    public function testGetNotifiableGeefters_Gift_notificationsFreq(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3)
            ->setEmailNotificationFreq(Constants::SENDING_FREQ_DAILY)
            ->save();
        $richard4 = $this->getRichard(true, 4)
            ->setEmailNotificationFreq(Constants::SENDING_FREQ_DISABLED)
            ->save();

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $richard->getId()
        ]])->save();
        $commonFamily->addGeeftee([
            $richard->getGeeftee(),
            $richard2->getGeeftee(),
            $richard3->getGeeftee(),
            $richard4->getGeeftee()
        ]);

        // Gift for 1 geeftee
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);

        // We're not passing actor_id as third argument unlike in
        // \Geeftlist\Observer\Cron\MailNotification\Geefter::prepareMailRecipients so $richard ID gets included
        // in the results (while it shouldn't when run from the previously mentioned method)
        $geefters = $cut->getNotifiableGeefters(GiftConstants::EVENT_ADD_TO_GEEFTEES, $gift);
        $this->assertEquals(
            [$richard->getId(), $richard3->getId()],
            $geefters->getAllIds()
        );
    }

    public function testGetNotifiableGeefters_Family(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $richard4 = $this->getRichard(true, 4);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $richard->getId()
        ]])->save();
        $commonFamily->addGeeftee([
            $richard->getGeeftee(),
            $richard2->getGeeftee(),
            $richard3->getGeeftee(),
            $richard4->getGeeftee()
        ]);

        /** @var Family $richard4Family */
        $richard4Family = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $richard4->getId()
        ]])->save();
        $richard4Family->addGeeftee([
            $richard4->getGeeftee()
        ]);

        $geefters = $cut->getNotifiableGeefters(
            FamilyConstants::EVENT_JOIN,
            $richard4Family,
            ['geeftee_ids' => [$richard4->getGeeftee()->getId()]]
        );
        $this->assertEmpty($geefters->getItems());

        $geefters = $cut->getNotifiableGeefters(
            FamilyConstants::EVENT_JOIN,
            $commonFamily,
            ['geeftee_ids' => [$richard3->getGeeftee()->getId(), $richard4->getGeeftee()->getId()]]
        );
        $this->assertEquals(
            [$richard->getId(), $richard2->getId()],
            $geefters->getAllIds()
        );

        $geefters = $cut->getNotifiableGeefters(
            FamilyConstants::EVENT_LEAVE,
            $commonFamily,
            ['geeftee_ids' => [$richard3->getGeeftee()->getId(), $richard4->getGeeftee()->getId()]]
        );
        $this->assertEquals(
            [$richard->getId(), $richard2->getId()],
            $geefters->getAllIds()
        );
    }

    public function testGetNotifiableGeefters_Reservation(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $richard4 = $this->getRichard(true, 4);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $richard->getId()
        ]])->save();
        $commonFamily->addGeeftee([
            $richard->getGeeftee(),
            $richard2->getGeeftee(),
            $richard3->getGeeftee(),
            $richard4->getGeeftee()
        ]);

        // Gift for 1 geeftee
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $richard3->getId()
        ]])->save();

        $geefters = $cut->getNotifiableGeefters(ReservationConstants::EVENT_NEW, $reservation);
        $this->assertEquals(
            [$richard->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );
        $geefters = $cut->getNotifiableGeefters(ReservationConstants::EVENT_UPDATE, $reservation);
        $this->assertEquals(
            [$richard->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );
        $geefters = $cut->getNotifiableGeefters(
            ReservationConstants::EVENT_DELETE,
            $reservation,
            ['gift_id' => $gift->getId(), 'geefter_id' => $richard3->getId()]
        );
        $this->assertEquals(
            [$richard->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );

        // Gift for 2 geeftees
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);
        $richard3->getGeeftee()->addGift($gift);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $richard4->getId()
        ]])->save();

        $geefters = $cut->getNotifiableGeefters(ReservationConstants::EVENT_NEW, $reservation);
        $this->assertEquals(
            [$richard->getId()],
            $geefters->getAllIds()
        );
        $geefters = $cut->getNotifiableGeefters(ReservationConstants::EVENT_UPDATE, $reservation);
        $this->assertEquals(
            [$richard->getId()],
            $geefters->getAllIds()
        );
        $geefters = $cut->getNotifiableGeefters(
            ReservationConstants::EVENT_DELETE,
            $reservation,
            ['gift_id' => $gift->getId(), 'geefter_id' => $richard4->getId()]
        );
        $this->assertEquals(
            [$richard->getId()],
            $geefters->getAllIds()
        );

        // Gift for all geeftees of the family
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);
        $richard3->getGeeftee()->addGift($gift);
        $richard4->getGeeftee()->addGift($gift);

        $reservation = $this->newEntity(Reservation::ENTITY_TYPE, ['data' => [
            'gift_id'    => $gift->getId(),
            'geefter_id' => $richard->getId()
        ]])->save();

        $geefters = $cut->getNotifiableGeefters(ReservationConstants::EVENT_NEW, $reservation);
        $this->assertEmpty($geefters->getItems());
        $geefters = $cut->getNotifiableGeefters(ReservationConstants::EVENT_UPDATE, $reservation);
        $this->assertEmpty($geefters->getItems());
        $geefters = $cut->getNotifiableGeefters(
            ReservationConstants::EVENT_DELETE,
            $reservation,
            ['gift_id' => $gift->getId(), 'geefter_id' => $richard->getId()]
        );
        $this->assertEmpty($geefters->getItems());
    }

    public function testGetNotifiableGeefters_GiftDiscussionPost(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);
        $richard3 = $this->getRichard(true, 3);
        $richard4 = $this->getRichard(true, 4);

        /** @var Family $commonFamily */
        $commonFamily = $this->newEntity(Family::ENTITY_TYPE, ['data' => [
            'name'     => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX),
            'owner_id' => $richard->getId()
        ]])->save();
        $commonFamily->addGeeftee([
            $richard->getGeeftee(),
            $richard2->getGeeftee(),
            $richard3->getGeeftee(),
            $richard4->getGeeftee()
        ]);

        // Gift for 1 geeftee
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard2->getGeeftee()->addGift($gift);

        // Protected topic
        $protectedTopic = $this->getTopicHelper()->getProtectedTopic($gift);
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'title'     => $this->faker()->sentence(),
            'message'   => $this->faker()->paragraphs(2, true),
            'author_id' => $richard3->getId(),
            'topic_id'  => $protectedTopic->getId()
        ]])->save();

        $geefters = $cut->getNotifiableGeefters(PostConstants::EVENT_NEW, $post);
        $this->assertEquals(
            [$richard->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );

        // Gift for 1 geeftee, creator is the geeftee
        $gift = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();
        $richard->getGeeftee()->addGift($gift);

        // Protected topic
        $protectedTopic = $this->getTopicHelper()->getProtectedTopic($gift);
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'title'     => $this->faker()->sentence(),
            'message'   => $this->faker()->paragraphs(2, true),
            'author_id' => $richard3->getId(),
            'topic_id'  => $protectedTopic->getId()
        ]])->save();

        $geefters = $cut->getNotifiableGeefters(PostConstants::EVENT_NEW, $post);
        $this->assertEquals(
            [$richard2->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );

        // Public topic
        $publicTopic = $this->getTopicHelper()->getPublicTopic($gift);
        $post = $this->newEntity(Post::ENTITY_TYPE, ['data' => [
            'title'     => $this->faker()->sentence(),
            'message'   => $this->faker()->paragraphs(2, true),
            'author_id' => $richard3->getId(),
            'topic_id'  => $publicTopic->getId()
        ]])->save();

        $geefters = $cut->getNotifiableGeefters(PostConstants::EVENT_NEW, $post);
        $this->assertEquals(
            [$richard->getId(), $richard2->getId(), $richard4->getId()],
            $geefters->getAllIds()
        );
    }

    /**
     * @return \Geeftlist\Service\MailNotification
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Service\MailNotification::class);
    }

    /**
     * @return \Geeftlist\Helper\Gift\Discussion\Topic
     * @throws \DI\NotFoundException
     */
    public function getTopicHelper() {
        return $this->getContainer()->get(\Geeftlist\Helper\Gift\Discussion\Topic::class);
    }
}
