<?php

namespace Geeftlist\Test\Service\Geefter;


use Geeftlist\Service\Geefter\Email;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @method \Geeftlist\Service\Geefter\Email newCut()
 *
 * @group base
 * @group services
 */
class EmailTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getEmailSender()->clearSentEmails();
    }

    /**
     * @return Email
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(Email::class);
    }

    public function testGetPreparedEmail(): void {
        $john = $this->getJohn();
        $john->setLanguage('fr-FR')
            ->save();
        $jane = $this->getJane();
        $jane->setLanguage('en-US')
            ->save();

        // Geefter using fr-FR locale
        $email = $this->getCut()->getPreparedEmail($john, \OliveOil\Core\Model\Email\Template::class)
            ->setTemplate('test/sample.phtml')
            ->setSubject('Test');
        $this->getEmailSender()->send($email);

        $sentEmails = $this->getEmailSender()->getSentEmails(true);
        $this->assertEquals(1, count($sentEmails));

        $this->assertStringContainsString($sentEmails[0]['from'], $this->getAppConfig()->getValue('EMAIL_DEFAULT_SENDER'));
        $this->assertEquals($john->getEmail(), $sentEmails[0]['to']);
        $this->assertStringContainsString('FR sample email', $sentEmails[0]['body']);

        // Geefter using en-US locale
        $email = $this->getCut()->getPreparedEmail($jane, \OliveOil\Core\Model\Email\Template::class)
            ->setTemplate('test/sample.phtml')
            ->setSubject('Test');
        $this->getEmailSender()->send($email);

        $sentEmails = $this->getEmailSender()->getSentEmails(true);
        $this->assertEquals(1, count($sentEmails));

        $this->assertStringContainsString($sentEmails[0]['from'], $this->getAppConfig()->getValue('EMAIL_DEFAULT_SENDER'));
        $this->assertEquals($jane->getEmail(), $sentEmails[0]['to']);
        $this->assertStringContainsString('EN sample email', $sentEmails[0]['body']);
    }

    /**
     * @group ticket-390
     */
    public function testSendNewAccountEmail(): void {
        $richard1 = $this->getRichard(true, 1);
        $richard1->generatePasswordToken()
            ->setLanguage('fr-FR')
            ->save();
        $richard2 = $this->getRichard(true, 2);
        $richard2->generatePasswordToken()
            ->setLanguage('en-US')
            ->save();

        // Geefter using fr-FR locale
        $this->getCut()->sendNewAccountEmail($richard1);

        $sentEmails = $this->getEmailSender()->getSentEmails(true);
        $this->assertEquals(1, count($sentEmails));

        $this->assertStringContainsString($sentEmails[0]['from'], $this->getAppConfig()->getValue('EMAIL_DEFAULT_SENDER'));
        $this->assertEquals($richard1->getEmail(), $sentEmails[0]['to']);
        $this->assertStringContainsString(sprintf('Bienvenue %s', $richard1->getUsername()), $sentEmails[0]['body']);

        // Check confirm link: must NOT use Redirect controller (see regression on #390)
        $this->assertEquals(1, preg_match('/id="confirm-link" href="([^"]+)/', (string) $sentEmails[0]['body'], $matches));
        $this->assertStringStartsWith($this->getUrl('signup/confirm'), $matches[1]);

        // Geefter using en-US locale
        $this->getCut()->sendNewAccountEmail($richard2);

        $sentEmails = $this->getEmailSender()->getSentEmails(true);
        $this->assertEquals(1, count($sentEmails));

        $this->assertStringContainsString($sentEmails[0]['from'], $this->getAppConfig()->getValue('EMAIL_DEFAULT_SENDER'));
        $this->assertEquals($richard2->getEmail(), $sentEmails[0]['to']);
        $this->assertStringContainsString(sprintf('Welcome %s', $richard2->getUsername()), $sentEmails[0]['body']);

        // Check confirm link: must NOT use Redirect controller (see regression on #390)
        $this->assertEquals(1, preg_match('/id="confirm-link" href="([^"]+)/', (string) $sentEmails[0]['body'], $matches));
        $this->assertStringStartsWith($this->getUrl('signup/confirm'), $matches[1]);
    }
}
