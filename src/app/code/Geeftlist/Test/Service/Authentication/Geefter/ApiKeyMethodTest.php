<?php

namespace Geeftlist\Test\Service\Authentication\Geefter;


use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;
use Geeftlist\Service\Authentication\Geefter\ApiKeyMethod;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group api
 * @group services
 */
class ApiKeyMethodTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return ApiKeyMethod
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(ApiKeyMethod::class);
    }

    public function testAuthenticate_invalidString(): void {
        $geefter = null;

        // Empty
        try {
            $geefter = $this->getCut()->authenticate('');
        }
        catch (\InvalidArgumentException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_invalidArray(): void {
        $geefter = null;

        // Empty
        try {
            $geefter = $this->getCut()->authenticate([]);
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 1
        try {
            $geefter = $this->getCut()->authenticate(['foo']);
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 2
        try {
            $geefter = $this->getCut()->authenticate(['api_key' => ['foo']]);
        }
        catch (\InvalidArgumentException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_invalidAuth(): void {
        $geefter = null;
        try {
            $geefter = $this->getCut()->authenticate([
                'api_key' => 'foo',
            ]);
        }
        catch (AuthenticationException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_validAuth(): void {
        $geefter = $this->getCut()->authenticate([
            'api_key' => $this->getJane()->getApiKey(),
        ]);
        $this->assertInstanceOf(Geefter::class, $geefter);
        $this->assertEquals($this->getJane()->getId(), $geefter->getId());
    }
}
