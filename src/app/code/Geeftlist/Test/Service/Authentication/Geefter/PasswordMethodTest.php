<?php

namespace Geeftlist\Test\Service\Authentication\Geefter;


use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;
use Geeftlist\Service\Authentication\Geefter\PasswordMethod;
use Geeftlist\Test\Constants;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group api
 * @group services
 */
class PasswordMethodTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return PasswordMethod
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(PasswordMethod::class);
    }

    public function testAuthenticate_invalidString(): void {
        $geefter = null;

        // Empty
        try {
            $geefter = $this->getCut()->authenticate('');
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid encoding
        try {
            $geefter = $this->getCut()->authenticate('foo:bar');
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 1
        try {
            $geefter = $this->getCut()->authenticate(base64_encode('foo'));
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 2
        try {
            $geefter = $this->getCut()->authenticate(base64_encode('foo:'));
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 3
        try {
            $geefter = $this->getCut()->authenticate(base64_encode(':bar'));
        }
        catch (\InvalidArgumentException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_invalidArray(): void {
        $geefter = null;

        // Empty
        try {
            $geefter = $this->getCut()->authenticate([]);
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 1
        try {
            $geefter = $this->getCut()->authenticate(['foo']);
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 2
        try {
            $geefter = $this->getCut()->authenticate(['email' => ['foo']]);
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 3
        try {
            $geefter = $this->getCut()->authenticate(['email' => Constants::GEEFTER_JANE_EMAIL]);
        }
        catch (\InvalidArgumentException $invalidArgumentException) {}

        $this->assertNull($geefter);

        // Invalid format 4
        try {
            $geefter = $this->getCut()->authenticate(['password' => Constants::GEEFTER_JANE_PASSWORD]);
        }
        catch (\InvalidArgumentException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_invalidAuth(): void {
        $geefter = null;
        try {
            $geefter = $this->getCut()->authenticate([
                'email' => Constants::GEEFTER_JANE_EMAIL,
                'password' => 'baz'
            ]);
        }
        catch (AuthenticationException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_validAuth(): void {
        $geefter = $this->getCut()->authenticate([
            'email' => Constants::GEEFTER_JANE_EMAIL,
            'password' => Constants::GEEFTER_JANE_PASSWORD
        ]);
        $this->assertInstanceOf(Geefter::class, $geefter);
        $this->assertEquals($this->getJane()->getId(), $geefter->getId());
    }
}
