<?php

namespace Geeftlist\Test\Service\Authentication\Geefter;


use Geeftlist\Exception\Security\AuthenticationException;
use Geeftlist\Model\Geefter;
use Geeftlist\Service\Authentication\Geefter\JwtMethod;
use Geeftlist\Test\Util\TestCaseTrait;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Configuration;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group api
 * @group services
 */
class JwtMethodTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    /**
     * @return JwtMethod
     * @throws \DI\NotFoundException
     */
    public function newCut() {
        return $this->getContainer()->make(JwtMethod::class);
    }

    public function testAuthenticate_invalidString(): void {
        $geefter = null;

        // Empty
        try {
            $geefter = $this->newCut()->authenticate('');
            $this->fail('Failed throwing exception when authenticating with an empty token.');
        }
        catch (\InvalidArgumentException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_invalidSignature(): void {
        $john = $this->getJohn();

        $config = $this->getJwtConfiguration();
        $invalidKey = \Lcobucci\JWT\Signer\Key\InMemory::plainText(str_repeat('a', 384 / 8));   // 384 bits minimum

        $token = $this->getJwtBuilder()->withClaim('geefter_id', $john->getId())
            ->getToken($config->signer(), $invalidKey);

        $geefter = null;
        try {
            $geefter = $this->newCut()->authenticate($token->toString());
            $this->fail('Failed throwing exception when authenticating with an invalid signature.');
        }
        catch (AuthenticationException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_expiredToken(): void {
        $john = $this->getJohn();

        $config = $this->getJwtConfiguration();

        $token = $this->getJwtBuilder()
            ->withClaim('geefter_id', $john->getId())
            ->identifiedBy(sprintf(
                '%s:foo',
                $this->getCryptService()->hash($john->getApiKey() . $config->signingKey()->contents())
            ))
            ->expiresAt(new \DateTimeImmutable(sprintf('@%d', time() - 600)))
            ->getToken($config->signer(), $config->signingKey());

        $geefter = null;
        try {
            $geefter = $this->newCut()->authenticate($token->toString());
            $this->fail('Failed throwing exception when authenticating with an expired token.');
        }
        catch (AuthenticationException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_invalidJti(): void {
        $john = $this->getJohn();

        $config = $this->getJwtConfiguration();

        $token = $this->getJwtBuilder()
            ->withClaim('geefter_id', $john->getId())
            // Using Jane's API key instead of John's to build the JTI
            ->identifiedBy(sprintf(
                '%s:foo',
                $this->getCryptService()->hash($this->getJane()->getApiKey() . $config->signingKey()->contents())
            ))
            ->expiresAt(new \DateTimeImmutable(sprintf('@%d', time() + 3600)))
            ->getToken($config->signer(), $config->signingKey());

        $geefter = null;
        try {
            $geefter = $this->newCut()->authenticate($token->toString());
            $this->fail('Failed throwing exception when authenticating with an invalid JTI.');
        }
        catch (AuthenticationException) {}

        $this->assertNull($geefter);
    }

    public function testAuthenticate_valid(): void {
        $john = $this->getJohn();

        $config = $this->getJwtConfiguration();

        $token = $this->getJwtBuilder()
            ->withClaim('geefter_id', $john->getId())
            ->identifiedBy(sprintf(
                '%s:foo',
                $this->getCryptService()->hash($john->getApiKey() . $config->signingKey()->contents())
            ))
            ->getToken($config->signer(), $config->signingKey());

        $geefter = $this->newCut()->authenticate($token->toString());

        $this->assertInstanceOf(Geefter::class, $geefter);
        $this->assertEquals($john->getId(), $geefter->getId());
    }

    public function testGetToken(): void {
        $john = $this->getJohn();

        $cut = $this->newCut();
        $stringToken = $cut->generateToken($john);

        $geefter = $cut->authenticate($stringToken);

        $this->assertInstanceOf(Geefter::class, $geefter);
        $this->assertEquals($john->getId(), $geefter->getId());
    }

    /**
     * @return Builder
     * @throws \DI\DependencyException
     */
    protected function getJwtBuilder() {
        return clone $this->getContainer()->get('jwt.geefter.builder');
    }

    /**
     * @return Configuration
     * @throws \DI\DependencyException
     */
    protected function getJwtConfiguration() {
        return $this->getContainer()->get('jwt.geefter.config');
    }
}
