<?php

namespace Geeftlist\Test\Service\Rest\JsonApi\Traits;


use Geeftlist\Model\Discussion\Post;
use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\GiftList;
use Geeftlist\Model\Reservation;
use OliveOil\Core\Model\Rest\JsonApi\Constants;
use OliveOil\Core\Model\Rest\JsonApi\Params;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\Constraint\Constraint;

trait ValidationTrait
{
    /**
     * @param string|null $entityType
     * @param int[]|null $entityIds
     * @throws \LogicException
     */
    public function assertIsValidJsonApiCollection(
        array $collectionData,
        $entityType = null,
        $entityIds = null,
        \OliveOil\Core\Model\Rest\JsonApi\Params $params = null
    ): void {
        /** @var Params $params */
        $params = $params ?: $this->getContainer()->make(Params::class);
        if (!$params->getObjectType()) {
            $params->setObjectType(Constants::PARAM_OBJECT_TYPE_RESOURCE_LITE);
        }

        $this->assertIsArray($collectionData);

        if ($collectionData !== []) {
            foreach ($collectionData as $item) {
                $this->assertIsValidResourceObject(
                    $item,
                    $entityType,
                    null,
                    $params
                );
            }

            if ($entityIds !== null) {
                if (empty($entityIds)) {
                    $this->assertEmpty($collectionData);
                }
                else {
                    $this->assertEqualsCanonicalizing(
                        $entityIds,
                        array_column($collectionData, 'id')
                    );
                }
            }
        }
    }

    /**
     * @param array $entityData
     * @param string|null $entityType
     * @param int|null $entityId
     * @throws \LogicException
     */
    public function assertIsValidResourceObjectIdentifier(
        $entityData,
        $entityType = null,
        $entityId = null,
        \OliveOil\Core\Model\Rest\JsonApi\Params $params = null
    ): void {
        /** @var Params $params */
        $params = $params ?: $this->getContainer()->make(Params::class);
        $params->setObjectType(Constants::PARAM_OBJECT_TYPE_RESOURCE_ID);

        $this->assertIsValidResourceObject($entityData, $entityType, $entityId, $params);
    }

    /**
     * @param string|null $entityType
     * @param int|null $entityId
     * @throws \LogicException
     */
    public function assertIsValidResourceObject(
        array $entityData,
        $entityType = null,
        $entityId = null,
        \OliveOil\Core\Model\Rest\JsonApi\Params $params = null
    ): void {
        try {
            /** @var Params $params */
            $params = $params ?: $this->getContainer()->make(Params::class);
            if (!$params->getObjectType()) {
                $params->setObjectType(Constants::PARAM_OBJECT_TYPE_RESOURCE_LITE);
            }

            $this->assertIsArray($entityData);

            if ($entityType === null) {
                $entityType = $this->getEntityCode();
                if ($entityType === null) {
                    throw new \Exception('No entity type specified in test class ' . static::class);
                }
            }

            $this->assertArrayHasKey('type', $entityData);
            $this->assertEquals($entityType, $entityData['type']);
            $this->assertArrayHasKey('id', $entityData);

            if ($entityId === null) {
                $this->assertGreaterThan(0, (int) $entityData['id']);
            }
            else {
                $this->assertEquals($entityId, $entityData['id']);
            }

            if ($params->includeAttributes()) {
                $this->assertIsValidResourceObjectAttributes($entityData, $entityType, $entityId);
            }
            else {
                $this->assertArrayNotHasKey('attributes', $entityData, 'Not expecting resource attributes here.');
            }

            if ($params->includeFullRelationships()) {
                $this->assertIsValidResourceObjectRelationships($entityData, $entityType, $entityId);
            }
            elseif ($params->includeLiteRelationships()) {
                //TODO
                //$this->fail('Not implemented');
            }
            else {
                $this->assertArrayNotHasKey('relationships', $entityData, 'Not expecting resource relationships here.');
            }
        }
        catch (AssertionFailedError $assertionFailedError) {
            throw new AssertionFailedError(sprintf('Not a valid %s entity.', $entityType), 0, $assertionFailedError);
        }
    }

    /**
     * @param string $entityType
     * @param int $entityId
     * @throws \LogicException
     */
    protected function assertIsValidResourceObjectAttributes(array $entityData, $entityType, $entityId) {
        $this->assertArrayHasKey('attributes', $entityData);
        $entityAttributesData = $entityData['attributes'];
        foreach ($this->getResourceObjectAttributesValidationConstraints($entityType) as $field => $constraint) {
            if ($constraint === false) {
                $this->assertArrayNotHasKey($field, $entityAttributesData);
            }
            elseif ($constraint instanceof Constraint) {
                // Notice: cannot assertArrayHasKey because if the field is nullable, it might just not exist
                //$this->assertArrayHasKey($field, $entityAttributesData);
                $value = $entityAttributesData[$field] ?? null;
                $printableValue = $value === null ? '<NULL>' : (string) $value;
                $printableValue = mb_strlen($printableValue) > 30
                    ? substr($printableValue, 0, 30) . '[...]'
                    : $printableValue;
                $this->assertThat(
                    $value,
                    $constraint,
                    'Field: ' . $field . ' (value = ' . $printableValue . ')'
                );
            }
        }
    }

    /**
     * @param string $entityType
     * @param int $entityId
     * @throws \LogicException
     */
    protected function assertIsValidResourceObjectRelationships(array $entityData, $entityType, $entityId) {
        $relationshipValidationConstraints = $this->getResourceObjectRelationshipsValidationConstraints($entityType);

        if (empty($relationshipValidationConstraints)) {
            $this->assertArrayNotHasKey('relationships', $entityData);
        }
        else {
            $validatedRelationships = [];
            foreach ($relationshipValidationConstraints as $relationship => $constraintDefinition) {
                if (!is_array($constraintDefinition)) {
                    $constraintDefinition = [$constraintDefinition];
                }

                /** @var bool|Constraint $constraint */
                foreach ($constraintDefinition as $constraint) {
                    $this->assertArrayHasKey(
                        'relationships',
                        $entityData,
                        sprintf('Missing relationships for type %s.', $entityType)
                    );
                    $this->assertArrayHasKey(
                        $relationship,
                        $entityData['relationships'],
                        sprintf('Missing relationship %s->%s.', $entityType, $relationship)
                    );

                    $this->assertThat(
                        $entityData['relationships'][$relationship],
                        $constraint,
                        sprintf('Relationship: %s->%s', $entityType, $relationship)
                    );
                }

                $validatedRelationships[] = $relationship;
            }

            $unknownRelationships = \OliveOil\array_discard($entityData['relationships'], $validatedRelationships);
            $this->assertEmpty(
                $unknownRelationships,
                sprintf(
                    'Unknown relationship(s) found for type %s: %s.',
                    $entityType,
                    implode(', ', array_keys($unknownRelationships))
                )
            );
        }
    }

    /**
     * @return array
     */
    protected function getResourceObjectAttributesValidationConstraints(?string $entityType) {
        if ($entityType === null || $entityType === '' || $entityType === '0') {
            throw new \InvalidArgumentException('$entityType must be specified.');
        }

        $method = 'getEntityValidationConstraints_' . str_replace('/', '_', $entityType);

        if (!is_callable([$this, $method])) {
            $this->markTestIncomplete('Missing entity validation for type ' . $entityType);
        }

        return $this->$method();
    }

    /**
     * @return array
     */
    protected function getResourceObjectRelationshipsValidationConstraints(?string $entityType) {
        if ($entityType === null || $entityType === '' || $entityType === '0') {
            throw new \InvalidArgumentException('$entityType must be specified.');
        }

        $method = 'getEntityRelationshipsValidationConstraints_' . str_replace('/', '_', $entityType);

        if (!is_callable([$this, $method])) {
            $this->markTestIncomplete('Missing relationships validation for entity type ' . $entityType);
        }

        return $this->$method();
    }

    protected function getEntityValidationConstraints_discussion_topic(): array {
        return [
            'topic_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'title' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'status' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsEqual(Topic::STATUS_OPEN),
                new \PHPUnit\Framework\Constraint\IsEqual(Topic::STATUS_CLOSED)
            ),
            'is_pinned' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsEqual(0),
                new \PHPUnit\Framework\Constraint\IsEqual(1)
            ),
            'linked_entity_type' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'linked_entity_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'author_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_discussion_topic(): array {
        return [
            'linked_entity' => $this->getIsRelationshipEntityConstraint(null),
            'posts' => $this->getIsRelationshipEntityCollectionConstraint(Post::ENTITY_TYPE),
        ];
    }

    protected function getEntityValidationConstraints_discussion_post(): array {
        return [
            'post_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'topic_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'title' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'message' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'is_pinned' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsEqual(0),
                new \PHPUnit\Framework\Constraint\IsEqual(1)
            ),
            'author_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_discussion_post(): array {
        return [
            'topic' => $this->getIsRelationshipEntityCollectionConstraint(Topic::ENTITY_TYPE),
        ];
    }

    protected function getEntityValidationConstraints_family(): array {
        return [
            'family_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'name' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'visibility' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsEqual(Family\Field\Visibility::PUBLIC),
                new \PHPUnit\Framework\Constraint\IsEqual(Family\Field\Visibility::PROTECTED),
                new \PHPUnit\Framework\Constraint\IsEqual(Family\Field\Visibility::PRIVATE),
                new \PHPUnit\Framework\Constraint\IsNull(),
            ),
            'owner_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_family(): array {
        return [
            'owner' => $this->getIsRelationshipEntityConstraint(Geefter::ENTITY_TYPE),
            'geeftees' => $this->getIsRelationshipEntityCollectionConstraint(Geeftee::ENTITY_TYPE),
        ];
    }

    protected function getEntityValidationConstraints_family_membershipRequest(): array {
        return [
            'membership_request_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'family_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'sponsor_id' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('numeric'),
                new \PHPUnit\Framework\Constraint\IsNull()
            ),
            'geeftee_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_family_membershipRequest(): array {
        return [
            'family' => $this->getIsRelationshipEntityConstraint(Family::ENTITY_TYPE),
            'geeftee' => $this->getIsRelationshipEntityConstraint(Geeftee::ENTITY_TYPE),
            'sponsor' => Assert::logicalOr(
                $this->getIsRelationshipEntityNullConstraint(),
                $this->getIsRelationshipEntityConstraint(Geefter::ENTITY_TYPE)
            ),
        ];
    }

    protected function getEntityValidationConstraints_geeftee(): array {
        return [
            'geeftee_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'name' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'email' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'creator_id' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'geefter_id' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'avatar_path' => false,
            'avatar_url' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\StringStartsWith(
                    $this->getContainer()->get('app.base_url')
                        . $this->getContainer()->get(\Geeftlist\Service\Avatar\Geeftee::class . '.avatarUrlBasePath')
                )
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_geeftee(): array {
        return [
            'geefter' => Assert::logicalOr(
                $this->getIsRelationshipEntityNullConstraint(),
                $this->getIsRelationshipEntityConstraint(Geefter::ENTITY_TYPE)
            ),
            'families' => $this->getIsRelationshipEntityCollectionConstraint(Family::ENTITY_TYPE)
        ];
    }

    protected function getEntityValidationConstraints_geefter(): array {
        return [
            'geefter_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'username' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'email' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_geefter(): array {
        return [
            'geeftee' => Assert::logicalOr(
                $this->getIsRelationshipEntityNullConstraint(),
                $this->getIsRelationshipEntityConstraint(Geeftee::ENTITY_TYPE)
            ),
        ];
    }

    protected function getEntityValidationConstraints_gift(): array {
        return [
            'gift_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'label' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'description' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'estimated_price' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'creator_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_gift(): array {
        return [
            'creator' => $this->getIsRelationshipEntityConstraint(Geefter::ENTITY_TYPE),
            'geeftees' => $this->getIsRelationshipEntityCollectionConstraint(Geeftee::ENTITY_TYPE),
            'reservations' => $this->getIsRelationshipEntityCollectionConstraint(Reservation::ENTITY_TYPE),
        ];
    }

    protected function getEntityValidationConstraints_reservation(): array {
        return [
            'geefter_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'gift_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'type' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'amount' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsType('null'),
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_reservation(): array {
        return [
            'geefter' => $this->getIsRelationshipEntityConstraint(Geefter::ENTITY_TYPE),
            'gift' => $this->getIsRelationshipEntityConstraint(Gift::ENTITY_TYPE),
        ];
    }

    protected function getEntityValidationConstraints_giftList(): array {
        return [
            'giftlist_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
            'name' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('string')
            ),
            'visibility' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsEqual(GiftList\Field\Visibility::PUBLIC),
                new \PHPUnit\Framework\Constraint\IsEqual(GiftList\Field\Visibility::PROTECTED),
                new \PHPUnit\Framework\Constraint\IsEqual(GiftList\Field\Visibility::PRIVATE),
                new \PHPUnit\Framework\Constraint\IsNull(),
            ),
            'status' => Assert::logicalOr(
                new \PHPUnit\Framework\Constraint\IsEqual(GiftList\Field\Status::ACTIVE),
                new \PHPUnit\Framework\Constraint\IsEqual(GiftList\Field\Status::ARCHIVED),
                new \PHPUnit\Framework\Constraint\IsNull(),
            ),
            'owner_id' => Assert::logicalAnd(
                new \PHPUnit\Framework\Constraint\IsType('numeric')
            ),
        ];
    }

    protected function getEntityRelationshipsValidationConstraints_giftList(): array {
        return [
            'owner' => $this->getIsRelationshipEntityConstraint(Geefter::ENTITY_TYPE),
            'gifts' => $this->getIsRelationshipEntityCollectionConstraint(Gift::ENTITY_TYPE),
        ];
    }

    /**
     * @param string|null $entityType
     * @param int $entityId
     */
    protected function getIsRelationshipEntityConstraint($entityType, $entityId = null): \PHPUnit\Framework\Constraint\Constraint {
        return new class($this, $entityType, $entityId) extends Constraint {
            private ?string $failureMessage = null;

            /**
             * @param object|\Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait $self
             * @param string $expectedEntityType
             * @param null|int $expectedEntityId
             */
            public function __construct(private $self, private $expectedEntityType, private $expectedEntityId)
            {
            }

            /**
             * @param mixed $other
             */
            protected function matches($other): bool {
                try {
                    $this->self->assertArrayHasKey('data', $other);
                    $this->self->assertIsValidResourceObjectIdentifier(
                        $other['data'],
                        $this->expectedEntityType,
                        $this->expectedEntityId,
                    );
                    // TODO
                    //$this->self->assertArrayHasKey('links', $other);
                    // ...

                    return true;
                }
                /*catch (\PHPUnit\Framework\IncompleteTestError | \PHPUnit\Framework\SkippedTestError $e) {
                    throw $e;
                }*/
                catch (\PHPUnit\Framework\AssertionFailedError $assertionFailedError) {
                    $this->failureMessage = $assertionFailedError->getMessage();

                    return false;
                }
            }

            public function toString(): string {
                return sprintf(
                    'is a valid entity of type %s (%s)',
                    $this->expectedEntityType,
                    $this->failureMessage
                );
            }
        };
    }

    /**
     * @param string $entityType
     * @param null|int[] $entityIds
     */
    protected function getIsRelationshipEntityCollectionConstraint($entityType, $entityIds = null): \PHPUnit\Framework\Constraint\Constraint {
        return new class($this, $entityType, $entityIds) extends Constraint {
            private ?string $failureMessage = null;

            /**
             * @param object|\Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait $self
             * @param string $expectedEntityType
             * @param null|int[] $expectedEntityIds
             */
            public function __construct(
                private $self,
                private $expectedEntityType,
                /** @var null|int[] */
                private $expectedEntityIds
            )
            {
            }

            /**
             * @param mixed $other
             */
            protected function matches($other): bool {
                try {
                    $this->self->assertArrayHasKey('data', $other);
                    $this->self->assertIsValidJsonApiCollection(
                        $other['data'],
                        $this->expectedEntityType,
                        $this->expectedEntityIds,
                        $this->self->new(Params::class, [
                            'objectType' => Constants::PARAM_OBJECT_TYPE_RESOURCE_ID
                        ])
                    );
                    // TODO
                    //$this->self->assertArrayHasKey('links', $other);
                    // ...

                    return true;
                }
                /*catch (\PHPUnit\Framework\IncompleteTestError | \PHPUnit\Framework\SkippedTestError $e) {
                    throw $e;
                }*/
                catch (\PHPUnit\Framework\AssertionFailedError $assertionFailedError) {
                    $this->failureMessage = $assertionFailedError->getMessage();

                    return false;
                }
            }

            public function toString(): string {
                return sprintf(
                    'is a valid collection of entities of type %s (%s)',
                    $this->expectedEntityType,
                    $this->failureMessage
                );
            }
        };
    }

    protected function getIsRelationshipEntityNullConstraint(): \PHPUnit\Framework\Constraint\Constraint {
        return new class($this) extends Constraint {
            private ?string $failureMessage = null;

            /**
             * @param object|\Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait $self
             */
            public function __construct(private $self)
            {
            }

            /**
             * @param mixed $other
             */
            protected function matches($other): bool {
                try {
                    $this->self->assertArrayHasKey('data', $other);
                    $this->self->assertNull($other['data']);

                    return true;
                }
                    /*catch (\PHPUnit\Framework\IncompleteTestError | \PHPUnit\Framework\SkippedTestError $e) {
                        throw $e;
                    }*/
                catch (\PHPUnit\Framework\AssertionFailedError $assertionFailedError) {
                    $this->failureMessage = $assertionFailedError->getMessage();

                    return false;
                }
            }

            public function toString(): string {
                return sprintf(
                    'is an empty (null) entity (%s)',
                    $this->failureMessage
                );
            }
        };
    }

    /**
     * @param array $constructorArgs
     * @return Params
     */
    public function newParams($constructorArgs = []) {
        return $this->getContainer()->make(Params::class, $constructorArgs);
    }
}
