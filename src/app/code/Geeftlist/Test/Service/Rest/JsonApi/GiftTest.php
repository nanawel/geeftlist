<?php

namespace Geeftlist\Test\Service\Rest\JsonApi;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Service\Rest\JsonApi\Traits\ValidationTrait;
use Geeftlist\Test\Util\TestCaseTrait;
use Geeftlist\Test\Util\TestDataTrait;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Model\Rest\RequestInterface;
use OliveOil\Core\Model\Rest\ResponseInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use OliveOil\Core\Test\Util\Rest\JsonApiTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group api
 * @group services
 */
class GiftTest extends TestCase
{
    use ValidationTrait;
    use FakerTrait;
    use TestCaseTrait;
    use TestDataTrait;
    use JsonApiTrait;
    protected function setUp(): void {
        parent::setUp();
        $this->getGeefterSession()->invalidate();
    }

    protected function tearDown(): void {
        parent::tearDown();
        $this->getGeefterSession()->invalidate();
    }

    public function getEntityCode(): string {
        return Gift::ENTITY_TYPE;
    }

    /**
     * @return \Geeftlist\Service\Rest\JsonApi\ModelHandler\Gift
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Service\Rest\JsonApi\ModelHandler\Gift::class);
    }

    /**
     * @group ticket-396
     * @group ticket-478
     */
    public function testGet_item(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $richardGiftsToHimself = $this->generateGifts(
            $richard,
            $richard->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            1
        );
        $richardGiftsToRichard2 = $this->generateGifts(
            $richard,
            $richard2->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            1
        );

        // We're going to retrieve geeftees so we need to disable geefterAccess listeners because Richards
        // do not share a common family here
        $this->disableGeefterAccessListeners();

        $this->setGeefterInSession($richard);

        // 1. GET Richard's gift to himself
        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        $request->setRouteParam('id', key($richardGiftsToHimself));

        $this->getCut()->get($request, $response);

        $document = $this->toJsonApiDocument($response);

        $this->assertEquals(Gift::ENTITY_TYPE, $document->get('data.type'));
        $this->assertEquals(key($richardGiftsToHimself), $document->get('data.id'));
        $this->assertFalse($document->has('data.relationships.geeftees.data.0.type'));
        $this->assertFalse($document->has('data.relationships.geeftees.data.0.id'));

        $this->assertFalse($document->has('included'));

        // 2. GET Richard's gift to Richard2
        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        $request->setRouteParam('id', key($richardGiftsToRichard2));

        $this->getCut()->get($request, $response);

        $document = $this->toJsonApiDocument($response);

        $this->assertEquals(Gift::ENTITY_TYPE, $document->get('data.type'));
        $this->assertEquals(key($richardGiftsToRichard2), $document->get('data.id'));
        $this->assertFalse($document->has('data.relationships.geeftees.data.0.type'));
        $this->assertFalse($document->has('data.relationships.geeftees.data.0.id'));

        $this->assertFalse($document->has('included'));
    }

    /**
     * @group ticket-396
     */
    public function testGet_itemWithInclude(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $richardGiftsToHimself = $this->generateGifts(
            $richard,
            $richard->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            1
        );
        $richardGiftsToRichard2 = $this->generateGifts(
            $richard,
            $richard2->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            1
        );

        // We're going to retrieve geeftees so we need to disable geefterAccess listeners because Richards
        // do not share a common family here
        $this->disableGeefterAccessListeners();

        $this->setGeefterInSession($richard);

        // 1. GET Richard's gift to himself
        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        $request->setRouteParam('id', key($richardGiftsToHimself))
            ->setQueryParam('include', 'geeftees,geeftees.geefter');

        $this->getCut()->get($request, $response);

        $document = $this->toJsonApiDocument($response);

        // Check primary data
        $this->assertEquals(Gift::ENTITY_TYPE, $document->get('data.type'));
        $this->assertEquals(key($richardGiftsToHimself), $document->get('data.id'));
        $this->assertEquals(Geeftee::ENTITY_TYPE, $document->get('data.relationships.geeftees.data.0.type'));
        $this->assertEquals($richard->getGeeftee()->getId(), $document->get('data.relationships.geeftees.data.0.id'));

        // Check included resources
        $this->assertEquals(2, count($includedIdx = $document->get('included')->getKeys()));
        $this->assertEquals(Geeftee::ENTITY_TYPE, $document->get('included.0')->get('type'));
        $this->assertEquals($richard->getGeeftee()->getId(), $document->get('included.0')->get('id'));
        $this->assertTrue($document->get('included.0')->has('attributes'));
        $this->assertEquals(Geefter::ENTITY_TYPE, $document->get('included.1')->get('type'));
        $this->assertEquals($richard->getId(), $document->get('included.1')->get('id'));
        $this->assertTrue($document->get('included.1')->has('attributes'));

        // 2. GET Richard's gift to Richard2
        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        $request->setRouteParam('id', key($richardGiftsToRichard2))
            ->setQueryParam('include', 'geeftees,geeftees.geefter');

        $this->getCut()->get($request, $response);

        $document = $this->toJsonApiDocument($response);

        // Check primary data
        $this->assertEquals(Gift::ENTITY_TYPE, $document->get('data.type'));
        $this->assertEquals(key($richardGiftsToRichard2), $document->get('data.id'));
        $this->assertEquals(Geeftee::ENTITY_TYPE, $document->get('data.relationships.geeftees.data.0.type'));
        $this->assertEquals($richard2->getGeeftee()->getId(), $document->get('data.relationships.geeftees.data.0.id'));

        // Check included resources
        $this->assertEquals(2, count($includedIdx = $document->get('included')->getKeys()));
        $this->assertEquals(Geeftee::ENTITY_TYPE, $document->get('included.0')->get('type'));
        $this->assertEquals($richard2->getGeeftee()->getId(), $document->get('included.0')->get('id'));
        $this->assertTrue($document->get('included.0')->has('attributes'));
        $this->assertEquals(Geefter::ENTITY_TYPE, $document->get('included.1')->get('type'));
        $this->assertEquals($richard2->getId(), $document->get('included.1')->get('id'));
        $this->assertTrue($document->get('included.1')->has('attributes'));
    }

    /**
     * @group ticket-478
     */
    public function testGet_itemWithIncludeAndFilters(): void {
        $richard = $this->getRichard();

        $richardGiftsToHimself = $this->generateGifts(
            $richard,
            $richard->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            1
        );

        // We're going to retrieve geeftees so we need to disable geefterAccess listeners because Richards
        // do not share a common family here
        $this->disableGeefterAccessListeners();

        $this->setGeefterInSession($richard);

        // 1. GET Richard's gift to himself
        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        $request->setRouteParam('id', key($richardGiftsToHimself))
            ->setQueryParam('include', 'geeftees,geeftees.geefter')
            ->setQueryParam('filter', [
                'geeftees.geefter.' . RepositoryInterface::WILDCARD_ID_FIELD => ['ne' => $richard->getId()]
            ]);

        $this->getCut()->get($request, $response);

        $document = $this->toJsonApiDocument($response);

        // Check primary data
        $this->assertEquals(Gift::ENTITY_TYPE, $document->get('data.type'));
        $this->assertEquals(key($richardGiftsToHimself), $document->get('data.id'));
        $this->assertEquals(Geeftee::ENTITY_TYPE, $document->get('data.relationships.geeftees.data.0.type'));
        $this->assertEquals($richard->getGeeftee()->getId(), $document->get('data.relationships.geeftees.data.0.id'));

        // Check included resources
        $this->assertEquals(1, count($includedIdx = $document->get('included')->getKeys()));
        $this->assertEquals(Geeftee::ENTITY_TYPE, $document->get('included.0')->get('type'));
        $this->assertEquals($richard->getGeeftee()->getId(), $document->get('included.0')->get('id'));
        $this->assertTrue($document->get('included.0')->has('attributes'));
        // Even if included, geefter should not appear here (filtered)
    }

    /**
     * @group ticket-401
     */
    public function testGet_collection(): void {
        $richard = $this->getRichard();
        $richard2 = $this->getRichard(true, 2);

        $richardGiftsToHimself = $this->generateGifts(
            $richard,
            $richard->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            5
        );
        $richardGiftsToRichard2 = $this->generateGifts(
            $richard,
            $richard2->getGeeftee(),
            Gift\Field\Status::AVAILABLE,
            5
        );
        $allGiftIds = array_merge(array_keys($richardGiftsToHimself), array_keys($richardGiftsToRichard2));

        $this->setGeefterInSession($richard);

        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        // 1. Simple request
        $this->getCut()->get($request, $response);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $responseData = $response->getData();
        $this->assertIsArray($responseData);
        $this->assertCount(10, $responseData);
        $this->assertIsValidJsonApiCollection($responseData, Gift::ENTITY_TYPE, $allGiftIds);

        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        // 2. Request using 1 filter
        $randomIds = $this->faker()->randomElements($allGiftIds, 5);
        $request->setQueryParams([
            'filter' => [
                'gift_id' => ['in' => $randomIds]
            ]
        ]);
        $this->getCut()->get($request, $response);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $responseData = $response->getData();
        $this->assertIsArray($responseData);
        $this->assertCount(5, $responseData);
        $this->assertIsValidJsonApiCollection($responseData, Gift::ENTITY_TYPE, $randomIds);

        /** @var RequestInterface $request */
        $request = $this->getContainer()->make(RequestInterface::class);
        /** @var ResponseInterface $response */
        $response = $this->getContainer()->make(ResponseInterface::class);

        // 3. Request using 1 complex filter and sort order
        $request->setQueryParams([
            'filter' => [
                'geeftees' => ['in' => [$richard2->getGeeftee()->getId()]]
            ],
            'sort' => '-label'
        ]);
        $this->getCut()->get($request, $response);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $responseData = $response->getData();
        $this->assertIsArray($responseData);
        $this->assertCount(5, $responseData);
        $this->assertIsValidJsonApiCollection($responseData, Gift::ENTITY_TYPE, array_keys($richardGiftsToRichard2));
        $sortedLabels = array_column($responseData, 'label');
        rsort($sortedLabels);
        $this->assertEquals($sortedLabels, array_column($responseData, 'label'));
    }
}
