<?php

namespace Geeftlist\Test\Service\Avatar;

use Geeftlist\Model\Geeftee;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;


/**
 * @group base
 * @group services
 */
class GeefteeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    public function testGenerateAvatar(): void {
        $geeftee = $this->newEntity(Geeftee::ENTITY_TYPE, ['data' => [
            'name' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX),
        ]])->save();

        $this->assertNull($geeftee->getAvatarPath());

        $cut = $this->getCut();
        $path = $cut->generateAvatar($geeftee);

        $this->assertIsString($path);
        $this->assertFileExists($fullPath = $cut->getAvatarBasePath() . '/' . $path);
        $this->assertEquals('image/png', mime_content_type($fullPath));
    }

    /**
     * @return \Geeftlist\Service\Avatar\Geeftee
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    protected function getCut() {
        return $this->getContainer()->make(\Geeftlist\Service\Avatar\Geeftee::class);
    }
}
