<?php

namespace Geeftlist\Test\Service;

use Geeftlist\Model\Geefter;
use Geeftlist\Model\Invitation;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Helper\DateTime;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;


/**
 * @group base
 * @group services
 */
class InvitationTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;

    protected function setUp(): void {
        parent::setUp();
        /** @var RepositoryInterface $invitationRepository */
        $invitationRepository = $this->getRepository(Invitation::ENTITY_TYPE);
        $invitationRepository->deleteAll();

        $this->getAppConfig()->setValue('INVITATION_ENABLED', true);
        $this->getAppConfig()->setValue('INVITATION_MAX_PER_GEEFTER', 0);
    }

    /**
     * @group ticket-668
     */
    public function testGenerateLink(): void {
        $john = $this->getJohn();
        $this->setGeefterInSession($john);
        list($url, $invitation) = $this->newInvitation($john);

        $this->assertStringEndsWith(
            $this->getUrl('signup', [
                '_query' => [\Geeftlist\Service\Invitation::INVITATION_TOKEN_URL_PARAM => $invitation->getToken()]]
            ),
            $url
        );
    }

    /**
     * @group ticket-668
     */
    public function testValidate_valid(): void {
        $john = $this->getJohn();
        $this->setGeefterInSession($john);
        list($url, $invitation) = $this->newInvitation($john);

        $validatedInvitation = $this->getCut()->validate($invitation);
        $this->assertInstanceOf(Invitation::class, $validatedInvitation);
        $this->assertEquals($john->getId(), $validatedInvitation->getCreatorId());
        $this->assertNotEmpty($validatedInvitation->getToken());
    }

    /**
     * @group ticket-668
     */
    public function testValidate_invalidDisabled(): void {
        $john = $this->getJohn();
        $this->setGeefterInSession($john);
        list($url, $invitation) = $this->newInvitation($john);

        $this->getAppConfig()->setValue('INVITATION_ENABLED', false);

        $this->expectExceptionMessage('Sorry, invitations are disabled on this server.');
        $this->getCut()->validate($invitation);
    }

    /**
     * @group ticket-668
     */
    public function testValidate_unsavedModel(): void {
        /** @var Invitation $fixture */
        $fixture = $this->newEntity(Invitation::ENTITY_TYPE, ['data' => [
            'creator_id' => $this->getJohn()->getId(),
            'token' => 123,
            'expires_at' => DateTime::getDateSql('1 day')
        ]]);

        $this->expectExceptionMessage('Invalid invitation token provided.');
        $this->getCut()->validate($fixture);
    }

    /**
     * @group ticket-668
     */
    public function testValidate_expiredToken(): void {
        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        /** @var Invitation $fixture */
        $fixture = $this->newEntity(Invitation::ENTITY_TYPE, ['data' => [
            'creator_id' => $john->getId(),
            'token' => $this->getCryptService()->generateToken(),
            'expires_at' => DateTime::getDateSql('-1h')
        ]])->save();

        $this->expectExceptionMessage('This invitation token is expired.');
        $this->getCut()->validate($fixture);
    }

    /**
     * @group ticket-668
     */
    public function testValidate_alreadyUsed(): void {
        $john = $this->getJohn();
        $this->setGeefterInSession($john);

        /** @var Invitation $fixture */
        $fixture = $this->newEntity(Invitation::ENTITY_TYPE, ['data' => [
            'creator_id' => $john->getId(),
            'token' => $this->getCryptService()->generateToken(),
            'expires_at' => DateTime::getDateSql('1 day'),
            'geefter_id' => $this->getGeefterButJohn()->getId(),
        ]])->save();

        $this->expectExceptionMessage('This invitation has already been used.');
        $this->getCut()->validate($fixture);
    }

    /**
     * @group ticket-668
     */
    public function testValidateToken_invalidToken(): void {
        $this->expectExceptionMessage('Invalid invitation token provided.');
        $this->getCut()->validateToken($this->getCryptService()->generateToken());
    }

    /**
     * @return array[string-url, Geefter]
     */
    public function newInvitation(Geefter $asGeefter): array {
        $url = $this->getCut()->generateSignupLink($asGeefter);
        /** @var Invitation $newFixture */
        $newFixture = $this->getEntity(Invitation::ENTITY_TYPE);

        return [$url, $newFixture];
    }

    /**
     * @return \Geeftlist\Service\Invitation
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Service\Invitation::class);
    }
}
