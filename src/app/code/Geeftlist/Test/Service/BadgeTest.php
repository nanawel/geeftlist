<?php

namespace Geeftlist\Test\Service;

use Geeftlist\Exception\BadgeException;
use Geeftlist\Exception\Security\PermissionException;
use Geeftlist\Model\Badge;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Test\Util\TestCaseTrait;
use OliveOil\Core\Model\RepositoryInterface;
use OliveOil\Core\Test\Util\FakerTrait;
use PHPUnit\Framework\TestCase;


/**
 * @group base
 * @group services
 */
class BadgeTest extends TestCase
{
    use FakerTrait;
    use TestCaseTrait;
    protected $badges = [];

    protected function setUp(): void {
        parent::setUp();
        $this->enableGeefterAccessListeners();
        $this->unsetGeefterInSession();

        if (!$this->badges) {
            /** @var RepositoryInterface $badgeRepository */
            $badgeRepository = $this->getRepository(Badge::ENTITY_TYPE);
            $badgeRepository->deleteAll([
                'filter' => [
                    'type' => [['eq' => 'ut']]
                ]
            ]);

            $geefteeBadge1 = $this->newEntity(Badge::ENTITY_TYPE, ['data' => [
                'entity_types'    => [Geeftee::ENTITY_TYPE],
                'type'            => 'ut',
                'code'            => 'foo',
                'translated_data' => [
                    'fr' => [
                        'label' => 'Foo LABEL FR',
                        'description' => 'Foo DESC FR',
                    ],
                    'en' => [
                        'label' => 'Foo LABEL EN',
                        'description' => 'Foo DESC EN',
                    ]
                ]
            ]]);

            $badgeRepository->save($geefteeBadge1);
            $geefteeBadge2 = $this->newEntity(Badge::ENTITY_TYPE, ['data' => [
                'entity_types'    => [Geeftee::ENTITY_TYPE],
                'type'            => 'ut',
                'code'            => 'bar',
                'translated_data' => [
                    'fr' => [
                        'label' => 'Bar LABEL FR',
                        'description' => 'Bar DESC FR',
                    ],
                    'en' => [
                        'label' => 'Bar LABEL EN',
                        'description' => 'Bar DESC EN',
                    ]
                ]
            ]]);
            $badgeRepository->save($geefteeBadge2);

            $this->badges[Geeftee::ENTITY_TYPE] = [
                $geefteeBadge1->getId() => $geefteeBadge1,
                $geefteeBadge2->getId() => $geefteeBadge2
            ];
        }
    }

    protected function tearDown(): void {
        parent::tearDown();

        /** @var RepositoryInterface $badgeRepository */
        $badgeRepository = $this->getRepository(Badge::ENTITY_TYPE);
        $badgeRepository->deleteAll([
            'filter' => [
                'type' => [['eq' => 'ut']]
            ]
        ]);
    }

    /**
     * @param string|null $entityType
     * @return Badge[]|Badge[]
     */
    public function getTestBadgeIds($entityType = null) {
        return $entityType ? $this->badges[$entityType] : $this->badges;
    }

    public function testSetBadgeIds_notAllowed(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $entity = $richard->getGeeftee();

        $badges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity' => [
                    ['eq' => [$entity->getEntityType(), $entity->getId()]]
                ]
            ]
        ]);
        $this->assertEmpty($badges);

        $geefteeBadges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity_type' => [
                    ['eq' => Geeftee::ENTITY_TYPE]
                ],
                'type' => [['eq' => 'ut']]
            ]
        ]);
        $this->assertNotEmpty($geefteeBadges);

        $this->setGeefterInSession($this->getJohn());

        try {
            $cut->setBadgeIds($entity, array_keys($this->badges[Geeftee::ENTITY_TYPE]));
            $this->fail('Failed preventing set badge as a non-allowed geefter.');
        }
        catch (PermissionException) {}

        $badges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity' => [
                    ['eq' => [$entity->getEntityType(), $entity->getId()]]
                ]
            ]
        ]);
        $this->assertEmpty($badges);
    }

    public function testSetBadgeIds_invalid(): void {
        $cut = $this->getCut();

        $richard = $this->getRichard();
        $entity = $richard->getGeeftee();

        // Preconditions
        $badges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity' => [
                    ['eq' => [$entity->getEntityType(), $entity->getId()]]
                ]
            ]
        ]);
        $this->assertEmpty($badges);

        // Invalid IDs
        try {
            $cut->setBadgeIds($entity, [0]);
            $this->fail('Failed preventing setting invalid badge IDs.');
        }
        catch (BadgeException $badgeException) {}

        $entity = $this->newEntity(Gift::ENTITY_TYPE, ['data' => [
            'label' => uniqid(\Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX),
            'creator_id' => $richard->getId()
        ]])->save();

        // Wrong entity_type for badges
        try {
            $cut->setBadgeIds($entity, array_keys($this->badges[Geeftee::ENTITY_TYPE]));
            $this->fail('Failed preventing setting invalid badge IDs.');
        }
        catch (BadgeException) {}
    }

    public function testSetBadgeIds_valid(): void {
        $cut = $this->getCut();

        $entity = $this->getRichard()->getGeeftee();

        // Preconditions
        $badges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity' => [
                    ['eq' => [$entity->getEntityType(), $entity->getId()]]
                ]
            ]
        ]);
        $this->assertEmpty($badges);

        $geefteeBadges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity_type' => [
                    ['eq' => Geeftee::ENTITY_TYPE]
                ],
                'type' => [['eq' => 'ut']]
            ]
        ]);
        $this->assertNotEmpty($geefteeBadges);

        // Set the 2 test badges
        $cut->setBadgeIds($entity, array_keys($geefteeBadges));

        $actualBadges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity' => [
                    ['eq' => [$entity->getEntityType(), $entity->getId()]]
                ]
            ]
        ]);

        // Check result
        $this->assertEqualsCanonicalizing($geefteeBadges, $actualBadges);

        // Now set an empty set
        $cut->setBadgeIds($entity, []);

        $actualBadges = $this->getEntityItems(Badge::ENTITY_TYPE, [
            'filter' => [
                'entity' => [
                    ['eq' => [$entity->getEntityType(), $entity->getId()]]
                ]
            ]
        ]);

        // Check result
        $this->assertEmpty($actualBadges);
    }

    public function testGetBadgesForEntityId_empty(): void {
        $cut = $this->getCut();

        $entity = $this->getRichard()->getGeeftee();

        $this->assertEmpty($cut->getBadgesForEntityId($entity->getEntityType(), $entity->getId()));
    }

    public function testGetBadgesForEntityId_notEmpty(): void {
        $cut = $this->getCut();

        $entity = $this->getRichard()->getGeeftee();
        $cut->setBadgeIds($entity, array_keys($this->badges[$entity->getEntityType()]));

        $this->assertEqualsCanonicalizing(
            array_keys($this->badges[$entity->getEntityType()]),
            array_keys($cut->getBadgesForEntityId($entity->getEntityType(), $entity->getId()))
        );
    }

    public function testGetBadgesForEntityType_empty(): void {
        $cut = $this->getCut();

        $this->assertEmpty($cut->getBadgesForEntityType(Geefter::ENTITY_TYPE));
    }

    public function testGetBadgesForEntityType_notEmpty(): void {
        $cut = $this->getCut();

        $entity = $this->getRichard()->getGeeftee();

        $actualEntityBadgeIds = array_keys($cut->getBadgesForEntityType($entity->getEntityType()));
        foreach ($this->badges[$entity->getEntityType()] as $badgeId => $badge) {
            $this->assertContainsEquals($badgeId, $actualEntityBadgeIds);
        }
    }

    /**
     * @return \Geeftlist\Service\Badge
     * @throws \DI\NotFoundException
     */
    public function getCut() {
        return $this->getContainer()->get(\Geeftlist\Service\Badge::class);
    }
}
