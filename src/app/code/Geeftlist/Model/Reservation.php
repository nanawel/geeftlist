<?php

namespace Geeftlist\Model;

use OliveOil\Core\Helper\DateTime;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Reservation getResource()
 *
 * @method string getType()
 * @method $this setType(string $type)
 * @method int getGeefterId()
 * @method $this setGeefterId(int $geefterId)
 * @method int getGiftId()
 * @method $this setGiftId(int $giftId)
 * @method float getAmount()
 * @method $this setAmount(float $amount)
 * @method string getReservedAt()
 * @method $this setReservedAt(string $reservedAt)
 * @method string getStaleNotificationSentAt()
 * @method $this setStaleNotificationSentAt(string $staleNotificationSentAt)
 */
class Reservation extends AbstractModel
{
    public const ENTITY_TYPE = 'reservation';

    public const TYPE_RESERVATION = 'reservation';

    public const TYPE_PURCHASE    = 'purchase';

    public const AVAILABLE_TYPES  = [
        self::TYPE_RESERVATION,
        self::TYPE_PURCHASE
    ];

    public function beforeSave(): static
    {
        parent::beforeSave();
        if (!$this->hasData('type')) {
            $this->setData('type', self::TYPE_RESERVATION);
        }

        if ($this->hasOrigData('type') && $this->getOrigData('type') != $this->getData('type')) {
            $this->setData('reserved_at', DateTime::getDateSql());
        }

        return $this;
    }

    /**
     * @return Gift|null
     */
    public function getGift() {
        if (!($gift = parent::getGift()) && ($giftId = $this->getGiftId())) {
            $gift = $this->modelFactory->resolveMake(Gift::ENTITY_TYPE);
            $this->setGift($gift->load($giftId));
            return $gift;
        }

        return $gift;
    }

    /**
     * @return Geefter|null
     */
    public function getGeefter() {
        if (!($geefter = parent::getGeefter()) && ($geefterId = $this->getGeefterId())) {
            $geefter = $this->modelFactory->resolveMake(Geefter::ENTITY_TYPE);
            $this->setGeefter($geefter->load($geefterId));
            return $geefter;
        }

        return $geefter;
    }

    /**
     * @return string
     */
    public function getGeefterUsername() {
        if (
            (! $geefterUsername = parent::getGeefterUsername())
            && $geefter = $this->getGeefter()
        ) {
            $this->setGeefterUsername($geefterUsername = $geefter->getUsername());
        }

        return $geefterUsername;
    }

    /**
     *
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    public function getGeeftees() {
        return $this->getResource()->getGeeftees($this);
    }
}
