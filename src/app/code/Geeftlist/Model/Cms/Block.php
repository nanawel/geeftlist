<?php

namespace Geeftlist\Model\Cms;


use Geeftlist\Model\AbstractModel;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Cms\Block getResource()
 * @method $this setCode(int $code)
 * @method int getCode()
 * @method $this setContent(string $content)
 * @method string getContent()
 * @method $this setContentType(string $content)
 * @method string getContentType()
 * @method $this setEnabled(bool $enabled)
 * @method bool getEnabled()
 * @method $this setLanguage(string $language)
 * @method string getLanguage()
 */
class Block extends AbstractModel
{
    public const ENTITY_TYPE = 'cms/block';

    protected string $eventObjectParam = 'block';

    /**
     * @param string $code
     * @param array|string $language
     * @return $this
     */
    public function loadByCodeAndLanguage($code, $language, $enabled = true): static {
        $this->getResource()->loadByCodeAndLanguage($this, $code, $language, $enabled);

        return $this;
    }

    public function beforeSave(): static {
        $this->setCode(preg_replace('/[^a-z0-9_-]+/', '-', strtolower($this->getCode())));

        return parent::beforeSave();
    }
}
