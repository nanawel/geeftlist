<?php

namespace Geeftlist\Model;


use OliveOil\Core\Model\ResourceModel\Iface\Collection;
use OliveOil\Core\Service\GenericFactory;

class ResourceFactory extends GenericFactory implements \OliveOil\Core\Model\FactoryInterface
{
    /**
     * @param string $code Entity code
     * @return Collection
     */
    public function makeCollectionFromCode(string $code, array $parameters = []): Collection {
        return parent::makeFromCode($code . '_collection', $parameters);
    }

    /**
     * @param string $rcn Relative Class Name
     * @return Collection
     */
    public function resolveMakeCollection(string $rcn, array $parameters = []): Collection {
        return parent::resolveMake($rcn . '/collection', $parameters);
    }

    /**
     * @param string $rcn Relative Class Name
     * @return string
     */
    public function resolveCollection(string $rcn, array $parameters = []): string {
        return parent::resolve($rcn . '/collection');
    }
}
