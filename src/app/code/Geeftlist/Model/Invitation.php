<?php

namespace Geeftlist\Model;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Invitation getResource()
 * @method int getCreatorId()
 * @method $this setCreatorId(int $creatorId)
 * @method string getToken()
 * @method $this setToken(string $token)
 * @method string getCreatedAt()
 * @method string getExpiresAt()
 * @method $this setExpiresAt(string $expiresAt)
 * @method string getUsedAt()
 * @method $this setUsedAt(string $expiresAt)
 * @method int getGeefterId()
 * @method $this setGeefterId(int $geefterId)
 */
class Invitation extends AbstractModel
{
    public const ENTITY_TYPE = 'invitation';

    /** @var Geefter */
    protected $creator;

    public function getCreator(): ?Geefter {
        if (!$this->creator && $this->getCreatorId()) {
            /** @var Geefter|null $creator */
            $creator = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
                ->get($this->getCreatorId());
            $this->creator = $creator;
        }

        return $this->creator;
    }
}
