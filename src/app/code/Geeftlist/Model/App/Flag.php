<?php

namespace Geeftlist\Model\App;


class Flag implements \OliveOil\Core\Model\App\FlagInterface
{
    public function __construct(
        protected \Geeftlist\Model\ResourceFactory $resourceFactory
    ) {
    }

    public function getResource(): \Geeftlist\Model\ResourceModel\Db\Flag {
        return $this->resourceFactory->get(\Geeftlist\Model\ResourceModel\Db\Flag::class);
    }

    public function get(string $path, mixed $default = null): mixed
    {
        return $this->getResource()->getValue($path) ?? $default;
    }

    public function set(string $path, mixed $value = null): static {
        $this->getResource()->setValue($path, $value);

        return $this;
    }

    public function del(string $path): static {
        $this->getResource()->deleteValue($path);

        return $this;
    }

    public function has(string $path): bool {
        return $this->getResource()->hasValue($path);
    }

    public function getAll(?string $prefix = null): array {
        return $this->getResource()->getAllValues($prefix);
    }
}
