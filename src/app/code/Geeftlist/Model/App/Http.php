<?php

namespace Geeftlist\Model\App;


use Narrowspark\HttpStatus\Exception\ServiceUnavailableException;
use OliveOil\Core\Model\App\Context;

class Http extends \Geeftlist\Model\App
{
    public function __construct(
        Context $context,
        protected \OliveOil\Core\Model\SetupInterface $setup
    ) {
        parent::__construct($context);
    }

    /**
     * @return $this
     */
    protected function setupResources(): static {
        parent::setupResources();

        if (!$this->setup->isDbUp()) {
            throw new ServiceUnavailableException("The database is currently unavailable.");
        }

        if (!$this->setup->isInstalled()) {
            throw new ServiceUnavailableException(
                "Geeftlist is not installed! You must run the setup from CLI first. Check the README to know more."
            );
        }

        return $this;
    }
}
