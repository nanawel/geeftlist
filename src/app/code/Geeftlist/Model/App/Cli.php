<?php

namespace Geeftlist\Model\App;

class Cli extends \Geeftlist\Model\App
{
    /**
     * @inheritDoc
     */
    protected function onSetup(): static {
        parent::onSetup();
        ini_set('max_execution_time', 0);

        return $this;
    }
}
