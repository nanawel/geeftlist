<?php

namespace Geeftlist\Model;

use OliveOil\Core\Exception\MissingConfigurationException;
use OliveOil\Core\Exception\SystemException;

class App extends \OliveOil\Core\Model\App
{
    /**
     * @throws \Exception
     */
    public function checkRequirements(): static {
        parent::checkRequirements();
        if (! extension_loaded('openssl')) {
            throw new SystemException('Missing openssl extension');
        }

        $requiredConfig = [
            'EMAIL_DEFAULT_SENDER',
        ];

        foreach ($requiredConfig as $rc) {
            if (! $this->config->getValue($rc)) {
                throw new MissingConfigurationException("Configuration key '$rc' must be set in INI file");
            }
        }

        return $this;
    }
}
