<?php

namespace Geeftlist\Model\Activity\Field;

use OliveOil\Core\Model\Entity\AbstractOptionsField;

class Action extends AbstractOptionsField
{
    public const CREATE   = 'create';

    public const UPDATE   = 'update';

    public const DELETE   = 'delete';

    public const JOIN     = 'join';

    public const LEAVE    = 'leave';

    public const VALUES = [
        self::CREATE,
        self::UPDATE,
        self::DELETE,
        self::JOIN,
        self::LEAVE,
    ];

    public const OPTIONS = [
        self::CREATE    => [
            'label'       => 'Create',
            'description' => 'Creation'
        ],
        self::UPDATE => [
            'label'       => 'Update',
            'description' => 'Update'
        ],
        self::DELETE   => [
            'label'       => 'Delete',
            'description' => 'Deletion'
        ],
        self::JOIN => [
            'label'       => 'Join',
            'description' => 'Join'
        ],
        self::LEAVE => [
            'label'       => 'Leave',
            'description' => 'Leave'
        ],
    ];
}
