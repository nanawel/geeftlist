<?php

namespace Geeftlist\Model\Session;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;

trait GeefterTrait
{
    /** @var \Geeftlist\Model\Geefter|null */
    protected ?\Geeftlist\Model\Geefter $geefter = null;

    /** @var \Geeftlist\Model\Geeftee|null */
    protected ?\Geeftlist\Model\Geeftee $geeftee = null;

    /** @var int[]|null */
    protected ?array $familyIds = null;

    /** @var string|null */
    protected ?string $language = null;

    public function isLoggedIn(): bool {
        return (bool) $this->getGeefter();
    }

    /**
     * @param Geefter|null $geefter
     * @throws \Exception
     */
    public function setGeefterAsLoggedIn(Geefter $geefter = null, array $options = []): bool {
        if ($geefter && $geefter->getId()) {
            $this->setGeefter($geefter);

            return true;
        }

        return false;
    }

    protected function setGeefter(Geefter $geefter) {
        // Enforce geefter/geefter_id sync
        $this->setGeefterId($geefter->getId());
        $this->geefter = $geefter;

        return $this;
    }

    /**
     * @see \Geeftlist\Model\Session\GeefterInterface::logout()
     * @return $this
     */
    public function logout() {
        $this->invalidate()
            ->init();

        return $this;
    }

    /**
     * @return Geefter|null
     */
    public function getGeefter() {
        $geefterId = $this->getGeefterId();
        if ($this->geefter && $this->geefter->getId() != $geefterId) {
            // If geefter ID has changed, clear current cached model
            $this->geefter = null;
        }

        if (!$this->geefter && $geefterId) {
            /** @var \Geeftlist\Model\Geefter $geefter */
            $geefter = $this->getGeefterRepository()->get($geefterId);
            if ($geefter && $geefter->getId()) {
                $this->setGeefter($geefter);
            }
        }

        return $this->geefter;
    }

    /**
     * @return Geeftee|null
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function getGeeftee() {
        return $this->getGeefter()
            ? $this->getGeefter()->getGeeftee()
            : null;
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Family\Collection|null
     */
    public function getFamilyCollection() {
        if ($currentGeefter = $this->getGeefter()) {
            return $currentGeefter->getFamilyCollection();
        }

        return null;
    }

    /**
     * @return int[]
     */
    public function getFamilyIds() {
        if ($this->familyIds === null) {
            $this->familyIds = ($families = $this->getFamilyCollection()) ? $families->getAllIds() : [];
        }

        return $this->familyIds;
    }

    /**
     * @return string
     */
    public function getLanguage() {
        $currentGeefter = $this->getGeefter();
        if (! $currentGeefter || ! ($language = $currentGeefter->getLanguage())) {
            // Get app default language
            $language = $this->getI18n()->getLanguage();
        }

        return $language;
    }

    /**
     * @param string $lang
     * @return $this
     */
    public function setLanguage($lang) {
        $this->language = $lang;
        $this->getI18n()->setLanguage($lang)
            ->setGlobalLanguage($lang);

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode() {
        if (!$this->getData('currency_code')) {
            $this->setData('currency_code', $this->getAppConfig()->getValue('CURRENCY'));
        }

        return $this->getData('currency_code');
    }

    /**
     * @return string
     */
    public function getTimezone() {
        if (!$this->getData('timezone')) {
            $this->setData('timezone', $this->getAppConfig()->getValue('TIMEZONE'));
        }

        return $this->getData('timezone');
    }

    /**
     * @return $this
     */
    public function resetGeefterData() {
        $this->geefter = null;
        $this->geeftee = null;
        $this->familyIds = null;
        $this->language = null;

        return $this;
    }
}
