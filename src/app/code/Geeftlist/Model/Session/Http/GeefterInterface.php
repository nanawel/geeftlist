<?php


namespace Geeftlist\Model\Session\Http;


use OliveOil\Core\Model\Session\HttpInterface;

interface GeefterInterface extends HttpInterface, \Geeftlist\Model\Session\GeefterInterface
{
}
