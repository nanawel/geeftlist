<?php

namespace Geeftlist\Model\Session\Http;

use Geeftlist\Model\Session\GeefterTrait;
use OliveOil\Core\Model\Session\Http;

/**
 * Class Geefter
 *
 * @method int getGeefterId()
 * @method int getGeefteeId()
 * @method $this setAfterLoginUrl(string $url)
 * @method string|null getAfterLoginUrl()
 * @method $this setPasswordValidationHash(string $hash)
 * @method string|null getPasswordValidationHash()
 */
class Geefter extends Http implements GeefterInterface
{
    use GeefterTrait {
        setGeefterAsLoggedIn as geefterTrait_setGeefterAsLoggedIn;
    }

    public function __construct(
        \OliveOil\Core\Model\Session\Context $context,
        \OliveOil\Core\Model\Session\Http\PhpSession $phpSession,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        $name = 'geefter'
    ) {
        parent::__construct(
            $context,
            $phpSession,
            $name
        );
    }

    /**
     * @inheritDoc
     */
    public function setGeefterAsLoggedIn(\Geeftlist\Model\Geefter $geefter = null, array $options = []): bool {
        if ($this->geefterTrait_setGeefterAsLoggedIn($geefter, $options)) {
            if (isset($options['type'])) {
                // Must set session type *before* regenerating to send the "Expires" value with the cookie update
                $this->setType($options['type']);
            }

            $this->regenerate();

            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function invalidate(): static {
        parent::invalidate()
            ->resetGeefterData();   // Also clear GeefterTrait's properties

        return $this;
    }

    public function getGeefterRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefterRepository;
    }
}
