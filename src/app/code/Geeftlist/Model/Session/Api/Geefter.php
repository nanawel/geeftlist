<?php

namespace Geeftlist\Model\Session\Api;

use Geeftlist\Model\Session\GeefterInterface;
use Geeftlist\Model\Session\GeefterTrait;
use OliveOil\Core\Model\Session;

/**
 * @method int|null getGeefterId()
 */
class Geefter extends Session\Api implements GeefterInterface
{
    use GeefterTrait;

    public function __construct(
        \OliveOil\Core\Model\Session\Context $context,
        protected \OliveOil\Core\Model\RepositoryInterface $geefterRepository,
        $name
    ) {
        parent::__construct($context, $name);
    }

    /**
     * @inheritDoc
     */
    public function invalidate(): static {
        parent::invalidate()
            ->resetGeefterData();   // Also clear GeefterTrait's properties

        return $this;
    }

    public function getGeefterRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->geefterRepository;
    }
}
