<?php

namespace Geeftlist\Model\Session;

use Geeftlist\Model\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Family\Collection;
use OliveOil\Core\Exception\SessionException;
use OliveOil\Core\Model\SessionInterface;

/**
 * Interface GeefterInterface
 *
 * @method int|null getGeefterId()
 */
interface GeefterInterface extends SessionInterface
{
    /**
     * @return bool
     */
    public function isLoggedIn();

    /**
     * @return bool
     * @throws SessionException
     */
    public function setGeefterAsLoggedIn(\Geeftlist\Model\Geefter $geefter = null, array $options = []);

    /**
     * Invalidate current session, and start a new one.
     * In HTTP context, refer to the following methods:
     * @see \OliveOil\Core\Model\SessionInterface::invalidate()
     * @see \OliveOil\Core\Model\SessionInterface::init()
     *
     * @return $this
     * @throws SessionException
     */
    public function logout();

    /**
     * @return \Geeftlist\Model\Geefter
     */
    public function getGeefter();

    /**
     * @return Geeftee|null
     * @throws \OliveOil\Core\Exception\NoSuchEntityException
     */
    public function getGeeftee();

    /**
     * @return Collection|null
     */
    public function getFamilyCollection();

    /**
     * @return int[]
     */
    public function getFamilyIds();

    /**
     * @return string
     */
    public function getLanguage();

    /**
     * @param string $lang
     * @return $this
     */
    public function setLanguage($lang);

    /**
     * @return string
     */
    public function getCurrencyCode();

    /**
     * @return string
     */
    public function getTimezone();
}
