<?php

namespace Geeftlist\Model;

use Geeftlist\Exception\GeefterException;
use Geeftlist\Exception\Security\PermissionException;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Helper\DateTime;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Geefter getResource()
 *
 * @method string getUsername()
 * @method string getEmail()
 * @method $this setPassword(string $password)
 * @method string getPasswordToken()
 * @method string getPasswordTokenDate()
 * @method string getPasswordHash()
 * @method string getApiKey()
 * @method $this setApiKey(string|null $apiKey)
 * @method $this setLanguage(string $language)
 * @method string getLanguage()
 * @method string getPendingEmail()
 * @method $this setPendingEmail(string|null $pendingEmail)
 * @method string getPendingEmailToken()
 * @method $this setPendingEmailToken(string|null $pendingEmailToken)
 * @method string getEmailNotificationFreq()
 * @method $this setEmailNotificationFreq(string $emailNotificationFreq)
 * @method string getEmailNotificationLast()
 * @method $this setEmailNotificationLast(string|null $emailNotificationLast)
 * @method string getStaleReservationNotificationEnabled()
 * @method $this setStaleReservationNotificationEnabled(bool $staleReservationNotificationEnabled)
 * @method string getLastLoginAt()
 * @method $this setLastLoginAt(string $lastLoginAt)
 */
class Geefter extends AbstractModel
{
    public const ENTITY_TYPE = 'geefter';

    public const API_KEY_LENGTH = 40;

    /** @var int */
    protected $tokenLifetime;

    /** @var \OliveOil\Core\Model\I18n */
    protected $i18n;

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \OliveOil\Core\Service\Crypt $cryptService,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        array $data = [],
        $tokenLifetime = 2 * 24
    ) {
        parent::__construct($context, $data);
        $this->tokenLifetime = max(1, (int) $tokenLifetime);
    }

    /**
     * @deprecated Use Repository instead
     * @param string $email
     * @return $this
     */
    public function loadByEmail($email): static {
        $this->getResource()->load($this, trim((string) $email), 'email');

        return $this;
    }

    /**
     * @deprecated Use Repository instead
     * @param string $token
     * @return $this
     */
    public function loadByPasswordToken($token): static {
        $this->getResource()->load($this, trim((string) $token), 'password_token');
        if (!$this->checkPasswordTokenValidity()) {
            $this->clearInstance();
        }

        return $this;
    }

    /**
     * @deprecated Use Repository instead
     * @param string $token
     * @return $this
     */
    public function loadByPendingEmailToken($token): static {
        $this->getResource()->load($this, trim((string) $token), 'pending_email_token');

        return $this;
    }

    /**
     * @deprecated Use Repository instead
     * @param string $apiKey
     * @return $this
     */
    public function loadByApiKey($apiKey): static {
        $this->getResource()->load($this, trim((string) $apiKey), 'api_key');

        return $this;
    }

    public function getName(): string {
        return (string) $this->getUsername();
    }

    /**
     * Return the families this geefter is in
     *
     * @return \Geeftlist\Model\ResourceModel\Db\Family\Collection
     */
    public function getFamilyCollection() {
        return $this->getGeeftee()->getFamilyCollection();
    }

    public function beforeSave(): static {
        parent::beforeSave();

        $username = trim((string) $this->getUsername());
        if ($username === '') {
            throw new GeefterException('Username cannot be empty');
        }

        if ($this->hasPassword() && ($password = $this->getPassword())) {
            $this->setPasswordHash($this->cryptService->hash($password))
                ->setPassword(null);
        }

        return $this;
    }

    public function afterSave(): static {
        parent::afterSave();

        // Sync geeftee data
        $this->geefteeHelper->syncFromGeefters([$this]);

        return $this;
    }

    protected function checkPasswordTokenValidity(): bool {
        $tokenDate = DateTime::getDate($this->getPasswordTokenDate());
        $refDate = DateTime::getDate();
        $refDate->sub(new \DateInterval(sprintf('PT%dH', $this->tokenLifetime)));

        return $tokenDate >= $refDate;
    }

    public function generatePasswordToken(): static {
        $this->setPasswordToken($this->cryptService->generateToken());
        $this->setPasswordTokenDate(DateTime::getDateSql());

        return $this;
    }

    public function generateUpdateEmailToken(): static {
        $this->setPendingEmailToken($this->cryptService->generateToken());

        return $this;
    }

    public function regenerateApiKey(): static {
        $this->setApiKey($this->cryptService->generateToken(static::API_KEY_LENGTH));

        return $this;
    }

    /**
     * @return Geeftee
     * @throws PermissionException
     */
    public function getGeeftee() {
        if (! $geeftee = parent::getGeeftee()) {
            $geeftee = $this->modelFactory->resolveMake(Geeftee::ENTITY_TYPE)
                ->load($this->getId(), 'geefter_id');
            if (! $geeftee->getId()) {
                throw new NoSuchEntityException('Cannot find geeftee with geefter #' . $this->getId());
            }

            $this->setGeeftee($geeftee);
        }

        return $geeftee;
    }

    /**
     * Return geeftee profile URL
     */
    public function getProfileUrl(array $params = []): string {
        return $this->urlHelper->getGeefteeProfileUrl($this, $params);
    }

    /**
     * Return geeftee profile avatar URL
     *
     * @return string
     */
    public function getAvatarUrl(array $params = []) {
        return $this->urlHelper->getGeefterAvatarUrl($this, $params);
    }

    public function assertEmailUnique(): bool {
        return $this->getResource()->assertEmailUnique($this);
    }

    public function assertUsernameUnique(): bool {
        return $this->getResource()->assertUsernameUnique($this);
    }

    /**
     * Delete geefter AND associated geeftee
     *
     * @return $this
     * @throws NoSuchEntityException
     */
    public function deleteWithGeeftee(): static {
        try {
            $geeftee = $this->getGeeftee();
        }
        catch (NoSuchEntityException) {
        }

        $this->delete();
        /** @phpstan-ignore-next-line */
        if (isset($geeftee)) {
            $geeftee->delete();
        }

        return $this;
    }

    /**
     * @return \OliveOil\Core\Model\I18n
     */
    public function getI18n() {
        if (! $this->i18n) {
            $this->i18n = $this->configureI18n(
                $this->modelFactory->make(\OliveOil\Core\Model\I18n::class)
            );
        }

        return $this->i18n;
    }

    /**
     * @see \Geeftlist\Observer\Geefter::forceTimezone()
     */
    protected function configureI18n(\OliveOil\Core\Model\I18n $i18n): \OliveOil\Core\Model\I18n {
        if ($lang = $this->getLanguage()) {
            $i18n->setLanguage($lang);
        }

        // Not handled yet but for later
        if ($tz = $this->getTimezone()) {
            $i18n->setTimezone($tz);
        }

        return $i18n;
    }
}
