<?php

namespace Geeftlist\Model;

/**
 * @method string getName()
 * @method $this setName(string $name)
 * @method string getDescription()
 * @method $this setDescription(string $description)
 * @method string getVisibility()
 * @method $this setVisibility(string $visibility)
 * @method string getStatus()
 * @method $this setStatus(string $status)
 * @method int getOwnerId()
 * @method $this setOwnerId(int $ownerId)
 * @method string getCreatedAt()
 */
class GiftList extends AbstractModel
{
    public const ENTITY_TYPE = 'giftList';

    /** @var Geefter */
    protected $owner;

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftRelation,
        array $data = []
    ) {
        $this->urlHelper = $context->getUrlHelper();
        parent::__construct($context, $data);
        $this->fieldModels['visibility'] = 'giftList/field/visibility';
        $this->fieldModels['status'] = 'giftList/field/status';
    }

    /**
     * @return Geefter|null
     */
    public function getOwner() {
        if (! $this->owner && $this->getOwnerId()) {
            /** @var Geefter|null $owner */
            $owner = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
                ->get($this->getOwnerId());
            $this->owner = $owner;
        }

        return $this->owner;
    }

    /**
     * @return int[]
     */
    public function getGiftIds() {
        return $this->giftListGiftRelation->getLinkedObjectIds($this);
    }

    public function getViewUrl(array $params = []): string {
        return $this->urlHelper->getGiftListViewUrl($this, $params);
    }
}
