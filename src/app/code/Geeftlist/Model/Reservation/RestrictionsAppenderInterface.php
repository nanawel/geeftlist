<?php

namespace Geeftlist\Model\Reservation;

/**
 * Interface RestrictionsAppenderInterface
 */
interface RestrictionsAppenderInterface extends \Geeftlist\Model\RestrictionsAppenderInterface
{
}
