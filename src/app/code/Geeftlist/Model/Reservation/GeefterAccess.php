<?php

namespace Geeftlist\Model\Reservation;


class GeefterAccess implements RestrictionsAppenderInterface
{
    public function __construct(protected \Geeftlist\Model\ResourceModel\Iface\Reservation\RestrictionsAppenderInterface $restrictionsAppender)
    {
    }

    /**
     * Exclude reservations matching unavailable gifts for the specified geeftee.
     *
     * @param string $actionType
     * @return $this
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter,
        $actionType
    ): static {
        $this->getRestrictionsAppender()->addGeefterAccessRestrictions($collection, $geefter, $actionType);

        return $this;
    }

    /**
     * Append allowed actions to the specified reservation collection.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): static {
        // Nothing atm

        return $this;
    }

    public function getRestrictionsAppender(): \Geeftlist\Model\ResourceModel\Iface\Reservation\RestrictionsAppenderInterface {
        return $this->restrictionsAppender;
    }
}
