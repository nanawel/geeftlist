<?php

namespace Geeftlist\Model;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Badge getResource()
 * @method string getType()
 * @method $this setType(string $type)
 * @method string getCode()
 * @method $this setCode(string $code)
 * @method string getLabel()
 * @method $this setLabel(string $label)
 * @method string getDescription()
 * @method $this setDescription(string $description)
 * @method string getIconType()
 * @method $this setIconType(string $iconType)
 * @method string getIconData()
 * @method $this setIconData(string $iconData)
 * @method array getTranslatedData()
 * @method $this setTranslatedData(array $translatedData)
 */
class Badge extends AbstractModel
{
    public const ENTITY_TYPE = 'badge';
}
