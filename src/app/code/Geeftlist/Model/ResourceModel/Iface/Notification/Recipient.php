<?php

namespace Geeftlist\Model\ResourceModel\Iface\Notification;


use Geeftlist\Model\Notification;

interface Recipient
{
    public function addRecipient(
        \Geeftlist\Model\Notification $object,
        $recipient,
        $status = Notification::STATUS_CREATED,
        $additionalData = []
    );

    public function updateRecipientStatus(\Geeftlist\Model\Notification $object, $recipient, $status);

    public function getRecipientStatus(\Geeftlist\Model\Notification $object, $recipient);

    public function getAllRecipientStatus(\Geeftlist\Model\Notification $object);

    public function getLastEventTimeByRecipient($status, $recipientIds = [], \DateTime $onlyBefore = null);

    public function getEventCountByRecipient($status = null, $recipientIds = []);
}
