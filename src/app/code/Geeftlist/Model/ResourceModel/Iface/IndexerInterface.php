<?php

namespace Geeftlist\Model\ResourceModel\Iface;


use Geeftlist\Model\AbstractModel;

interface IndexerInterface
{
    /**
     * @param AbstractModel[] $entities
     */
    public function reindexEntities(array $entities): void;

    public function reindexEntity(AbstractModel $entity): void;
}
