<?php

namespace Geeftlist\Model\ResourceModel\Iface\Gift;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\PredicateInterface;

interface RestrictionsAppenderInterface extends \Geeftlist\Model\ResourceModel\Iface\RestrictionsAppenderInterface
{
    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection $collection
     * @param string $giftTableAlias
     * @param string $giftIdColumnAlias
     * @param string $actionType
     * @param PredicateInterface $additionalPredicate
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $giftTableAlias,
        $giftIdColumnAlias = 'gift_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    );
}
