<?php

namespace Geeftlist\Model\ResourceModel\Iface;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geefter;

interface GeefterAccessInterface
{
    /**
     * @param AbstractModel[] $entities
     * @param string[] $actions
     * @return bool
     */
    public function isAllowed(Geefter $geefter, array $entities, array $actions);

    /**
     * Return all geefter IDs that are allowed to do $actions on $entities
     *
     * @param AbstractModel[] $entities
     * @param string[] $actions
     * @return int[]
     */
    public function getAllowed(array $entities, array $actions);
}
