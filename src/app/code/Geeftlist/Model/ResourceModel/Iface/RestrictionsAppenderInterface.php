<?php

namespace Geeftlist\Model\ResourceModel\Iface;


interface RestrictionsAppenderInterface
{
    /**
     * @param string $actionType
     * @return $this
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter,
        $actionType
    );

    /**
     * @return $this
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    );
}
