<?php

namespace Geeftlist\Model\ResourceModel\Db\ActivityLog;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\ActivityLog;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\ActivityLog>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\ActivityLog::ENTITY_TYPE,
            'activitylog_id',
            ActivityLog::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
