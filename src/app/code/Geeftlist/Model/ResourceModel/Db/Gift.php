<?php

namespace Geeftlist\Model\ResourceModel\Db;


use Geeftlist\Model\Reservation;
use Geeftlist\Model\ResourceModel\Db\Reservation\Collection;

class Gift extends AbstractModel
{
    public const MAIN_TABLE = 'gift';

    //For backward compatibility
    public const GEEFTEE_GIFT_TABLE = \Geeftlist\Model\ResourceModel\Db\Geeftee\Gift::MAIN_TABLE;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'gift_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @param \Geeftlist\Model\Gift $object
     */
    protected function beforeSave(\OliveOil\Core\Model\AbstractModel $object): static {
        if (($estimatedPrice = $object->getEstimatedPrice()) !== null) {
            $object->setEstimatedPrice((float) $estimatedPrice);
        }

        return parent::beforeSave($object);
    }

    /**
     * TODO Move to a dedicated helper like for Geeftee/Gift link
     */
    public function getReservations(\Geeftlist\Model\Gift $gift): Collection {
        /** @var Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(Reservation::ENTITY_TYPE)
            ->addFieldToFilter('main_table.gift_id', $gift->getId() ?: 0);

        return $collection;
    }
}
