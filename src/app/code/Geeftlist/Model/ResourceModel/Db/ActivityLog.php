<?php

namespace Geeftlist\Model\ResourceModel\Db;

class ActivityLog extends AbstractModel
{
    public const MAIN_TABLE = 'activitylog';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'activitylog_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
