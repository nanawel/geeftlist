<?php

namespace Geeftlist\Model\ResourceModel\Db;

use Geeftlist\Model\Family as FamilyModel;
use Geeftlist\Model\Geeftee as GeefteeModel;
use Geeftlist\Model\Geefter as GeefterModel;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee as FamilyGeefteeResource;
use Laminas\Db\Sql\Expression;

class Family extends AbstractModel
{
    public const MAIN_TABLE = 'family';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'family_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @deprecated Use Geeftee collection with family filter instead
     * @throws \Throwable
     */
    public function getGeefteeIds(FamilyModel $family): array {
        $select = $this->connection->select(FamilyGeefteeResource::MAIN_TABLE)
            ->columns(['geeftee_id']);
        $select->where->equalTo('family_id', (int) $family->getId());

        $result = $this->connection->query($select);

        return $this->connection->fetchCol($result, 'geeftee_id');
    }

    /**
     * @deprecated Use Geefter collection with family filter instead
     * @throws \Throwable
     */
    public function getGeefterIds(FamilyModel $family): array {
        $select = $this->connection->select(['family_geeftee' => FamilyGeefteeResource::MAIN_TABLE])
            ->join(
                ['geeftee' => Geeftee::MAIN_TABLE],
                'geeftee.geeftee_id = family_geeftee.geeftee_id',
                ['geefter_id']
            )
            ->columns([]);
        $select->where->equalTo('family_id', (int) $family->getId())
            ->and->isNotNull('geefter_id');

        $result = $this->connection->query($select);

        return $this->connection->fetchCol($result, 'geefter_id');
    }

    /**
     * @deprecated Use Family collection with 'geeftee' filter instead
     * @throws \Throwable
     */
    public function getFamilyIds(\Geeftlist\Model\Geeftee $geeftee): array {
        $select = $this->connection->select(FamilyGeefteeResource::MAIN_TABLE)
            ->columns(['family_id']);
        $select->where->equalTo('geeftee_id', (int) $geeftee->getId());

        $result = $this->connection->query($select);

        return $this->connection->fetchCol($result, 'family_id');
    }

    /**
     * @deprecated Use Geeftee collection with 'family' filter instead
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    public function getGeefteeCollection(FamilyModel $family) {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(GeefteeModel::ENTITY_TYPE);
        $collection->addFieldToFilter('family', (int) $family->getId());

        return $collection;
    }

    public function isMember(FamilyModel $family, GeefteeModel $geeftee): bool {
        if (!$family->getId() || !$geeftee->getId()) {
            return false;
        }

        $select = $this->connection->select(FamilyGeefteeResource::MAIN_TABLE)
            ->columns(['cnt' => new Expression('COUNT(*)')]);
        $select->where->equalTo('family_id', (int) $family->getId())
            ->AND->equalTo('geeftee_id', (int) $geeftee->getId());
        $result = $this->connection->query($select);

        return $result->current()['cnt'] >= 1;  // Should be == 1, but...
    }

    public function isOwner(FamilyModel $family, GeefterModel $geefter): bool {
        if (!$family->getId() || !$geefter->getId()) {
            return false;
        }

        $select = $this->connection->select($this->getMainTable());
        $select->where->equalTo('family_id', (int) $family->getId())
            ->AND->equalTo('owner_id', (int) $geefter->getId());
        ;
        $result = $this->connection->query($select);

        return $result->count() >= 1;  // Should be == 1, but...
    }

    /**
     * @return bool TRUE if unique, FALSE otherwise
     */
    public function assertNameUnique(FamilyModel $object): bool {
        $select = $this->connection->select($this->getMainTable())
            ->columns([$this->idField]);
        $select->where->equalTo('name', (string) $object->getName());
        if ($id = $object->getId()) {
            $select->where->notEqualTo('family_id', (int) $object->getId());
        }

        $result = $this->connection
            ->query($select, \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return !$result->count();
    }
}
