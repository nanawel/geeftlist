<?php

namespace Geeftlist\Model\ResourceModel\Db;


use Geeftlist\Exception\BadgeException;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;

class Badge extends AbstractModel
{
    public const MAIN_TABLE              = 'badge';

    public const BADGE_T9N_TABLE         = 'badge_t9n';

    public const BADGE_ENTITY_TYPE_TABLE = 'badge_entity_type';

    public const TRANSLATABLE_FIELDS = [
        'label',
        'description'
    ];

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        protected \OliveOil\Core\Model\I18n $i18n,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'badge_id',
            self::MAIN_TABLE,
            $data
        );
    }

    protected function afterLoad(\OliveOil\Core\Model\AbstractModel $object): static {
        parent::afterLoad($object);
        $this->loadTranslatedFields($object);

        return $this;
    }

    protected function beforeSave(\OliveOil\Core\Model\AbstractModel $object): static {
        parent::beforeSave($object);
        if (
            !$object->getId()
            && (!is_array($object->getEntityTypes()) || empty($object->getEntityTypes()))
        ) {
            throw new BadgeException('Cannot create a badge with no entity types set.');
        }

        return $this;
    }

    protected function afterSave(\OliveOil\Core\Model\AbstractModel $object): static {
        parent::afterSave($object);

        if (is_array($entityTypes = $object->getEntityTypes())) {
            $this->saveEntityTypes($object, $entityTypes);
        }

        if ($translatedData = $object->getTranslatedData()) {
            $this->saveTranslations($object, $translatedData);
        }

        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function loadTranslatedFields(\OliveOil\Core\Model\AbstractModel $object): static {
        if ($language = $object->getFlag('language')) {
            $langCodes = $this->i18n->deriveLanguage($language, null, true);

            $selectTranslatedFields = $this->connection->select(['main_table' => self::MAIN_TABLE]);
            $selectTranslatedFields->where->equalTo('main_table.badge_id', $object->getId());

            $tableAliases = [];
            foreach ($langCodes as $langCode) {
                $tableAlias = $tableAliases[] = sprintf('badge_translation_%s', preg_replace('/[^a-z0-9]/i', '_', (string) $langCode));
                $selectTranslatedFields->join(
                    [$tableAlias => Badge::BADGE_T9N_TABLE],
                    new Expression(
                        sprintf('main_table.badge_id = %s.badge_id AND %s.language = ?', $tableAlias, $tableAlias),
                        $langCode
                    ),
                    [],
                    Select::JOIN_LEFT
                );
            }

            $tableAliases = array_reverse($tableAliases);
            $columns = [];
            foreach (Badge::TRANSLATABLE_FIELDS as $translatableField) {
                $sql = '';
                foreach ($tableAliases as $tableAlias) {
                    $sql = $sql !== '' && $sql !== '0'
                        ? sprintf('IFNULL(%s.%s, %s)', $tableAlias, $translatableField, $sql)
                        : sprintf('%s.%s', $tableAlias, $translatableField);
                }

                $columns[$translatableField] = new Expression($sql);
            }

            $selectTranslatedFields->columns($columns);

            $translatedFields = $this->connection->fetchOne(
                $this->connection->query($selectTranslatedFields)
            );

            $object->addData($translatedFields);
        }

        return $this;
    }

    /**
     * Warning: *Replaces* any existing localized data for $object
     *
     * @return $this
     */
    protected function saveEntityTypes(\OliveOil\Core\Model\AbstractModel $object, array $entityTypes): static {
        if (!$object->getId()) {
            throw new \InvalidArgumentException('Cannot save entity types for an object with no ID.');
        }

        if ($entityTypes === []) {
            throw new \InvalidArgumentException('Cannot save entity types for an object with no ID.');
        }

        $data = [];
        foreach ($entityTypes as $entityType) {
            $data[] = [
                'badge_id' => $object->getId(),
                'entity_type' => $entityType
            ];
        }

        $conn = $this->connection;
        try {
            $conn->beginTransaction();

            // Delete existing rows for $object
            $delete = $conn->delete(self::BADGE_ENTITY_TYPE_TABLE);
            $delete->where->equalTo('badge_id', (int) $object->getId());
            $conn->query($delete);

            // Insert $data as new rows
            $tableColumns = $conn->getTableColumns(self::BADGE_ENTITY_TYPE_TABLE);
            $insert = $conn->insertMultiple(self::BADGE_ENTITY_TYPE_TABLE)
                ->columns($tableColumns)
                ->values($data);
            $conn->query($insert);

            $conn->commitTransaction();
        } catch (\Throwable $throwable) {
            $conn->rollbackTransaction();

            throw $throwable;
        }

        return $this;
    }

    /**
     * Warning: *Replaces* any existing translated data for $object
     *
     * @return $this
     */
    protected function saveTranslations(\OliveOil\Core\Model\AbstractModel $object, array $data): static {
        if (!$object->getId()) {
            throw new \InvalidArgumentException('Cannot save translated data for an object with no ID.');
        }

        foreach ($data as $locale => &$datum) {
            $datum['badge_id'] = $object->getId();
            $datum['language'] = $locale;
        }

        $conn = $this->connection;
        try {
            $conn->beginTransaction();

            // Delete existing rows for $object
            $delete = $conn->delete(self::BADGE_T9N_TABLE);
            $delete->where->equalTo('badge_id', (int) $object->getId());
            $conn->query($delete);

            // Insert $data as new rows
            $tableColumns = $conn->getTableColumns(self::BADGE_T9N_TABLE);
            $insert = $conn->insertMultiple(self::BADGE_T9N_TABLE)
                ->columns($tableColumns)
                ->values($data);
            $conn->query($insert);

            $conn->commitTransaction();
        } catch (\Throwable $throwable) {
            $conn->rollbackTransaction();

            throw $throwable;
        }

        return $this;
    }
}
