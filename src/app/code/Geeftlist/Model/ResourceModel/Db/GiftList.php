<?php

namespace Geeftlist\Model\ResourceModel\Db;


class GiftList extends AbstractModel
{
    public const MAIN_TABLE = 'giftlist';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'giftlist_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
