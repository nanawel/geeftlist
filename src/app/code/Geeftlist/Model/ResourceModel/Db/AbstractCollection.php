<?php

namespace Geeftlist\Model\ResourceModel\Db;

/**
 * @template T of \Geeftlist\Model\AbstractModel
 * @extends \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection<T>
 */
abstract class AbstractCollection extends \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection
{
    /**
     * Overridden here to force model & resource factory types with DI
     *
     * @param \OliveOil\Core\Model\ResourceModel\Db\Collection\RelationHelperInterface[] $relationHelpers
     */
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        string $itemClass,
        mixed $idField,
        string $mainTable,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $itemClass,
            $idField,
            $mainTable,
            $relationHelpers,
            $data
        );
    }
}
