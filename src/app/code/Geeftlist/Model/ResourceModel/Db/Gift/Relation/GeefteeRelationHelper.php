<?php

namespace Geeftlist\Model\ResourceModel\Db\Gift\Relation;

use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Gift;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GeefteeRelationHelper extends AbstractRelationHelper
{
    public function join_geeftees(Gift\Collection $collection, ?array $joinConditions): static {
        $collection
            ->joinTable(
                ['geeftee_gift' => Geeftee\Gift::MAIN_TABLE],
                'main_table.gift_id = geeftee_gift.gift_id',
                Select::SQL_STAR,
                $joinConditions['type'] ?? Select::JOIN_INNER
            )
            ->joinTable(
                ['geeftee' => Geeftee::MAIN_TABLE],
                'geeftee_gift.geeftee_id = geeftee.geeftee_id',
                Select::SQL_STAR,
                Select::JOIN_INNER,
                true
            );
        $collection->getSelect()->group('main_table.gift_id');

        return $this;
    }

    public function join_geeftee(Gift\Collection $collection, ?array $joinConditions): static {
        return $this->join_geeftees($collection, $joinConditions);
    }

    protected function filterConditionToPredicate_geeftees_in(array $geefteeIds): array {
        return $this->filterConditionToPredicate_geeftees($geefteeIds, true);
    }

    protected function filterConditionToPredicate_geeftees_nin(array $geefteeIds): array {
        return $this->filterConditionToPredicate_geeftees($geefteeIds, false);
    }

    protected function filterConditionToPredicate_geeftee_eq(\Geeftlist\Model\Geeftee|int $geefteeId): array {
        return $this->filterConditionToPredicate_geeftees([$geefteeId], true);
    }

    protected function filterConditionToPredicate_geeftee_ne(\Geeftlist\Model\Geeftee|int $geefteeId): array {
        return $this->filterConditionToPredicate_geeftees([$geefteeId], false);
    }

    private function filterConditionToPredicate_geeftees(array $geefteeIds, bool $in): array {
        $geefteeIds = AbstractCollection::toIdArray($geefteeIds);

        $select = $this->connection->select(['geeftee_gift_filter' => Geeftee\Gift::MAIN_TABLE]);
        $select->where->equalTo(
            'geeftee_gift_filter.gift_id',
            'main_table.gift_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($geefteeIds !== []) {
            if ($in) {
                // Strict IN
                $select->where->in('geeftee_gift_filter.geeftee_id', $geefteeIds);
            }
            else {
                // Loose NOT IN (= "NOT IN" OR "IS NULL")
                $select->where
                    ->NEST
                    ->in('geeftee_gift_filter.geeftee_id', $geefteeIds)
                    ->AND
                    ->isNotNull('geeftee_gift_filter.geeftee_id')
                    ->UNNEST
                ;
            }
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }

    protected function filterConditionToPredicate_geeftee_null(bool $null = true): array {
        $subSelect = $this->connection->select(['geeftee_gift_filter' => Geeftee\Gift::MAIN_TABLE]);
        $subSelect->where->equalTo(
            'main_table.gift_id',
            'geeftee_gift_filter.gift_id',
            PredicateInterface::TYPE_IDENTIFIER,
            PredicateInterface::TYPE_IDENTIFIER
        );

        return $null ? ['NOT EXISTS (?)' => $subSelect] : ['EXISTS (?)' => $subSelect];
    }
}
