<?php

namespace Geeftlist\Model\ResourceModel\Db\Gift\Relation;

use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class ReserverRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_reserver_eq(Geefter|int $geefterId): array {
        return $this->filterConditionToPredicate_reserver($geefterId, true);
    }

    protected function filterConditionToPredicate_reserver_ne(Geefter|int $geefterId): array {
        return $this->filterConditionToPredicate_reserver($geefterId, false);
    }

    private function filterConditionToPredicate_reserver(Geefter|int $geefterId, bool $equals): array {
        $geefterId = AbstractCollection::toId($geefterId);

        $select = $this->connection->select(['reservation_filter' => Reservation::MAIN_TABLE]);
        $select->where->equalTo(
            'reservation_filter.gift_id',
            'main_table.gift_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        $select->where->equalTo('reservation_filter.geefter_id', $geefterId);

        return $equals ? ['EXISTS (?)' => $select] : ['NOT EXISTS(?)' => $select];
    }
}
