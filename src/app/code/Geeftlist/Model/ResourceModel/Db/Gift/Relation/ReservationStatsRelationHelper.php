<?php

namespace Geeftlist\Model\ResourceModel\Db\Gift\Relation;

use Geeftlist\Model\ResourceModel\Db\Gift;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class ReservationStatsRelationHelper extends AbstractRelationHelper
{
    public function join_reservation_stats(Gift\Collection $collection): static {
        $collection->joinTable(
            ['rs' => Reservation::MAIN_TABLE],
            'rs.gift_id = main_table.gift_id',
            [
                'reservations_count' => new Expression('COUNT(rs.reservation_id)'),
                'reservations_total_amount' => new Expression('SUM(rs.amount)'),
                'reservations_remaining_amount' => new Expression(
                    'GREATEST(0, IFNULL(main_table.estimated_price - SUM(rs.amount), main_table.estimated_price))'
                )
            ],
            Select::JOIN_LEFT
        );
        $collection->getSelect()->group('main_table.gift_id');

        return $this;
    }
}
