<?php

namespace Geeftlist\Model\ResourceModel\Db\Gift\Relation;

use Geeftlist\Model\ResourceModel\Db\GiftList;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GiftListRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_giftlists_in(array $giftListIds): array {
        return $this->filterConditionToPredicate_giftlists($giftListIds, true);
    }

    protected function filterConditionToPredicate_giftlists_nin(array $giftListIds): array {
        return $this->filterConditionToPredicate_giftlists($giftListIds, false);
    }

    protected function filterConditionToPredicate_giftlist_eq(\Geeftlist\Model\GiftList|int $giftListId): array {
        return $this->filterConditionToPredicate_giftlists([$giftListId], true);
    }

    protected function filterConditionToPredicate_giftlist_ne(\Geeftlist\Model\GiftList|int $giftListId): array {
        return $this->filterConditionToPredicate_giftlists([$giftListId], false);
    }

    private function filterConditionToPredicate_giftlists(array $giftListIds, bool $in): array {
        $giftListIds = AbstractCollection::toIdArray($giftListIds);

        $select = $this->connection->select(['giftlist_gift_filter' => GiftList\Gift::MAIN_TABLE]);
        $select->where->equalTo(
            'giftlist_gift_filter.gift_id',
            'main_table.gift_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($giftListIds !== []) {
            $select->where->in('giftlist_gift_filter.giftlist_id', $giftListIds);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }
}
