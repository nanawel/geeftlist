<?php

namespace Geeftlist\Model\ResourceModel\Db\Gift\Relation;

use Geeftlist\Model\ResourceModel\Db\Geefter;
use Geeftlist\Model\ResourceModel\Db\Gift;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class CreatorRelationHelper extends AbstractRelationHelper
{
    public function join_creator(Gift\Collection $collection): static {
        $collection->joinTable(
            ['creator' => Geefter::MAIN_TABLE],
            'main_table.creator_id = creator.geefter_id',
            Select::SQL_STAR,
            Select::JOIN_INNER,
            true
        );

        return $this;
    }
}
