<?php
namespace Geeftlist\Model\ResourceModel\Db\Gift;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Gift;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Gift>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Gift::ENTITY_TYPE,
            'gift_id',
            Gift::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
