<?php

namespace Geeftlist\Model\ResourceModel\Db\Gift;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Indexer\AbstractGeefterAccess;
use Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer;
use Geeftlist\Model\ResourceModel\Iface\Gift\RestrictionsAppenderInterface;
use Geeftlist\Model\Share\Constants;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\Operator;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Predicate\PredicateSet;

class GeefterAccess extends AbstractGeefterAccess implements RestrictionsAppenderInterface
{
    public const MAIN_TABLE = GeefterAccessIndexer::MAIN_TABLE;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            ['entity_type', 'entity_id', 'geefter_id', 'action_type'],
            self::MAIN_TABLE,
            Gift::ENTITY_TYPE,
            $data
        );
    }

    /**
     * Exclude unavailable gifts for the specified geefter.
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $this->addGeefterAccessRestrictionsToEntityCollection(
            $collection,
            $geefter,
            'main_table',
            'gift_id',
            $actionType
        );

        return $this;
    }

    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection $collection
     * @param string $giftTableAlias
     * @param string $giftIdColumnAlias
     * @param string $actionType
     * @param PredicateInterface $additionalPredicate
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $giftTableAlias,
        $giftIdColumnAlias = 'gift_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): static {
        $select = $this->connection->select(['geefter_access_gift_idx' => self::MAIN_TABLE]);
        $select->where
            ->equalTo(
                'geefter_access_gift_idx.entity_id',
                sprintf('%s.%s', $giftTableAlias, $giftIdColumnAlias),
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_IDENTIFIER
            )->AND->equalTo('geefter_access_gift_idx.geefter_id', $geefter->getId())
            ->AND->equalTo('geefter_access_gift_idx.entity_type', Gift::ENTITY_TYPE)
            ->AND->equalTo('geefter_access_gift_idx.action_type', $actionType)
            ->AND->equalTo('geefter_access_gift_idx.is_allowed', Constants::ALLOWED);

        if ($entityTypeColumnAlias) {
            // Joining with a collection that references multiple types of entity, so we must
            // be sure we filter only on entities we support (gifts)
            $isNotEntityTypeGift = new Operator(
                sprintf('%s.%s', $giftTableAlias, $entityTypeColumnAlias),
                Operator::OP_NE,
                $this->entityType
            );

            $predicates = [
                new Operator(
                    sprintf('%s.%s', $giftTableAlias, $entityTypeColumnAlias),
                    Operator::OP_EQ,
                    $this->entityType
                ),
                new \Laminas\Db\Sql\Predicate\Expression(
                    'EXISTS (?)',
                    $select
                )
            ];
            if ($additionalPredicate) {
                $predicates[] = $additionalPredicate;
            }

            $isGiftActionAllowed = new PredicateSet($predicates);

            $collection->getSelect()->where(new \Laminas\Db\Sql\Predicate\Expression(
                '((?) XOR (?))',
                [$isNotEntityTypeGift, $isGiftActionAllowed]
            ));
        }
        else {
            // Only working with gifts and no other entity, so we just have to check for EXISTS
            $collection->getSelect()->where->expression('EXISTS (?)', $select);
        }

        return $this;
    }

    /**
     * Append allowed actions to the specified gift collection.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): static {
        $this->appendGeefterAccessPermissionsToEntityCollection($collection, $geefter, 'main_table', 'gift_id');

        return $this;
    }
}
