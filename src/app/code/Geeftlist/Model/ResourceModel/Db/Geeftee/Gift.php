<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee;


use OliveOil\Core\Model\ResourceModel\Db\AbstractRelationHelper;

/**
 * @template T1 of \Geeftlist\Model\Geeftee
 * @template T2 of \Geeftlist\Model\Gift
 * @template T1Collection of \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
 * @template T2Collection of \Geeftlist\Model\ResourceModel\Db\Gift\Collection
 *
 * @extends \OliveOil\Core\Model\ResourceModel\Db\AbstractRelationHelper<
 *     \Geeftlist\Model\Geeftee,
 *     \Geeftlist\Model\Gift,
 *     \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection,
 *     \Geeftlist\Model\ResourceModel\Db\Gift\Collection
 * >
 */
class Gift extends AbstractRelationHelper
{
    public const MAIN_TABLE = 'geeftee_gift';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct(
            $context,
            self::MAIN_TABLE,
            \Geeftlist\Model\Geeftee::ENTITY_TYPE,
            \Geeftlist\Model\Gift::ENTITY_TYPE
        );
    }

    /**
     * @param \Geeftlist\Model\Gift[] $gifts
     * @return \Geeftlist\Model\Gift[][]
     */
    public function getGiftsPerGeeftee(array $gifts): array {
        if ($gifts === []) {
            return [];
        }

        $giftsById = [];
        foreach ($gifts as $gift) {
            $giftsById[$gift->getId()] = $gift;
        }

        $select = $this->connection->select(self::MAIN_TABLE)
            ->columns(['gift_id', 'geeftee_id']);
        $select->where->in('gift_id', array_keys($giftsById));

        $giftsByGeeftee = [];
        $result = $this->connection->query($select);
        while ($row = $result->current()) {
            $giftsByGeeftee[$row['geeftee_id']][] = $giftsById[$row['gift_id']];
            $result->next();
        }

        return $giftsByGeeftee;
    }
}
