<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee\Relation;

use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GiftRelationHelper extends AbstractRelationHelper
{
    public function join_gifts(Geeftee\Collection $collection, ?array $joinConditions): static {
        $collection
            ->joinTable(
                ['geeftee_gift' => Geeftee\Gift::MAIN_TABLE],
                'main_table.geeftee_id = geeftee_gift.geeftee_id',
                Select::SQL_STAR,
                $joinConditions['type'] ?? Select::JOIN_INNER
            )
            ->joinTable(
                ['gift' => \Geeftlist\Model\ResourceModel\Db\Gift::MAIN_TABLE],
                'geeftee_gift.gift_id = gift.gift_id',
                Select::SQL_STAR,
                Select::JOIN_INNER,
                true
            );
        $collection->getSelect()->group('main_table.geeftee_id');

        return $this;
    }

    public function join_gift(Geeftee\Collection $collection, ?array $joinConditions): static {
        return $this->join_gifts($collection, $joinConditions);
    }

    protected function filterConditionToPredicate_gifts_in(array $giftIds): array {
        return $this->filterConditionToPredicate_gifts($giftIds, true);
    }

    protected function filterConditionToPredicate_gifts_nin(array $giftIds): array {
        return $this->filterConditionToPredicate_gifts($giftIds, false);
    }

    protected function filterConditionToPredicate_gift_eq(Gift|int $giftId): array {
        return $this->filterConditionToPredicate_gifts([$giftId], true);
    }

    protected function filterConditionToPredicate_gift_ne(Gift|int $giftId): array {
        return $this->filterConditionToPredicate_gifts([$giftId], false);
    }

    /**
     * IF $in = TRUE
     *     Filters the collection to keep only geeftees with *at least one* gift from
     *     the specified $giftIds list.
     * IF $in = FALSE
     *     Filters the collection to keep only geeftees with *not any of the gifts* of the specified
     *     $giftIds list.
     *
     * @param Gift[]|int[] $giftIds
     */
    private function filterConditionToPredicate_gifts(array $giftIds, bool $in): array {
        $giftIds = AbstractCollection::toIdArray($giftIds);

        $select = $this->connection->select(['geeftee_gift_filter' => Geeftee\Gift::MAIN_TABLE]);
        $select->where->equalTo(
            'geeftee_gift_filter.geeftee_id',
            'main_table.geeftee_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($giftIds !== []) {
            $select->where->in('geeftee_gift_filter.gift_id', $giftIds);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }
}
