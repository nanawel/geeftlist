<?php
namespace Geeftlist\Model\ResourceModel\Db\Geeftee;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Geeftee;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Geeftee>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Geeftee::ENTITY_TYPE,
            'geeftee_id',
            Geeftee::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
