<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee\Relation;

use Geeftlist\Model\Family;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee as FamilyGeeftee;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class FamilyRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_families_in(array $familyIds): array {
        return $this->filterConditionToPredicate_families($familyIds, true);
    }

    protected function filterConditionToPredicate_families_nin(array $familyIds): array {
        return $this->filterConditionToPredicate_families($familyIds, false);
    }

    protected function filterConditionToPredicate_family_eq(Family|int $familyId): array {
        return $this->filterConditionToPredicate_families([$familyId], true);
    }

    protected function filterConditionToPredicate_family_ne(Family|int $familyId): array {
        return $this->filterConditionToPredicate_families([$familyId], false);
    }

    /**
     * IF $in = TRUE
     *     Filters the collection to keep only geeftees who are members of *at least one* family from
     *     the specified $familyIds list.
     * IF $in = FALSE
     *     Filters the collection to keep only geeftees who are *not members of any* of the specified
     *     $familyIds list.
     *
     * @param Family[]|int[] $familyIds
     * @return array
     */
    private function filterConditionToPredicate_families(array $familyIds, bool $in): array {
        $familyIds = AbstractCollection::toIdArray($familyIds);

        $select = $this->connection->select(['family_geeftee_filter' => FamilyGeeftee::MAIN_TABLE]);
        $select->where->equalTo(
            'family_geeftee_filter.geeftee_id',
            'main_table.geeftee_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($familyIds !== []) {
            $select->where->in('family_geeftee_filter.family_id', $familyIds);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }
}
