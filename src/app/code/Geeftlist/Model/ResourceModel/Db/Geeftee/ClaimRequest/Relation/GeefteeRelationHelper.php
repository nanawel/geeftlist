<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest\Relation;

use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GeefteeRelationHelper extends AbstractRelationHelper
{
    public function join_geeftee(Geeftee\ClaimRequest\Collection $collection): static {
        $collection->joinTable(
            ['geeftee' => Geeftee::MAIN_TABLE],
            'main_table.geeftee_id = geeftee.geeftee_id',
            Select::SQL_STAR,
            Select::JOIN_INNER,
            true
        );

        return $this;
    }
}
