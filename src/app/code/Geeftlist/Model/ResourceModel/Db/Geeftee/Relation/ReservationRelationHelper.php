<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee\Relation;

use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class ReservationRelationHelper extends AbstractRelationHelper
{
    public function join_reservation(Geeftee\Collection $collection, ?array $joinConditions): static {
        $collection->joinTable(
            ['geeftee_gift' => Geeftee\Gift::MAIN_TABLE],
            'main_table.geeftee_id = geeftee_gift.geeftee_id',
            ['gift_ids' => new Expression('GROUP_CONCAT(geeftee_gift.gift_id)')],
            $joinConditions['type'] ?? Select::JOIN_INNER
        )
            ->joinTable(
                ['reservation' => Reservation::MAIN_TABLE],
                'geeftee_gift.gift_id = reservation.gift_id',
                ['reserver_ids' => new Expression('GROUP_CONCAT(reservation.geefter_id)')],
                $joinConditions['type'] ?? Select::JOIN_INNER
            )
            ->getSelect()->group('main_table.geeftee_id')
        ;

        return $this;
    }
}
