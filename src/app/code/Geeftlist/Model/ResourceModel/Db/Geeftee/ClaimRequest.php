<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee;


use Geeftlist\Model\ResourceModel\Db\AbstractModel;

class ClaimRequest extends AbstractModel
{
    public const MAIN_TABLE = 'geeftee_claim_request';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'claim_request_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
