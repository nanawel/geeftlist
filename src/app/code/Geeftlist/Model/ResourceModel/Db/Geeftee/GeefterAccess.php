<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\ResourceModel\Db\Family;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee as FamilyGeeftee;
use Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest;
use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Geeftee\Gift as GeefteeGift;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Geeftlist\Model\ResourceModel\Iface\Geeftee\RestrictionsAppenderInterface;
use Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface;
use Geeftlist\Model\Share\Geeftee\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Exception\NotImplementedException;

class GeefterAccess implements GeefterAccessInterface, RestrictionsAppenderInterface
{
    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context
    ) {
        $this->connection = $context->getConnection();
    }

    /**
     * @inheritDoc
     * @throws \Throwable
     */
    public function isAllowed(Geefter $geefter, array $entities, array $actions): bool {
        $entityIds = [];
        foreach ($entities as $e) {
            if (! $e instanceof \Geeftlist\Model\Geeftee) {
                throw new \InvalidArgumentException('Not a geeftee.');
            }

            // Exclude entities with no ID = always allow actions on *new* entities
            if ($e->getId()) {
                $entityIds[] = $e->getId();
            }
        }

        if ($actions === [] || $entityIds === []) {
            return true;
        }

        sort($entityIds);
        sort($actions);

        $allowedActionsByGeefteeId = [];

        // Use $this->addGeefterAccessRestrictionsToSelect() for each required $action, and aggregate results
        // for later comparison
        foreach ($actions as $action) {
            $actionSelect = $this->connection->select(['main_table' => Geeftee::MAIN_TABLE])
                ->columns(['geeftee_id']);
            $actionSelect->where->in('geeftee_id', $entityIds);
            $this->addGeefterAccessRestrictionsToSelect(
                $actionSelect,
                $geefter,
                'main_table',
                'geeftee_id',
                null,
                $action
            );
            $result = $this->connection->query($actionSelect);

            foreach ($this->connection->fetchCol($result, 'geeftee_id') as $geefteeId) {
                $allowedActionsByGeefteeId[$geefteeId][] = $action;
            }
        }

        foreach ($entityIds as $geefteeId) {
            // Missing all permissions for geeftee
            if (!isset($allowedActionsByGeefteeId[$geefteeId])) {
                return false;
            }

            sort($allowedActionsByGeefteeId[$geefteeId]);
            // Missing at least one required permission for a geeftee
            if ($allowedActionsByGeefteeId[$geefteeId] != $actions) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getAllowed(array $entities, array $actions): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * Exclude unavailable gifts for the specified geefter.
     *
     * @throws \Exception
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $this->addGeefterAccessRestrictionsToCollection(
            $collection,
            $geefter,
            'main_table',
            'geeftee_id',
            null,
            $actionType
        );

        return $this;
    }

    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection $collection
     * @param string $geefteeTableAlias
     * @param string $geefteeIdColumnAlias
     * @param string $actionType
     * @param PredicateInterface $additionalPredicate
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $geefteeTableAlias,
        $geefteeIdColumnAlias = 'geeftee_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): static {
        $this->addGeefterAccessRestrictionsToSelect(
            $collection->getSelect(),
            $geefter,
            $geefteeTableAlias,
            $geefteeIdColumnAlias,
            $entityTypeColumnAlias,
            $actionType,
            $additionalPredicate
        );

        return $this;
    }

    /**
     * @param string $geefteeIdColumnAlias
     * @param PredicateInterface|null $additionalPredicate
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToSelect(
        Select $select,
        Geefter $geefter,
        string $geefteeTableAlias,
        $geefteeIdColumnAlias = 'geeftee_id',
        $entityTypeColumnAlias = null,
        string $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): void {
        if ($entityTypeColumnAlias !== null) {
            throw new NotImplementedException('$entityTypeColumnAlias is not supported.');
        }

        if ($additionalPredicate !== null) {
            throw new NotImplementedException('$additionalPredicate is not supported.');
        }

//        SET @relatedGeefteeId = 2;
//         SELECT * FROM geeftee AS main_table
//         WHERE main_table.geeftee_id = @relatedGeefteeId
//         OR EXISTS(
//            -- $commonFamilyRelationSelect:
//            SELECT * FROM family_geeftee AS common_family_relation
//            WHERE common_family_relation.geeftee_id = main_table.geeftee_id
//            AND EXISTS(
//                -- $relatedGeefteeFamilyRelationSelect:
//                SELECT * FROM family_geeftee related_geefter_family_relation
//                JOIN geeftee related_geeftee ON related_geeftee.geeftee_id = related_geefter_family_relation.geeftee_id
//                WHERE related_geefter_family_relation.family_id = common_family_relation.family_id
//                AND related_geeftee.geeftee_id = @relatedGeefteeId
//            )
//         )
//        OR EXISTS (
//            -- $activeReservationSelect
//            SELECT * FROM reservation
//            JOIN geeftee ON reservation.geefter_id = geeftee.geefter_id
//            JOIN geeftee_gift ON geeftee_gift.gift_id = reservation.gift_id
//            JOIN gift ON gift.gift_id = geeftee_gift.gift_id
//            WHERE geeftee.geeftee_id = @relatedGeefteeId
//            AND geeftee_gift.geeftee_id = main_table.geeftee_id
//            AND gift.status = 'available'
//         )
//         -- Missing some more...

        $orPredicates = [];
        switch ($actionType) {
            case TypeInterface::ACTION_VIEW:
                // An active reservation for a gift exists by the $geefter on the geeftee
                $orPredicates[] = new Expression(
                    'EXISTS (?)',
                    $this->getExistsActiveReservationSelect($geefter, $geefteeTableAlias, $geefteeIdColumnAlias)
                );
                // A claim request open by the geeftee's geefter is pending on a geeftee created by the $geefter
                $orPredicates[] = new Expression(
                    'EXISTS (?)',
                    $this->getExistsPendingClaimRequestSelect($geefter, $geefteeTableAlias, $geefteeIdColumnAlias)
                );
                // A membership request opened by the geeftee's geefter is pending on a family owned by the $geefter
                $orPredicates[] = new Expression(
                    'EXISTS (?)',
                    $this->getExistsPendingMembershipRequestSelect($geefter, $geefteeTableAlias, $geefteeIdColumnAlias)
                );
                // no break!

            case TypeInterface::ACTION_INTERACT:
                // Geeftee is geefterless and owned by $geefter
                $orPredicates[] = new Expression($geefteeTableAlias . '.creator_id = ?', $geefter->getId());
                // no break!

            case TypeInterface::ACTION_ADD_GIFT:
                $orPredicates[] = new Expression($geefteeTableAlias . '.geefter_id = ?', $geefter->getId());
                // no break!

            case TypeInterface::ACTION_RESERVE_GIFT:
                $orPredicates[] = new Expression(
                    'EXISTS (?)',
                    $this->getRelatedGeefteeSelect($geefter, $geefteeTableAlias, $geefteeIdColumnAlias)
                );
                break;

            case TypeInterface::ACTION_CREATE_CLAIM_REQUEST:
                // Must be related
                $relatedSelect = $this->getRelatedGeefteeSelect($geefter, $geefteeTableAlias, $geefteeIdColumnAlias);
                // AND target must be a *geefterless* geeftee
                $relatedSelect->where->isNull($geefteeTableAlias . '.geefter_id')
                    // where the creator is not $geefter
                    ->notEqualTo($geefteeTableAlias . '.creator_id', $geefter->getId());
                $orPredicates[] = new Expression('EXISTS (?)', $relatedSelect);
                break;

            case TypeInterface::ACTION_EDIT:
            case TypeInterface::ACTION_DELETE:
            case TypeInterface::ACTION_SET_BADGES:
                $orPredicates[] = new Expression($geefteeTableAlias . '.geefter_id = ?', $geefter->getId());
                // no break!

            case TypeInterface::ACTION_MERGE_TO:
            case TypeInterface::ACTION_ACCEPT_CLAIM_REQUEST:
            case TypeInterface::ACTION_REJECT_CLAIM_REQUEST:
                $orPredicates[] = new Expression($geefteeTableAlias . '.creator_id = ?', $geefter->getId());
                break;

            default:
                throw new InvalidValueException('Invalid action type: ' . $actionType);
        }

        $select->where->nest()
            ->addPredicates($orPredicates, PredicateSet::COMBINED_BY_OR)
            ->unnest();
    }

    /**
     * Append allowed actions to the specified gift collection.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * @param string $geefteeTableAlias
     * @param string $geefteeIdColumnAlias
     * @return Select
     */
    protected function getExistsActiveReservationSelect(Geefter $geefter, $geefteeTableAlias, $geefteeIdColumnAlias) {
        $select = $this->connection->select(
            ['related_geefter_active_reservation' => Reservation::MAIN_TABLE]
        );
        $select->join(
            ['reserver_geeftee' => Geeftee::MAIN_TABLE],
            'reserver_geeftee.geefter_id = related_geefter_active_reservation.geefter_id'
        )
            ->join(
                ['geeftee_gift' => GeefteeGift::MAIN_TABLE],
                'geeftee_gift.gift_id = related_geefter_active_reservation.gift_id'
            )
            ->join(
                ['gift' => \Geeftlist\Model\ResourceModel\Db\Gift::MAIN_TABLE],
                'gift.gift_id = geeftee_gift.gift_id'
            );
        $select->where->equalTo('reserver_geeftee.geeftee_id', $geefter->getGeeftee()->getId())
            ->AND->equalTo(
                'geeftee_gift.geeftee_id',
                sprintf('%s.%s', $geefteeTableAlias, $geefteeIdColumnAlias),
                Predicate::TYPE_IDENTIFIER,
                Predicate::TYPE_IDENTIFIER
            )
            ->AND->equalTo('gift.status', Status::AVAILABLE);

        return $select;
    }

    /**
     * @param string $geefteeIdColumnAlias
     * @return Select
     */
    protected function getExistsPendingClaimRequestSelect(Geefter $geefter, string $geefteeTableAlias, $geefteeIdColumnAlias) {
        $select = $this->connection->select(
            ['claim_request' => ClaimRequest::MAIN_TABLE],
        );
        $select->join(
            ['requestee' => Geeftee::MAIN_TABLE],
            'requestee.geeftee_id = claim_request.geeftee_id'
        );
        $select->where->equalTo('claim_request.geefter_id', $geefter->getId())
            ->OR->NEST->equalTo('requestee.creator_id', $geefter->getId())
            ->AND->equalTo(
                'claim_request.geefter_id',
                $geefteeTableAlias . '.geefter_id',
                Predicate::TYPE_IDENTIFIER,
                Predicate::TYPE_IDENTIFIER
            )
            ->AND->equalTo('claim_request.decision_code', \Geeftlist\Model\Geeftee\ClaimRequest::DECISION_CODE_PENDING)
            ->UNNEST;

        return $select;
    }

    /**
     * @param string $geefteeTableAlias
     * @param string $geefteeIdColumnAlias
     * @return Select
     */
    protected function getExistsPendingMembershipRequestSelect(
        Geefter $geefter,
        $geefteeTableAlias,
        $geefteeIdColumnAlias
    ) {
        $select = $this->connection->select(
            ['membership_request' => MembershipRequest::MAIN_TABLE]
        );
        $select
            ->join(
                ['family' => Family::MAIN_TABLE],
                'family.family_id = membership_request.family_id'
            );
        $select->where
            ->nest()
                // MR where $geefter is the family's owner
                ->equalTo('family.owner_id', $geefter->getId())
                // or where $geefter is the sponsor
                ->OR->equalTo('membership_request.sponsor_id', $geefter->getId())
            ->unnest()
            ->AND->equalTo(
                'membership_request.geeftee_id',
                sprintf('%s.%s', $geefteeTableAlias, $geefteeIdColumnAlias),
                Predicate::TYPE_IDENTIFIER,
                Predicate::TYPE_IDENTIFIER
            )
            ->AND->equalTo(
                'membership_request.decision_code',
                \Geeftlist\Model\Family\MembershipRequest::DECISION_CODE_PENDING
            );

        return $select;
    }

    /**
     * @param string $geefteeTableAlias
     * @param string $geefteeIdColumnAlias
     * @return Select
     */
    protected function getRelatedGeefteeSelect(Geefter $geefter, $geefteeTableAlias, $geefteeIdColumnAlias) {
        $relatedGeefteeFamilyRelationSelect = $this->connection->select(
            ['related_geefter_family_relation' => FamilyGeeftee::MAIN_TABLE]
        );
        $relatedGeefteeFamilyRelationSelect->join(
            ['related_geeftee' => \Geeftlist\Model\ResourceModel\Db\Geeftee::MAIN_TABLE],
            'related_geeftee.geeftee_id = related_geefter_family_relation.geeftee_id'
        );
        $relatedGeefteeFamilyRelationSelect->where->equalTo(
            'related_geefter_family_relation.family_id',
            'common_family_relation.family_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        )
            // = "Not geefter himself"
            ->AND->notEqualTo(
                'related_geeftee.geeftee_id',
                sprintf('%s.%s', $geefteeTableAlias, $geefteeIdColumnAlias),
                Predicate::TYPE_IDENTIFIER,
                Predicate::TYPE_IDENTIFIER
            )
            ->AND->equalTo('related_geeftee.geeftee_id', $geefter->getGeeftee()->getId());

        $commonFamilyRelationSelect = $this->connection->select(
            ['common_family_relation' => FamilyGeeftee::MAIN_TABLE]
        );
        $commonFamilyRelationSelect->where
            ->equalTo(
                'common_family_relation.geeftee_id',
                sprintf('%s.%s', $geefteeTableAlias, $geefteeIdColumnAlias),
                Predicate::TYPE_IDENTIFIER,
                Predicate::TYPE_IDENTIFIER
            )
            ->AND->addPredicates(['EXISTS (?)' => $relatedGeefteeFamilyRelationSelect])
        ;

        return $commonFamilyRelationSelect;
    }

    /**
     * @return \OliveOil\Core\Model\ResourceModel\Db\Connection
     */
    public function getConnection() {
        return $this->connection;
    }
}
