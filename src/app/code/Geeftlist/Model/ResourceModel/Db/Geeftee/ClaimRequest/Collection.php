<?php

namespace Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Geeftee;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Geeftee\ClaimRequest>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Geeftee\ClaimRequest::ENTITY_TYPE,
            'claim_request_id',
            Geeftee\ClaimRequest::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
