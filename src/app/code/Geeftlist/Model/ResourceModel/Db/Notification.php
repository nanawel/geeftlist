<?php

namespace Geeftlist\Model\ResourceModel\Db;

use Geeftlist\Model\Notification as NotificationModel;
use Laminas\Db\Sql\Expression;
use OliveOil\Core\Helper\DateTime;

class Notification extends AbstractModel
{
    public const MAIN_TABLE = 'notification';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'notification_id',
            self::MAIN_TABLE,
            $data
        );
    }

    public function addRecipient(
        \Geeftlist\Model\Notification $object,
        $recipient,
        $status = NotificationModel::STATUS_CREATED,
        $additionalData = []
    ): static {
        $this->getRecipientResourceModel($object)->addRecipient($object, $recipient, $status, $additionalData);

        return $this;
    }

    public function updateRecipientStatus(
        \Geeftlist\Model\Notification $object,
        $recipient,
        $status
    ): static {
        $this->getRecipientResourceModel($object)->updateRecipientStatus($object, $recipient, $status);

        return $this;
    }

    public function getRecipientStatus(
        \Geeftlist\Model\Notification $object,
        $recipient
    ) {
        return $this->getRecipientResourceModel($object)->getRecipientStatus($object, $recipient);
    }

    public function getAllRecipientStatus(\Geeftlist\Model\Notification $object) {
        return $this->getRecipientResourceModel($object)->getAllRecipientStatus($object);
    }

    public function getLastEventTimeByRecipient(
        string $recipientType,
        $status,
        $recipientIds = [],
        \DateTime $onlyBefore = null
    ) {
        return $this->getRecipientResourceModelByType($recipientType)
            ->getLastEventTimeByRecipient($status, $recipientIds, $onlyBefore);
    }

    public function getEventCountByRecipient(string $recipientType, $status = null, $recipientIds = []) {
        return $this->getRecipientResourceModelByType($recipientType)
            ->getEventCountByRecipient($status, $recipientIds);
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient
     */
    public function getRecipientResourceModel(\Geeftlist\Model\Notification $object) {
        return $this->getRecipientResourceModelByType($object->getRecipientType());
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient
     */
    public function getRecipientResourceModelByType(string $type) {
        return $this->getResourceModelFactory()->getFromCode(NotificationModel::ENTITY_TYPE . '/' . $type);
    }

    /**
     * @param string $status
     * @param string $recipientType
     * @param \DateTime $onlyBefore
     * @return array
     */
    public function getLastEventTime($recipientType, $status, \DateTime $onlyBefore = null) {
        $select = $this->connection->select(self::MAIN_TABLE);
        $select->columns([
                'last_event_time' => new Expression('MAX(created_at)')
            ])
            ->where->equalTo('recipient_type', $recipientType)
            ->and->equalTo('status', $status);
        if ($onlyBefore instanceof \DateTime) {
            $select->having->lessThanOrEqualTo('last_event_time', DateTime::getDateSql($onlyBefore));
        }

        $resultSet = $this->connection->query($select);

        return $this->connection->fetchFirstValue($resultSet);
    }
}
