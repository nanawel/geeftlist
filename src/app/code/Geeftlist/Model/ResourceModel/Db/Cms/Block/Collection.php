<?php

namespace Geeftlist\Model\ResourceModel\Db\Cms\Block;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Cms\Block;
use Laminas\Db\Sql\Expression;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Cms\Block>
 *
 * @method $this setLanguage(string|array $language)
 * @method string|array getLanguage()
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Cms\Block::ENTITY_TYPE,
            'block_id',
            Block::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }

    protected function sortByLanguage(): static {
        if ($language = $this->getLanguage()) {
            // Little trick to order results by language priority
            // https://dev.mysql.com/doc/refman/5.7/en/sorting-rows.html
            $this->getSelect()->order(
                array_map(
                    static fn($lang): \Laminas\Db\Sql\Expression => new Expression('language = ? DESC', $lang),
                    $language
                )
            );
        }

        return $this;
    }

    protected function applyOrder(): static {
        if (! $this->getFlag('skip_language_sorting')) {
            $this->sortByLanguage();
        }

        return parent::applyOrder();
    }
}
