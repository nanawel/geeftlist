<?php

namespace Geeftlist\Model\ResourceModel\Db\Cms;


use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Laminas\Db\Sql\Expression;

class Block extends AbstractModel
{
    public const MAIN_TABLE = 'cms_block';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'block_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @param \Geeftlist\Model\Cms\Block $object
     */
    protected function beforeSave(\OliveOil\Core\Model\AbstractModel $object): static {
        $object->setEnabled($object->getEnabled() ? 1 : 0);

        return parent::beforeSave($object);
    }

    public function loadByCodeAndLanguage(
        \Geeftlist\Model\Cms\Block $object,
        string $code,
        array|string $language,
        ?bool $enabled = true
    ): static {
        if (! is_array($language)) {
            $language = [$language];
        }

        $select = $this->connection->select(self::MAIN_TABLE)
            ->columns([$this->getIdField()]);
        $select->where->equalTo('code', $code)
                ->AND->in('language', $language);
        if ($enabled !== null) {
            $select->where->AND->equalTo('enabled', $enabled ? 1 : 0);
        }

        // Little trick to order results by language priority
        // https://dev.mysql.com/doc/refman/5.7/en/sorting-rows.html
        $select->order(array_map(
            static fn($lang): \Laminas\Db\Sql\Expression => new Expression('language = ? DESC', $lang),
            $language
        ));

        if ($id = $this->connection->fetchFirstValue($this->connection->query($select))) {
            $this->load($object, $id);
        }

        return $this;
    }
}
