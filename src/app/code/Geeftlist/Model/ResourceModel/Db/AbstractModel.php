<?php

namespace Geeftlist\Model\ResourceModel\Db;


/**
 * @method \Geeftlist\Model\ModelFactory getModelFactory()
 * @method \Geeftlist\Model\ResourceFactory getResourceModelFactory()
 */
abstract class AbstractModel extends \OliveOil\Core\Model\ResourceModel\Db\AbstractModel
{
    protected \Geeftlist\Model\RepositoryFactory $repositoryFactory;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        mixed $idField,
        ?string $mainTable,
        array $data = []
    ) {
        $this->repositoryFactory = $context->getRepositoryFactory();
        parent::__construct($context, $idField, $mainTable, $data);
    }
}
