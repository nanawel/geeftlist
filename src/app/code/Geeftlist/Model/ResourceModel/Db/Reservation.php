<?php

namespace Geeftlist\Model\ResourceModel\Db;

class Reservation extends AbstractModel
{
    public const MAIN_TABLE = 'reservation';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'reservation_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @param \Geeftlist\Model\Reservation $object
     */
    protected function beforeSave(\OliveOil\Core\Model\AbstractModel $object): static {
        if (($amount = $object->getAmount()) !== null) {
            $object->setAmount((float) $amount);
        }

        return parent::beforeSave($object);
    }

    public function getGeeftees($object): \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $collection */
        $collection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE);
        $collection->joinTable(
            ['geeftee_gift' => Gift::GEEFTEE_GIFT_TABLE],
            'main_table.geeftee_id = geeftee_gift.geeftee_id',
            []
        )
            ->addFieldToFilter('geeftee_gift.gift_id', $object->getGiftId());

        return $collection;
    }
}
