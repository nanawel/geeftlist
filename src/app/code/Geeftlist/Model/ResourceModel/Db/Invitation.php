<?php

namespace Geeftlist\Model\ResourceModel\Db;


class Invitation extends AbstractModel
{
    public const MAIN_TABLE = 'invitation';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'invitation_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
