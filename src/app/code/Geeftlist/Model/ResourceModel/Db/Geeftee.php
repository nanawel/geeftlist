<?php

namespace Geeftlist\Model\ResourceModel\Db;

class Geeftee extends AbstractModel
{
    public const MAIN_TABLE = 'geeftee';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'geeftee_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @return bool TRUE if unique, FALSE otherwise
     */
    public function assertEmailUnique(\Geeftlist\Model\Geeftee $object): bool {
        $select = $this->connection->select($this->getMainTable())
            ->columns([$this->idField]);
        $select->where->equalTo('email', (string) $object->getEmail());
        if ($id = $object->getId()) {
            $select->where->notEqualTo('geeftee_id', (int) $id);
        }

        $result = $this->connection->query($select);
        return !$result->count();
    }

    /**
     * @return bool TRUE if unique, FALSE otherwise
     */
    public function assertNameUnique(\Geeftlist\Model\Geeftee $object): bool {
        $select = $this->connection->select($this->getMainTable())
            ->columns([$this->idField]);
        $select->where->equalTo('name', (string) $object->getName());
        if ($id = $object->getId()) {
            $select->where->notEqualTo('geeftee_id', (int) $id);
        }

        $result = $this->connection->query($select);
        return !$result->count();
    }
}
