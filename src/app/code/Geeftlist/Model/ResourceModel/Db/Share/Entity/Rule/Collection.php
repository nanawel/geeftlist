<?php
namespace Geeftlist\Model\ResourceModel\Db\Share\Entity\Rule;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Share\Entity\Rule;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Share\Entity\Rule>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Share\Entity\Rule::ENTITY_TYPE,
            'share_entity_rule_id',
            Rule::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
