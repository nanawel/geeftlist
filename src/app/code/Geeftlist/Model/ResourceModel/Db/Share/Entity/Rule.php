<?php

namespace Geeftlist\Model\ResourceModel\Db\Share\Entity;

use Geeftlist\Model\ResourceModel\Db\AbstractModel;

/**
 * Class Rule
 *
 * @method int getTargetEntityType()
 * @method $this setTargetEntityType(int $entityType)
 * @method int getTargetEntityId()
 * @method $this setTargetEntityId(int $entityId)
 * @method string getRuleType()
 * @method $this setRuleType(string $ruleType)
 * @method mixed getParams()
 * @method $this setParams(mixed $params)
 * @method int getPriority()
 * @method $this setPriority(int $priority)
 */
class Rule extends AbstractModel
{
    public const MAIN_TABLE = 'share_entity_rule';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'share_entity_rule_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
