<?php

namespace Geeftlist\Model\ResourceModel\Db;

/**
 * @method \Geeftlist\Model\ResourceFactory getResourceModelFactory()
 */
class Setup extends \OliveOil\Core\Model\ResourceModel\Db\Setup
{
    protected \Geeftlist\Model\RepositoryFactory $repositoryFactory;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Setup\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->repositoryFactory = $context->getRepositoryFactory();
    }

    /**
     * @return \Geeftlist\Model\RepositoryFactory
     */
    public function getRepositoryFactory() {
        return $this->repositoryFactory;
    }
}
