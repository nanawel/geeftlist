<?php

namespace Geeftlist\Model\ResourceModel\Db\Invitation;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Invitation;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Invitation>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Invitation::ENTITY_TYPE,
            'invitation_id',
            Invitation::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
