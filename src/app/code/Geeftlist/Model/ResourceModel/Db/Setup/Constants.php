<?php

namespace Geeftlist\Model\ResourceModel\Db\Setup;

interface Constants
{
    public const DEPRECATED_TABLES = [
        self::DEPRECATED_TABLE_CONFIG,
        self::DEPRECATED_TABLE_GIFT_THING,
        self::DEPRECATED_TABLE_THING,
    ];
    public const DEPRECATED_TABLE_CONFIG = 'config';
    public const DEPRECATED_TABLE_GIFT_THING = 'gift_thing';
    public const DEPRECATED_TABLE_THING = 'thing';
}
