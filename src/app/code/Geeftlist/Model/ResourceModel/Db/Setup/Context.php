<?php

namespace Geeftlist\Model\ResourceModel\Db\Setup;


class Context extends \OliveOil\Core\Model\ResourceModel\Db\Setup\Context
{
    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Connection $connection,
        \OliveOil\Core\Model\ResourceModel\Db\Transaction $transaction,
        \OliveOil\Core\Service\GenericFactoryInterface $modelFactory,
        \OliveOil\Core\Model\FactoryInterface $resourceModelFactory,
        \OliveOil\Core\Model\App\ConfigInterface $appConfig,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Di\Container $container,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Model\App\FlagInterface $appFlag,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory
    ) {
        parent::__construct(
            $connection,
            $transaction,
            $modelFactory,
            $resourceModelFactory,
            $eventService,
            $logService,
            $container,
            $appConfig,
            $appFlag
        );
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }
}
