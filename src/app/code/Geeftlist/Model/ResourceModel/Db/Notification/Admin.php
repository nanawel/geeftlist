<?php

namespace Geeftlist\Model\ResourceModel\Db\Notification;

use Geeftlist\Model\Notification;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;

class Admin extends AbstractModel implements \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            null,
            null,
            $data
        );
    }

    public function addRecipient(
        \Geeftlist\Model\Notification $object,
        $recipient,
        $status = Notification::STATUS_CREATED,
        $additionalData = []
    ): static {
        // Ignore here

        return $this;
    }

    public function updateRecipientStatus(\Geeftlist\Model\Notification $object, $recipient, $status): static {
        // Ignore here

        return $this;
    }

    public function getRecipientStatus(\Geeftlist\Model\Notification $object, $recipient) {
        return $object->getStatus();
    }

    public function getAllRecipientStatus(\Geeftlist\Model\Notification $object): array {
        return [];
    }

    public function getLastEventTimeByRecipient($status, $recipientIds = [], \DateTime $onlyBefore = null): array {
        return [];
    }

    public function getEventCountByRecipient($status = null, $recipientIds = []): array {
        return [];
    }

    protected function assertAdmin(...$objects) {
        foreach ($objects as $object) {
            if (is_array($object)) {
                $this->assertAdmin(...$object);
            } elseif ($object instanceof \Geeftlist\Model\Notification) {
                if ($object->getRecipientType() != Notification::RECIPIENT_TYPE_ADMIN) {
                    throw new \InvalidArgumentException('Not an Admin notification.');
                }
            }
        }
    }
}
