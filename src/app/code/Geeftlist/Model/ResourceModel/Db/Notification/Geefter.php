<?php

namespace Geeftlist\Model\ResourceModel\Db\Notification;

use Geeftlist\Model\Notification;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\InsertMultiple;
use OliveOil\Core\Helper\DateTime;

class Geefter extends AbstractModel implements \Geeftlist\Model\ResourceModel\Iface\Notification\Recipient
{
    public const NOTIFICATION_GEEFTER_TABLE = 'notification_geefter';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            null,
            null,
            $data
        );
    }

    public function addRecipient(
        \Geeftlist\Model\Notification $object,
        $recipient,
        $status = Notification::STATUS_CREATED,
        $additionalData = []
    ): static {
        $this->assertGeefter($object, $recipient);
        if (! is_array($recipient)) {
            $recipient = [$recipient];
        }

        if ($recipient === []) {
            return $this;
        }

        $insert = $this->connection->insertMultiple(Geefter::NOTIFICATION_GEEFTER_TABLE)
            ->columns(array_merge(['notification_id', 'geefter_id', 'status'], array_keys($additionalData)));

        foreach ($recipient as $g) {
            $insert->values(
                [
                    'notification_id' => $object->getId(),
                    'geefter_id' => $g->getId(),
                    'status' => $status
                ] + $additionalData,
                InsertMultiple::VALUES_MERGE
            );
        }

        $this->connection->query($insert);

        return $this;
    }

    public function updateRecipientStatus(
        \Geeftlist\Model\Notification $object,
        $recipient,
        $status
    ): static {
        $this->assertGeefter($object, $recipient);
        if (! is_array($recipient)) {
            $recipient = [$recipient];
        }

        $geefterIds = [];
        foreach ($recipient as $g) {
            $geefterIds[] = $g->getId();
        }

        $update = $this->connection->update(Geefter::NOTIFICATION_GEEFTER_TABLE);
        $update->set(['status' => $status])
            ->where->equalTo('notification_id', $object->getId())
            ->and->in('geefter_id', $geefterIds);
        $this->connection->query($update);

        return $this;
    }

    public function getRecipientStatus(
        \Geeftlist\Model\Notification $object,
        $recipient
    ) {
        $this->assertGeefter($object, $recipient);
        $select = $this->connection->select(Geefter::NOTIFICATION_GEEFTER_TABLE);
        $select->columns(['status'])
            ->where->equalTo('notification_id', $object->getId())
            ->and->equalTo('geefter_id', $recipient->getId());

        $resultSet = $this->connection->query($select);
        if ($resultSet->count() === 0) {
            return false;
        }

        return $this->connection->fetchFirstValue($resultSet);
    }

    public function getAllRecipientStatus(\Geeftlist\Model\Notification $object): array {
        $this->assertGeefter($object);
        $select = $this->connection->select(Geefter::NOTIFICATION_GEEFTER_TABLE);
        $select->columns(['geefter_id', 'status'])
            ->where->equalTo('notification_id', $object->getId());

        $resultSet = $this->connection->query($select)->toArray();

        return array_column($resultSet, 'status', 'geefter_id');
    }

    public function getLastEventTimeByRecipient($status, $recipientIds = [], \DateTime $onlyBefore = null): array {
        $select = $this->connection->select(Geefter::NOTIFICATION_GEEFTER_TABLE);
        $select->columns([
            'geefter_id'      => 'geefter_id',
            'last_event_time' => new Expression('MAX(created_at)')
        ])
            ->where->equalTo('status', $status);
        if (! empty($recipientIds)) {
            $select->where->in('geefter_id', $recipientIds);
        }

        if ($onlyBefore instanceof \DateTime) {
            $select->having->lessThanOrEqualTo('last_event_time', DateTime::getDateSql($onlyBefore));
        }

        $select->group('geefter_id');
        $resultSet = $this->connection->query($select)->toArray();

        return array_column($resultSet, 'last_event_time', 'geefter_id');
    }

    public function getEventCountByRecipient($status = null, $recipientIds = []): array {
        $select = $this->connection->select(Geefter::NOTIFICATION_GEEFTER_TABLE);
        $select->columns([
            'geefter_id' => 'geefter_id',
            'cnt'        => new Expression('COUNT(*)')
        ]);
        if ($status) {
            $select->where->equalTo('status', $status);
        }

        if (! empty($recipientIds)) {
            $select->where->in('geefter_id', $recipientIds);
        }

        $select->group('geefter_id');
        $resultSet = $this->connection->query($select)->toArray();

        return array_column($resultSet, 'cnt', 'geefter_id');
    }

    protected function assertGeefter(...$objects) {
        foreach ($objects as $object) {
            if (is_array($object)) {
                $this->assertGeefter(...$object);
            } elseif ($object instanceof \Geeftlist\Model\Notification) {
                if ($object->getRecipientType() != Notification::RECIPIENT_TYPE_GEEFTER) {
                    throw new \InvalidArgumentException('Not a Geefter notification.');
                }
            } elseif (! $object instanceof \Geeftlist\Model\Geefter) {
                throw new \InvalidArgumentException('Not a Geefter.');
            }
        }
    }
}
