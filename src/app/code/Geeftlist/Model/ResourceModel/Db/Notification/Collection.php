<?php
namespace Geeftlist\Model\ResourceModel\Db\Notification;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Notification;
use Laminas\Db\Sql\Select;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Notification>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Notification::ENTITY_TYPE,
            'notification_id',
            Notification::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }

    public function getTargetIds(): array {
        $select = $this->getFullSelect();
        $select->columns(['target_id'])
            ->quantifier(Select::QUANTIFIER_DISTINCT);
        $result = $this->connection->query($select);

        return $this->connection->fetchCol($result, 'target_id');
    }
}
