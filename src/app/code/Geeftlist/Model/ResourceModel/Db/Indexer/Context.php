<?php

namespace Geeftlist\Model\ResourceModel\Db\Indexer;


class Context
{
    /** @var \Geeftlist\Model\Share\Entity\Rule[] */
    protected $staticEntityShareRules;

    protected \OliveOil\Core\Service\Log $logService;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \OliveOil\Core\Model\RepositoryInterface $entityShareRuleRepository,
        protected \OliveOil\Core\Service\GenericFactoryInterface $shareRuleTypeFactoryFactory
    ) {
        $this->logService = $context->getLogService();
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }

    public function getEntityShareRuleRepository(): \OliveOil\Core\Model\RepositoryInterface {
        return $this->entityShareRuleRepository;
    }

    public function getShareRuleTypeFactoryFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->shareRuleTypeFactoryFactory;
    }

    /**
     * @return \Geeftlist\Model\Share\Entity\Rule[]
     */
    public function getStaticEntityShareRules() {
        return $this->staticEntityShareRules;
    }

    /**
     * @return \OliveOil\Core\Service\Log
     */
    public function getLogService() {
        return $this->logService;
    }
}
