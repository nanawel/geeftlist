<?php

namespace Geeftlist\Model\ResourceModel\Db\Indexer;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface;
use Geeftlist\Model\Share\RuleTypeInterface;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Select;

abstract class AbstractGeefterAccess extends AbstractModel implements GeefterAccessInterface
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        mixed $idField,
        string $mainTable,
        protected string $entityType,
        array $data = []
    ) {
        parent::__construct($context, $idField, $mainTable, $data);
    }

    /**
     * @inheritDoc
     */
    public function isAllowed(Geefter $geefter, array $entities, array $actions): bool {
        $entityIds = [];
        foreach ($entities as $e) {
            if (! $e instanceof \Geeftlist\Model\AbstractModel) {
                throw new \InvalidArgumentException('Not a valid entity object.');
            }

            // Exclude entities with no ID = always allow actions on *new* entities
            if ($e->getId()) {
                $entityIds[] = $e->getId();
            }
        }

        if ($actions === [] || $entityIds === []) {
            return true;
        }

        \sort($entities);

        $select = $this->connection->select($this->getMainTable());
        $select->columns([
            'entity_id'       => 'entity_id',
            'allowed_actions' => new Expression('GROUP_CONCAT(action_type ORDER BY action_type ASC)')
        ])
            ->where->equalTo('geefter_id', $geefter->getId())
            ->and->in('entity_id', $entityIds)
            ->and->equalTo('is_allowed', RuleTypeInterface::ALLOWED);
        $select->group('entity_id');

        $allowedEntityIds = [];
        $result = $this->connection->query($select)->toArray();
        foreach ($result as $entityAllowedActions) {
            $allowedActions = explode(',', (string) $entityAllowedActions['allowed_actions']);
            if (array_diff($actions, $allowedActions) !== []) {
                // First mismatch: don't go any further and return FALSE
                return false;
            }

            // Otherwise, add the current entity to the list of allowed ones
            $allowedEntityIds[] = $entityAllowedActions['entity_id'];
        }

        \sort($allowedEntityIds);

        // *All* entities must be allowed for the given actions to return TRUE
        return $allowedEntityIds == $entityIds;
    }

    /**
     * @inheritDoc
     */
    public function getAllowed(array $entities, array $actions): array {
        $entityIds = [];
        foreach ($entities as $e) {
            if ($e === null) {
                throw new \InvalidArgumentException('Entity cannot be null.');
            }

            $entityIds[] = $e->getId();
        }

        \sort($entityIds);
        \sort($actions);

        /*
         * That query took me some time to figure out. Hope it's producing the expected result.
         *
         * Example of result:
         * SELECT DISTINCT t1.geefter_id AS geefter_id
         * FROM
         *     (SELECT t2.geefter_id AS geefter_id,
         *             GROUP_CONCAT(entity_id
         *                          ORDER BY entity_id ASC) AS allowed_entities
         *      FROM
         *          (SELECT geefter_access_gift_idx.geefter_id AS geefter_id,
         *                  geefter_access_gift_idx.entity_id AS entity_id,
         *                  GROUP_CONCAT(action_type
         *                               ORDER BY action_type ASC) AS allowed_actions
         *           FROM geefter_access_gift_idx
         *           WHERE entity_type = 'gift'
         *               AND entity_id IN ('1678')
         *               AND action_type IN ('be_notified')
         *               AND is_allowed = '1'
         *           GROUP BY geefter_id,
         *                    entity_id
         *           HAVING allowed_actions = 'be_notified') AS t2
         *      GROUP BY geefter_id
         *      HAVING allowed_entities = '1678') AS t1
         */

        $subSelect2 = $this->connection->select($this->getMainTable());
        $subSelect2->columns([
            'geefter_id'      => 'geefter_id',
            'entity_id'       => 'entity_id',
            'allowed_actions' => new Expression('GROUP_CONCAT(action_type ORDER BY action_type ASC)')
        ]);
        $subSelect2->where->equalTo('entity_type', $this->entityType)
            ->and->in('entity_id', $entityIds)
            ->and->in('action_type', $actions)
            ->and->equalTo('is_allowed', RuleTypeInterface::ALLOWED);
        $subSelect2->group(['geefter_id', 'entity_id']);
        $subSelect2->having->equalTo('allowed_actions', implode(',', $actions));

        $subSelect1 = $this->connection->select(['t2' => $subSelect2]);
        $subSelect1->columns([
            'geefter_id'       => 'geefter_id',
            'allowed_entities' => new Expression('GROUP_CONCAT(entity_id ORDER BY entity_id ASC)')
        ]);
        $subSelect1->group(['geefter_id']);

        $subSelect1->having->equalTo('allowed_entities', implode(',', $entityIds));

        $select = $this->connection->select(['t1' => $subSelect1])
            ->quantifier(Select::QUANTIFIER_DISTINCT)
            ->columns(['geefter_id']);
        $result = $this->connection->query($select);

        return $this->connection->fetchCol($result, 'geefter_id');
    }

    /**
     * @param string $entityTableAlias
     * @param string $entityIdColumnAlias
     * @param string $actionType
     * @return $this
     */
    public function addGeefterAccessRestrictionsToEntityCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $entityTableAlias,
        $entityIdColumnAlias,
        $actionType
    ): static {
        $idxTableAlias = 'geefter_access_idx';

        $select = $this->connection->select([$idxTableAlias => $this->getMainTable()]);
        $select->where->equalTo(
            sprintf('%s.%s', $entityTableAlias, $entityIdColumnAlias),
            $idxTableAlias . '.entity_id',
            PredicateInterface::TYPE_IDENTIFIER,
            PredicateInterface::TYPE_IDENTIFIER
        )
            ->AND->equalTo($idxTableAlias . '.geefter_id', $geefter->getId())
            ->AND->equalTo($idxTableAlias . '.entity_type', $this->entityType)
            ->AND->equalTo($idxTableAlias . '.action_type', $actionType)
            ->AND->equalTo($idxTableAlias . '.is_allowed', RuleTypeInterface::ALLOWED);
        $collection->getSelect()->where(['EXISTS (?)' => $select]);

        return $this;
    }

    /**
     * Append allowed actions to the specified entity collection.
     *
     * @param string $entityTableAlias
     * @param string $entityIdColumnAlias
     * @return $this
     */
    public function appendGeefterAccessPermissionsToEntityCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter,
        $entityTableAlias,
        $entityIdColumnAlias
    ): static {
        $idxTableAlias = 'geefter_access_permissions_idx';
        $permissionSelect = $this->connection->select([$idxTableAlias => 'geefter_access_gift_idx'])
            ->columns([
                new Expression(
                    sprintf('GROUP_CONCAT(%s.action_type ORDER BY %s.action_type ASC)', $idxTableAlias, $idxTableAlias)
                )
            ]);
        $permissionSelect->where->equalTo(
            $idxTableAlias . '.entity_id',
            sprintf('%s.%s', $entityTableAlias, $entityIdColumnAlias),
            PredicateInterface::TYPE_IDENTIFIER,
            PredicateInterface::TYPE_IDENTIFIER
        )
            ->and->equalTo(
                $idxTableAlias . '.entity_type',
                $this->entityType,
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_VALUE
            )
            ->and->equalTo(
                $idxTableAlias . '.geefter_id',
                $geefter->getId(),
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_VALUE
            )
            ->and->equalTo(
                $idxTableAlias . '.is_allowed',
                RuleTypeInterface::ALLOWED,
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_VALUE
            );
        $collection->addFieldToSelect([
            'allowed_actions' => $permissionSelect
        ]);

        // Postprocess calculated allowed_actions field on collection items
        $collection->attachInstanceListener('load::after', static function () use ($collection, $geefter): void {
            /** @var \Geeftlist\Model\AbstractModel $entity */
            foreach ($collection as $entity) {
                $entity->setData(
                    'allowed_actions_geefter' . $geefter->getId(),
                    explode(',', (string) $entity->getAllowedActions())
                );
            }
        });

        return $this;
    }
}
