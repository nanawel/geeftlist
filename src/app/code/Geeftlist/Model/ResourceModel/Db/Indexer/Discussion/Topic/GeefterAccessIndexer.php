<?php

namespace Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic;


use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\ResourceModel\Db\Indexer\AbstractGeefterAccessIndexer;

class GeefterAccessIndexer extends AbstractGeefterAccessIndexer
{
    public const MAIN_TABLE = 'geefter_access_discussion_topic_idx';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        \Geeftlist\Model\ResourceModel\Db\Indexer\Context $geefterAccessIndexerContext,
        array $staticEntityShareRules = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $geefterAccessIndexerContext,
            ['entity_type', 'entity_id', 'geefter_id', 'action_type'],
            self::MAIN_TABLE,
            Topic::ENTITY_TYPE,
            $staticEntityShareRules,
            $data
        );
    }
}
