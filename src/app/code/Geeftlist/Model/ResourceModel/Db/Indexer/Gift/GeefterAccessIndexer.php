<?php

namespace Geeftlist\Model\ResourceModel\Db\Indexer\Gift;


use Geeftlist\Model\Gift;
use Geeftlist\Model\ResourceModel\Db\Indexer\AbstractGeefterAccessIndexer;

class GeefterAccessIndexer extends AbstractGeefterAccessIndexer
{
    public const MAIN_TABLE = 'geefter_access_gift_idx';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        \Geeftlist\Model\ResourceModel\Db\Indexer\Context $geefterAccessIndexerContext,
        array $staticEntityShareRules = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $geefterAccessIndexerContext,
            ['entity_type', 'entity_id', 'geefter_id', 'action_type'],
            self::MAIN_TABLE,
            Gift::ENTITY_TYPE,
            $staticEntityShareRules,
            $data
        );
    }
}
