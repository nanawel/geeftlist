<?php

namespace Geeftlist\Model\ResourceModel\Db\Indexer;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Geeftlist\Model\ResourceModel\Iface\IndexerInterface;
use Geeftlist\Model\Share\Constants;
use Geeftlist\Model\Share\RuleTypeInterface;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\PredicateInterface;

abstract class AbstractGeefterAccessIndexer extends AbstractModel implements IndexerInterface
{
    protected \Geeftlist\Model\RepositoryFactory $repositoryFactory;

    protected \OliveOil\Core\Model\RepositoryInterface $entityShareRuleRepository;

    protected \OliveOil\Core\Service\GenericFactoryInterface $shareRuleTypeFactory;

    protected \Psr\Log\LoggerInterface $logger;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        \Geeftlist\Model\ResourceModel\Db\Indexer\Context $geefterAccessIndexerContext,
        mixed $idField,
        string $mainTable,
        protected string $entityType,
        /** @var \Geeftlist\Model\Share\Entity\Rule[] */
        protected array $staticEntityShareRules = [],
        array $data = []
    ) {
        $this->repositoryFactory = $geefterAccessIndexerContext->getRepositoryFactory();
        $this->entityShareRuleRepository = $geefterAccessIndexerContext->getEntityShareRuleRepository();
        $this->shareRuleTypeFactory = $geefterAccessIndexerContext->getShareRuleTypeFactoryFactory()
            ->getFromCode($entityType);
        $this->logger = $geefterAccessIndexerContext->getLogService()->getLoggerForClass($this);
        parent::__construct($context, $idField, $mainTable, $data);
    }

    public function addGeefterAccessRestrictionsToEntityCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        string $entityTableAlias,
        string $entityIdColumnAlias,
        string $actionType
    ): static {
        $idxTableAlias = 'geefter_access_idx';

        $select = $this->connection->select([$idxTableAlias => $this->getMainTable()]);
        $select->where->equalTo(
            sprintf('%s.%s', $entityTableAlias, $entityIdColumnAlias),
            $idxTableAlias . '.entity_id',
            PredicateInterface::TYPE_IDENTIFIER,
            PredicateInterface::TYPE_IDENTIFIER
        )
            ->AND->equalTo($idxTableAlias . '.geefter_id', $geefter->getId())
            ->AND->equalTo($idxTableAlias . '.entity_type', $this->getEntityType())
            ->AND->equalTo($idxTableAlias . '.action_type', $actionType)
            ->AND->equalTo($idxTableAlias . '.is_allowed', RuleTypeInterface::ALLOWED);
        $collection->getSelect()->where(['EXISTS (?)' => $select]);

        return $this;
    }

    /**
     * Append allowed actions to the specified entity collection.
     */
    public function appendGeefterAccessPermissionsToEntityCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter,
        string $entityTableAlias,
        string $entityIdColumnAlias
    ): static {
        $idxTableAlias = 'geefter_access_permissions_idx';
        $permissionSelect = $this->connection->select([$idxTableAlias => 'geefter_access_gift_idx'])
            ->columns([
                new Expression(
                    sprintf('GROUP_CONCAT(%s.action_type ORDER BY %s.action_type ASC)', $idxTableAlias, $idxTableAlias)
                )
            ]);
        $permissionSelect->where->equalTo(
            $idxTableAlias . '.entity_id',
            sprintf('%s.%s', $entityTableAlias, $entityIdColumnAlias),
            PredicateInterface::TYPE_IDENTIFIER,
            PredicateInterface::TYPE_IDENTIFIER
        )
            ->and->equalTo(
                $idxTableAlias . '.entity_type',
                $this->getEntityType(),
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_VALUE
            )
            ->and->equalTo(
                $idxTableAlias . '.geefter_id',
                $geefter->getId(),
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_VALUE
            )
            ->and->equalTo(
                $idxTableAlias . '.is_allowed',
                RuleTypeInterface::ALLOWED,
                PredicateInterface::TYPE_IDENTIFIER,
                PredicateInterface::TYPE_VALUE
            );
        $collection->addFieldToSelect([
            'allowed_actions' => $permissionSelect
        ]);

        // Postprocess calculated allowed_actions field on collection items
        $collection->attachInstanceListener('load::after', static function () use ($collection, $geefter): void {
            /** @var \Geeftlist\Model\AbstractModel $entity */
            foreach ($collection as $entity) {
                $entity->setData(
                    'allowed_actions_geefter' . $geefter->getId(),
                    explode(',', (string) $entity->getAllowedActions())
                );
            }
        });

        return $this;
    }

    /**
     * @param \Geeftlist\Model\AbstractModel[] $entities
     */
    public function reindexEntities(array $entities): void {
        try {
            $this->connection->beginTransaction();
            $reindexedEntityIds = [];
            foreach ($entities as $entity) {
                // Prevent reindexing same entity multiple times
                if (! isset($reindexedEntityIds[$entity->getId()])) {
                    $this->reindexEntity($entity);
                }

                $reindexedEntityIds[$entity->getId()] = true;
            }

            $this->connection->commitTransaction();
        }
        catch (\Throwable $throwable) {
            $this->logger->critical($throwable);
            $this->connection->rollbackTransaction();

            throw $throwable;
        }
    }

    public function reindexEntity(\Geeftlist\Model\AbstractModel $entity): void {
        if ($entity->isDeleted()) {
            $this->clearTable($entity->getId());
            $this->logger->debug(sprintf(
                'Deleted entity #%d has been reindexed (index cleared)',
                $entity->getId()
            ));

            return;
        }

        $entityId = $entity->getId();

        $entityRules = $this->collectEntityRules($entityId);
        $indexData = $this->collectIndexData($entity, $entityRules);
        $insertRows = $this->indexDataToRows($indexData, $entityId);

        $insert = $this->connection->insertMultiple($this->getMainTable())
            ->columns(array_keys($this->getRowTemplate()))
            ->values($insertRows);

        try {
            $this->connection->beginTransaction();
            $this->clearEntity($entityId);
            if ($indexData !== []) {
                $this->connection->query($insert);
            }

            $this->connection->commitTransaction();
        }
        catch (\Throwable $throwable) {
            $this->logger->critical($throwable);
            $this->connection->rollbackTransaction();

            throw $throwable;
        }

        $this->logger->debug(sprintf(
            'Entity #%d has been reindexed (%d share entity rules generated %d index rows for %d geefters)',
            $entity->getId(),
            count($entityRules),
            count($indexData),
            count(array_unique(array_keys($indexData)))
        ));
    }

    protected function collectIndexData(
        \Geeftlist\Model\AbstractModel $entity,
        array $entityRules
    ): array {
        $indexData = [];

//        $transportObject = new MagicObject([
//            'index_data' => [],
//            'indexer' => $this,
//            'entity_rules' => $entityRules,
//            'entity' => $entity
//        ]);
//        $entity->triggerEvent('collectIndexData::before', ['transport_object' => $transportObject]);
//        $indexData = $transportObject->getData('index_data');
//        $entityRules = $transportObject->getData('entity_rules');

        foreach ($entityRules as $entityRule) {
            /** @var \Geeftlist\Model\Share\RuleTypeInterface $shareRuleType */
            $shareRuleType = $this->shareRuleTypeFactory->resolveGet($entityRule->getRuleType());
            $indexData = $this->mergeIndexData(
                $indexData,
                $shareRuleType->getActionAuthorizationsByGeefter($entityRule, $entity->getId())
            );
        }

//        $transportObject = new MagicObject([
//            'index_data' => $indexData,
//            'indexer' => $this,
//            'entity_rules' => $entityRules,
//            'entity' => $entity
//        ]);
//        $entity->triggerEvent('collectIndexData::after', ['transport_object' => $transportObject]);
//
//        return $transportObject->getData('index_data');

        return $indexData;
    }

    public function mergeIndexData(array $indexData, array $indexDataToMerge): array {
        // If WILDCARD_GEEFTER_ID is present, remove all existing index data sharing the same actions
        if (array_key_exists(Constants::WILDCARD_GEEFTER_ID, $indexDataToMerge)) {
            $overridingPermissions = $indexDataToMerge[Constants::WILDCARD_GEEFTER_ID];
            foreach ($indexData as $geefterId => $geefterPermissions) {
                $newGeefterPermissions = array_diff_key($geefterPermissions, $overridingPermissions);
                if ($newGeefterPermissions === []) {
                    unset($indexData[$geefterId]);
                }
                else {
                    $indexData[$geefterId] = $newGeefterPermissions;
                }
            }

            // We do not insert permissions for wildcard geefter in the index
            unset($indexDataToMerge[Constants::WILDCARD_GEEFTER_ID]);
        }

        foreach (array_keys($indexDataToMerge) as $geefterId) {
            if (array_key_exists($geefterId, $indexData)) {
                $indexData[$geefterId] = array_merge($indexData[$geefterId], $indexDataToMerge[$geefterId]);
            }
            else {
                $indexData[$geefterId] = $indexDataToMerge[$geefterId];
            }
        }

        return $indexData;
    }

    /**
     * @return \Geeftlist\Model\Share\Entity\Rule[]
     */
    protected function collectEntityRules(int $entityId): array {
        $entityShareRules = $this->entityShareRuleRepository->find([
            'filter' => [
                'target_entity_type' => [['eq' => $this->getEntityType()]],
                'target_entity_id'   => [['eq' => $entityId]]
            ],
            'sort_order' => [
                'priority' => SORT_ASC
            ]
        ]);

        $sortedEntityShareRules = [];
        /** @var \Geeftlist\Model\Share\Entity\Rule $entityShareRule */
        foreach ($entityShareRules as $entityShareRule) {
            $sortedEntityShareRules[(int) $entityShareRule->getPriority()] = $entityShareRule;
        }

        foreach ($this->staticEntityShareRules as $entityShareRule) {
            $sortedEntityShareRules[(int) $entityShareRule->getPriority()] = $entityShareRule;
        }

        ksort($sortedEntityShareRules, SORT_NUMERIC);

        return $sortedEntityShareRules;
    }

    /**
     * @param int $entityId
     */
    protected function indexDataToRows(array $indexData, $entityId): array {
        $rowTemplate = $this->getRowTemplate();

        $rows = [];
        foreach ($indexData as $geefterId => $geefterPermissions) {
            foreach ($geefterPermissions as $action => $isAllowed) {
                $rows[] = array_merge($rowTemplate, [
                    'entity_id'   => (int) $entityId,
                    'geefter_id'  => (int) $geefterId,
                    'action_type' => $action,
                    'is_allowed'  => (int) $isAllowed
                ]);
            }
        }

        return $rows;
    }

    protected function getRowTemplate(): array {
        return [
            'entity_type' => $this->getEntityType(),
            'entity_id'   => null,
            'geefter_id'  => null,
            'action_type' => null,
            'is_allowed'  => null
        ];
    }

    public function clearEntity(int $entityId): static {
        return $this->clearTable($entityId);
    }

    public function clearTable(?int $entityId = null): static {
        $delete = $this->connection->delete($this->getMainTable());
        $delete->where->equalTo('entity_type', $this->getEntityType());
        if ($entityId) {
            $delete->where->equalTo('entity_id', $entityId);
        }

        $this->connection->query($delete);

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityType() {
        return $this->entityType;
    }
}
