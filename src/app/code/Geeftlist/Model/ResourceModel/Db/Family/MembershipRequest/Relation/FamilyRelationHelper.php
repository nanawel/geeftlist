<?php

namespace Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation;

use Geeftlist\Model\ResourceModel\Db\Family;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class FamilyRelationHelper extends AbstractRelationHelper
{
    public function join_family(Family\MembershipRequest\Collection $collection): static {
        $collection->joinTable(
            ['family' => Family::MAIN_TABLE],
            'main_table.family_id = family.family_id',
            Select::SQL_STAR,
            Select::JOIN_INNER,
            true
        );

        return $this;
    }
}
