<?php

namespace Geeftlist\Model\ResourceModel\Db\Family;

use Geeftlist\Model\ResourceModel\Db\AbstractModel;

class MembershipRequest extends AbstractModel
{
    public const MAIN_TABLE = 'family_membership_request';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'membership_request_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
