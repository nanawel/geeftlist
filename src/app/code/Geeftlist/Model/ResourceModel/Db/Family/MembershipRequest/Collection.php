<?php

namespace Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Family;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Family\MembershipRequest>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Family\MembershipRequest::ENTITY_TYPE,
            'membership_request_id',
            Family\MembershipRequest::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
