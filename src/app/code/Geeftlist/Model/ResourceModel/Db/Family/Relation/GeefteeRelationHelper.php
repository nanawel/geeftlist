<?php

namespace Geeftlist\Model\ResourceModel\Db\Family\Relation;

use Geeftlist\Model\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Family;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GeefteeRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_geeftees_in(array $geefteeIds): array {
        return $this->filterConditionToPredicate_geeftees($geefteeIds, true);
    }

    protected function filterConditionToPredicate_geeftees_nin(array $geefteeIds): array {
        return $this->filterConditionToPredicate_geeftees($geefteeIds, false);
    }

    protected function filterConditionToPredicate_geeftee_eq(Geeftee|int $geefteeId): array {
        return $this->filterConditionToPredicate_geeftees([$geefteeId], true);
    }

    protected function filterConditionToPredicate_geeftee_ne(Geeftee|int $geefteeId): array {
        return $this->filterConditionToPredicate_geeftees([$geefteeId], false);
    }

    /**
     * @param Geeftee[]|int[] $geefteeIds
     * @return array
     */
    private function filterConditionToPredicate_geeftees(array $geefteeIds, bool $in = true): array {
        $geefteeIds = Family\Collection::toIdArray($geefteeIds);

        $select = $this->connection->select(
            ['family_geeftee_filter' => \Geeftlist\Model\ResourceModel\Db\Family\Geeftee::MAIN_TABLE]
        );
        $select->where->equalTo(
            'family_geeftee_filter.family_id',
            'main_table.family_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($geefteeIds !== []) {
            $select->where->in('family_geeftee_filter.geeftee_id', $geefteeIds);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }
}
