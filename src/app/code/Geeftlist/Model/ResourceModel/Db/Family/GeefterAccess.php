<?php

namespace Geeftlist\Model\ResourceModel\Db\Family;


use Geeftlist\Model\Family\Field\Visibility;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Family;
use Geeftlist\Model\ResourceModel\Iface\Family\RestrictionsAppenderInterface;
use Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface;
use Geeftlist\Model\Share\Family\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Exception\NotImplementedException;

class GeefterAccess implements GeefterAccessInterface, RestrictionsAppenderInterface
{
    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context
    ) {
        $this->connection = $context->getConnection();
    }

    /**
     * @inheritDoc
     * @throws \Throwable
     */
    public function isAllowed(Geefter $geefter, array $entities, array $actions): bool {
        $entityIds = [];
        foreach ($entities as $e) {
            if (! $e instanceof \Geeftlist\Model\Family) {
                throw new \InvalidArgumentException('Not a family.');
            }

            // Exclude entities with no ID = always allow actions on *new* entities
            if ($e->getId()) {
                $entityIds[] = $e->getId();
            }
        }

        if ($actions === [] || $entityIds === []) {
            return true;
        }

        sort($entityIds);
        sort($actions);

        $allowedActionsByEntityId = [];

        // Use $this->addGeefterAccessRestrictionsToSelect() for each required $action, and aggregate results
        // for later comparison
        foreach ($actions as $action) {
            $actionSelect = $this->connection->select(['main_table' => Family::MAIN_TABLE])
                ->columns(['family_id']);
            $actionSelect->where->in('family_id', $entityIds);
            $this->addGeefterAccessRestrictionsToSelect(
                $actionSelect,
                $geefter,
                'main_table',
                'family_id',
                null,
                $action
            );
            $result = $this->connection->query($actionSelect);

            foreach ($this->connection->fetchCol($result, 'family_id') as $entityId) {
                $allowedActionsByEntityId[$entityId][] = $action;
            }
        }

        foreach ($entityIds as $entityId) {
            // Missing all permissions for family
            if (!isset($allowedActionsByEntityId[$entityId])) {
                return false;
            }

            sort($allowedActionsByEntityId[$entityId]);
            // Missing at least one required permission for a family
            if ($allowedActionsByEntityId[$entityId] != $actions) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getAllowed(array $entities, array $actions): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * Exclude unavailable gifts for the specified geefter.
     *
     * @throws \Exception
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $this->addGeefterAccessRestrictionsToCollection(
            $collection,
            $geefter,
            'main_table',
            'family_id',
            null,
            $actionType
        );

        return $this;
    }

    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection $collection
     * @param string $familyTableAlias
     * @param string $familyIdColumnAlias
     * @param string $actionType
     * @param PredicateInterface $additionalPredicate
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $familyTableAlias,
        $familyIdColumnAlias = 'family_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): static {
        $this->addGeefterAccessRestrictionsToSelect(
            $collection->getSelect(),
            $geefter,
            $familyTableAlias,
            $familyIdColumnAlias,
            $entityTypeColumnAlias,
            $actionType,
            $additionalPredicate
        );

        return $this;
    }

    /**
     * @param string $familyIdColumnAlias
     * @param PredicateInterface|null $additionalPredicate
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToSelect(
        Select $select,
        Geefter $geefter,
        string $familyTableAlias,
        $familyIdColumnAlias = 'family_id',
        $entityTypeColumnAlias = null,
        string $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): void {
        if ($entityTypeColumnAlias !== null) {
            throw new NotImplementedException('$entityTypeColumnAlias is not supported.');
        }

        if ($additionalPredicate !== null) {
            throw new NotImplementedException('$additionalPredicate is not supported.');
        }

        $orPredicates = [];
        switch ($actionType) {
            case TypeInterface::ACTION_VIEW:
                $orPredicates[] = new Expression(
                    'EXISTS (?)',
                    $this->getExistsVisibleFamilySelect($geefter, $familyTableAlias, $familyIdColumnAlias)
                );
                break;

            case TypeInterface::ACTION_EDIT:
            case TypeInterface::ACTION_DELETE:
            case TypeInterface::ACTION_TRANSFER:
            case TypeInterface::ACTION_REMOVE_GEEFTEE:
                $orPredicates[] = new Expression($familyTableAlias . '.owner_id = ?', $geefter->getId());
                break;

            case TypeInterface::ACTION_ADD_GEEFTEE:
                // Nobody, even the owner can directly add a geeftee to a family
                $orPredicates[] = new Expression('FALSE');
                break;

            default:
                throw new InvalidValueException('Invalid action type: ' . $actionType);
        }

        $select->where->nest()
            ->addPredicates($orPredicates, PredicateSet::COMBINED_BY_OR)
            ->unnest();
    }

    /**
     * Append allowed actions to the specified gift collection.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): never {
        throw new NotImplementedException(__METHOD__);
    }

    public function getExistsVisibleFamilySelect(
        Geefter $geefter,
        string $familyTableAlias,
        string $familyIdColumnAlias
    ): array {
        $select = $this->connection->select(
            ['family_geeftee_visibility_restrictions' => Family\Geeftee::MAIN_TABLE]
        );
        $select->where
            ->NEST
                ->in($familyTableAlias . '.visibility', [Visibility::PUBLIC, Visibility::PROTECTED])
                ->OR
                ->NEST
                    ->equalTo(
                        sprintf('%s.%s', $familyTableAlias, $familyIdColumnAlias),
                        'family_geeftee_visibility_restrictions.family_id',
                        Predicate::TYPE_IDENTIFIER,
                        Predicate::TYPE_IDENTIFIER
                    )
                    ->equalTo($familyTableAlias . '.visibility', Visibility::PRIVATE)
                    ->AND
                    ->equalTo('family_geeftee_visibility_restrictions.geeftee_id', $geefter->getGeeftee()->getId())
                ->UNNEST
            ->UNNEST
        ;

        return ['EXISTS (?)' => $select];
    }
}
