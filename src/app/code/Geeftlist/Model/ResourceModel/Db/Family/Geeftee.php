<?php

namespace Geeftlist\Model\ResourceModel\Db\Family;


use OliveOil\Core\Model\ResourceModel\Db\AbstractRelationHelper;

/**
 * @template T1 of \Geeftlist\Model\Family
 * @template T2 of \Geeftlist\Model\Geeftee
 * @template T1Collection of \Geeftlist\Model\ResourceModel\Db\Family\Collection
 * @template T2Collection of \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
 */
class Geeftee extends AbstractRelationHelper
{
    public const MAIN_TABLE = 'family_geeftee';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct(
            $context,
            self::MAIN_TABLE,
            \Geeftlist\Model\Family::ENTITY_TYPE,
            \Geeftlist\Model\Geeftee::ENTITY_TYPE
        );
    }

    public function isInFamily(\Geeftlist\Model\Geeftee $geeftee, \Geeftlist\Model\Family $family): bool {
        return $this->isLinkedTo($family, $geeftee);
    }

    public function hasGeeftee(\Geeftlist\Model\Family $family, \Geeftlist\Model\Geeftee $geeftee): bool {
        return $this->isLinkedTo($family, $geeftee);
    }

    public function getGeefteeIds(\Geeftlist\Model\Family $family): array {
        return $this->getLinkedIds($family);
    }

    public function getGeefteeCollection(\Geeftlist\Model\Family $family) {
        return $this->getLinkedCollection($family);
    }
}
