<?php

namespace Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation;

use Geeftlist\Model\ResourceModel\Db\Family;
use Geeftlist\Model\ResourceModel\Db\Geefter;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class SponsorRelationHelper extends AbstractRelationHelper
{
    public function join_sponsor(Family\MembershipRequest\Collection $collection): static {
        $collection->joinTable(
            ['sponsor' => Geefter::MAIN_TABLE],
            'main_table.sponsor_id = sponsor.geefter_id',
            Select::SQL_STAR,
            Select::JOIN_LEFT,
            true
        );

        return $this;
    }
}
