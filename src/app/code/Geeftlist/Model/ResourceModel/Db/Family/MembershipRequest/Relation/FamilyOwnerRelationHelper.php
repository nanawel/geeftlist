<?php

namespace Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest\Relation;

use Geeftlist\Model\ResourceModel\Db\Family;
use Geeftlist\Model\ResourceModel\Db\Geefter;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class FamilyOwnerRelationHelper extends AbstractRelationHelper
{
    public function join_family_owner(Family\MembershipRequest\Collection $collection): static {
        $collection
            ->addJoinRelation('family')
            ->joinTable(
                ['family_owner' => Geefter::MAIN_TABLE],
                'family.owner_id = family_owner.geefter_id',
                Select::SQL_STAR,
                Select::JOIN_INNER,
                true
            )
        ;

        return $this;
    }
}
