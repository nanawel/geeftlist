<?php

namespace Geeftlist\Model\ResourceModel\Db\Family\Relation;

use Geeftlist\Model\ResourceModel\Db\Family;
use Geeftlist\Model\ResourceModel\Db\Geefter;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class OwnerRelationHelper extends AbstractRelationHelper
{
    public function join_owner(Family\Collection $collection): static {
        $collection
            ->joinTable(
                ['owner' => Geefter::MAIN_TABLE],
                'main_table.owner_id = owner.geefter_id',
                Select::SQL_STAR,
                Select::JOIN_INNER,
                true
            )
        ;

        return $this;
    }
}
