<?php

namespace Geeftlist\Model\ResourceModel\Db\Family;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Family;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\Operator;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Family>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Family::ENTITY_TYPE,
            'family_id',
            Family::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }

    /**
     * @inheritDoc
     */
    public function addMembersCount(): static {
        $subSelect = $this->connection->select(['family_geeftee' => Geeftee::MAIN_TABLE]);
        $subSelect->columns(['cnt' => new Expression('COUNT(family_geeftee.geeftee_id)')])
            ->where->equalTo(
                'main_table.family_id',
                'family_geeftee.family_id',
                Operator::TYPE_IDENTIFIER,
                Operator::TYPE_IDENTIFIER
            );
        $this->addFieldToSelect(['members_count' => $subSelect]);

        return $this;
    }
}
