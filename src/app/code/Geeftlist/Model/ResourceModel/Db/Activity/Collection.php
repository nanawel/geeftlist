<?php

namespace Geeftlist\Model\ResourceModel\Db\Activity;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Activity;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Activity>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Activity::ENTITY_TYPE,
            'activity_id',
            Activity::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
