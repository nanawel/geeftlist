<?php

namespace Geeftlist\Model\ResourceModel\Db;

use Geeftlist\Model\ResourceModel\Db\Setup\Constants;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Sql\Expression;

class Flag extends AbstractModel
{
    public const MAIN_TABLE = 'flag';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'name',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * Backward compatibility between v0.47 and v0.48
     */
    public function useOldConfigTable(): bool
    {
        try {
            $select = $this->connection->select(Constants::DEPRECATED_TABLE_CONFIG);
            $this->connection->query($select, Adapter::QUERY_MODE_EXECUTE, false);

            return true;
        }
        catch (\PDOException $e) {
            if (str_contains($e->getMessage(), 'SQLSTATE[42S02]')) {
                return false;
            }
            throw $e;
        }
    }

    public function getValue(string $name): mixed {
        if ($this->useOldConfigTable()) {
            $select = $this->connection->select(Constants::DEPRECATED_TABLE_CONFIG)
                ->columns(['value'])
                ->limit(1);
            $select->where->equalTo('path', $name);
        }
        else {
            $select = $this->connection->select($this->getMainTable())
                ->columns(['value'])
                ->limit(1);
            $select->where->equalTo('name', $name);
        }

        return $this->connection->fetchFirstValue($this->connection->query($select));
    }

    public function setValue(string $name, mixed $value): static {
        if ($this->hasValue($name)) {
            if ($this->useOldConfigTable()) {
                $update = $this->connection->update(Constants::DEPRECATED_TABLE_CONFIG)
                    ->set(['value' => $value]);
                $update->where->equalTo('path', $name);
            }
            else {
                $update = $this->connection->update($this->getMainTable())
                    ->set(['value' => $value]);
                $update->where->equalTo('name', $name);
            }
            $this->connection->query($update);
        }
        else {
            if ($this->useOldConfigTable()) {
                $insert = $this->connection->insert(Constants::DEPRECATED_TABLE_CONFIG)
                    ->columns(['path', 'value'])
                    ->values([$name, $value]);
            }
            else {
                $insert = $this->connection->insert($this->getMainTable())
                    ->columns(['name', 'value'])
                    ->values([$name, $value]);
            }
            $this->connection->query($insert, null, false);
        }

        return $this;
    }

    public function hasValue(string $name): bool {
        if ($this->useOldConfigTable()) {
            $select = $this->connection->select(Constants::DEPRECATED_TABLE_CONFIG)
                ->columns(['cnt' => new Expression('COUNT(value)')]);
            $select->where->equalTo('path', $name);
        }
        else {
            $select = $this->connection->select($this->getMainTable())
                ->columns(['cnt' => new Expression('COUNT(value)')]);
            $select->where->equalTo('name', $name);
        }

        return $this->connection->fetchFirstValue($this->connection->query($select)) > 0;
    }

    public function deleteValue(string $name): static {
        if ($this->useOldConfigTable()) {
            $delete = $this->connection->delete(Constants::DEPRECATED_TABLE_CONFIG);
            $delete->where->equalTo('path', $name);
        }
        else {
            $delete = $this->connection->delete($this->getMainTable());
            $delete->where->equalTo('name', $name);
        }
        $this->connection->query($delete);

        return $this;
    }

    public function getAllValues(?string $prefix = null): array {
        $select = $this->connection->select($this->getMainTable());
        if ($prefix !== null) {
            $select->where->like('name', "$prefix%");
        }
        $select->order('name');

        return $this->connection->fetchCol(
            $this->connection->query($select),
            'value',
            'name'
        );
    }

    public function load(\OliveOil\Core\Model\AbstractModel $object, $id, $field = null): never
    {
        throw new \BadFunctionCallException('Not available');
    }

    public function save(\OliveOil\Core\Model\AbstractModel $object): never
    {
        throw new \BadFunctionCallException('Not available');
    }

    public function delete(\OliveOil\Core\Model\AbstractModel $object): never
    {
        throw new \BadFunctionCallException('Not available');
    }
}
