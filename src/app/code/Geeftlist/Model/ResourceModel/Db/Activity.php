<?php

namespace Geeftlist\Model\ResourceModel\Db;

class Activity extends AbstractModel
{
    public const MAIN_TABLE = 'activity';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'activity_id',
            self::MAIN_TABLE,
            $data
        );
    }
}
