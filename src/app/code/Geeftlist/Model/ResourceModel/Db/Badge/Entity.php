<?php

namespace Geeftlist\Model\ResourceModel\Db\Badge;


use Geeftlist\Exception\BadgeException;
use Geeftlist\Model\Badge;
use Laminas\Db\Sql\Expression;
use OliveOil\Core\Exception\NotImplementedException;

/**
 * Even if we're dealing with a many-to-many relation here, this class dos *not* extend
 * \OliveOil\Core\Model\ResourceModel\Db\AbstractRelationHelper as the relation table
 * is working in a special way (3 columns, because of the generic handling of badges for
 * entities).
 *
 * @implements \OliveOil\Core\Model\ResourceModel\Iface\RelationHelperInterface<
 *     \Geeftlist\Model\AbstractModel,
 *     \Geeftlist\Model\Badge,
 *     \OliveOil\Core\Model\ResourceModel\Iface\Collection<\Geeftlist\Model\AbstractModel>,
 *     \Geeftlist\Model\ResourceModel\Db\Badge\Collection
 * >
 */
class Entity implements \OliveOil\Core\Model\ResourceModel\Iface\RelationHelperInterface
{
    public const MAIN_TABLE = 'badge_entity';

    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    protected \OliveOil\Core\Model\ResourceModel\Db\Transaction $transaction;

    protected \OliveOil\Core\Model\FactoryInterface $resourceModelFactory;

    public function __construct(
        \OliveOil\Core\Model\ResourceModel\Db\Context $context
    ) {
        $this->connection = $context->getConnection();
        $this->transaction = $context->getTransaction();
        $this->resourceModelFactory = $context->getResourceModelFactory();
    }

    /**
     * @inheritDoc
     */
    public function addLinks(array $entities, array $badges): int {
        $entityIdsByType = [];
        foreach ($entities as $entity) {
            if (! $entity instanceof \Geeftlist\Model\AbstractModel) {
                throw new \InvalidArgumentException('$entities must be an array of AbstractModel');
            }

            $entityIdsByType[$entity->getEntityType()][] = [
                'entity_type' => $entity->getEntityType(),
                'entity_id' => $entity->getId()
            ];
        }

        $badgeIds = [];
        foreach ($badges as $badge) {
            if (! $badge instanceof Badge) {
                throw new \InvalidArgumentException('$badges must be an array of Geeftee');
            }

            $badgeIds[] = $badge->getId();
        }

        if ($entities === [] || $badges === []) {
            return 0;
        }

        $this->assertBadgesAreValidForEntities($entities, $badges);

        // Prepare new rows
        $newAssociationsByType = [];
        foreach ($badgeIds as $badgeId) {
            foreach ($entityIdsByType as $entityType => $entityIds) {
                foreach ($entityIds as $entityId) {
                    $newAssociationsByType[$entityType][] = $entityId + ['badge_id' => $badgeId];
                }
            }
        }

        $changesCount = 0;
        foreach ($newAssociationsByType as $entityType => $newAssociations) {
            // Collect existing association
            $select = $this->connection->select(self::MAIN_TABLE)
                ->columns(['entity_type', 'entity_id', 'badge_id']);
            $select->where
                ->equalTo('entity_type', $entityType)
                ->and->in('entity_id', array_column($newAssociations, 'entity_id'))
                ->and->in('badge_id', $badgeIds);
            $result = $this->connection->query($select);

            // Only insert non-existing associations
            $existingAssociations = $result->toArray();
            $newAssociations = \OliveOil\array_diff($newAssociations, $existingAssociations);

            if ($newAssociations) {
                $insert = $this->connection->insertMultiple(self::MAIN_TABLE)
                    ->columns(['entity_type', 'entity_id', 'badge_id'])
                    ->values($newAssociations);
                $changesCount += $this->connection->query($insert)->count();
            }
        }

        return $changesCount;
    }

    /**
     * @inheritDoc
     */
    public function removeLinks(array $entities, array $badges, bool $negate = false): int {
        \OliveOil\assertObjectArrayOfType($entities, \Geeftlist\Model\AbstractModel::class);
        \OliveOil\assertObjectArrayOfType($badges, \Geeftlist\Model\Badge::class);

        $entityIdsByType = [];
        foreach ($entities as $e) {
            $entityIdsByType[$e->getEntityType()][] = $e->getId();
        }

        $badgeIds = array_map(static fn(Badge $item) => $item->getId(), $badges);

        if (!$negate && ($entityIdsByType === [] || $badgeIds === [])) {
            return 0;
        }

        $this->assertBadgesAreValidForEntities($entities, $badges);

        $changesCount = 0;
        foreach ($entityIdsByType as $entityType => $entityIds) {
            $delete = $this->connection->delete(self::MAIN_TABLE)
                ->from(self::MAIN_TABLE);
            $delete->where
                ->equalTo('entity_type', $entityType)
                ->and->in('entity_id', $entityIds);
            if ($badgeIds !== []) {
                if ($negate) {
                    $delete->where->notIn('badge_id', $badgeIds);
                }
                else {
                    $delete->where->in('badge_id', $badgeIds);
                }
            }

            $changesCount += $this->connection->query($delete)->count();
        }

        return $changesCount;
    }

    /**
     * @inheritDoc
     */
    public function replaceLinks(object $entity, array $badges, array $replaceOnlyBadges = null): int {
        return $this->transaction->execute(function () use ($entity, $badges, $replaceOnlyBadges): int {
            $changesCount = 0;
            $currentBadges = $replaceOnlyBadges ?? $this->getLinkedCollection($entity)->getItems();

            $badgesToRemove = array_udiff($currentBadges, $badges, static fn($b1, $b2): int => $b1->getId() <=> $b2->getId());
            $this->removeLinks([$entity], $badgesToRemove);
            $changesCount += count($badgesToRemove);

            $badgesToAdd = array_udiff($badges, $currentBadges, static fn($b1, $b2): int => $b1->getId() <=> $b2->getId());
            $this->addLinks([$entity], $badgesToAdd);

            return $changesCount + count($badgesToAdd);
        });
    }

    /**
     * @inheritDoc
     */
    public function getBadgeIds(\Geeftlist\Model\AbstractModel $entity): array {
        return $this->getLinkedIds($entity);
    }

    /**
     * @inheritDoc
     */
    public function getBadgeCollection(
        \Geeftlist\Model\AbstractModel $entity
    ): \Geeftlist\Model\ResourceModel\Db\Badge\Collection {
        return $this->getLinkedCollection($entity);
    }

    /**
     * @param \Geeftlist\Model\AbstractModel[] $entities
     * @param Badge[] $badges
     * @throws BadgeException
     */
    protected function assertBadgesAreValidForEntities(array $entities, array $badges) {
        if ($badges === []) {
            // An empty set is always valid
            return;
        }

        $entityTypes = [];
        foreach ($entities as $entity) {
            $entityTypes[$entity->getEntityType()] = true;
        }

        $badgeIds = [];
        foreach ($badges as $badge) {
            $badgeIds[] = $badge->getId();
        }

        sort($badgeIds);

        $select = $this->connection->select(\Geeftlist\Model\ResourceModel\Db\Badge::BADGE_ENTITY_TYPE_TABLE)
            ->columns(['badge_id'])
            ->order('badge_id');
        $select->where->in('entity_type', array_keys($entityTypes))
            ->and->in('badge_id', $badgeIds);

        $availableBadgeIds = $this->connection->fetchCol(
            $this->connection->query($select),
            'badge_id'
        );

        if ($badgeIds != $availableBadgeIds) {
            throw new BadgeException('Invalid badges for the specified entities.');
        }
    }

    /**
     * @inheritDoc
     */
    public function isLinkedTo($a, $b): bool {
        if (!$a->getId() || !$a->getId()) {
            return false;
        }

        if ($a instanceof Badge) {
            $tmp = $b;
            $b = $a;
            $a = $tmp;
        }

        if (!$b instanceof Badge) {
            throw new \InvalidArgumentException('Must provide an entity and a Badge instance.');
        }

        $select = $this->connection->select(self::MAIN_TABLE)
            ->columns(['e' => new Expression('1')]);
        $select->where->equalTo('badge_id', $b->getId())
            ->and->equalTo('entity_type', $a->getEntityType())
            ->and->equalTo('entity_id', $a->getId());
        $select->limit(1);
        $result = $this->connection->query($select);

        return (bool) $result->current();
    }

    /**
     * @inheritDoc
     */
    public function getLinkedCollection($object): \OliveOil\Core\Model\ResourceModel\Iface\Collection {
        if ($object instanceof Badge) {
            throw new NotImplementedException('$object cannot be a Badge.');
        }

        /** @var \Geeftlist\Model\ResourceModel\Db\Badge\Collection $collection */
        $collection = $this->resourceModelFactory->resolveMakeCollection(Badge::ENTITY_TYPE)
            ->addFieldToFilter('entity', [$object->getEntityType(), $object->getId()]);

        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function getLinkedIds($object): array {
        if ($object instanceof Badge) {
            throw new NotImplementedException('$object cannot be a Badge.');
        }

        $select = $this->connection->select(self::MAIN_TABLE);
        $select->columns(['badge_id'])
            ->where->equalTo('entity_id', $object->getEntityType())
            ->and->equalTo('entity_id', $object->getId());

        return $this->connection->fetchCol($this->connection->query($select), 'badge_id');
    }
}
