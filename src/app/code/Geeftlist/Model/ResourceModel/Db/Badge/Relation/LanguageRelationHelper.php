<?php

namespace Geeftlist\Model\ResourceModel\Db\Badge\Relation;

use Geeftlist\Model\ResourceModel\Db\Badge;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class LanguageRelationHelper extends AbstractRelationHelper
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        protected \OliveOil\Core\Model\I18n $i18n
    ) {
        parent::__construct($context);
    }

    public function join_language(Badge\Collection $collection, string $language): void {
        if (!is_string($language)) {
            throw new \InvalidArgumentException('Not a valid language code: ' . $language);
        }

        if ($language !== '' && $language !== '0') {
            $langCodes = $this->i18n->deriveLanguage($language, null, true);

            $tableAliases = [];
            foreach ($langCodes as $langCode) {
                $tableAlias = $tableAliases[] = sprintf('badge_translation_%s', preg_replace('/[^a-z0-9]/i', '_', (string) $langCode));
                $collection->joinTable(
                    [$tableAlias => Badge::BADGE_T9N_TABLE],
                    new Expression(
                        sprintf('main_table.badge_id = %s.badge_id AND %s.language = ?', $tableAlias, $tableAlias),
                        $langCode
                    ),
                    [],
                    Select::JOIN_LEFT
                );
            }

            $tableAliases = array_reverse($tableAliases);
            foreach (Badge::TRANSLATABLE_FIELDS as $translatableField) {
                $sql = '';
                foreach ($tableAliases as $tableAlias) {
                    $sql = $sql !== '' && $sql !== '0'
                        ? sprintf('IFNULL(%s.%s, %s)', $tableAlias, $translatableField, $sql)
                        : sprintf('%s.%s', $tableAlias, $translatableField);
                }

                $collection->addFieldToSelect([$translatableField => new Expression($sql)]);
            }
        }
    }
}
