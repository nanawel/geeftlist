<?php

namespace Geeftlist\Model\ResourceModel\Db\Badge\Relation;

use Geeftlist\Model\ResourceModel\Db\Badge;
use Geeftlist\Model\ResourceModel\Db\Badge\Entity;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class EntityRelationHelper extends AbstractRelationHelper
{
    /*
     * ENTITY
     */

    protected function filterConditionToPredicate_entity_eq(array $args): array {
        return $this->filterConditionToPredicate_entity($args, true);
    }

    protected function filterConditionToPredicate_entity_ne(array $args): array {
        return $this->filterConditionToPredicate_entity($args, false);
    }

    private function filterConditionToPredicate_entity(array $args, bool $equals): array {
        [$entityType, $entityId] = $args;
        $entityId = AbstractCollection::toId($entityId);

        $select = $this->connection->select(['badge_entity_filter' => Entity::MAIN_TABLE]);
        $select->where->equalTo(
            'main_table.badge_id',
            'badge_entity_filter.badge_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($equals) {
            $select->where->equalTo('badge_entity_filter.entity_type', $entityType)
                ->and->equalTo('badge_entity_filter.entity_id', $entityId);
        }
        else {
            $select->where->notEqualTo('badge_entity_filter.entity_type', $entityType)
                ->or->notEqualTo('badge_entity_filter.entity_id', $entityId);
        }

        return $equals ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }

    /*
     * ENTITY TYPE
     */

    protected function filterConditionToPredicate_entity_types_in(array $entityTypes): array {
        return $this->filterConditionToPredicate_entity_types($entityTypes, true);
    }

    protected function filterConditionToPredicate_entity_types_nin(array $entityTypes): array {
        return $this->filterConditionToPredicate_entity_types($entityTypes, false);
    }

    protected function filterConditionToPredicate_entity_type_eq(string $entityType): array {
        return $this->filterConditionToPredicate_entity_types([$entityType], true);
    }

    protected function filterConditionToPredicate_entity_type_ne(string $entityType): array {
        return $this->filterConditionToPredicate_entity_types([$entityType], false);
    }

    private function filterConditionToPredicate_entity_types(array $entityTypes, bool $in): array {
        $select = $this->connection->select(['badge_entity_type_filter' => Badge::BADGE_ENTITY_TYPE_TABLE]);
        $select->where->equalTo(
            'main_table.badge_id',
            'badge_entity_type_filter.badge_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($entityTypes !== []) {
            $select->where->in('badge_entity_type_filter.entity_type', $entityTypes);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }
}
