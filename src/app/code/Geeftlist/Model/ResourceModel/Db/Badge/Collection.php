<?php

namespace Geeftlist\Model\ResourceModel\Db\Badge;


use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Badge;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Badge>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Badge::ENTITY_TYPE,
            'badge_id',
            Badge::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
