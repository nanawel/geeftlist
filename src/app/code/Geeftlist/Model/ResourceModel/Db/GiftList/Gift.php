<?php

namespace Geeftlist\Model\ResourceModel\Db\GiftList;

use OliveOil\Core\Model\ResourceModel\Db\AbstractRelationHelper;

/**
 * @template T1 of \Geeftlist\Model\GiftList
 * @template T2 of \Geeftlist\Model\Gift
 * @template T1Collection of \Geeftlist\Model\ResourceModel\Db\GiftList\Collection
 * @template T2Collection of \Geeftlist\Model\ResourceModel\Db\Gift\Collection
 *
 * @extends \OliveOil\Core\Model\ResourceModel\Db\AbstractRelationHelper<
 *     \Geeftlist\Model\GiftList,
 *     \Geeftlist\Model\Gift,
 *     \Geeftlist\Model\ResourceModel\Db\GiftList\Collection,
 *     \Geeftlist\Model\ResourceModel\Db\Gift\Collection
 * >
 */
class Gift extends AbstractRelationHelper
{
    public const MAIN_TABLE = 'giftlist_gift';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct(
            $context,
            self::MAIN_TABLE,
            \Geeftlist\Model\GiftList::ENTITY_TYPE,
            \Geeftlist\Model\Gift::ENTITY_TYPE
        );
    }

    /**
     * @inheritDoc
     */
    public function getGiftListIds(\Geeftlist\Model\Gift $gift): array {
        return $this->getLinkedIds($gift);
    }

    /**
     * @inheritDoc
     */
    public function getGiftCollection(
        \Geeftlist\Model\GiftList $giftList
    ): \Geeftlist\Model\ResourceModel\Db\Gift\Collection {
        return $this->getLinkedCollection($giftList);
    }

    /**
     * @inheritDoc
     */
    public function getGiftListCollection(
        \Geeftlist\Model\Gift $gift
    ): \Geeftlist\Model\ResourceModel\Db\GiftList\Collection {
        return $this->getLinkedCollection($gift);
    }
}
