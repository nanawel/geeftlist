<?php

namespace Geeftlist\Model\ResourceModel\Db\GiftList\Relation;

use Geeftlist\Model\ResourceModel\Db\Geefter;
use Geeftlist\Model\ResourceModel\Db\GiftList;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class OwnerRelationHelper extends AbstractRelationHelper
{
    /**
     * @inheritDoc
     */
    public function join_owner(GiftList\Collection $collection): static {
        $collection->joinTable(
            ['owner' => Geefter::MAIN_TABLE],
            'main_table.owner_id = owner.geefter_id',
            Select::SQL_STAR,
            Select::JOIN_INNER,
            true
        );

        return $this;
    }
}
