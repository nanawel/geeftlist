<?php
namespace Geeftlist\Model\ResourceModel\Db\GiftList;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\GiftList;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\GiftList>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\GiftList::ENTITY_TYPE,
            'giftlist_id',
            GiftList::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
