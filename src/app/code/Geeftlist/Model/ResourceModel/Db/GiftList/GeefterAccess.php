<?php

namespace Geeftlist\Model\ResourceModel\Db\GiftList;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\GiftList;
use Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface;
use Geeftlist\Model\ResourceModel\Iface\GiftList\RestrictionsAppenderInterface;
use Geeftlist\Model\Share\GiftList\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Exception\NotImplementedException;

class GeefterAccess implements GeefterAccessInterface, RestrictionsAppenderInterface
{
    protected \OliveOil\Core\Model\ResourceModel\Db\Connection $connection;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context
    ) {
        $this->connection = $context->getConnection();
    }

    /**
     * @inheritDoc
     * @throws \Throwable
     */
    public function isAllowed(Geefter $geefter, array $entities, array $actions): bool {
        $entityIds = [];
        foreach ($entities as $e) {
            if (! $e instanceof \Geeftlist\Model\GiftList) {
                throw new \InvalidArgumentException('Not a gift list.');
            }

            // Exclude entities with no ID = always allow actions on *new* entities
            if ($e->getId()) {
                $entityIds[] = $e->getId();
            }
        }

        if ($actions === [] || $entityIds === []) {
            return true;
        }

        sort($entityIds);
        sort($actions);

        $allowedActionsByGiftListId = [];

        // Use $this->addGeefterAccessRestrictionsToSelect() for each required $action, and aggregate results
        // for later comparison
        foreach ($actions as $action) {
            $select = $this->connection->select(['main_table' => GiftList::MAIN_TABLE]);
            $select->where->in('main_table.giftlist_id', $entityIds);
            $this->addGeefterAccessRestrictionsToSelect($select, $geefter, 'main_table', 'giftlist_id', $action);
            $result = $this->connection->query($select);

            foreach ($this->connection->fetchCol($result, 'giftlist_id') as $giftListId) {
                $allowedActionsByGiftListId[$giftListId][] = $action;
            }
        }

        foreach ($entityIds as $giftListId) {
            // Missing all permissions for list
            if (!isset($allowedActionsByGiftListId[$giftListId])) {
                return false;
            }

            sort($allowedActionsByGiftListId[$giftListId]);
            // Missing at least one required permission for a list
            if ($allowedActionsByGiftListId[$giftListId] != $actions) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getAllowed(array $entities, array $actions): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * Exclude unavailable lists for the specified geefter.
     *
     * @throws \Exception
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $this->addGeefterAccessRestrictionsToSelect(
            $collection->getSelect(),
            $geefter,
            'main_table',
            'giftlist_id',
            $actionType
        );

        return $this;
    }

    /**
     * NOTICE: There's currently only the PRIVATE status implemented (= must be the owner to do any action
     *         on the list)
     *
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToSelect(
        Select $select,
        Geefter $geefter,
        string $giftListTableAlias,
        $giftListIdColumnAlias = 'giftlist_id',
        string $actionType = TypeInterface::ACTION_VIEW
    ): void {
        $orPredicates = [];
        $orPredicates[] = match ($actionType) {
            TypeInterface::ACTION_VIEW, TypeInterface::ACTION_EDIT, TypeInterface::ACTION_ARCHIVE, TypeInterface::ACTION_DELETE, TypeInterface::ACTION_ADD_REMOVE_GIFT => new Expression($giftListTableAlias . '.owner_id = ?', $geefter->getId()),
            default => throw new InvalidValueException('Invalid action type: ' . $actionType),
        };

        $select->where->nest()
            ->addPredicates($orPredicates, PredicateSet::COMBINED_BY_OR)
            ->unnest();
    }

    /**
     * Append allowed actions to the specified gift list collection.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): never {
        throw new NotImplementedException(__METHOD__);
    }
}
