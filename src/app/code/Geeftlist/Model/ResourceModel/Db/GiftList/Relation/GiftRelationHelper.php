<?php

namespace Geeftlist\Model\ResourceModel\Db\GiftList\Relation;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GiftRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_gifts_in($giftIds): array {
        return $this->filterConditionToPredicate_gifts($giftIds, true);
    }

    protected function filterConditionToPredicate_gifts_nin($giftIds): array {
        return $this->filterConditionToPredicate_gifts($giftIds, false);
    }

    protected function filterConditionToPredicate_gift_eq($giftId): array {
        return $this->filterConditionToPredicate_gifts([$giftId], true);
    }

    protected function filterConditionToPredicate_gift_ne($giftId): array {
        return $this->filterConditionToPredicate_gifts([$giftId], false);
    }

    private function filterConditionToPredicate_gifts($giftIds, bool $in): array {
        $giftIds = AbstractCollection::toIdArray($giftIds);

        $select = $this->connection->select(
            ['giftlist_gift_filter' => \Geeftlist\Model\ResourceModel\Db\GiftList\Gift::MAIN_TABLE]
        );
        $select->where->equalTo(
            'giftlist_gift_filter.giftlist_id',
            'main_table.giftlist_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($giftIds !== []) {
            $select->where->in('giftlist_gift_filter.gift_id', $giftIds);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }
}
