<?php

namespace Geeftlist\Model\ResourceModel\Db\Geefter\Relation;

use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GeefteeRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_geeftees_in(array $geefteeIds): array {
        return $this->filterConditionToPredicate_geeftees($geefteeIds, true);
    }

    protected function filterConditionToPredicate_geeftees_nin(array $geefteeIds): array {
        return $this->filterConditionToPredicate_geeftees($geefteeIds, false);
    }

    protected function filterConditionToPredicate_geeftee_eq(int $geefteeId): array {
        return $this->filterConditionToPredicate_geeftees([$geefteeId], true);
    }

    protected function filterConditionToPredicate_geeftee_ne(int $geefteeId): array {
        return $this->filterConditionToPredicate_geeftees([$geefteeId], false);
    }

    /**
     * @param AbstractModel[]|int[] $geefteeIds
     * @return array
     */
    private function filterConditionToPredicate_geeftees(array $geefteeIds, bool $in): array {
        $geefteeIds = AbstractCollection::toIdArray($geefteeIds);

        $select = $this->connection->select(['geeftee_filter' => Geeftee::MAIN_TABLE]);
        $select->where->equalTo(
            'geeftee_filter.geefter_id',
            'main_table.geefter_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );
        if ($geefteeIds !== []) {
            $select->where->in('geeftee_filter.geeftee_id', $geefteeIds);
        }
        else {
            $select->where(new Literal($in ? 'FALSE' : 'TRUE'));
        }

        return $in ? ['EXISTS (?)' => $select] : ['NOT EXISTS (?)' => $select];
    }

    protected function filterConditionToPredicate_geeftee_null(bool $null): array {
        return $this->filterConditionToPredicate_geeftee($null);
    }

    /**
     * @param bool $null
     * @return array
     */
    protected function filterConditionToPredicate_geeftee(bool $null): array {
        $select = $this->connection->select(['geeftee_null_filter' => Geeftee::MAIN_TABLE]);
        $select->where->equalTo(
            'geeftee_null_filter.geefter_id',
            'main_table.geefter_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        );

        return $null ? ['NOT EXISTS (?)' => $select] : ['EXISTS (?)' => $select];
    }
}
