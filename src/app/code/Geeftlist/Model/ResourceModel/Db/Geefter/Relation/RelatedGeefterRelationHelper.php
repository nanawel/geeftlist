<?php

namespace Geeftlist\Model\ResourceModel\Db\Geefter\Relation;

use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee as FamilyGeeftee;
use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Laminas\Db\Sql\Predicate\Predicate;
use OliveOil\Core\Model\ResourceModel\Db\AbstractCollection;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class RelatedGeefterRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_related_geefter_eq(Geefter|int $geefterId): array {
        return $this->filterConditionToPredicate_related_geefters($geefterId, true);
    }

    protected function filterConditionToPredicate_related_geefter_ne(Geefter|int $geefterId): array {
        return $this->filterConditionToPredicate_related_geefters($geefterId, false);
    }

    public function filterConditionToPredicate_related_geefters(Geefter|int $geefterId, bool $related): array {
        $geefterId = AbstractCollection::toId($geefterId);

        // SET @relatedGeefterId = 104;
        // SELECT * FROM geefter AS main_table
        // WHERE EXISTS(
        //    -- $commonFamilyRelationSelect:
        //    SELECT * FROM family_geeftee AS common_family_relation,
        //                  geeftee AS geeftee
        //    WHERE geeftee.geefter_id = main_table.geefter_id
        //        AND common_family_relation.geeftee_id = geeftee.geeftee_id
        //        AND EXISTS(
        //            -- $relatedGeefterFamilyRelationSelect:
        //            SELECT * FROM family_geeftee related_geefter_family_relation
        //            JOIN geeftee related_geeftee ON related_geeftee.geeftee_id = related_geefter_family_relation.geeftee_id
        //            WHERE related_geefter_family_relation.family_id = common_family_relation.family_id
        //            AND related_geeftee.geefter_id = @relatedGeefterId
        //    )
        // )

        $relatedGeefterFamilyRelationSelect = $this->connection->select(
            ['related_geefter_family_relation' => FamilyGeeftee::MAIN_TABLE]
        );
        $relatedGeefterFamilyRelationSelect->join(
            ['related_geeftee' => Geeftee::MAIN_TABLE],
            'related_geeftee.geeftee_id = related_geefter_family_relation.geeftee_id'
        );
        $relatedGeefterFamilyRelationSelect->where->equalTo(
            'related_geefter_family_relation.family_id',
            'common_family_relation.family_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        )
            ->and->equalTo('related_geeftee.geefter_id', $geefterId);

        $commonFamilyRelationSelect = $this->connection->select(
            ['common_family_relation' => FamilyGeeftee::MAIN_TABLE]
        );
        $commonFamilyRelationSelect->join(
            ['geeftee' => Geeftee::MAIN_TABLE],
            // Cannot reference main_table in the ON part, so using a full join followed by a WHERE condition
            new \Laminas\Db\Sql\Predicate\Expression('TRUE')
        );
        $commonFamilyRelationSelect->where
            // Special case: a geefter is always related to him/herself
            ->equalTo(
                'geeftee.geefter_id',
                $geefterId,
                Predicate::TYPE_IDENTIFIER,
                Predicate::TYPE_VALUE
            )
            ->or->nest()
                ->equalTo(
                    'geeftee.geefter_id',
                    'main_table.geefter_id',
                    Predicate::TYPE_IDENTIFIER,
                    Predicate::TYPE_IDENTIFIER
                )
                ->and->equalTo(
                    'common_family_relation.geeftee_id',
                    'geeftee.geeftee_id',
                    Predicate::TYPE_IDENTIFIER,
                    Predicate::TYPE_IDENTIFIER
                )
                ->and->addPredicates(['EXISTS (?)' => $relatedGeefterFamilyRelationSelect])
            ->unnest();

        return $related
            ? ['EXISTS (?)' => $commonFamilyRelationSelect]
            : ['NOT EXISTS (?)' => $commonFamilyRelationSelect];
    }
}
