<?php
namespace Geeftlist\Model\ResourceModel\Db\Geefter;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Geefter;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Geefter>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Geefter::ENTITY_TYPE,
            'geefter_id',
            Geefter::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
