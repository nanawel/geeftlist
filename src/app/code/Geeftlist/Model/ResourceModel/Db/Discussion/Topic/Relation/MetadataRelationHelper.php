<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Relation;

use Geeftlist\Model\ResourceModel\Db\Discussion\Topic;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class MetadataRelationHelper extends AbstractRelationHelper
{
    public function join_metadata(Topic\Collection $collection, $joinConditions): \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection {
        return $collection->joinTable(
            ['discussion_topic_metadata' => Topic::METADATA_TABLE],
            'main_table.topic_id = discussion_topic_metadata.topic_id',
            [],
            Select::JOIN_LEFT
        );
    }

    protected function filterConditionToPredicate_metadata_eq(array $args): array {
        [$key, $value] = $args;

        $select = $this->connection->select(Topic::METADATA_TABLE);
        $select->where->equalTo(
            'main_table.topic_id',
            'discussion_topic_metadata.topic_id',
            Predicate::TYPE_IDENTIFIER,
            Predicate::TYPE_IDENTIFIER
        )
            ->and->equalTo('discussion_topic_metadata.key', $key)
            ->and->equalTo('discussion_topic_metadata.value', $value)
        ;

        return ['EXISTS (?)' => $select];
    }
}
