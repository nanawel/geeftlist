<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Topic;


use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\Indexer\AbstractGeefterAccess;
use Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer;
use Geeftlist\Model\ResourceModel\Iface\RestrictionsAppenderInterface;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface;

class GeefterAccess extends AbstractGeefterAccess implements RestrictionsAppenderInterface
{
    public const MAIN_TABLE = GeefterAccessIndexer::MAIN_TABLE;

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            ['entity_type', 'entity_id', 'geefter_id', 'action_type'],
            self::MAIN_TABLE,
            Topic::ENTITY_TYPE,
            $data
        );
    }

    /**
     * Exclude unavailable gifts for the specified geefter.
     *
     * @inheritDoc
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $this->addGeefterAccessRestrictionsToEntityCollection(
            $collection,
            $geefter,
            'main_table',
            'topic_id',
            $actionType
        );

        return $this;
    }

    /**
     * Append allowed actions to the specified gift collection.
     *
     * @inheritDoc
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): static {
        $this->appendGeefterAccessPermissionsToEntityCollection($collection, $geefter, 'main_table', 'topic_id');

        return $this;
    }
}
