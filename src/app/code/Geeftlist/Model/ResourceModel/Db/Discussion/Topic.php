<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion;


use Geeftlist\Model\Discussion\Post as PostModel;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Geeftlist\Model\ResourceModel\Db\Discussion;

class Topic extends AbstractModel
{
    public const MAIN_TABLE         = 'discussion_topic';

    public const METADATA_TABLE     = 'discussion_topic_metadata';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'topic_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @return Discussion\Post\Collection
     */
    public function getPostCollection(\Geeftlist\Model\Discussion\Topic $object) {
        /** @var Discussion\Post\Collection $postCollection */
        $postCollection = $this->getResourceModelFactory()->resolveMakeCollection(PostModel::ENTITY_TYPE)
            ->addFieldToFilter('main_table.topic_id', $object->getId());

        return $postCollection;
    }

    public function getModelMetadata(\Geeftlist\Model\Discussion\Topic $object): array {
        if (!$object->getId()) {
            return [];
        }

        $select = $this->connection->select(self::METADATA_TABLE);
        $select->columns(['key', 'value'])
            ->where->equalTo('topic_id', $object->getId());
        $result = $this->connection->query($select);

        return array_column($result->toArray(), 'value', 'key');
    }

    /**
     * @param \Geeftlist\Model\Discussion\Topic $object
     */
    protected function afterSave(\OliveOil\Core\Model\AbstractModel $object): static {
        parent::afterSave($object);
        $this->saveMetadata($object);

        return $this;
    }

    protected function saveMetadata(\Geeftlist\Model\Discussion\Topic $object): static {
        $metadata = $object->getMetadata();

        // 1. Delete
        $delete = $this->connection->delete(self::METADATA_TABLE);
        $delete->where->equalTo('topic_id', $object->getId());
        if ($metadataKeys = array_keys($metadata)) {
            $delete->where->notIn('key', $metadataKeys);
        }

        $this->connection->query($delete);

        // 2. Update
        $rowsToBeInserted = [];
        foreach ($metadata as $key => $value) {
            $update = $this->connection->update(self::METADATA_TABLE);
            $update->set(['value' => $value])
                ->where->equalTo('topic_id', $object->getId())
                ->and->equalTo('key', $key);
            /** @var \Laminas\Db\Adapter\Driver\ResultInterface $result */
            $result = $this->connection->query($update);

            if (! $result->getAffectedRows()) {
                $rowsToBeInserted[] = [
                    'key'   => $key,
                    'value' => $value
                ];
            }
        }

        // 3. Insert
        if ($rowsToBeInserted !== []) {
            $insert = $this->connection->insert(self::METADATA_TABLE);
            foreach ($rowsToBeInserted as $rowToBeInserted) {
                $rowToBeInserted['topic_id'] = $object->getId();
                $insert->values($rowToBeInserted);
                $this->connection->query($insert);
            }
        }

        return $this;
    }
}
