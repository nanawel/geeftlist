<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Post;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Discussion\Post;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Discussion\Post>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Discussion\Post::ENTITY_TYPE,
            'post_id',
            Post::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }

    public function getPinnedItems(): static {
        $clone = clone $this;
        $clone->addFieldToFilter('main_table.is_pinned', 1);

        return $clone;
    }
}
