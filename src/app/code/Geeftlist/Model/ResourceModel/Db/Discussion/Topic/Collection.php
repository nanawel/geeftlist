<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Topic;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Discussion\Topic;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Discussion\Topic>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Discussion\Topic::ENTITY_TYPE,
            'topic_id',
            Topic::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
