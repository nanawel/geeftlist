<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Post;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Geeftlist\Model\ResourceModel\Db\Discussion\Post;
use Geeftlist\Model\ResourceModel\Db\Discussion\Topic;
use Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer as TopicGeefterAccessResource;
use Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface;
use Geeftlist\Model\ResourceModel\Iface\RestrictionsAppenderInterface;
use Geeftlist\Model\Share\Discussion\Post\RuleType\TypeInterface;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\TypeInterface as TopicRuleType;
use Geeftlist\Model\Share\RuleTypeInterface;
use Laminas\Db\Sql\Combine;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Predicate\PredicateInterface;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Exception\InvalidValueException;
use OliveOil\Core\Exception\NotImplementedException;

class GeefterAccess extends AbstractModel implements GeefterAccessInterface, RestrictionsAppenderInterface
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            null,
            null,
            $data
        );
    }

    /**
     * @inheritDoc
     * @throws \Throwable
     */
    public function isAllowed(Geefter $geefter, array $entities, array $actions): bool {
        $entityIds = [];
        foreach ($entities as $e) {
            if (! $e instanceof \Geeftlist\Model\Discussion\Post) {
                throw new \InvalidArgumentException('Not a post.');
            }

            // Exclude entities with no ID = always allow actions on *new* entities
            if ($e->getId()) {
                $entityIds[] = $e->getId();
            }
        }

        if ($actions === [] || $entityIds === []) {
            return true;
        }

        sort($entityIds);
        sort($actions);

        $allowedActionsByEntityId = [];

        // Use $this->addGeefterAccessRestrictionsToSelect() for each required $action, and aggregate results
        // for later comparison
        foreach ($actions as $action) {
            $actionSelect = $this->connection->select(['main_table' => Post::MAIN_TABLE])
                ->columns(['post_id']);
            $actionSelect->where->in('post_id', $entityIds);
            $this->addGeefterAccessRestrictionsToSelect(
                $actionSelect,
                $geefter,
                'main_table',
                'post_id',
                null,
                $action
            );
            $result = $this->connection->query($actionSelect);

            foreach ($this->connection->fetchCol($result, 'post_id') as $entityId) {
                $allowedActionsByEntityId[$entityId][] = $action;
            }
        }

        foreach ($entityIds as $entityId) {
            // Missing all permissions for post
            if (!isset($allowedActionsByEntityId[$entityId])) {
                return false;
            }

            sort($allowedActionsByEntityId[$entityId]);
            // Missing at least one required permission for a post
            if ($allowedActionsByEntityId[$entityId] != $actions) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getAllowed(array $entities, array $actions): never {
        throw new NotImplementedException(__METHOD__);
    }

    /**
     * @inheritDoc
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $this->addGeefterAccessRestrictionsToCollection(
            $collection,
            $geefter,
            'main_table',
            'post_id',
            null,
            $actionType
        );

        return $this;
    }

    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection $collection
     * @param string $postIdColumnAlias
     * @param PredicateInterface $additionalPredicate
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        string $postTableAlias,
        $postIdColumnAlias = 'post_id',
        $entityTypeColumnAlias = null,
        string $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): static {
        $this->addGeefterAccessRestrictionsToSelect(
            $collection->getSelect(),
            $geefter,
            $postTableAlias,
            $postIdColumnAlias,
            $entityTypeColumnAlias,
            $actionType,
            $additionalPredicate
        );

        return $this;
    }

    /**
     * Exclude unavailable posts for the specified {$geefter + $actionType}.
     * => We're partially using the discussion_topic index to infer posts permissions.
     *
     * @param string $postIdColumnAlias
     * @param PredicateInterface|null $additionalPredicate
     * @throws \Exception
     */
    public function addGeefterAccessRestrictionsToSelect(
        Select $select,
        Geefter $geefter,
        string $postTableAlias,
        $postIdColumnAlias = 'post_id',
        $entityTypeColumnAlias = null,
        string $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): void {
        if ($entityTypeColumnAlias !== null) {
            throw new NotImplementedException('$entityTypeColumnAlias is not supported.');
        }

        if ($additionalPredicate !== null) {
            throw new NotImplementedException('$additionalPredicate is not supported.');
        }

        $orPredicates = [];
        switch ($actionType) {
            case TypeInterface::ACTION_VIEW:
                // View permission on a post == view permission of the parent topic
                $topicViewIdxSelect = $this->connection->select([
                    'geefter_access_topic_view'
                    => \Geeftlist\Model\ResourceModel\Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE
                ]);
                $topicViewIdxSelect->where
                    ->equalTo(
                        'geefter_access_topic_view.entity_id',
                        $postTableAlias . '.topic_id',
                        Predicate::TYPE_IDENTIFIER,
                        Predicate::TYPE_IDENTIFIER
                    )
                    ->and->equalTo('geefter_access_topic_view.action_type', TopicRuleType::ACTION_VIEW)
                    ->and->equalTo('geefter_access_topic_view.is_allowed', RuleTypeInterface::ALLOWED)
                    ->and->equalTo('geefter_access_topic_view.geefter_id', $geefter->getId());
                $orPredicates[] = new Expression('EXISTS (?)', $topicViewIdxSelect);
                break;

            case TypeInterface::ACTION_EDIT:
            case TypeInterface::ACTION_DELETE:
                $orPredicates[] = new Expression($postTableAlias . '.author_id = ?', $geefter->getId());
                break;

            case TypeInterface::ACTION_PIN:
                // "Pin" permission on a post == author of parent topic
                $topicPinIdxSelect = $this->connection->select([
                    'discussion_topic'
                    => \Geeftlist\Model\ResourceModel\Db\Discussion\Topic::MAIN_TABLE
                ]);
                $topicPinIdxSelect->where
                    ->equalTo(
                        'discussion_topic.topic_id',
                        $postTableAlias . '.topic_id',
                        Predicate::TYPE_IDENTIFIER,
                        Predicate::TYPE_IDENTIFIER
                    )
                    ->and->equalTo('discussion_topic.author_id', $geefter->getId());
                $orPredicates[] = new Expression('EXISTS (?)', $topicPinIdxSelect);
                break;

            default:
                throw new InvalidValueException('Invalid action type: ' . $actionType);
        }

        $select->where->nest()
            ->addPredicates($orPredicates, PredicateSet::COMBINED_BY_OR)
            ->unnest();
    }

    /**
     * Append allowed actions to the specified post collection.
     * => We're partially using the discussion_topic index to infer posts permissions.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): static {
        $allowedActionsUnionAlias = 'geefter_access_actions_union';
        $allowedActionsUnionPostIdAlias = $allowedActionsUnionAlias . '_post_id';

        $viewSelect = $this->connection->select(['discussion_post' => Post::MAIN_TABLE]);
        $viewSelect->columns([
                $allowedActionsUnionPostIdAlias => 'post_id',
                'allowed_actions' => new \Laminas\Db\Sql\Expression(sprintf('"%s"', TypeInterface::ACTION_VIEW))
            ])
            ->join(
                ['discussion_topic_geefter_access' => TopicGeefterAccessResource::MAIN_TABLE],
                'discussion_topic_geefter_access.entity_id = discussion_post.topic_id',
                []
            )
            ->where->equalTo('discussion_topic_geefter_access.action_type', TopicRuleType::ACTION_VIEW)
            ->and->equalTo('discussion_topic_geefter_access.is_allowed', RuleTypeInterface::ALLOWED)
            ->and->equalTo('discussion_topic_geefter_access.geefter_id', $geefter->getId())
        ;

        $editDeleteSelect = $this->connection->select(['discussion_post' => Post::MAIN_TABLE]);
        $editDeleteSelect->columns([
                $allowedActionsUnionPostIdAlias => 'post_id',
                'allowed_actions' => new \Laminas\Db\Sql\Expression(sprintf(
                    '"%s"',
                    implode(',', [TypeInterface::ACTION_EDIT, TypeInterface::ACTION_DELETE])
                ))
            ])
            ->where->equalTo('discussion_post.author_id', $geefter->getId())
        ;

        $pinSelect = $this->connection->select(['discussion_post' => Post::MAIN_TABLE]);
        $pinSelect->columns([
                $allowedActionsUnionPostIdAlias => 'post_id',
                'allowed_actions' => new \Laminas\Db\Sql\Expression(sprintf('"%s"', TypeInterface::ACTION_PIN))
            ])
            ->join(
                ['discussion_topic' => Topic::MAIN_TABLE],
                'discussion_topic.topic_id = discussion_post.topic_id',
                []
            )
            ->where->equalTo('discussion_topic.author_id', $geefter->getId())
        ;

        /** @var Select $select */
        $select = $collection->getSelect();
        $select->join(
            [
                    $allowedActionsUnionAlias => new \Laminas\Db\Sql\Expression(sprintf(
                        '(%s)',
                        (new Combine())->union([
                            $viewSelect,
                            $editDeleteSelect,
                            $pinSelect
                        ])->getSqlString($this->connection->getAdapter()->getPlatform())
                    ))
                ],
            sprintf('%s.%s = main_table.post_id', $allowedActionsUnionAlias, $allowedActionsUnionPostIdAlias),
            [
                    'allowed_actions' => new \Laminas\Db\Sql\Expression(
                        "GROUP_CONCAT(allowed_actions ORDER BY allowed_actions ASC)"
                    )
                ],
            Select::JOIN_LEFT
        )
            ->group('main_table.post_id')
        ;

        // Postprocess calculated allowed_actions field on collection items
        $collection->attachInstanceListener('load::after', static function () use ($collection, $geefter): void {
            /** @var \Geeftlist\Model\AbstractModel $entity */
            foreach ($collection as $entity) {
                $entity->setData(
                    'allowed_actions_geefter' . $geefter->getId(),
                    explode(',', (string) $entity->getAllowedActions())
                );
            }
        });

        return $this;
    }
}
