<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Post\Relation;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Discussion\Post;
use Geeftlist\Model\ResourceModel\Db\Discussion\Topic;
use Laminas\Db\Sql\Predicate\In;
use Laminas\Db\Sql\Predicate\Literal;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class TopicRelationHelper extends AbstractRelationHelper
{
    public function join_topic(Post\Collection $collection, $joinConditions): \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection {
        return $collection->joinTable(
            ['topic' => Topic::MAIN_TABLE],
            'main_table.topic_id = topic.topic_id',
            Select::SQL_STAR,
            $joinConditions['type'] ?? Select::JOIN_INNER,
            true
        );
    }

    protected function filterConditionToPredicate_topic_eq($topic): array {
        return ['main_table.topic_id = ?' => AbstractCollection::toId($topic)];
    }

    protected function filterConditionToPredicate_topics_in(array $topics): \Laminas\Db\Sql\Predicate\In|\Laminas\Db\Sql\Predicate\Literal {
        if ($topicIds = AbstractCollection::toIdArray($topics)) {
            return new In('main_table.topic_id', $topicIds);
        }

        return new Literal('FALSE');
    }
}
