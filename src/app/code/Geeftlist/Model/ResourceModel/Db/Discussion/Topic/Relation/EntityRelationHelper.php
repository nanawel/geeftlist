<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Topic\Relation;

use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class EntityRelationHelper extends AbstractRelationHelper
{
    protected function filterConditionToPredicate_entity_eq(array $args): array {
        [$entityType, $entityId] = $args;

        return [
            'main_table.linked_entity_type = ?' => $entityType,
            'main_table.linked_entity_id = ?' => $entityId
        ];
    }
}
