<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion;


use Geeftlist\Model\ResourceModel\Db\AbstractModel;

class Post extends AbstractModel
{
    public const MAIN_TABLE = 'discussion_post';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'post_id',
            self::MAIN_TABLE,
            $data
        );
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post $object
     */
    protected function beforeSave(\OliveOil\Core\Model\AbstractModel $object): static {
        $object->setIsPinned($object->getIsPinned() ? 1 : 0);

        return parent::beforeSave($object);
    }

    /**
     * @param \Geeftlist\Model\Discussion\Post $object
     */
    protected function afterLoad(\OliveOil\Core\Model\AbstractModel $object): static {
        $object->setIsPinned((bool) $object->getIsPinned());

        return parent::afterLoad($object);
    }
}
