<?php

namespace Geeftlist\Model\ResourceModel\Db\Discussion\Post\Relation;

use Geeftlist\Model\ResourceModel\Db\Discussion\Post;
use Geeftlist\Model\ResourceModel\Db\Geefter;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class AuthorRelationHelper extends AbstractRelationHelper
{
    public function join_author(Post\Collection $collection, $joinConditions): \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection {
        return $collection->joinTable(
            ['author' => Geefter::MAIN_TABLE],
            'main_table.author_id = author.geefter_id',
            Select::SQL_STAR,
            $joinConditions['type'] ?? Select::JOIN_INNER,
            true
        );
    }
}
