<?php
namespace Geeftlist\Model\ResourceModel\Db\Reservation;

use Geeftlist\Model\ResourceModel\Db\AbstractCollection;
use Geeftlist\Model\ResourceModel\Db\Reservation;

/**
 * @extends \Geeftlist\Model\ResourceModel\Db\AbstractCollection<\Geeftlist\Model\Reservation>
 */
class Collection extends AbstractCollection
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $relationHelpers = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            \Geeftlist\Model\Reservation::ENTITY_TYPE,
            'reservation_id',
            Reservation::MAIN_TABLE,
            $relationHelpers,
            $data
        );
    }
}
