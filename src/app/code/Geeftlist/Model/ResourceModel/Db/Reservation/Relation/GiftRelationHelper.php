<?php

namespace Geeftlist\Model\ResourceModel\Db\Reservation\Relation;

use Geeftlist\Model\ResourceModel\Db\Gift;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GiftRelationHelper extends AbstractRelationHelper
{
    public function join_gift(Reservation\Collection $collection): static {
        $collection->joinTable(
            ['gift' => Gift::MAIN_TABLE],
            'main_table.gift_id = gift.gift_id',
            Select::SQL_STAR,
            Select::JOIN_INNER,
            true
        );

        return $this;
    }
}
