<?php

namespace Geeftlist\Model\ResourceModel\Db\Reservation\Relation;

use Geeftlist\Model\ResourceModel\Db\Geefter;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GeefterRelationHelper extends AbstractRelationHelper
{
    public function join_geefter(Reservation\Collection $collection): static {
        $collection->joinTable(
            ['geefter' => Geefter::MAIN_TABLE],
            'main_table.geefter_id = geefter.geefter_id',
            Select::SQL_STAR,
            Select::JOIN_INNER,
            true
        );

        return $this;
    }
}
