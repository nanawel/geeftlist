<?php

namespace Geeftlist\Model\ResourceModel\Db\Reservation;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\ResourceModel\Db\AbstractModel;
use Geeftlist\Model\ResourceModel\Iface\Reservation\RestrictionsAppenderInterface;
use Geeftlist\Model\Share\Reservation\RuleType\TypeInterface;
use Geeftlist\Model\Share\RuleTypeInterface;
use OliveOil\Core\Exception\MissingImplementationException;

class GeefterAccess extends AbstractModel implements RestrictionsAppenderInterface
{
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            null,
            null,
            $data
        );
    }

    public const RESERVATION_TO_GIFT_ACTIONS_MAPPING = [
        TypeInterface::ACTION_VIEW   => \Geeftlist\Model\Share\Gift\RuleType\TypeInterface::ACTION_VIEW_RESERVATIONS,
        TypeInterface::ACTION_EDIT   => \Geeftlist\Model\Share\Gift\RuleType\TypeInterface::ACTION_RESERVE_PURCHASE,
        TypeInterface::ACTION_DELETE => \Geeftlist\Model\Share\Gift\RuleType\TypeInterface::ACTION_CANCEL_RESERVATION,
    ];

    /**
     * Exclude reservations matching unavailable gifts for the specified geeftee.
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType = TypeInterface::ACTION_VIEW
    ): static {
        $giftActionType = $this->convertReservationActionToGiftAction($actionType);
        if (! $giftActionType) {
            throw new \InvalidArgumentException('Invalid action type: ' . $actionType);
        }

        /** @var \Geeftlist\Model\ResourceModel\Db\Reservation\Collection $collection  */
        $collection->joinTable(
            ['geefter_access_idx' => \Geeftlist\Model\ResourceModel\Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE],
            'main_table.gift_id = geefter_access_idx.entity_id',
            []
        );
        $collection->addFieldToFilter('geefter_access_idx.geefter_id', $geefter->getId())
            ->addFieldToFilter('geefter_access_idx.entity_type', \Geeftlist\Model\Gift::ENTITY_TYPE)
            ->addFieldToFilter('geefter_access_idx.action_type', $giftActionType)
            ->addFieldToFilter('geefter_access_idx.is_allowed', RuleTypeInterface::ALLOWED);

        return $this;
    }

    /**
     * Append allowed actions to the specified reservation collection.
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): never {
        throw new MissingImplementationException(__METHOD__);
    }

    protected function convertReservationActionToGiftAction($actionType): ?string {
        return self::RESERVATION_TO_GIFT_ACTIONS_MAPPING[$actionType] ?? null;
    }
}
