<?php

namespace Geeftlist\Model\ResourceModel\Db\Reservation\Relation;

use Geeftlist\Model\ResourceModel\Db\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Reservation;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use OliveOil\Core\Model\ResourceModel\Db\Collection\AbstractRelationHelper;

// phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
class GeefteeRelationHelper extends AbstractRelationHelper
{
    public function join_geeftee(Reservation\Collection $collection, ?array $joinConditions): static {
        $collection->joinTable(
            ['gift_geeftee_gift' => Geeftee\Gift::MAIN_TABLE],
            'main_table.gift_id = gift_geeftee_gift.gift_id',
            [],
            $joinConditions['type'] ?? Select::JOIN_INNER,
        )
            ->joinTable(
                ['gift_geeftee' => Geeftee::MAIN_TABLE],
                'gift_geeftee_gift.geeftee_id = gift_geeftee.geeftee_id',
                ['geeftee_ids' => new Expression('GROUP_CONCAT(gift_geeftee.geeftee_id)')],
                $joinConditions['type'] ?? Select::JOIN_INNER,
            )
            ->getSelect()->group('main_table.gift_id')
        ;

        return $this;
    }
}
