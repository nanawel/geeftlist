<?php

namespace Geeftlist\Model\ResourceModel\Db;


use OliveOil\Core\Helper\DateTime;

class Geefter extends AbstractModel
{
    public const MAIN_TABLE = 'geefter';

    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\Context $context,
        array $data = []
    ) {
        parent::__construct(
            $context,
            'geefter_id',
            self::MAIN_TABLE,
            $data
        );
    }

    public function updateLastLoginAt(\Geeftlist\Model\Geefter $object, ?\DateTime $lastLoginAt = null) {
        $update = $this->connection->update($this->getMainTable())
            ->set(['last_login_at' => DateTime::getDateSql($lastLoginAt)]);
        $update->where->equalTo('geefter_id', $object->getId());
        $this->connection->query($update);
    }

    public function getGeefterIdFromEmail($email) {
        $select = $this->connection->select($this->getMainTable())
            ->columns([$this->idField]);
        $select->where->equalTo('email', (string) $email);
        $result = $this->connection
            ->query($select, \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        if ($result->count()) {
            return $result->toArray()[0][$this->idField];
        }

        return null;
    }

    /**
     * @return bool TRUE if unique, FALSE otherwise
     */
    public function assertEmailUnique(\Geeftlist\Model\Geefter $object): bool {
        $select = $this->connection->select($this->getMainTable())
            ->columns([$this->idField]);
        $where = $select->where;
        $nest = $where->nest();
        $nest->equalTo('email', (string) $object->getEmail());

        // Also check unicity with "pending_email" if present
        if ($pendingEmail = $object->getPendingEmail()) {
            $nest->OR->equalTo('email', $pendingEmail);
        }

        $nest->unnest();

        if ($id = $object->getId()) {
            $select->where->notEqualTo('geefter_id', (int) $object->getId());
        }

        $result = $this->connection
            ->query($select, \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return !$result->count();
    }

    /**
     * @return bool TRUE if unique, FALSE otherwise
     */
    public function assertUsernameUnique(\Geeftlist\Model\Geefter $object): bool {
        $select = $this->connection->select($this->getMainTable())
            ->columns([$this->idField]);
        $select->where->equalTo('username', (string) $object->getUsername());
        if ($id = $object->getId()) {
            $select->where->notEqualTo('geefter_id', (int) $object->getId());
        }

        $result = $this->connection
            ->query($select, \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return !$result->count();
    }
}
