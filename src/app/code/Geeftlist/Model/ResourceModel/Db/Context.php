<?php

namespace Geeftlist\Model\ResourceModel\Db;


class Context extends \OliveOil\Core\Model\ResourceModel\Db\Context
{
    /**
     * Context constructor.
     *
     * Overriden here to force model & resource factory types with DI
     */
    public function __construct(
        \Geeftlist\Model\ResourceModel\Db\DefaultConnection $connection,
        \OliveOil\Core\Model\ResourceModel\Db\Transaction $transaction,
        \Geeftlist\Model\ModelFactory $modelFactory,
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Log $logService,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory
    ) {
        parent::__construct(
            $connection,
            $transaction,
            $modelFactory,
            $resourceModelFactory,
            $eventService,
            $logService
        );
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }
}
