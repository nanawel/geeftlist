<?php

namespace Geeftlist\Model\Notification\Field;

use OliveOil\Core\Model\Entity\AbstractField;
use OliveOil\Core\Model\Validation\ErrorInterface;

class EventParams extends AbstractField
{
    public const ENTITY_TYPE = 'notification/field/eventParams';

    /**
     * @param mixed $value
     * @param array|null $params
     * @return ErrorInterface|null
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null) {
        if (! is_array($value)) {
            return $this->getNewErrorInstance(
                ErrorInterface::TYPE_FIELD_INVALID,
                'Invalid value'
            );
        }

        return null;
    }

    /**
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null)
    {
        return json_encode($value);
    }

    /**
     * @param object $object
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function serialize($object, $value, $params = null)
    {
        return json_encode($value, JSON_OBJECT_AS_ARRAY);
    }

    /**
     * @param object $object
     * @param string $value
     * @param array|null $params
     * @return mixed
     */
    public function unserialize($object, $value, $params = null)
    {
        return $value === null
            ? null
            : json_decode($value, true);
    }
}
