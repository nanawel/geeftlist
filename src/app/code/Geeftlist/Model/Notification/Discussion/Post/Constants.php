<?php

namespace Geeftlist\Model\Notification\Discussion\Post;


interface Constants
{
    public const EVENT_NEW = 'new';

    public const EVENT_NEW_RECIPIENTS = 'new_recipients';
}
