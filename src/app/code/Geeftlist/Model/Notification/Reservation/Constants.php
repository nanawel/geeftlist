<?php

namespace Geeftlist\Model\Notification\Reservation;


interface Constants
{
    public const EVENT_NEW    = 'new';

    public const EVENT_UPDATE = 'update';

    public const EVENT_DELETE = 'delete';
}
