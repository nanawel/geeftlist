<?php

namespace Geeftlist\Model\Notification\Geefter;


interface Constants
{
    public const EVENT_NEW = 'new';

    public const EVENT_DELETE = 'delete';
}
