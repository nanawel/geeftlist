<?php

namespace Geeftlist\Model\Notification\Family;


interface Constants
{
    public const EVENT_JOIN  = 'join';

    public const EVENT_LEAVE = 'leave';
}
