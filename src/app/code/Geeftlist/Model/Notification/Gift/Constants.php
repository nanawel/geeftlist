<?php

namespace Geeftlist\Model\Notification\Gift;


interface Constants
{
    public const EVENT_CHANGE          = 'change';

    public const EVENT_ADD_TO_GEEFTEES = 'add_to_geeftees';

    public const EVENT_ADD_TO_ME       = 'add_to_me';
}
