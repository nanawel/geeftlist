<?php

namespace Geeftlist\Model;

use Geeftlist\Model\ResourceModel\Db\Geeftee\Collection as GeefteeCollection;
use Geeftlist\Model\ResourceModel\Db\Reservation\Collection as ReservationCollection;
use Geeftlist\Service\Gift\Image;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Gift getResource()
 * @method string getLabel()
 * @method $this setLabel(string $label)
 * @method string getDescription()
 * @method $this setDescription(string $description)
 * @method int getCreatorId()
 * @method $this setCreatorId(int $creatorId)
 * @method float getEstimatedPrice()
 * @method $this setEstimatedPrice(float $estimatedPrice)
 * @method string getStatus()
 * @method $this setStatus(string $status)
 * @method int getPriority()
 * @method $this setPriority(int $priority)
 * @method string getImageId()
 * @method $this setImageId(string $imageId)
 * @method string getCreatedAt()
 */
class Gift extends AbstractModel
{
    public const ENTITY_TYPE = 'gift';

    protected ?Geefter $creator = null;

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        array $data = []
    ) {
        $this->urlHelper = $context->getUrlHelper();
        parent::__construct($context, $data);
        $this->fieldModels['status'] = 'gift/field/status';
        $this->fieldModels['priority'] = 'gift/field/priority';
        $this->fieldModels['image_id'] = 'gift/field/imageId';
    }

    public function getCreator(): ?Geefter {
        if (!$this->creator && $this->getCreatorId()) {
            /** @var Geefter|null $creator */
            $creator = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
                ->get($this->getCreatorId());
            $this->creator = $creator;
        }

        return $this->creator;
    }

    public function getCreatorUsername(): ?string {
        if (
            (! $creatorUsername = parent::getCreatorUsername())
            && ($creator = $this->getCreator())
        ) {
            $this->setCreatorUsername($creatorUsername = $creator->getUsername());
        }

        return $creatorUsername;
    }

    public function getReservationCollection(): ReservationCollection {
        return $this->getResource()->getReservations($this);
    }

    /**
     * @return int[]
     */
    public function getGeefteeIds(): array {
        return $this->getGeefteeGiftHelper()->getGeefteeIds($this);
    }

    public function getGeefteeCollection(): GeefteeCollection {
        return $this->getGeefteeGiftHelper()->getGeefteeCollection($this);
    }

    public function getGeefteeSummary(): array {
        $geeftees = $this->getGeefteeCollection();

        $summary = [];
        foreach ($geeftees as $geeftee) {
            $summary[] = [
                'id' => $geeftee->getId(),
                'name' => $geeftee->getName(),
                'model' => $geeftee
            ];
        }

        return $summary;
    }

    public function getViewUrl(array $params = []): string {
        return $this->urlHelper->getGiftViewUrl($this, $params);
    }

    public function getEditUrl(array $params = []): string {
        return $this->urlHelper->getGiftEditUrl($this, $params);
    }

    public function getArchiveUrl(array $params = []): string {
        return $this->urlHelper->getGiftArchiveUrl($this, $params);
    }

    public function getDuplicateArchiveUrl(array $params = []): string {
        return $this->urlHelper->getGiftDuplicateArchiveUrl($this, $params);
    }

    public function getDuplicateUrl(array $params = []): string {
        return $this->urlHelper->getGiftDuplicateUrl($this, $params);
    }

    public function getConfirmArchivingRequestUrl(array $params = []): string {
        return $this->urlHelper->getConfirmArchivingRequestUrl($this, $params);
    }

    public function getArchivingRequestUrl(array $params = []): string {
        return $this->urlHelper->getArchivingRequestUrl($this, $params);
    }

    public function getReportUrl(array $params = []): string {
        return $this->urlHelper->getGiftReportUrl($this, $params);
    }

    public function getDeleteUrl(array $params = []): string {
        return $this->urlHelper->getGiftDeleteUrl($this, $params);
    }

    public function getReserveUrl(array $params = []): string {
        return $this->urlHelper->getGiftReserveUrl($this, $params);
    }

    public function getImageUrl($type = Image::IMAGE_TYPE_FULL, $params = []) {
        return $this->urlHelper->getGiftImageUrl($this, $type, $params);
    }

    public function markAsDeleted(): static {
        $this->setStatus(Gift\Field\Status::DELETED);

        return $this;
    }

    public function getGeefteeGiftHelper(): \Geeftlist\Model\Geeftee\Gift {
        return $this->geefteeGiftHelper;
    }
}
