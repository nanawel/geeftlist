<?php

namespace Geeftlist\Model\ActivityLog\Field;

use OliveOil\Core\Model\Entity\AbstractField;

class Payload extends AbstractField
{
    public const ENTITY_TYPE = 'activityLog/field/payload';

    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null) {
        return null;
    }

    /**
     * @param object $object
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function serialize($object, $value, $params = null)
    {
        return json_encode($value, JSON_OBJECT_AS_ARRAY);
    }

    /**
     * @param object $object
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function unserialize($object, $value, $params = null)
    {
        return json_decode((string) $value, true);
    }
}
