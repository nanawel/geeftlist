<?php

namespace Geeftlist\Model;


use Geeftlist\Model\Share\Entity\Rule;
use OliveOil\Core\Exception\MissingImplementationException;
use OliveOil\Core\Iface\IdentityInterface;

/**
 * @method Rule[]|null getShareRules()
 */
class AbstractModel extends \OliveOil\Core\Model\AbstractModel implements IdentityInterface
{
    public const ENTITY_TYPE = '<not-set>';

    protected \Geeftlist\Model\ModelFactory $modelFactory;

    protected \Geeftlist\Model\ResourceFactory $resourceFactory;

    protected \Geeftlist\Helper\Url $urlHelper;

    protected \Geeftlist\Model\RepositoryFactory $repositoryFactory;

    public function __construct(
        \Geeftlist\Model\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->modelFactory = $context->getModelFactory();
        $this->resourceFactory = $context->getResourceModelFactory();
        $this->urlHelper = $context->getUrlHelper();
        $this->repositoryFactory = $context->getRepositoryFactory();
        if (!$this->eventObjectParam) {
            $this->eventObjectParam = str_replace('/', '_', $this->getEntityType());
        }
    }

    /**
     * @inheritDoc
     */
    public function getResource() {
        return $this->resourceFactory->resolveGet($this->getEntityType());
    }

    /**
     * @deprecated Use Repository instead
     */
    public function getCollection(array $params = []) {
        return $this->resourceFactory->resolveMakeCollection($this->getEntityType(), $params);
    }

    public function getEntityType() {
        if (! defined('static::ENTITY_TYPE')) {
            throw new MissingImplementationException('Missing ENTITY_TYPE constant on class ' . static::class . ' .');
        }

        return static::ENTITY_TYPE;
    }

    /**
     * @inheritDoc
     */
    public function getIdentities(): array {
        return self::getEntityIdentities($this);
    }

    /**
     * @param string|AbstractModel $entityType
     * @param int|null $entityId
     * @return string[]
     */
    public static function getEntityIdentities($entityType, $entityId = null): array {
        if ($entityType instanceof AbstractModel) {
            $entityId = $entityType->getId();
            $entityType = $entityType->getEntityType();
        }

        $tags = [];
        if ($entityId) {
            // Slash is a reserved character for cache keys
            $tags[] = sprintf('%s-%d', str_replace('/', '__', strtoupper((string) $entityType)), $entityId);
        }

        return $tags;
    }
}
