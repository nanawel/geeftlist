<?php

namespace Geeftlist\Model\MailNotification;


interface Constants
{
    public const SENDING_FREQ_REALTIME = 'realtime';

    public const SENDING_FREQ_DAILY    = 'daily';

    public const SENDING_FREQ_WEEKLY   = 'weekly';

    public const SENDING_FREQ_DISABLED = 'disabled';

    public const SENDING_FREQ = [
        self::SENDING_FREQ_REALTIME,
        self::SENDING_FREQ_DAILY,
        self::SENDING_FREQ_WEEKLY,
        self::SENDING_FREQ_DISABLED,
    ];
}
