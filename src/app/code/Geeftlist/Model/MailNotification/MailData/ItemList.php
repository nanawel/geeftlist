<?php

namespace Geeftlist\Model\MailNotification\MailData;


class ItemList
{
    /** @var string */
    protected $targetType;

    /** @var Item[] */
    protected $items = [];

    /**
     * @return string
     */
    public function getTargetType() {
        return $this->targetType;
    }

    /**
     * @param string $targetType
     * @return $this
     */
    public function setTargetType($targetType): static {
        $this->targetType = $targetType;

        return $this;
    }

    /**
     * @return Item[]
     */
    public function getAllItems() {
        return $this->items;
    }

    /**
     * By default, prevent duplicate items in returned array by using items hashes.
     *
     * @group ticket-569
     * @return Item[]
     */
    public function getItems(): array {
        $items = [];
        foreach ($this->items as $item) {
            $items[$item->getHash()] = $item;
        }

        return array_values($items);
    }

    /**
     * @param Item[] $items
     * @return $this
     */
    public function setItems($items): static {
        $this->items = $items;

        return $this;
    }

    /**
     * @param Item|Item[] $items
     * @return $this
     */
    public function addItems($items): static {
        $this->setItems(array_merge(
            $this->items,
            is_array($items) ? $items : [$items]
        ));

        return $this;
    }

    /**
     * @return \Geeftlist\Model\Notification[]
     */
    public function getItemsNotifications(): array {
        $notifications = [];
        foreach ($this->items as $it) {
            $notifications[$it->getNotification()->getId()] = $it->getNotification();
        }

        return $notifications;
    }
}
