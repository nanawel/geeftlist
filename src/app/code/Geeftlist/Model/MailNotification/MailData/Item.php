<?php

namespace Geeftlist\Model\MailNotification\MailData;


use OliveOil\Core\Model\MagicObject;

/**
 * @method $this setEventName(string $eventName)
 * @method string getEventName()
 */
class Item extends MagicObject
{
    protected \Geeftlist\Model\Notification $notification;

    protected ?\Geeftlist\Model\Geefter $geefter = null;

    /**
     * @return \Geeftlist\Model\Notification
     */
    public function getNotification() {
        return $this->notification;
    }

    public function setNotification(\Geeftlist\Model\Notification $notification): static {
        $this->notification = $notification;

        return $this;
    }

    /**
     * @return \Geeftlist\Model\Geefter|null
     */
    public function getGeefter() {
        return $this->geefter;
    }

    public function setGeefter(\Geeftlist\Model\Geefter $geefter = null): static {
        $this->geefter = $geefter;

        return $this;
    }

    /**
     * Create a "unicity hash" for this item based on its data.
     * It's not really crucial to have a strong hash here, so CRC32 should do.
     */
    public function getHash(): string {
        return hash('crc32b', (implode('|', [
            $this->getGeefter() ? $this->getGeefter()->getId() : null,
            $this->getNotification()->getTargetType(),
            $this->getNotification()->getTargetId(),
            $this->getNotification()->getEventName(),
            serialize($this->getNotification()->getEventParams())
        ])));
    }
}
