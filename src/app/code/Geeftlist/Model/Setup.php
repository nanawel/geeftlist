<?php

namespace Geeftlist\Model;


use OliveOil\Core\Service\EventInterface;

class Setup extends \OliveOil\Core\Model\Setup
{
    public function getResource(): \OliveOil\Core\Model\ResourceModel\Iface\Setup {
        return $this->resourceModelFactory->get(\OliveOil\Core\Model\ResourceModel\Iface\Setup::class);
    }

    protected function doInstall(bool $dropFirst = false, ?string $targetVersion = null): bool {
        return $this->runWithoutListeners([\OliveOil\Core\Model\Setup::class, __FUNCTION__], func_get_args());
    }

    protected function doUpgrade(?string $targetVersion = null, bool $force = false) {
        return $this->runWithoutListeners([\OliveOil\Core\Model\Setup::class, __FUNCTION__], func_get_args());
    }

    protected function doUninstall(?string $targetVersion = null, bool $force = false) {
        return $this->runWithoutListeners([\OliveOil\Core\Model\Setup::class, __FUNCTION__], func_get_args());
    }

    protected function doInstallSampleData(?string $targetVersion = null) {
        return $this->runWithoutListeners([\OliveOil\Core\Model\Setup::class, __FUNCTION__], func_get_args());
    }

    protected function doUpgradeSampleData(?string $targetVersion = null) {
        return $this->runWithoutListeners([\OliveOil\Core\Model\Setup::class, __FUNCTION__], func_get_args());
    }

    protected function doUninstallSampleData(?string $targetVersion = null) {
        return $this->runWithoutListeners([\OliveOil\Core\Model\Setup::class, __FUNCTION__], func_get_args());
    }

    protected function runWithoutListeners(callable $callable, array $args = []) {
        $listenersStatus = $this->eventService->getAllListenersStatus();
        $this->eventService->disableListeners(EventInterface::EVENT_SPACE_WILDCARD);
        try {
            return call_user_func_array($callable, $args);
        }
        finally {
            $this->eventService->setListenersStatus($listenersStatus);
        }
    }
}
