<?php

namespace Geeftlist\Model\Share;


interface Constants
{
    public const ALLOWED   = 1;

    public const FORBIDDEN = 0;

    public const WILDCARD_GEEFTER_ID = '*';

    public const ACTION_VIEW        = 'view';

    public const ACTION_EDIT        = 'edit';

    public const ACTION_DELETE      = 'delete';

    public const ACTION_ARCHIVE     = 'archive';

    public const ACTION_BE_NOTIFIED = 'be_notified';
}
