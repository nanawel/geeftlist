<?php

namespace Geeftlist\Model\Share\Family\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW           = Constants::ACTION_VIEW;

    public const ACTION_EDIT           = Constants::ACTION_EDIT;

    public const ACTION_DELETE         = Constants::ACTION_DELETE;

    public const ACTION_ADD_GEEFTEE    = 'add_geeftee';

    public const ACTION_REMOVE_GEEFTEE = 'remove_geeftee';

    public const ACTION_TRANSFER       = 'transfer';
}
