<?php

namespace Geeftlist\Model\Share\Discussion\Topic\RuleType\System;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Constants;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\AbstractType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;

class GiftType extends AbstractType
{
    public const CODE = 'system/gift';

    public function __construct(
        \Geeftlist\Model\Share\Discussion\Topic\RuleType\Context $context,
        protected \Geeftlist\Service\PermissionInterface $permissionService
    ) {
        parent::__construct($context);
    }

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $this->getTopicRepository()->get($entityId);
        if (!$topic) {
            $this->getLogger()->warning('Invalid topic ID: ' . $entityId);

            return [];
        }

        if ($topic->getLinkedEntityType() != Gift::ENTITY_TYPE) {
            return [];
        }
        if (!$giftId = $topic->getLinkedEntityId()) {
            return [];
        }
        /** @var \Geeftlist\Model\Gift|null $gift */
        $gift = $this->getGiftRepository()->get($giftId);

        if (!$gift) {
            // Gift does not exist (anymore). Should not happen but might.
            $actionAuthorizations = [];
            foreach (self::ALL_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::FORBIDDEN;
            }

            // Forbidden to all
            return [Constants::WILDCARD_GEEFTER_ID => $actionAuthorizations];
        }

        $return = [];
        $topicVisibility = $topic->getMetadataByKey(\Geeftlist\Model\Gift\Constants::TOPIC_VISIBILITY_KEY);

        $giftGeefteeGeefterIds = [];
        foreach ($this->getGeefteeGiftHelper()->getGeeftees($gift) as $giftGeeftee) {
            $giftGeefteeGeefterIds[] = $giftGeeftee->getGeefterId();
        }

        // We re-use the gift/geefterAccess index data to infer topic's permissions
        $allowedGiftGeefters = $this->getPermissionService()->getAllowed($gift, TypeInterface::ACTION_VIEW);

        foreach ($allowedGiftGeefters as $allowedGiftGeefter) {
            $actionAuthorizations = [];
            foreach (self::RELATIVE_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::ALLOWED;
            }

            $return[$allowedGiftGeefter->getId()] = $actionAuthorizations;
        }

        // Handle visibility for gift's geeftees on protected topic
        if ($topicVisibility == \Geeftlist\Model\Gift\Constants::TOPIC_VISIBILITY_PROTECTED) {
            foreach ($giftGeefteeGeefterIds as $giftGeefteeGeefterId) {
                $actionAuthorizations = [];
                foreach (self::GIFT_GEEFTEE_PROTECTED_ACTIONS as $action) {
                    $actionAuthorizations[$action] = self::ALLOWED;
                }

                foreach (array_diff(self::ALL_ACTIONS, self::GIFT_GEEFTEE_PROTECTED_ACTIONS) as $action) {
                    $actionAuthorizations[$action] = self::FORBIDDEN;
                }

                $return[$giftGeefteeGeefterId] = $actionAuthorizations;
            }
        }

        return $return;
    }

    public function getPermissionService(): \Geeftlist\Service\PermissionInterface {
        return $this->permissionService;
    }
}
