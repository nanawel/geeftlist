<?php

namespace Geeftlist\Model\Share\Discussion\Topic\RuleType\System;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Constants;
use Geeftlist\Model\Share\Discussion\Topic\RuleType\AbstractType;

class DeletedGiftType extends AbstractType
{
    public const CODE = 'system/deletedGift';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $this->getTopicRepository()->get($entityId);
        if (!$topic) {
            $this->getLogger()->warning('Invalid topic ID: ' . $entityId);

            return [];
        }

        $return = [];
        if (
            $topic->getLinkedEntityType() == Gift::ENTITY_TYPE
            && ($giftId = $topic->getLinkedEntityId())
        ) {
            /** @var \Geeftlist\Model\Gift|null $gift */
            $gift = $this->getGiftRepository()->get($giftId);

            if (! $gift || $gift->getStatus() == Gift\Field\Status::DELETED) {
                $actionAuthorizations = [];
                foreach (self::ALL_ACTIONS as $action) {
                    $actionAuthorizations[$action] = self::FORBIDDEN;
                }

                $return[Constants::WILDCARD_GEEFTER_ID] = $actionAuthorizations;
            }
        }

        return $return;
    }
}
