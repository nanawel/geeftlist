<?php

namespace Geeftlist\Model\Share\Discussion\Topic\RuleType;


use Geeftlist\Model\Discussion\Topic;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Gift;

class Context extends \Geeftlist\Model\Share\Entity\RuleType\Context
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $topicRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $giftRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefteeRepository;

    public function __construct(
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->topicRepository = $repositoryFactory->resolveGet(Topic::ENTITY_TYPE);
        $this->giftRepository = $repositoryFactory->resolveGet(Gift::ENTITY_TYPE);
        $this->geefteeRepository = $repositoryFactory->resolveGet(Geeftee::ENTITY_TYPE);
        parent::__construct(
            $coreFactory,
            $repositoryFactory,
            $modelValidator,
            $logService
        );
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getTopicRepository() {
        return $this->topicRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGiftRepository() {
        return $this->giftRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefteeRepository() {
        return $this->geefteeRepository;
    }

    public function getGeefteeGiftHelper(): \Geeftlist\Model\Geeftee\Gift {
        return $this->geefteeGiftHelper;
    }
}
