<?php

namespace Geeftlist\Model\Share\Discussion\Topic\RuleType;


use Geeftlist\Model\Discussion\Topic;

abstract class AbstractType extends \Geeftlist\Model\Share\Entity\RuleType\AbstractType implements TypeInterface
{
    public const CODE = '_MISSING_TYPE_CODE_';

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $topicRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $giftRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefteeRepository;

    protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper;

    public function __construct(
        \Geeftlist\Model\Share\Discussion\Topic\RuleType\Context $context
    ) {
        $this->code = static::CODE;
        $this->supportedEntities = [Topic::ENTITY_TYPE];
        $this->topicRepository = $context->getTopicRepository();
        $this->giftRepository = $context->getGiftRepository();
        $this->geefteeRepository = $context->getGeefteeRepository();
        $this->geefteeGiftHelper = $context->getGeefteeGiftHelper();
        parent::__construct($context);
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getTopicRepository() {
        return $this->topicRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGiftRepository() {
        return $this->giftRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefteeRepository() {
        return $this->geefteeRepository;
    }

    /**
     * @return \Geeftlist\Model\Geeftee\Gift
     */
    public function getGeefteeGiftHelper() {
        return $this->geefteeGiftHelper;
    }
}
