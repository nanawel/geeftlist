<?php

namespace Geeftlist\Model\Share\Discussion\Topic\RuleType\System;


use Geeftlist\Model\Share\Discussion\Topic\RuleType\AbstractType;

class AuthorType extends AbstractType
{
    public const CODE = 'system/author';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Discussion\Topic $topic */
        $topic = $this->getTopicRepository()->get($entityId);
        if (!$topic) {
            $this->getLogger()->warning('Invalid topic ID: ' . $entityId);

            return [];
        }

        if ($authorId = $topic->getAuthorId()) {
            $actionAuthorizations = [];
            foreach (self::AUTHOR_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::ALLOWED;
            }

            return [$authorId => $actionAuthorizations];
        }

        return [];
    }
}
