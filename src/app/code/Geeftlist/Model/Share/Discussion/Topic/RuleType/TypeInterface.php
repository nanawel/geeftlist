<?php

namespace Geeftlist\Model\Share\Discussion\Topic\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW              = Constants::ACTION_VIEW;

    public const ACTION_BE_NOTIFIED       = Constants::ACTION_BE_NOTIFIED;

    public const ACTION_POST              = 'post';

    public const ACTION_MANAGE            = 'manage';

    public const WILDCARD_GEEFTER_ID = Constants::WILDCARD_GEEFTER_ID;

    public const ALL_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_POST,
        self::ACTION_BE_NOTIFIED,
        self::ACTION_MANAGE
    ];

    public const AUTHOR_ACTIONS = self::MANAGE_ACTIONS;

    public const MANAGE_ACTIONS = [
        self::ACTION_MANAGE
    ];

    public const RELATIVE_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_POST,
        self::ACTION_BE_NOTIFIED,
    ];

    public const GIFT_GEEFTEE_PUBLIC_ACTIONS = self::ALL_ACTIONS;

    public const GIFT_GEEFTEE_PROTECTED_ACTIONS = [];
}
