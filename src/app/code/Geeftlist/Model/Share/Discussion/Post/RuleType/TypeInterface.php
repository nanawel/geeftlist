<?php

namespace Geeftlist\Model\Share\Discussion\Post\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW   = Constants::ACTION_VIEW;

    public const ACTION_EDIT   = Constants::ACTION_EDIT;

    public const ACTION_DELETE = Constants::ACTION_DELETE;

    public const ACTION_PIN    = 'pin';

    public const WILDCARD_GEEFTER_ID = Constants::WILDCARD_GEEFTER_ID;

    public const ALL_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_EDIT,
        self::ACTION_DELETE,
        self::ACTION_PIN
    ];

    public const MANAGE_ACTIONS = [
        self::ACTION_EDIT,
        self::ACTION_DELETE,
    ];

    public const AUTHOR_ACTIONS = self::MANAGE_ACTIONS;

    public const TOPIC_AUTHOR_ACTIONS = [
        self::ACTION_PIN,
    ];
}
