<?php

namespace Geeftlist\Model\Share\Entity;

use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Share\Entity\Rule\Constants;
use function OliveOil\array_mask;

/**
 * Class Rule
 *
 *
 * @method string getTargetEntityType()
 * @method $this setTargetEntityType(string $entityType)
 * @method int getTargetEntityId()
 * @method $this setTargetEntityId(int $entityId)
 * @method string getRuleType()
 * @method $this setRuleType(string $ruleType)
 * @method array getParams()
 * @method $this setParams(array $params)
 * @method int getPriority()
 * @method $this setPriority(int $priority)
 */
class Rule extends AbstractModel
{
    public const ENTITY_TYPE = 'share/entity/rule';

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \OliveOil\Core\Service\GenericFactoryInterface $shareRuleTypeFactoryFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fieldModels['params'] = \Geeftlist\Model\Share\Entity\Rule\Field\Params::ENTITY_TYPE;
    }

    public function beforeSave(): static {
        parent::beforeSave();
        $this->filterParams();

        return $this;
    }

    protected function filterParams(): static {
        if (
            ($entityType = $this->getTargetEntityType())
            && ($ruleType = $this->getRuleType())
        ) {
            /** @var \OliveOil\Core\Service\GenericFactoryInterface $shareRuleTypeFactory */
            $shareRuleTypeFactory = $this->getShareRuleTypeFactoryFactory()->getFromCode($entityType);

            /** @var \Geeftlist\Model\Share\RuleTypeInterface $shareRuleType */
            $shareRuleType = $shareRuleTypeFactory->resolveGet($ruleType);
            $this->setParams($shareRuleType->filterParams($this->getParams() ?: []));
        }

        return $this;
    }

    protected function filterPriority(): static {
        $priority = $this->getPriority() ?? 0;
        // Ensure priority is between allowed MIN and MAX
        $this->setPriority(
            min(Constants::SHARE_ENTITY_RULE_MAX_PRIORITY, max(Constants::SHARE_ENTITY_RULE_MIN_PRIORITY, $priority))
        );

        return $this;
    }

    public function getTargetEntity() {
        if (
            !($entity = parent::getTargetEntity()) && (($entityType = $this->getTargetEntityType())
            && ($entityId = $this->getTargetEntityId()))
        ) {
            $entity = $this->modelFactory->resolveMake($entityType)
                ->load($entityId);
            if ($entity->getId()) {
                $this->setTargetEntity($entity);
            }
        }

        return parent::getTargetEntity();
    }

    public function toArray(): array {
        return array_mask($this->getData(), [
            'target_entity_type',
            'target_entity_id',
            'rule_type',
            'params',
            'priority'
        ]);
    }

    public function getShareRuleTypeFactoryFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->shareRuleTypeFactoryFactory;
    }
}
