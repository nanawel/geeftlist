<?php

namespace Geeftlist\Model\Share\Entity\Rule;


interface Constants
{
    public const SHARE_ENTITY_RULE_MODEL_DATA_KEY = 'share_entity_rule_configuration';

    public const SHARE_ENTITY_RULE_MIN_PRIORITY = 0;

    public const SHARE_ENTITY_RULE_MAX_PRIORITY = 999;
}
