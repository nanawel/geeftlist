<?php

namespace Geeftlist\Model\Share\Entity\Rule\Field;

use OliveOil\Core\Model\Entity\AbstractField;
use OliveOil\Core\Model\Validation\ErrorInterface;

class Params extends AbstractField
{
    public const ENTITY_TYPE = 'share/entity/rule/field/params';

    /**
     * @param mixed $value
     * @param array|null $params
     * @return ErrorInterface|null
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null) {
        if ($params !== null && ! is_array($value)) {
            return $this->getNewErrorInstance(
                ErrorInterface::TYPE_FIELD_INVALID,
                'Invalid value'
            );
        }

        return null;
    }

    /**
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null)
    {
        return json_encode($value);
    }

    /**
     * @param object $object
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function serialize($object, $value, $params = null)
    {
        return json_encode($value, JSON_OBJECT_AS_ARRAY);
    }

    /**
     * @param object $object
     * @param mixed $value
     * @param array|null $params
     * @return string
     */
    public function unserialize($object, $value, $params = null)
    {
        return json_decode((string) $value, true);
    }
}
