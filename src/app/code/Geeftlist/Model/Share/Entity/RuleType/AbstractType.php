<?php

namespace Geeftlist\Model\Share\Entity\RuleType;


use Geeftlist\Model\Share\RuleTypeInterface as ShareRuleInterface;

abstract class AbstractType implements ShareRuleInterface
{
    public const DEFAULT_PRIORITY = 0;

    protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory;

    protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var string */
    protected $code;

    /** @var string[] */
    protected $supportedEntities = [];

    public function __construct(
        \Geeftlist\Model\Share\Entity\RuleType\Context $context
    ) {
        $this->coreFactory = $context->getCoreFactory();
        $this->modelValidator = $context->getModelValidator();
        $this->logger = $context->getLogService()->getLoggerForClass($this);
    }

    public function filterParams(array $params): array {
        return $params;
    }

    /**
     * @return $this
     */
    public function validateRule(
        \Geeftlist\Model\Share\Entity\Rule $shareEntityRule,
        \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator
    ): static {
        return $this;
    }

    /**
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @return string[]
     */
    public function getSupportedEntities() {
        return $this->supportedEntities;
    }

    /**
     * @return int
     */
    public function getDefaultPriority() {
        return static::DEFAULT_PRIORITY;
    }

    /**
     * @return \OliveOil\Core\Service\GenericFactoryInterface
     */
    public function getCoreFactory() {
        return $this->coreFactory;
    }

    /**
     * @return \OliveOil\Core\Service\Validation\Model\ValidatorInterface
     */
    public function getModelValidator() {
        return $this->modelValidator;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }
}
