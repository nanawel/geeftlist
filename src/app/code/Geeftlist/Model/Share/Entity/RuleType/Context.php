<?php

namespace Geeftlist\Model\Share\Entity\RuleType;


class Context
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefterRepository;

    public function __construct(protected \OliveOil\Core\Service\GenericFactoryInterface $coreFactory, protected \Geeftlist\Model\RepositoryFactory $repositoryFactory, protected \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator, protected \OliveOil\Core\Service\Log $logService)
    {
    }

    public function getCoreFactory(): \OliveOil\Core\Service\GenericFactoryInterface {
        return $this->coreFactory;
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }

    public function getModelValidator(): \OliveOil\Core\Service\Validation\Model\ValidatorInterface {
        return $this->modelValidator;
    }

    public function getLogService(): \OliveOil\Core\Service\Log {
        return $this->logService;
    }
}
