<?php

namespace Geeftlist\Model\Share\Gift\RuleType\System;


use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Share\Gift\RuleType\AbstractType;

class ArchivedGiftsType extends AbstractType
{
    public const CODE = 'system/archivedGifts';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }


        $return = [];
        if ($gift->getStatus() == Status::ARCHIVED) {
            $reservationActions = [
                self::ACTION_RESERVE_PURCHASE,
                self::ACTION_EDIT_RESERVATION,
                self::ACTION_CANCEL_RESERVATION
            ];

            // Creator can't reserve/purchase (but can still do other actions)
            $actionAuthorizations = [];
            foreach ($reservationActions as $action) {
                $actionAuthorizations[$action] = self::FORBIDDEN;
            }

            $return[$gift->getCreatorId()] = $actionAuthorizations;

            // Others can't do anything
            $actionAuthorizations = [];
            foreach (self::ALL_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::FORBIDDEN;
            }

            $return[self::WILDCARD_GEEFTER_ID] = $actionAuthorizations;
        }

        return $return;
    }
}
