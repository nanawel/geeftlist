<?php

namespace Geeftlist\Model\Share\Gift\RuleType\System;


use Geeftlist\Model\Share\Gift\RuleType\AbstractType;

class GiftGeefteesType extends AbstractType
{
    public const CODE = 'system/giftGeeftees';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $return = [];
        foreach ($this->getGeefteeGiftHelper()->getGeeftees($gift) as $geeftee) {
            $geefterId = $geeftee->getGeefterId();
            if ($geefterId) {
                $actionAuthorizations = [];
                foreach (self::ALL_ACTIONS as $action) {
                    $actionAuthorizations[$action] = self::FORBIDDEN;
                }

                $return[$geefterId] = $actionAuthorizations;
            }
        }

        return $return;
    }
}
