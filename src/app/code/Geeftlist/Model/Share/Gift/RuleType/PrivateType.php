<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Gift;

class PrivateType extends AbstractType
{
    public const CODE = 'private';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $creatorActionAuthorizations = [];
        foreach (self::RELATIVE_ACTIONS as $action) {
            $creatorActionAuthorizations[$action] = self::ALLOWED;
        }

        $othersActionAuthorizations = [];
        foreach (self::ALL_ACTIONS as $action) {
            $othersActionAuthorizations[$action] = self::FORBIDDEN;
        }

        return [
            $gift->getCreatorId()     => $creatorActionAuthorizations,
            self::WILDCARD_GEEFTER_ID => $othersActionAuthorizations
        ];
    }
}
