<?php

namespace Geeftlist\Model\Share\Gift\RuleType\System;


use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Share\Gift\RuleType\AbstractType;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;

class CreatorType extends AbstractType
{
    public const CODE = 'system/creator';

    protected \OliveOil\Core\Model\Cache\ArrayObjectInterface $relatedGeefteeIdCache;

    /**
     * @param bool $useRelatedGeefteeIdsCache
     * @param class-string<\OliveOil\Core\Model\Cache\ArrayObjectInterface> $cacheClass
     */
    public function __construct(
        \Geeftlist\Model\Share\Gift\RuleType\Context $context,
        protected \Geeftlist\Helper\Geeftee $geefteeHelper,
        string $cacheClass = \OliveOil\Core\Model\Cache\ArrayObjectInterface::class,
        protected bool $useRelatedGeefteeIdsCache = true
    ) {
        parent::__construct($context);
        /** @phpstan-ignore-next-line */
        $this->relatedGeefteeIdCache = $this->getCoreFactory()->make($cacheClass);
    }

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $return = [];
        if ($creatorId = $gift->getCreatorId()) {
            $creatorGeeftee = $gift->getCreator()->getGeeftee();
            $geeftees = $this->getGeefteeGiftHelper()->getGeeftees($gift);

            $actionAuthorizations = [];
            // Creator always has manage actions
            foreach (self::MANAGE_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::ALLOWED;
            }

            // Unless one geeftee is not related anymore, in which case edition is prohibited
            foreach ($geeftees as $geeftee) {
                if (!$this->areGeefteeRelated($creatorGeeftee, $geeftee)) {
                    $actionAuthorizations[TypeInterface::ACTION_EDIT] = self::FORBIDDEN;
                    break;
                }
            }

            // If the creator is not a geeftee, also add some actions relative to reservations
            if (!array_key_exists($creatorGeeftee->getId(), $geeftees)) {
                foreach ([self::ACTION_VIEW_RESERVATIONS, self::ACTION_BE_NOTIFIED] as $action) {
                    $actionAuthorizations[$action] = self::ALLOWED;
                }
            }

            $return[$creatorId] = $actionAuthorizations;
        }

        return $return;
    }

    protected function areGeefteeRelated(Geeftee $geefteeA, Geeftee $geefteeB) {
        $geefteeIdA = $geefteeA->getId();
        $geefteeIdB = $geefteeB->getId();

        if (
            !$this->useRelatedGeefteeIdsCache
            || !isset($this->relatedGeefteeIdCache[$geefteeIdA][$geefteeIdB])
        ) {
            $this->relatedGeefteeIdCache[$geefteeIdA][$geefteeIdB]
                = $this->relatedGeefteeIdCache[$geefteeIdB][$geefteeIdA]
                = $this->getGeefteeHelper()->assertIsGeefteeIdRelated($geefteeIdA, $geefteeIdB);
        }

        return $this->relatedGeefteeIdCache[$geefteeIdA][$geefteeIdB];
    }

    public function getGeefteeHelper(): \Geeftlist\Helper\Geeftee {
        return $this->geefteeHelper;
    }
}
