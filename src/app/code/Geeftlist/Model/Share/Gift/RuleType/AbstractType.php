<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\RuleTypeInterface as ShareRuleInterface;

abstract class AbstractType extends \Geeftlist\Model\Share\Entity\RuleType\AbstractType implements TypeInterface, ShareRuleInterface
{
    public const CODE = '_MISSING_TYPE_CODE_';

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $giftRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefteeRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $familyRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefterRepository;

    protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper;

    public function __construct(
        \Geeftlist\Model\Share\Gift\RuleType\Context $context
    ) {
        $this->code = static::CODE;
        $this->supportedEntities = [Gift::ENTITY_TYPE];
        $this->giftRepository = $context->getGiftRepository();
        $this->geefteeRepository = $context->getGeefteeRepository();
        $this->familyRepository = $context->getFamilyRepository();
        $this->geefterRepository = $context->getGeefterRepository();
        $this->geefteeGiftHelper = $context->getGeefteeGiftHelper();
        parent::__construct($context);
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGiftRepository() {
        return $this->giftRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefteeRepository() {
        return $this->geefteeRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getFamilyRepository() {
        return $this->familyRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefterRepository() {
        return $this->geefterRepository;
    }

    /**
     * @return \Geeftlist\Model\Geeftee\Gift
     */
    public function getGeefteeGiftHelper() {
        return $this->geefteeGiftHelper;
    }
}
