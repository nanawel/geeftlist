<?php

namespace Geeftlist\Model\Share\Gift\RuleType\System;


use Geeftlist\Model\Reservation;
use Geeftlist\Model\Share\Gift\RuleType\AbstractType;

class ReserverType extends AbstractType
{
    public const CODE = 'system/reserver';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $return = [];
        /** @var Reservation $reservation */
        foreach ($gift->getReservationCollection() as $reservation) {
            $actionAuthorizations = [];
            // Reserver always has reservation actions
            foreach (self::RESERVER_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::ALLOWED;
            }

            $return[$reservation->getGeefterId()] = $actionAuthorizations;
        }

        return $return;
    }
}
