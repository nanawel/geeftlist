<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;

class Context extends \Geeftlist\Model\Share\Entity\RuleType\Context
{
    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $giftRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefteeRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $geefterRepository;

    /** @var \OliveOil\Core\Model\RepositoryInterface */
    protected $familyRepository;

    public function __construct(
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftHelper,
        \OliveOil\Core\Service\Validation\Model\ValidatorInterface $modelValidator,
        \OliveOil\Core\Service\Log $logService
    ) {
        $this->giftRepository = $repositoryFactory->resolveGet(Gift::ENTITY_TYPE);
        $this->geefteeRepository = $repositoryFactory->resolveGet(Geeftee::ENTITY_TYPE);
        $this->geefterRepository = $repositoryFactory->resolveGet(Geefter::ENTITY_TYPE);
        $this->familyRepository = $repositoryFactory->resolveGet(Family::ENTITY_TYPE);
        parent::__construct(
            $coreFactory,
            $repositoryFactory,
            $modelValidator,
            $logService
        );
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGiftRepository() {
        return $this->giftRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefteeRepository() {
        return $this->geefteeRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getGeefterRepository() {
        return $this->geefterRepository;
    }

    /**
     * @return \OliveOil\Core\Model\RepositoryInterface
     */
    public function getFamilyRepository() {
        return $this->familyRepository;
    }

    public function getGeefteeGiftHelper(): \Geeftlist\Model\Geeftee\Gift {
        return $this->geefteeGiftHelper;
    }
}
