<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use OliveOil\Core\Model\Validation\ErrorInterface;

class GeefterType extends AbstractType
{
    public const CODE = 'geefter';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $params = $entityRule->getParams();
        if (!is_array($params) || $params === [] || empty($params['geefter_ids']) || !is_array($params['geefter_ids'])) {
            $this->getLogger()->warning('Missing required params in ' . $entityRule . '.');

            return [];
        }

        $geefterIds = $params['geefter_ids'];

        $result = [];
        foreach ($geefterIds as $geefterId) {
            /** @var Geefter $geefter */
            $geefter = $this->getGeefterRepository()->get($geefterId);
            if ($geefter) {
                $actionAuthorizations = [];
                foreach (self::RELATIVE_ACTIONS as $action) {
                    $actionAuthorizations[$action] = self::ALLOWED;
                }

                $result[$geefter->getId()] = $actionAuthorizations;
            }
        }

        // Force also relative permissions for creator here
        foreach (self::RELATIVE_ACTIONS as $action) {
            $actionAuthorizations[$action] = self::ALLOWED;
        }

        $result[$gift->getCreatorId()] = $actionAuthorizations;

        return $result;
    }

    public function filterParams(array $params): array {
        $params = \OliveOil\array_mask($params, ['geefter_ids'])
            + ['geefter_ids' => []];
        $params['geefter_ids'] = array_unique($params['geefter_ids']);

        return $params;
    }

    /**
     * @return $this
     */
    public function validateRule(
        \Geeftlist\Model\Share\Entity\Rule $shareEntityRule,
        \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator
    ): static {
        /** @var Gift $gift */
        $gift = $shareEntityRule->getTargetEntity();
        if (!$gift) {
            throw new \InvalidArgumentException('Invalid gift for share rule.');
        }

        /** @var Geefter $creator */
        $creator = $gift->getCreator();

        $validationRules = [
            'params' => [
                function ($value) use ($creator) {
                    if (
                        !isset($value['geefter_ids'])
                        || ! is_array($value['geefter_ids'])
                        || empty($value['geefter_ids'])
                    ) {
                        return $this->getCoreFactory()->make(ErrorInterface::class)
                            ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                            ->setLocalizableMessage('Missing geefters for share rule.')
                            ->setMessage('Missing geefters for share rule.');
                    }

                    /** @var int[] $geefterIds */
                    $geefterIds = array_unique($value['geefter_ids']);

                    $relatedGeefters = $this->getGeefterRepository()->find([
                        'filter' => [
                            'related_geefter' => [['eq' => $creator->getId()]]
                        ]
                    ]);
                    if (array_diff($geefterIds, array_keys($relatedGeefters)) !== []) {
                        return $this->getCoreFactory()->make(ErrorInterface::class)
                            ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('Invalid geefters for share rule.')
                            ->setMessage('Invalid geefters for share rule.');
                    }
                }
            ]
        ];
        $this->getModelValidator()->validate($shareEntityRule, $validationRules, $errorAggregator);

        return $this;
    }
}
