<?php

namespace Geeftlist\Model\Share\Gift\RuleType\System;


use Geeftlist\Model\Gift\Field\Status;
use Geeftlist\Model\Share\Gift\RuleType\AbstractType;

class DeletedGiftsType extends AbstractType
{
    public const CODE = 'system/deletedGifts';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $return = [];
        if ($gift->getStatus() == Status::DELETED) {
            $actionAuthorizations = [];
            foreach (self::ALL_ACTIONS as $action) {
                $actionAuthorizations[$action] = self::FORBIDDEN;
            }

            $return[self::WILDCARD_GEEFTER_ID] = $actionAuthorizations;

            //TODO Add exception for geefters with reservations
        }

        return $return;
    }
}
