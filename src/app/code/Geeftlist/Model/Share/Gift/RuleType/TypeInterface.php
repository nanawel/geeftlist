<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW                = Constants::ACTION_VIEW;

    public const ACTION_EDIT                = Constants::ACTION_EDIT;

    public const ACTION_DELETE              = Constants::ACTION_DELETE;

    public const ACTION_ARCHIVE             = Constants::ACTION_ARCHIVE;

    public const ACTION_BE_NOTIFIED         = Constants::ACTION_BE_NOTIFIED;

    public const ACTION_VIEW_RESERVATIONS   = 'view_reservations';

    public const ACTION_RESERVE_PURCHASE    = 'reserve-purchase';

    public const ACTION_EDIT_RESERVATION    = 'edit_reservation';

    public const ACTION_CANCEL_RESERVATION  = 'cancel_reservation';

    public const WILDCARD_GEEFTER_ID = Constants::WILDCARD_GEEFTER_ID;

    public const ALL_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_EDIT,
        self::ACTION_DELETE,
        self::ACTION_ARCHIVE,
        self::ACTION_VIEW_RESERVATIONS,
        self::ACTION_RESERVE_PURCHASE,
        self::ACTION_EDIT_RESERVATION,
        self::ACTION_CANCEL_RESERVATION,
        self::ACTION_BE_NOTIFIED,
    ];

    public const MANAGE_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_EDIT,
        self::ACTION_DELETE,
        self::ACTION_ARCHIVE,
    ];

    public const RELATIVE_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_VIEW_RESERVATIONS,
        self::ACTION_RESERVE_PURCHASE,
        self::ACTION_BE_NOTIFIED,
    ];

    public const RESERVER_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_VIEW_RESERVATIONS,
        self::ACTION_EDIT_RESERVATION,
        self::ACTION_CANCEL_RESERVATION,
        self::ACTION_BE_NOTIFIED
    ];

    public const OPEN_GIFT_GEEFTEE_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_BE_NOTIFIED,
    ];

    public const DEFAULT_ACTIONS = self::RELATIVE_ACTIONS;
}
