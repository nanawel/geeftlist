<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Family;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use OliveOil\Core\Model\Validation\ErrorInterface;

class FamilyType extends AbstractType
{
    public const CODE = 'family';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $params = $entityRule->getParams();
        if (!is_array($params) || $params === [] || empty($params['family_ids']) || !is_array($params['family_ids'])) {
            $this->getLogger()->warning('Missing required params in ' . $entityRule . '.');

            return [];
        }

        $familyIds = $params['family_ids'];

        $result = [];
        foreach ($familyIds as $familyId) {
            /** @var Family $family */
            $family = $this->getFamilyRepository()->get($familyId);
            if ($family) {
                foreach ($family->getGeefteeCollection() as $familyGeeftee) {
                    if ($geefterId = $familyGeeftee->getGeefterId()) {
                        $actionAuthorizations = [];
                        foreach (self::RELATIVE_ACTIONS as $action) {
                            $actionAuthorizations[$action] = self::ALLOWED;
                        }

                        $result[$geefterId] = $actionAuthorizations;
                    }
                }
            }
        }

        // Force also relative permissions for creator here
        foreach (self::RELATIVE_ACTIONS as $action) {
            $actionAuthorizations[$action] = self::ALLOWED;
        }

        $result[$gift->getCreatorId()] = $actionAuthorizations;

        return $result;
    }

    public function filterParams(array $params): array {
        $params = \OliveOil\array_mask($params, ['family_ids'])
            + ['family_ids' => []];
        $params['family_ids'] = array_unique($params['family_ids']);

        return $params;
    }

    /**
     * @return $this
     */
    public function validateRule(
        \Geeftlist\Model\Share\Entity\Rule $shareEntityRule,
        \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator
    ): static {
        /** @var Gift $gift */
        $gift = $shareEntityRule->getTargetEntity();
        if (!$gift) {
            throw new \InvalidArgumentException('Invalid gift for share rule.');
        }

        /** @var Geefter $creator */
        $creator = $gift->getCreator();

        $validationRules = [
            'params' => [
                function ($value) use ($creator) {
                    if (
                        !isset($value['family_ids'])
                        || ! is_array($value['family_ids'])
                        || empty($value['family_ids'])
                    ) {
                        return $this->getCoreFactory()->make(ErrorInterface::class)
                            ->setType(ErrorInterface::TYPE_FIELD_EMPTY)
                            ->setLocalizableMessage('Missing families for share rule.')
                            ->setMessage('Missing families for share rule.');
                    }

                    /** @var int[] $familyIds */
                    $familyIds = array_unique($value['family_ids']);

                    $creatorFamilyIds = $creator->getFamilyCollection()->getAllIds();
                    if (array_diff($familyIds, $creatorFamilyIds) !== []) {
                        return $this->getCoreFactory()->make(ErrorInterface::class)
                            ->setType(ErrorInterface::TYPE_FIELD_INVALID)
                            ->setLocalizableMessage('Invalid families for share rule.')
                            ->setMessage('Invalid families for share rule.');
                    }
                }
            ]
        ];
        $this->getModelValidator()->validate($shareEntityRule, $validationRules, $errorAggregator);

        return $this;
    }
}
