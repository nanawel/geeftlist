<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Family;
use Geeftlist\Model\Gift;

class RegularType extends AbstractType
{
    public const CODE = 'regular';

    /** @var \OliveOil\Core\Model\Cache\ArrayObjectInterface [familyId => [geefterIds], ...] */
    protected static $familyGeefterIdsCache;

    /**
     * @param bool $useFamilyGeefterIdsCache
     */
    public function __construct(
        \Geeftlist\Model\Share\Gift\RuleType\Context $context,
        $cacheClass = \OliveOil\Core\Model\Cache\ArrayObjectInterface::class,
        protected $useFamilyGeefterIdsCache = true
    ) {
        if (self::$familyGeefterIdsCache === null) {
            self::$familyGeefterIdsCache = $context->getCoreFactory()->make($cacheClass);
        }
        parent::__construct($context);
    }

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $result = [];

        foreach ($this->getGeefteeGiftHelper()->getGeeftees($gift) as $geeftee) {
            foreach ($geeftee->getFamilyIds() as $familyId) {
                /** @var Family $family */
                $family = $this->getFamilyRepository()->get($familyId);

                foreach ($this->getFamilyGeefterIds($family) as $relatedGeefterId) {
                    $actionAuthorizations = [];
                    foreach (self::RELATIVE_ACTIONS as $action) {
                        $actionAuthorizations[$action] = self::ALLOWED;
                    }

                    $result[$relatedGeefterId] = $actionAuthorizations;
                }
            }
        }

        return $result;
    }

    /**
     * @return int[]
     */
    protected function getFamilyGeefterIds(Family $family) {
        if (!isset(self::$familyGeefterIdsCache[$family->getId()]) || !$this->useFamilyGeefterIdsCache) {
            self::$familyGeefterIdsCache[$family->getId()] = $family->getGeefterIds();
        }

        return self::$familyGeefterIdsCache[$family->getId()];
    }
}
