<?php

namespace Geeftlist\Model\Share\Gift\RuleType;


use Geeftlist\Model\Gift;

class OpenGiftType extends AbstractType
{
    public const CODE = 'openGift';

    /* Default priority must be higher than \Geeftlist\Model\Share\Gift\RuleType\System\GiftGeefteesType (500) */
    public const DEFAULT_PRIORITY = 600;

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $return = [];
        foreach ($this->getGeefteeGiftHelper()->getGeeftees($gift) as $geeftee) {
            $geefterId = $geeftee->getGeefterId();
            if ($geefterId) {
                $actionAuthorizations = [];
                foreach (self::OPEN_GIFT_GEEFTEE_ACTIONS as $action) {
                    $actionAuthorizations[$action] = self::ALLOWED;
                }

                $return[$geefterId] = $actionAuthorizations;
            }
        }

        return $return;
    }
}
