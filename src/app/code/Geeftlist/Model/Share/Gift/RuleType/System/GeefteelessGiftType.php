<?php

namespace Geeftlist\Model\Share\Gift\RuleType\System;


use Geeftlist\Model\Share\Constants;
use Geeftlist\Model\Share\Gift\RuleType\AbstractType;

class GeefteelessGiftType extends AbstractType
{
    public const CODE = 'system/geefteelessGift';

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId): array {
        /** @var \Geeftlist\Model\Gift $gift */
        $gift = $this->getGiftRepository()->get($entityId);
        if (!$gift) {
            $this->getLogger()->warning('Invalid gift ID: ' . $entityId);

            return [];
        }

        $return = [];

        $reservationActions = [
            self::ACTION_RESERVE_PURCHASE,
            self::ACTION_VIEW_RESERVATIONS
        ];

        if (! $this->getGeefteeGiftHelper()->getGeefteeIds($gift)) {
            $actionAuthorizations = [];
            foreach ($reservationActions as $action) {
                $actionAuthorizations[$action] = self::FORBIDDEN;
            }

            $return[Constants::WILDCARD_GEEFTER_ID] = $actionAuthorizations;
        }

        return $return;
    }
}
