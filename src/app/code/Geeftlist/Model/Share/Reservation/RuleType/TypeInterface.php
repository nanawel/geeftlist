<?php

namespace Geeftlist\Model\Share\Reservation\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW   = Constants::ACTION_VIEW;

    public const ACTION_EDIT   = Constants::ACTION_EDIT;

    public const ACTION_DELETE = Constants::ACTION_DELETE;
}
