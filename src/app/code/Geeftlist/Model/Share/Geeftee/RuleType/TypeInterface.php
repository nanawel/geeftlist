<?php

namespace Geeftlist\Model\Share\Geeftee\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW                  = Constants::ACTION_VIEW;

    public const ACTION_EDIT                  = Constants::ACTION_EDIT;

    public const ACTION_DELETE                = Constants::ACTION_DELETE;

    public const ACTION_INTERACT              = 'interact';

    public const ACTION_MERGE_TO              = 'merge';

    public const ACTION_ADD_GIFT              = 'add_gift';

    public const ACTION_RESERVE_GIFT          = 'reserve_gift';

    public const ACTION_CREATE_CLAIM_REQUEST  = 'create_claim_request';

    public const ACTION_ACCEPT_CLAIM_REQUEST  = 'accept_claim_request';

    public const ACTION_REJECT_CLAIM_REQUEST  = 'reject_claim_request';

    public const ACTION_CANCEL_CLAIM_REQUEST  = 'cancel_claim_request';

        // TODO Not used yet
    public const ACTION_SET_BADGES            = 'set_badges';
}
