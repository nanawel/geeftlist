<?php

namespace Geeftlist\Model\Share;


interface RuleTypeInterface
{
    public const ALLOWED   = Constants::ALLOWED;

    public const FORBIDDEN = Constants::FORBIDDEN;

    /**
     * @return string
     */
    public function getCode();

    /**
     * @return string[]
     */
    public function getSupportedEntities();

    /**
     * Get the allowed actions per geefter ID
     *
     * @param int $entityId
     * @return int[][]
     */
    public function getActionAuthorizationsByGeefter(\Geeftlist\Model\Share\Entity\Rule $entityRule, $entityId);

    /**
     * @return array
     */
    public function filterParams(array $params);

    /**
     * @return $this
     */
    public function validateRule(
        \Geeftlist\Model\Share\Entity\Rule $shareEntityRule,
        \OliveOil\Core\Model\Validation\ErrorAggregatorInterface $errorAggregator
    );

    /**
     * @return int
     */
    public function getDefaultPriority();
}
