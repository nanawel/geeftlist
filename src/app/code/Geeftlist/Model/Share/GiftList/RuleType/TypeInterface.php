<?php

namespace Geeftlist\Model\Share\GiftList\RuleType;


use Geeftlist\Model\Share\Constants;

interface TypeInterface
{
    public const ACTION_VIEW            = Constants::ACTION_VIEW;

    public const ACTION_EDIT            = Constants::ACTION_EDIT;

    public const ACTION_ARCHIVE         = Constants::ACTION_ARCHIVE;

    public const ACTION_DELETE          = Constants::ACTION_DELETE;

    public const ACTION_ADD_REMOVE_GIFT = 'add_remove_gift';
}
