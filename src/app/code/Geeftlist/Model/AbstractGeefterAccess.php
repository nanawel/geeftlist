<?php

namespace Geeftlist\Model;


use Geeftlist\Service\Permission\Geefter\DelegatorInterface;
use OliveOil\Core\Model\RepositoryInterface;

abstract class AbstractGeefterAccess implements DelegatorInterface, RestrictionsAppenderInterface
{
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /**
     * @param string $entityType
     */
    public function __construct(
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Service\Security $securityService,
        protected \Geeftlist\Model\ResourceModel\Iface\RestrictionsAppenderInterface $restrictionsAppender,
        protected \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface $geefterAccessResource,
        \OliveOil\Core\Service\Log $logService,
        protected $entityType
    ) {
        $this->logger = $logService->getLoggerForClass($this);
    }

    /**
     * @param AbstractModel[] $entities
     * @param string|string[] $actions
     * @return bool
     */
    public function isAllowed(Geefter $geefter, array $entities, $actions) {
        if (!is_array($actions)) {
            $actions = [$actions];
        }

        // Quick check in case we just have one item and allowed actions have been attached to entity's data
        if (count($entities) == 1) {
            $entity = current($entities);
            if ($entity->hasData('allowed_actions_geefter' . $geefter->getId())) {
                return array_intersect(
                    $actions,
                    $entity->getDataUsingMethod('allowed_actions_geefter' . $geefter->getId())
                ) === $actions;
            }
        }

        return $this->geefterAccessResource->isAllowed($geefter, $entities, $actions);
    }

    /**
     * @param AbstractModel[] $entities
     * @param string|string[] $actions
     * @return Geefter[]
     */
    public function getAllowed(array $entities, $actions) {
        if (!is_array($actions)) {
            $actions = [$actions];
        }

        $geefterIds = $this->geefterAccessResource->getAllowed($entities, $actions);

        return $this->securityService->callPrivileged(fn() => $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)->find([
            'filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [['in' => $geefterIds]]
            ]
        ]));
    }

    /**
     * Exclude unavailable topics for the specified geefter.
     *
     * @param \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection
     * @param string $actionType
     * @return $this
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter,
        $actionType
    ): static {
        $this->restrictionsAppender->addGeefterAccessRestrictions($collection, $geefter, $actionType);

        return $this;
    }

    /**
     * Append allowed actions to the specified collection.
     *
     * @param \Geeftlist\Model\ResourceModel\Db\Gift\Collection $collection
     * @return $this
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        \Geeftlist\Model\Geefter $geefter
    ): static {
        $this->restrictionsAppender->appendGeefterAccessPermissions($collection, $geefter);

        return $this;
    }
}
