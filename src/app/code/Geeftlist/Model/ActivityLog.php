<?php

namespace Geeftlist\Model;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\ActivityLog getResource()
 * @method string getPayload()
 * @method $this setPayload(mixed $payload)
 * @method string getLoggedAt()
 */
class ActivityLog extends AbstractModel
{
    public const ENTITY_TYPE = 'activityLog';

    public function __construct(
        \Geeftlist\Model\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fieldModels['payload'] = 'activityLog/field/payload';
    }
}
