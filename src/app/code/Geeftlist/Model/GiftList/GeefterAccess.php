<?php

namespace Geeftlist\Model\GiftList;


use Geeftlist\Model\AbstractGeefterAccess;
use Geeftlist\Model\GiftList;

class GeefterAccess extends AbstractGeefterAccess implements RestrictionsAppenderInterface
{
    public function __construct(
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        \Geeftlist\Service\Security $securityService,
        \Geeftlist\Model\ResourceModel\Iface\GiftList\RestrictionsAppenderInterface $restrictionsAppender,
        \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface $geefterAccessResource,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $repositoryFactory,
            $securityService,
            $restrictionsAppender,
            $geefterAccessResource,
            $logService,
            GiftList::ENTITY_TYPE
        );
    }
}
