<?php

namespace Geeftlist\Model\GiftList;

use OliveOil\Core\Model\AbstractModelManyToManyHelper;

/**
 * @extends \OliveOil\Core\Model\AbstractModelManyToManyHelper<
 *     \Geeftlist\Model\GiftList,
 *     \Geeftlist\Model\Gift,
 *     \Geeftlist\Model\ResourceModel\Db\GiftList\Collection,
 *     \Geeftlist\Model\ResourceModel\Db\Gift\Collection
 * >
 * @property \Geeftlist\Model\ResourceModel\Db\GiftList\Gift $resourceRelationHelper
 */
class Gift extends AbstractModelManyToManyHelper
{
    public function __construct(
        \Geeftlist\Model\Context $context,
        \Geeftlist\Model\ResourceModel\Db\GiftList\Gift $resourceRelationHelper
    ) {
        parent::__construct(
            $context,
            $resourceRelationHelper,
            \Geeftlist\Model\GiftList::ENTITY_TYPE,
            \Geeftlist\Model\Gift::ENTITY_TYPE,
            'giftlists',
            'gifts'
        );
    }

    /**
     * @return int[]
     */
    public function getGiftListIds(\Geeftlist\Model\Gift $gift): array {
        return $this->resourceRelationHelper->getGiftListIds($gift);
    }

    /**
     * @return array<int, \Geeftlist\Model\GiftList>
     */
    public function getGiftLists(\Geeftlist\Model\Gift $gift): array {
        return $this->resourceRelationHelper->getGiftListCollection($gift)->getItems();
    }
}
