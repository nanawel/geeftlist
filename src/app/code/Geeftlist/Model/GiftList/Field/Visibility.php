<?php

namespace Geeftlist\Model\GiftList\Field;

use OliveOil\Core\Model\Entity\AbstractOptionsField;

class Visibility extends AbstractOptionsField
{
    public const ENTITY_TYPE = 'giftList/field/visibility';

    public const PRIVATE    = 'private';

    public const PROTECTED  = 'protected';

    public const PUBLIC     = 'public';       // Not used atm

    public const VALUES = [
        self::PRIVATE,
        self::PROTECTED,
        //self::PUBLIC,
    ];

    public const OPTIONS = [
        self::PRIVATE   => [
            'label'       => 'Private',
            'description' => 'The list is only visible to you.'
        ],
        self::PROTECTED => [
            'label'       => 'Protected',
            'description' => 'The list and its content are visible to you and all your relatives.'
        ]
    ];
}
