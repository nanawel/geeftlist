<?php

namespace Geeftlist\Model\GiftList\Field;

use OliveOil\Core\Model\Entity\AbstractOptionsField;

class Status extends AbstractOptionsField
{
    public const ENTITY_TYPE = 'giftList/field/status';

    public const ACTIVE     = 'active';

    public const ARCHIVED   = 'archived';

    public const VALUES = [
        self::ACTIVE,
        self::ARCHIVED,
    ];

    public const OPTIONS = [
        self::ACTIVE    => [
            'label'       => 'Active',
            'description' => 'The list is active and gifts can be added to or removed from it.'
        ],
        self::ARCHIVED => [
            'label'       => 'Archived',
            'description' => 'The list is archived and is not displayed by default.'
        ]
    ];
}
