<?php

namespace Geeftlist\Model;


use OliveOil\Core\Service\GenericFactory;

/**
 * @extends \OliveOil\Core\Service\GenericFactory<\OliveOil\Core\Model\RepositoryInterface>
 */
class RepositoryFactory extends GenericFactory
{
}
