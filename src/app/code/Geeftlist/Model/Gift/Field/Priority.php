<?php

namespace Geeftlist\Model\Gift\Field;

use OliveOil\Core\Model\Entity\AbstractOptionsField;

class Priority extends AbstractOptionsField
{
    public const ENTITY_TYPE = 'gift/field/priority';

    public const PRIORITY_NONE    = 0;

    public const PRIORITY_LOW     = 100;

    public const PRIORITY_MEDIUM  = 200;

    public const PRIORITY_HIGH    = 300;

    public const VALUES = [
        self::PRIORITY_NONE,
        self::PRIORITY_LOW,
        self::PRIORITY_MEDIUM,
        self::PRIORITY_HIGH,
    ];

    public const OPTIONS = [
        self::PRIORITY_NONE => [
            'label'       => 'Not set',
            'description' => null
        ],
        self::PRIORITY_LOW => [
            'label'       => 'Low',
            'description' => 'Low Priority'
        ],
        self::PRIORITY_MEDIUM => [
            'label'       => 'Medium',
            'description' => 'Medium Priority'
        ],
        self::PRIORITY_HIGH => [
            'label'       => 'High',
            'description' => 'High Priority'
        ],
    ];

    /**
     * @inheritDoc
     */
    public function getAllOptions(): array {
        return self::OPTIONS;
    }
}
