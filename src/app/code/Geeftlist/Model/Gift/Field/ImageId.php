<?php

namespace Geeftlist\Model\Gift\Field;

use Geeftlist\Service\Gift\Image;
use OliveOil\Core\Model\Entity\AbstractField;
use OliveOil\Core\Model\Validation\ErrorInterface;

class ImageId extends AbstractField
{
    public const ENTITY_TYPE = 'gift/field/imageId';

    public function __construct(
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        protected \Geeftlist\Service\Gift\Image $giftImageService
    ) {
        parent::__construct($coreFactory);
    }

    /**
     * @param mixed $value
     * @param array|null $params
     * @return ErrorInterface|null
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null) {
        if (
            $value && (! $this->giftImageService->validate($value, Image::IMAGE_TYPE_FULL)
            && ! $this->giftImageService->validate($value, Image::IMAGE_TYPE_TMP))
        ) {
            return $this->getNewErrorInstance(
                ErrorInterface::TYPE_FIELD_INVALID,
                'Invalid image ID'
            );
        }

        return null;
    }
}
