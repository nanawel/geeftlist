<?php

namespace Geeftlist\Model\Gift\Field;

use OliveOil\Core\Model\Entity\AbstractOptionsField;

class Status extends AbstractOptionsField
{
    public const ENTITY_TYPE = 'gift/field/status';

    public const AVAILABLE     = 'available';

    public const ARCHIVED      = 'archived';

    public const DELETED       = 'deleted';

    public const VALUES = [
        self::AVAILABLE,
        self::ARCHIVED,
        self::DELETED
    ];

    public const OPTIONS = [
        self::AVAILABLE    => [
            'label'       => 'Available',
            'description' => 'The gift is available for reservation or purchase.'
        ],
        self::ARCHIVED => [
            'label'       => 'Archived',
            'description' => 'The gift is archived and cannot be reserved nor purchased anymore. It is only visible by its creator.'
        ],
        self::DELETED => [
            'label'       => 'Deleted',
            'description' => 'The gift is not visible anymore except for the geefters who already had it reserved at the time of deletion.'
        ]
    ];

    /** @var bool */
    protected $valueIsMandatory = true;
}
