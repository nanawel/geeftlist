<?php

namespace Geeftlist\Model\Gift;


use Geeftlist\Model\AbstractGeefterAccess;
use Geeftlist\Model\Geefter;
use Geeftlist\Model\Gift;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;

/**
 * @property \Geeftlist\Model\ResourceModel\Iface\Gift\RestrictionsAppenderInterface $restrictionsAppender
 */
class GeefterAccess extends AbstractGeefterAccess implements Gift\RestrictionsAppenderInterface
{
    public function __construct(
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        \Geeftlist\Service\Security $securityService,
        \Geeftlist\Model\ResourceModel\Iface\Gift\RestrictionsAppenderInterface $restrictionsAppender,
        \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface $geefterAccessResource,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $repositoryFactory,
            $securityService,
            $restrictionsAppender,
            $geefterAccessResource,
            $logService,
            Gift::ENTITY_TYPE
        );
    }

    /**
     * Exclude unavailable gifts for the specified geefter.
     *
     * @param string $giftTableAlias
     * @param string $giftIdColumnAlias
     * @param string $actionType
     * @param mixed $additionalPredicate
     * @return $this
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $giftTableAlias,
        $giftIdColumnAlias = 'gift_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): static {
        $this->restrictionsAppender->addGeefterAccessRestrictionsToCollection(
            $collection,
            $geefter,
            $giftTableAlias,
            $giftIdColumnAlias,
            $entityTypeColumnAlias,
            $actionType,
            $additionalPredicate
        );

        return $this;
    }
}
