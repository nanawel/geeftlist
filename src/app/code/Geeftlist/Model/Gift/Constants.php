<?php

namespace Geeftlist\Model\Gift;


interface Constants
{
    public const TOPIC_VISIBILITY_KEY        = 'gift.visibility';

    public const TOPIC_VISIBILITY_PROTECTED  = 'protected';

    public const TOPIC_VISIBILITY_PUBLIC     = 'public';

    public const TOPIC_DEFAULT_METADATA      = [
        self::TOPIC_VISIBILITY_KEY => self::TOPIC_VISIBILITY_PROTECTED
    ];

    public const REPORT_CREATOR       = 'creator';

    public const REPORT_ADMINISTRATOR = 'administrator';
}
