<?php

namespace Geeftlist\Model\Gift;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\PredicateInterface;

interface RestrictionsAppenderInterface extends \Geeftlist\Model\RestrictionsAppenderInterface
{
    /**
     * @param string $giftTableAlias
     * @param string $giftIdColumnAlias
     * @param string $actionType
     * @param PredicateInterface $additionalPredicate
     * @return $this
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $giftTableAlias,
        $giftIdColumnAlias = 'gift_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    );
}
