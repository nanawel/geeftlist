<?php

namespace Geeftlist\Model;

use Geeftlist\Exception\Activity\InvalidActorException;
use Geeftlist\Exception\Activity\InvalidObjectType;
use OliveOil\Core\Exception\Di\Exception;

/**
 * Class Activity
 *
 * @method \Geeftlist\Model\ResourceModel\Db\Activity getResource()
 * @method string getActorType()
 * @method $this setActorType(string $actorType)
 * @method int getActorId()
 * @method $this setActorId(int $actorId)
 * @method string getObjectType()
 * @method $this setObjectType(string $objectType)
 * @method int getObjectId()
 * @method $this setObjectId(int $objectId)
 * @method string getAction()
 * @method $this setAction(string $type)
 * @method string getPayload()
 * @method $this setPayload(string $payload)
 * @method string getCreatedAt()
 * @method $this setCreatedAt(string $createdAt)
 */
class Activity extends AbstractModel
{
    public const ENTITY_TYPE = 'activity';

    /** @var Geefter|Geeftee */
    protected $actorCache;

    /** @var Gift|Geeftee|Family */
    protected $objectCache;

    public function __construct(
        \Geeftlist\Model\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fieldModels['action'] = 'activity/field/action';
    }

    /**
     * @return Geefter|Geeftee
     */
    public function getActor($beforeLoadObjectFlags = []) {
        if (! $this->actorCache) {
            try {
                /** @var Geefter|Geeftee $actor */
                $actor = $this->modelFactory->resolveMake($this->getActorType());
            }
            catch (Exception $e) {
                throw new InvalidActorException($this->getActorType(), 0, $e);
            }

            $actor->setFlag($beforeLoadObjectFlags)
                ->load($this->getActorId());
            if ($actor->getId() === null) {
                throw new InvalidActorException($this->getActorId());
            }

            $this->actorCache = $actor;
        }

        return $this->actorCache;
    }

    /**
     *
     * @return Gift|Geeftee|Family
     */
    public function getObject($beforeLoadObjectFlags = []) {
        if (! $this->objectCache) {
            try {
                /** @var Gift|Geeftee|Family $object */
                $object = $this->modelFactory->resolveMake($this->getObjectType());
            }
            catch (Exception $e) {
                throw new InvalidObjectType($this->getObjectType(), null, $e);
            }

            $object->setFlag($beforeLoadObjectFlags)
                ->load($this->getObjectId());
            if ($object->getId() === null) {
                throw new InvalidObjectType(
                    sprintf('Object %s with ID %d cannot be found.', $this->getObjectType(), $this->getObjectId())
                );
            }

            $this->objectCache = $object;
        }

        return $this->objectCache;
    }

    public function getTypeId(): string {
        return implode('-', [
            $this->getActorType(),
            $this->getAction(),
            $this->getObjectType()
        ]);
    }
}
