<?php

namespace Geeftlist\Model;

/**
 * @method string getType()
 * @method $this setType(string $type)
 * @method string getRecipientType()
 * @method $this setRecipientType(string $type)
 * @method string getTargetType()
 * @method $this setTargetType(string $targetType)
 * @method int getTargetId()
 * @method $this setTargetId(int $targetId)
 * @method int getCategory()
 * @method $this setCategory(string $category)
 * @method string getEventName()
 * @method $this setEventName(string $eventName)
 * @method mixed getEventParams()
 * @method $this setEventParams(mixed $eventParams)
 * @method bool getDeferrable()
 * @method $this setDeferrable(bool $deferrable)
 * @method string getStatus()
 * @method $this setStatus(string $status)
 * @method string getMessage()
 * @method $this setMessage(string $message)
 * @method \DateTime getCreatedAt()
 * @method \DateTime getUpdatedAt()
 * @method $this setUpdatedAt(\DateTime|int $updatedAt)
 * @method \Geeftlist\Model\ResourceModel\Db\Notification getResource()
 */
class Notification extends AbstractModel
{
    public const ENTITY_TYPE = 'notification';

    public const TYPE_EMAIL = 'email';

    public const RECIPIENT_TYPE_GEEFTER = Geefter::ENTITY_TYPE;

    public const RECIPIENT_TYPE_ADMIN   = 'admin';

    public const CATEGORY_FAMILIES_ACTIVITY = 'families-activity';

    public const CATEGORY_INSTANCE_ACTIVITY = 'instance-activity';

    public const STATUS_CREATED    = 'created';

    public const STATUS_READY      = 'ready';

    public const STATUS_PENDING    = 'pending';

    public const STATUS_PROCESSING = 'processing';

       //Not used ATM
    public const STATUS_PROCESSED  = 'processed';

    //Not used ATM
    public const STATUS_CANCELED   = 'canceled';

    public const STATUS_FAILED     = 'failed';

    public function __construct(
        \Geeftlist\Model\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fieldModels['event_params'] = 'notification/field/eventParams';
    }

    /**
     * @param object|array $recipient
     * @param string $status
     * @return $this
     */
    public function addRecipient(
        $recipient,
        $status = \Geeftlist\Model\Notification::STATUS_CREATED
    ): static {
        $this->getResource()->addRecipient($this, $recipient, $status);

        return $this;
    }

    /**
     * @param object $recipient
     * @param string $status
     * @return $this
     */
    public function updateRecipientStatus($recipient, $status): static {
        $this->getResource()->updateRecipientStatus($this, $recipient, $status);

        return $this;
    }

    /**
     * @param object $recipient
     * @return string|false
     */
    public function getRecipientStatus($recipient) {
        return $this->getResource()->getRecipientStatus($this, $recipient);
    }

    /**
     * @return string[]
     */
    public function getAllRecipientStatus() {
        return $this->getResource()->getAllRecipientStatus($this);
    }
}
