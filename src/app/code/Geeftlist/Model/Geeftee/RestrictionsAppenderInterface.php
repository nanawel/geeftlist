<?php

namespace Geeftlist\Model\Geeftee;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Laminas\Db\Sql\Predicate\PredicateInterface;

interface RestrictionsAppenderInterface extends \Geeftlist\Model\RestrictionsAppenderInterface
{
    /**
     * @param \OliveOil\Core\Model\ResourceModel\Db\AbstractCollection $collection
     * @param string $geefteeTableAlias
     * @param string $geefteeIdColumnAlias
     * @param string $actionType
     * @param PredicateInterface $additionalPredicate
     * @return $this
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $geefteeTableAlias,
        $geefteeIdColumnAlias = 'geeftee_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    );
}
