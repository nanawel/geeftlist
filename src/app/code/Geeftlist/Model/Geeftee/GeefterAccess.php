<?php

namespace Geeftlist\Model\Geeftee;


use Geeftlist\Model\Geefter;
use Geeftlist\Model\Share\Gift\RuleType\TypeInterface;
use Geeftlist\Service\Permission\Geefter\DelegatorInterface;
use OliveOil\Core\Model\RepositoryInterface;

class GeefterAccess implements DelegatorInterface, RestrictionsAppenderInterface
{
    public function __construct(protected \Geeftlist\Service\Security $securityService, protected \Geeftlist\Model\RepositoryFactory $repositoryFactory, protected \Geeftlist\Model\ResourceModel\Iface\Geeftee\RestrictionsAppenderInterface $restrictionsAppender, protected \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface $geefterAccessResource)
    {
    }

    /**
     * @inheritDoc
     */
    public function isAllowed(Geefter $geefter, array $entities, $actions) {
        return $this->getGeefterAccessResource()->isAllowed($geefter, $entities, $actions);
    }

    /**
     * @inheritDoc
     */
    public function getAllowed(array $entities, $actions) {
        if (!is_array($actions)) {
            $actions = [$actions];
        }

        $geefterIds = $this->getGeefterAccessResource()->getAllowed($entities, $actions);

        return $this->getSecurityService()->callPrivileged(fn() => $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)->find([
            'filter' => [
                RepositoryInterface::WILDCARD_ID_FIELD => [['in' => $geefterIds]]
            ]
        ]));
    }

    /**
     * @inheritDoc
     */
    public function addGeefterAccessRestrictions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $actionType
    ): static {
        $this->getRestrictionsAppender()->addGeefterAccessRestrictions($collection, $geefter, $actionType);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function appendGeefterAccessPermissions(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter
    ): static {
        $this->getRestrictionsAppender()->appendGeefterAccessPermissions($collection, $geefter);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addGeefterAccessRestrictionsToCollection(
        \OliveOil\Core\Model\ResourceModel\Iface\Collection $collection,
        Geefter $geefter,
        $geefteeTableAlias,
        $geefteeIdColumnAlias = 'geeftee_id',
        $entityTypeColumnAlias = null,
        $actionType = TypeInterface::ACTION_VIEW,
        $additionalPredicate = null
    ): static {
        $this->getRestrictionsAppender()->addGeefterAccessRestrictionsToCollection(
            $collection,
            $geefter,
            $geefteeTableAlias,
            $geefteeIdColumnAlias,
            $entityTypeColumnAlias,
            $actionType,
            $additionalPredicate
        );

        return $this;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }

    public function getRestrictionsAppender(): \Geeftlist\Model\ResourceModel\Iface\Geeftee\RestrictionsAppenderInterface {
        return $this->restrictionsAppender;
    }

    public function getGeefterAccessResource(): \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface {
        return $this->geefterAccessResource;
    }
}
