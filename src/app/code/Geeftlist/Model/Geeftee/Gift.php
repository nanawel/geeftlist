<?php

namespace Geeftlist\Model\Geeftee;

use OliveOil\Core\Model\AbstractModelManyToManyHelper;

/**
 * @extends \OliveOil\Core\Model\AbstractModelManyToManyHelper<
 *     \Geeftlist\Model\Geeftee,
 *     \Geeftlist\Model\Gift,
 *     \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection,
 *     \Geeftlist\Model\ResourceModel\Db\Gift\Collection
 * >
 * @property \Geeftlist\Model\ResourceModel\Db\Geeftee\Gift $resourceRelationHelper
 */
class Gift extends AbstractModelManyToManyHelper
{
    protected \Geeftlist\Service\Security $securityService;

    public function __construct(
        \Geeftlist\Model\Context $context,
        \Geeftlist\Model\ResourceModel\Db\Geeftee\Gift $resourceRelationHelper
    ) {
        parent::__construct(
            $context,
            $resourceRelationHelper,
            \Geeftlist\Model\Geeftee::ENTITY_TYPE,
            \Geeftlist\Model\Gift::ENTITY_TYPE,
            'geeftees',
            'gifts'
        );
        $this->securityService = $context->getSecurityService();
    }

    public function getLinkedObjectsForReplace($object): array {
        if ($object instanceof \Geeftlist\Model\Gift) {
            // Make sure we get *all* the geeftees, even if normally not accessible to the current geefter
            return $this->securityService->callPrivileged(fn() => parent::getLinkedObjectsForReplace($object));
        }

        return parent::getLinkedObjectsForReplace($object);
    }

    public function hasGift(\Geeftlist\Model\Geeftee $geeftee, \Geeftlist\Model\Gift $gift): bool {
        return $this->resourceRelationHelper->isLinkedTo($geeftee, $gift);
    }

    /**
     * @return int[]
     */
    public function getGeefteeIds(\Geeftlist\Model\Gift $gift): array {
        return $this->resourceRelationHelper->getLinkedIds($gift);
    }

    /**
     * @param \Geeftlist\Model\Gift[] $gifts
     * @return \Geeftlist\Model\Gift[][]
     */
    public function getGiftsPerGeeftee(array $gifts) {
        return $this->resourceRelationHelper->getGiftsPerGeeftee($gifts);
    }

    /**
     * @deprecated Use self::getGeeftees() instead
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    public function getGeefteeCollection(
        \Geeftlist\Model\Gift $gift
    ): \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection {
        /** @var \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection $c */
        $c = $this->resourceRelationHelper->getLinkedCollection($gift);
        return $c;
    }

    /**
     * @return array<int, \Geeftlist\Model\Geeftee>
     */
    public function getGeeftees(\Geeftlist\Model\Gift $gift): array {
        return $this->getLinkedObjects($gift);
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Gift\Collection
     */
    public function getGiftCollection(
        \Geeftlist\Model\Geeftee $geeftee
    ): \Geeftlist\Model\ResourceModel\Db\Gift\Collection {
        /** @var \Geeftlist\Model\ResourceModel\Db\Gift\Collection $c */
        $c = $this->resourceRelationHelper->getLinkedCollection($geeftee);
        return $c;
    }
}
