<?php

namespace Geeftlist\Model\Geeftee;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Geeftee\ClaimRequest getResource()
 *
 * @method int getGeefteeId()
 * @method $this setGeefteeId(int $geefteeId)
 * @method int getGeefterId()
 * @method $this setGeefterId(int $sponsorId)
 * @method string getDecisionCode()
 * @method $this setDecisionCode(string $decisionCode)
 * @method string getDecisionDate()
 * @method $this setDecisionDate(string $decisionDate)
 * @method string getCreatedAt()
 * @method $this setCreatedAt(string $createdAt)
 */
class ClaimRequest extends AbstractModel
{
    public const ENTITY_TYPE = 'geeftee/claimRequest';

    public const DECISION_CODE_PENDING  = 'pending';

    public const DECISION_CODE_ACCEPTED = 'accepted';

    public const DECISION_CODE_REJECTED = 'rejected';

    public const DECISION_CODE_CANCELED = 'canceled';

    protected string $eventObjectParam = 'claim_request';

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \Geeftlist\Model\GiftList\Gift $giftListGiftRelation,
        array $data = []
    ) {
        $this->urlHelper = $context->getUrlHelper();
        parent::__construct($context, $data);

        // Mainly for UT, where object is not reloaded from DB before use
        if (!$this->getDecisionCode()) {
            $this->setDecisionCode(static::DECISION_CODE_PENDING);
        }
    }

    /**
     * @return Geefter
     */
    public function getGeefter() {
        if (!($geefter = parent::getGeefter()) && ($geefterId = $this->getGeefterId())) {
            $geefter = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)->get($geefterId);
            $this->setGeefter($geefter);
        }

        return $geefter;
    }

    /**
     * @return Geeftee
     */
    public function getGeeftee() {
        if (!($geeftee = parent::getGeeftee()) && ($geefteeId = $this->getGeefteeId())) {
            $geeftee = $this->repositoryFactory->resolveGet(Geeftee::ENTITY_TYPE)->get($geefteeId);
            $this->setGeeftee($geeftee);
        }

        return $geeftee;
    }

    public function getGeefteeName() {
        if ($geeftee = $this->getGeeftee()) {
            return $geeftee->getName();
        }

        return '';
    }

    public function getGeefterName() {
        if ($geefter = $this->getGeefter()) {
            return $geefter->getUsername();
        }

        return '';
    }
}
