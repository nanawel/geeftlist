<?php

namespace Geeftlist\Model\Api\Rest\JsonApi;


class Request extends \OliveOil\Core\Model\Rest\Request
{
    /**
     * @inheritDoc
     */
    public function fromGlobals(): static {
        parent::fromGlobals();

        $data = $this->body['data'] ?? [];
        $meta = $this->body['meta'] ?? [];

        $this->setData($data)
            ->setMeta($meta);

        return $this;
    }
}
