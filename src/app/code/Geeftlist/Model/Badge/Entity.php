<?php

namespace Geeftlist\Model\Badge;

use Geeftlist\Model\Badge;
use OliveOil\Core\Model\AbstractModelManyToManyHelper;

/**
 * @extends \OliveOil\Core\Model\AbstractModelManyToManyHelper<
 *     \Geeftlist\Model\AbstractModel,
 *     \Geeftlist\Model\Badge,
 *     \OliveOil\Core\Model\ResourceModel\Iface\Collection<\Geeftlist\Model\AbstractModel>,
 *     \Geeftlist\Model\ResourceModel\Db\Badge\Collection
 * >
 */
class Entity extends AbstractModelManyToManyHelper
{
    public function __construct(
        \Geeftlist\Model\Context $context,
        \OliveOil\Core\Model\ResourceModel\Iface\RelationHelperInterface $resourceRelationHelper
    ) {
        parent::__construct(
            $context,
            $resourceRelationHelper,
            null,
            Badge::ENTITY_TYPE,
            'entities',
            'badges'
        );
    }
}
