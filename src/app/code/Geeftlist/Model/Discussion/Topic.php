<?php

namespace Geeftlist\Model\Discussion;


use Geeftlist\Exception\Discussion\TopicException;
use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Gift\Constants;
use Geeftlist\Model\ResourceModel\Db;
use OliveOil\Core\Model\MagicObject;
use OliveOil\Core\Model\TransportObject;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Discussion\Topic getResource()
 *
 * @method int getAuthorId()
 * @method $this setAuthorId(int $authorId)
 * @method string getTitle()
 * @method $this setTitle(string $title)
 * @method string getStatus()
 * @method $this setStatus(string $status)
 * @method bool getIsPinned()
 * @method $this setIsPinned(bool $isPinned)
 * @method string getCreatedAt()
 * @method $this setCreatedAt(string $createdAt)
 * @method string getUpdatedAt()
 * @method $this setUpdatedAt(string $updatedAt)
 * @method string getLastActivityAt()
 * @method $this setLastActivityAt(string $lastActivityAt)
 * @method string getLinkedEntityType()
 * @method $this setLinkedEntityType(string $entityType)
 * @method int getLinkedEntityId()
 * @method $this setLinkedEntityId(int $entityId)
 */
class Topic extends AbstractModel
{
    public const ENTITY_TYPE = 'discussion/topic';

    public const STATUS_OPEN   = 'open';

    public const STATUS_CLOSED = 'closed';

    protected string $eventObjectParam = 'topic';

    public function beforeSave(): static {
        parent::beforeSave();

        // Set default metadata on save
        foreach (Constants::TOPIC_DEFAULT_METADATA as $key => $value) {
            if (! $this->hasMetadataByKey($key)) {
                $this->setMetadataByKey($key, $value);
            }
        }

        return $this;
    }

    public function getSortedPostCollection(): Db\Discussion\Post\Collection {
        return $this->getPostCollection()
            ->addJoinRelation('author', ['type' => 'left']) // #554 Also retrieve posts from deleted geefters
            ->orderBy('main_table.created_at', SORT_ASC);
    }

    public function getPostCollection(): Db\Discussion\Post\Collection {
        return $this->getResource()->getPostCollection($this);
    }

    public function getPostCount(): int {
        return $this->getPostCollection()->count();
    }

    public function getUrl(): ?string {
        $transportObject = new TransportObject();
        $this->eventManager->trigger('get_url', $this, ['transport_object' => $transportObject]);

        return $transportObject->getUrl();
    }

    public function getMetadata(): array {
        $this->loadMetadata();

        return parent::getMetadata();
    }

    public function getMetadataByKey(string $key): ?string {
        return $this->getMetadata()[$key] ?? null;
    }

    public function hasMetadataByKey(string $key): bool {
        return isset($this->getMetadata()[$key]);
    }

    public function setMetadataByKey(string $key, string $value): static {
        $metadata = $this->getMetadata();
        $metadata[(string) $key] = (string) $value;
        $this->setMetadata($metadata);

        return $this;
    }

    public function removeMetadataByKey(string $key): static {
        $metadata = $this->getMetadata();
        unset($metadata[(string) $key]);
        $this->setMetadata($metadata);

        return $this;
    }

    /**
     * Load or reload metadata internal cache
     */
    public function loadMetadata(bool $force = false): static {
        if ($force || ($metadata = parent::getMetadata()) === null) {
            $metadata = $this->getResource()->getModelMetadata($this);
            $this->setMetadata(array_merge($this->getDefaultMetadata(), $metadata));
        }

        return $this;
    }

    protected function getDefaultMetadata(): array {
        $defaultMetadata = new MagicObject();
        $this->triggerEvent('getDefaultMetadata::append', ['default_metadata' => $defaultMetadata]);

        return $defaultMetadata->getData();
    }

    /**
     * @return AbstractModel|null
     */
    public function getEntity(): ?AbstractModel {
        $entity = parent::getEntity();

        if (! $entity) {
            if (!$this->getLinkedEntityType()) {
                throw new TopicException('Invalid topic: no linked entity type has been defined.');
            }

            if (!$this->getLinkedEntityId()) {
                throw new TopicException('Invalid topic: no linked entity ID has been set.');
            }

            $transportObject = new MagicObject();
            $this->triggerEvent('getEntity::instanciate', ['transport_object' => $transportObject]);
            $entity = $transportObject->getEntity();

            if (! $entity || ! $entity->getId()) {
                throw new TopicException(sprintf(
                    'Invalid topic: no linked %s found with ID #%s.',
                    $this->getLinkedEntityType(),
                    $this->getLinkedEntityId()
                ));
            }
        }

        return $entity;
    }

    /**
     * @override
     * @return string[]
     */
    public function getIdentities(): array {
        return array_merge(
            parent::getIdentities(),
            $this->getEntity() ? $this->getEntity()->getIdentities() : []
        );
    }
}
