<?php

namespace Geeftlist\Model\Discussion\Topic;


use Geeftlist\Model\AbstractGeefterAccess;
use Geeftlist\Model\Discussion\Topic;

class GeefterAccess extends AbstractGeefterAccess implements Topic\RestrictionsAppenderInterface
{
    public function __construct(
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        \Geeftlist\Service\Security $securityService,
        \Geeftlist\Model\ResourceModel\Iface\RestrictionsAppenderInterface $restrictionsAppender,
        \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface $geefterAccessResource,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $repositoryFactory,
            $securityService,
            $restrictionsAppender,
            $geefterAccessResource,
            $logService,
            Topic::ENTITY_TYPE
        );
    }
}
