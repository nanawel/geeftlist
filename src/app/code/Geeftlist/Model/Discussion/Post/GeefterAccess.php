<?php

namespace Geeftlist\Model\Discussion\Post;


use Geeftlist\Model\AbstractGeefterAccess;
use Geeftlist\Model\Discussion\Post;

class GeefterAccess extends AbstractGeefterAccess implements Post\RestrictionsAppenderInterface
{
    public function __construct(
        \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        \Geeftlist\Service\Security $securityService,
        \Geeftlist\Model\ResourceModel\Iface\RestrictionsAppenderInterface $restrictionsAppender,
        \Geeftlist\Model\ResourceModel\Iface\GeefterAccessInterface $geefterAccessResource,
        \OliveOil\Core\Service\Log $logService
    ) {
        parent::__construct(
            $repositoryFactory,
            $securityService,
            $restrictionsAppender,
            $geefterAccessResource,
            $logService,
            Post::ENTITY_TYPE
        );
    }
}
