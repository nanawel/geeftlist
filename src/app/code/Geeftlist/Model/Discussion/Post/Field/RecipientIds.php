<?php

namespace Geeftlist\Model\Discussion\Post\Field;

use OliveOil\Core\Model\Entity\AbstractField;
use OliveOil\Core\Model\Validation\ErrorInterface;

class RecipientIds extends AbstractField
{
    public const ENTITY_TYPE = 'discussion/post/field/recipientIds';

    /**
     * @param mixed $value
     * @param array|null $params
     * @return ErrorInterface|null
     */
    public function validateValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null) {
        if ($value !== null && !is_array($value)) {
            return $this->getNewErrorInstance(
                ErrorInterface::TYPE_FIELD_INVALID,
                'Invalid value'
            );
        }

        return null;
    }

    /**
     * @param mixed $value
     * @param array|null $params
     */
    public function getFrontendValue(\OliveOil\Core\Model\AbstractModel $object, $value, $params = null): string
    {
        return implode(',', $value);
    }

    /**
     * @param object $object
     * @param mixed $value
     * @param array|null $params
     */
    public function serialize($object, $value, $params = null): string
    {
        return implode(',', $value);
    }

    /**
     * @param object $object
     * @param string $value
     * @param array|null $params
     */
    public function unserialize($object, $value, $params = null): ?array
    {
        return $value === null
            ? null
            : explode(',', (string) $value);
    }
}
