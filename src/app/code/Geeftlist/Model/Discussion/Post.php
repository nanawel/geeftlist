<?php

namespace Geeftlist\Model\Discussion;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Exception\NoSuchEntityException;
use OliveOil\Core\Helper\DateTime;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Discussion\Post getResource()
 * @method int getTopicId()
 * @method $this setTopicId(int $topicId)
 * @method string getStatus()
 * @method $this setStatus(string $status)
 * @method bool getIsPinned()
 * @method $this setIsPinned(bool $isPinned)
 * @method string getTitle()
 * @method $this setTitle(string $title)
 * @method string getMessage()
 * @method $this setMessage(string $message)
 * @method int[]|null getRecipientIds()
 * @method $this setRecipientIds(array $recipientIds)
 * @method int getAuthorId()
 * @method $this setAuthorId(int $authorId)
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method string getLastEditionAt()
 * @method $this setLastEditionAt(string $lastEditionAt)
 */
class Post extends AbstractModel
{
    public const ENTITY_TYPE = 'discussion/post';

    public const STATUS_NORMAL = 'normal';

    public const STATUS_DRAFT  = 'draft';

    protected string $eventObjectParam = 'post';

    public function __construct(
        \Geeftlist\Model\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fieldModels['recipient_ids'] = 'discussion/post/field/recipientIds';
    }

    public function beforeSave(): static {
        parent::beforeSave();

        if (
            $this->getId()
            && $this->getStatus() == self::STATUS_NORMAL
            && ($this->getOrigData('title') != $this->getTitle()
                || $this->getOrigData('message') != $this->getMessage())
        ) {
            $this->setLastEditionAt(DateTime::getDateSql());
        }

        return $this;
    }

    public function getTopic(): ?Topic {
        if (! $topic = parent::getTopic()) {
            if ($topicId = $this->getTopicId()) {
                /** @var Topic $topic */
                $topic = $this->repositoryFactory->resolveGet(Topic::ENTITY_TYPE)
                    ->get($topicId);
                if (!$topic) {
                    throw new NoSuchEntityException('Invalid topic.');
                }
                $this->setTopic($topic);

                return $topic;
            }
        }

        return $topic;
    }

    public function getAuthor(): ?Geefter {
        if (
            (! $author = parent::getAuthor())
            || $this->getAuthorId() != parent::getAuthor()->getId()
        ) {
            if ($this->getAuthorId()) {
                try {
                    $author = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
                        ->get($this->getAuthorId());
                    $this->setAuthor($author);
                } catch (NoSuchEntityException $e) {
                }
            }
        }

        return $author;
    }

    /**
     * @override
     * @return string[]
     */
    public function getIdentities(): array {
        return array_merge(
            parent::getIdentities(),
            $this->getTopic()->getIdentities()
        );
    }
}
