<?php

namespace Geeftlist\Model;


/**
 * @method \Geeftlist\Model\ModelFactory getModelFactory()
 * @method \Geeftlist\Model\ResourceFactory getResourceModelFactory()
 */
class Context extends \OliveOil\Core\Model\Context
{
    public function __construct(
        \Geeftlist\Model\ModelFactory $modelFactory,
        \Geeftlist\Model\ResourceFactory $resourceModelFactory,
        \OliveOil\Core\Service\GenericFactoryInterface $fieldModelFactory,
        \OliveOil\Core\Service\EventInterface $eventService,
        \OliveOil\Core\Service\Log $logService,
        \OliveOil\Core\Service\GenericFactoryInterface $coreFactory,
        \OliveOil\Core\Helper\Transaction $transactionHelper,
        protected \Geeftlist\Helper\Url $urlHelper,
        protected \Geeftlist\Model\RepositoryFactory $repositoryFactory,
        protected \Geeftlist\Service\Security $securityService
    ) {
        parent::__construct(
            $modelFactory,
            $resourceModelFactory,
            $fieldModelFactory,
            $eventService,
            $logService,
            $coreFactory,
            $transactionHelper
        );
    }

    public function getUrlHelper(): \Geeftlist\Helper\Url {
        return $this->urlHelper;
    }

    public function getRepositoryFactory(): \Geeftlist\Model\RepositoryFactory {
        return $this->repositoryFactory;
    }

    public function getSecurityService(): \Geeftlist\Service\Security {
        return $this->securityService;
    }
}
