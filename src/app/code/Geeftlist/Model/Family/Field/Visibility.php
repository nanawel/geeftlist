<?php

namespace Geeftlist\Model\Family\Field;

use OliveOil\Core\Model\Entity\AbstractOptionsField;

class Visibility extends AbstractOptionsField
{
    public const PUBLIC     = 'public';

    public const PROTECTED  = 'protected';

    public const PRIVATE    = 'private';

    public const VALUES = [
        self::PUBLIC,
        self::PROTECTED,
        self::PRIVATE
    ];

    public const OPTIONS = [
        self::PUBLIC    => [
            'label'       => 'Public',
            'description' => 'The family is visible by all users and can be joined without confirmation.'
        ],
        self::PROTECTED => [
            'label'       => 'Protected',
            'description' => 'The family is visible to all users but new members must be approved by its owner or invited by existing members.'
        ],
        self::PRIVATE   => [
            'label'       => 'Private',
            'description' => 'The family is only visible by its members. The owner and its members can invite users.'
        ]
    ];

    /** @var bool */
    protected $valueIsMandatory = true;
}
