<?php

namespace Geeftlist\Model\Family;

use OliveOil\Core\Model\AbstractModelManyToManyHelper;

/**
 * @extends \OliveOil\Core\Model\AbstractModelManyToManyHelper<
 *     \Geeftlist\Model\Family,
 *     \Geeftlist\Model\Geeftee,
 *     \Geeftlist\Model\ResourceModel\Db\Family\Collection,
 *     \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
 * >
 * @property \Geeftlist\Model\ResourceModel\Db\Family\Geeftee $resourceRelationHelper
 */
class Geeftee extends AbstractModelManyToManyHelper
{
    public function __construct(
        \Geeftlist\Model\Context $context,
        \Geeftlist\Model\ResourceModel\Db\Family\Geeftee $resourceRelationHelper
    ) {
        parent::__construct(
            $context,
            $resourceRelationHelper,
            \Geeftlist\Model\Family::ENTITY_TYPE,
            \Geeftlist\Model\Geeftee::ENTITY_TYPE,
            'families',
            'geeftees'
        );
    }

    /**
     * @return bool
     */
    public function isInFamily(\Geeftlist\Model\Geeftee $geeftee, \Geeftlist\Model\Family $family) {
        return $this->resourceRelationHelper->isInFamily($geeftee, $family);
    }

    /**
     * @return bool
     */
    public function hasGeeftee(\Geeftlist\Model\Family $family, \Geeftlist\Model\Geeftee $geeftee) {
        return $this->resourceRelationHelper->hasGeeftee($family, $geeftee);
    }

    /**
     * @return int[]
     */
    public function getGeefteeIds(\Geeftlist\Model\Family $family) {
        return $this->resourceRelationHelper->getGeefteeIds($family);
    }

    /**
     * @deprecated Use self::getGeeftees() instead
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    public function getGeefteeCollection(\Geeftlist\Model\Family $family) {
        return $this->resourceRelationHelper->getGeefteeCollection($family);
    }

    /**
     * @return array<int, \Geeftlist\Model\Geeftee>
     */
    public function getGeeftees(\Geeftlist\Model\Family $family) {
        return $this->getLinkedObjects($family);
    }
}
