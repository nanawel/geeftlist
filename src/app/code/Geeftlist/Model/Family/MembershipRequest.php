<?php

namespace Geeftlist\Model\Family;


use Geeftlist\Model\AbstractModel;
use Geeftlist\Model\Family;
use Geeftlist\Model\Geeftee;
use Geeftlist\Model\Geefter;
use OliveOil\Core\Helper\DateTime;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Family\MembershipRequest getResource()
 *
 * @method int getFamilyId()
 * @method $this setFamilyId(int $familyId)
 * @method int getGeefteeId()
 * @method $this setGeefteeId(int $geefteeId)
 * @method int getSponsorId()
 * @method $this setSponsorId(int $sponsorId)
 * @method string getDecisionCode()
 * @method $this setDecisionCode(string $decisionCode)
 * @method string getDecisionDate()
 * @method $this setDecisionDate(string $decisionDate)
 * @method string getCreatedAt()
 * @method $this setCreatedAt(string $createdAt)
 */
class MembershipRequest extends AbstractModel
{
    public const ENTITY_TYPE = 'family/membershipRequest';

    public const DECISION_CODE_PENDING  = 'pending';

    public const DECISION_CODE_ACCEPTED = 'accepted';

    public const DECISION_CODE_REJECTED = 'rejected';

    public const DECISION_CODE_CANCELED = 'canceled';

    public const ACTION_ACCEPT = 'accept';

    public const ACTION_REJECT = 'reject';

    public const ACTION_CANCEL = 'cancel';

    protected string $eventObjectParam = 'membership_request';

    public function isInvitation(): bool {
        return $this->getSponsorId() !== null;
    }

    public function beforeSave(): static {
        if (
            $this->getOrigData('decision_code') == self::DECISION_CODE_PENDING
            && $this->getDecisionCode() != self::DECISION_CODE_PENDING
        ) {
            $this->setDecisionDate(DateTime::getDateSql());
        }

        return parent::beforeSave();
    }

    /**
     * @return Family
     */
    public function getFamily() {
        if (!($family = parent::getFamily()) && ($familyId = $this->getFamilyId())) {
            $family = $this->repositoryFactory->resolveGet(Family::ENTITY_TYPE)->get($familyId);
            $this->setFamily($family);
        }

        return $family;
    }

    /**
     * @return Geeftee
     */
    public function getGeeftee() {
        if (!($geeftee = parent::getGeeftee()) && ($geefteeId = $this->getGeefteeId())) {
            $geeftee = $this->repositoryFactory->resolveGet(Geeftee::ENTITY_TYPE)->get($geefteeId);
            $this->setGeeftee($geeftee);
        }

        return $geeftee;
    }

    /**
     * @return Geefter
     */
    public function getSponsor() {
        if (!($sponsor = parent::getSponsor()) && ($sponsorId = $this->getSponsorId())) {
            $sponsor = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)->get($sponsorId);
            $this->setSponsor($sponsor);
        }

        return $sponsor;
    }
}
