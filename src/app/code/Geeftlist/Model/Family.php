<?php

namespace Geeftlist\Model;

use Geeftlist\Exception\FamilyException;
use Geeftlist\Model\Family\MembershipRequest;
use OliveOil\Core\Exception\NoSuchEntityException;

/**
 * @method \Geeftlist\Model\ResourceModel\Db\Family getResource()
 * @method string getName()
 * @method $this setName(string $name)
 * @method int|null getOwnerId()
 * @method $this setOwnerId(int $ownerId)
 * @method string getVisibility()
 * @method $this setVisibility(string $visibility)
 */
class Family extends AbstractModel
{
    public const ENTITY_TYPE = 'family';

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \Geeftlist\Model\Family\Geeftee $familyGeefteeHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fieldModels['visibility'] = 'family/field/visibility';
    }

    /**
     * @return int[]
     */
    public function getGeefteeIds() {
        return $this->getResource()->getGeefteeIds($this);
    }

    /**
     * @return int[]
     */
    public function getGeefterIds() {
        return $this->getResource()->getGeefterIds($this);
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Geeftee\Collection
     */
    public function getGeefteeCollection() {
        return $this->getResource()->getGeefteeCollection($this)
            ->orderBy('name');
    }

    public function isMember(Geeftee $geeftee): bool {
        return $this->getResource()->isMember($this, $geeftee);
    }

    public function isOwner(Geefter $geefter): bool {
        return $this->getResource()->isOwner($this, $geefter);
    }

    /**
     * @return Geefter
     * @throws NoSuchEntityException
     */
    public function getOwner() {
        if (! $owner = parent::getOwner()) {
            $owner = $this->modelFactory->resolveMake(Geefter::ENTITY_TYPE);
            $owner->load($this->getOwnerId());
            if (! $owner->getId()) {
                throw new NoSuchEntityException('Invalid geefter');
            }

            $this->setOwner($owner);
        }

        return $owner;
    }

    /**
     * @param Geeftee[]|Geeftee $geeftee
     * @return $this
     */
    public function addGeeftee($geeftee): static {
        if (! is_array($geeftee)) {
            $geeftee = [$geeftee];
        }

        $this->familyGeefteeHelper->addLinks([$this], $geeftee);

        return $this;
    }

    /**
     * @param Geeftee[]|Geeftee $geeftee
     * @return $this
     */
    public function removeGeeftee($geeftee): static {
        if (! is_array($geeftee)) {
            $geeftee = [$geeftee];
        }

        $this->familyGeefteeHelper->removeLinks([$this], $geeftee);

        return $this;
    }

    /**
     * @param int|Geeftee $geeftee
     * @return MembershipRequest
     * @throws \Exception
     */
    public function newMembershipRequest($geeftee) {
        $this->triggerEvent('newMembershipRequest::before', ['geeftee' => $geeftee]);
        if (! $familyId = $this->getId()) {
            throw new FamilyException('Family has no ID');
        }

        $mr = $this->modelFactory->resolveMake(MembershipRequest::ENTITY_TYPE, ['data' => [
            'family_id'  => $familyId,
            'geeftee_id' => is_object($geeftee) ? $geeftee->getId() : (int) $geeftee
        ]]);
        $this->triggerEvent('newMembershipRequest::after', ['membership_request' => $mr]);

        return $mr;
    }

    public function getViewUrl(array $params = []): string {
        return $this->urlHelper->getFamilyViewUrl($this, $params);
    }

    public function assertNameUnique(): bool {
        return $this->getResource()->assertNameUnique($this);
    }
}
