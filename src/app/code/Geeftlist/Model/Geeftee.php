<?php

namespace Geeftlist\Model;

use Geeftlist\Model\Geeftee\ClaimRequest;

/**
 * Class Geeftee
 *
 * @method string getName()
 * @method $this setName(string $name)
 * @method string getEmail()
 * @method $this setEmail(string|null $email)
 * @method $this setCreatorId(int|null $creatorId)
 * @method int getCreatorId()
 * @method $this setGeefterId(int|null $geefterId)
 * @method int getGeefterId()
 * @method string getDob()
 * @method $this setDob(string|int|null $dob)
 * @method string getAvatarPath()
 * @method $this setAvatarPath(string|null $avatarPath)
 * @method string getCreatedAt()
 * @method \Geeftlist\Model\ResourceModel\Db\Geeftee getResource()
 */
class Geeftee extends AbstractModel
{
    public const ENTITY_TYPE = 'geeftee';

    /** @var Geefter|null */
    protected $creator;

    /** @var Geefter|null */
    protected $geefter;

    public function __construct(
        \Geeftlist\Model\Context $context,
        protected \Geeftlist\Model\Geeftee\Gift $geefteeGiftRelation,
        array $data = []
    ) {
        $this->urlHelper = $context->getUrlHelper();
        parent::__construct($context, $data);
    }

    public function loadByEmail($email): static {
        $this->getResource()->load($this, trim((string) $email), 'email');

        return $this;
    }

    public function loadByGeefterId($geefterId): static {
        $this->getResource()->load($this, trim((string) $geefterId), 'geefter_id');

        return $this;
    }

    /**
     *
     * @return Geefter|null
     */
    public function getCreator() {
        if (! $this->creator && $this->getCreatorId()) {
            /** @var Geefter|null $creator */
            $creator = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
                ->get($this->getCreatorId());
            $this->creator = $creator;
        }

        return $this->creator;
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Gift\Collection
     */
    public function getGiftCollection() {
        return $this->geefteeGiftRelation->getGiftCollection($this);
    }

    /**
     * @return $this
     */
    public function addGift(Gift $gift): static {
        $this->geefteeGiftRelation->addLinks([$this], [$gift]);

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGift(Gift $gift): static {
        $this->geefteeGiftRelation->removeLinks([$this], [$gift]);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasGift(Gift $gift) {
        return $this->geefteeGiftRelation->hasGift($this, $gift);
    }

    public function getProfileUrl(array $params = []): string {
        return $this->urlHelper->getGeefteeProfileUrl($this, $params);
    }

    /**
     * Return geeftee profile avatar URL
     *
     * @return string
     */
    public function getAvatarUrl(array $params = []) {
        return $this->urlHelper->getGeefteeAvatarUrl($this, $params);
    }

    /**
     * @return int[]
     */
    public function getFamilyIds() {
        return $this->resourceFactory->resolveGet(Family::ENTITY_TYPE)->getFamilyIds($this);
    }

    /**
     * @return \Geeftlist\Model\ResourceModel\Db\Family\Collection
     */
    public function getFamilyCollection() {
        /** @var \Geeftlist\Model\ResourceModel\Db\Family\Collection $families */
        $families = $this->resourceFactory->resolveMakeCollection(Family::ENTITY_TYPE);

        return $families->addFieldToFilter('geeftee', $this->getId());
    }

    /**
     * @param int|Geefter $geefter
     * @return ClaimRequest
     */
    public function newClaimRequest($geefter) {
        $this->triggerEvent('newClaimRequest::before', ['geefter' => $geefter]);
        $cr = $this->modelFactory->resolveMake(ClaimRequest::ENTITY_TYPE, ['data' => [
            'geeftee_id' => $this->getId(),
            'geefter_id' => is_object($geefter) ? $geefter->getId() : (int) $geefter
        ]]);
        $this->triggerEvent('newClaimRequest::after', ['claim_request' => $cr]);

        return $cr;
    }

    /**
     * @inheritDoc
     */
    public function getClaimRequestCollection() {
        return $this->resourceFactory->resolveMakeCollection(ClaimRequest::ENTITY_TYPE)
            ->addFieldToFilter('geeftee_id', $this->getId());
    }

    public function beforeMerge(Geeftee $sourceGeeftee): static {
        $this->triggerEvent('merge::before', ['source_geeftee' => $sourceGeeftee]);
        $sourceGeeftee->triggerEvent('merge_to::before', ['target_geeftee' => $this]);

        return $this;
    }

    public function afterMerge(Geeftee $sourceGeeftee): static {
        $this->triggerEvent('merge::after', ['source_geeftee' => $sourceGeeftee]);
        $sourceGeeftee->triggerEvent('merge_to::after', ['target_geeftee' => $this]);

        return $this;
    }

    /**
     *
     * @return Geefter|null
     */
    public function getGeefter() {
        if (!$this->geefter && $this->getGeefterId()) {
            /** @var Geefter|null $geefter */
            $geefter = $this->repositoryFactory->resolveGet(Geefter::ENTITY_TYPE)
                ->get($this->getGeefterId());
            $this->geefter = $geefter;
        }

        return $this->geefter;
    }

    public function assertEmailUnique(): bool {
        return $this->getResource()->assertEmailUnique($this);
    }

    public function assertNameUnique(): bool {
        return $this->getResource()->assertNameUnique($this);
    }
}
