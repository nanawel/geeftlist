<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::METADATA_TABLE);
$alterStmt->addConstraint(new Ddl\Index\Index(
    'value',
    $this->getConnection()->getIndexName(Db\Discussion\Topic::METADATA_TABLE, 'value')
));
$this->getConnection()->query($alterStmt);

// Note: an index on (A,B) is also automatically used for queries on A,
// so we just need to add a single-column index on B
$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::MAIN_TABLE);
$alterStmt->addConstraint(new Ddl\Index\Index(
    ['linked_entity_type', 'linked_entity_id'],
    $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, ['linked_entity_type', 'linked_entity_id'])
))->addConstraint(new Ddl\Index\Index(
    'linked_entity_id',
    $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'linked_entity_id')
));
$this->getConnection()->query($alterStmt);
