<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/uninstall/feature-admin-notifications.php';
include __DIR__ . '/uninstall/feature-update-geefter-email.php';
