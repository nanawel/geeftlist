<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$stmt = $this->getConnection()->delete(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$stmt->where->equalTo('path', 'app/version/data');
$this->getConnection()->query($stmt);
