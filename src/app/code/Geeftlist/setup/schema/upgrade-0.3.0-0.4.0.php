<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

/*
 * Fix FOREIGN KEY "ON DELETE" behavior on several tables and set those columns as nullable
 */

//
// MEMBERSHIP REQUEST
//
$fkName = $this->getConnection()->getForeignKeyName(Db\Family\MembershipRequest::MAIN_TABLE, 'sponsor_id', Db\Geefter::MAIN_TABLE, 'geefter_id');
$this->dropForeignKey(Db\Family\MembershipRequest::MAIN_TABLE, $fkName);

$alterStmt = new Ddl\AlterTable(Db\Family\MembershipRequest::MAIN_TABLE);
$alterStmt->addConstraint(new Ddl\Constraint\ForeignKey(
    $fkName,
    'sponsor_id',
    Db\Geefter::MAIN_TABLE,
    'geefter_id',
    ConstraintKeyObject::FK_SET_NULL,
    ConstraintKeyObject::FK_CASCADE
));
$this->getConnection()->query($alterStmt);

//
// CLAIM REQUEST
//
$fkName = $this->getConnection()->getForeignKeyName(Db\Geeftee\ClaimRequest::MAIN_TABLE, 'geefter_id', Db\Geefter::MAIN_TABLE, 'geefter_id');
$this->dropForeignKey(Db\Geeftee\ClaimRequest::MAIN_TABLE, $fkName);

$alterStmt = new Ddl\AlterTable(Db\Geeftee\ClaimRequest::MAIN_TABLE);
$alterStmt->changeColumn('geefter_id', new Ddl\Column\Integer('geefter_id', true, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $fkName,
        'geefter_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($alterStmt);

//
// DISCUSSION TOPIC
//
$fkName = $this->getConnection()->getForeignKeyName(Db\Discussion\Topic::MAIN_TABLE, 'author_id', Db\Geefter::MAIN_TABLE, 'geefter_id');
$this->dropForeignKey(Db\Discussion\Topic::MAIN_TABLE, $fkName);

$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::MAIN_TABLE);
$alterStmt->changeColumn('author_id', new Ddl\Column\Integer('author_id', true, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $fkName,
        'author_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($alterStmt);

//
// DISCUSSION POST
//
$fkName = $this->getConnection()->getForeignKeyName(Db\Discussion\Post::MAIN_TABLE, 'author_id', Db\Geefter::MAIN_TABLE, 'geefter_id');
$this->dropForeignKey(Db\Discussion\Post::MAIN_TABLE, $fkName);

$alterStmt = new Ddl\AlterTable(Db\Discussion\Post::MAIN_TABLE);
$alterStmt->changeColumn('author_id', new Ddl\Column\Integer('author_id', true, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $fkName,
        'author_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($alterStmt);
