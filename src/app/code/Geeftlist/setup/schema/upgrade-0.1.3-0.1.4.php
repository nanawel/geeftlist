<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Gift::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar('status', 16, false, \Geeftlist\Model\Gift\Field\Status::AVAILABLE));
$this->getConnection()->query($alterStmt);
