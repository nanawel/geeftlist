<?php

use Geeftlist\Model\ResourceModel\Db;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Gift;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// CONFIG
//
try {
    $dropStmt = new Ddl\DropTable(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// RESERVATION
//
try {
    $dropStmt = new Ddl\DropTable(Db\Reservation::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GEEFTEE-GIFT
//
try {
    $dropStmt = new Ddl\DropTable(Gift::GEEFTEE_GIFT_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// FAMILY-GEEFTEE
//
try {
    $dropStmt = new Ddl\DropTable(Geeftee::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// MEMBERSHIP_REQUEST
//
try {
    $dropStmt = new Ddl\DropTable(Db\Family\MembershipRequest::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GEEFTEE CLAIM REQUEST
//
try {
    $dropStmt = new Ddl\DropTable(Db\Geeftee\ClaimRequest::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// FAMILY
//
try {
    $dropStmt = new Ddl\DropTable(Db\Family::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GEEFTEE
//
try {
    $dropStmt = new Ddl\DropTable(Db\Geeftee::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GIFT THING
//
try {
    $dropStmt = new Ddl\DropTable(Db\Setup\Constants::DEPRECATED_TABLE_GIFT_THING);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// THING
//
try {
    $dropStmt = new Ddl\DropTable(Db\Setup\Constants::DEPRECATED_TABLE_THING);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GIFT
//
try {
    $dropStmt = new Ddl\DropTable(Db\Gift::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GEEFTER
//
try {
    $dropStmt = new Ddl\DropTable(Db\Geefter::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// CMS BLOCK
//
try {
    $dropStmt = new Ddl\DropTable(Db\Cms\Block::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
