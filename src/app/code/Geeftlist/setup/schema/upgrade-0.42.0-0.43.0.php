<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/install/feature-notification-category.php';
include __DIR__ . '/install/feature-discussion-post-direct-recipients.php';
