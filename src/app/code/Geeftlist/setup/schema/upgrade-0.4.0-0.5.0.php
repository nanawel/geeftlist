<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// ACTIVITY
//
$createStmt = new Ddl\CreateTable(Db\Activity::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('activity_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('actor_type', 32, true))
    ->addColumn(new Ddl\Column\Integer('actor_id', true, 0))
    ->addColumn(new Ddl\Column\Varchar('object_type', 32, true))
    ->addColumn(new Ddl\Column\Integer('object_id', true, 0))
    ->addColumn(new Ddl\Column\Varchar('action', 32, false))
    ->addColumn(new Ddl\Column\Text('payload', 1024, true))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('activity_id'))
    ->addConstraint(new Ddl\Index\Index('actor_type', $this->getConnection()->getIndexName(Db\Activity::MAIN_TABLE, 'actor_type')))
    ->addConstraint(new Ddl\Index\Index('actor_id', $this->getConnection()->getIndexName(Db\Activity::MAIN_TABLE, 'actor_id')))
    ->addConstraint(new Ddl\Index\Index('object_type', $this->getConnection()->getIndexName(Db\Activity::MAIN_TABLE, 'object_type')))
    ->addConstraint(new Ddl\Index\Index('object_id', $this->getConnection()->getIndexName(Db\Activity::MAIN_TABLE, 'object_id')))
    ->addConstraint(new Ddl\Index\Index('action', $this->getConnection()->getIndexName(Db\Activity::MAIN_TABLE, 'action')))
    ->addConstraint(new Ddl\Index\Index('created_at', $this->getConnection()->getIndexName(Db\Activity::MAIN_TABLE, 'created_at')));
$this->getConnection()->query($createStmt);
