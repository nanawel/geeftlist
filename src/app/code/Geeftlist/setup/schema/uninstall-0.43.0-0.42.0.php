<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/uninstall/feature-notification-category.php';
include __DIR__ . '/uninstall/feature-discussion-post-direct-recipients.php';
