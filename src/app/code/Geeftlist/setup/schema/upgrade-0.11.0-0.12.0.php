<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

// Fix setup config version
$stmt = $this->getConnection()->insert(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG)
    ->values([
        'path'  => 'app/version/data',
        'value' => '0.11.0'
    ]);
$this->getConnection()->query($stmt);
