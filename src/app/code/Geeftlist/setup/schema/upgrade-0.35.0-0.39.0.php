<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/install/fix-database-charset-utf8mb4-again.php';

include __DIR__ . '/install/feature-gift-lists.php';
include __DIR__ . '/install/fix-remove-thing-tables.php';
