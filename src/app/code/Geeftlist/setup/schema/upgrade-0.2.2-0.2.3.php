<?php

use Geeftlist\Model\ResourceModel\Db;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Gift::GEEFTEE_GIFT_TABLE);
$alterStmt->addConstraint(
    new Ddl\Index\Index(
        'geeftee_id',
        $this->getConnection()->getForeignKeyName(Db\Gift::GEEFTEE_GIFT_TABLE, 'geeftee_id', Db\Geeftee::MAIN_TABLE, 'geeftee_id')
    )
);
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Geeftee::MAIN_TABLE);
$alterStmt->addConstraint(
    new Ddl\Index\Index(
        'family_id',
        $this->getConnection()->getForeignKeyName(Geeftee::MAIN_TABLE, 'family_id', Db\Family::MAIN_TABLE, 'family_id')
    )
);
$this->getConnection()->query($alterStmt);
