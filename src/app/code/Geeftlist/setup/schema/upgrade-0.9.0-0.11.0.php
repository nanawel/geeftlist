<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/install/feature-admin-notifications.php';
include __DIR__ . '/install/feature-update-geefter-email.php';
