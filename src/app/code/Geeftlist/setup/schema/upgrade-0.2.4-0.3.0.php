<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// TOPIC
//
$createStmt = new Ddl\CreateTable(Db\Discussion\Topic::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('topic_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('title', 255, true))
    ->addColumn(new Ddl\Column\Varchar('status', 16, false, \Geeftlist\Model\Discussion\Topic::STATUS_OPEN))
    ->addColumn(new Ddl\Column\Integer('is_pinned', false, 0))
    ->addColumn(new Ddl\Column\Integer('author_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('entity_type', 32, true))
    ->addColumn(new Ddl\Column\Integer('entity_id', true, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Timestamp('updated_at', false))
    ->addColumn(new Ddl\Column\Datetime('last_activity_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('topic_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Discussion\Topic::MAIN_TABLE, 'author_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'author_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index(
        'last_activity_at',
        $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'last_activity_at')
    ))
    ->addConstraint(new Ddl\Index\Index('author_id', $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'author_id')))
    ->addConstraint(new Ddl\Index\Index('status', $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'status')))
    ->addConstraint(new Ddl\Index\Index('created_at', $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'created_at')))
    ->addConstraint(new Ddl\Index\Index('updated_at', $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'updated_at')));
$this->getConnection()->query($createStmt);

//
// TOPIC METADATA
//
$createStmt = new Ddl\CreateTable(Db\Discussion\Topic::METADATA_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('topic_metadata_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Integer('topic_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('key', 255, true))
    ->addColumn(new Ddl\Column\Varchar('value', 255, true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('topic_metadata_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Discussion\Topic::METADATA_TABLE, 'topic_id', Db\Discussion\Topic::MAIN_TABLE, 'topic_id'),
        'topic_id',
        Db\Discussion\Topic::MAIN_TABLE,
        'topic_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\UniqueKey(['topic_id', 'key']))
    ->addConstraint(new Ddl\Index\Index(
        'key',
        $this->getConnection()->getIndexName(Db\Discussion\Topic::METADATA_TABLE, 'key')
    ));
$this->getConnection()->query($createStmt);

//
// POST
//
$createStmt = new Ddl\CreateTable(Db\Discussion\Post::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('post_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Integer('topic_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('title', 255, false))
    ->addColumn(new Ddl\Column\Varchar('status', 16, false, \Geeftlist\Model\Discussion\Post::STATUS_NORMAL))
    ->addColumn(new Ddl\Column\Integer('is_pinned', false, 0))
    ->addColumn(new Ddl\Column\Integer('author_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Text('message'))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Timestamp('updated_at', false))
    ->addColumn(new Ddl\Column\Datetime('last_edition_at', true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('post_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Discussion\Post::MAIN_TABLE, 'topic_id', Db\Discussion\Topic::MAIN_TABLE, 'topic_id'),
        'topic_id',
        Db\Discussion\Topic::MAIN_TABLE,
        'topic_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Discussion\Post::MAIN_TABLE, 'author_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'author_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index('status', $this->getConnection()->getIndexName(Db\Discussion\Post::MAIN_TABLE, 'status')))
    ->addConstraint(new Ddl\Index\Index('author_id', $this->getConnection()->getIndexName(Db\Discussion\Post::MAIN_TABLE, 'author_id')))
    ->addConstraint(new Ddl\Index\Index('created_at', $this->getConnection()->getIndexName(Db\Discussion\Post::MAIN_TABLE, 'created_at')))
    ->addConstraint(new Ddl\Index\Index('updated_at', $this->getConnection()->getIndexName(Db\Discussion\Post::MAIN_TABLE, 'updated_at')));
$this->getConnection()->query($createStmt);
