<?php

use Geeftlist\Model\ResourceModel\Db\Gift;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Gift::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Integer('priority', false, 0));
$this->getConnection()->query($alterStmt);
