<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

// ENTITY SHARE RULE
$createStmt = new Ddl\CreateTable(Db\Share\Entity\Rule::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('share_entity_rule_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('target_entity_type', 32, true))
    ->addColumn(new Ddl\Column\Integer('target_entity_id', true, 0))
    ->addColumn(new Ddl\Column\Varchar('rule_type', 32, false))
    ->addColumn(new Ddl\Column\Text('params', 1024, true))
    ->addColumn(new Ddl\Column\Integer('priority', false, 0))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('share_entity_rule_id'))
    ->addConstraint(new Ddl\Index\Index(
        ['target_entity_type', 'target_entity_id'],
        $this->getConnection()->getIndexName(Db\Share\Entity\Rule::MAIN_TABLE, ['target_entity_type', 'target_entity_id'])
    ))
    ->addConstraint(new Ddl\Index\Index('target_entity_id', $this->getConnection()->getIndexName(Db\Share\Entity\Rule::MAIN_TABLE, 'target_entity_id')))
    ->addConstraint(new Ddl\Index\Index('rule_type', $this->getConnection()->getIndexName(Db\Share\Entity\Rule::MAIN_TABLE, 'rule_type')))
    ->addConstraint(new Ddl\Index\Index('created_at', $this->getConnection()->getIndexName(Db\Share\Entity\Rule::MAIN_TABLE, 'created_at')));
$this->getConnection()->query($createStmt);

// GEEFTER-GIFT ACCESS INDEX
$createStmt = new Ddl\CreateTable(Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Varchar('entity_type', 32, true))
    ->addColumn(new Ddl\Column\Integer('entity_id', true, 0))
    ->addColumn(new Ddl\Column\Integer('geefter_id', true, 0))
    ->addColumn(new Ddl\Column\Varchar('action_type', 32, false))
    ->addColumn(new Ddl\Column\Binary('is_allowed', 1, false))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['entity_type', 'entity_id', 'geefter_id', 'action_type']))
    ->addConstraint(new Ddl\Index\Index(
        ['entity_type', 'entity_id'],
        $this->getConnection()->getIndexName(Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE, ['entity_type', 'entity_id'])
    ))
    ->addConstraint(new Ddl\Index\Index('entity_id', $this->getConnection()->getIndexName(Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE, 'entity_id')))
    ->addConstraint(new Ddl\Index\Index('action_type', $this->getConnection()->getIndexName(Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE, 'action_type')))
    ->addConstraint(new Ddl\Index\Index('is_allowed', $this->getConnection()->getIndexName(Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE, 'is_allowed')))
    ->addConstraint(new Ddl\Constraint\UniqueKey(['entity_type', 'entity_id', 'geefter_id', 'action_type']));
$this->getConnection()->query($createStmt);


// GEEFTER-DISCUSSION-TOPIC ACCESS INDEX
$createStmt = new Ddl\CreateTable(Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Varchar('entity_type', 32, true))
    ->addColumn(new Ddl\Column\Integer('entity_id', true, 0))
    ->addColumn(new Ddl\Column\Integer('geefter_id', true, 0))
    ->addColumn(new Ddl\Column\Varchar('action_type', 32, false))
    ->addColumn(new Ddl\Column\Binary('is_allowed', 1, false))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['entity_type', 'entity_id', 'geefter_id', 'action_type']))
    ->addConstraint(new Ddl\Index\Index(
        ['entity_type', 'entity_id'],
        $this->getConnection()->getIndexName(Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE, ['entity_type', 'entity_id'])
    ))
    ->addConstraint(new Ddl\Index\Index('entity_id', $this->getConnection()->getIndexName(Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE, 'entity_id')))
    ->addConstraint(new Ddl\Index\Index('action_type', $this->getConnection()->getIndexName(Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE, 'action_type')))
    ->addConstraint(new Ddl\Index\Index('is_allowed', $this->getConnection()->getIndexName(Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE, 'is_allowed')))
    ->addConstraint(new Ddl\Constraint\UniqueKey(['entity_type', 'entity_id', 'geefter_id', 'action_type']));
$this->getConnection()->query($createStmt);
