<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GEEFTER
//
$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar(
    'pending_email',
    255,
    true
))->addColumn(new Ddl\Column\Varchar(
    'pending_email_token',
    255,
    true
));
$this->getConnection()->query($alterStmt);
