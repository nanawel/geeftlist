<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$dbName = $this->getAppConfig()->getValue('RESOURCE_CONFIG.db.database');

$columnsToConvert = [
    'activity' => ['payload'],
    'badge_t9n' => ['label', 'description'],
    'cms_block' => ['content'],
    'config' => ['value'],
    'discussion_post' => ['title', 'message'],
    'discussion_topic' => ['title'],
    'family' => ['name'],
    'gift' => ['label', 'description'],
    'notification' => ['event_params'],
    'thing' => ['description'],
];

$selectColumnSpecQuery = <<<"EOSQL"
    SELECT column_name, column_type, column_default, is_nullable FROM information_schema.columns
    WHERE table_schema = "{$dbName}"
        AND table_name = "%s"
        AND column_name IN (%s);
EOSQL;

$alterColumnQuery = static fn(string $table, string $column, array $columnData): string => sprintf(
    'ALTER TABLE %s MODIFY %s %s CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci %s %s',
    $table,
    $column,
    $columnData['column_type'],
    $columnData['is_nullable'] ? '' : 'NOT NULL',
    $columnData['is_nullable'] || $columnData['column_default']
        ? sprintf(
            'DEFAULT %s',
            $columnData['column_default'] === null
            ? 'NULL'
            : sprintf('"%s"', $columnData['column_default'])
        )
        : ''
);

foreach ($columnsToConvert as $table => $columns) {
    $result = $this->getConnection()->getAdapter()->query(
        sprintf(
            $selectColumnSpecQuery,
            $table,
            implode(', ', array_map(static fn($t): string => sprintf('"%s"', $t), $columns))
        ),
        \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE
    );

    foreach ($result as $columnData) {
        $this->getConnection()->getAdapter()->query(
            $alterColumnQuery($table, $columnData['column_name'], (array) $columnData),
            \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE
        );
    }
}
