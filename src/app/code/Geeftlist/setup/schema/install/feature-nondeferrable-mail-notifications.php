<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// NOTIFICATION
//
$alterStmt = new Ddl\AlterTable(Db\Notification::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Boolean(
    'deferrable',
    false,
    true,
    ['after' => 'event_params']
));
$this->getConnection()->query($alterStmt);
