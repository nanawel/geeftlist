<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GIFT
//
$alterStmt = new Ddl\AlterTable(Db\Gift::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar(
    'image_id',
    128,
    true
));
$this->getConnection()->query($alterStmt);
