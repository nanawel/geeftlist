<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

try {
    $alterStmt = new Ddl\DropTable(Db\Setup\Constants::DEPRECATED_TABLE_GIFT_THING);
    $this->getConnection()->query($alterStmt);
}
catch (Exception $exception) {
    // just ignore
}

try {
    $alterStmt = new Ddl\DropTable(Db\Setup\Constants::DEPRECATED_TABLE_THING);
    $this->getConnection()->query($alterStmt);
}
catch (Exception) {
    // just ignore
}
