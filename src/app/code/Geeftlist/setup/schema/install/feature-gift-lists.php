<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GIFTLIST
//
$createStmt = new Ddl\CreateTable(Db\GiftList::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('giftlist_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('name', 255, false))
    ->addColumn(new Ddl\Column\Text('description', 1024, true))
    ->addColumn(new Ddl\Column\Varchar('visibility', 16, false, \Geeftlist\Model\GiftList\Field\Visibility::PRIVATE))
    ->addColumn(new Ddl\Column\Varchar('status', 16, false, \Geeftlist\Model\GiftList\Field\Status::ACTIVE))
    ->addColumn(new Ddl\Column\Integer('owner_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('giftlist_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Db\GiftList::MAIN_TABLE,
            'owner_id',
            Db\Geefter::MAIN_TABLE,
            'geefter_id'
        ),
        'owner_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);

//
// GIFTLIST-GIFT
//
$createStmt = new Ddl\CreateTable(Db\GiftList\Gift::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('giftlist_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('gift_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('added_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['giftlist_id', 'gift_id']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Db\GiftList\Gift::MAIN_TABLE,
            'giftlist_id',
            Db\GiftList::MAIN_TABLE,
            'giftlist_id'
        ),
        'giftlist_id',
        Db\GiftList::MAIN_TABLE,
        'giftlist_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Db\GiftList\Gift::MAIN_TABLE,
            'gift_id',
            Db\Gift::MAIN_TABLE,
            'gift_id'
        ),
        'gift_id',
        Db\Gift::MAIN_TABLE,
        'gift_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index(
        'gift_id',
        $this->getConnection()->getIndexName(Db\GiftList\Gift::MAIN_TABLE, 'gift_id')
    ));
$this->getConnection()->query($createStmt);
