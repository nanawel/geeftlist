<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GEEFTEE
//
$alterStmt = new Ddl\AlterTable(Db\Geeftee::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar(
    'avatar_path',
    255,
    true
));
$this->getConnection()->query($alterStmt);
