<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// INVITATION
//
$createStmt = new Ddl\CreateTable(Db\Invitation::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('invitation_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Integer('creator_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Text('token', null, true))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Datetime('expires_at', false))
    ->addColumn(new Ddl\Column\Datetime('used_at', true))
    ->addColumn(new Ddl\Column\Integer('geefter_id', true, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('invitation_id'));
$this->getConnection()->query($createStmt);
