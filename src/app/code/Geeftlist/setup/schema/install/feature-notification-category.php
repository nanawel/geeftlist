<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// NOTIFICATION
//
$alterStmt = new Ddl\AlterTable(Db\Notification::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar(
    'category',
    64,
    false,
    null,
    ['after' => 'target_id']
));
$this->getConnection()->query($alterStmt);
