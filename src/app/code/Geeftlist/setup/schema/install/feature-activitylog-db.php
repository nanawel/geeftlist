<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// ACTIVITYLOG
//
$createStmt = new Ddl\CreateTable(Db\ActivityLog::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Datetime('logged_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Json('payload', true))
    ->addConstraint(new Ddl\Index\Index(
        'logged_at',
        $this->getConnection()->getIndexName(Db\ActivityLog::MAIN_TABLE, 'logged_at')
    ))
    ->addConstraint(new Ddl\Index\Index(
        'payload',
        $this->getConnection()->getIndexName(Db\ActivityLog::MAIN_TABLE, 'payload')
    ))
;
$this->getConnection()->query($createStmt);
