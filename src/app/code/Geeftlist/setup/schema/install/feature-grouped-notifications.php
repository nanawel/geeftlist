<?php

use Geeftlist\Model\ResourceModel\Db;
use Geeftlist\Model\ResourceModel\Db\Notification\Geefter;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// NOTIFICATION
//
$createStmt = new Ddl\CreateTable(Db\Notification::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('notification_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('type', 32, false))
    ->addColumn(new Ddl\Column\Varchar('target_type', 127, false))
    ->addColumn(new Ddl\Column\Integer('target_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('event_name', 80, true))
    ->addColumn(new Ddl\Column\Text('event_params'))
    ->addColumn(new Ddl\Column\Varchar('status', 32, false, \Geeftlist\Model\Notification::STATUS_CREATED))
    ->addColumn(new Ddl\Column\Varchar('message', 255, true))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Timestamp('updated_at', false))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('notification_id'))
    ->addConstraint(new Ddl\Index\Index(
        'status',
        $this->getConnection()->getIndexName(Db\Notification::MAIN_TABLE, 'status')
    ))
    ->addConstraint(new Ddl\Index\Index(
        'type',
        $this->getConnection()->getIndexName(Db\Notification::MAIN_TABLE, 'type')
    ))
    ->addConstraint(new Ddl\Index\Index(
        'target_type',
        $this->getConnection()->getIndexName(Db\Notification::MAIN_TABLE, 'target_type')
    ))
    ->addConstraint(new Ddl\Index\Index(
        'target_id',
        $this->getConnection()->getIndexName(Db\Notification::MAIN_TABLE, 'target_id')
    ))
    ->addConstraint(new Ddl\Index\Index(
        'event_name',
        $this->getConnection()->getIndexName(Db\Notification::MAIN_TABLE, 'event_name')
    ));
$this->getConnection()->query($createStmt);

//
// NOTIFICATION-GEEFTER
//
$createStmt = new Ddl\CreateTable(Geefter::NOTIFICATION_GEEFTER_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('notification_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('geefter_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('status', 32, false, \Geeftlist\Model\Notification::STATUS_CREATED))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['notification_id', 'geefter_id']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Geefter::NOTIFICATION_GEEFTER_TABLE,
            'notification_id',
            Db\Notification::MAIN_TABLE,
            'notification_id'
        ),
        'notification_id',
        Db\Notification::MAIN_TABLE,
        'notification_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Geefter::NOTIFICATION_GEEFTER_TABLE,
            'geefter_id',
            Db\Geefter::MAIN_TABLE,
            'geefter_id'
        ),
        'geefter_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index(
        'status',
        $this->getConnection()->getIndexName(Geefter::NOTIFICATION_GEEFTER_TABLE, 'status')
    ));
$this->getConnection()->query($createStmt);
