<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GEEFTER
//
$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar(
    'email_notification_freq',
    32,
    false,
    \Geeftlist\Model\MailNotification\Constants::SENDING_FREQ_REALTIME,
    ['after' => 'pending_email_token']
))->addColumn(new Ddl\Column\Datetime(
    'email_notification_last',
    true,   // Fix #547
    null,
    ['after' => 'email_notification_freq']
));
$this->getConnection()->query($alterStmt);
