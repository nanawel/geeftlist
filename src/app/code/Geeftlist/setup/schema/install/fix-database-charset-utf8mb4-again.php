<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$dbName = $this->getAppConfig()->getValue('RESOURCE_CONFIG.db.database');

// Missing correct encoding on the *database* (was only fixed on *tables*, see fix-database-charset-utf8mb4.php)
$this->getConnection()->getAdapter()->query(
    sprintf('ALTER DATABASE %s CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;', $dbName),
    \Laminas\Db\Adapter\Adapter::QUERY_MODE_EXECUTE
);
