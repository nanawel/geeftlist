<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// NOTIFICATION
//
$alterStmt = new Ddl\AlterTable(Db\Notification::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar(
    'recipient_type',
    32,
    false,
    \Geeftlist\Model\Notification::RECIPIENT_TYPE_GEEFTER,
    ['after' => 'type']
));
$this->getConnection()->query($alterStmt);
