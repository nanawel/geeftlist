<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// ACTIVITYLOG
//
$alterStmt = new Ddl\AlterTable(Db\ActivityLog::MAIN_TABLE);
$alterStmt->addColumn(
    new Ddl\Column\BigInteger(
        'activitylog_id',
        false,
        null,
        [
            'unsigned' => true,
            'autoincrement' => true,
            'first' => true // Not supported by Laminas, so meh :\
        ]
    )
)
    ->addConstraint(new Ddl\Constraint\PrimaryKey('activitylog_id'))
;
$this->getConnection()->query($alterStmt);
