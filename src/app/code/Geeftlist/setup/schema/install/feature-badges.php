<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// BADGE
//
$createStmt = new Ddl\CreateTable(Db\Badge::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('badge_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('type', 32, false))
    ->addColumn(new Ddl\Column\Varchar('code', 32, false))
    ->addColumn(new Ddl\Column\Varchar('icon_type', 16, true))
    ->addColumn(new Ddl\Column\Text('icon_data', null, true))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('badge_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey(
        'code',
        $this->getConnection()->getIndexName(Db\Badge::MAIN_TABLE, 'code')
    ));
$this->getConnection()->query($createStmt);

//
// BADGE-ENTITY TYPE
//
$createStmt = new Ddl\CreateTable(Db\Badge::BADGE_ENTITY_TYPE_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('badge_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('entity_type', 32, false))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['badge_id', 'entity_type']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Db\Badge::BADGE_ENTITY_TYPE_TABLE,
            'badge_id',
            Db\Badge::MAIN_TABLE,
            'badge_id'
        ),
        'badge_id',
        Db\Badge::MAIN_TABLE,
        'badge_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index(
        'entity_type',
        $this->getConnection()->getIndexName(Db\Badge::BADGE_ENTITY_TYPE_TABLE, 'entity_type')
    ));
$this->getConnection()->query($createStmt);

//
// BADGE-L10N
//
$createStmt = new Ddl\CreateTable(Db\Badge::BADGE_T9N_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('badge_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('language', 8, false))
    ->addColumn(new Ddl\Column\Varchar('label', 64, false))
    ->addColumn(new Ddl\Column\Varchar('description', 255, true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['badge_id', 'language']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Db\Badge::BADGE_T9N_TABLE,
            'badge_id',
            Db\Badge::MAIN_TABLE,
            'badge_id'
        ),
        'badge_id',
        Db\Badge::MAIN_TABLE,
        'badge_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index(
        'language',
        $this->getConnection()->getIndexName(Db\Badge::BADGE_T9N_TABLE, 'language')
    ));
$this->getConnection()->query($createStmt);

//
// BADGE-ENTITY
//
$createStmt = new Ddl\CreateTable(Db\Badge\Entity::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('badge_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Varchar('entity_type', 32, false))
    ->addColumn(new Ddl\Column\Integer('entity_id', false, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['badge_id', 'entity_type', 'entity_id']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(
            Db\Badge\Entity::MAIN_TABLE,
            'badge_id',
            Db\Badge::MAIN_TABLE,
            'badge_id'
        ),
        'badge_id',
        Db\Badge::MAIN_TABLE,
        'badge_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Index\Index(
        'entity_type',
        $this->getConnection()->getIndexName(Db\Badge\Entity::MAIN_TABLE, 'entity_type')
    ))
    ->addConstraint(new Ddl\Index\Index(
        'entity_id',
        $this->getConnection()->getIndexName(Db\Badge\Entity::MAIN_TABLE, 'entity_id')
    ));
$this->getConnection()->query($createStmt);
