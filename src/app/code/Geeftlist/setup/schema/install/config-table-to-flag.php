<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// FLAG
//
$createStmt = new Ddl\CreateTable(Db\Flag::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Varchar('name', 255, false))
    ->addColumn(new Ddl\Column\Text('value', null, true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('name'));
$this->getConnection()->query($createStmt);

// Move current values to their new location
$insert = $this->getConnection()->insert(Db\Flag::MAIN_TABLE);
$select = $this->getConnection()->select(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG)
    ->columns(['name' => 'path', 'value' => 'value']);
$select->where->in('path', [
    'app/version/schema',
    'app/version/data',
    'app/version/sampledata',
    'cron/last_success',
    'cron/last_success_duration',
    'cron/last_run',
]);
$insert->select($select)
    ->columns(['name', 'value']);
$this->getConnection()->query($insert);

// Copy deprecated values for compatibility only
$insert = $this->getConnection()->insert(Db\Flag::MAIN_TABLE);
$select = $this->getConnection()->select(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG)
    ->columns([
        'name' => new \Laminas\Db\Sql\Expression('CONCAT("compat/", path)'),
        'value' => 'value'
    ]);
$select->where->in('path', [
    'api/authentication/jwt/secret_key',
    'session/geefter/secret_key',
]);
$insert->select($select)
    ->columns(['name', 'value']);
$this->getConnection()->query($insert);

$dropStmt = new Ddl\DropTable(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$this->getConnection()->query($dropStmt);
