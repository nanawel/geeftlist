<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GEEFTER
//
$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->changeColumn(
    'email_notification_last',
    new Ddl\Column\Datetime(
        'email_notification_last',
        true,
        null,
        ['after' => 'email_notification_freq']
    )
);
$this->getConnection()->query($alterStmt);

//
// GEEFTEE CLAIM REQUEST
//
// There should *not* be a FK here, as the target geeftee is likely to be eventually merged and so deleted
$dropStmt = new Ddl\AlterTable(Db\Geeftee\ClaimRequest::MAIN_TABLE);
$dropStmt->dropConstraint(new Ddl\Constraint\ForeignKey(
    $this->getConnection()->getForeignKeyName(
        Db\Geeftee\ClaimRequest::MAIN_TABLE,
        'geeftee_id',
        Db\Geeftee::MAIN_TABLE,
        'geeftee_id'
    ),
    'geeftee_id',
    Db\Geeftee::MAIN_TABLE,
    'geeftee_id'
));
try {
    $this->getConnection()->query($dropStmt);
} catch (PDOException $e) {
    if ($e->getCode() != 42000) {
        throw $e;
    }
    // ignore otherwise, the FK simply does not exist
}

//
// FAMILY MEMBERSHIP REQUEST
//
$alterStmt = new Ddl\AlterTable(Db\Family\MembershipRequest::MAIN_TABLE);
$alterStmt->changeColumn(
    'decision_date',
    new Ddl\Column\Datetime(
        'decision_date',
        true,
        null,
        ['after' => 'decision_code']
    )
);
$this->getConnection()->query($alterStmt);

//
// CMS BLOCK
//
$alterStmt = new Ddl\AlterTable(Db\Cms\Block::MAIN_TABLE);
$alterStmt->changeColumn(
    'content_type',
    new Ddl\Column\Varchar(
        'content_type',
        64,
        false,
        'text/plain',
        ['after' => 'title']
    )
)->changeColumn(
    'language',
    new Ddl\Column\Varchar(
        'language',
        32,
        true,
        null,
        ['after' => 'enabled']
    )
);
$this->getConnection()->query($alterStmt);
