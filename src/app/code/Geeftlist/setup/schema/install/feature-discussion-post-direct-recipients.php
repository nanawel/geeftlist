<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// DISCUSSION POST
//
$alterStmt = new Ddl\AlterTable(Db\Discussion\Post::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Text(
    'recipient_ids',
    null,
    true,
    null,
    ['after' => 'message']
));
$this->getConnection()->query($alterStmt);
