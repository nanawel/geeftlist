<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\GiftList::MAIN_TABLE);
$alterStmt
    ->addConstraint(new Ddl\Constraint\UniqueKey(
        ['owner_id', 'name'],
        $this->getConnection()->getIndexName(
            Db\GiftList::MAIN_TABLE,
            ['owner_id', 'name'],
            'UNIQ'
        )
    ));
$this->getConnection()->query($alterStmt);
