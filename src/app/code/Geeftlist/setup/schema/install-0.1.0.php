<?php

use Geeftlist\Model\ResourceModel\Db;
use Geeftlist\Model\ResourceModel\Db\Family\Geeftee;
use Geeftlist\Model\ResourceModel\Db\Gift;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// CONFIG
//
$createStmt = new Ddl\CreateTable(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$createStmt
    ->addColumn(new Ddl\Column\Integer('config_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('path', 255, false))
    ->addColumn(new Ddl\Column\Varchar('type', 32, true))
    ->addColumn(new Ddl\Column\Text('value', null, true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('config_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey('path'));
$this->getConnection()->query($createStmt);

//
// GEEFTER
//
$createStmt = new Ddl\CreateTable(Db\Geefter::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('geefter_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('email', 255, false))
    ->addColumn(new Ddl\Column\Varchar('username', 127, false))
    ->addColumn(new Ddl\Column\Varchar('password_hash', 80, true))
    ->addColumn(new Ddl\Column\Varchar('password_token', 80, true))
    ->addColumn(new Ddl\Column\Datetime('password_token_date', true))
    ->addColumn(new Ddl\Column\Datetime('last_login_at', true))
    ->addColumn(new Ddl\Column\Integer('login_failure_count', true, 0))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('geefter_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey('email'))
    ->addConstraint(new Ddl\Constraint\UniqueKey('username'));
$this->getConnection()->query($createStmt);

//
// GEEFTEE
//
$createStmt = new Ddl\CreateTable(Db\Geeftee::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('geeftee_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('email', 255, true))
    ->addColumn(new Ddl\Column\Varchar('name', 127, false))
    ->addColumn(new Ddl\Column\Integer('creator_id', true, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Integer('geefter_id', true, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('geeftee_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey('email'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Geeftee::MAIN_TABLE, 'creator_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'creator_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Geeftee::MAIN_TABLE, 'geefter_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'geefter_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);

//
// FAMILY
//
$createStmt = new Ddl\CreateTable(Db\Family::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('family_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('name', 127, false))
    ->addColumn(new Ddl\Column\Integer('owner_id', true, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('family_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey('name'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Family::MAIN_TABLE, 'owner_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'owner_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);

//
// FAMILY-GEEFTEE
//
$createStmt = new Ddl\CreateTable(Geeftee::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('family_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('geeftee_id', false, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['family_id', 'geeftee_id']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Geeftee::MAIN_TABLE, 'family_id', Db\Family::MAIN_TABLE, 'family_id'),
        'family_id',
        Db\Family::MAIN_TABLE,
        'family_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Geeftee::MAIN_TABLE, 'geeftee_id', Db\Geeftee::MAIN_TABLE, 'geeftee_id'),
        'geeftee_id',
        Db\Geeftee::MAIN_TABLE,
        'geeftee_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);

//
// GIFT
//
$createStmt = new Ddl\CreateTable(Db\Gift::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('gift_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('label', 255, false))
    ->addColumn(new Ddl\Column\Text('description', 1024, true))
    ->addColumn(new Ddl\Column\Floating('estimated_price', null, null, true))
    ->addColumn(new Ddl\Column\Integer('creator_id', true, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('gift_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Gift::MAIN_TABLE, 'creator_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'creator_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_SET_NULL,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);

//
// GEEFTEE-GIFT
//
$createStmt = new Ddl\CreateTable(Gift::GEEFTEE_GIFT_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('geeftee_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('gift_id', false, null, ['unsigned' => true]))
    ->addConstraint(new Ddl\Constraint\PrimaryKey(['geeftee_id', 'gift_id']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Gift::GEEFTEE_GIFT_TABLE, 'geeftee_id', Db\Geeftee::MAIN_TABLE, 'geeftee_id'),
        'geeftee_id',
        Db\Geeftee::MAIN_TABLE,
        'geeftee_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Gift::GEEFTEE_GIFT_TABLE, 'gift_id', Db\Gift::MAIN_TABLE, 'gift_id'),
        'gift_id',
        Db\Gift::MAIN_TABLE,
        'gift_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);

//
// RESERVATION
//
$createStmt = new Ddl\CreateTable(Db\Reservation::MAIN_TABLE);
$createStmt
    ->addColumn(new Ddl\Column\Integer('reservation_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('type', 255, false))
    ->addColumn(new Ddl\Column\Integer('geefter_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('gift_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Floating('amount', null, null, true))
    ->addColumn(new Ddl\Column\Datetime('reserved_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('reservation_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey(['geefter_id', 'gift_id']))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Reservation::MAIN_TABLE, 'geefter_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'geefter_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Reservation::MAIN_TABLE, 'gift_id', Db\Gift::MAIN_TABLE, 'gift_id'),
        'gift_id',
        Db\Gift::MAIN_TABLE,
        'gift_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ));
$this->getConnection()->query($createStmt);
