<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$createStmt = new Ddl\CreateTable(Db\Cms\Block::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('block_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('code', 255, true))
    ->addColumn(new Ddl\Column\Varchar('title', 255, true))
    ->addColumn(new Ddl\Column\Varchar('content_type', 64, false, 'text/plain'))  // Fix #547
    ->addColumn(new Ddl\Column\Integer('enabled', false, 1, ['length' => 1]))
    ->addColumn(new Ddl\Column\Varchar('language', 32, false))
    ->addColumn(new Ddl\Column\Text('content'))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Timestamp('updated_at', false))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('block_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey(['code', 'language']))
    ->addConstraint(new Ddl\Index\Index('code', $this->getConnection()->getIndexName(Db\Cms\Block::MAIN_TABLE, 'code')))
    ->addConstraint(new Ddl\Index\Index('language', $this->getConnection()->getIndexName(Db\Cms\Block::MAIN_TABLE, 'language')))
    ->addConstraint(new Ddl\Index\Index('created_at', $this->getConnection()->getIndexName(Db\Cms\Block::MAIN_TABLE, 'created_at')));
$this->getConnection()->query($createStmt);
