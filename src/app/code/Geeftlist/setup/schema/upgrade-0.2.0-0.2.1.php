<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Geeftee::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Date('dob', true));
$this->getConnection()->query($alterStmt);
