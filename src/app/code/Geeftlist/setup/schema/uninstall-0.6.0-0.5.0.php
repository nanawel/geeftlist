<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::MAIN_TABLE);
$alterStmt->changeColumn('linked_entity_type', new Ddl\Column\Varchar('entity_type', 32, true));
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->changeColumn('password_hash', new Ddl\Column\Varchar('password_hash', 80, true))
    ->changeColumn('password_token', new Ddl\Column\Varchar('password_token', 80, true));
$this->getConnection()->query($alterStmt);

// Config table: Do not revert column drop
