<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$createStmt = new Ddl\CreateTable(Db\Geeftee\ClaimRequest::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('claim_request_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Integer('geeftee_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('geefter_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Varchar('decision_code', 32, null, \Geeftlist\Model\Geeftee\ClaimRequest::DECISION_CODE_PENDING))
    ->addColumn(new Ddl\Column\Datetime('decision_date', true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('claim_request_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Geeftee\ClaimRequest::MAIN_TABLE, 'geeftee_id', Db\Geeftee::MAIN_TABLE, 'geeftee_id'),
        'geeftee_id',
        Db\Geeftee::MAIN_TABLE,
        'geeftee_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Geeftee\ClaimRequest::MAIN_TABLE, 'geefter_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'geefter_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(
        new Ddl\Index\Index(
            'decision_code',
            $this->getConnection()->getIndexName(Db\Geeftee\ClaimRequest::MAIN_TABLE, 'decision_code')
        )
    )
    ->addConstraint(
        new Ddl\Index\Index(
            'created_at',
            $this->getConnection()->getIndexName(Db\Geeftee\ClaimRequest::MAIN_TABLE, 'created_at')
        )
    );
$this->getConnection()->query($createStmt);
