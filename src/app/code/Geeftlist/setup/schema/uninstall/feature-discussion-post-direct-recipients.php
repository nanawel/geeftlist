<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// DISCUSSION POST
//
try {
    $alterStmt = new Ddl\AlterTable(Db\Discussion\Post::MAIN_TABLE);
    $alterStmt->dropColumn('recipient_ids');
    $this->getConnection()->query($alterStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
