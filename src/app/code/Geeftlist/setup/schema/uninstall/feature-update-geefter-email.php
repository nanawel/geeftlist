<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

try {
    $alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
    $alterStmt->dropColumn('pending_email')
        ->dropColumn('pending_email_token');
    $this->getConnection()->query($alterStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
