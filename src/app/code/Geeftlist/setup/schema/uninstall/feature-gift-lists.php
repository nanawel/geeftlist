<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// GIFTLIST-GIFT
//
try {
    $dropStmt = new Ddl\DropTable(Db\GiftList\Gift::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
} catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GIFTLIST
//
try {
    $dropStmt = new Ddl\DropTable(Db\GiftList::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
