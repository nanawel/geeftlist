<?php

use Geeftlist\Model\ResourceModel\Db;
use Geeftlist\Model\ResourceModel\Db\Notification\Geefter;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// NOTIFICATION-GEEFTER
//
try {
    $dropStmt = new Ddl\DropTable(Geefter::NOTIFICATION_GEEFTER_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// NOTIFICATION
//
try {
    $dropStmt = new Ddl\DropTable(Db\Notification::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
