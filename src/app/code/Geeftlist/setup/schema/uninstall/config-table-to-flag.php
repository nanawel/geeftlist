<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$createStmt = new Ddl\CreateTable(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$createStmt
    ->addColumn(new Ddl\Column\Integer('config_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Varchar('path', 255, false))
    ->addColumn(new Ddl\Column\Text('value', null, true))
    ->addConstraint(new Ddl\Constraint\PrimaryKey('config_id'))
    ->addConstraint(new Ddl\Constraint\UniqueKey('path'));
$this->getConnection()->query($createStmt);

// Move back values to their old location
$insert = $this->getConnection()->insert(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$select = $this->getConnection()->select(Db\Flag::MAIN_TABLE)
    ->columns(['path' => 'name', 'value' => 'value']);
$select->where->in('name', [
        'app/version/schema',
        'app/version/data',
        'app/version/sampledata',
        'cron/last_success',
        'cron/last_success_duration',
        'cron/last_run',
    ])
;
$insert->select($select)
    ->columns(['path', 'value']);
$this->getConnection()->query($insert);

// Copy back deprecated values
$insert = $this->getConnection()->insert(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$select = $this->getConnection()->select(Db\Flag::MAIN_TABLE)
    ->columns([
        'path' => new \Laminas\Db\Sql\Expression('SUBSTR(name, 8)'),
        'value' => 'value'
    ]);
$select->where->in('name', [
    'compat/api/authentication/jwt/secret_key',
    'compat/session/geefter/secret_key',
]);
$insert->select($select)
    ->columns(['path', 'value']);
$this->getConnection()->query($insert);

$alterStmt = new Ddl\DropTable(Db\Flag::MAIN_TABLE);
$this->getConnection()->query($alterStmt);
