<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

// Can't straightforwardly rollback the added UNIQUE INDEX in upgrade setup because of
// "Cannot drop index 'IDX_UNIQ_GIFTLIST_OWNER_ID_NAME': needed in a foreign key constraint"
// so meh, doing nothing.
