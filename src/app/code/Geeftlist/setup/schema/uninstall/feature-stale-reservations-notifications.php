<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Reservation::MAIN_TABLE);
$alterStmt->dropConstraint(new Ddl\Index\Index(
    'type',
    $this->getConnection()->getIndexName(Db\Reservation::MAIN_TABLE, 'type')
));
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Reservation::MAIN_TABLE);
$alterStmt->dropConstraint(new Ddl\Index\Index(
    'reserved_at',
    $this->getConnection()->getIndexName(Db\Reservation::MAIN_TABLE, 'reserved_at')
));
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Reservation::MAIN_TABLE);
$alterStmt->dropColumn('stale_notification_sent_at');
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->dropColumn('stale_reservation_notification_enabled');
$this->getConnection()->query($alterStmt);
