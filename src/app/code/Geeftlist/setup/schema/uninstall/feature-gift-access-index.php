<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// ENTITY SHARE RULE
//
try {
    $dropStmt = new Ddl\DropTable(Db\Share\Entity\Rule::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GEEFTER-GIFT ACCESS INDEX
//
try {
    $dropStmt = new Ddl\DropTable(Db\Indexer\Gift\GeefterAccessIndexer::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// GEEFTER-DISCUSSION-TOPIC ACCESS INDEX
//
try {
    $dropStmt = new Ddl\DropTable(Db\Indexer\Discussion\Topic\GeefterAccessIndexer::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
