<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// ACTIVITYLOG
//
try {
    $dropStmt = new Ddl\DropTable(Db\ActivityLog::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
} catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
