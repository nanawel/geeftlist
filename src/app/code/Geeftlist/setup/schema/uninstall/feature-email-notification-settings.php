<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

try {
    $alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
    $alterStmt->dropColumn('email_notification_freq')
        ->dropColumn('email_notification_last');
    $this->getConnection()->query($alterStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
