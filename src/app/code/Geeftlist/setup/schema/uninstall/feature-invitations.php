<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// INVITATION
//
try {
    $dropStmt = new Ddl\DropTable(Db\Invitation::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
} catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
