<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// NOTIFICATION
//
try {
    $alterStmt = new Ddl\AlterTable(Db\Notification::MAIN_TABLE);
    $alterStmt->dropColumn('category');
    $this->getConnection()->query($alterStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
