<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// BADGE-ENTITY
//
try {
    $dropStmt = new Ddl\DropTable(Db\Badge\Entity::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// BADGE-L10N
//
try {
    $dropStmt = new Ddl\DropTable(Db\Badge::BADGE_T9N_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// BADGE-ENTITY TYPE
//
try {
    $dropStmt = new Ddl\DropTable(Db\Badge::BADGE_ENTITY_TYPE_TABLE);
    $this->getConnection()->query($dropStmt);
} catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// BADGE
//
try {
    $dropStmt = new Ddl\DropTable(Db\Badge::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
