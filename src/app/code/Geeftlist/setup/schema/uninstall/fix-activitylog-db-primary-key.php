<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// ACTIVITYLOG
//
$alterStmt = new Ddl\AlterTable(Db\ActivityLog::MAIN_TABLE);
$alterStmt->dropColumn('activitylog_id');
$this->getConnection()->query($alterStmt);
