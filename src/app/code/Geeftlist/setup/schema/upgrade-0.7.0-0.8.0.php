<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar('api_key', 128, true));
$this->getConnection()->query($alterStmt);
