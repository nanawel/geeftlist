<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// POST
//
try {
    $dropStmt = new Ddl\DropTable(Db\Discussion\Post::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// TOPIC METADATA
//
try {
    $dropStmt = new Ddl\DropTable(Db\Discussion\Topic::METADATA_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}

//
// TOPIC
//
try {
    $dropStmt = new Ddl\DropTable(Db\Discussion\Topic::MAIN_TABLE);
    $this->getConnection()->query($dropStmt);
}
catch (Exception $exception) {
    $setup->addError($exception->getMessage(), $exception);
}
