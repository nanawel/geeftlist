<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

//
// FAMILY
//
$alterStmt = new Ddl\AlterTable(Db\Family::MAIN_TABLE);
$alterStmt->addColumn(new Ddl\Column\Varchar('visibility', 16, true, \Geeftlist\Model\Family\Field\Visibility::PROTECTED, [
    'after' => 'name'
]));
$this->getConnection()->query($alterStmt);
