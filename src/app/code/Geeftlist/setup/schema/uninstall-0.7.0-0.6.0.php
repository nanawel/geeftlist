<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::METADATA_TABLE);
$alterStmt->dropConstraint(new Ddl\Index\Index(
    'value',
    $this->getConnection()->getIndexName(Db\Discussion\Topic::METADATA_TABLE, 'value')
));
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::MAIN_TABLE);
$alterStmt->dropConstraint(new Ddl\Index\Index(
    ['linked_entity_type', 'linked_entity_id'],
    $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, ['linked_entity_type', 'linked_entity_id'])
))->dropConstraint(new Ddl\Index\Index(
    'linked_entity_id',
    $this->getConnection()->getIndexName(Db\Discussion\Topic::MAIN_TABLE, 'linked_entity_id')
));
$this->getConnection()->query($alterStmt);
