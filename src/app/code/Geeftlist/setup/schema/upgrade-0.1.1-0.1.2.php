<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Metadata\Object\ConstraintKeyObject;
use Laminas\Db\Sql\Ddl;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$createStmt = new Ddl\CreateTable(Db\Family\MembershipRequest::MAIN_TABLE);
$createStmt->addColumn(new Ddl\Column\Integer('membership_request_id', false, null, ['unsigned' => true, 'autoincrement' => true]))
    ->addColumn(new Ddl\Column\Integer('family_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('geeftee_id', false, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Integer('sponsor_id', true, null, ['unsigned' => true]))
    ->addColumn(new Ddl\Column\Datetime('created_at', false, new \Laminas\Db\Sql\Expression('CURRENT_TIMESTAMP')))
    ->addColumn(new Ddl\Column\Varchar('decision_code', 32, null, \Geeftlist\Model\Family\MembershipRequest::DECISION_CODE_PENDING))
    ->addColumn(new Ddl\Column\Datetime('decision_date', true)) // Fix #547
    ->addConstraint(new Ddl\Constraint\PrimaryKey('membership_request_id'))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Family\MembershipRequest::MAIN_TABLE, 'family_id', Db\Family::MAIN_TABLE, 'family_id'),
        'family_id',
        Db\Family::MAIN_TABLE,
        'family_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Family\MembershipRequest::MAIN_TABLE, 'geeftee_id', Db\Geeftee::MAIN_TABLE, 'geeftee_id'),
        'geeftee_id',
        Db\Geeftee::MAIN_TABLE,
        'geeftee_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(new Ddl\Constraint\ForeignKey(
        $this->getConnection()->getForeignKeyName(Db\Family\MembershipRequest::MAIN_TABLE, 'sponsor_id', Db\Geefter::MAIN_TABLE, 'geefter_id'),
        'sponsor_id',
        Db\Geefter::MAIN_TABLE,
        'geefter_id',
        ConstraintKeyObject::FK_CASCADE,
        ConstraintKeyObject::FK_CASCADE
    ))
    ->addConstraint(
        new Ddl\Index\Index(
            'decision_code',
            $this->getConnection()->getIndexName(Db\Family\MembershipRequest::MAIN_TABLE, 'decision_code')
        )
    )
    ->addConstraint(
        new Ddl\Index\Index(
            'created_at',
            $this->getConnection()->getIndexName(Db\Family\MembershipRequest::MAIN_TABLE, 'created_at')
        )
    );
$this->getConnection()->query($createStmt);
