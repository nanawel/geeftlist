<?php

use Geeftlist\Model\ResourceModel\Db;
use Laminas\Db\Sql\Ddl;


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$alterStmt = new Ddl\AlterTable(Db\Discussion\Topic::MAIN_TABLE);
$alterStmt->changeColumn('entity_type', new Ddl\Column\Varchar('linked_entity_type', 32, true))
    ->changeColumn('entity_id', new Ddl\Column\Integer('linked_entity_id', true, null, ['unsigned' => true]));
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Geefter::MAIN_TABLE);
$alterStmt->changeColumn('password_hash', new Ddl\Column\Varchar('password_hash', 255, true))
    ->changeColumn('password_token', new Ddl\Column\Varchar('password_token', 255, true));
$this->getConnection()->query($alterStmt);

$alterStmt = new Ddl\AlterTable(Db\Setup\Constants::DEPRECATED_TABLE_CONFIG);
$alterStmt->dropColumn('type');
$this->getConnection()->query($alterStmt);
