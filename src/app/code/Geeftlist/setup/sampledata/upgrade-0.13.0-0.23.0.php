<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$faker = \Faker\Factory::create();
$faker->seed(20190913);

/** @var \Geeftlist\Service\Badge $badgeService */
$badgeService = $setup->getContainer()->get(\Geeftlist\Service\Badge::class);

//
// GEEFTEES
//
$geefteeBadgeIds = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Badge::ENTITY_TYPE)
    ->addFieldToFilter('entity_type', \Geeftlist\Model\Geeftee::ENTITY_TYPE)
    ->getAllIds();

$geeftees = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE);
foreach ($geeftees as $geeftee) {
    $badgeService->setBadgeIds(
        $geeftee,
        $faker->randomElements(
            $geefteeBadgeIds,
            $faker->numberBetween(0, count($geefteeBadgeIds))
        )
    );
}

//
// GIFTS
//
$giftBadgeIds = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Badge::ENTITY_TYPE)
    ->addFieldToFilter('entity_type', \Geeftlist\Model\Gift::ENTITY_TYPE)
    ->getAllIds();

$gifts = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE);
foreach ($gifts as $gift) {
    $badgeService->setBadgeIds(
        $gift,
        $faker->randomElements(
            $giftBadgeIds,
            $faker->numberBetween(0, count($giftBadgeIds))
        )
    );
}
