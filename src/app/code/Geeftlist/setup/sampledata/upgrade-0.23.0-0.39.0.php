<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$faker = \Faker\Factory::create();
$faker->seed(20220205);

$avgGiftListsPerGeefter = 3;
$avgGiftsPerGiftList = 3;

$geefterCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geefter::ENTITY_TYPE)
    ->load();
$gifts = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE)
    ->getItems();

/** @var \Geeftlist\Model\Geefter $geefter */
foreach ($geefterCollection as $geefter) {
    // Special for John & Jane: more lists
    if (in_array($geefter->getEmail(), [\Geeftlist\Test\Constants::GEEFTER_JOHN_EMAIL, \Geeftlist\Test\Constants::GEEFTER_JANE_EMAIL])) {
        $listsCount = $faker->numberBetween(3, $avgGiftListsPerGeefter * 3);
    } else {
        $listsCount = $faker->numberBetween(0, $avgGiftListsPerGeefter * 2);
    }

    for ($i = 0; $i < $listsCount; ++$i) {
        /** @var \Geeftlist\Model\GiftList $giftList */
        $giftList = $this->getModelFactory()->resolveMake(\Geeftlist\Model\GiftList::ENTITY_TYPE, ['data' => [
            'name'     => sprintf('%s List %d', \Geeftlist\Test\Constants::SAMPLEDATA_GIFTLIST_LABEL_PREFIX, $i + 1),
            'owner_id' => $geefter->getId(),
        ]]);
        $giftList->save();

        // Pick random gifts (even if not available to the current geefter)
        $randomGifts = $faker->randomElements(
            $gifts,
            $faker->numberBetween(0, $avgGiftsPerGiftList * 2)
        );

        /** @var \Geeftlist\Model\GiftList\Gift $giftListGiftHelper */
        $giftListGiftHelper = $this->getContainer()->get(\Geeftlist\Model\GiftList\Gift::class);
        $giftListGiftHelper->addLinks([$giftList], $randomGifts);
    }
}
