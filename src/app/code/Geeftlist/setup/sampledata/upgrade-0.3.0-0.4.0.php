<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$faker = Faker\Factory::create();
$faker->seed(20180306);

$crypt = $setup->getCryptService();

// Set date of birth on all geeftees
$geeftees = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
    ->addFieldToFilter('email', ['nlike' => \Geeftlist\Test\Constants::GEEFTER_JOHN_EMAIL])
    ->addFieldToFilter('email', ['nlike' => \Geeftlist\Test\Constants::GEEFTER_JANE_EMAIL])
    ->addFieldToFilter('email', ['nlike' => \Geeftlist\Test\Constants::GEEFTER_RICHARD_EMAIL]);

$dob = [];
foreach ($geeftees as $geeftee) {
    if ($faker->boolean(10)) {
        // Keep 10% of geeftee with empty dob
        continue;
    }

    $stmt = $this->getConnection()->update(Db\Geeftee::MAIN_TABLE)
        ->set(['dob' => \OliveOil\Core\Helper\DateTime::getDateSql($faker->dateTimeThisCentury)]);
    $stmt->where->equalTo('geeftee_id', $geeftee->getId());
    $this->getConnection()->query($stmt);
}

//Fixed Geefter
$mrNoFamily = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Geefter::ENTITY_TYPE, ['data' => [
    'username'      => 'Mr No Family',
    'email'         => 'mr.nofamily@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN,
    'password_hash' => $crypt->hash('123456')
]]);
$mrNoFamily->save();
$mrNoFamily->getGeeftee()->addData([
    'dob'           => '1970-03-11'
])->save();
