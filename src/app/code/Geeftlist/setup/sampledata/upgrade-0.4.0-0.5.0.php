<?php

$faker = \Faker\Factory::create();
$faker->seed(20170802);

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

// Gift creation
$gifts = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE);
foreach ($gifts as $gift) {
    /** @var \Geeftlist\Model\Activity $createActivity */
    $createActivity = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Activity::ENTITY_TYPE);
    $createActivity->setAction(\Geeftlist\Model\Activity\Field\Action::CREATE)
        ->setActorType(\Geeftlist\Model\Geefter::ENTITY_TYPE)
        ->setActorId($gift->getCreatorId())
        ->setObjectType(\Geeftlist\Model\Gift::ENTITY_TYPE)
        ->setObjectId($gift->getId())
        ->setCreatedAt($gift->getCreatedAt())
        ->save();

    // Gift update
    if ($faker->boolean(30)) {
        /** @var \Geeftlist\Model\Activity $updateActivity */
        $updateActivity = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Activity::ENTITY_TYPE);
        $updateActivity->setAction(\Geeftlist\Model\Activity\Field\Action::UPDATE)
            ->setActorType(\Geeftlist\Model\Geefter::ENTITY_TYPE)
            ->setActorId($gift->getCreatorId())
            ->setObjectType(\Geeftlist\Model\Gift::ENTITY_TYPE)
            ->setObjectId($gift->getId())
            ->setCreatedAt(\OliveOil\Core\Helper\DateTime::getDateSql(
                $faker->dateTimeBetween($gift->getCreatedAt(), 'now')
            ))->save();
    }
}

// Family join/leave
$families = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Family::ENTITY_TYPE);
foreach ($families as $family) {
    /** @var \Geeftlist\Model\Family $family */
    $members = $family->getGeefteeCollection();

    /** @var \Geeftlist\Model\Geeftee $geeftee */
    foreach ($members as $geeftee) {
        $actor = $geeftee->getGeefter() ?: $geeftee->getCreator();

        // #560 Skip geefterless geeftees without creator (e.g. old members with deleted accounts)
        if (!$actor) {
            continue;
        }

        /** @var \Geeftlist\Model\Activity $joinActivity */
        $joinActivity = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Activity::ENTITY_TYPE);
        $joinActivity->setAction(\Geeftlist\Model\Activity\Field\Action::JOIN)
            ->setActorType(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->setActorId($actor->getId())
            ->setObjectType(\Geeftlist\Model\Family::ENTITY_TYPE)
            ->setObjectId($family->getId())
            ->setCreatedAt(\OliveOil\Core\Helper\DateTime::getDateSql(
                $faker->dateTimeBetween($actor->getCreatedAt(), 'now')
            ))->save();
    }

    $nonMembers = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
        ->addFieldToFilter('family', $family->getId())
        ->setLimit(20);

    foreach ($nonMembers as $geeftee) {
        $actor = $geeftee->getGeefter() ?: $geeftee->getCreator();

        // #560 Skip geefterless geeftees without creator (e.g. old members with deleted accounts)
        if (!$actor) {
            continue;
        }

        $joinDate = $faker->dateTimeBetween($actor->getCreatedAt(), 'now');

        /** @var \Geeftlist\Model\Activity $joinActivity */
        $joinActivity = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Activity::ENTITY_TYPE);
        $joinActivity->setAction(\Geeftlist\Model\Activity\Field\Action::JOIN)
            ->setActorType(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->setActorId($actor->getId())
            ->setObjectType(\Geeftlist\Model\Family::ENTITY_TYPE)
            ->setObjectId($family->getId())
            ->setCreatedAt(\OliveOil\Core\Helper\DateTime::getDateSql($joinDate))
            ->save();

        /** @var \Geeftlist\Model\Activity $leaveActivity */
        $leaveActivity = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Activity::ENTITY_TYPE);
        $leaveActivity->setAction(\Geeftlist\Model\Activity\Field\Action::LEAVE)
            ->setActorType(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
            ->setActorId($actor->getId())
            ->setObjectType(\Geeftlist\Model\Family::ENTITY_TYPE)
            ->setObjectId($family->getId())
            ->setCreatedAt(\OliveOil\Core\Helper\DateTime::getDateSql(
                $faker->dateTimeBetween($joinDate, 'now')
            ))
            ->save();
    }
}
