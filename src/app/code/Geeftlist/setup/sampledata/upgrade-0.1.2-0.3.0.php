<?php

use Geeftlist\Model\ResourceModel\Db;

$faker = Faker\Factory::create();
$faker->seed(20170213);

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$stmt = $this->getConnection()->update(Db\Geefter::MAIN_TABLE);
$stmt->set(['language' => 'fr'])
    ->where->expression('MOD(geefter_id, 2) = ?', 0)
    ->and->notIn(
        'email',
        [
            'johndoe@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'janedoe@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]
    );
$this->getConnection()->query($stmt);

$stmt = $this->getConnection()->update(Db\Geefter::MAIN_TABLE);
$stmt->set(['language' => 'en'])
    ->where->expression('MOD(geefter_id, 2) = ?', 1)
    ->and->notIn(
        'email',
        [
            'johndoe@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN,
            'janedoe@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN
        ]
    );
$this->getConnection()->query($stmt);
