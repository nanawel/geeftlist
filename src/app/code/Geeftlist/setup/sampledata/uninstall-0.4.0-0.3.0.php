<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$stmt = $this->getConnection()->delete(Db\Geefter::MAIN_TABLE);
$stmt->where->equalTo('email', 'mr.nofamily@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN);
$this->getConnection()->query($stmt);

$stmt = $this->getConnection()->update(Db\Geeftee::MAIN_TABLE)
    ->set(['dob' => null]);
$stmt->where->notIn('email', [
        \Geeftlist\Test\Constants::GEEFTER_JOHN_EMAIL,
        \Geeftlist\Test\Constants::GEEFTER_JANE_EMAIL,
        \Geeftlist\Test\Constants::GEEFTER_RICHARD_EMAIL
    ]);
$this->getConnection()->query($stmt);
