<?php

use Geeftlist\Model\Family\Field\Visibility;

$faker = Faker\Factory::create();
$faker->seed(20170213);


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$crypt = $setup->getCryptService();

$geefterCount                    = 80;
$geefteeCount                    = 20;
$familyCount                     = 30;
$avgFamilyPerGeeftee             = 3;
$avgGiftPerGeeftee               = 12;
$avgOwnGiftPerGeefter            = 4;
$avgReservationPerGeefter        = 8;
$avgMembershipRequestsPerFamily  = 10;
$geefterPasswordHash             = $crypt->hash(\Geeftlist\Test\Constants::GEEFTER_OTHERS_PASSWORD);

$sampleData = [
    'geefters'      => [],
    'geeftees'      => [],
    'families'      => [],
    'gifts'         => [],
    'own_gifts'     => [],
    'gift_geeftees' => []
];

//Fixed Geefters
$john = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Geefter::ENTITY_TYPE, ['data' => [
    'geefter_id'    => 1000, // We force the ID to avoid having the same ID for geefter and geeftee which could lead to undetected bugs
    'username'      => \Geeftlist\Test\Constants::GEEFTER_JOHN_USERNAME,
    'email'         => \Geeftlist\Test\Constants::GEEFTER_JOHN_EMAIL,
    'password_hash' => $crypt->hash(\Geeftlist\Test\Constants::GEEFTER_JOHN_PASSWORD)
]]);
$john->setFlag('force_create')
    ->save();
$john->getGeeftee()->addData([
    'dob'           => \Geeftlist\Test\Constants::GEEFTEE_JOHN_DOB
])->save();
$sampleData['geefters'][$john->getId()] = $john;
$sampleData['gift_geeftees'][$john->getGeeftee()->getId()] = $john->getGeeftee();
$jane = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Geefter::ENTITY_TYPE, ['data' => [
    'geefter_id'    => 1010, // We force the ID to avoid having the same ID for geefter and geeftee which could lead to undetected bugs
    'username'      => \Geeftlist\Test\Constants::GEEFTER_JANE_USERNAME,
    'email'         => \Geeftlist\Test\Constants::GEEFTER_JANE_EMAIL,
    'password_hash' => $crypt->hash(\Geeftlist\Test\Constants::GEEFTER_JANE_PASSWORD)
]]);
$jane->setFlag('force_create')
    ->save();
$jane->getGeeftee()->addData([
    'dob'           => \Geeftlist\Test\Constants::GEEFTEE_JANE_DOB
])->save();
$sampleData['geefters'][$jane->getId()] = $jane;
$sampleData['gift_geeftees'][$jane->getGeeftee()->getId()] = $jane->getGeeftee();
$this->getLogger()->debug('Created John & Jane');

// Random Geeftees (non-users)
for ($i = 0; $i < $geefteeCount; ++$i) {
    $name = $faker->name;
    $email = $faker->boolean(30)
        ? \Geeftlist\Test\Util\SampleData::fakeEmailFromName($name)
        : null;
    $name = ($email === null ? \Geeftlist\Test\Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX : '') . $name;
    //$creator = $faker->randomElement($sampleData['geefters'])->getId();
    $creator = $faker->boolean() ? $john : $jane;
    $model = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Geeftee::ENTITY_TYPE, ['data' => [
        'name'          => $name,
        'email'         => $email,
        'creator_id'    => $creator->getId()
    ]]);
    $model->save();
    $sampleData['geeftees'][$model->getId()] = $model;
    $sampleData['gift_geeftees'][$model->getId()] = $model;
}

$this->getLogger()->debug(sprintf('Created %d random geeftees', $i));

// Random Geefters
for ($i = 0; $i < $geefterCount - 1; ++$i) {
    $name = $faker->name;
    if ($faker->boolean(30)) {
        $name = sprintf('<%s>', $name);  // HTML node-like names to check escaping
    }

    $model = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Geefter::ENTITY_TYPE, ['data' => [
        'username'      => $name,
        'email'         => \Geeftlist\Test\Util\SampleData::fakeEmailFromName($name),
        'password_hash' => $geefterPasswordHash
    ]]);
    $model->save();
    $sampleData['geefters'][$model->getId()] = $model;
    $sampleData['gift_geeftees'][$model->getGeeftee()->getId()] = $model->getGeeftee();
}

// Special hacker geefter
$name = '<script type="application/javascript">alert("Mr H4ck3r");</script>';
$model = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Geefter::ENTITY_TYPE, ['data' => [
    'username'      => $name,
    'email'         => \Geeftlist\Test\Util\SampleData::fakeEmailFromName('mr-hacker'),
    'password_hash' => $geefterPasswordHash
]]);
$model->save();
$sampleData['geefters'][$model->getId()] = $model;
$sampleData['gift_geeftees'][$model->getGeeftee()->getId()] = $model->getGeeftee();

$this->getLogger()->debug(sprintf('Created %d random geefters', $i));

//Random Families
for ($i = 0; $i < $familyCount; ++$i) {
    /** @var \Geeftlist\Model\Geefter $owner */
    $owner = $faker->boolean(25)
        ? $faker->randomElement([$john, $jane])
        : $faker->randomElement($sampleData['geefters']);
    /** @var \Geeftlist\Model\Family $model */
    $model = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Family::ENTITY_TYPE, ['data' => [
        'name'          => \Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . sprintf('%s-%s', $faker->lastName, $faker->lastName),
        'visibility'    => $faker->randomElement([Visibility::PUBLIC, Visibility::PROTECTED, Visibility::PRIVATE]),
        'owner_id'      => $owner->getId()
    ]]);
    $model->save();
    $model->addGeeftee($owner->getGeeftee());

    if ($faker->randomDigit() < 4 && $owner !== $john) {
        $model->addGeeftee($john->getGeeftee());
    }

    if ($faker->randomDigit() < 4 && $owner !== $jane) {
        $model->addGeeftee($jane->getGeeftee());
    }

    $sampleData['families'][$model->getId()] = $model;
}

$this->getLogger()->debug(sprintf('Created %d random families', $i));

// Random Gifts
$totalGiftCount = 0;
foreach ($sampleData['gift_geeftees'] as $geeftee) {
    $giftCount = $faker->numberBetween(0, $avgGiftPerGeeftee * 2);
    for ($i = 0; $i < $giftCount; ++$i) {
        /** @var \Geeftlist\Model\Geefter $creator */
        $creator = $faker->randomElement($sampleData['geefters']);
        $description = sprintf(
            "Gift for %s (%s) from %s (%s)\n\n%s",
            $geeftee->getName(),
            $geeftee->getEmail() ?: '<no email>',
            $creator->getUsername(),
            $creator->getEmail(),
            implode("\n\n", $faker->paragraphs($faker->randomDigit() % 4))
        );

        $model = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Gift::ENTITY_TYPE, ['data' => [
            'label'         => \Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . $faker->sentence(5, true),
            'description'   => $description,
            'estimated_price' => $faker->randomFloat(2, 5, 2000),
            'creator_id'    => $creator->getId(),
            'created_at'    => \OliveOil\Core\Helper\DateTime::getDateSql($faker->dateTimeBetween('-2 years', 'now')),
            'status'        => $faker->randomDigit() < 8
                ? \Geeftlist\Model\Gift\Field\Status::AVAILABLE
                : \Geeftlist\Model\Gift\Field\Status::ARCHIVED,
            'priority'      => $faker->randomElement(\Geeftlist\Model\Gift\Field\Priority::VALUES)
        ]]);
        $model->save();
        $sampleData['gifts'][$model->getId()] = $model;

        $geeftee->addGift($model);
        ++$totalGiftCount;
    }

    $this->getLogger()->debug(sprintf('Created %d random gifts for %s', $i, $geeftee->getName()));

    //Random own gifts (if not a geefterless geeftee)
    if ($geefter = $geeftee->getGeefter()) {
        $giftCount = $faker->numberBetween(0, $avgOwnGiftPerGeefter * 2);
        for ($i = 0; $i < $giftCount; ++$i) {
            $description = sprintf(
                "Own gift for %s (%s)\n\n%s",
                $geeftee->getName(),
                $geeftee->getEmail(),
                implode("\n\n", $faker->paragraphs($faker->randomDigit() % 4))
            );

            /** @var \Geeftlist\Model\Geefter $geefter */
            $model = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Gift::ENTITY_TYPE, ['data' => [
                'label'       => \Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . $faker->sentence(5, true),
                'description' => $description,
                'estimated_price' => $faker->randomFloat(2, 5, 2000),
                'creator_id'  => $geefter->getId(),
                'created_at'  => \OliveOil\Core\Helper\DateTime::getDateSql($faker->dateTimeBetween('-2 years', 'now'))
            ]]);
            $model->save();
            $sampleData['own_gifts'][$model->getId()] = $model;

            $geeftee->addGift($model);
            ++$totalGiftCount;
        }

        $this->getLogger()->debug(sprintf('Created %d random own gifts for %s', $i, $geeftee->getName()));
    }
}

$this->getLogger()->debug(sprintf('Created %d total random gifts', $totalGiftCount));

// Assign geeftees to families
$geefteeCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE);
foreach ($geefteeCollection as $geeftee) {
    $familyCount = $faker->numberBetween(0, $avgFamilyPerGeeftee * 2);
    /** @var \Geeftlist\Model\Family $family */
    foreach ($faker->randomElements($sampleData['families'], $familyCount) as $family) {
        if (!$family->isMember($geeftee)) {
            $family->addGeeftee($geeftee);
        }

        $sampleData['family_geeftees'][$family->getId()][$geeftee->getId()] = $geeftee;
    }
}

// Create reservations
$totalReservationCount = 0;
foreach ($sampleData['geefters'] as $geefter) {
    /** @var \Geeftlist\Model\Geefter $geefter */
    $familyIds = $geefter->getFamilyCollection()->getAllIds();
    if (empty($familyIds)) {
        continue;
    }

    $geefteeCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
        ->addFieldToFilter('families', ['in' => $familyIds]);

    $reservationCount = $faker->numberBetween(0, $avgReservationPerGeefter * 2);
    $giftCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE);
    $gifts = $giftCollection
        ->addFieldToFilter('geeftees', ['in' => $geefteeCollection->getAllIds()])
        ->getItems();

    foreach ($faker->randomElements($gifts, min(count($gifts), $reservationCount)) as $gift) {
        $reservation = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Reservation::ENTITY_TYPE, ['data' => [
            'geefter_id' => $geefter->getId(),
            'gift_id' => $gift->getId(),
            'type' => $faker->randomElement([
                \Geeftlist\Model\Reservation::TYPE_RESERVATION,
                \Geeftlist\Model\Reservation::TYPE_PURCHASE
            ]),
            'amount' => $faker->randomFloat(2, 5, $gift->getEstimatedPrice()),
            'reserved_at' => \OliveOil\Core\Helper\DateTime::getDateSql($faker->dateTimeBetween('-2 years', 'now'))
        ]]);
        $reservation->save();
        ++$totalReservationCount;
    }
}

$this->getLogger()->debug(sprintf('Created %d total reservations', $totalReservationCount));

// Create membership requests
$totalMembershipRequests = 0;
foreach ($sampleData['families'] as $family) {
    $requestCount = $faker->numberBetween(0, $avgMembershipRequestsPerFamily * 2);

    $maxTry = 100;
    $requestCreated = 0;
    while ($maxTry-- && $requestCreated < $requestCount) {
        /** @var \Geeftlist\Model\Geeftee $geeftee */
        $geeftee = $faker->randomElement($sampleData['geeftees']);
        if (! array_key_exists($geeftee->getId(), $sampleData['family_geeftees'][$family->getId()])) {
            $mr = $family->newMembershipRequest($geeftee);
            $mr->setCreatedAt(\OliveOil\Core\Helper\DateTime::getDateSql($faker->dateTimeBetween('-3 months', 'now')))
                ->save();

            $sampleData['membership_requests'][$family->getId()][$mr->getId()] = $mr;
            ++$requestCreated;
            ++$totalMembershipRequests;
        }
    }
}

$this->getLogger()->debug(sprintf('Created %d total membership requests', $totalMembershipRequests));

// Create claim requests
$totalClaimRequests = 0;
$geefterlessGeeftees = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
    ->addFieldToFilter('geefter_id', ['null' => true]);
foreach ($geefterlessGeeftees as $geeftee) {
    /** @var \Geeftlist\Model\Geeftee $geeftee */
    if ($faker->boolean(70)) {
        $geefter = $faker->randomElement($sampleData['geefters']);

        $cr = $geeftee->newClaimRequest($geefter);
        $cr->setCreatedAt(\OliveOil\Core\Helper\DateTime::getDateSql($faker->dateTimeBetween('-3 months', 'now')))
            ->save();

        $sampleData['geeftee_claim_requests'][$geeftee->getId()][$cr->getId()] = $cr;
        ++$totalClaimRequests;
    }
}

$this->getLogger()->debug(sprintf('Created %d total claim requests', $totalClaimRequests));
