<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$stmt = $this->getConnection()->update(Db\Geefter::MAIN_TABLE)
    ->set(['api_key' => null]);
$this->getConnection()->query($stmt);
