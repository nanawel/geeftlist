<?php

use Geeftlist\Model\ResourceModel\Db;

$faker = Faker\Factory::create();
$faker->seed(20170213);

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$avgPostsPerGift = 3;


// Create discussion posts for on gifts
$giftCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE);
/** @var Db\Geefter\Collection $geefterCollection */
$geefterCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geefter::ENTITY_TYPE)
    ->load();
foreach ($giftCollection as $gift) {
    $postsCount = $faker->numberBetween(0, $avgPostsPerGift * 2);

    /** @var \Geeftlist\Model\Discussion\Topic $topic */
    $topic = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE, ['data' => [
        'title'       => \Geeftlist\Test\Constants::SAMPLEDATA_DISCUSSION_TOPIC_TITLE_PREFIX . ('Gift ' . $gift->getId()),
        'author_id'   => $gift->getCreatorId(),
        'linked_entity_type' => \Geeftlist\Model\Gift::ENTITY_TYPE,
        'linked_entity_id'   => $gift->getId()
    ]]);
    $topic->save();

    for ($i = 0; $i < $postsCount; ++$i) {
        $authorId = $faker->randomElement($geefterCollection->getAllIds());

        /** @var \Geeftlist\Model\Discussion\Post $post */
        $post = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE, ['data' => [
            'topic_id'  => $topic->getId(),
            'author_id' => $authorId,
            'title'     => \Geeftlist\Test\Constants::SAMPLEDATA_DISCUSSION_TOPIC_TITLE_PREFIX . $faker->sentence(),
            'message'   => $faker->paragraphs($faker->numberBetween(1, 4), true),
            'is_pinned' => (int) $faker->boolean(10)    // Fix #547
        ]]);
        $post->save();
    }
}
