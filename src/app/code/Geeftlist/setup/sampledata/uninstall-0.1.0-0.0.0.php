<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

// Geefters
$geefterCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geefter::ENTITY_TYPE)
    ->addFieldToFilter('email', ['like' => '%@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN])
    ->delete();

// Geeftees
$geefteeCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
    ->addFieldToFilter('email', ['like' => '%@' . \Geeftlist\Test\Constants::SAMPLEDATA_EMAIL_DOMAIN])
    ->delete();
$geefteeCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE)
    ->addFieldToFilter(
        'name',
        ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_GEEFTERLESS_GEEFTEE_NAME_PREFIX . '%']
    )
    ->delete();

// Families
$familyCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Family::ENTITY_TYPE)
    ->addFieldToFilter('name', ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . '%'])
    ->delete();

// Gifts
$giftCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE)
    ->addFieldToFilter('label', ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . '%'])
    ->delete();
