<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$stmt = $this->getConnection()->update(Db\Geefter::MAIN_TABLE)
    ->set(['api_key' => new \Laminas\Db\Sql\Expression('CONCAT("SAMPLEDATA_APIKEY_", geefter_id)')]);
$this->getConnection()->query($stmt);
