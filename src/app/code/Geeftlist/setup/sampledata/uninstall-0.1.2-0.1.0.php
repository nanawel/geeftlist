<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

// Posts
$postCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Discussion\Post::ENTITY_TYPE)
    ->addFieldToFilter('title', ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_DISCUSSION_POST_TITLE_PREFIX . '%'])
    ->delete();

// Topics
$topicCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Discussion\Topic::ENTITY_TYPE)
    ->addFieldToFilter('title', ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_DISCUSSION_TOPIC_TITLE_PREFIX . '%'])
    ->delete();
