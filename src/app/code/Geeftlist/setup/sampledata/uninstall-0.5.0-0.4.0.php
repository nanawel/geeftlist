<?php

use Geeftlist\Model\ResourceModel\Db;

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$giftCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Gift::ENTITY_TYPE)
    ->addFieldToFilter('label', ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_GIFT_LABEL_PREFIX . '%']);
$familyCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Family::ENTITY_TYPE)
    ->addFieldToFilter('name', ['like' => \Geeftlist\Test\Constants::SAMPLEDATA_FAMILY_NAME_PREFIX . '%']);

$delete = $this->getConnection()->delete(Db\Activity::MAIN_TABLE);
$delete->where->equalTo('object_type', \Geeftlist\Model\Gift::ENTITY_TYPE)
        ->AND->in('object_id', $giftCollection->getAllIds())
    ->OR
        ->equalTo('object_type', \Geeftlist\Model\Family::ENTITY_TYPE)
        ->AND->in('object_id', $familyCollection->getAllIds());
$this->getConnection()->query($delete);
