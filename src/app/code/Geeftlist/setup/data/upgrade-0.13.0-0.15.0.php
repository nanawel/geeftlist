<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/install/fix-hints-20180929.php';
include __DIR__ . '/install/feature-jwt-authentication.php';
