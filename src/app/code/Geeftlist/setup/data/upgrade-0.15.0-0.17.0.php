<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/install/fix-hints-20190120.php';
include __DIR__ . '/install/feature-generate-geeftee-avatars.php';
