<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$csvReader = \League\Csv\Reader::createFromPath(__DIR__ . '/../fixtures/badges-v1.csv');
$csvReader->setHeaderOffset(0);

/** @var \Geeftlist\Model\Badge $badgePrototype */
$badgePrototype = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Badge::ENTITY_TYPE);
foreach ($csvReader->getRecords() as $row) {
    $row['entity_types'] = explode(',', (string) $row['entity_types']);
    $translatedData = [];
    foreach ($row as $k => $v) {
        @[$field, $lang] = explode('.', $k);
        if (!empty($lang)) {
            $translatedData[$lang][$field] = $v;
        }
    }

    $badge = clone $badgePrototype;
    $badge->setTranslatedData($translatedData)
        ->addData($row)
        ->save();
}
