<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

/** @var \Geeftlist\Helper\Share\Entity $shareEntityRuleHelper */
$shareEntityRuleHelper = $this->getModelFactory()->get(\Geeftlist\Helper\Share\Entity::class);
$shareEntityRuleHelper->applyDefaultShareRuleToAllEntities(\Geeftlist\Model\Gift::ENTITY_TYPE);

/** @var \Geeftlist\Indexer\Gift\GeefterAccess $indexer */
$indexer = $this->getModelFactory()->get(\Geeftlist\Indexer\Gift\GeefterAccess::class);
$indexer->reindexAll();

/** @var \Geeftlist\Indexer\Gift\GeefterAccess $indexer */
$indexer = $this->getModelFactory()->get(\Geeftlist\Indexer\Discussion\Topic\GeefterAccess::class);
$indexer->reindexAll();
