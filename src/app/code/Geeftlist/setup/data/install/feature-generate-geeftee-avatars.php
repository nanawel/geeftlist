<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

/** @var \Geeftlist\Service\Avatar\Geeftee $avatarService */
$avatarService = $setup->getContainer()->get(\Geeftlist\Service\Avatar\Geeftee::class);
foreach ($this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE) as $geeftee) {
    $avatarService->getAvatarPath($geeftee, true, true);
}
