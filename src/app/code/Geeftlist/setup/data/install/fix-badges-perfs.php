<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$csvReader = \League\Csv\Reader::createFromPath(__DIR__ . '/../fixtures/badges-v2.csv');
$csvReader->setHeaderOffset(0);

/** @var \OliveOil\Core\Model\RepositoryInterface $badgeRepository */
$badgeRepository = $this->getRepositoryFactory()->resolveGet(\Geeftlist\Model\Badge::ENTITY_TYPE);

/** @var \Geeftlist\Model\Badge $badgePrototype */
$badgePrototype = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Badge::ENTITY_TYPE);
foreach ($csvReader->getRecords() as $row) {
    $badge = $badgeRepository->get($row['code'], 'code')
        ->addData($row);

    $badgeRepository->save($badge);
}
