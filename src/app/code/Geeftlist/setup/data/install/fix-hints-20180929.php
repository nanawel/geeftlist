<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$csvReader = \League\Csv\Reader::createFromPath(__DIR__ . '/../fixtures/hints-v3.csv');
$csvReader->setHeaderOffset(0);

$cmsBlockPrototype = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Cms\Block::ENTITY_TYPE);
foreach ($csvReader->getRecords() as $row) {
    $row['code'] = sprintf("%s%'.03d", \Geeftlist\Model\Cms\Constants::HINT_BLOCK_CODE_PREFIX, $row['code']);
    $cmsBlock = clone $cmsBlockPrototype;
    $cmsBlock->loadByCodeAndLanguage($row['code'], $row['language'], null)
        ->addData($row)
        ->save();
}
