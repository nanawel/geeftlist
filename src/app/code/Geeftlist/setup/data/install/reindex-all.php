<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

/** @var \Geeftlist\Indexer\IndexerInterface $indexer */
foreach ($this->getContainer()->get('indexers.all') as $indexer) {
    $indexer->reindexAll();
}
