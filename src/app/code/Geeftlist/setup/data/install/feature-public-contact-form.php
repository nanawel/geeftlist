<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$config = $this->getEnvConfig();

$config->set('visitor/contact/enable_captcha', 1);
$config->set('geefter/contact/enable_captcha', 0);
