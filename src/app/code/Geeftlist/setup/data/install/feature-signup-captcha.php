<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$config = $this->getEnvConfig();

$config->set('geefter/signup/enable_captcha', 1);

// Rename "api/geefter/allow_registration" to "geefter/signup/allow_registration"
$allowRegistration = $config->get('api/geefter/allow_registration');
$config->del('api/geefter/allow_registration');
$config->set('geefter/signup/allow_registration', $allowRegistration);
