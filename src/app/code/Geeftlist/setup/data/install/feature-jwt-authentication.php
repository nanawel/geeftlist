<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$config = $this->getEnvConfig();

$config->set('api/authentication/jwt/secret_key', $setup->getCryptService()->generateToken(256));
$config->set('api/enable', 1);
$config->set('api/geefter/allow_registration', 1);
