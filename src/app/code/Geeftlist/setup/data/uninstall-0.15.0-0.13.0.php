<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/uninstall/feature-jwt-authentication.php';
include __DIR__ . '/uninstall/fix-hints-20180929.php';
