<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

include __DIR__ . '/install/fix-hints-20180826.php';
include __DIR__ . '/install/feature-gift-access-index.php';
