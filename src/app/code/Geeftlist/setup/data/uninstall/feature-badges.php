<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$csvReader = \League\Csv\Reader::createFromPath(__DIR__ . '/../fixtures/badges-v1.csv');
$csvReader->setHeaderOffset(0);

$codes = [];
foreach ($csvReader->getRecords() as $row) {
    $codes[] = $row['code'];
}

$badgeCollection = $this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Badge::ENTITY_TYPE)
    ->addFieldToFilter('type', 'system')
    ->addFieldToFilter('code', ['in' => $codes])
    ->delete();
