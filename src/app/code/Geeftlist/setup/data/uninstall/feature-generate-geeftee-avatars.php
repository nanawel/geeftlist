<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

/** @var \Geeftlist\Model\Geeftee $geeftee */
foreach ($this->getResourceModelFactory()->resolveMakeCollection(\Geeftlist\Model\Geeftee::ENTITY_TYPE) as $geeftee) {
    $geeftee->setAvatarPath(null)
        ->save();
}
