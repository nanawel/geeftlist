<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$config = $this->getEnvConfig();

$config->del('visitor/contact/enable_captcha');
$config->del('geefter/contact/enable_captcha');
