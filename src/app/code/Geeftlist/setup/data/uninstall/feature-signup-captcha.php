<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$config = $this->getEnvConfig();

$config->del('geefter/signup/enable_captcha');

// Revert rename "api/geefter/allow_registration" to "geefter/signup/allow_registration"
$allowRegistration = $config->get('geefter/signup/allow_registration');
$config->del('geefter/signup/allow_registration');
$config->set('api/geefter/allow_registration', $allowRegistration);
