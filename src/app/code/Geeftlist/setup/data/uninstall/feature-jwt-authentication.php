<?php

/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$config = $this->getEnvConfig();

$config->del('api/authentication/jwt/secret_key');
$config->del('api/enable');
$config->del('api/geefter/allow_registration');
