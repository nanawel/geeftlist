<?php


/** @var \Geeftlist\Model\ResourceModel\Db\Setup $this */
/** @var \OliveOil\Core\Model\SetupInterface $setup */

$csvReader = \League\Csv\Reader::createFromPath(__DIR__ . '/../fixtures/hints-v1.csv');
$csvReader->setHeaderOffset(0);

/** @var \Geeftlist\Model\Cms\Block $cmsBlock */
$cmsBlock = $this->getModelFactory()->resolveMake(\Geeftlist\Model\Cms\Block::ENTITY_TYPE);
foreach ($csvReader->getRecords() as $row) {
    $code = sprintf("%s%'.03d", \Geeftlist\Model\Cms\Constants::HINT_BLOCK_CODE_PREFIX, $row['code']);
    $cmsBlock->loadByCodeAndLanguage($code, $row['language']);

    if ($cmsBlock->getId()) {
        $cmsBlock->delete();
    }

    $cmsBlock->resetData();
}
