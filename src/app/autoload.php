<?php

/* @var $fw \Base */
$fw = \Base::instance();

if ((float) PCRE_VERSION < 7.9) {
    trigger_error('PCRE version is out of date');
}
if ($fw->get('DEBUG') > 1) {
    error_reporting(E_ALL | E_STRICT);
}

define('OLIVEOIL_ETC_PATH', getenv('OLIVEOIL_ETC_PATH') ?: (realpath('./etc/') . DIRECTORY_SEPARATOR));
