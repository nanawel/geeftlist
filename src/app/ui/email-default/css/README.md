This theme uses **Foundation for Emails**: https://get.foundation/emails.html

The file `base.css` comes from the release 2.2.1 that is available at the following locations:
 - https://s3.amazonaws.com/zurb-foundation/foundation-emails.zip
 - https://github.com/foundation/foundation-emails/releases/tag/v2.2.1

Overrides can be inserted in `theme.css` in order to keep Foundation's file intact
for future update.
