'use strict';

import browser from 'browser-sync';
import fs from 'fs';
import gulp from 'gulp';
import log from 'fancy-log';
import mergeStream from 'merge-stream';
import named from 'vinyl-named';
import path from 'path';
import plugins from 'gulp-load-plugins';
import rename from 'gulp-rename';
import rimraf from 'rimraf';
import through from 'through2';
import webpack2 from 'webpack';
import webpackStream from 'webpack-stream';
import yaml from 'js-yaml';
import yargs from 'yargs';

// Load all Gulp plugins into one variable
const $ = plugins({
    postRequireTransforms: {
        // Necessary with gulp-sass 5+
        // See https://github.com/dlmanning/gulp-sass/issues/806#issuecomment-906496279
        sass: function(sass) {
            return sass(require('sass'));
        }
    }
});

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);
log.info('PRODUCTION MODE:', PRODUCTION);

// Load settings from settings.yml
const { COMPATIBILITY, PORT, UNCSS_OPTIONS, PATHS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// Build the "dist" folder by running all the tasks
gulp.task('buildTask',
    gulp.series(
        cleanTask,
        gulp.parallel(
            sassTask,
            javascriptTask,
            imagesTask,
            extimagesTask,
            extfontsTask,
            extfilesTask,
            convertDatatablesI18nFilesTask,
            copyTask
        )
    )
);

// Build the site, run the server, and watch for file changes
gulp.task('default', gulp.series('buildTask', watchTask));

// Dev only - See #506
gulp.task('convertDatatablesI18nFilesTask', convertDatatablesI18nFilesTask);

// Delete the "dist" folder
// This happens every time a build starts
function cleanTask(done) {
  rimraf(PATHS.dist, done);
}

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately
function copyTask() {
  return gulp.src(PATHS.assets)
    //.pipe($.debug())
    .pipe(gulp.dest(PATHS.dist));
}

// Compile Sass into CSS
// In production, the CSS is compressed
function sassTask() {
  return gulp.src('src/assets/scss/app.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
          includePaths: PATHS.sass
        })
        .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: COMPATIBILITY
    }))
    // Comment in the pipe below to run UnCSS in production
    //.pipe($.if(PRODUCTION, $.uncss(UNCSS_OPTIONS)))
    .pipe($.if(PRODUCTION, $.cleanCss({ compatibility: 'ie9' })))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/css'))
    .pipe(browser.reload({ stream: true }));
}

let webpackConfig = {
  mode: PRODUCTION ? 'production' : 'development',
  module: {
    rules: [
      {
        test: /.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              // Note (2020-04-20): Needed for i18next-http-backend ("spread" operator)
              plugins: ['@babel/plugin-proposal-object-rest-spread']
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
        // Avoids relative paths in imports
        // => "import X from '../../Geeftlist/X';" => import X from 'Geeftlist/X
        Geeftlist: path.resolve(__dirname, 'src/assets/js/Geeftlist/'),
        OliveOil: path.resolve(__dirname, 'src/assets/js/OliveOil/'),
      },
  },
}
// Combine JavaScript into one file
// In production, the file is minified
function javascriptTask() {
  return gulp.src(PATHS.entries)
    .pipe(named())
    .pipe($.sourcemaps.init())
    .pipe(webpackStream(webpackConfig, webpack2))
    .pipe($.if(PRODUCTION, $.terser()         // Note (2019-11-01): uglify is deprecated and has been replaced by terser
      .on('error', e => { console.log(e); })
    ))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/js'));
}
gulp.task('javascript', javascriptTask);

// Copy images to the "dist" folder
// In production, the images are compressed
function imagesTask() {
  return gulp.src('src/assets/img/**/*')
    .pipe($.if(PRODUCTION, $.imagemin({
      progressive: true
    })))
    .pipe(gulp.dest(PATHS.dist + '/img'));
}

// Copy external images to the "dist" folder
// In production, the images are compressed
function extimagesTask() {
  return gulp.src(PATHS.extimages)
    .pipe($.if(PRODUCTION, $.imagemin({
      progressive: true
    })))
    .pipe(gulp.dest(PATHS.dist + '/img'));
}

// Copy external fonts to the "dist" folder
function extfontsTask() {
  return gulp.src(PATHS.extfonts)
    .pipe(gulp.dest(PATHS.dist + '/font'));
}

// Copy external files to specified folders
function extfilesTask() {
    let streams = [];
    for (let src in PATHS.extfiles_dest) {
        let stream;
        let cfg = PATHS.extfiles_dest[src];
        if (! stream) {
            stream = gulp.src([src]);
        }
        if (cfg.transform instanceof Array) {
            cfg.transform.forEach(function (func) {
                stream = stream.pipe(through.obj(eval(func)));
            });
        }
        stream.pipe(gulp.dest(PATHS.dist + "/" + cfg.dest));

        streams.push(stream);
    }
    return mergeStream(streams);
}

//
// DATATABLES I18N
//
// Convert DataTables i18n *.json files to valid JSON with simplified ISO codes.
// There's no clean support for language fallback unfortunately...
function convertDatatablesI18nFilesTask() {
    return gulp.src([PATHS.datatablesI18n.src])
        .pipe(rename(function (path) {
            path.basename = path.basename.split('-')[0];
            path.extname = ".json";
        }))
        .pipe(gulp.dest(PATHS.dist + "/" + PATHS.datatablesI18n.dest))
    ;
}

// Reload the browser with BrowserSync
function reload(done) {
  browser.reload();
  done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watchTask() {
  gulp.watch(PATHS.assets, copyTask);
  gulp.watch('src/assets/scss/**/*.scss', sassTask);
  gulp.watch('src/assets/js/**/*.js').on('change', javascriptTask);
  gulp.watch('src/assets/img/**/*').on('change', imagesTask);
  gulp.watch(PATHS.extimages).on('change', extimagesTask);
  gulp.watch(PATHS.extfonts).on('change', extfontsTask);
  gulp.watch(PATHS.extfiles).on('change', extfilesTask);
  gulp.watch(PATHS.datatablesI18n.src).on('change', convertDatatablesI18nFilesTask);
}
