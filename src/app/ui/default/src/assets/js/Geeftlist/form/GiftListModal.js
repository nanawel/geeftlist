import jQuery from 'jquery';
import ko from 'knockout';
import Toast from 'OliveOil/Toast';

'use strict';

const GiftListModal = function (config, oliveOil, i18n, apiClient) {
    const self = this;
    this.config = jQuery.extend(true, config, {});
    this.oliveOil = oliveOil || window.oliveOil;
    this.i18n = i18n || window.i18n;
    this.apiClient = apiClient || window.geeftlistApiClient;

    this.modalTemplate = (modalElId) => `
        <div class="gift-lists-selector tiny reveal" id="${modalElId}" data-reveal>
            <div class="grid-y grid-margin-y">
                <h3 class="cell">
                    <span data-bind="text: gift.label"></span>
                </h3>
                <fieldset data-bind="visible: listsVisible">
                    <p data-bind="text: i18n.tr('gift-lists-selector.label')"></p>
                    <!-- ko ifnot: visibleLists().length -->
                        <div class="callout warning empty-notice"
                            data-bind="text: i18n.tr('gift-lists-selector.empty-notice')"></div>
                    <!-- /ko -->
                    <div class="lists-container grid-y cell expanded" data-bind="foreach: {data: lists, as: 'l'}">
                        <div data-bind="visible: l.status == 'active'">
                            <input type="checkbox"
                                name="selected_lists"
                                data-bind="attr: {id: $root.getCheckboxId(l.id)}, checked: $root.selectedListIds, value: l.id">
                            <label data-bind="attr: {for: $root.getCheckboxId(l.id)}, text: l.name"></label>
                        </div>
                    </div>
                </fieldset>
                <div class="cell">
                    <div class="expanded button-group">
                        <button class="button save" data-bind="enable: canSave, click: save">
                            <span data-bind="text: i18n.tr('save')"></span>
                        </button>
                        <a class="button cancel" data-close>
                            <span data-bind="text: i18n.tr('close')"></span>
                        </a>
                    </div>
                </div>
            </div>
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `;
    //<!-- KO DEBUG --><pre style="font-size: x-small" data-bind="text: ko.toJSON($data, null, 2)"></pre>

    this.show = function (el) {
        const giftDataEl = $(el).parents('[data-entity-type="gift"]')[0];
        if (!giftDataEl || !$(giftDataEl).data('id')) {
            console.error('Cannot find gift ID in DOM using element: ', el);
            return;
        }
        const giftData = $(giftDataEl).data();
        const modalElId = `add-to-list-modal-${giftData.id}`;

        const modal = $(self.modalTemplate(modalElId));
        $('body').append(modal);

        modal.on('open.zf.reveal', function() {
            var koModel = new (function() {
                const selfModel = this;
                this.gift = giftData;
                this.originalSelectedListIds = [];

                // Observables
                this.listsVisible = ko.observable(false);
                this.savingInProgress = ko.observable(false);
                this.lists = ko.observableArray();
                this.selectedListIds = ko.observableArray();
                this.visibleLists = ko.computed(function() {
                    return selfModel.lists().filter(l => l.status === 'active');
                });
                this.canSave = ko.computed(function() {
                    return selfModel.visibleLists().length
                        && !selfModel.savingInProgress()
                        && JSON.stringify(selfModel.originalSelectedListIds.sort())
                            != JSON.stringify(selfModel.selectedListIds().sort());
                });

                this.getCheckboxId = (listId) => 'gift-' + selfModel.gift.id + '-list-' + listId;
                this.save = function() {
                    console.debug('Click => save()');
                    if (!selfModel.canSave()) {
                        console.debug('Cannot save right now.');
                        return;
                    }

                    this.savingInProgress(true);
                    const requestBody = selfModel.selectedListIds().map((id) => ({type: 'giftList', id: id}));
                    self.apiClient.patch(`gift/${selfModel.gift.id}`, requestBody, {_relationships: 'giftLists'})
                        .then(function() {
                            Toast.showSuccess(i18n.tr('gift-lists-selector.success-notification'));
                            modal.foundation('close');
                        })
                        .catch(function(err) {
                            Toast.showError(i18n.tr('gift-lists-selector.error-notification'));
                        })
                        .finally(function() {
                            selfModel.savingInProgress(false);
                        })
                    ;
                };
            });

            const p1 = self.apiClient.get('giftList')
                .then(function(giftLists) {
                    koModel.lists(giftLists);
                })
            ;
            const p2 = self.apiClient.get('giftList', {filter: {gift: {eq: giftData.id}}})
                .then(function(giftLists) {
                    koModel.originalSelectedListIds = giftLists.map((l) => l.id);
                    koModel.selectedListIds(Array.from(koModel.originalSelectedListIds));
                })
            ;
            Promise.all([p1, p2])
                .then(() => {
                    koModel.listsVisible(true);
                })
                .catch(function(reason) {
                    console.error(reason);
                })
            ;
            ko.applyBindings(koModel, document.getElementById(modalElId));
        });

        // Cleanup on close
        modal.on('closed.zf.reveal', function() {
            // In this order precisely!
            modal.foundation('_destroy');
            modal.remove();
        });

        modal.foundation();
        modal.foundation('open');
    };
};

export {
    GiftListModal as default
};
