import jQuery from 'jquery';

'use strict';

/**
 * @param {Object} element
 * @param {Object} settings
 * @param {OliveOil} oliveOil
 * @constructor
 */
const GeefteeSelect = function (element, settings, oliveOil) {
    const self = this;
    this.element = typeof element === 'string' ? document.querySelector(element) : element;
    this.oliveOil = oliveOil || window.oliveOil;
    settings = settings || {};

    this.tagTemplate = function(tagData) {
        tagData.email = tagData.email || '';
        return `
            <tag title="${(tagData.name || tagData.email)}"
                    contenteditable="false"
                    spellcheck="false"
                    tabIndex="-1"
                    class="${this.settings.classNames.tag} ${tagData.class ? tagData.class : ""}"
                    ${this.getAttributes(tagData)}>
                <x title="" class="${this.settings.classNames.tagX}" role="button" aria-label="remove tag"></x>
                <div>
                    <div class="tagify__tag__avatar-wrap">
                        <img onerror="this.style.visibility='hidden'" src="${tagData.avatarUrl}">
                    </div>
                    <span class="${this.settings.classNames.tagText}">${tagData.name}</span>
                </div>
            </tag>
        `
    };

    this.suggestionItemTemplate = function(tagData) {
        tagData.email = tagData.email || '';
        return `
            <div ${this.getAttributes(tagData)}
                class="tagify__dropdown__item ${tagData.class ? tagData.class : ""}"
                tabindex="0"
                role="option">
                ${ tagData.avatarUrl ? `
                    <div class="tagify__dropdown__item__avatar-wrap">
                        <img onerror="this.style.visibility='hidden'" src="${tagData.avatarUrl}">
                    </div>`
                    : ''
                }
                <strong>${tagData.name}</strong>
                <span>${tagData.email}</span>
            </div>
        `
    };

    this.newTagify = this.newTagify || function () {
        const dropdownAppendTarget = $(self.element)
            .parents(settings._dropdownAppendTargetSelector || '.geefteeSelect-dropdown-append-target')[0];

        return new Tagify(self.element, jQuery.extend(true, {
            tagTextProp: 'geefteeId',
            originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(','),
            enforceWhitelist: true,
            skipInvalid: true, // do not temporarily add invalid tags
            dropdown: {
                enabled: 0,
                maxItems: 30,
                classname: 'geeftee-select-dropdown',
                searchKeys: ['value'],
                placeAbove: false,
                appendTarget: dropdownAppendTarget || null
            },
            templates: {
                tag: self.tagTemplate,
                dropdownItem: self.suggestionItemTemplate
            },
            // Notice: Do *not* use data-whitelist as it's already used by Tagify but limited to array of *strings*
            whitelist: jQuery(self.element).data('tags-whitelist')
        }, settings));
    }

    // initialize Tagify on the above input node reference
    this.tagify = this.newTagify();

    /** @see https://github.com/yairEO/tagify/issues/922#issuecomment-956202540 */
    setTimeout(() =>
        self.tagify.settings.dropdown.searchKeys = ['name', 'email']
    );
};

export {
    GeefteeSelect as default
};
