import jQuery from 'jquery';
import GeefteeSelect from './GeefteeSelect';

'use strict';

/**
 * @param {Object} element
 * @param {Object} settings
 * @param {OliveOil} oliveOil
 * @constructor
 */
const GeefterSelect = function (element, settings, oliveOil) {
    settings = jQuery.extend(true, {
        _dropdownAppendTargetSelector: '.geefterSelect-dropdown-append-target',

        // Tagify settings below
        tagTextProp: 'geefterId',
        dropdown: {
            classname: 'geefter-select-dropdown',
        }
    }, settings);

    GeefteeSelect.call(this, element, settings, oliveOil);
}

export {
    GeefterSelect as default
};
