import jQuery from "jquery";

'use strict';

const GiftListSelect = function (element, settings, oliveOil, i18n) {
    const self = this;
    this.element = typeof element === 'string' ? document.querySelector(element) : element;
    this.settings = Object.assign({
        enforceWhitelist: true
    }, settings);
    this.oliveOil = oliveOil || window.oliveOil;
    this.i18n = i18n || window.i18n;

    this.tagTemplate = function(tagData) {
        if (!self.settings.enforceWhitelist && !tagData.name && !parseInt(tagData.value)) {
            tagData.name = self.i18n.tr('gift-lists-selector.name-new', {name: tagData.value});
            tagData.giftListId = null;
            tagData.class += ' isnew';
            console.info('New list', tagData);
        }
        return `
            <tag title="${(tagData.name)}"
                    contenteditable="false"
                    spellcheck="false"
                    tabIndex="-1"
                    class="${this.settings.classNames.tag} ${tagData.class ? tagData.class : ""}"
                    ${this.getAttributes(tagData)}>
                <x title="" class="${this.settings.classNames.tagX}" role="button" aria-label="remove tag"></x>
                <div>
                    <span class="${this.settings.classNames.tagText}">${tagData.name}</span>
                </div>
            </tag>
        `
    };

    this.suggestionItemTemplate = function(tagData) {
        return `
            <div ${this.getAttributes(tagData)}
                class="tagify__dropdown__item ${tagData.class ? tagData.class : ""}"
                tabindex="0"
                role="option">
                <strong>${tagData.name}</strong>
            </div>
        `
    };

    // initialize Tagify on the above input node reference
    this.tagify = new Tagify(this.element, {
        tagTextProp: 'giftListId',
        originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(','),
        enforceWhitelist: this.settings.enforceWhitelist,
        skipInvalid: true, // do not temporarily add invalid tags
        dropdown: {
            enabled: 0,
            maxItems: 30,
            classname: 'giftlist-select-dropdown',
            searchKeys: ['value'],
            placeAbove: false,
        },
        templates: {
            tag: this.tagTemplate,
            dropdownItem: this.suggestionItemTemplate
        },
        // Notice: Do *not* use the "data-whitelist" attribute as it's already used by
        // Tagify but limited to array of *strings*
        whitelist: jQuery(this.element).data('tags-whitelist')
    });

    /** @see https://github.com/yairEO/tagify/issues/922#issuecomment-956202540 */
    setTimeout(() =>
        self.tagify.settings.dropdown.searchKeys = ['name']
    );
};

export {
    GiftListSelect as default
};
