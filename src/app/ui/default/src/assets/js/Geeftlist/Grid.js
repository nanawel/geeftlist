import jQuery from "jquery";

"use strict";

const GeeftlistGrid = function (element, config, oliveOil) {
    const self = this;
    this.element = element;
    this.config = jQuery.extend(true, config, {
        customDataDefaults: {}
    });
    this.oliveOil = oliveOil || window.oliveOil;

    this.model = {};
    this.dataTableConfig = jQuery.extend(
        true,
        {},
        document.dataTablesDefaultConfig,
        {
            stateSaveCallback: function(settings, data) {
                self.dataTableSave(data);
            },
            stateLoadCallback: function(settings) {
                return self.dataTableLoad();
            }
        },
        config.dataTable
    );
    this.dataTableId = this.oliveOil.getElementPathId(jQuery(element).attr('id'), 'DataTable');
    this.dataTableCustomDataId = this.oliveOil.getElementPathId(jQuery(element).attr('id'), 'DataTable_customData');

    this.dataTableLoad = function () {
        return this.oliveOil.loadFromLocalStorage(self.dataTableId) || {};
    };
    this.dataTableSave = function (data) {
        this.oliveOil.saveToLocalStorage(self.dataTableId, data);
    };
    this.dataTableCustomDataLoad = function () {
        return this.oliveOil.loadFromLocalStorage(self.dataTableCustomDataId) || self.config.customDataDefaults;
    };
    this.dataTableCustomDataSave = function (data) {
        this.oliveOil.saveToLocalStorage(
            self.dataTableCustomDataId,
            jQuery.extend(true, self.dataTableCustomDataLoad(), data)
        );
    };

    this.getAdditionalPostData = function () {
        return self.dataTableCustomDataLoad();
    };

    // Init DataTable
    this.dataTable = jQuery(element).DataTable(this.dataTableConfig);

    this.dataTable.on('init', function(e, settings, json) {
        const searchField = jQuery(self.element)
            .parent()
            .children('.dataTables_filter')
            .find('input[type="search"]');
        // Only send search request on Enter key
        searchField.unbind();
        searchField.keydown(function(ev) {
            if (ev.which == 13) {
                ev.preventDefault();
                return false;
            }
        });
        searchField.keyup(function(ev) {
            if (ev.which == 13) {
                self.dataTable.search(jQuery(ev.target).val()).draw();
            }
        });
        // Special case: when the user clicks the "X" to clear the field (Chrome only atm)
        searchField.on('search', function(ev) {
            if (jQuery(ev.target).val().trim() === '') {
                self.dataTable.search('').draw();
            }
        });
    });
    this.dataTable.on('preXhr', function(e, settings, json) {
        jQuery.extend(json, self.getAdditionalPostData());
    })
    this.dataTable.on('xhr', function(e, settings, json, xhr ) {
        if (json) {
            if (typeof json.length === 'number') {
                self.dataTable.page.len(json.length);
            }
            if (typeof json.start === 'number') {
                self.dataTable.page(parseInt(json.start / self.dataTable.page.len()));
            }
        }
    })
};

export {
    GeeftlistGrid as default
};
