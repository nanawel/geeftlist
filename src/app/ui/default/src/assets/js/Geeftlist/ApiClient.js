"use strict";

import Kitsu from "kitsu";
import Toast from "OliveOil/Toast";

/**
 * @param {Object} config
 */
const GeeftlistApiClient = function (config) {
    const self = this;
    this.config = config || {};
    this.kitsu = new Kitsu({
        baseURL: this.config.baseUrl || document.API_BASE_URL,
        camelCaseTypes: false,
        pluralize: false,
        resourceCase: 'none',
        axiosOptions: {
            headers: $.extend(true, {}, document.API_AUTHORIZATION, this.config.headers || {})
        }
    });

    this.handleResponseError = function(err) {
        console.error(err);
        if (err && parseInt(err.status) === 401) {
            Toast.showError(i18n.tr("api-error.401"));
        }

        return Promise.reject(err);
    };

    /**
     * @param {string} model
     * @param {Object} params
     * @param {Object} config
     * @returns {Promise}
     */
    this.get = function(model, params, config) {
        params = params || {};
        config = config || {};
        config.params = params;

        return self.kitsu.get(model, config)
            .then(function(result) {
                // Automatically unwrap data node from response
                return result.data;
            })
            .catch(self.handleResponseError);
    };

    /**
     * @param {string} model
     * @param {Object} body
     * @param {Object} config
     * @returns {Promise}
     */
    this.patch = function(model, body, config) {
        if (!body) {
            throw 'body cannot be empty for update.';
        }
        config = config || {};

        if (typeof model === 'object') {
            model = `${model.type}/${model.id}`;
        }
        // Alternative way to
        if (typeof config['_relationships'] != 'undefined') {
            model += `/relationships/${config['_relationships']}`;
        }

        return self.kitsu.patch(model, body, config)
            .then(function(result) {
                // Automatically unwrap data node from response
                return result.data;
            })
            .catch(self.handleResponseError);
    };
};

export {
    GeeftlistApiClient as default
};
