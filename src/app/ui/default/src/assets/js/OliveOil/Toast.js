"use strict";

import $ from 'jquery';
import Toastify from 'toastify-js';

/**
 * @param {Object} config
 */
const Toast = new (function (config) {
    config = config || {};
    const self = this;

    /* See https://github.com/apvarun/toastify-js#api */
    this.toastifyDefaults = $.extend(true, {
        close: true,
        gravity: 'bottom',
        position: 'center',
        duration: 6000
    }, config.toastifyDefaults || {});

    this.toastifyDefaultsPerType = {
        info: $.extend(true, this.toastifyDefaults),
        success: $.extend(true, this.toastifyDefaults),
        warning: $.extend(true, this.toastifyDefaults, {
            duration: -1     // persistent
        }),
        error: $.extend(true, this.toastifyDefaults, {
            duration: -1     // persistent
        }),
    };

    this.showInfo = function(message) {
        self.showToast(message, 'info');
    }

    this.showSuccess = function(message) {
        self.showToast(message, 'success');
    }

    this.showWarning = function(message) {
        self.showToast(message, 'warning');
    }

    this.showError = function(message) {
        self.showToast(message, 'error');
    }

    this.showToast = function(message, type, escapeMarkup) {
        console.debug(`SHOW TOAST [${type}]`, message);
        Toastify($.extend(true, {}, self.toastifyDefaultsPerType[type] ?? {}, {
            text: message,
            className: type,
            escapeMarkup: escapeMarkup ?? true
        })).showToast();
    }
});

export {
    Toast as default
};
