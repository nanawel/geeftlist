"use strict";

const OliveOil = function(options) {
    const self = this;

    this.getElementPathId = function(id, prefix, useAvid) {
        useAvid = typeof useAvid === 'undefined' ? true : useAvid;
        return (typeof(prefix) === 'string' ? prefix + '_' : '')
            + id
            + '_'
            + location.pathname
            + (useAvid ? '_' + this.getAssetsVersionId() : '');
    };

    this.saveToLocalStorage = function(key, data) {
        localStorage.setItem(key, JSON.stringify(data));
    };

    this.loadFromLocalStorage = function(key, defaultData) {
        const data = localStorage.getItem(key);
        if (data !== null) {
            return JSON.parse(data);
        }
        return defaultData;
    };

    this.getAssetsVersionId = function() {
        return document.querySelectorAll('head meta[data-assets-version-id]')[0]
            .getAttribute('data-assets-version-id');
    };

    this.goToUrl = function(url, options) {
        options = options || {};
        if (!options.noLoadingOverlay && this.loadingOverlay) {
            this.loadingOverlay.call(function() {
                document.location.href = url;
            });
        }
        else {
            document.location.href = url;
        }
    };

    this.confirmAction = function(message, urlConfirm, urlCancel, options) {
        options = options || {};
        if (confirm(message)) {
            self.goToUrl(urlConfirm, options);
        }
        else if (urlCancel) {
            self.goToUrl(urlCancel, options);
        }
    };
};

export {
    OliveOil as default
};
