import jQuery from "jquery";

"use strict";

const LoadingOverlay = function() {
    const self = this;
    this.fadeDuration = 400;
    this.overlayTimeout = 2000;

    this.show = function(fadeDuration) {
        if (typeof fadeDuration == 'undefined') {
            fadeDuration = self.fadeDuration;
        }
        jQuery('#loading-overlay').fadeIn(fadeDuration);
    };

    this.hide = function(fadeDuration) {
        if (typeof fadeDuration == 'undefined') {
            fadeDuration = self.fadeDuration;
        }
        jQuery('#loading-overlay').stop(true, true )
            .fadeOut(fadeDuration);
    };

    this.call = function(callback, timeout, ...args) {
        if (typeof timeout == 'undefined') {
            timeout = self.overlayTimeout;
        }
        if (typeof callback == 'function') {
            callback(...args);
        }
        setTimeout(function() {
            self.show();
        }, timeout);
    };

    this.wrap = function(callback, ...args) {
        this.show();
        try {
            return callback(...args);
        } finally {
            this.hide();
        }
    };
};

export {
    LoadingOverlay as default
};
