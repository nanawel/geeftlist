import jQuery from "jquery";
import i18next from "i18next";
import i18nextHttpBackend from "i18next-http-backend";

"use strict";

const I18n = function(locale, currency, translationLoadPath) {
    this.i18next = i18next;
    this.translationLoadPath = translationLoadPath;
    this.locales = locale instanceof Array ? locale : [locale];
    this.currency = 'USD';
    this.timezone = 'UTC';
    this.dateTimeStyles = {
        date: {
            short: {
                year    : 'numeric',
                month   : '2-digit',
                day     : '2-digit',
            },
            long: {
                year    : 'numeric',
                month   : 'long',
                day     : 'numeric',
            },
        },
        time: {
            short: {
                hour    : '2-digit',
                minute  : '2-digit',
            },
            long: {
                hour    : '2-digit',
                minute  : '2-digit',
            },
        }
    };

    this.setLocale = function(locale) {
        if (typeof locale == 'string') {
            this.locales = locale.split(',');
        }
    };

    this.setCurrency = function(currency) {
        if (typeof currency == 'string') {
            this.currency = currency;
        }
    };

    this.tr = function(str, options) {
        return this.i18next.t(str, options);
    };

    this.formatDateTime = function(date, dateStyle, timeStyle) {
        let options = {
            timeZone: this.timezone
        };
        if (dateStyle) {
            options = jQuery.extend(true, this.dateTimeStyles.date[dateStyle]);
        }
        if (timeStyle) {
            options = jQuery.extend(true, this.dateTimeStyles.time[timeStyle]);
        }
        for (var l in this.locales) {
            let locale = this.locales[l];
            try {
                if (typeof date == 'number' && parseInt(date)) {
                    date = new Date(parseInt(date));
                }
                else if (typeof date == 'string') {
                    date = new Date(date);
                }
                if (date instanceof Date) {
                    date = date.toLocaleString(locale, options);
                    break;
                }
            }
            catch (e) {
                console.error(e);
            }
        }
        return date;
    };

    this.formatCurrency = function (number, currency) {
        currency = typeof currency != 'undefined' ? currency : this.currency;
        for (var l in this.locales) {
            var locale = this.locales[l];
            try {
                number = parseFloat(number).toLocaleString(locale, {
                    style: 'currency',
                    currency: currency,
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                });
                break;
            }
            catch (e) {
                console.error(e);
            }
        }
        return number;
    };

    this.setLocale(locale);
    this.setCurrency(currency);

    this.init = function() {
        const i18nInitPromise = this.i18next
            .use(i18nextHttpBackend)
            .init({
                lng: this.locales[0],
                fallbackLng: this.locales,
                //debug: true,

                backend: {
                    loadPath: this.translationLoadPath,
                }
            }
        );

        return new Promise(function(resolve, reject) {
            i18nInitPromise.then(resolve)
                .catch(reject);
        });
    }
};

export {
    I18n as default
};
