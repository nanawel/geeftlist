import jQuery from 'jquery';
import ko from 'knockout';
import whatInput from 'what-input';
import 'mhead-js';
import Mmenu from 'mmenu-js';
import DataTables from 'datatables.net';
import DataTablesButtons from 'datatables.net-buttons';
import DataTablesButtonsZf from 'datatables.net-buttons-zf';
import DataTablesFixedHeader from 'datatables.net-fixedheader';
import DataTablesResponsive from 'datatables.net-responsive';
import DataTablesResponsiveZf from 'datatables.net-responsive-zf';
import DataTablesSelect from 'datatables.net-select';
import Foundation from 'foundation-sites/js/foundation';
import multiSelect from 'multiselect/js/jquery.multi-select';
import simpleAjaxUploader from 'simple-ajax-uploader/SimpleAjaxUploader.min';
import {Luminous} from 'luminous-lightbox';
import tippy from 'tippy.js';
import Dropmic from 'dropmic';
import Tagify from '@yaireo/tagify';
import I18n from 'OliveOil/I18n';
import OliveOil from 'OliveOil/OliveOil';
import LoadingOverlay from 'OliveOil/LoadingOverlay';
import Toast from 'OliveOil/Toast';
import GeeftlistGrid from 'Geeftlist/Grid';
import GeefteeSelect from 'Geeftlist/form/GeefteeSelect';
import GeefterSelect from 'Geeftlist/form/GeefterSelect';
import GiftListSelect from 'Geeftlist/form/GiftListSelect';
import GiftListModal from 'Geeftlist/form/GiftListModal';
import GeeftlistApiClient from 'Geeftlist/ApiClient';

// Third-party libs
window.$ = window.jQuery = jQuery;
window.ko = ko;
window.DataTable = window.DataTables = DataTables;
window.simpleAjaxUploader = simpleAjaxUploader;
window.tippy = tippy;
window.Tagify = Tagify;
window.Toast = Toast;

// Internal libs
const i18n = window.i18n = new I18n(window.LANGUAGE_WITH_FALLBACK, window.CURRENCY, window.I18N_TRANSLATION_PATH);
const oliveOil = window.oliveOil = new OliveOil;
window.oliveOil.loadingOverlay = new LoadingOverlay;
window.oliveOil.Toast = Toast;
window.geeftlistApiClient = new GeeftlistApiClient;
window.giftListModal = new GiftListModal({}, oliveOil, i18n, window.geeftlistApiClient);
window.GeeftlistGrid = GeeftlistGrid;
window.GeefteeSelect = GeefteeSelect;
window.GeefterSelect = GeefterSelect;
window.GiftListSelect = GiftListSelect;

/* Dirty hack to avoid PHPStorm from "optimizing" unused imports */
whatInput;
DataTables;
DataTablesButtons;
DataTablesButtonsZf;
DataTablesFixedHeader;
DataTablesResponsive;
DataTablesResponsiveZf;
DataTablesSelect;
Foundation;
multiSelect;
Luminous;

const runPageScripts = function() {
    // This syntax replaces $(document).ready() which is deprecated.
    // See https://api.jquery.com/ready/
    $(function() {
        try {
            // Init Mhead (sticky header)
            new Mhead("#header", {
                hide: 200
            });

            // Init MmenuLight
            new Mmenu(
                document.querySelector("#nav-menu"),
                {
                    // backButton: {    // Conflicts with "onClick:close" below (cancels request)
                    //     close: true
                    // },
                    columns: false,
                    counters: true,
                    drag: {
                        open: false
                    },
                    extensions: [
                        "multiline",
                        "pagedim-black"
                    ],
                    navbars: [
                        {
                            position: "top",
                            content: [
                                "prev",
                                "title",
                                "close"
                            ]
                        }
                    ],
                    onClick: {
                        close: true
                    },
                    setSelected: {
                        hover: true,
                        parent: true
                    }
                }
            );

            document.dataTablesDefaultConfig = {
                processing: true,
                responsive: true,
                dom: "lfrBtip",
                buttons: [],
                stateSave: true,
                language: {
                    "url": BASE_JS_URL + "/datatables/lang/" + LANGUAGE.split('-')[0] + ".json"
                },
                drawCallback: function(settings) {
                    // Fix resize bug with DT that sets a width too large on mobile display
                    $(settings.nTable).css('width', '');
                }
            };

            $('script[type="text/javascript-lazy"][on="ready"]').each(function(i, scriptTag) {
                let scriptId = (scriptTag.id ? scriptTag.id : 'anonymous-' + i);
                try {
                    eval(scriptTag.innerHTML + "\n//# sourceURL=" + scriptId + ".js");
                }
                catch (e) {
                    console.error('Error evaluating deferred script with ID "' + scriptId + '"', e);
                }
            });

            $('.back-button').on('click', function(ev) {
                ev.preventDefault();
                history.back();
            });

            // Init lightbox
            $(document.querySelectorAll('a[data-lightbox]')).each(function(i, el) {
                new Luminous(el);
            });

            // Add Tippy tooltips
            $(document.querySelectorAll('[data-tippy-tooltip]')).each(function(i, el) {
                tippy(el);
            });

            // Init dropmic (as a faster replacement for Foundation's dropdown)
            $(document.querySelectorAll('[data-dropmic="1"]')).each(function(i, el) {
                new Dropmic(el);
            });

            // Init "loading" overlay triggers (non-Abide forms)
            $('form:not(.no-loading-anim,[data-abide])').on('submit', oliveOil.loadingOverlay.call);
            // Init "loading" overlay triggers (Abide forms)
            $(document).on('formvalid.zf.abide', function(ev, frm) {
                if (!$(frm).hasClass('no-loading-anim')
                    && frm.find('.is-invalid-input').length === 0
                ) {
                    oliveOil.loadingOverlay.call();
                }
            });
            /* -- DISABLED - Buggy when combined with confirm() dialogs
            $('body').on(
                'click',
                'a[href^="http"]:not(.no-loading-anim)',
                function(ev) {
                    if (ev.which === 1   // left click only
                        && !ev.ctrlKey
                        && !ev.shiftKey
                        && !ev.altKey
                        && !ev.target.target
                        && ev.target.href
                        && !ev.target.href.startsWith(document.location + "#")
                        && ev.target.href.startsWith(BASE_URL)
                    ) {
                        console.log('oliveOil.loadingOverlay.call');
                        oliveOil.loadingOverlay.call();
                    }
                }
            );
            */
        }
        catch (e) {
            console.error(e);
        }
        finally {
            const startTime = new Date();
            $(document).foundation();
            console.log('Foundation startup time: ' + Math.round(new Date() - startTime) + ' msec.');
        }
    });
};

// Only process deferred scripts in page when i18n is initialized
i18n.init().then(runPageScripts)
    .catch(runPageScripts); // Also on error, too bad
