<?php
chdir(dirname(__DIR__));
umask(0002);

define('COMPOSER_VENDOR_DIR', getenv('COMPOSER_VENDOR_DIR') ?: './vendor');
if (! is_file('./vendor/autoload.php')) {
    echo "You must run `composer install` from this directory to install the dependencies first.\n";
    echo "Visit https://getcomposer.org/ to download the necessary tool or use the package provided by your distro.\n";
    exit(250);
}
require COMPOSER_VENDOR_DIR . '/autoload.php';

/* @var $fw \Base */
$fw = \Base::instance();

// Configure F3
\OliveOil\injectEnvVarsToHive($fw); // Needed because SEED is used when initializing Cache instance by F3
$fw->config(OLIVEOIL_ETC_PATH . 'config.ini')
    ->config(OLIVEOIL_ETC_PATH . 'config-api.ini')
    ->config(OLIVEOIL_ETC_PATH . 'routing-api.ini')
    ->config(OLIVEOIL_ETC_PATH . 'app.ini')
    ->config(OLIVEOIL_ETC_PATH . 'app-api.ini')
    ->config(OLIVEOIL_ETC_PATH . 'events.ini')
    ->config(getenv('WEBAPP_LOCAL_INI')
        ? OLIVEOIL_ETC_PATH . getenv('WEBAPP_LOCAL_INI')
        : OLIVEOIL_ETC_PATH . 'local.ini'
    );
\OliveOil\injectEnvVarsToHive($fw); // Override existing values if any (highest priority)

// Configure DI
$containerService = \OliveOil\Core\Service\Di\Container::instance();
$containerService
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'di/sections/*.php')
    ->addDefinitionsAsFiles($fw->get('DI_LOCAL_CONFIG'))
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'di/di-api.php')
    ->addDefinitionsAsFiles($fw->get('DI_LOCAL_API_CONFIG'))
    ->init();


$containerService->get(\OliveOil\Core\BootstrapInterface::class)->run();
