#!/usr/bin/env php
<?php
if (PHP_SAPI !== 'cli') {
    exit("This controller can only be executed in CLI mode.");
}

chdir(__DIR__);
umask(0002);

if (! is_file('./vendor/autoload.php')) {
    echo "You must run `composer install` from this directory to install the dependencies first.\n";
    echo "Visit https://getcomposer.org/ to download the necessary tool or use the package provided by your distro.\n";
    exit(250);
}

require './vendor/autoload.php';

/* @var $fw \Base */
$fw = \Base::instance();

if ((float)PCRE_VERSION < 7.9) {
	trigger_error('PCRE version is out of date');
}
if ($fw->get('DEBUG') > 1) {
    error_reporting(E_ALL | E_STRICT);
}

if (!defined('OLIVEOIL_ETC_PATH')) {
    define('OLIVEOIL_ETC_PATH', realpath('./etc/') . DIRECTORY_SEPARATOR);
}

$fw->set('ONERROR', function($fw, $params) {
    if ($e = $fw->get('EXCEPTION')) {
        echo "\n";
        echo "{$e}\n";
        exit(min(((int) $e->getCode()) ?: 255, 255));
    }

    $err = $fw->get('ERROR');
    $argv = implode(' ', $_SERVER['argv'] ?? ['<Unknown command>']);
    echo "\n";
    echo "{$err['code']} {$err['status']} {$err['text']}\n";
    echo "{$err['trace']}\n";
    echo "Command was: {$argv}\n";
    exit(min(((int) $err['code']) ?: 255, 255));
});


// Configure F3
\OliveOil\injectEnvVarsToHive($fw); // Needed because SEED is used when initializing Cache instance by F3
$fw->config(OLIVEOIL_ETC_PATH . 'config.ini')
    ->config(OLIVEOIL_ETC_PATH . 'config-cli.ini')
    ->config(OLIVEOIL_ETC_PATH . 'routing-cli.ini')
    ->config(OLIVEOIL_ETC_PATH . 'app.ini')
    ->config(OLIVEOIL_ETC_PATH . 'app-cli.ini')
    ->config(OLIVEOIL_ETC_PATH . 'events.ini')
    ->config(getenv('WEBAPP_LOCAL_INI')
        ? OLIVEOIL_ETC_PATH . getenv('WEBAPP_LOCAL_INI')
        : OLIVEOIL_ETC_PATH . 'local.ini'
    )
;
\OliveOil\injectEnvVarsToHive($fw); // Override existing values if any (highest priority)

// Configure DI
$containerService = \OliveOil\Core\Service\Di\Container::instance();
$containerService
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'di/sections/*.php')
    ->addDefinitionsAsFiles($fw->get('DI_LOCAL_CONFIG'))
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'di/di-cli.php')
    ->addDefinitionsAsFiles($fw->get('DI_LOCAL_CLI_CONFIG'))
    ->init();

$fw->set('SERVER.URL_PATH_PREFIX', '');

exit($containerService->get(\OliveOil\Core\BootstrapInterface::class)->run());
