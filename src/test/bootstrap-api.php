<?php

chdir(dirname(__DIR__));

define('COMPOSER_VENDOR_DIR', getenv('COMPOSER_VENDOR_DIR') ?: './vendor');
require COMPOSER_VENDOR_DIR . '/autoload.php';

/* @var $fw \Base */
$fw = \Base::instance();

$fw->config(OLIVEOIL_ETC_PATH . 'config.ini')
    ->config(OLIVEOIL_ETC_PATH . 'routing.ini')
    ->config(OLIVEOIL_ETC_PATH . 'routing-api.ini')
    ->config(OLIVEOIL_ETC_PATH . 'app.ini')
    ->config(OLIVEOIL_ETC_PATH . 'events.ini')
    ->config(getenv('WEBAPP_LOCAL_INI')
        ? OLIVEOIL_ETC_PATH . getenv('WEBAPP_LOCAL_INI')
        : OLIVEOIL_ETC_PATH . 'local.ini'
    )
    ->config(OLIVEOIL_ETC_PATH . 'app-api.ini')
    ->config(OLIVEOIL_ETC_PATH . 'test/config.ini')
    ->config(OLIVEOIL_ETC_PATH . 'test/config-api.ini')
    ->config(OLIVEOIL_ETC_PATH . 'test/local.ini')
    ->config(OLIVEOIL_ETC_PATH . 'test/local-api.ini');

// Configure DI
$containerService = \OliveOil\Core\Service\Di\Container::instance()
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'di/sections/*.php')
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'di/di-api.php')
    ->addDefinitionsAsFiles($fw->get('DI_LOCAL_CONFIG'))
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'test/di/di.php')
    ->addDefinitionsAsFiles(OLIVEOIL_ETC_PATH . 'test/di/di-api.php')
    ->addDefinitionsAsFiles($fw->get('TEST_DI_LOCAL_CONFIG'));

// Prepare application for tests
$containerService->get(\OliveOil\Core\Test\BootstrapInterface::class)->bootstrap();
