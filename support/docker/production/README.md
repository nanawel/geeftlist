Geeftlist Docker Compose Stack 🐋
===

**The provided Docker stack is the only deployment method that is supported, but you're free to adapt it to your needs.**

If you need to tune some settings, the better way is to keep the `docker-compose.yml` intact and create a `docker-compose.override.yml`. [Learn more here](https://docs.docker.com/compose/how-tos/multiple-compose-files/merge/).

This stack exists to simplify the deployment of the application, by providing the database (MariaDB) and the cache (Redis). But you can also create your own by building the image yourself or using the official one at <https://hub.docker.com/r/nanawel/geeftlist>.


## General Architecture

![Docker Compose Deployment Architecture](../../../resources/doc/docker-compose.png)

```plantuml
@startuml
left to right direction

cloud Internet as int

node "Reverse-proxy" as rp

node "Geeftlist Docker Compose Stack" {
  portin http
  component "webapp" as wa
  database "MariaDB" as db
  database "Redis" as redis
}

int .. rp : "HTTPS (tcp:443)"
rp .. http : "HTTP (tcp:8080)"
http .. wa : "tcp:80"
wa .. db : "tcp:3306"
wa .. redis : "tcp:6379"
@enduml
```

## 📑 Requirements

- [Docker](https://docs.docker.com/engine/install/) (27+ is recommended)
- [Docker Compose plugin](https://docs.docker.com/compose/install/) (2.29+ is recommended)
- A [reverse-proxy](https://en.wikipedia.org/wiki/Reverse_proxy) with TLS support if you want to have HTTPS support (e.g. Apache, Nginx, etc.)
    - An example for **Apache** is available here: [apache/vhost.example](../../apache/vhost.example)
    - An example for **Nginx** is available here: [nginx/vhost.example](../../nginx/vhost.example)

The installation and configuration of these components is not covered here but you can find a lot of tutorials on the Internet.

The Docker image is hosted on Docker Hub.  
Available tags are listed at <https://hub.docker.com/r/nanawel/geeftlist/tags>


## 🏗️ Installation

Create an empty directory and copy the files from this folder:

- `.env`
- `docker-compose.yml`

Open the `.env` file and adjust the main variables. Only the main settings are present here for now.

Boolean value must use `1` (true/yes) and `0` (false/no).

- `BASE_URL`: Public URL of the instance.  
  ⚠️ If set incorrectly, JS and CSS won't be loaded properly.
- `EMAIL_ENABLED`: If set to `1`, you must also set a valid SMTP connection string in `EMAIL_MAILER_DSN`.
- `EMAIL_MAILER_DSN`: Full DSN of the SMTP server used to send mail. [Read more here](https://symfony.com/doc/current/mailer.html).
- `APP_REGISTRATION_ENABLE`: Allow new users to register.
- `APP_SKIP_CONFIRMATION_EMAIL`: If you don't have a valid SMTP server of you don't want your new users to confirm their email, set to `1`.
- `AUTHENTICATION_JWT_SECRET_KEY`: Random secret key used to secure JWT tokens.
- `GEEFTER_SESSION_SECRET_KEY`: Random secret key used to secure session IDs.  
  Changing it on a live instance will terminate all active sessions.

Then create the directories for persistent data.
If you kept the default values in `.env`, it means running these commands:

```shell
mkdir -p ./data/geeftlist/var/{lock,log,sessions} ./data/geeftlist/pub/userdata
chown -R 33:33 ./data/geeftlist
```

Make sure they are writable by the UID `33`, used internally by the application (`www-data` on Debian).

You can now start the stack:

```shell
docker compose up -d
```


## 🚀 First startup

### Database intialization

You need to initialize the database. To do so, simply run this command:

```shell
docker compose exec webapp php ./src/console setup/install
```

### Cronjobs

This stack **does not come with cronjob support** so you must create a cronjob on the host to trigger the scheduled actions (deferred notifications, etc.)

Here is an example of the required cronjobs to set up (replace `BASE_URL` by the same value you previously set in your `.env`):

```
# Every minute
* * * * * www-data /usr/bin/curl -s BASE_URL/cron/run
# Once a day or so
0 0 * * * www-data /usr/bin/curl -s BASE_URL/cron/runJob/job/cleanup_orphan_gift_images
```

Make sure the IP that triggers the cronjob is allowed by `APP_CRON_ALLOWED_IPS` in your `.env`.  
By default it allows these ranges: `127.0.0.1/8`, `::0`, `172.16.0.1/12`

Once this is done, you're all set!

## ✔️ Validation

Open a browser to the URL set in `BASE_URL`: the application should load with no error and with a proper styling.

Check that the email configuration is working by running the following command:

```shell
docker compose exec webapp php ./src/console dev/sendTestEmail
```

A report email should be sent to the address set in `ADMIN_EMAIL`.

A Prometheus endpoint is available at `BASE_URL/system/healthcheck` and allows collecting some metrics and checking the proper working of the application.


## ⛑️ Troubleshooting

### Containers / Docker

If something's wrong, try to take a look at the containers' logs with

```shell
docker compose --profile run logs -f --tail=50
```

### Application's behaviour

The application's logs are located in `./data/geeftlist/var/log` on your host (see `DOCKER_WEBAPP_LOG_VOLUME`).

If you need to enter the running container, use this command to make sure you're using the same UID as the webapp instead of `root`:

```shell
docker compose exec -u www-data: webapp bash
```

### Application's data

If the cache gets corrupted, you can clear it with:

```shell
docker compose exec webapp php ./src/console cache/clear
```

If indexes get corrupted, you can force a full reindex with:

```shell
docker compose exec webapp php ./src/console indexer/reindexAll
```

## 🤓 Advanced/Alternative configuration

You can mount a custom `local.ini` at `${WEBAPP_PATH}/src/etc/local.ini`.
Use this sample file if needed: [local.ini.sample](/src/etc/local.ini.sample).

Be advised that environment variables (if present) have a higher priority than the settings set in this file.


## ⏫ Upgrading

When upgrading, always check whether `.env` and/or `docker-compose.yml` have been updated. They might contain new settings or new default values to use.

- Pull the new images with `docker compose pull`
- Update the running stack with `docker compose up -d`
- Update the application with `docker compose exec webapp php ./src/console setup/upgrade`


## 💾 Backup

### Database

Run the following command (the resulting backup file will be placed in `DOCKER_MARIADB_BACKUP_VOLUME`):

```shell
docker compose --profile backup run --rm db-backup
```

### User Data (images, etc.)

User data are located in the folder at `DOCKER_WEBAPP_PUBLIC_USERDATA_VOLUME`. If you want to create a backup, you may use a simple `tar` command as follows (here using the default path to the folder as defined in `.env`):

```shell
tar -czf geeftlist-appdata_$(date +%F_%H-%M-%S).tar.gz ./data/geeftlist/pub/userdata
```
