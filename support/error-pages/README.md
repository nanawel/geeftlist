These files can be used on the reverse-proxy to properly
handle the cases where the application container cannot
respond.

## Nginx example

Assuming your vhost points to `/var/www/geeftlist` and you have deployed
this `error-pages/` folder inside it:

```
server {
    root /var/www/geeftlist
    server_name geeftlist.tld
    
    error_page 404 /error-pages/404.html;
    error_page 502 /error-pages/maintenance.html;
    error_page 503 /error-pages/maintenance.html;
    location ~ ^/error-pages/ {
        root /var/www/geeftlist;
        internal;
    }
    location ~ ^/error-pages-assets/ {
        root /var/www/geeftlist/error-pages;
    }
    
    ... (the rest of the configuration is not relevant here, see main README instead)
}
```
