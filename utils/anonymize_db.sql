SET @lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel nisl malesuada, ultrices arcu vel, bibendum ex. Maecenas ac lobortis erat. Proin tincidunt suscipit ornare. Aliquam eu felis tristique, feugiat sem sit amet, eleifend felis. Phasellus in dolor id est efficitur volutpat. Aliquam ac lobortis nisi. Donec sed lorem quis eros egestas eleifend. Integer tristique sem at neque aliquam rutrum. Etiam eu risus ac lectus fermentum pulvinar vel sit amet neque.

Pellentesque sed rutrum sem. Suspendisse ac risus facilisis, rhoncus est in, venenatis risus. Nulla non tempor diam. Curabitur eget tellus orci. Sed ex leo, auctor a lobortis quis, consectetur quis ipsum. Fusce varius felis eget blandit egestas. Ut scelerisque, libero vel interdum ultricies, dui lacus vehicula libero, vitae sagittis turpis ex nec velit. Integer egestas, ante eu dictum maximus, dolor urna vehicula lorem, id venenatis tortor eros sit amet sapien. Nulla mollis magna vitae enim tempus, vel mollis tortor porttitor. Nulla consequat, enim in commodo elementum, orci leo tincidunt augue, sagittis porttitor est odio vitae orci.

Donec iaculis interdum tellus, at sodales tellus gravida et. Maecenas sit amet gravida turpis. Proin tempus ligula sed quam ullamcorper, quis hendrerit quam venenatis. Vestibulum ullamcorper, ante euismod rutrum suscipit, risus sem vehicula est, id pellentesque erat ligula sed diam. Quisque venenatis nunc non sem lobortis, luctus consequat sem vulputate. Cras rhoncus, tortor eget elementum varius, libero tellus convallis nibh, et vulputate urna mi efficitur sapien. Maecenas interdum ipsum sed arcu laoreet eleifend.

Duis egestas nunc lacus, vitae posuere nunc vulputate non. Pellentesque ullamcorper, quam sed euismod tincidunt, lacus elit molestie dolor, id scelerisque dolor est ac arcu. Etiam pharetra ut lacus quis gravida. Pellentesque nec lectus a nunc pulvinar euismod sed placerat sapien. In dolor lorem, commodo viverra gravida et, ultrices sed quam. Pellentesque porttitor felis sed sagittis hendrerit. Aliquam sit amet felis massa. Pellentesque neque lectus, congue vel ipsum non, congue tempor nibh.

In gravida id eros eu bibendum. Proin fermentum eros at nunc consequat varius. Maecenas molestie justo id vulputate consectetur. Vestibulum dapibus dolor sed finibus viverra. Aliquam vulputate mauris suscipit, lacinia lorem eget, tincidunt sem. Nullam dictum mollis lacus, id gravida quam pellentesque id. Aliquam quis volutpat ante, vel congue quam. Curabitur vitae est consequat, venenatis nunc iaculis, ornare quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam mauris justo, congue non massa non, tristique dictum eros. Nunc non nisi sagittis, ultrices mi congue, ultrices quam. Suspendisse et felis tempor, fermentum ligula non, pretium magna.";


-- Password is "123456"
UPDATE `geefter`
SET email = CONCAT(REPLACE(email, "@", "+"), "@geeftlist.example.org"),
  password_hash = "$2y$10$jGBSDzWZ1t4/J5d.7aj.2uFRk3bgdY5s70kpYKmgjZuIgJQyD.prW";
UPDATE `geeftee`
SET email = CONCAT(REPLACE(email, "@", "+"), "@geeftlist.example.org");

UPDATE `gift`
SET label = LEFT(CONCAT("ANON-", MD5(label)), LENGTH(label)),
  description = LEFT(@lorem_ipsum, LENGTH(description));

UPDATE `discussion_topic`
SET title = LEFT(CONCAT("ANON-", MD5(title)), LENGTH(title));

UPDATE `discussion_post`
SET title = LEFT(CONCAT("ANON-", MD5(title)), LENGTH(title)),
  message = LEFT(@lorem_ipsum, LENGTH(message));

-- Set all geefter notifications to realtime
UPDATE `geefter`
SET `email_notification_freq` = 'realtime';
