#!/bin/bash

set -e

[ ! -z "$1" ] || { echo -e >&2 "No target environment specified.\nUsage:\t$0 [env]"; exit 1; }

THISDIR="$(dirname "$0")"
CONFIG_FILE=${CONFIG_FILE:-$HOME/.geeftlist_prod_db2$1.conf}
SKIP_ANONYMIZE_DATA=${SKIP_ANONYMIZE_DATA:-0}

if [ ! -f $CONFIG_FILE ]; then
    echo >&2 "ERROR: Missing config file $CONFIG_FILE"
    echo >&2
    exit 1
fi

source $CONFIG_FILE

echo "Copying from: $GEEFTLIST_MYSQL_PROD_SSH:$GEEFTLIST_MYSQL_PROD_DB"
echo "          to: $GEEFTLIST_MYSQL_TARGET_SSH:$GEEFTLIST_MYSQL_TARGET_DB..."
read -p "Confirm? (Ctrl-C to abort)"

echo "Dropping and recreating database $GEEFTLIST_MYSQL_TARGET_DB..."
echo "DROP DATABASE $GEEFTLIST_MYSQL_TARGET_DB; CREATE DATABASE $GEEFTLIST_MYSQL_TARGET_DB /*!40100 DEFAULT CHARACTER SET utf8 */;" \
    | ssh $GEEFTLIST_MYSQL_TARGET_SSH_OPTS $GEEFTLIST_MYSQL_TARGET_SSH "mysql --defaults-group-suffix=$GEEFTLIST_MYSQL_TARGET_GROUP_SUFFIX"
echo "Done"

ssh $GEEFTLIST_MYSQL_PROD_SSH_OPTS $GEEFTLIST_MYSQL_PROD_SSH \
    "mysqldump --defaults-group-suffix=$GEEFTLIST_MYSQL_PROD_GROUP_SUFFIX --add-drop-table $GEEFTLIST_MYSQL_PROD_DB" \
    | pv \
    | ssh $GEEFTLIST_MYSQL_TARGET_SSH_OPTS $GEEFTLIST_MYSQL_TARGET_SSH "mysql --defaults-group-suffix=$GEEFTLIST_MYSQL_TARGET_GROUP_SUFFIX"

if [ "$SKIP_ANONYMIZE_DATA" != "1" ]; then
    echo "Scrambling data..."
    ssh $GEEFTLIST_MYSQL_TARGET_SSH "mysql --defaults-group-suffix=$GEEFTLIST_MYSQL_TARGET_GROUP_SUFFIX" < $THISDIR/anonymize_db.sql
fi

echo "Done"

echo "Running upgrades..."
#ssh $GEEFTLIST_TARGET_SSH_OPTS $GEEFTLIST_TARGET_SSH docker exec $GEEFTLIST_TARGET_CONTAINER php src/cli.php setup/upgrade
echo "DISABLED -- PLEASE RUN UPGRADES MANUALLY"
echo
echo "Done"
