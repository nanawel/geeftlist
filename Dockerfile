# GEEFTLIST
# All-in-one production image definition.
# Requires:
#   - geeftlist-theme-builder (see docker/theme-builder/Dockerfile)
#   - geeftlist-web (see docker/base-web/Dockerfile)

# IMPORTANT! *All* args used in this file MUST be first declared here!
ARG base_web_image_name
ARG base_web_image_tag

###############################################################################
# THEME BUILDER INTERMEDIATE IMAGE
###############################################################################

ARG theme_builder_image_tag
ARG theme_name
FROM geeftlist-theme-builder:${theme_builder_image_tag} AS theme-build

COPY src/app/ui /themebuilder/app/ui

WORKDIR /themebuilder/app/ui/${theme_name:-default}
RUN mkdir -p /themebuilder/pub/asset \
 && time yarn install \
 && time npm run build # Replaces "foundation build" as from v6.3

###############################################################################
# COMPOSER VENDOR INTERMEDIATE IMAGE
###############################################################################

# Need to redeclare args for the next FROM (see https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact)
ARG base_web_image_name
ARG base_web_image_tag
FROM ${base_web_image_name}:${base_web_image_tag} AS composer-build

COPY resources/patches /var/www/resources/patches
COPY src/composer.json src/composer.lock /var/www/src/

RUN umask 0002 \
 && time /usr/local/bin/php /usr/local/bin/composer install \
    --working-dir=/var/www/src \
    --no-dev \
    --optimize-autoloader \
    --verbose

###############################################################################
# === Container provisioning ===
# Should be cached at each build.
# Starting from 0.17, see docker/web/Dockerfile instead.
###############################################################################

# Need to redeclare args for the next FROM (see https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact)
ARG base_web_image_name
ARG base_web_image_tag
FROM ${base_web_image_name}:${base_web_image_tag}

###############################################################################
# === Project installation ===
# PATHS:
#    Project:      /var/www/webapp
#    DocumentRoot: /var/www/html => /var/www/webapp/src/pub
# ARGS:
#    revision: Build # (commit SHA)
#    version: Full version string (if any)
###############################################################################

ARG revision
ARG version
ARG webapp_path=/var/www/webapp

# Apply webapp build configuration
ENV app__VERSION=${version:-${revision}} \
    app__ASSETS_VERSION_ID=${revision}
ENV WEBAPP_PATH=${webapp_path}

HEALTHCHECK --interval=2m --timeout=20s --retries=3 \
  CMD wget --no-verbose --tries=1 http://localhost/system/healthcheck -O- || exit 1

# Copy webapp files
WORKDIR ${webapp_path}
COPY . ${webapp_path}/

# Create var/* folders www-data has write access to webapp var/ folder
RUN mkdir -p -m 0775 \
    src/var/lock \
    src/var/log \
    src/var/reports \
    src/var/sessions \
    src/var/tmp \
    src/vendor \
 && chgrp -R www-data src/var \
 && chmod -R g+wX src/var

# Copy generated vendor dependencies files from previous stage
COPY --from=composer-build /var/www/src/vendor src/vendor/

# Generate EN dict from FR
RUN WEBAPP_LOCAL_INI=local.build.ini php ./src/cli.php dev/dictSync --language=en

# Copy generated theme files from previous stage
COPY --from=theme-build /themebuilder/pub/asset src/pub/asset
