#!/bin/bash

source "$(dirname $0)/../resources/shell/include.sh"

errorsFound=0

# Check for var/* folders
for dir in di lock sessions tmp; do
    dirContent="$(find $WEBAPP_SRC_DIR/var/$dir -type f)"
    test -z "$dirContent" \
        || { echo -e >&2 "** ERROR The directory $WEBAPP_SRC_DIR/var/$dir/ should be empty. Found files:\n$dirContent"; (( errorsFound++ )); }
done

[ $errorsFound -gt 0 ] && { echo >&2 "** FAIL $errorsFound errors found."; exit 1; } || echo "** OK No error found."
