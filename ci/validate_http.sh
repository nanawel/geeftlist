#!/bin/bash
set -e

dumpErrorLogIfNotEmpty() {
    rc=$?
    webappPath=${WEBAPP_PATH:-/var/www/webapp}
    errorLog="$webappPath/src/var/log/error.log"
    if [ $rc -gt 0 ] && [ -f "${errorLog}" ]; then
        echo && echo "==== ERROR LOG :: ${errorLog} ===="
        cat "${errorLog}"
    fi
    exit $rc
}

trap dumpErrorLogIfNotEmpty EXIT

echo "Checking homepage title at http://localhost/..."
curl -sSf http://localhost/ | grep '<title>Geeftlist'
echo "Checking API ping at http://localhost/api/system/ping..."
curl -sSf http://localhost/api/system/ping | grep 'pong!'

echo "Finished"
