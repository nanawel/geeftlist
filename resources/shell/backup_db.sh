#!/bin/bash -e
# Author: Nanawel <nanawel at gmail dot com>
set -o pipefail

if [ $# -eq 0 ]; then
    echo >&2 "Missing arguments"
    exit 250
fi

skip_confirm=0
db_host=''
db_port='3306'
db_database=''
db_user=''
db_password="$MYSQL_PWD"
db_options=''
target_dir="$(pwd)"
target_file=''
file_suffix=''

while getopts ':h:P:u:p:d:o:t:f:s:c' arg; do
    case "${arg}" in
        h)  db_host="${OPTARG}" ;;
        P)  db_port="${OPTARG}" ;;
        d)  db_database="${OPTARG}" ;;
        u)  db_user="${OPTARG}" ;;
        p)  db_password="${OPTARG}" ;;
        o)  db_options="${OPTARG}" ;;
        t)  target_dir="${OPTARG}" ;;
        f)  target_file="${OPTARG}" ;;
        s)  file_suffix="${OPTARG}" ;;
        c)  skip_confirm=1 ;;
        \?) echo "Invalid option: -${OPTARG}" >&2; exit 250 ;;
        :)  echo "Option -$OPTARG requires an argument." >&2; exit 250 ;;
        *)  echo "Unexpected option ${arg}" >&2; exit 250 ;;
    esac
done

for reqarg in "$db_database"; do
    [ ! -z "$reqarg" ] || (echo "Missing mandatory argument"; exit 250;)
done

if [ $? -gt 0 ]; then
    echo >&2 "Invalid arguments"
    exit 250
fi

now=$(date +%Y-%m-%d_%H-%M-%S)
hostname=$(hostname)
target_dir=$(readlink -f $target_dir)

# Build mysqldump command
mysqldump_cmd="mysqldump -P $db_port $db_database $db_options"
if [ ! -z "$db_host" ]; then
    mysqldump_cmd="$mysqldump_cmd -h $db_host"
fi
if [ ! -z "$db_user" ]; then
    mysqldump_cmd="$mysqldump_cmd -u $db_user"
fi

if [ -z "$target_file" ]; then
    target_file=${db_database}@${db_host}_${now}_${hostname}${file_suffix}.sql.gz
fi
fullpath=$(echo ${target_dir}/${target_file} | tr '//' '/')

echo "==MySQLDump Tool=="
echo "Hostname      : $hostname"
echo "Date          : $now"
echo "DB Host       : $db_host"
echo "DB Database   : $db_database"
echo "DB Port       : $db_port"
echo "DB User       : $db_user"
echo "DB Password   : $(echo "$db_password" | sed 's/./\*/g')"
echo "DB Options    : $db_options"
echo "Dump command  : $mysqldump_cmd"
echo "Target dir    : $target_dir"
echo "Target file   : $target_file"

if [ "$skip_confirm" != 1 ]; then
    echo "Confirm? (y/n)"
    read confirm
    if [ "$confirm" != "y" ]; then
        echo "Cancelled"
        exit 2
    fi
fi

export MYSQL_PWD="$db_password"

echo
time $mysqldump_cmd | gzip > ${fullpath}

if [ $? -ne 0 ]; then
    echo >&2 "mysqldump returned an error"
    exit 3
fi

echo "Dump finished!"
ls -hs ${fullpath}
