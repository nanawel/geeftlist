#!/bin/bash -e

set -e
[ "$RUN_VERBOSE" != "1" ] || set -x

[ "$(whoami)" != "root" ] || { echo >&2 "Cannot be run as root!"; exit 253; }

source "$(dirname $0)/include.sh"
PHPSTAN_BIN="${PHPSTAN_BIN:-$WEBAPP_SRC_DIR/vendor/bin/phpstan}"

xdebug=0
strictnessLevel=${PHPSTAN_LEVEL}
memoryLimit=${PHPSTAN_MEMORY_LIMIT:-1G}
configurationFile=

while getopts 'dc:l:m:o:' arg; do
    case "${arg}" in
        d)  xdebug=1 ;;
        c)  configurationFile=${OPTARG} ;;
        l)  strictnessLevel=${OPTARG} ;;
        m)  memoryLimit=${OPTARG} ;;
        o)  phpstanArgs="$phpstanArgs --${OPTARG}" ;;
        \?) echo "Invalid option: -${OPTARG}" >&2; exit 250 ;;
        :)  echo "Option -$OPTARG requires an argument." >&2; exit 250 ;;
        *)  echo "Unexpected option ${arg}" >&2; exit 250 ;;
    esac
done

# Remove parsed options to get a clean $* for the final command
shift "$((OPTIND-1))"

# New in PHPStan 0.12+: need to add /proc/cpuinfo to open_basedir
phpArgs="-d open_basedir='/var/www/:/tmp/:/usr/local/bin/:/usr/local/lib/:/proc/cpuinfo'"

cd "$WEBAPP_DIR"

# Always run composer install to ensure dependencies are up-to-date with composer.lock
(umask 0002 && time composer install --working-dir $WEBAPP_SRC_DIR)

if [ "$xdebug" == "1" ]; then
    phpArgs="$phpArgs -d xdebug.mode=debug -d xdebug.start_with_request=1"
    phpstanArgs="$phpstanArgs --xdebug"
else
    phpArgs="$phpArgs -d xdebug.mode=off"
fi
if [ ! -z "$configurationFile" ]; then
    phpstanArgs="$phpstanArgs --configuration=$configurationFile"
fi

# Always run composer install to ensure dependencies are up-to-date with composer.lock
(umask 0002 && time composer install --working-dir $WEBAPP_SRC_DIR)

if [ ! -e "$PHPSTAN_BIN" ]; then
    export PHPSTAN_BIN=${COMPOSER_VENDOR_DIR:-$WEBAPP_SRC_DIR/vendor}/bin/phpstan
fi

cd "$WEBAPP_SRC_DIR"

phpstanArgs="$phpstanArgs --memory-limit=$memoryLimit"

if [ ! -z "$strictnessLevel" ]; then
    phpstanArgs="$phpstanArgs --level=$strictnessLevel"
fi

cmd="$PHP_BIN $phpArgs $PHPSTAN_BIN analyse $phpstanArgs $*"
echo "$cmd"
time $cmd
phpstanReturnCode=$?

# Must return the result of the PHPUnit execution
exit $phpstanReturnCode
