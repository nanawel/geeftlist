#!/bin/bash
# Author: nanawel@gmail.com
# Usage:
#    source <(./loadenv.sh [-o] [env-file])
#
#   -o         Override values that are already present in current environment.
#   env-file   Which env file to load (default: .env)

override="${override:-0}"

while getopts 'o' arg; do
    case "${arg}" in
        o)  override=1 ;;
        \?) echo "Invalid option: -${OPTARG}" >&2; exit 250 ;;
        :)  echo "Option -$OPTARG requires an argument." >&2; exit 250 ;;
        *)  echo "Unexpected option ${arg}" >&2; exit 250 ;;
    esac
    shift
done

envFile="${1:-.env}"

which gawk >/dev/null 2>&1 || { echo "Missing gawk." >&2; exit 1; }
[ -f "$envFile" ] || { echo "Cannot find .env file." >&2; exit 2; }

cat "$envFile" \
    | awk -F'=' -v override=${override} '/^[^#].+=/{ if (override || ! ENVIRON[$1]) print "export " $1 "=" $2 }'
