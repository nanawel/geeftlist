#!/bin/bash -e
#
# Usage:
#     ./run_unit_tests.sh [ -dpP ] [ -s test-suite ] [ -u phpunit-option ] [ -G GROUP ] [ test-class ]
# Ex:
#     ./run_unit_tests.sh -I -R app/code/Geeftlist/Test/Model/GeefteeTest.php
#
# Other example (= debug a single test class while keeping the same database between each run):
#     ./run_unit_tests.sh -d app/code/Geeftlist/Test/Model/GeefteeTest.php
#
#  -d : Enable XDebug
#  -G : Test group ("api" is the only supported group atm)
#  -p : Enable XDebug profiler
#  -P : Enable Xhprof profiler
#  -R : Shortcut to set RECREATE_DATABASE=1 to recreate database before running tests (usually used with -I)
#  -I : Shortcut to set INJECT_SAMPLE_DATA=1 to inject sample data before running tests (usually used with -R)
#  -s : Test suite to run
#  -u : Additional phpunit argument (ex: -u stop-on-failure)
#
# Notice: Class path must be relative to src/

function printHelp() {
    tail -n+3 $0 | awk 'BEGIN {stop=0} /^\s*$/{stop=1} /^# /{if (stop) next; gsub(/# /, ""); print}'
}

set -e
[ "$RUN_VERBOSE" != "1" ] || set -x

[ "$(whoami)" != "root" ] || { echo >&2 "Cannot be run as root!"; exit 253; }

skipComposerInstall=0
xdebug=0
xdebugProfiler=0
xhprofProfiler=0
recreateDatabase=0
injectSampleData=0
configFileSuffix=
testGroup=
logJunit=0
phpUnitArgs="--stderr"

while getopts 'cdg:G:hIlpPRs:u:U' arg; do
    case "${arg}" in
        c)  skipComposerInstall=1 ;;
        d)  xdebug=1 ;;
        g)  configFileSuffix="${OPTARG}" ;;
        G)  testGroup="${OPTARG}" ;;
        h)  printHelp; exit 0 ;;
        I)  injectSampleData=1 ;;
        l)  logJunit=1 ;;
        p)  xdebugProfiler=1 ;;
        P)  xhprofProfiler=1 ;;
        R)  recreateDatabase=1 ;;
        s)  testSuite="${OPTARG}" ;;
        u)  phpUnitArgs="$phpUnitArgs --${OPTARG}" ;;
        U)  upgradeSchema=1 ;;
        \?) echo "Invalid option: -${OPTARG}" >&2; exit 250 ;;
        :)  echo "Option -$OPTARG requires an argument." >&2; exit 250 ;;
        *)  echo "Unexpected option ${arg}" >&2; exit 250 ;;
    esac
done

source "$(dirname $0)/include.sh"
PHPUNIT_BIN="${PHPUNIT_BIN:-$WEBAPP_SRC_DIR/vendor/phpunit/phpunit/phpunit}"

# Remove parsed options to get a clean $* for the final command
shift "$((OPTIND-1))"

phpArgs="$PHP_ARGS"

cd "$WEBAPP_DIR"

# Make sure i18n is up-to-date (can cause wrong failures during the run otherwise)
./src/cli.php dev/dictSync --language=en

if [ "$skipComposerInstall" == "0" ]; then
    # Always run composer install to ensure dependencies are up-to-date with composer.lock
    (umask 0002 && time $PHP_BIN -d xdebug.mode=off $(which composer) install --working-dir $WEBAPP_SRC_DIR)
fi

if [ ! -e "$PHPUNIT_BIN" ]; then
    export PHPUNIT_BIN=${COMPOSER_VENDOR_DIR:-$WEBAPP_SRC_DIR/vendor}/phpunit/phpunit/phpunit
fi

cd "$WEBAPP_SRC_DIR"

if [ "$xdebug" == "1" ]; then
    phpArgs="$phpArgs -d xdebug.start_with_request=yes -d xdebug.start_upon_error=yes"
fi
if [ "$xdebugProfiler" == "1" ]; then
    phpArgs="$phpArgs -d xdebug.mode=profile"
fi
if [ "$xdebug" != "1" ] && [ "$xdebugProfiler" != "1" ]; then
    phpArgs="$phpArgs -d xdebug.mode=off"
fi
if [ "$xhprofProfiler" == "1" ]; then
    export XHPROF_ENABLE=1
fi
if [ "$recreateDatabase" == "1" ]; then
    export RECREATE_DATABASE=1
fi
if [ "$upgradeSchema" == "1" ]; then
    export UPGRADE_SCHEMA=1
fi
if [ "$injectSampleData" == "1" ]; then
    export INJECT_SAMPLE_DATA=1
fi

[ -z "$configFileSuffix" ] || configFileSuffix="-$configFileSuffix"
[ ! -z "$testSuite" ] || testSuite="all"
[ -z "$testGroup" ] || phpUnitArgs="$phpUnitArgs --group $testGroup"

if [ "$logJunit" == "1" ]; then
    phpUnitLogFile="$WEBAPP_REPORTS_DIR/phpunit$configFileSuffix.log.xml"
    phpUnitArgs="$phpUnitArgs --log-junit $phpUnitLogFile"
fi

phpUnitArgs="$phpUnitArgs -c $WEBAPP_SRC_DIR/test/phpunit${configFileSuffix}.xml"
phpUnitArgs="$phpUnitArgs --testsuite $testSuite"

cmd="$PHP_BIN $phpArgs $PHPUNIT_BIN $phpUnitArgs $*"
echo "$cmd"
time $cmd
phpunitReturnCode=$?

if [ "$logJunit" == "1" ] && [ -f "$phpUnitLogFile" ]; then
    cp "$phpUnitLogFile" "$WEBAPP_REPORTS_DIR/phpunit$configFileSuffix.log.$(date +%F_%H-%M-%S).xml"
fi

# Must return the result of the PHPUnit execution
exit $phpunitReturnCode
