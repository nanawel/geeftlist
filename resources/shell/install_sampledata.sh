#!/bin/bash -e

source "$(dirname $0)/include.sh"

# Always run composer install to ensure dependencies are up-to-date with composer.lock
composer install --working-dir $WEBAPP_SRC_DIR

waitForDbUp

# Force clear cache
$PHP_BIN $WEBAPP_SRC_DIR/cli.php cache/clear

$PHP_BIN $WEBAPP_SRC_DIR/cli.php setup/installSampleData

# Run upgrade too just in case
$PHP_BIN $WEBAPP_SRC_DIR/cli.php setup/upgradeSampleData
