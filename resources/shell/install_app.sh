#!/bin/bash -e

source "$(dirname $0)/include.sh"

waitForDbUp

if [ "$1" == '--drop-first' ]; then
    $PHP_BIN $WEBAPP_SRC_DIR/cli.php setup/install --dropFirst
else
    if ! $PHP_BIN $WEBAPP_SRC_DIR/cli.php setup/isInstalled; then
        $PHP_BIN $WEBAPP_SRC_DIR/cli.php setup/install
    fi
fi

$PHP_BIN $WEBAPP_SRC_DIR/cli.php setup/upgrade
