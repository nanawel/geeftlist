#!/bin/bash -e

set -e
[ "$RUN_VERBOSE" != "1" ] || set -x

[ "$(whoami)" != "root" ] || { echo >&2 "Cannot be run as root!"; exit 253; }

source "$(dirname $0)/include.sh"
PHPLOC_BIN="${PHPLOC_BIN:-$WEBAPP_SRC_DIR/vendor/bin/phploc}"

excludeOpt='--exclude app/code/Geeftlist/setup/'
logJson=0
jsonLogOpt=
countTestsOpt=
phplocArgs=${phpLocArgs:-$WEBAPP_SRC_DIR/app/}

while getopts 'e:lto:' arg; do
    case "${arg}" in
        e)  excludeOpt="${excludePath} --exclude ${OPTARG}" ;;
        l)  logJson=1 ;;
        t)  phplocArgs="$phplocArgs --count-tests" ;;
        o)  phplocArgs="$phplocArgs --${OPTARG}" ;;
        \?) echo "Invalid option: -${OPTARG}" >&2; exit 250 ;;
        :)  echo "Option -$OPTARG requires an argument." >&2; exit 250 ;;
        *)  echo "Unexpected option ${arg}" >&2; exit 250 ;;
    esac
done

# Remove parsed options to get a clean $* for the final command
shift "$((OPTIND-1))"

phpArgs=""

cd "$WEBAPP_DIR"

# Always run composer install to ensure dependencies are up-to-date with composer.lock
(umask 0002 && time composer install --working-dir $WEBAPP_SRC_DIR)

if [ ! -e "$PHPLOC_BIN" ]; then
    export PHPLOC_BIN=${COMPOSER_VENDOR_DIR:-$WEBAPP_SRC_DIR/vendor}/bin/phploc
fi

if [ "$logJson" == "1" ]; then
    jsonLogFile="$WEBAPP_REPORTS_DIR/phploc.log.json"
    phplocArgs="$phplocArgs --log-json $jsonLogFile"
fi

cd "$WEBAPP_SRC_DIR"

phplocArgs="$excludeOpt $phplocArgs"

cmd="$PHP_BIN $phpArgs $PHPLOC_BIN $phplocArgs $*"
echo "$cmd"
time $cmd
phplocReturnCode=$?

if [ "$logJson" == "1" ] && [ -f "$jsonLogFile" ]; then
    cp "$jsonLogFile" "$WEBAPP_REPORTS_DIR/phploc.log.$(date +%F_%H-%M-%S).json"
fi

# Must return the result of the PHPUnit execution
exit $phplocReturnCode
