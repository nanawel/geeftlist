#!/bin/bash

#set -e

WEBAPP_DIR="$(readlink -f $(dirname $0)/../..)"
WEBAPP_SRC_DIR="$WEBAPP_DIR/src"
WEBAPP_REPORTS_DIR="$WEBAPP_DIR/src/var/reports"

if [ -z "$PHP_BIN" ]; then
    export PHP_BIN="$(which php)"
fi

function waitForDbUp() {
    $PHP_BIN $WEBAPP_SRC_DIR/cli.php system/waitForDb
}

# Print environment variables for debug
env | sort
echo "Current user: $(id)"
echo "umask       : $(umask)"
