#!/bin/bash -e

set -e
[ "$RUN_VERBOSE" != "1" ] || set -x

[ "$(whoami)" != "root" ] || { echo >&2 "Cannot be run as root!"; exit 253; }

source "$(dirname $0)/include.sh"
PHPCS_BIN="${PHPCS_BIN:-$WEBAPP_SRC_DIR/vendor/bin/phpcs}"

xdebug=0

while getopts 'c:djo:O:' arg; do
    case "${arg}" in
        c)  codeDirs="$codeDirs ${OPTARG}" ;;
        d)  xdebug=1 ;;
        j)  logJunit=1 ;;
        o)  PHPCSArgs="$PHPCSArgs --${OPTARG}" ;;
        O)  PHPCSArgs="$PHPCSArgs -${OPTARG}" ;;
        \?) echo "Invalid option: -${OPTARG}" >&2; exit 250 ;;
        :)  echo "Option -$OPTARG requires an argument." >&2; exit 250 ;;
        *)  echo "Unexpected option ${arg}" >&2; exit 250 ;;
    esac
done

# Remove parsed options to get a clean $* for the final command
shift "$((OPTIND-1))"

cd "$WEBAPP_DIR"

# Always run composer install to ensure dependencies are up-to-date with composer.lock
(umask 0002 && time composer install --working-dir $WEBAPP_SRC_DIR)

if [ "$xdebug" == "1" ]; then
    phpArgs="$phpArgs -d xdebug.mode=debug -d xdebug.start_with_request=1"
    PHPCSArgs="$PHPCSArgs --xdebug"
else
    phpArgs="$phpArgs -d xdebug.mode=off"
fi
if [ ! -z "$configurationFile" ]; then
    PHPCSArgs="$PHPCSArgs --configuration=$configurationFile"
fi

if [ ! -e "$PHPCS_BIN" ]; then
    export PHPCS_BIN=${COMPOSER_VENDOR_DIR:-$WEBAPP_SRC_DIR/vendor}/bin/phpcs
fi

if [ "$logJunit" == "1" ]; then
    junitLogFile="$WEBAPP_REPORTS_DIR/phpcs.junit.xml"
    PHPCSArgs="$PHPCSArgs --report=junit --report-file=$junitLogFile"
fi

cd "$WEBAPP_SRC_DIR"

if [ -z "$codeDirs" ]; then
    codeDirs='app/code/ app/template/'
fi

cmd="$PHP_BIN $phpArgs $PHPCS_BIN $PHPCSArgs $codeDirs $*"
echo "$cmd"
time $cmd
PHPCSReturnCode=$?

# Must return the result of the PHPUnit execution
exit $PHPCSReturnCode
