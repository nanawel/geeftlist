**Version 0.48.4** (2024-10-24)

- Fix for the "gift lists" field on the gift editing form (thanks Gilles)
- Added documentation for deploying an instance via Docker Compose
- Code cleanup and reformatting with PHPCS
- Added configuration for protecting the cron job launch endpoint
- Minor fixes

**Versions 0.48.2 & 0.48.3** (2024-07-08)

- Fixed (again) the recording of CSP violations

**Version 0.48.1** (2024-07-07)

- Fixed the recording of CSP violations
- Fixed the "Save" button on the popup for adding to lists, which sometimes prevented clicking
- Updated frontend and backend dependencies

**Version 0.48.0** (2024-07-04)

- ⭐ Added the invitation system
- Removed configuration from the database (EnvConfig), which was redundant with configuration by environment variables
- Added configurable CSP headers
- Refactorings and cleanups

**Version 0.47.2** (2024-06-09)

- Silent fallback in case of error when retrieving a geeftee/geefter's avatar

**Version 0.47.1** (2024-06-05)

- Fixed permission management for retrieving information about geeftees and geefters

**Version 0.47.0** (2024-04-29)

- Upgraded to PHP 8.3
- Deep refactoring with added explicit typing (PHP 8+ syntax)
- Removed unnecessary layers in the code for general simplification
- Fixed a regression following the abandonment of MongoDB
- Updated frontend libraries
- Improved accessibility of the navigation menu (thanks Marie)
- Cleanup and various other fixes

**Version 0.46.1** (2024-02-25)

- Fixed domain management for cookies, which sometimes blocked login
- Quick and dirty reimplementation of the developer profiler without MongoDB

**Version 0.46.0** (2024-01-28)

- Fixed the sending of certain emails, especially for gifts intended for multiple geeftees
- Removed dependency on MongoDB for activity history
- Cleanup and various other fixes

**Version 0.45.6** (2023-11-19)

- Fixed confirmation on form submission (oops)

**Version 0.45.5** (2023-11-18)

- Added confirmation on exit from main forms

**Version 0.45.4** (2023-11-09)

- Fixed visibility of public topics on gifts for the creator

**Version 0.45.3** (2023-11-06)

- Fixed regression on Prometheus endpoint

**Version 0.45.2** (2023-11-05)

- Fixed rendering of notification messages after an action (thanks Noémie ❤)
- Fixed translations of grids (DataTables)
- Refactored management of the development environment and unit tests

**Version 0.45.1** (2023-10-22)

- Added a CMS block for a warning on the login page

**Version 0.45.0** (2023-10-22)

- Migrated from Gandi to OVH following [the acquisition of the former by a company with questionable practices](https://www.nextinpact.com/article/71103/gandi-fusionne-avec-total-webhosting-solutions-tws-pour-devenir-your-online-et-cela-inquiete)
- Upgraded to PHP 8.2
- Updated base images and third-party libraries
    - FatFree 3.8.2
    - Foundation 6.8.1
- Upgraded to NodeJS 20 for the _theme builder_
- Overhauled the deployment system and cleaned up in preparation for opening the source repository
- Cleanup, fixes, and various refactorings

**Version 0.44.1** (2023-10-18)

- Fixed cookie domain management
- Upgraded to NodeJS 16 for the _theme builder_

**Version 0.44.0** (2023-06-25)

- Added the ability to pin one or more messages on a gift page
- Added new shortcut entries in the menu
- Fixed the form for adding gifts in bulk (broken since 0.43.0)
- The profile page of a geeftee now only displays available gifts
- Fixed the inability to archive a gift when the geeftee is no longer a close contact
- Fixed redirection after archiving a gift
- Various cleanups and refactorings
- Updated base images and third-party libraries

**Version 0.43.0** (2023-04-27)

- Ability to mention one or more geefters when sending a discussion message
- Fixed/reimplemented the saving of pagination on grids
- Fixed the sorting of gifts on a geeftee's page
- Upgraded to Fat Free Framework 3.8
- Upgraded to PHP 8.1
- Other fixes and various adjustments

**Version 0.42.3** (2022-12-20)

- Fixed list management (especially invalid display when reserving a gift)

**Version 0.42.2** (2022-11-20)

- Fixed family deletion
- Added CSRF token checks on sensitive actions
- Added missing translation

**Version 0.42.1** (2022-11-08)

- Fixed notifications for "dormant" reservations

**Version 0.42.0** (2022-09-07)

- Created lists on the fly from the reservation page
- Added the ability to delete geeftees by their creator
- Fixed the display of the amount on gifts for oneself
- The _Help_ and _About_ pages are now accessible to visitors
- Updated dependencies (back & front)
- Various fixes

**Version 0.41.1** (2022-06-11)

- Added monitoring metrics for the cron

**Version 0.41.0** (2022-06-10)

- Added a special icon for gifts with _private_ visibility
- Hid the public discussion thread in cases where it is not necessary
- Upgraded to FontAwesome 6
- Fixed a lack of reindexing after linking a geeftee after registration (thanks Nico ❤)
- Fixed the total of reservations when there is no amount target
- Fixed a regression on the gift grids (search and sort KO)
- Other refactorings, fixes, and cleanups

**Version 0.40.1** (2022-03-10)

- Fixed symfony/mailer configuration

**Version 0.40.0** (2022-03-09)

- Fixed regressions introduced in 0.39.0 (oops)
    - Missing notifications
    - Display of a reservation at "0,00 €" when no price is specified
- Fixed the behavior of DataTables when deleting elements
- Added the distinction between reservation/purchase on the dashboard widget
- Replaced Swiftmailer (obsolete) with symfony/mailer
- Other internal refactorings and various cleanups

**Version 0.39.0** (2022-02-28)

- ⭐ Added personal gift lists _(not shareable for now)_
- Refactored UX/UI of gift list pages for mobile
- Refactored the management of relationships between entities
- Refactored the structure of SCSS styles
- Various fixes and improvements
- Updated third-party libraries
- Updated PHP (7.4.28)

**Version 0.38.1** (2021-12-01)

- Fixed the rendering of placeholders between Chrome and Firefox

**Version 0.38.0** (2021-11-22)

- Added the number of messages on the "Discuss" button of gifts
- New color palette for the interface
- UX fixes (thanks Marie ❤)
- Various fixes and refactorings

**Version 0.37.3** (2021-11-14)

- Fixed/refactored the management of CMS blocks and message banners

**Version 0.37.2** (2021-11-07)

- Improved reservation notifications (thanks Fanny ❤)
- Fixed notifications when deleting a reservation

**Version 0.37.1** (2021-11-02)

- Fixed the rendering of the _Xmas Hat_ on mobile

**Version 0.37.0** (2021-10-22)

- The _hamburger_ button is now on the "right" side of the header (thanks Marie ❤)
- Refactored the navigation menu and various UI/UX elements (thanks Marie ❤)
- Fixed the notification email when adding a gift to multiple geeftees
- Updated Foundation (6.7.3) and JS libs
- Various fixes and refactorings

**Version 0.36.0** (2021-08-16)

- Managed the display of posts from geefters who have deleted their account
- Fixed the naming of geeftees in reservation reminder emails
- Fixed the handling of historical names with now-forbidden characters (thanks Cyril ❤)
- Strengthened session security
- Updated third-party libraries
- Various fixes and refactorings

**Version 0.35.0** (2021-02-22)

- Added a notification to reservation holders when a gift is archived
- Fixed invalid autocompletion of the date of birth on Firefox
- Fixed the request to link a geeftee when you are the creator yourself
- Updated third-party libraries
- Various fixes and refactorings

**Version 0.34.1** (2021-01-24)

- Fixed regressions following the upgrade to MariaDB 10.3

**Version 0.34.0** (2021-01-24)

- Dedicated error page when a gift does not exist/no longer exists
- Fixed escaping of objects in emails
- Fixed the archiving confirmation message
- Hid the gift addition button for non-close geeftees in the list
- Fixed the missing "geeftee" column in gift grids for my close contacts
- Moved to PHP 7.4
- Restructured the application tree for better security
- Updated third-party libraries (Laminas, PHPUnit, JWT, ...)
- Various fixes and refactorings

**Version 0.33.0** (2020-12-13)

- ⭐ Ability to assign a gift to **multiple geeftees**
- Improved geeftee selectors
- Updated Fat-Free (3.7.3)
- Updated third-party libraries
- Various fixes and refactorings

**Version 0.32.0** (2020-12-01)

- Added support for [WebP images](https://fr.wikipedia.org/wiki/WebP) on gifts and avatars
- Added the "copy and archive" function to easily duplicate a recurring gift (thanks Lilou ❤)
- Fixed the invitation of geefters by email (thanks Bruno ❤)
- Fixed grid sorting
- Various fixes and refactorings

**Version 0.31.3** (2020-11-19)

- Fixed the "Recent Activity" widget (thanks Noémie ❤)

**Version 0.31.2** (2020-11-18)

- Fixed the profile update form

**Version 0.31.1** (2020-11-17)

- Fixed notification emails

**Version 0.31.0** (2020-11-17)

- Notification emails for "dormant" reservations

**Version 0.30.2** (2020-11-14)

- Christmas mode 🎅

**Version 0.30.1** (2020-11-13)

- Fixed error handling when sending notification emails

**Version 0.30.0** (2020-11-11)

- Added support for emojis in main fields (thanks Christophe ❤)
- Gift images and geefter avatars can now be updated from a URL
- Fixed duplicate notifications in the case of an uncovered gift

**Version 0.29.0** (2020-10-01)

- Refactored the navigation menu for better usability on mobile (mmenujs)
- Fixed the display of messages from non-close contacts on a gift's discussions
- Updated third-party libraries

**Version 0.28.1** (2020-07-01)

- Fixed email HTML templates

**Version 0.28.0** (2020-06-30)

- ⭐ Integrated email graphics
- Micro-optimizations for indexing
- Updated third-party libraries
- Various fixes

**Version 0.27.0** (2020-04-23)

- Added **filters and sorting** on gift list pages
- Contact page accessible without prior login
- Updated Foundation 6.6.3
- Fixed the number of available gifts in geeftee cards

**Version 0.26.0** (2020-04-15)

- Optimized indexing
- Fixed the display of the Reserve button on the reservation grid
- Added a persistent banner if no families yet
- Various refactorings and fixes
- Updated third-party libraries

**Version 0.25.0** (2020-03-05)

- Added management of access to gifts for geeftees who are no longer close
- API: Fixed the update of the last login date
- API: Improved filter management (thanks Bruno ❤)
- Fixed notifications
- Updated third-party libraries

**Version 0.24.7** (2019-12-15)

- Fixed the uncovered gift selector
- Moved the gift badge selector outside of advanced settings

**Version 0.24.6** (2019-12-11)

- Optimized gift list pages (dropdowns)

**Version 0.24.5** (2019-12-08)

- Improved message banners

**Version 0.24.4** (2019-12-05)

- Optimized gift list pages (badges)
- Added a tooltip on reservations

**Version 0.24.3** (2019-12-02)

- Fixed the family addition link when creating a geeftee
- Fixed loggers

**Version 0.24.2** (2019-11-26)

- Fixed the geeftee linking page

**Version 0.24.1** (2019-11-24)

- Fixed session management on mobile
- More fine-grained logging

**Version 0.24.0** (2019-11-20)

- Sent a notification when modifying a reserved gift
- Improved dashboard performance to speed up login
- API: Added support for [included resources](https://jsonapi.org/format/#document-compound-documents)
- Various fixes and adjustments

**Version 0.23.0** (2019-09-30)

- ⭐ Added **badges** on geeftees and gifts (thanks Lilou ❤)
- Added a captcha on the registration form
- Fixed a bug preventing account deletion
- Various optimizations (DB, cache, etc.)
- Various fixes

**Version 0.22.1** (2019-08-27)

- Fixed a bug preventing the application from running in production

**Version 0.22.0** (2019-08-26)

- Added **persistent sessions**
- Security fixes (thanks Cyril ❤)
- Various fixes

**Version 0.21.0** (2019-07-02)

- API: Refactoring, cleanup, optimizations
- API: Added management of _compound documents_
- The owner of a family is notified when a new member joins
- The "Leave" button no longer displays for family owners
- Moved to PHP 7.3
- Various fixes

**Version 0.20.0** (2019-06-13)

- Added the **archiving request** for gifts
- Added the **reporting** of gifts to the creator or administrator
- Optimized reindexing upon family creation

**Version 0.19.3** (2019-06-01)

- Fixed cronjob

**Version 0.19.2** (2019-05-31)

- Fixed breadcrumbs (thanks Cyril ❤)
- Fixed the overlay
- API: Support for JSON-API compliant sorting
- Various fixes and refactorings

**Version 0.19.1** (2019-05-12)

- Fixed the loading overlay for long actions
- Fixed empty prices in bulk gift addition
- API: Added missing tests

**Version 0.19.0** (2019-04-14)

- Fixed links to the account confirmation page in the welcome email
- Displayed images in gift grids
- Refactored the mobile rendering of gift grids
- Added the loading overlay for long actions
- Updated Fat-Free (3.6.5)
- Security: Fixed a missing escape on the edit form (thanks Cyril ❤)

**Version 0.18.0** (2019-03-10)

- ⭐ Added **images** on gifts
- API: Added support for account creation
- API: JSON-API compliance fixes
- Fixed redirections for links containing fragments (especially in emails)

**Version 0.17.0** (2019-02-03)

- Added **avatars** on geeftees
- Saved the state of gift grids/lists during actions
- Fixed URL tags in fields supporting BBCode
- API: Fixed pagination management

**Version 0.16.1** (2019-02-01)

- API: Fixes (thanks Bruno ❤)

**Version 0.16.0** (2018-12-05)

- Partial design overhaul
- Added the **duplication** function for gifts
- Added management of notification sending frequency
- Various UI fixes

**Version 0.15.1** (2018-11-20)

- Fixed the exclusion of a family member, which could empty the family entirely! (thanks Bruno ❤)

**Version 0.15.0** (2018-11-15)

- Implemented the API with JSON-API (partial - in progress)
- Fixed available actions for invitation requests
- Fixed recipients of reservation notifications
- Other various fixes

**Version 0.14.1** (2018-11-06)

- Fixed the sending of new discussion notifications

**Version 0.14.0** (2018-09-22)

- ⭐ Added the **"uncovered gift"** feature, allowing the creation of a gift for a close contact and for them to see it
- Fixed gift lists (archived gifts not hidden)
- API: Fixes and improvements
- Other various fixes
- Moved to PHP 7.2

**Version 0.13.1** (2018-09-15)

- API fixes and improvements (thanks Bruno ❤)

**Version 0.13.0** (2018-09-14)

- ⭐ Added management of **gift visibility by family or by geefter**
- ⭐ Added the ability to create **private gifts**
- Automatic login upon account creation (thanks Cyril ❤)
- Normalized the API with [JSON-API](http://jsonapi.org)
- Security fixes ([OWASP](https://www.owasp.org/index.php/Session_Management_Cheat_Sheet#Renew_the_Session_ID_After_Any_Privilege_Level_Change))
- Optimizations for the display of gift list pages
- Various fixes and refactorings

**Version 0.12.1** (2018-04-15)

- Fixed the confirmation dialog (thanks Pierre ❤)

**Version 0.12.0** (2018-03-13)

- Added the display of the remaining amount on gifts
- Fixed the grouping of notifications before sending
- New **"tips"** widget on the dashboard
- New **"upcoming birthdays"** widget on the dashboard
- Linking a new geefter to an existing geeftee only upon email validation
- Notification to the new owner upon family transfer
- Support for **account deletion**
- FAQ and help page improvements
- Various fixes and adjustments

**Version 0.11.0** (2018-03-02)

- Ability to change your email address
- Current password required to update your password
- Notification to the creator of a geeftee upon registration of the corresponding geefter
- Updated Foundation (6.4)
- Security fix in emails
- Other minor fixes

**Version 0.10.0** (2018-02-08)

- ⭐ Added a **"My Close Contacts"** page listing all geefters in my families
- Notification by email upon deletion of a reserved gift
- Fixed automatic links in text bodies
- Overhauled gift grids
    - asynchronous loading, sorting, pagination
    - ability to display or hide archived gifts
- Overhauled forms, partial on-the-fly validation
- Geeftees are now modifiable by their creators
- Various fixes and improvements

**Version 0.9.2** (2017-12-15)

- Fixed the change of gift assignment
- Fixed the display of discussions on the dashboard
- Fixed the link to the profile on a post
- Fixed the display of descriptions in lists
- Various fixes

**Version 0.9.1** (2017-12-13)

- Fixed REST API

**Version 0.9.0** (2017-12-12)

- ⭐ Added **grouped notification emails**
- Explicit error message when choosing an existing username
- Empty prices are no longer converted to "0,00 €"
- Redirection to the family page now reopens the geeftee from which you came (and not the first one)
- Initial implementation of the **REST API**
- Various fixes and refactorings

**Version 0.8.1** (2017-11-16)

- Fixed the link for creating a gift from the family page

**Version 0.8.0** (2017-11-15)

- Added **support for BBCode formatting** to gift descriptions and discussions
- Multiple UX adjustments and fixes (thanks Cyril ❤)
- Various fixes

**Version 0.7.1** (2017-10-09)

- Minor fixes

**Version 0.7.0** (2017-10-09)

- Added the contact form once logged in
- Minor fixes and adjustments

**Version 0.6.5** (2017-09-09)

- ⭐ Migrated to a **dedicated server** and moved to the address [www.geeftlist.com](https://www.geeftlist.com)
- Minor fixes

**Version 0.6.4** (2017-08-29)

- Optimized email sending

**Version 0.6.3** (2017-08-26)

- Fixed notification email for new gifts
- Fixed email subject encoding (thanks Quentin ❤)

**Version 0.6.2** (2017-08-23)

- Fixed discussions

**Version 0.6.1** (2017-08-21)

- Fixed emails

**Version 0.6.0** (2017-08-21)

- ⭐ Added email notifications
    - new gift
    - new family member
    - new reservation
    - etc.
- General architecture refactoring (using [PHP-DI](http://php-di.org/))
- Cleanup and various fixes

**Version 0.5.1** (2017-07-04)

- Fixed the display of error messages
- Fixed the display of activities on the dashboard

**Version 0.5.0** (2017-06-13)

- Added **family activity history** with a preview on the dashboard
- Refactored and cleaned up the custom F3 layer
- Various fixes

**Version 0.4.0** (2017-05-26)

- Added links to gift pages on the "Reservations" widget of the dashboard
- Replaced the custom logging system with Monolog
- Refactored and cleaned up the custom F3 layer
- Various fixes

**Version 0.3.0** (2017-05-18)

- Added discussions on gift pages
- Various fixes and optimizations

**Version 0.2.5** (2017-04-18)

- Improved management of deleted gifts with reservations
- Added **email invitation**
- Added the required confirmation when inviting someone to a family
- Fixed session cleanup upon logout
- Updated Foundation (6.2 -> 6.3)
- Various fixes

**Version 0.2.4** (2017-04-12)

- Added tokens to prevent CSRF
- Added CMS blocks
- Added the "About" page
- Various fixes and optimizations

**Version 0.2.3** (2017-04-07)

- Added the concept of priority to gifts
- Added the cancellation of linking requests
- Various fixes and optimizations

**Version 0.2.1** (2017-04-05)

- Added the cancellation of application requests
- Added the date of birth to the profile
- Fixed the caching of static elements
- Various fixes and optimizations

**Version 0.2.0** (2017-03-30)

- Added the **request to link to a geeftee**
- Added **applications to families**
- Major cleanup and refactorings
- Various fixes and optimizations

**Version 0.1.0** (2017-02-28)

- Initial release
