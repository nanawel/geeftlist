![Geeflist](src/app/ui/default/src/assets/img/logo.png)

## 🤖 Presentation

Geeftlist is a PHP web application designed to simplify and coordinate the sharing, reservation, and purchase of gifts
among close ones. After 8 years of development and 7 years of being live, the source code has finally been published.

**Background and Motivation**

The idea for Geeftlist originated from the complexity of gift exchanges within my family, especially during Christmas.
Each family member gives gifts to everyone else, making coordination essential to avoid duplicates. Traditional methods
like phone calls and emails were slow and error-prone, leading to the development of Geeftlist.

**Development Journey**

Geeftlist was initially built using PHP and the [Fat Free Framework](https://fatfreeframework.com/), with a focus on
server-side rendering and minimal JavaScript. The application includes features like user profiles, gift ideas,
reservations, and notifications.
Over the years, it has evolved to include more advanced features such as API integration, email notifications, and
user-defined visibility settings for gifts.

**Future Plans**

While the development pace has slowed in recent years, there are still many planned enhancements and improvements. The
roadmap includes features like an admin interface, improved onboarding, global search, and better management of gift
series and events. The goal is to continue evolving Geeftlist to better serve its users.

## 🏗️ Installation

⭐️ Want to self-host your own instance? [See instructions here](./support/docker/production/) ⭐️


## 📊 Monitoring & Healthcheck

A Prometheus endpoint is available at `{base_url}/system/healthcheck`.


## 🗓️ History

See [HISTORY.md](./HISTORY.md)


## 📜 Licence

See [LICENSE](./LICENSE)


## 👷 Developer Notes

☝️ The following instructions are only intended for **developers only**.  
If you want to deploy/install your own instance check the appropriate instructions [here](./support/docker/production/).

### Configuration

> As of 2019, developer environment uses Docker containers managed through a dedicated `Makefile`.

Run `make` once to create a prefilled `.env` file with your UID/GID.  
Then adjust this file to your needs, most of the configurations should already be okay.

### Force build Docker containers

> Use these instructions in case you modified `docker/base-web/Dockerfile` for example.

First, increment the version number in `.build.env` files for the following variables:

```shell
DOCKER_BASE_WEB_IMAGE_TAG
DOCKER_WEBAPP_IMAGE_TAG
```

Then, force rebuild all images (base + dev):

```shell
make dev-build-base-web-force
make dev-build
```

### Run Docker containers

Start the containers in background:

```shell
make dev-upd
```

> Images for containers will be built the first time you run this. It may take a while.

### Install application & sample data

```shell
make dev-install
```

> You might be required to enter your `sudo` password so that the script can set the right
> permissions on some folders.

### Build the assets

```shell
make dev-theme-build
```

If you work on JS and/or SASS/CSS files, you better use the watcher as presented below
in _Run Foundation watcher (for theme development)_.

### Enter shell as webapp user

```shell
make dev-bash
```

With this user, you can safely run CLI commands without messing with the permissions required by Apache.

### Run Unit Tests

```shell
make dev-run-unit-tests
```

> This will start a new ephemeral container at each execution, recreate the database and inject sample data.

When working on UT, you might instead want to enter a pre-parameterized container and use the helper script:

```shell
# Enter the container with the "test" context
make dev-bash-test

./resources/shell/run_unit_tests.sh [-R] [-U] [-I] [-s testSuite] [-G testGroup]
```

With:

- `-R`: Recreate database
- `-U`: Upgrade schema
- `-I`: Inject sample data

Make sure to pass `-R` and `-I` on the first run to create the database and inject sample data.  
If new setups need to be applied, use `-U` on the next run.

> Learn more here: [run_unit_tests.sh](resources/shell/run_unit_tests.sh)

You can also enable profiling using `-p` or `-P` flags like so:

```shell
./resources/shell/run_unit_tests.sh -P
```

`-p` Generates a XDebug profile, while `-P` generates a [XHProf](https://www.php.net/manual/en/book.xhprof.php) profile.

### Start the XHProf GUI

```shell
make dev-start-xhprofgui
```

You can then access the GUI using its dedicated port (default: [`48142`](http://localhost:48142/),
see `HOST_XHPROF_PORT`).


### Install application and sample data

```shell
make app-install && make app-install-sample-data
```

### Run Foundation watcher (for theme development)

```shell
make dev-theme-watch
```

In the event the dependencies would be changed/updated, you might need to clear the `node_modules`
directory and run install again from scratch;

```shell
make dev-theme-shell
# Then in the container:
cd app/ui/theme/default/
rm -rf node_modules/ && yarn install
```

### XDebug configuration

> :warning: Instructions are only available for PHPStorm.

Create a "Remote Debug" configuration in the IDE then associate a **server** named after the variable
`$DOCKER_DEV_XDEBUG_SERVERNAME` in `.env` (default: `geeftlist.local`).

Debugging HTTP request should then be available out-of-the-box with the right browser extension (e.g.
[Xdebug helper](https://addons.mozilla.org/firefox/addon/xdebug-helper-for-firefox/) and similar).

For CLI debugging from a shell inside the container (`make dev-bash`), just start a debug session with the
`-d xdebug.remote_autostart=1` flag on the command line.

```shell
$ make dev-bash
webapp@b2a4bbe3758b:~$ php -d xdebug.remote_autostart=1 src/tests/test.cli.php
```

For unit tests, the option `-d` of the wrapper script does the same under the hood.

```shell
$ make dev-bash
webapp@b2a4bbe3758b:~$ ./resources/shell/run_unit_tests.sh -d $(pwd)/src/app/code/Geeftlist/SomeTest.php
```

### Default Ports Summary

Environment | Container           | Internal Port | External Port (host) | Usage | Note
-|-|-|-|-|-
Docker Compose | reverse-proxy       | 443          | 48443 | HTTPS                 | **App HTTPS**
Docker Compose | db                  | 3306         | 48336 | MySQL                 |
Docker Compose | db-test             | 3306         | 48337 | MySQL                 | Unit test database
Docker Compose | mailcatcher         | 1080         | 48081 | Mailcatcher (Web GUI) |
Docker Compose | placeholder-service | 80           | 48082 | Placeholder image API |
